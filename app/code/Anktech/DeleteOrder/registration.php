<?php
/**
 * @author Anktech Team
 * @copyright Copyright (c) 2020 Anktech (http://anktech.co.in/)
 * @package Anktech_DeleteOrder
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Anktech_DeleteOrder',
    __DIR__
);
