<?php
/**
 * @author Anktech Team
 * @copyright Copyright (c) 2020 Anktech (http://anktech.co.in/)
 * @package Anktech_DeleteOrder
 */

namespace Anktech\DeleteOrder\Controller\Adminhtml\DeleteOrder;

use Anktech\DeleteOrder\Helper\Data;
use Exception;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Controller\Adminhtml\Order;
use Psr\Log\LoggerInterface;
use Flordelcampo\VendorInventory\Model\RevertInventoryForVendor;

/**
 * Class Delete Order Controller
 */
class DeleteOrder extends Order
{
    protected $order;
    protected $remove;
    protected $request;
    protected $revertInventoryForVendor;

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::delete';

    /**
     * @return $this|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $helper = $this->_objectManager->get(Data::class);
        if (!$helper->getStoreConfig(\Anktech\DeleteOrder\Helper\Data::CONFIG_DELETEORDER_STATUS)) {
            $this->messageManager->addError(__('Cannot delete the order.'));
            return $resultRedirect->setPath('sales/order/view', [
                'order_id' => $this->getRequest()->getParam('order_id')
            ]);
        }

        $order = $this->_initOrder();

        if ($order) {
            try {
                /** delete order*/
                $revertInventory = $this->_objectManager->create(RevertInventoryForVendor::class);
                foreach ($order->getItems() as $item) {
                    $revertInventory->revertInventoryForVendorByItemID($item->getItemId());
                }
                $this->orderRepository->delete($order);
                /** delete order data on grid report data related*/
                $helper->deleteOrderEntity($order->getId());

                $this->messageManager->addSuccessMessage(__('The order has been deleted.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
            } catch (Exception $e) {
                $this->messageManager->addErrorMessage(__('An error occurred while deleting the order. Please try again later.')); //phpcs:ignore
                $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);

                return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
            }
        }

        return $resultRedirect->setPath('sales/order/index');
    }
}
