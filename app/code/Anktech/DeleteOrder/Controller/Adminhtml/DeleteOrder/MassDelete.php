<?php
/**
 * @author Anktech Team
 * @copyright Copyright (c) 2020 Anktech (http://anktech.co.in/)
 * @package Anktech_DeleteOrder
 */

namespace Anktech\DeleteOrder\Controller\Adminhtml\DeleteOrder;

use Flordelcampo\VendorInventory\Model\RevertInventoryForVendor;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction;
use Magento\Sales\Model\OrderRepository;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Anktech\DeleteOrder\Helper\Data as DataHelper;

/**
 * Class Mass Delete Controller
 */
class MassDelete extends AbstractMassAction
{
    /**
     * Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::delete';

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var DataHelper
     */
    protected $helper;

    protected $_orderCollectionFactory;

    protected $request;

    protected $filter;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param OrderRepository $orderRepository
     * @param DataHelper $dataHelper
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        OrderRepository $orderRepository,
        DataHelper $dataHelper
    )
    {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->orderRepository = $orderRepository;
        $this->helper = $dataHelper;
    }

    /**
     * @param AbstractCollection $collection
     * @return Redirect|ResponseInterface|ResultInterface
     */
    protected function massAction(AbstractCollection $collection)
    {
        if ($this->helper->getStoreConfig(\Anktech\DeleteOrder\Helper\Data::CONFIG_DELETEORDER_STATUS)) {
            $deleted = 0;
            $allStatus = explode(',', $this->helper->getStoreConfig(\Anktech\DeleteOrder\Helper\Data::CONFIG_ORDER_STATUS));

            /** @var OrderInterface $order */
            foreach ($collection->getItems() as $order) {
                $status = $order->getStatus();

                if (is_array($allStatus) && !empty($allStatus) && !in_array($status, $allStatus)) {
                    continue;
                }

                try {
                    /** delete order*/
                    $revertInventory = $this->_objectManager->create(RevertInventoryForVendor::class);
                    foreach ($order->getItems() as $item) {
                        $revertInventory->revertInventoryForVendorByItemID($item->getItemId());
                    }
                    $this->orderRepository->delete($order);
                    /** delete order data on grid report data related*/
                    $this->helper->deleteOrderEntity($order->getId());
                    $deleted++;
                } catch (Exception $e) {
                    $this->logger->critical($e);
                    $this->messageManager->addErrorMessage(__('Cannot delete order #%1. Please try again later.', $order->getIncrementId())); //phpcs:ignore
                }
            }
            if ($deleted) {
                $this->messageManager->addSuccessMessage(__('A total of %1 order(s) has been deleted.', $deleted));
            }
        }
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('sales/order/index');

        return $resultRedirect;
    }
}
