<?php
/**
 * @author Anktech Team
 * @copyright Copyright (c) 2020 Anktech (http://anktech.co.in/)
 * @package Anktech_DeleteOrder
 */
namespace Anktech\DeleteOrder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Sales\Model\ResourceModel\OrderFactory;

/**
 * Class Data Helper
 */
class Data extends AbstractHelper
{
    const CONFIG_DELETEORDER_STATUS  = 'deleteorderconfig/general/enable';
    const CONFIG_ORDER_STATUS  = 'deleteorderconfig/general/order_status';

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var OrderFactory
     */
    private $orderResourceFactory;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param OrderFactory $orderResourceFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        OrderFactory $orderResourceFactory
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->orderResourceFactory = $orderResourceFactory;
        parent::__construct($context);
    }

    /**
     * Get Configuration
     *
     * @param string $path
     * @return void
     */
    public function getStoreConfig($path)
    {
        $storeconfig = $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $storeconfig;
    }
    
    /**
     * Delete Invoice, Shipment & Creditmemo
     *
     * @param int $orderId
     * @return void
     */
    public function deleteOrderEntity($orderId)
    {
        /** @var Order $resource */
        $resource = $this->orderResourceFactory->create();
        $connection = $resource->getConnection();

        /** delete invoice grid record via resource model*/
        $connection->delete(
            $resource->getTable('sales_invoice_grid'),
            $connection->quoteInto('order_id = ?', $orderId)
        );

        /** delete shipment grid record via resource model*/
        $connection->delete(
            $resource->getTable('sales_shipment_grid'),
            $connection->quoteInto('order_id = ?', $orderId)
        );

        /** delete creditmemo grid record via resource model*/
        $connection->delete(
            $resource->getTable('sales_creditmemo_grid'),
            $connection->quoteInto('order_id = ?', $orderId)
        );
    }
}
