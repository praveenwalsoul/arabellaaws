<?php
/**
 * @author Anktech Team
 * @copyright Copyright (c) 2020 Anktech (https://www.anktech.co.in)
 * @package Anktech_DeleteOrder
 */

namespace Anktech\DeleteOrder\Model\Config\Source\Order;

use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;

class Status implements \Magento\Framework\Option\ArrayInterface
{

    protected $_options = null;

    /**
     * @var CollectionFactory $statusCollectionFactory
     */
    protected $orderStatusCollectionFactory;
    /**
     * @param CollectionFactory $orderStatusCollectionFactory
     */
    public function __construct(
        CollectionFactory $orderStatusCollectionFactory
    ) {
        $this->orderStatusCollectionFactory = $orderStatusCollectionFactory;
    }
    
    /**
     * Get all options
     *
     * @return void
     */
    public function toOptionArray()
    {
        if (null === $this->_options) {
            $options = $this->orderStatusCollectionFactory->create()->toOptionArray();
            $this->_options = $options;
        }
        return $this->_options;
    }
}