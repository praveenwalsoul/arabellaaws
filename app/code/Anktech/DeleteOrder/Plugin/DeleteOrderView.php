<?php
/**
 * @author Anktech Team
 * @copyright Copyright (c) 2020 Anktech (http://anktech.co.in/)
 * @package Anktech_DeleteOrder
 */
namespace Anktech\DeleteOrder\Plugin;

use Magento\Backend\Model\UrlInterface;
use Anktech\DeleteOrder\Helper\Data;
use Magento\Framework\View\Element\UiComponent\Context;
use Magento\Sales\Api\OrderRepositoryInterface;

class DeleteOrderView
{
    protected $_backendUrl;
    protected $helper;
    protected $context;
    protected $order;
    protected $orderRepository;
    protected $_coreRegistry;

    /**
     * @param Data $helper
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Model\Order $order
     * @param OrderRepositoryInterface $orderRepository
     * @param UrlInterface $backendUrl
     * @param Context $context
     */
    public function __construct(
        Data $helper,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\Order $order,
        OrderRepositoryInterface $orderRepository,
        UrlInterface $backendUrl,
        Context $context
    ) {
        $this->_coreRegistry = $registry;
        $this->helper = $helper;
        $this->orderRepository = $orderRepository;
        $this->_backendUrl = $backendUrl;
        $this->context = $context;
        $this->order = $order;
    }

    /**
     * Get current order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }
    
    /**
     * Get base url()
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrl($route, $params);
    }

    /**
     * Get Delete Order URL 
     *
     * @return void
     */
    public function getDeleteOrderUrl()
    {
        return $this->getUrl('deleteorder/deleteorder/deleteorder', ['_current'=>true]);
    }

    /**
     * @param \Magento\Sales\Block\Adminhtml\Order\View $subject
     * @return void
     */
    public function beforeSetLayout(\Magento\Sales\Block\Adminhtml\Order\View $subject)
    {
        if ($this->helper->getStoreConfig(\Anktech\DeleteOrder\Helper\Data::CONFIG_DELETEORDER_STATUS)) {
            $order = $this->getOrder();
            $status = $order->getStatus();
            $allStatus = explode(',', $this->helper->getStoreConfig(\Anktech\DeleteOrder\Helper\Data::CONFIG_ORDER_STATUS));
            
            if (is_array($allStatus) && !empty($allStatus) && in_array($status, $allStatus)) {
                $deleteorder = __('Are you sure you want to delete this order?');
                $subject->addButton(
                    'deleteorder',
                    [
                        'label' => __('Delete Order'),
                        'class'   => 'delete',
                        'onclick' => "confirmSetLocation('{$deleteorder}', '{$this->getDeleteOrderUrl()}')"
                    ]
                );
            }
        }
    }
}
