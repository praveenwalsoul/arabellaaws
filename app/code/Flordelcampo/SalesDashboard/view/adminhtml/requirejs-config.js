var config = {
    map: {
        '*': {
            chart: 'Flordelcampo_SalesDashboard/js/chart.min',
            accessibility: 'Flordelcampo_SalesDashboard/js/accessibility',
            highcharts: 'Flordelcampo_SalesDashboard/js/highcharts',
            exporting: 'Flordelcampo_SalesDashboard/js/exporting' 
        }
    },
   shim: {
        highcharts : 
        {
            deps: ['jquery']
        }
    }
};

// var config = {
//     paths: {
//         highchart: "Flordelcampo_SalesDashboard/js/highcharts"
//     },
//     shim: {
//         highchart: {
//             deps: ['jquery']
//         }
//     }
// }