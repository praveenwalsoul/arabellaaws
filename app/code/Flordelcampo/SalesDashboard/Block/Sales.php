<?php

namespace Flordelcampo\SalesDashboard\Block;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Flordelcampo\Vendor\Helper\Data as salesHelper;
use Flordelcampo\Order\Helper\Data as salesOrderHelper;
use Flordelcampo\Catalog\Helper\Data as salesCategoryHelper;
use Magento\Framework\App\ResourceConnection as dataConnection;

/**
 * Class Ups
 *
 * Flordelcampo\Dashboards\Block
 */
class Sales extends Template
{

    /**
     * @var vendorHelper
     */
    protected $salesHelper;
    /**
     * @var Salesorder
     */
    protected $orderItemCollectionFactory;
    /**
     * @var Salesorder
     */


    protected $salesOrderHelper;
    protected $salesCategoryHelper;
    protected $_params;
    protected $dataConnection;

    protected $filter = [];

    /**
     * Ups constructor.
     * @param Context $context
     * @param vendorHelper $vendorHelper
     * @param orderHelper $orderHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        salesHelper $salesHelper,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory,
        salesOrderHelper $salesOrderHelper,
        salesCategoryHelper $salesCategoryHelper,
        dataConnection $dataConnection,
        array $data = []
    )
    {
        $this->dataConnection = $dataConnection;
        $this->salesHelper = $salesHelper;
        $this->orderItemCollectionFactory = $orderItemCollectionFactory;
        $this->salesOrderHelper = $salesOrderHelper;
        $this->salesCategoryHelper = $salesCategoryHelper;
        parent::__construct($context, $data);
    }


    protected function _prepareLayout(){

        $this->initFilters();

        return $this;
    }
 
    /**
     * Sales Order
     * @return false|string
     */

     public function getOrderCollection(){


         $startDate = $this->getFilterField('startDate');
         $endDate = $this->getFilterField('endDate');
         $salestype = $this->getFilterField('sales_type');
         $datetype = $this->getFilterField('data_type');
         $minute = $this->getFilterField('minute_select');
         $vendorId = $this->getFilterField('vendorId');
         
         if($vendorId ==''){
         	$vendorId = 5;
         }
         if($startDate ==''){
         	$startDate =date('Y-m-d');
         }
         if($salestype ==''){
         	$salestype ="order_count";
         }
         if($startDate ==''){
         	$startDate =date('Y-m-d');
         }
         if(empty($datetype)){
         	$datetype = "order_date";

         }
         if($minute == 'MINUTE'){
            $minute = 'MINUTE';

            $startDate =  date('Y-m-d h:i:s',time()-3600);
            $endDate =  date('Y-m-d h:i:s',time());

            $onedayBeforeSD = date('Y-m-d h:i:s',time()-24*3600-3600);
            $onedayBeforeED = date('Y-m-d h:i:s',time()-24*3600);

            $weekBeforeSD = date('Y-m-d h:i:s',time()-7*24*3600-3600);
            $weekBeforeED = date('Y-m-d h:i:s',time()-7*24*3600); 

            $PrevMonthBeforeSD = date('Y-m-d h:i:s',time()-24*30*3600-3600);
            $PrevMonthBeforeED = date('Y-m-d h:i:s',time()-24*30*3600);
            

            $Prev3MonthBeforeSD = date('Y-m-d h:i:s',time()-24*90*3600-3600);
            $Prev3MonthBeforeED = date('Y-m-d h:i:s',time()-24*90*3600);
            
            $Prev6MonthBeforeSD = date('Y-m-d h:i:s',time()-24*180*3600-3600);
            $Prev6MonthBeforeED = date('Y-m-d h:i:s',time()-24*180*3600);

            $prevYearSD = date('Y-m-d h:i:s',time()-60*60*24*365 -3600);
            $prevYearED = date('Y-m-d h:i:s',time()-60*60*24*365);
              
        }

        if($minute == 'HOUR'){
            $minute = 'HOUR';

            $startDate =  date('Y-m-d h:i:s',time()-24*3600);
            $endDate =  date('Y-m-d h:i:s',time());

            $onedayBeforeSD = date('Y-m-d h:i:s',time()-24*3600-24*3600);
            $onedayBeforeED = date('Y-m-d h:i:s',time()-24*3600);

            $weekBeforeSD = date('Y-m-d h:i:s',time()-7*24*3600-24*3600);
            $weekBeforeED = date('Y-m-d h:i:s',time()-7*24*3600); 

            $PrevMonthBeforeSD = date('Y-m-d h:i:s',time()- 24*30*3600-24*3600);
            $PrevMonthBeforeED = date('Y-m-d h:i:s',time()- 24*30*3600);

            $Prev3MonthBeforeSD = date('Y-m-d h:i:s',time()- 24*90*3600-24*3600);
            $Prev3MonthBeforeED = date('Y-m-d h:i:s',time()- 24*90*3600);
            
            $Prev6MonthBeforeSD = date('Y-m-d h:i:s',time()- 24*180*3600-24*3600);
            $Prev6MonthBeforeED = date('Y-m-d h:i:s',time()- 24*180*3600);
            
            $prevYearSD = date('Y-m-d h:i:s',time()-60*60*24*365-24*3600);
            $prevYearED = date('Y-m-d h:i:s',time()-60*60*24*365);
        }

        if($minute == 'DAY' || empty($minute)){
            $minute = 'DAYOFWEEK';
           // $minute = 'DAY';

            $startDate =  date('Y-m-d',time()-6*24*3600);
            $endDate =  date('Y-m-d h:i:s',time());

            $onedayBeforeSD = date('Y-m-d',time()-24*3600-6*24*3600);
            $onedayBeforeED = date('Y-m-d h:i:s',time()-24*3600);

            $weekBeforeSD = date('Y-m-d',time()-7*24*3600-6*24*3600);
            $weekBeforeED = date('Y-m-d h:i:s',time()-7*24*3600);

            $PrevMonthBeforeSD = date('Y-m-d',time()-24*30*3600-6*24*3600);
            $PrevMonthBeforeED = date('Y-m-d h:i:s',time()-24*30*3600);
            

            $Prev3MonthBeforeSD = date('Y-m-d',time()-24*90*3600-6*24*3600);
            $Prev3MonthBeforeED = date('Y-m-d h:i:s',time()-24*90*3600);
            
            $Prev6MonthBeforeSD = date('Y-m-d',time()-24*180*3600-6*24*3600);
            $Prev6MonthBeforeED = date('Y-m-d h:i:s',time()-24*180*3600);

            $prevYearSD = date('Y-m-d',time()-60*60*24*365-6*24*3600);
            $prevYearED = date('Y-m-d h:i:s',time()-60*60*24*365);
            
              
        }



        if($minute == 'WEEK'){
        	 $minute = 'WEEK';

            $startDate =  date('Y-m-d h:i:s',time()-8*7*24*3600);
            $endDate =  date('Y-m-d h:i:s',time());

            $onedayBeforeSD = date('Y-m-d h:i:s',time()-24*3600-8*7*24*3600);
            $onedayBeforeED = date('Y-m-d h:i:s',time()-24*3600);

            $weekBeforeSD = date('Y-m-d h:i:s',time()-7*24*3600-8*7*24*3600);
            $weekBeforeED = date('Y-m-d h:i:s',time()-7*24*3600); 

            $PrevMonthBeforeSD = date('Y-m-d h:i:s',time()-7*24*3600-8*7*24*3600);
            $PrevMonthBeforeED = date('Y-m-d h:i:s',time()-7*24*3600);

            $Prev3MonthBeforeSD = date('Y-m-d h:i:s',time()-24*90*3600-8*7*24*3600);
            $Prev3MonthBeforeED = date('Y-m-d h:i:s',time()-24*90*3600);
            
            $Prev6MonthBeforeSD = date('Y-m-d h:i:s',time()-24*180*3600-8*7*24*3600);
            $Prev6MonthBeforeED = date('Y-m-d h:i:s',time()-24*180*3600);

            $prevYearSD = date('Y-m-d h:i:s',time()-60*60*24*365-8*7*24*3600);
            $prevYearED = date('Y-m-d h:i:s',time()-60*60*24*365);
        }

        if($minute == 'MONTH' || empty($minute)){
        	   $minute = 'MONTH';

            $startDate =  date('Y-m-d h:i:s',time()-365*24*3600);
            $endDate =  date('Y-m-d h:i:s',time());

            $onedayBeforeSD = date('Y-m-d h:i:s',time()-24*3600-365*24*3600);
            $onedayBeforeED = date('Y-m-d h:i:s',time()-24*3600);

            $weekBeforeSD = date('Y-m-d h:i:s',time()-7*24*3600-365*24*3600);
            $weekBeforeED = date('Y-m-d h:i:s',time()-7*24*3600); 

            $PrevMonthBeforeSD = date('Y-m-d h:i:s',time()-7*24*3600-365*24*3600);
            $PrevMonthBeforeED = date('Y-m-d h:i:s',time()-7*24*3600-24*3600);

            $Prev3MonthBeforeSD = date('Y-m-d h:i:s',time()-24*90*3600-365*24*3600);
            $Prev3MonthBeforeED = date('Y-m-d h:i:s',time()-24*90*3600);
            
            $Prev6MonthBeforeSD = date('Y-m-d h:i:s',time()-24*180*3600-365*24*3600);
            $Prev6MonthBeforeED = date('Y-m-d h:i:s',time()-24*180*3600);

            $prevYearSD = date('Y-m-d h:i:s',time()-60*60*24*365-365*24*3600);
            $prevYearED = date('Y-m-d h:i:s',time()-60*60*24*365);
        }
        if($minute == 'YEAR' || empty($minute)){
        	  $minute = 'YEAR';

            $startDate =  date('Y-m-d h:i:s',time()-24*3600);
            $endDate =  date('Y-m-d h:i:s',time());

            $onedayBeforeSD = date('Y-m-d h:i:s',time()-24*3600-24*3600);
            $onedayBeforeED = date('Y-m-d h:i:s',time()-24*3600);

            $weekBeforeSD = date('Y-m-d h:i:s',time()-7*24*3600-24*3600);
            $weekBeforeED = date('Y-m-d h:i:s',time()-7*24*3600); 

            $PrevMonthBeforeSD = date('Y-m-d h:i:s',time()-7*24*3600-24*3600);
            $PrevMonthBeforeED = date('Y-m-d h:i:s',time()-7*24*3600);

            $Prev3MonthBeforeSD = date('Y-m-d h:i:s',time()-24*90*3600-3600);
            $Prev3MonthBeforeED = date('Y-m-d h:i:s',time()-24*90*3600);
            
            $Prev6MonthBeforeSD = date('Y-m-d h:i:s',time()-24*180*3600-3600);
            $Prev6MonthBeforeED = date('Y-m-d h:i:s',time()-24*180*3600);

            $prevYearSD = date('Y-m-d h:i:s',time()-60*60*24*365);
            $prevYearED = date('Y-m-d h:i:s',time()-60*60*24*365);
        }

        $collection = $this->orderItemCollectionFactory->create()
                      ->addFieldToSelect(['created_at','row_total_incl_tax','pickup_date','vendor','sku','product_id','order_id','order_item_status'])
                      ->addFieldToFilter('main_table.created_at', array('gteq' => $startDate))
                      ->addFieldToFilter('main_table.created_at', array('lteq' => $endDate));

         //echo $collection->getSelect();die;
         $collection->getSelect()->joinLeft(
            ['orderitembox_alias' => $collection->getResource()->getTable('order_item_box')],
            'main_table.item_id = orderitembox_alias.item_id',
            [
                'order_item_box_status' => 'orderitembox_alias.status',
                'order_item_box_gift_message' => 'orderitembox_alias.gift_message'
            ]
        );   
         
        if($salestype == "order_count"){
           
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('COUNT(distinct main_table.order_id)')]);
        }

        if($salestype == "revenue"){
            
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('SUM(row_total_incl_tax)')]);
           
        }

        if($datetype == "order_date"){

           
            $exp = $minute.'(main_table.created_at)';
          
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);

        }

        if($datetype == "ship_date"){
            $exp = $minute.'(main_table.pickup_date)';       
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);
            
        }

        $collection->getSelect()->joinLeft(
            ['vendor_reg' => $collection->getResource()->getTable('vendor_registrations')],
            'main_table.vendor = vendor_reg.vendor_id',
            [
                'vendor_name' => 'vendor_reg.vendor_name'
            ]
        );

        if($vendorId = $this->getFilterField('vendorId')){

            $collection->getSelect()->where("main_table.vendor = ?", $vendorId);
        }

        if($minute == "xyz"){
        	$collection->getSelect()->group('main_table.created_at');

        }else{
        	$collection->getSelect()->group('x');
        }
       
        $finalCollection['first'] = $collection->getData();

        $collection = $this->orderItemCollectionFactory->create()
                      ->addFieldToSelect(['created_at','row_total_incl_tax','pickup_date','vendor','sku','product_id','order_id','order_item_status'])
                      ->addFieldToFilter('main_table.created_at', array('gteq' => $onedayBeforeSD))
                      ->addFieldToFilter('main_table.created_at', array('lteq' => $onedayBeforeED));

         $collection->getSelect()->joinLeft(
            ['orderitembox_alias' => $collection->getResource()->getTable('order_item_box')],
            'main_table.item_id = orderitembox_alias.item_id',
            [
                'order_item_box_status' => 'orderitembox_alias.status',
                'order_item_box_gift_message' => 'orderitembox_alias.gift_message'
            ]
        );       

        if($salestype == "order_count"){

           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('COUNT(distinct main_table.order_id)')]);
           

        }
        if($salestype == "revenue"){
            
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('SUM(row_total_incl_tax)')]);
            
        }
        
        if($datetype == "order_date"){

           
            $exp = $minute.'(main_table.created_at)';
          
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);

        }

        if($datetype == "ship_date"){
            $exp = $minute.'(main_table.pickup_date)';        
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);
            
        }

        $collection->getSelect()->joinLeft(
            ['vendor_reg' => $collection->getResource()->getTable('vendor_registrations')],
            'main_table.vendor = vendor_reg.vendor_id',
            [
                'vendor_name' => 'vendor_reg.vendor_name'
            ]
        );

        if($vendorId = $this->getFilterField('vendorId')){

            $collection->getSelect()->where("main_table.vendor = ?", $vendorId);
        }

        if($minute == "xyz"){
            $collection->getSelect()->group('main_table.created_at');

        }else{
            $collection->getSelect()->group('x');
        }
       
        $finalCollection['second'] = $collection->getData();

        $collection = $this->orderItemCollectionFactory->create()
                      ->addFieldToSelect(['created_at','row_total_incl_tax','pickup_date','vendor','sku','product_id','order_id','order_item_status'])
                      ->addFieldToFilter('main_table.created_at', array('gteq' => $weekBeforeSD))
                      ->addFieldToFilter('main_table.created_at', array('lteq' => $weekBeforeED));

         $collection->getSelect()->joinLeft(
            ['orderitembox_alias' => $collection->getResource()->getTable('order_item_box')],
            'main_table.item_id = orderitembox_alias.item_id',
            [
                'order_item_box_status' => 'orderitembox_alias.status',
                'order_item_box_gift_message' => 'orderitembox_alias.gift_message'
            ]
        );   
    
        if($salestype == "order_count"){
           
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('COUNT(distinct main_table.order_id)')]);

        }
        if($salestype == "revenue"){
            
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('SUM(row_total_incl_tax)')]);
            
        }

        if($datetype == "order_date"){

           
            $exp = $minute.'(main_table.created_at)';
          
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);

        }
        
        if($datetype == "ship_date"){
            $exp = $minute.'(main_table.pickup_date)';      
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);
            
        }

        $collection->getSelect()->joinLeft(
            ['vendor_reg' => $collection->getResource()->getTable('vendor_registrations')],
            'main_table.vendor = vendor_reg.vendor_id',
            [
                'vendor_name' => 'vendor_reg.vendor_name'
            ]
        );

        if($vendorId = $this->getFilterField('vendorId')){

            $collection->getSelect()->where("main_table.vendor = ?", $vendorId);
        }

        if($minute == "xyz"){
            $collection->getSelect()->group('main_table.created_at');

        }else{
            $collection->getSelect()->group('x');
        }
       
        $finalCollection['third'] = $collection->getData();

        /*======= fourth array  ====================================================== */

        $collection = $this->orderItemCollectionFactory->create()
                      ->addFieldToSelect(['created_at','row_total_incl_tax','pickup_date','vendor','sku','product_id','order_id','order_item_status'])
                      ->addFieldToFilter('main_table.created_at', array('gteq' => $PrevMonthBeforeSD))
                      ->addFieldToFilter('main_table.created_at', array('lteq' => $PrevMonthBeforeED));

         $collection->getSelect()->joinLeft(
            ['orderitembox_alias' => $collection->getResource()->getTable('order_item_box')],
            'main_table.item_id = orderitembox_alias.item_id',
            [
                'order_item_box_status' => 'orderitembox_alias.status',
                'order_item_box_gift_message' => 'orderitembox_alias.gift_message'
            ]
        );   
      
        if($salestype == "order_count"){
           
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('COUNT(distinct main_table.order_id)')]);
           

        }
        if($salestype == "revenue"){
            
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('SUM(row_total_incl_tax)')]);
           
        }
         
        if($datetype == "order_date"){

           
            $exp = $minute.'(main_table.created_at)';
          
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);

        }
       
        if($datetype == "ship_date"){

            $exp = $minute.'(main_table.pickup_date)';         
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);
            
        }

        $collection->getSelect()->joinLeft(
            ['vendor_reg' => $collection->getResource()->getTable('vendor_registrations')],
            'main_table.vendor = vendor_reg.vendor_id',
            [
                'vendor_name' => 'vendor_reg.vendor_name'
            ]
        );

        if($vendorId = $this->getFilterField('vendorId')){

            $collection->getSelect()->where("main_table.vendor = ?", $vendorId);
        }

        if($minute == "xyz"){
            $collection->getSelect()->group('main_table.created_at');

        }else{
            $collection->getSelect()->group('x');
        }
       
        $finalCollection['fourth'] = $collection->getData();
        /*============ end fourth array   ======================================*/

         /*===== fifth array  ====================================================== */
        $collection = $this->orderItemCollectionFactory->create()
                      ->addFieldToSelect(['created_at','row_total_incl_tax','pickup_date','vendor','sku','product_id','order_id','order_item_status'])
                      ->addFieldToFilter('main_table.created_at', array('gteq' => $Prev3MonthBeforeSD))
                      ->addFieldToFilter('main_table.created_at', array('lteq' => $Prev3MonthBeforeED));

         //echo $collection->getSelect();die;
         $collection->getSelect()->joinLeft(
            ['orderitembox_alias' => $collection->getResource()->getTable('order_item_box')],
            'main_table.item_id = orderitembox_alias.item_id',
            [
                'order_item_box_status' => 'orderitembox_alias.status',
                'order_item_box_gift_message' => 'orderitembox_alias.gift_message'
            ]
        );          

        if($salestype == "order_count"){

           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('COUNT(distinct main_table.order_id)')]);

        }
        if($salestype == "revenue"){
            
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('SUM(row_total_incl_tax)')]);
            
        }
        
        if($datetype == "order_date"){

           
            $exp = $minute.'(main_table.created_at)';
          
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);

        }

        if($datetype == "ship_date"){
            $exp = $minute.'(main_table.pickup_date)';
                     
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);
            
        }

        $collection->getSelect()->joinLeft(
            ['vendor_reg' => $collection->getResource()->getTable('vendor_registrations')],
            'main_table.vendor = vendor_reg.vendor_id',
            [
                'vendor_name' => 'vendor_reg.vendor_name'
            ]
        );

        if($vendorId = $this->getFilterField('vendorId')){

            $collection->getSelect()->where("main_table.vendor = ?", $vendorId);
        }

       
        if($minute == "xyz"){
            $collection->getSelect()->group('main_table.created_at');

        }else{
            $collection->getSelect()->group('x');
        }
       
        $finalCollection['fifth'] = $collection->getData();

        /*============ end fifth array   ======================================*/
        
        /*===== sixth array  ====================================================== */
        $collection = $this->orderItemCollectionFactory->create()
                      ->addFieldToSelect(['created_at','row_total_incl_tax','pickup_date','vendor','sku','product_id','order_id','order_item_status'])
                      ->addFieldToFilter('main_table.created_at', array('gteq' => $Prev6MonthBeforeSD))
                      ->addFieldToFilter('main_table.created_at', array('lteq' => $Prev6MonthBeforeED));

         //echo $collection->getSelect();die;
         $collection->getSelect()->joinLeft(
            ['orderitembox_alias' => $collection->getResource()->getTable('order_item_box')],
            'main_table.item_id = orderitembox_alias.item_id',
            [
                'order_item_box_status' => 'orderitembox_alias.status',
                'order_item_box_gift_message' => 'orderitembox_alias.gift_message'
            ]
        );          

        if($salestype == "order_count"){

           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('COUNT(distinct main_table.order_id)')]);

        }
        if($salestype == "revenue"){
            
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('SUM(row_total_incl_tax)')]);
            
        }
        
        if($datetype == "order_date"){

           
            $exp = $minute.'(main_table.created_at)';
          
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);

        }

        if($datetype == "ship_date"){
            $exp = $minute.'(main_table.pickup_date)';
                     
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);
            
        }

        $collection->getSelect()->joinLeft(
            ['vendor_reg' => $collection->getResource()->getTable('vendor_registrations')],
            'main_table.vendor = vendor_reg.vendor_id',
            [
                'vendor_name' => 'vendor_reg.vendor_name'
            ]
        );

        if($vendorId = $this->getFilterField('vendorId')){

            $collection->getSelect()->where("main_table.vendor = ?", $vendorId);
        }

       
        if($minute == "xyz"){
            $collection->getSelect()->group('main_table.created_at');

        }else{
            $collection->getSelect()->group('x');
        }
       
        $finalCollection['sixth'] = $collection->getData();

        /*============ end sixth array   ======================================*/
         /*===== seventh array  ====================================================== */
        $collection = $this->orderItemCollectionFactory->create()
                      ->addFieldToSelect(['created_at','row_total_incl_tax','pickup_date','vendor','sku','product_id','order_id','order_item_status'])
                      ->addFieldToFilter('main_table.created_at', array('gteq' => $prevYearSD))
                      ->addFieldToFilter('main_table.created_at', array('lteq' => $prevYearED));

         //echo $collection->getSelect();die;
         $collection->getSelect()->joinLeft(
            ['orderitembox_alias' => $collection->getResource()->getTable('order_item_box')],
            'main_table.item_id = orderitembox_alias.item_id',
            [
                'order_item_box_status' => 'orderitembox_alias.status',
                'order_item_box_gift_message' => 'orderitembox_alias.gift_message'
            ]
        );          

        if($salestype == "order_count"){

           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('COUNT(distinct main_table.order_id)')]);

        }
        if($salestype == "revenue"){
            
           $collection->getSelect()->columns(['y' => new \Zend_Db_Expr('SUM(row_total_incl_tax)')]);
            
        }
        
        if($datetype == "order_date"){

           
            $exp = $minute.'(main_table.created_at)';
          
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);

        }

        if($datetype == "ship_date"){
            $exp = $minute.'(main_table.pickup_date)';
                     
           $collection->getSelect()->columns(['x' => new \Zend_Db_Expr($exp)]);
            
        }

        $collection->getSelect()->joinLeft(
            ['vendor_reg' => $collection->getResource()->getTable('vendor_registrations')],
            'main_table.vendor = vendor_reg.vendor_id',
            [
                'vendor_name' => 'vendor_reg.vendor_name'
            ]
        );

        if($vendorId = $this->getFilterField('vendorId')){

            $collection->getSelect()->where("main_table.vendor = ?", $vendorId);
        }

       
        if($minute == "xyz"){
            $collection->getSelect()->group('main_table.created_at');

        }else{
            $collection->getSelect()->group('x');
        }
       
        $finalCollection['seventh'] = $collection->getData();

        /*============ end seventh array   ======================================*/


        return($finalCollection);
        
    }

    public function initFilters(){

        $filter = $this->getRequest()->getParam('filter');

        if($filter){
            $data = [];
            $filter = base64_decode($filter);
            parse_str($filter, $this->filter);
        }

        return $this->filter;
    }

    public function getFilterField($field){

        if(isset($this->filter[$field]) && !empty($this->filter[$field])){

            return $this->filter[$field];
        }

        return null; 
    }

    /**
     * Order To Date
     * @return false|string
     */

    public function createMinutes($id='minute_select', $selected=null)
    {
       
        $mint = 'MINUTE';
        $day = 'DAY';
        $hoursly = 'HOUR'; 
        $Weekly = 'WEEK';
        $monthly = 'MONTH';
        $annualy = 'YEAR';
        
        $minutes = array(
            "Minute" => $mint,
            "Per Day" => $day,
            "Hourly" => $hoursly,
            "Weekly" => $Weekly,
            "Monthly" => $monthly,
            "Annualy" => $annualy
        );
        //print_r($minutes);die("test");

    $selected = in_array($selected, $minutes) ? $selected : 0;

        $select = "<select name=\"$id\" id=\"$id\">\n";
        foreach($minutes as $option => $min)
        {
            $select .= "<option value=\"$min\"";
            $select .= ($option==$selected) ? ' selected="selected"' : '';
            $select .= ">".str_pad($option, 2, '0')."</option>\n";
        }
        $select .= '</select>';
        return $select;
    }

    /**
     * Get vendors list
     *
     * @return array
     */
    public function getAllVendors()
    {
        return $this->salesHelper->getAllVendorListArray();
    }

    /**
     * Store List
     *
     * @return array
     */
    
    public function getAllStores()
    {

        $storeArray = $this->salesOrderHelper->getStoreListArray();
        $storeArray[''] = 'all';
        asort($storeArray);
        return $storeArray;
    } 
    
    public function getOrderCollectionByIds($orderArray)
    {
        return $this->salesOrderHelper->getOrderCollectionByIds($orderArray);
    }

}