<?php

namespace Flordelcampo\CronJobs\Model;

use Flordel\Amazon\Model\ListOrders;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;

class UpdateAmazonPrice
{
    const CONFIG_PATH = 'flordel_cron_configuration/general/from_date_interval';
    /**
     * @var ListOrders
     */
    protected $listOrders;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * UpdateAmazonPrice constructor.
     * @param ListOrders $listOrders
     * @param OrderHelper $orderHelper
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ListOrders $listOrders,
        OrderHelper $orderHelper,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->listOrders = $listOrders;
        $this->orderHelper = $orderHelper;
    }

    /**
     * Update Order Item Price
     * @param null $fromDate
     * @param null $toDate
     * @return string
     */
    public function updateOrderItemPrice($fromDate = null, $toDate = null)
    {
        $msg = __('price updated successfully');
        $today = date('Y-m-d');
        if ($fromDate == null) {
            $intervalDays = $this->getTimeInterval();
            $interval = '-' . $intervalDays . ' Days';
            $fromDate = date('Y-m-d', strtotime($interval, strtotime($today)));
            $toDate = $today;
        }
        $data = $this->listOrders->getOrdersItemsPriceByDate($fromDate, $toDate);
        if (count($data) > 0) {
            foreach ($data as $partner_po => $price) {
                try {
                    $params['partner_po'] = $partner_po;
                    $orderItemCollection = $this->orderHelper->getOrderItemCollection($params);
                    if ($orderItemCollection->getSize() > 0) {
                        foreach ($orderItemCollection as $item) {
                            $rowTotal = $item->getQtyOrdered() * $price;
                            $item->setData('price', $price);
                            $item->setData('row_total', $rowTotal);
                            $item->save();
                        }
                    }
                } catch (\Exception $e) {
                    $this->writeLog($e->getMessage());
                    continue;
                }
            }
        } else {
            $msg = __('No orders to update price');
        }
        $this->writeLog($msg);
        return $msg;
    }
    /**
     * @return mixed
     */
    public function getTimeInterval()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    /**
     * Write Log
     * @param $message
     */
    public function writeLog($message)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/update_amazon_price.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
        /*$logger->debug('array', $array);*/
    }
}