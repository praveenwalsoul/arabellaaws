<?php

namespace Flordelcampo\CronJobs\Cron;

use Flordelcampo\CronJobs\Model\UpdateAmazonPrice;

/**
 * Class AmazonPriceUpdate
 * Flordelcampo\CronJobs\Cron
 */
class AmazonPriceUpdate
{
    /**
     * @var UpdateAmazonPrice
     */
    protected $updateAmazonPrice;

    /**
     * AmazonPriceUpdate constructor.
     * @param UpdateAmazonPrice $updateAmazonPrice
     */
    public function __construct(
        UpdateAmazonPrice $updateAmazonPrice
    ) {
        $this->updateAmazonPrice = $updateAmazonPrice;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        try {
            $msg = $this->updateAmazonPrice->updateOrderItemPrice();
            $this->writeLog($msg);
        } catch (\Exception $e) {
            $this->writeLog($e->getMessage());
            return $this;
        }
        return $this;
    }

    public function writeLog($msg)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testing.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($msg);
    }
}