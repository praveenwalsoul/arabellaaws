<?php

namespace Flordelcampo\VendorApi\Api;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;

/**
 * Interface VendorManagementInterface
 * Flordelcampo\VendorApi\Api
 */
interface VendorManagementInterface
{
    /**
     * Authenticate Login
     *
     * @return ApiResponseInterface
     */
    public function authenticate();

    /**
     * Return vendor data
     *
     * @param integer $vendorId
     * @return mixed
     */
    public function getVendorData($vendorId);

    /**
     * @return ApiResponseInterface
     */
    public function updateVendorDetails();

    /**
     * Return Location List
     *
     * @param integer $vendorId
     *
     * @return ApiResponseInterface
     */
    public function getLocationsList($vendorId);

    /**
     * Return Location Details By ID
     *
     * @param integer $locationId
     *
     * @return ApiResponseInterface
     */
    public function getLocationById($locationId);

    /**
     * Update Location
     *
     * @return ApiResponseInterface
     */
    public function updateLocation();

    /**
     * Return All Logistics List
     * @param integer $vendorId
     * @param integer $locationId
     * @return ApiResponseInterface
     */
    public function getLogisticsList($vendorId, $locationId);

    /**
     * Return Logistic Data by Id
     * @param integer $logisticId
     * @return ApiResponseInterface
     */
    public function getLogisticById($logisticId);

    /**
     * Update Logistic Data
     * @return ApiResponseInterface
     */
    public function updateLogistic();

    /**
     * Return Order Count
     * @return ApiResponseInterface
     */
    public function getOrdersCount();

    /**
     * Return Order Data
     * @return ApiResponseInterface
     */
    public function getOrdersDetails();

    /**
     * Return UPS File Data
     * @return ApiResponseInterface
     */
    public function getUpsFileData();

    /**
     * Return UPS Track Id File Data
     * @return ApiResponseInterface
     */
    public function getTrackIdTemplate();

    /**
     * Upload Track ID
     * @return ApiResponseInterface
     */
    public function uploadTrackId();

    /**
     * Label URL
     * @return ApiResponseInterface
     */
    public function getLabelUrlForBoxes();

    /**
     * Vendor Item Wise Details
     * @return ApiResponseInterface
     */
    public function getOrderItemDetails();

    /**
     * Vendor Order Details For Finance Dashboard
     * @return ApiResponseInterface
     */
    public function getVendorOrderDetailsForReport();

    /**
     * Return All Shipping Method
     * @return ApiResponseInterface
     */
    public function getAllShippingMethods();

    /**
     * Vendor Order Details For Finance Dashboard By Website
     * @return ApiResponseInterface
     */
    public function getOrderDetailsByWebsite();

    /**
     * Return All Orders Revenue for Customer
     * @return ApiResponseInterface
     */
    public function getStatusWiseOrderDetailsByWebsite();

    /**
     * Add vendor Confirmed boxes
     * @return ApiResponseInterface
     */
    public function addVendorConfirmedBoxes();

}