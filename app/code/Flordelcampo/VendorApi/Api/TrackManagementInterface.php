<?php

namespace Flordelcampo\VendorApi\Api;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;

/**
 * Interface TrackManagementInterface
 * Flordelcampo\VendorApi\Api
 */
interface TrackManagementInterface
{
    /**
     * @return ApiResponseInterface
     */
    public function getTrackDashboardReport();
}