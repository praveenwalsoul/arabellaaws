<?php

namespace Flordelcampo\VendorApi\Api;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;

/**
 * Interface VendorManagementInterface
 * Flordelcampo\VendorApi\Api
 */
interface VendorInventoryManagementInterface
{
    /**
     * Get All Inventory
     * @return ApiResponseInterface
     */
    public function getAllInventory();

    /**
     * Get Inventory By Offer Id
     *
     * @param integer $offerId
     * @return ApiResponseInterface
     */
    public function getInventoryByOfferId($offerId);

    /**
     * Get Update Inventory
     * @return ApiResponseInterface
     */
    public function updateInventory();

    /**
     * Get Vendor Product By Vendor Id , Product Type
     * @return ApiResponseInterface
     */
    public function getVendorAssociateProduct();

    /**
     * Create Offer
     * @return ApiResponseInterface
     */
    public function createOffer();

    /**
     * Get SKU wise inventory data
     * @return ApiResponseInterface
     */
    public function getAllSkuWiseInventory();

    /**
     * Update Offer Qty
     * @return ApiResponseInterface
     */
    public function updateOfferQty();

    /**
     * Return Log Details
     * @return ApiResponseInterface
     */
    public function getLogDetails();

}