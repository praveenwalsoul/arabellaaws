<?php

namespace Flordelcampo\VendorApi\Model\Vendor;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\CalendarApi\Controller\BaseController;
use Flordelcampo\VendorApi\Api\VendorManagementInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;

date_default_timezone_set('America/New_York');

/**
 *
 * Class VendorManagement
 * Flordelcampo\VendorApi\Model\Vendor
 */
class VendorManagement extends BaseController implements VendorManagementInterface
{
    /**
     * Login Authentication
     *
     * @return ApiResponseInterface|void
     */
    public function authenticate()
    {
        $vendorData = $vendorTypes = $allShippingData = $allCostChannelData = $locationData = [];
        $userData = [];
        $isSubVendor = false;
        try {
            $inputParams = $this->request->getBodyParams();
            if (isset($inputParams['user_name']) && $inputParams['password']) {
                $vendorId = $this->vendorHelper->checkAuthentication(
                    $inputParams['user_name'],
                    $inputParams['password']
                );
                if (is_array($vendorId)) {
                    $isSubVendor = true;
                    $userEntityId = $vendorId['user_entity_id'];
                    $userData = $this->vendorHelper->getVendorUserApiDataById($userEntityId);
                    $locationId = $vendorId['location_id'];
                    $vendorId = $vendorId['vendor_id'];
                    $locationData = $this->vendorHelper->getVendorLocationApiData($locationId);
                } else {
                    if ($vendorId != null) {
                        $vendorLocationCollection = $this->vendorHelper->getVendorLocationsCollectionByVendorId($vendorId);
                        /* $vendorLocationCollection->addFieldToFilter('status', '1');*/
                        if ($vendorLocationCollection->getSize() > 0) {
                            foreach ($vendorLocationCollection as $location) {
                                $data = $this->vendorHelper->getVendorLocationApiData($location->getId());
                                if (count($data) > 0) {
                                    $locationData[] = $data;
                                }
                            }
                        }
                    }
                }
                $vendorData = $this->vendorHelper->getVendorLoginData($vendorId);
                $vendorTypes = $this->vendorHelper->getSelectedVendorTypesArray();
                $allShippingData = $this->vendorHelper->getAllShippingMethodsArray();
                $allCostChannelData = $this->vendorHelper->getAllCostChannelArray();
                $productTypesArray = $this->vendorHelper->getAllAttributeSetData();
                $response = [
                    'status' => true,
                    'is_sub_vendor' => $isSubVendor,
                    'vendor' => $vendorData,
                    'user_data' => $userData,
                    'locations' => $locationData,
                    'vendor_types' => $vendorTypes,
                    'shipping_methods' => $allShippingData,
                    'cost_channels' => $allCostChannelData,
                    'product_types' => $productTypesArray
                ];
            } else {
                throw new LocalizedException(__("Invalid username or password"));
            }
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Vendor Data by ID
     *
     * @param int $vendorId
     * @return mixed
     */
    public function getVendorData($vendorId)
    {
        try {
            $this->validateVendor($vendorId);
            $vendorData = $this->vendorHelper->getVendorLoginData($vendorId);
            $vendorLocationCollection = $this->vendorHelper->getVendorLocationsCollectionByVendorId($vendorId);
            $locationData = [];
            /* $vendorLocationCollection->addFieldToFilter('status', '1');*/
            if ($vendorLocationCollection->getSize() > 0) {
                foreach ($vendorLocationCollection as $location) {
                    $data = $this->vendorHelper->getVendorLocationApiData($location->getId());
                    if (count($data) > 0) {
                        $locationData[] = $data;
                    }
                }
            }
            $vendorTypes = $this->vendorHelper->getSelectedVendorTypesArray();
            $allShippingData = $this->vendorHelper->getAllShippingMethodsArray();
            $allCostChannelData = $this->vendorHelper->getAllCostChannelArray();
            $productTypesArray = $this->vendorHelper->getAllAttributeSetData();
            $response = [
                'status' => true,
                'vendor' => $vendorData,
                'locations' => $locationData,
                'vendor_types' => $vendorTypes,
                'shipping_methods' => $allShippingData,
                'cost_channels' => $allCostChannelData,
                'product_types' => $productTypesArray
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * @return ApiResponseInterface|void
     */
    public function updateVendorDetails()
    {
        try {
            $params = $this->request->getBodyParams();
            $response = [];
            $this->validateVendor($params['vendor_id']);
            $vendor = $this->vendorHelper->getVendorModelById($params['vendor_id']);
            if (isset($params['vendor_name'])) {
                $vendor->setData('vendor_name', $params['vendor_name']);
            }
            if (isset($params['company_name'])) {
                $vendor->setData('last_name', $params['company_name']);
            }
            if (isset($params['telephone'])) {
                $vendor->setData('telephone', $params['telephone']);
            }
            if (isset($params['email'])) {
                $vendor->setData('email', $params['email']);
            }
            if (isset($params['vendor_type'])) {
                $vendor->setData('vendor_type', $params['vendor_type']);
            }
            if (isset($params['status'])) {
                $vendor->setData('status', $params['status']);
            }
            if (isset($params['address']['street_1'])) {
                $vendor->setData('address', $params['address']['street_1']);
            }
            if (isset($params['address']['street_2'])) {
                $vendor->setData('street', $params['address']['street_2']);
            }
            if (isset($params['address']['city'])) {
                $vendor->setData('city', $params['address']['city']);
            }
            if (isset($params['address']['state'])) {
                $vendor->setData('region', $params['address']['state']);
            }
            if (isset($params['address']['country'])) {
                $vendor->setData('country_id', $params['address']['country']);
            }
            if (isset($params['address']['zip_code'])) {
                $vendor->setData('zipcode', $params['address']['zip_code']);
            }
            $vendor->save();
            return $this->getVendorData($params['vendor_id']);
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Location List
     *
     * @param int $vendorId
     * @return ApiResponseInterface|mixed
     */
    public function getLocationsList($vendorId)
    {
        try {
            $this->validateVendor($vendorId);
            $locationData = [];
            $vendorLocationCollection = $this->vendorHelper->getVendorLocationsCollectionByVendorId($vendorId);
            /* $vendorLocationCollection->addFieldToFilter('status', '1');*/
            if ($vendorLocationCollection->getSize() > 0) {
                foreach ($vendorLocationCollection as $location) {
                    $data = $this->vendorHelper->getVendorLocationApiData($location->getId());
                    if (count($data) > 0) {
                        $locationData[] = $data;
                    }
                }
            }
            $response = ['status' => true, 'location' => $locationData];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Vendor Location Data by ID
     *
     * @param int $locationId
     * @return ApiResponseInterface|mixed
     */
    public function getLocationById($locationId)
    {
        try {
            $this->validateVendorLocation($locationId);
            $locationData = $this->vendorHelper->getVendorLocationApiData($locationId);
            $response = ['status' => true, 'location' => $locationData];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Update Vendor Location
     *
     * @return ApiResponseInterface|mixed
     */
    public function updateLocation()
    {
        try {
            $payload = $this->request->getBodyParams();
            if (isset($payload['location_id'])) {
                $locationId = $payload['location_id'];
                $locationModel = $this->vendorHelper->getVendorLocationModelByLocationId($locationId);
                $this->validateVendorLocation($locationId);
                $locationItems = [];
                if (isset($payload['loc_name'])) {
                    $locationItems['loc_name'] = $payload['loc_name'];
                }
                if (isset($payload['loc_cust_disp_name'])) {
                    $locationItems['loc_cust_disp_name'] = $payload['loc_cust_disp_name'];
                }
                if (isset($payload['loc_phone'])) {
                    $locationItems['loc_phone'] = $payload['loc_phone'];
                }
                if (isset($payload['loc_email'])) {
                    $locationItems['loc_email'] = $payload['loc_email'];
                }
                if (isset($payload['status'])) {
                    $statusData = ($payload['status'] == '') ? '1' : $payload['status'];
                    $locationItems['status'] = $statusData;
                }
                if (isset($payload['address'])) {
                    if (isset($payload['address']['street'])) {
                        if (isset($payload['address']['street'][0])) {
                            $locationItems['loc_address'] = $payload['address']['street'][0];
                        }
                        if (isset($payload['address']['street'][1])) {
                            $locationItems['loc_street'] = $payload['address']['street'][1];
                        }
                    }
                    if (isset($payload['address']['loc_city'])) {
                        $locationItems['loc_city'] = $payload['address']['loc_city'];
                    }
                    if (isset($payload['address']['loc_state'])) {
                        $locationItems['loc_state'] = $payload['address']['loc_state'];
                    }
                    if (isset($payload['address']['loc_country'])) {
                        $locationItems['loc_country'] = $payload['address']['loc_country'];
                    }
                    if (isset($payload['address']['loc_postcode'])) {
                        $locationItems['loc_postcode'] = $payload['address']['loc_postcode'];
                    }
                    if (isset($payload['address']['loc_fax'])) {
                        $locationItems['loc_fax'] = $payload['address']['loc_fax'];
                    }
                }
                if (count($locationItems) > 0) {
                    foreach ($locationItems as $key => $value) {
                        if ($locationModel->hasData($key)) {
                            $locationModel->setData($key, $value);
                        }
                    }
                    $locationModel->save();
                }
                $locationData = $this->vendorHelper->getVendorLocationApiData($locationId);
                $response = [
                    'status' => true,
                    'message' => __('Location updated successfully'),
                    'location' => $locationData];
            } else {
                throw new LocalizedException(__("This vendor location doesn't exist"));
            }
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return All Logistics List
     * @param int $vendorId
     * @param int $locationId
     * @return ApiResponseInterface|mixed
     */
    public function getLogisticsList($vendorId, $locationId)
    {
        $logisticData = [];
        try {
            $this->validateVendor($vendorId);
            $this->validateVendorLocation($locationId);
            $logisticCollection = $this->vendorHelper->getVendorLogisticCollectionByVendorIdAndLocationId(
                $vendorId,
                $locationId
            );
            if ($logisticCollection->getSize() > 0) {
                foreach ($logisticCollection as $logistic) {
                    $data = $this->vendorHelper->getVendorLogisticApiData($logistic->getId());
                    if (count($data) > 0) {
                        $logisticData[] = $data;
                    }
                }
            }
            $response = ['status' => true, 'location' => $logisticData];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Logistic Data by ID
     * @param int $logisticId
     * @return ApiResponseInterface|mixed
     */
    public function getLogisticById($logisticId)
    {
        try {
            $this->validateLogistic($logisticId);
            $logisticData = $this->vendorHelper->getVendorLogisticApiData($logisticId);
            $response = ['status' => true, 'logistic' => $logisticData];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Update Logistic Data
     *
     * @return ApiResponseInterface|mixed
     */
    public function updateLogistic()
    {
        try {
            $payload = $this->request->getBodyParams();
            if (isset($payload['logistic_entity_id'])) {
                $logisticId = $payload['logistic_entity_id'];
                $this->validateLogistic($logisticId);
                $logistic = $this->vendorHelper->getVendorLogisticModelById($logisticId);
                foreach ($payload as $key => $value) {
                    if ($key != 'logistic_entity_id') {
                        if ($logistic->hasData($key)) {
                            $logistic->setData($key, $value);
                        }
                    }
                }
                $logistic->save();
                $logisticData = $this->vendorHelper->getVendorLogisticApiData($logisticId);
                $response = [
                    'status' => true,
                    'message' => __('Logistic updated successfully'),
                    'logistic' => $logisticData];
            } else {
                throw new LocalizedException(__("This vendor logistic doesn't exist"));
            }
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * @return ApiResponseInterface|void
     */
    public function getOrdersCount()
    {
        $response = $orderItemBoxCountData = [];
        try {
            $vendorId = $locationId = null;
            $params = $this->getRequest()->getParams();
            if (isset($params['vendor_id'])) {
                $this->validateVendor($params['vendor_id']);
                $vendorId = $params['vendor_id'];
            } else {
                throw new LocalizedException(__("Vendor Id is required"));
            }
            if (isset($params['location_id'])) {
                $this->validateVendorLocation($params['location_id']);
                $locationId = $params['location_id'];
            } else {
                throw new LocalizedException(__("Location Id is required"));
            }
            $fromDate = $toDate = $shippingMethodArray = null;
            $reportType = 'ship_date';
            if (isset($params['from_date']) && isset($params['to_date'])) {

                $fromDate = $params['from_date'];
                $toDate = $params['to_date'];
                $orderItemBoxCountData = $this->getOrderItemBoxCollection(
                    $vendorId,
                    $locationId,
                    $fromDate,
                    $toDate,
                    $shippingMethodArray,
                    $reportType
                );
            } else {
                /**
                 * Create 4 types of date array
                 */
                $today = date('Y-m-d');

                $previousWeek = date('Y-m-d', strtotime('-1 week', strtotime($today)));
                $previousMonth = date('Y-m-d', strtotime('-1 month', strtotime($today)));
                $previousYear = date('Y-m-d', strtotime('-1 year', strtotime($today)));
                $dateArray['today'] = ['from' => $today, 'to' => $today];
                $dateArray['last_week'] = ['from' => $previousWeek, 'to' => $today];
                $dateArray['last_month'] = ['from' => $previousMonth, 'to' => $today];
                $dateArray['last_year'] = ['from' => $previousYear, 'to' => $today];
                foreach ($dateArray as $type => $fromTo) {
                    $fromDate = $fromTo['from'];
                    $toDate = $fromTo['to'];
                    $orderItemBoxData = $this->getOrderItemBoxCollection(
                        $vendorId,
                        $locationId,
                        $fromDate,
                        $toDate,
                        $shippingMethodArray,
                        $reportType
                    );
                    $orderItemBoxCountData[$type] = $orderItemBoxData;
                }
            }
            $response = ['status' => true, 'order_count' => $orderItemBoxCountData];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Order Details by website wise
     * @return ApiResponseInterface|mixed
     */
    public function getOrderDetailsByWebsite()
    {
        $response = $orderItemBoxCountData = [];
        try {
            $vendorId = $locationId = null;
            $params = $this->getRequest()->getParams();
            /*echo '<pre>';
            print_r($params);
            die;*/
            if (isset($params['vendor_id'])) {
                $this->validateVendor($params['vendor_id']);
                $vendorId = $params['vendor_id'];
            }
            if (isset($params['location_id'])) {
                $this->validateVendorLocation($params['location_id']);
                $locationId = $params['location_id'];
            }
            $reportType = $params['report_type'];
            $websiteId = $params['website_id'];
            $websiteIdWithStoreIds = $this->orderHelper->getAllStoreIdsForWebsiteId($websiteId);
            $shippingMethodString = $params['shipping_method'];
            $shippingMethodArray = explode(",", $shippingMethodString);

            $actualFromDate = $params['from_date'];
            $actualToDate = $params['to_date'];
            if ($reportType == 'order_date') {
                $fromDate = $actualFromDate . " 00:00:00";
                $toDate = $actualToDate . " 23:59:59";
            } else {
                $reportType = 'order_date';
                $actualFromDate = date('Y-m-d', strtotime('-1 week', strtotime($actualFromDate)));
                $actualToDate = date('Y-m-d', strtotime('+1 week', strtotime($actualToDate)));
                $fromDate = $actualFromDate . " 00:00:00";
                $toDate = $actualToDate . " 23:59:59";
            }
            $finalWebsiteWiseItemData = $total = [];
            foreach ($websiteIdWithStoreIds as $websiteId => $storeIds) {
                $finalStoreWiseItemData = $total = [];
                $itemDataCollection = $this->vendorHelper->getOrderItemBoxWiseCountCollection(
                    $vendorId,
                    $locationId,
                    $fromDate,
                    $toDate,
                    null,
                    $shippingMethodArray,
                    $reportType
                );
                $itemDataCollection->addFieldToFilter('main_table.store_id', ['in' => $storeIds]);
                $total['order_count'] = $total['revenue'] = 0;
                $total['website_id'] = $websiteId;
                if ($itemDataCollection->getSize() > 0) {
                    /**
                     * Get All Item ids-Box Ids to fetch track Ids
                     */
                    $tempBoxIds = [];
                    foreach ($itemDataCollection as $item) {
                        $tempBoxIds[] = $item->getId() . '_1';
                    }
                    $boxCollection = $this->orderHelper->getBoxCollectionByIds($tempBoxIds);
                    $tempTrackIds = [];
                    foreach ($boxCollection as $oneTempBox) {
                        $itemId = $oneTempBox->getData('item_id');
                        $tempTrackIds[$itemId] = $oneTempBox->getData('track_id');
                    }
                    $allTrackIds = array_values($tempTrackIds);
                    $firstTrackStatusArray = $this->vendorHelper->getFirstTrackStatusDataByTrackId($allTrackIds);
                    $tempData = $storeWistItemData = $storeWiseOrderCount = $storeWiseRevenue = [];
                    $customerWiseCount = [];
                    foreach ($itemDataCollection as $item) {
                        $valid = 0;
                        $itemId = $item->getId();
                        $trackId = $tempTrackIds[$itemId];
                        if ($params['report_type'] == 'ship_date') {
                            $boxId = $item->getId() . '_1';
                            $trackId = $this->orderHelper->getBoxModelByBoxId($boxId)->getData('track_id');
                            $boxShipDate = $item->getData('pickup_date');
                            if (!empty($trackId)) {
                                $firstTrackStatusData = (isset ($firstTrackStatusArray[$trackId])) ? $firstTrackStatusArray[$trackId] : [];
                                if (count($firstTrackStatusData) > 0) {
                                    $firstStatus = $firstTrackStatusData['status'];
                                    if ($firstStatus == 'Order Processed: Ready for UPS'
                                        ||
                                        $firstStatus == 'Shipper created a label, UPS has not received the package yet.'
                                    ) {
                                        $firstTime = $firstTrackStatusData['time'];
                                        $boxShipDate = date('Y-m-d', strtotime($firstTime));
                                    }
                                }
                            }
                            if (strtotime($boxShipDate) >= strtotime($params['from_date'])
                                &&
                                strtotime($boxShipDate) <= strtotime($params['to_date'])
                            ) {
                                $valid = 1;
                            } else {
                                continue;
                            }
                        }
                        $storeId = $item->getData('store_id');
                        $orderId = $item->getData('order_id');
                        $itemId = $item->getData('item_id');
                        $order = $this->orderHelper->getOrderModelById($orderId);
                        $customerId = $order->getCustomerId();
                        $orderShippingAmount = $order->getShippingAmount();
                        $orderedTotalBoxes = $order->getTotalQtyOrdered();

                        $customer = $this->customerHelper->getCustomerById($customerId);
                        $customerName = $customer->getName();
                        $shippingAddress = $order->getShippingAddress()->toArray();
                        $customerArray['customer_id'] = $customerId;
                        $customerArray['customer_name'] = $customerName;
                        $itemsData = [
                            'item' => $item->toArray(),
                            'order_id' => $order->getIncrementId(),
                            'customer' => $customerArray,
                            'shipping' => $shippingAddress
                        ];
                        $boxCount = (int)$item->getData('box_count');
                        $price = $item->getData('price');
                        $shipPriceForBox = 0;
                        if ($orderShippingAmount > 0) {
                            $shipPriceForBox = $orderShippingAmount / $orderedTotalBoxes;
                        }
                        $actualItemPrice = round(($boxCount * $price), 2);
                        $shipPriceForItem = $boxCount * $shipPriceForBox;
                        $revenue = round($actualItemPrice + $shipPriceForItem, 2);
                        if (isset($storeWistItemData[$storeId])) {
                            $storeWistItemData[$storeId]['order_count'] += $boxCount;
                            $storeWistItemData[$storeId]['revenue'] += $revenue;
                        } else {
                            $storeWistItemData[$storeId]['store_id'] = $storeId;
                            $storeWistItemData[$storeId]['order_count'] = $boxCount;
                            $storeWistItemData[$storeId]['revenue'] = $revenue;
                        }
                        if (isset($customerWiseCount[$storeId][$customerId])) {
                            $customerWiseCount[$storeId][$customerId]['order_count'] += $boxCount;
                            $customerWiseCount[$storeId][$customerId]['revenue'] += $revenue;
                        } else {
                            $customerWiseCount[$storeId][$customerId]['customer_id'] = $customerId;
                            $customerWiseCount[$storeId][$customerId]['customer_name'] = $customerName;
                            $customerWiseCount[$storeId][$customerId]['order_count'] = $boxCount;
                            $customerWiseCount[$storeId][$customerId]['revenue'] = $revenue;
                        }
                        $total['order_count'] += $boxCount;
                        $total['revenue'] += $revenue;
                        $tempData[$storeId][$customerId][] = $itemsData;
                    }
                    foreach ($storeWistItemData as $storeId => $storeOrderCount) {
                        $orderCount = $storeOrderCount['order_count'];
                        $storeRevenue = $storeOrderCount['revenue'];
                        $storeRevenue = round($storeRevenue, 2);
                        $orderItem = $tempData[$storeId];
                        $tempItemsData = [];
                        foreach ($orderItem as $customerId => $items) {
                            $customerId = $customerWiseCount[$storeId][$customerId]['customer_id'];
                            $customerName = $customerWiseCount[$storeId][$customerId]['customer_name'];
                            $boxCount = $customerWiseCount[$storeId][$customerId]['order_count'];
                            $customerRevenue = $customerWiseCount[$storeId][$customerId]['revenue'];
                            $customerRevenue = round($customerRevenue, 2);
                            $tempCustomer['customer_id'] = $customerId;
                            $tempCustomer['customer_name'] = $customerName;
                            $tempCustomer['order_count'] = $boxCount;
                            $tempCustomer['revenue'] = $customerRevenue;
                            $tempCustomer['items'] = $items;
                            $tempItemsData[$customerId][] = $tempCustomer;
                        }
                        $customerWiseItems = [];
                        foreach ($tempItemsData as $cid => $customerItemsArray) {
                            foreach ($customerItemsArray as $oneItem) {
                                $customerWiseItems[] = $oneItem;
                            }
                        }
                        $finalStoreWiseItemData[] = [
                            'store_id' => $storeId,
                            'website_id' => $websiteId,
                            'order_count' => $orderCount,
                            'revenue' => $storeRevenue,
                            'order_items' => $customerWiseItems
                        ];

                    }
                }
                if (isset($total['revenue'])) {
                    $total['revenue'] = round($total['revenue'], 2);
                }
                $finalWebsiteWiseItemData[] = [
                    'website_id' => $websiteId,
                    'website_data' => $finalStoreWiseItemData,
                    'total' => $total
                ];
            }
            $response = [
                'status' => true,
                'website_wise_data' => $finalWebsiteWiseItemData
                /* 'total_data' => $total*/
            ];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Orders Data by Website-stores
     * @return ApiResponseInterface|mixed
     */
    public function getStatusWiseOrderDetailsByWebsite()
    {
        try {
            $boxStatusConditionArray = ['pending', 'processing', 'printed',
                'vendorPrint', 'shipped'];
            $vendorId = $locationId = null;
            $params = $this->getRequest()->getParams();
            if (isset($params['vendor_id'])) {
                $this->validateVendor($params['vendor_id']);
                $vendorId = $params['vendor_id'];
            }
            if (isset($params['location_id'])) {
                $this->validateVendorLocation($params['location_id']);
                $locationId = $params['location_id'];
            }
            $reportType = $params['report_type'];
            $websiteId = $params['website_id'];
            $websiteIdWithStoreIds = $this->orderHelper->getAllStoreIdsForWebsiteId($websiteId);
            $fromDate = $params['from_date'];
            $toDate = $params['to_date'];
            $shippingMethodString = $params['shipping_method'];
            $shippingMethodArray = explode(",", $shippingMethodString);
            if ($reportType == 'order_date') {
                $fromDate = $fromDate . " 00:00:00";
                $toDate = $toDate . " 23:59:59";
            }
            $finalWebsiteWiseItemData = $total = [];
            foreach ($websiteIdWithStoreIds as $websiteId => $storeIds) {
                $finalStoreWiseItemData = $total = [];
                $orderBoxItemDetails = $this->vendorHelper->getBoxWiseItemCollection(
                    $vendorId,
                    $locationId,
                    null,
                    null,
                    $boxStatusConditionArray
                );
                $shipMethodQry = "'" . implode("', '", $shippingMethodArray) . "'";
                $storeIdQryString = "'" . implode("', '", $storeIds) . "'";
                $orderBoxItemDetails->getSelect()->where(
                    "order_item.shipping_method IN ( " . $shipMethodQry . ") 
                    AND order_item.store_id IN ( " . $storeIdQryString . ")"
                );
                if ($fromDate != null && $toDate != null) {
                    if ($reportType != null && $reportType == 'order_date') {
                        $orderBoxItemDetails->getSelect()->where(
                            "order_item.created_at BETWEEN '" . $fromDate . "' AND '" . $toDate . "' "
                        );
                    } else {
                        $orderBoxItemDetails->getSelect()->where(
                            "order_item.pickup_date BETWEEN '" . $fromDate . "' AND '" . $toDate . "' "
                        );
                    }
                }
                $total['order_count'] = $total['revenue'] = 0;
                $total['website_id'] = $websiteId;
                if ($orderBoxItemDetails->getSize() > 0) {
                    $tempData = $storeWistItemData = $storeWiseOrderCount = $storeWiseRevenue = [];
                    $customerWiseCount = $customerWiseStatusCount = [];
                    foreach ($orderBoxItemDetails as $box) {
                        /*echo '<pre>';
                        print_r($box->getData());
                        die;*/
                        $storeId = $box->getData('store_id');
                        $orderId = $box->getData('order_id');
                        $itemId = $box->getData('item_id');
                        $boxStatus = $box->getData('status');
                        $order = $this->orderHelper->getOrderModelById($orderId);
                        $customerId = $order->getCustomerId();
                        $orderShippingAmount = $order->getShippingAmount();
                        $orderedTotalBoxes = $order->getTotalQtyOrdered();

                        $customer = $this->customerHelper->getCustomerById($customerId);
                        $customerName = $customer->getName();
                        $customerArray['customer_id'] = $customerId;
                        $customerArray['customer_name'] = $customerName;

                        $boxCount = 1;
                        $price = $box->getData('price');
                        $shipPriceForBox = 0;
                        if ($orderShippingAmount > 0) {
                            $shipPriceForBox = $orderShippingAmount / $orderedTotalBoxes;
                        }
                        $actualItemPrice = round(($boxCount * $price), 2);
                        $shipPriceForItem = $boxCount * $shipPriceForBox;
                        $revenue = round($actualItemPrice + $shipPriceForItem, 2);

                        if (isset($storeWistItemData[$storeId])) {
                            $storeWistItemData[$storeId]['order_count'] += $boxCount;
                            $storeWistItemData[$storeId]['revenue'] += $revenue;
                        } else {
                            $storeWistItemData[$storeId]['store_id'] = $storeId;
                            $storeWistItemData[$storeId]['order_count'] = $boxCount;
                            $storeWistItemData[$storeId]['revenue'] = $revenue;
                        }
                        if (isset($customerWiseCount[$storeId][$customerId])) {
                            $customerWiseCount[$storeId][$customerId]['order_count'] += $boxCount;
                            $customerWiseCount[$storeId][$customerId]['revenue'] += $revenue;
                        } else {
                            $customerWiseCount[$storeId][$customerId]['customer_id'] = $customerId;
                            $customerWiseCount[$storeId][$customerId]['customer_name'] = $customerName;
                            $customerWiseCount[$storeId][$customerId]['order_count'] = $boxCount;
                            $customerWiseCount[$storeId][$customerId]['revenue'] = $revenue;
                        }
                        if (isset($customerWiseStatusCount[$storeId][$customerId][$boxStatus])) {
                            $customerWiseStatusCount[$storeId][$customerId][$boxStatus]['order_count'] += $boxCount;
                            $customerWiseStatusCount[$storeId][$customerId][$boxStatus]['revenue'] += $revenue;
                        } else {
                            $customerWiseStatusCount[$storeId][$customerId][$boxStatus]['order_count'] = $boxCount;
                            $customerWiseStatusCount[$storeId][$customerId][$boxStatus]['revenue'] = $revenue;
                            $customerWiseStatusCount[$storeId][$customerId][$boxStatus]['status'] = $boxStatus;
                        }

                        $total['order_count'] += $boxCount;
                        $total['revenue'] += $revenue;
                    }
                    /**
                     * Initializing the status array
                     */
                    $tempStoreCustomerStatusData = [];
                    foreach ($customerWiseStatusCount as $storeId => $customerData) {
                        foreach ($customerData as $customerId => $statusData) {
                            $availableBoxStatus = array_keys($statusData);
                            foreach ($boxStatusConditionArray as $oneStatus) {
                                if (in_array($oneStatus, $availableBoxStatus) == false) {
                                    $tempStoreCustomerStatusData[$storeId][$customerId][$oneStatus]['order_count'] = 0;
                                    $tempStoreCustomerStatusData[$storeId][$customerId][$oneStatus]['revenue'] = 0;
                                    $tempStoreCustomerStatusData[$storeId][$customerId][$oneStatus]['status'] = $oneStatus;
                                } else {
                                    $tempStoreCustomerStatusData[$storeId][$customerId][$oneStatus]['order_count'] = $statusData[$oneStatus]['order_count'];
                                    $tempStoreCustomerStatusData[$storeId][$customerId][$oneStatus]['revenue'] = $statusData[$oneStatus]['revenue'];
                                    $tempStoreCustomerStatusData[$storeId][$customerId][$oneStatus]['status'] = $statusData[$oneStatus]['status'];
                                }
                            }

                        }
                    }
                    foreach ($storeWistItemData as $storeId => $storeOrderCount) {
                        $orderCount = $storeOrderCount['order_count'];
                        $storeRevenue = $storeOrderCount['revenue'];
                        $storeRevenue = round($storeRevenue, 2);
                        $orderItem = $tempStoreCustomerStatusData[$storeId];
                        $tempItemsData = [];

                        foreach ($orderItem as $customerId => $items) {
                            $customerId = $customerWiseCount[$storeId][$customerId]['customer_id'];
                            $customerName = $customerWiseCount[$storeId][$customerId]['customer_name'];
                            $boxCount = $customerWiseCount[$storeId][$customerId]['order_count'];
                            $customerRevenue = $customerWiseCount[$storeId][$customerId]['revenue'];
                            $customerRevenue = round($customerRevenue, 2);
                            $tempCustomer['customer_id'] = $customerId;
                            $tempCustomer['customer_name'] = $customerName;
                            $tempCustomer['order_count'] = $boxCount;
                            $tempCustomer['revenue'] = $customerRevenue;
                            $tempCustomer['items'] = $items;
                            $tempItemsData[$customerId][] = $tempCustomer;
                        }
                        $customerWiseItems = [];
                        foreach ($tempItemsData as $cid => $customerItemsArray) {
                            foreach ($customerItemsArray as $oneItem) {
                                $customerWiseItems[] = $oneItem;
                            }
                        }
                        $finalStoreWiseItemData[] = [
                            'store_id' => $storeId,
                            'website_id' => $websiteId,
                            'order_count' => $orderCount,
                            'revenue' => $storeRevenue,
                            'order_items' => $customerWiseItems
                        ];
                    }
                }
                if (isset($total['revenue'])) {
                    $total['revenue'] = round($total['revenue'], 2);
                }
                $finalWebsiteWiseItemData[] = [
                    'website_id' => $websiteId,
                    'website_data' => $finalStoreWiseItemData,
                    'total' => $total
                ];
            }
            $response = [
                'status' => true,
                'website_wise_data' => $finalWebsiteWiseItemData
                /* 'total_data' => $total*/
            ];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Finance Dashboard Order Details for vendor
     * @return ApiResponseInterface|mixed
     */
    public function getVendorOrderDetailsForReport()
    {
        try {
            $params = $this->getRequest()->getParams();
            $vendorId = $locationId = null;
            if (isset($params['vendor_id'])) {
                $this->validateVendor($params['vendor_id']);
                $vendorId = $params['vendor_id'];
            }
            if (isset($params['location_id'])) {
                $this->validateVendorLocation($params['location_id']);
                $locationId = $params['location_id'];
            }
            $reportType = $params['report_type'];
            $actualFromDate = $params['from_date'];
            $actualToDate = $params['to_date'];
            if ($reportType == 'order_date') {
                $fromDate = $actualFromDate . " 00:00:00";
                $toDate = $actualToDate . " 23:59:59";
            } else {
                $reportType = 'order_date';
                $actualFromDate = date('Y-m-d', strtotime('-1 week', strtotime($actualFromDate)));
                $actualToDate = date('Y-m-d', strtotime('+1 week', strtotime($actualToDate)));
                $fromDate = $actualFromDate . " 00:00:00";
                $toDate = $actualToDate . " 23:59:59";
            }
            $shippingMethodString = $params['shipping_method'];
            $shippingMethodArray = explode(",", $shippingMethodString);
            $itemDataCollection = $this->vendorHelper->getOrderItemBoxWiseCountCollection(
                $vendorId,
                $locationId,
                $fromDate,
                $toDate,
                null,
                $shippingMethodArray,
                $reportType
            );
            $reportData = $finalData = $vendorLocationItems = [];
            if ($itemDataCollection->getSize() > 0) {
                /**
                 * Get All Item ids-Box Ids to fetch track Ids
                 */
                $tempBoxIds = [];
                foreach ($itemDataCollection as $item) {
                    $tempBoxIds[] = $item->getId() . '_1';
                }
                $boxCollection = $this->orderHelper->getBoxCollectionByIds($tempBoxIds);
                $tempTrackIds = [];
                foreach ($boxCollection as $oneTempBox) {
                    $itemId = $oneTempBox->getData('item_id');
                    $tempTrackIds[$itemId] = $oneTempBox->getData('track_id');
                }
                $allTrackIds = array_values($tempTrackIds);
                $firstTrackStatusArray = $this->vendorHelper->getFirstTrackStatusDataByTrackId($allTrackIds);
                foreach ($itemDataCollection as $item) {
                    $itemId = $item->getId();
                    $valid = 0;
                    if ($params['report_type'] == 'ship_date') {
                        /* $boxId = $item->getId() . '_1';*/
                        $trackId = $tempTrackIds[$itemId];
                        $boxShipDate = $item->getData('pickup_date');
                        if (!empty($trackId)) {
                            $firstTrackStatusData = (isset ($firstTrackStatusArray[$trackId])) ? $firstTrackStatusArray[$trackId] : [];
                            if (count($firstTrackStatusData) > 0) {
                                $firstStatus = $firstTrackStatusData['status'];
                                if ($firstStatus == 'Order Processed: Ready for UPS'
                                    ||
                                    $firstStatus == 'Shipper created a label, UPS has not received the package yet.'
                                ) {
                                    $firstTime = $firstTrackStatusData['time'];
                                    $boxShipDate = date('Y-m-d', strtotime($firstTime));
                                }
                            }
                        }
                        if (strtotime($boxShipDate) >= strtotime($params['from_date'])
                            &&
                            strtotime($boxShipDate) <= strtotime($params['to_date'])
                        ) {
                            $valid = 1;
                        } else {
                            continue;
                        }
                    }
                    $vendorId = $item->getData('vendor');
                    $locationId = $item->getData('vendor_farm');
                    $boxCount = (int)$item['box_count'];
                    $price = $item['price'];
                    /**
                     * Items Details
                     */
                    $orderId = $item->getData('order_id');
                    $order = $this->orderHelper->getOrderModelById($orderId);
                    $orderShippingAmount = $order->getShippingAmount();
                    $orderedTotalBoxes = $order->getTotalQtyOrdered();
                    $shipPriceForBox = 0;
                    if ($orderShippingAmount > 0) {
                        $shipPriceForBox = $orderShippingAmount / $orderedTotalBoxes;
                    }
                    $actualItemPrice = round(($boxCount * $price), 2);
                    $shipPriceForItem = $boxCount * $shipPriceForBox;
                    $revenue = round($actualItemPrice + $shipPriceForItem, 2);

                    $shippingAddress = $order->getShippingAddress()->toArray();
                    $itemsData = [
                        'item' => $item->toArray(),
                        'order_id' => $order->getIncrementId(),
                        'shipping' => $shippingAddress
                    ];
                    $vendorLocationItems[$vendorId][$locationId][] = $itemsData;

                    if (isset($reportData[$vendorId][$locationId])) {
                        $reportData[$vendorId][$locationId]['boxes_shipped'] += $boxCount;
                        $reportData[$vendorId][$locationId]['revenue'] += $revenue;
                    } else {
                        $reportData[$vendorId][$locationId]['vendor_name'] = $item['vendor_name'];
                        $reportData[$vendorId][$locationId]['location_name'] = $item['location_name'];
                        $reportData[$vendorId][$locationId]['boxes_shipped'] = $boxCount;
                        $reportData[$vendorId][$locationId]['revenue'] = $revenue;
                    }
                }
                $finalData = [];
                if (count($reportData) > 0) {
                    foreach ($reportData as $vendorId => $locationArray) {
                        foreach ($locationArray as $locationId => $orderData) {
                            $temp = [];
                            $temp['vendor_id'] = $vendorId;
                            $temp['location_id'] = $locationId;
                            foreach ($orderData as $key => $val) {
                                $temp[$key] = $val;
                            }
                            $temp['items_data'] = $vendorLocationItems[$vendorId][$locationId];
                            $finalData[] = $temp;
                        }
                    }
                }

            }
            $response = ['status' => true, 'order_data' => $finalData];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * @param $vendorId
     * @param $locationId
     * @param $fromDate
     * @param $toDate
     * @param $shippingMethod
     * @param $reportType
     * @return array
     */
    public function getOrderItemBoxCollection(
        $vendorId,
        $locationId,
        $fromDate,
        $toDate,
        $shippingMethod,
        $reportType
    )
    {
        $orderCountData = [];
        $boxDataCollection = $this->vendorHelper->getOrderItemBoxWiseCountCollection(
            $vendorId,
            $locationId,
            $fromDate,
            $toDate,
            null,
            $shippingMethod,
            $reportType
        );
        if ($boxDataCollection->getSize() > 0) {
            $tempAllOrderIds = [];
            foreach ($boxDataCollection as $item) {
                $tempAllOrderIds[] = $item->getData('order_id');
            }
            $orderIds = array_unique($tempAllOrderIds);
            $orderCollection = $this->orderHelper->getOrderCollectionByIds($orderIds);
            $orderPriceArray = [];
            if ($orderCollection->getSize() > 0) {
                foreach ($orderCollection as $oneOrder) {
                    $orderId = $oneOrder->getId();
                    $shippingAmount = $oneOrder->getData('shipping_amount');
                    $totalQtyOrdered = $oneOrder->getData('total_qty_ordered');
                    $orderPriceArray[$orderId]['total_qty'] = $totalQtyOrdered;
                    $orderPriceArray[$orderId]['shipping_amount'] = $shippingAmount;
                }
            }
            foreach ($boxDataCollection as $item) {
                $orderId = $item->getData('order_id');
                $boxCount = $item['box_count'];
                $price = $item['price'];

                /**
                 *
                 * Actual revenue with shipping amount
                 */
                $orderShippingAmount = (isset($orderPriceArray[$orderId]['shipping_amount']))
                    ? $orderPriceArray[$orderId]['shipping_amount'] : 0;
                $orderedTotalBoxes = (isset($orderPriceArray[$orderId]['total_qty']))
                    ? $orderPriceArray[$orderId]['shipping_amount'] : 1;
                $shipPriceForBox = 0;
                if ($orderShippingAmount > 0) {
                    $shipPriceForBox = $orderShippingAmount / $orderedTotalBoxes;
                }
                $actualItemPrice = round(($boxCount * $price), 2);
                $shipPriceForItem = $boxCount * $shipPriceForBox;
                $revenue = round($actualItemPrice + $shipPriceForItem, 2);

                /**
                 * order count
                 */
                if (isset($orderCountData['count'])) {
                    $orderCountData['count'] += (int)$boxCount;
                } else {
                    $orderCountData['count'] = (int)$boxCount;
                }
                /**
                 * Revenue
                 */
                if (isset($orderCountData['revenue'])) {
                    $orderCountData['revenue'] += $revenue;
                } else {
                    $orderCountData['revenue'] = $revenue;
                }
            }
        } else {
            $orderCountData['count'] = 0;
            $orderCountData['revenue'] = 0;
        }
        return $orderCountData;
    }

    /**
     * @return ApiResponseInterface|void
     */
    public function getOrdersDetails()
    {
        try {
            $params = $this->getRequest()->getParams();
            if (isset($params['vendor_id'])) {
                $this->validateVendor($params['vendor_id']);
            }
            if (isset($params['location_id'])) {
                $this->validateVendorLocation($params['location_id']);
            }
            $vendorId = (isset($params['vendor_id'])) ? $params['vendor_id'] : null;
            $locationId = (isset($params['loc_id'])) ? $params['loc_id'] : null;
            if (isset($params['from_date']) && isset($params['to_date'])) {
                $fromDate = $params['from_date'];
                $toDate = $params['to_date'];
            } else {
                $today = date('Y-m-d');
                $fromDate = date('Y-m-d', strtotime('-1 days', strtotime($today)));
                $toDate = date('Y-m-d', strtotime('+3 days', strtotime($today)));
            }
            $orderBoxItemDetails = $this->vendorHelper->getBoxWiseItemCollection(
                $vendorId,
                $locationId,
                $fromDate,
                $toDate
            );
            $orderBoxItemDetails->getSelect()
                ->joinLeft(
                    ['vendor' => 'vendor_registrations'],
                    "order_item.vendor=vendor.vendor_id",
                    ['vendor_name' => 'vendor.vendor_name']
                )->joinLeft(
                    ['location' => 'dropship_vendor_locations'],
                    "order_item.vendor_farm = location.loc_id",
                    ['location_name' => 'location.loc_name']
                );
            /* echo $orderBoxItemDetails->getSelect();
             die;*/
            $boxWiseStatusData = $boxIdGiftMessage = [];
            $finalResponse = [];
            $allStatusArray = ['new_order', 'un_printed', 'printed', 'total',
                'fedex_pickup_confirm', 'fedex_sent', 'fedex_ground', 'fedex_air'];
            if ($orderBoxItemDetails->getSize() > 0) {
                /**
                 * Get track details
                 */
                $tempAllTrackIds = $tempAllOfferIds = [];
                foreach ($orderBoxItemDetails as $box) {
                    if (!empty($box->getData('track_id'))) {
                        $tempAllTrackIds[] = $box->getData('track_id');
                    }
                    if (!empty($box->getData('offer_avail_id'))) {
                        $tempAllOfferIds[] = $box->getData('offer_avail_id');
                    }
                }
                $offersCollection = $this->vendorHelper->getVendorInventoryOfferById($tempAllOfferIds);
                $offerIdInventoryArray = [];
                if ($offersCollection->getSize() > 0) {
                    foreach ($offersCollection as $oneOffer) {
                        $oneOfferId = $oneOffer->getData('offer_id');
                        $offerIdInventoryArray[$oneOfferId] = $oneOffer->getData('inventory_qty');
                    }
                }
                $count = 0;
                foreach ($orderBoxItemDetails as $oneBox) {
                    $boxId = $oneBox['box_id'];
                    $offerId = $oneBox['offer_avail_id'];
                    $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
                    $currentStatus = $oneBox['status'];
                    $trackId = $oneBox['track_id'];
                    $sku = $oneBox['sku'];
                    $productName = $oneBox['name'];
                    $vendorId = $oneBox['vendor'];
                    $vendorName = $oneBox['vendor_name'];
                    $locationName = $oneBox['location_name'];
                    $giftMessage = $oneBox['gift_message'];
                    $shippingMethod = $oneBox['shipping_method'];

                    $boxWiseStatusData[$vendorId][$offerId][$sku]['sku'] = $sku;
                    $boxWiseStatusData[$vendorId][$offerId][$sku]['offer_id'] = $offerId;
                    $boxWiseStatusData[$vendorId][$offerId][$sku]['product_name'] = $productName;
                    $boxWiseStatusData[$vendorId][$offerId][$sku]['vendor_id'] = $vendorId;
                    $boxWiseStatusData[$vendorId][$offerId][$sku]['vendor_name'] = $vendorName;
                    $boxWiseStatusData[$vendorId][$offerId][$sku]['location_name'] = $locationName;
                    $invOnHand = isset($offerIdInventoryArray[$offerId]) ? $offerIdInventoryArray[$offerId] : '0';
                    $boxWiseStatusData[$vendorId][$offerId][$sku]['inventory_on_hand'] = $invOnHand;
                    $boxIdGiftMessage[$boxId] = $giftMessage;
                    $lastTrackStatusData = (isset($latestTrackStatusArray[$trackId])) ?
                        $latestTrackStatusArray[$trackId] : [];
                    if (isset($lastTrackStatusData['status'])) {
                        $latestTrackStatus = $lastTrackStatusData['status'];
                    } else {
                        $latestTrackStatus = '';
                    }
                    if ($currentStatus == 'pending' || $currentStatus === 'processing') {
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['new_order']['count'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['new_order']['count'] += 1;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['new_order']['count'] = 1;
                        }

                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['new_order']['box_ids'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['new_order']['box_ids'] .= "," . $boxId;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['new_order']['box_ids'] = $boxId;
                        }
                    }
                    if ($currentStatus == 'printed') {
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['un_printed']['count'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['un_printed']['count'] += 1;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['un_printed']['count'] = 1;
                        }

                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['un_printed']['box_ids'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['un_printed']['box_ids'] .= "," . $boxId;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['un_printed']['box_ids'] = $boxId;
                        }
                    }
                    if ($currentStatus == 'vendorPrint') {
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['count'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['count'] += 1;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['count'] = 1;
                        }
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['box_ids'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['box_ids'] .= "," . $boxId;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['box_ids'] = $boxId;
                        }
                    }

                    /**
                     * Box status should be printed , vendor print, shipped , invoice
                     * Order Processed: Ready for UPS
                     */
                    if (($latestTrackStatus == 'Shipping information sent to FedEx' ||
                            $latestTrackStatus == 'Order Processed: Ready for UPS' ||
                            $latestTrackStatus == 'Shipper created a label, UPS has not received the package yet.')
                        &&
                        ($currentStatus != 'pending' || $currentStatus != 'processing'
                        )
                    ) {
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_sent']['count'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_sent']['count'] += 1;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_sent']['count'] = 1;
                        }
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_sent']['box_ids'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_sent']['box_ids'] .= "," . $boxId;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_sent']['box_ids'] = $boxId;
                        }
                    }

                    if (($shippingMethod == 'FEDEX_GROUND' || $shippingMethod == 'UPS Ground')
                        &&
                        (($currentStatus != 'pending' || $currentStatus != 'processing')
                            && $currentStatus != 'invoiced')
                    ) {
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_ground']['count'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_ground']['count'] += 1;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_ground']['count'] = 1;
                        }
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_ground']['box_ids'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_ground']['box_ids'] .= "," . $boxId;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_ground']['box_ids'] = $boxId;
                        }
                    }

                    if (($shippingMethod == 'STANDARD_OVERNIGHT' ||
                            $shippingMethod == 'PRIORITY_OVERNIGHT' ||
                            $shippingMethod == 'UPS Next Day Air') &&
                        (($currentStatus != 'pending' || $currentStatus != 'processing')
                            && $currentStatus != 'invoiced')
                    ) {
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_air']['count'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_air']['count'] += 1;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_air']['count'] = 1;
                        }
                        if (isset($boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_air']['box_ids'])) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_air']['box_ids'] .= "," . $boxId;
                        } else {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_air']['box_ids'] = $boxId;
                        }
                    }
                }
            }

            /*echo '<pre>';
            print_r($boxWiseStatusData);
            die;*/
            /**
             * Initialize the array with 0 for all status
             */
            if (count($boxWiseStatusData) > 0) {
                foreach ($allStatusArray as $status) {
                    foreach ($boxWiseStatusData as $vendorId => $offerData) {
                        foreach ($offerData as $offerId => $boxData) {
                            foreach ($boxData as $sku => $skuData) {
                                if (!isset($skuData[$status])) {
                                    $boxWiseStatusData[$vendorId][$offerId][$sku][$status]['count'] = 0;
                                    $boxWiseStatusData[$vendorId][$offerId][$sku][$status]['box_ids'] = '';
                                }
                            }
                        }
                    }
                }

                foreach ($boxWiseStatusData as $vendorId => $offerData) {
                    foreach ($offerData as $offerId => $skuData) {
                        foreach ($skuData as $sku => $boxData) {
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['total']['count'] = $boxData['printed']['count'] + $boxData['un_printed']['count'];
                            $totalBoxes = '';
                            if ($boxWiseStatusData[$vendorId][$offerId][$sku]['un_printed']['box_ids'] != '') {
                                $totalBoxes = $boxWiseStatusData[$vendorId][$offerId][$sku]['un_printed']['box_ids'];
                            }
                            if ($boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['box_ids'] != '') {
                                if ($totalBoxes != '') {
                                    $totalBoxes .= "," . $boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['box_ids'];
                                } else {
                                    $totalBoxes = $boxWiseStatusData[$vendorId][$offerId][$sku]['printed']['box_ids'];
                                }
                            }
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['total']['box_ids'] = $totalBoxes;
                            $fedexPick = $boxWiseStatusData[$vendorId][$offerId][$sku]['total']['count'] - $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_sent']['count'];
                            if ($fedexPick < 0) {
                                $fedexPick = 0;
                            }
                            $boxWiseStatusData[$vendorId][$offerId][$sku]['fedex_pickup_confirm']['count'] = $fedexPick;
                        }
                    }
                }
                foreach ($boxWiseStatusData as $vendorId => $offerData) {
                    foreach ($offerData as $offerId => $boxData) {
                        foreach ($boxData as $sku => $skuData) {
                            $finalResponse[] = $skuData;
                        }
                    }
                }

            }
            $response = ['status' => true, 'order_detail' => $finalResponse];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return UPS file data
     * @return ApiResponseInterface|void
     */
    public function getUpsFileData()
    {
        try {
            $params = $this->request->getParams();
            $upsFileHeaders = $this->upsTemplates->getUpsFileHeaders();
            $boxCollection = $this->vendorHelper->getBoxWiseItemAndOrderCollection(
                $params['vendor_id'],
                $params['location_id'],
                $params['box_status']
            );
            $fileData['headers'] = $upsFileHeaders;
            $upsBoxWiseData = [];
            if ($boxCollection->getSize() > 0) {
                foreach ($boxCollection as $box) {
                    try {
                        $sku = $box->getData('sku');
                        $orderId = $box->getData('order_id');
                        $orderCollection = $this->orderHelper->getOrderCollectionById($orderId);
                        if ($orderCollection->getSize() > 0) {
                            foreach ($orderCollection as $order) {
                                $partnerId = $order->getData('partner_order_id');
                                $shippingAddress = $order->getShippingAddress();
                                $countryId = $shippingAddress->getCountryId();
                                $billingAddress = $order->getBillingAddress();
                                $billingName = $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName();
                                $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
                                $shippingCity = $shippingAddress->getCity();
                                $shippingStreet = $shippingAddress->getStreet();
                                $shippingPostCode = $shippingAddress->getPostcode();
                                $shippingTelephone = $shippingAddress->getTelephone();
                                $shippingState = $shippingAddress->getRegionCode();
                                $shippingAddressLine1 = (isset($shippingStreet[0])) ? $shippingStreet[0] : '';
                                $shippingAddressLine2 = (isset($shippingStreet[1])) ? $shippingStreet[1] : '';
                                $shippingAddressLine3 = (isset($shippingStreet[2])) ? $shippingStreet[2] : '';
                                $postCodeArray = explode("-", $shippingPostCode);
                                if (count($postCodeArray) > 1) {
                                    $postCode = $shippingPostCode;
                                } else {
                                    $postCode = $postCodeArray[0];
                                    if (strlen($postCode) < 5) {
                                        $padLength = 5 - strlen($postCode);
                                        $postCode = str_pad($postCode, 5, "0", STR_PAD_LEFT);
                                        $postCode = "=" . $postCode;
                                    }
                                }
                                /**
                                 * Product
                                 */
                                $product = $this->catalogHelper->getProductBySku($sku);
                                $productName = $product->getName();
                                $boxLength = $product->getBoxLength();
                                $boxWidth = $product->getBoxWidth();
                                $boxHeight = $product->getBoxHeight();
                                $boxWeight = $product->getBoxWeight();
                                $poPrice = $product->getPoPrice();
                                $group = $product->getGroup();
                                $attr = $product->getResource()->getAttribute('group');
                                $groupName = '';
                                if ($attr->usesSource()) {
                                    $groupName = $attr->getSource()->getOptionText($group);
                                }

                                $giftMessage = $box->getData('gift_message');
                                $giftMessage = (trim($giftMessage) == '') ?
                                    $billingName : $giftMessage;

                                $giftMessage = str_replace("?", "", $giftMessage);

                                $temp = [
                                    $shippingName,
                                    $shippingName,
                                    $shippingAddressLine1,
                                    $shippingAddressLine2,
                                    $shippingAddressLine3,
                                    $shippingCity,
                                    $shippingState,
                                    $postCode,
                                    $countryId,
                                    $shippingTelephone,
                                    $giftMessage,
                                    $partnerId,
                                    $productName,
                                    '1',
                                    $productName,
                                    'Fresh Flowers',
                                    $boxLength,
                                    $boxWidth,
                                    $boxHeight,
                                    $boxWeight,
                                    $poPrice,
                                    'ES',
                                    'CP',
                                    $groupName,
                                    'USD',
                                    'CO',
                                    'Box',
                                    'Isha Flowers',
                                    'Zach Hemple',
                                    '4862 Cadiz Circle',
                                    'Palm Beach Gardens',
                                    'FL',
                                    'US',
                                    '33418',
                                    '8562819187',
                                    '85086E'
                                ];
                                $upsBoxWiseData[] = array_combine(array_values($upsFileHeaders), $temp);
                            }
                        } else {
                            continue;
                        }

                    } catch (\Exception $e) {
                        continue;
                    }
                }

                $fileData['ups_data'] = $upsBoxWiseData;
            }
            $response = [
                'status' => true,
                'file_headers' => $upsFileHeaders,
                'file_data' => $upsBoxWiseData
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * @return ApiResponseInterface|void
     */
    public function getTrackIdTemplate()
    {
        try {
            $params = $this->request->getParams();
            $headers = $this->upsTemplates->getTrackIdTemplateHeaders();
            $boxCollection = $this->vendorHelper->getBoxWiseItemAndOrderCollection(
                $params['vendor_id'],
                $params['location_id'],
                $params['box_status']
            );
            $fileData['headers'] = $headers;
            $trackIdData = [];
            if ($boxCollection->getSize() > 0) {
                foreach ($boxCollection as $box) {
                    try {
                        $boxId = $box->getId();
                        $sku = $box->getData('sku');
                        $productName = $box->getData('name');
                        $trackId = $box->getData('track_id');
                        $orderId = $box->getData('order_id');
                        $status = $box->getData('status');
                        $orderCollection = $this->orderHelper->getOrderCollectionById($orderId);
                        if ($orderCollection->getSize() > 0) {
                            foreach ($orderCollection as $order) {
                                $partnerId = $order->getData('partner_order_id');
                                $orderNumber = $order->getData('increment_id');
                                $shippingAddress = $order->getShippingAddress();
                                $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
                                $temp = [
                                    $boxId,
                                    $sku,
                                    $productName,
                                    $trackId,
                                    $shippingName,
                                    $orderNumber,
                                    $partnerId,
                                ];
                                $trackIdData[] = array_combine(array_values($headers), $temp);
                            }
                        }
                    } catch (\Exception $e) {
                        continue;
                    }
                }

            }
            $response = [
                'status' => true,
                'file_headers' => $headers,
                'file_data' => $trackIdData
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * @return ApiResponseInterface|void
     */
    public function uploadTrackId()
    {
        $response = [];
        try {
            $params = $this->request->getBodyParams();
            foreach ($params as $param) {
                try {
                    $boxId = $param['box_id'];
                    $trackId = $param['track_id'];
                    $boxModel = $this->orderHelper->getBoxModelByBoxId($boxId);
                    if ($boxModel != null) {
                        $boxModel->setTrackId($trackId)
                            ->setStatus('shipped')
                            ->save();
                        $this->orderHelper->syncStatusByBoxId($boxId);
                    }
                } catch (\Exception $e) {
                    continue;
                }
            }

            $response = ['status' => true, 'message' => __('Track Id updated successfully')];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * @return ApiResponseInterface|void|mixed
     */
    public function getLabelUrlForBoxes()
    {
        $boxLabelArray = [];
        try {
            $params = $this->request->getParams();
            if (isset($params['box_id'])) {
                $boxCollection = $this->vendorHelper->getBoxWiseItemAndOrderCollection(
                    null,
                    null,
                    null,
                    $params['box_id']
                );
                if ($boxCollection->getSize() > 0) {
                    $tempAllOrderIds = $tempAllSku = [];
                    foreach ($boxCollection as $oneBox) {
                        $tempAllOrderIds[] = $oneBox->getData('order_id');
                        $tempAllSku[] = $oneBox->getData('sku');
                    }
                    $orderIds = array_unique($tempAllOrderIds);
                    $allSkus = array_unique($tempAllSku);
                    /**
                     * Hard Good SKu
                     */
                    $productInstructionArray = $hgProductDataArray = [];
                    $productCollection = $this->catalogHelper->getProductCollectionBySku($allSkus);
                    if ($productCollection->getSize() > 0) {
                        foreach ($productCollection as $oneProduct) {
                            $oneSku = $oneProduct->getSku();
                            $hgSkuArray = $this->vendorHelper->getHardGoodMappedProductSkuArrayForSku($oneSku);
                            if (count($hgSkuArray) > 0) {
                                $hgProductsCol = $this->catalogHelper->getProductCollectionBySku($hgSkuArray);
                                if ($hgProductsCol->getSize() > 0) {
                                    foreach ($hgProductsCol as $oneHg) {
                                        $hgProductDataArray[$oneSku][] = [
                                            'hard_good_sku' => $oneHg->getSku(),
                                            'name' => $oneHg->getName(),
                                            'qty' => $oneHg->getQty(),
                                        ];
                                    }
                                }
                            }
                            $instruction = $oneProduct->getData('instructions');
                            $productInstructionArray[$oneSku] = $instruction;
                        }
                    }
                    $orderCollection = $this->orderHelper->getOrderCollectionByIds($orderIds);
                    $orderGiftMessage = $orderPartnerIds = [];
                    if ($orderCollection->getSize() > 0) {
                        foreach ($orderCollection as $oneOrder) {
                            $orderId = $oneOrder->getId();
                            $orderPartnerIds[$orderId] = $oneOrder->getData('partner_order_id');
                            $billingAddress = $oneOrder->getBillingAddress();
                            $billingName = $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName();
                            $giftMessage = $billingName;
                            $giftMessage = str_replace("?", "", $giftMessage);
                            $orderGiftMessage[$orderId] = $giftMessage;
                        }
                    }
                    foreach ($boxCollection as $box) {
                        try {
                            $giftMessage = $box->getData('gift_message');
                            $orderId = $box->getData('order_id');
                            $sku = $box->getData('sku');
                            /**
                             * Get Hard Good Products for SKU
                             */
                            $hgProductsData = [];
                            if (isset($hgProductDataArray[$sku])) {
                                $hgProductsData = $hgProductDataArray[$sku];
                            }
                            if ($giftMessage == null) {
                                if (isset($orderGiftMessage[$orderId])) {
                                    $giftMessage = $orderGiftMessage[$orderId];
                                    $giftMessage = str_replace("?", "", $giftMessage);
                                }
                            }
                            $partnerOrderId = $instructions = null;
                            if (isset($orderPartnerIds[$orderId])) {
                                $partnerOrderId = $orderPartnerIds[$orderId];
                            }
                            if (isset($productInstructionArray[$sku])) {
                                $instructions = $productInstructionArray[$sku];
                            }
                            $boxLabelArray[] = [
                                'box_id' => $box->getId(),
                                'label_url' => $box->getData('ImageUrl'),
                                'gift_message' => $giftMessage,
                                'partner_order_id' => $partnerOrderId,
                                'product_name' => $box->getData('name'),
                                'sku' => $sku,
                                'ship_method' => $box->getData('shipping_method'),
                                'instruction' => $instructions,
                                'hard_good_products' => $hgProductsData
                            ];
                        } catch (\Exception $e) {
                            continue;
                        }
                    }
                }
            } else {
                throw new LocalizedException(__("Box Id is required"));
            }
            $response = ['status' => true, 'box_details' => $boxLabelArray];

        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Item wise Details
     * @return ApiResponseInterface|void
     */
    public function getOrderItemDetails()
    {
        try {
            $params = $this->getRequest()->getParams();
            if (isset($params['vendor_id']) && isset($params['location_id'])) {
                $itemCollection = $this->vendorHelper->getVendorOrderItemCollection(
                    $params['vendor_id'],
                    $params['location_id'],
                    null
                );
                $itemCollection->setOrder('item_id', 'DESC');
                $orderWiseItems = [];
                if ($itemCollection->getSize() > 0) {
                    $mediaUrl = $this->orderHelper->getPrescriptionMediaUrl();
                    foreach ($itemCollection as $item) {
                        try {
                            $itemsData = $orders = $address = $orderPrescription = $itemPrescription = [];
                            $orderId = $item->getData('order_id');
                            $order = $this->orderHelper->getOrderModelById($orderId);
                            $orderPrescriptionId = $order->getPrescriptionId();
                            if ($orderPrescriptionId != '') {
                                $prescription = $this->orderHelper->getPrescriptionModelById($orderPrescriptionId);
                                $orderPrescriptionUrl = $mediaUrl . $prescription->getData('filename');
                                $orderPrescription['prescription_id'] = $orderPrescriptionId;
                                $orderPrescription['prescription_url'] = $orderPrescriptionUrl;
                                $orderPrescription['type'] = $prescription->getData('type');
                                $orderPrescription['uploaded_date'] = $prescription->getData('created_at');
                                $orderPrescription['uploaded_by'] = $prescription->getData('uploaded_by');
                                $orderPrescription['username'] = $prescription->getData('username');
                            }
                            $orders = [
                                'order_number' => $order->getIncrementId(),
                                'status' => $order->getStatus(),
                                'prescriptions' => $orderPrescription,
                                'order_date' => $order->getCreatedAt(),
                                'subtotal' => $order->getSubtotal(),
                                'discount' => $order->getDiscountAmount(),
                                'shipping_amount' => $order->getShippingAmount(),
                                'grand_total' => $order->getGrandTotal()
                            ];
                            $addressId = $item->getData('address_id');
                            $items = $item->getData();
                            if ($addressId != null) {
                                try {
                                    $address = $this->addressRepository->getById($addressId);
                                    $ageObj = $address->getCustomAttribute('age');
                                    $sexObj = $address->getCustomAttribute('sex');
                                    $items['age'] = ($ageObj != null) ? $ageObj->getValue() : null;
                                    $items['sex'] = ($sexObj != null) ? $sexObj->getValue() : null;
                                    $items['patient_name'] = $address->getFirstname();
                                } catch (\Exception $e) {
                                    $items['age'] = $items['patient_name'] = $items['sex'] = null;
                                }
                            } else {
                                $items['age'] = $items['patient_name'] = $items['sex'] = null;
                            }
                            /**
                             * Item prescription Data
                             */

                            $temp = [];
                            if (trim($item->getData('prescription_id')) != '') {
                                $prescriptionIdArray = explode(",", $item->getData('prescription_id'));
                                foreach ($prescriptionIdArray as $prescriptionId) {
                                    $prescription = $this->orderHelper->getPrescriptionModelById($prescriptionId);
                                    $orderPrescriptionUrl = $mediaUrl . $prescription->getData('filename');
                                    $temp['prescription_id'] = $prescriptionId;
                                    $temp['prescription_url'] = $orderPrescriptionUrl;
                                    $temp['type'] = $prescription->getData('type');
                                    $temp['uploaded_date'] = $prescription->getData('created_at');
                                    $temp['uploaded_by'] = $prescription->getData('uploaded_by');
                                    $temp['username'] = $prescription->getData('username');
                                    $itemPrescription[] = $temp;
                                }
                            }

                            $items['prescriptions'] = $itemPrescription;
                            /**
                             * Vendor Type Label
                             */
                            $vendorTypeId = $items['vendor_type'];
                            $vendorTypeLabel = $this->vendorHelper->getVendorTypeNameById($vendorTypeId);
                            $items['vendor_type_label'] = $vendorTypeLabel;

                            $billingAddress = $order->getBillingAddress()->getData();
                            $streetAr = explode("\n", $billingAddress['street']);
                            $billingAddress['street'] = $streetAr;
                            $shippingAddress = $order->getShippingAddress()->getData();
                            $streetAr = explode("\n", $shippingAddress['street']);
                            $shippingAddress['street'] = $streetAr;
                            $itemsData = [
                                'items' => $items,
                                'order' => $orders,
                                'billing_address' => $billingAddress,
                                'shipping_address' => $shippingAddress
                            ];
                            $orderWiseItems[$orderId][] = $itemsData;
                        } catch (\Exception $e) {
                            continue;
                        }
                    }
                }
            } else {
                throw new LocalizedException(__("Vendor Id and Location Id is required"));
            }
            $response = ['status' => true, 'orders' => $orderWiseItems];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Validate Vendor
     *
     * @param $vendorId
     * @return bool
     * @throws LocalizedException
     */
    public function validateVendor($vendorId)
    {
        $vendor = $this->vendorHelper->getVendorModelById($vendorId);
        if ($vendor->getId() == null) {
            throw new LocalizedException(__("This vendor doesn't exist"));
        }
        return true;
    }

    /**
     * Validate Vendor Location
     *
     * @param $locationId
     * @return bool
     * @throws LocalizedException
     */
    public function validateVendorLocation($locationId)
    {
        $location = $this->vendorHelper->getVendorLocationModelByLocationId($locationId);
        if ($location->getId() == null) {
            throw new LocalizedException(__("This vendor location doesn't exist"));
        }
        return true;
    }

    /**
     * Validate Logistic
     *
     * @param $logisticId
     * @return bool
     * @throws LocalizedException
     */
    public function validateLogistic($logisticId)
    {
        $logistic = $this->vendorHelper->getVendorLogisticModelById($logisticId);
        if ($logistic->getId() == null) {
            throw new LocalizedException(__("This vendor logistic doesn't exist"));
        }
        return true;
    }

    /**
     * Return All Shipping Methods
     * @return mixed
     */
    public function getAllShippingMethods()
    {
        try {
            $shippingMethods = $this->vendorHelper->getAllShippingMethodsArray();
            $shipMethods = [];
            foreach ($shippingMethods as $id => $name) {
                $shipMethods[] = [
                    'id' => $id,
                    'name' => $name,
                ];
            }
            $response = ['status' => true, 'methods' => $shipMethods];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Add Vendor Confirm Boxes
     * @return ApiResponseInterface|mixed
     */
    public function addVendorConfirmedBoxes()
    {
        try {
            $params = $this->request->getBodyParams();
            $vendorConfirm = $this->vendorHelper->getVendorConfirmBoxesModel();
            foreach ($params as $key => $value) {
                $vendorConfirm->setData($key, $value);
            }
            $vendorConfirm->save();
            $response = ['status' => true, 'message' => __('Added successfully')];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    public function execute()
    {
        /*$response = [];
        try {
            // throw new LocalizedException(__("Invalid username or password"));
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;*/
    }
}