<?php

namespace Flordelcampo\VendorApi\Model\Vendor;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\Api\Controller\BaseController;
use Flordelcampo\VendorApi\Api\TrackManagementInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class TrackManagement
 * Flordelcampo\VendorApi\Model\Vendor
 */
class TrackManagement extends BaseController implements TrackManagementInterface
{
    /**
     * @var null
     */
    private $_params = null;

    /**
     * @return ApiResponseInterface|mixed
     */
    public function getTrackDashboardReport()
    {
        try {
            $params = $this->getRequest()->getParams();
            $this->_params = $params;
            $data = [];
            if ($params['report_type'] == 'shipping_agreement') {
                $data = $this->getShippingAgreementData();
            } elseif ($params['report_type'] == 'promise_delivery') {
                $data = $this->getPromiseDeliveryData();
            }
            $response = ['status' => true, 'report_data' => $data];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * @return array
     */
    public function getShippingAgreementData()
    {
        $params = $this->_params;
        $fromDate = $params['from_date'];
        $toDate = $params['to_date'];
        $boxCollection = $this->vendorHelper->getBoxWiseItemCollection();
        $boxCollection->getSelect()->where(
            "order_item.created_at BETWEEN '" . $fromDate . "' AND '" . $toDate . "' "
        );
        $boxCollection->getSelect()
            ->joinLeft(
                ['vendor' => 'vendor_registrations'],
                "order_item.vendor=vendor.vendor_id",
                ['vendor_name' => 'vendor.vendor_name']
            )->joinLeft(
                ['location' => 'dropship_vendor_locations'],
                "order_item.vendor_farm = location.loc_id",
                ['location_name' => 'location.loc_name']
            );
        if ($params['ship_type'] == 'ndd') {
            $nddShipMethods = $this->getNDDShippingMethods();
            $shipMethodQry = "'" . implode("', '", $nddShipMethods) . "'";
            $boxCollection->getSelect()->where(
                "order_item.shipping_method IN ( " . $shipMethodQry . ") "
            );
        }
        if ($params['ship_type'] == '2dd') {
            $twoDdShipMethods = $this->get2DShippingMethods();
            $shipMethodQry = "'" . implode("', '", $twoDdShipMethods) . "'";
            $boxCollection->getSelect()->where(
                "order_item.shipping_method IN ( " . $shipMethodQry . ") "
            );
        }
        $tempResponse = $tempBoxIds = $vendorNames = [];
        if ($boxCollection->getSize() > 0) {
            /**
             * Get track details
             */
            $tempAllTrackIds = [];
            foreach ($boxCollection as $box) {
                if (!empty($box->getData('track_id'))) {
                    $tempAllTrackIds[] = $box->getData('track_id');
                }
            }
            $firstTrackStatusArray = $this->vendorHelper->getFirstTrackStatusDataByTrackId($tempAllTrackIds);
            $latestTrackStatusArray = $this->vendorHelper->getLatestTrackStatusByTrackId($tempAllTrackIds);
            foreach ($boxCollection as $oneBox) {
                $boxId = $oneBox->getData('box_id');
                $boxStatus = $oneBox->getData('status');
                $trackId = $oneBox->getData('track_id');
                $vendorId = $oneBox->getData('vendor');
                $vendorName = $oneBox->getData('vendor_name');
                $vendorNames[$vendorId] = $vendorName;
                $lastTrackStatusData = $firstTrackStatusData = [];
                $currentStatus = 'not_delivered';
                if ($trackId != '') {
                    $lastTrackStatusData = (isset ($latestTrackStatusArray[$trackId])) ? $latestTrackStatusArray[$trackId] : [];
                    $firstTrackStatusData = (isset ($firstTrackStatusArray[$trackId])) ? $firstTrackStatusArray[$trackId] : [];
                }
                if (count($lastTrackStatusData) > 0) {
                    $firstStatus = $firstTrackStatusData['status'];
                    $calculatedShipDate = $calculatedDeliveryDate = null;
                    if ($firstStatus == 'Order Processed: Ready for UPS'
                        ||
                        $firstStatus == 'Shipper created a label, UPS has not received the package yet.'
                    ) {
                        $firstTime = $firstTrackStatusData['time'];
                        $calculatedShipDate = date('Y-m-d', strtotime($firstTime));
                    }
                    if ($calculatedShipDate != null) {
                        $day = date('l', strtotime($calculatedShipDate));
                        $additionDayVariable = "+1 days";
                        if ($params['ship_type'] == 'ndd') {
                            switch ($day) {
                                case 'Sunday':
                                case 'Friday':
                                case 'Saturday':
                                    $additionDayVariable = "+3 days";
                                    break;
                            }
                        }
                        if ($params['ship_type'] == '2dd') {
                            $additionDayVariable = "+2 days";
                            switch ($day) {
                                case 'Thursday':
                                    $additionDayVariable = "+4 days";
                                    break;
                                case 'Friday':
                                case 'Saturday':
                                    $additionDayVariable = "+3 days";
                                    break;
                            }
                        }
                        $calculatedDeliveryDate = date('Y-m-d', strtotime($additionDayVariable, strtotime($calculatedShipDate)));
                    }
                    $lastStatus = $lastTrackStatusData['status'];
                    $lastTime = $lastTrackStatusData['time'];
                    $lastStatusDate = date('Y-m-d', strtotime($lastTime));
                    if ($lastStatus == 'Order Processed: Ready for UPS'
                        ||
                        $lastStatus == 'Shipper created a label, UPS has not received the package yet.'
                    ) {
                        $currentStatus = 'not_pick_up';
                    } elseif ($lastStatus == 'Delivered') {
                        if (strtotime($lastStatusDate) < strtotime($calculatedDeliveryDate)) {
                            $currentStatus = 'before_dd';
                        }
                        if (strtotime($lastStatusDate) == strtotime($calculatedDeliveryDate)) {
                            $currentStatus = 'on_time_dd';
                        }
                        if (strtotime($lastStatusDate) > strtotime($calculatedDeliveryDate)) {
                            $currentStatus = 'after_dd';
                        }
                    }
                } else {
                    if ($boxStatus == 'invoiced' || $boxStatus == 'shipped') {
                        $currentStatus = 'on_time_dd';
                    }
                }
                if (isset($tempResponse[$vendorId][$currentStatus])) {
                    $prevBoxIds = $tempResponse[$vendorId][$currentStatus]['box_ids'];
                    $tempResponse[$vendorId][$currentStatus]['box_ids'] = $prevBoxIds . ',' . $boxId;
                    $tempResponse[$vendorId][$currentStatus]['count'] += 1;
                } else {
                    $tempResponse[$vendorId][$currentStatus]['box_ids'] = $boxId;
                    $tempResponse[$vendorId][$currentStatus]['count'] = 1;
                }
            }
        }
        /**
         * Initializing with null
         */
        $allResponseCodes = $this->getAllResultStatusCodes();
        foreach ($tempResponse as $vendorId => $trackData) {
            $tmpTrackData = [];
            $availableCodes = array_keys($trackData);
            foreach ($allResponseCodes as $oneCode) {
                if (in_array($oneCode, $availableCodes) == false) {
                    $tempResponse[$vendorId][$oneCode]['box_ids'] = null;
                    $tempResponse[$vendorId][$oneCode]['count'] = 0;
                }
            }
        }
        $finalResponse = [];
        foreach ($tempResponse as $vendorId => $trackData) {
            $vendorName = $vendorNames[$vendorId];
            $tmp = [];
            $tmp['vendor_name'] = $vendorName;
            $tmpBoxData = [];
            foreach ($trackData as $trackStatusCode => $boxData) {
                $boxIds = $boxData['box_ids'];
                $boxIdArray = explode(",", $boxIds);
                $boxRecords = $this->getBoxRecordsToDownloadByBoxId($boxIdArray);
                $tmpBoxData[$trackStatusCode]['box_ids'] = $boxIds;
                $tmpBoxData[$trackStatusCode]['count'] = $boxData['count'];
                $tmpBoxData[$trackStatusCode]['box_data'] = $boxRecords;
            }
            $tmp['track_data'][] = $tmpBoxData;
            $finalResponse[] = $tmp;
        }
        return $finalResponse;
    }

    /**
     * @return array
     */
    public function getPromiseDeliveryData()
    {
        $params = $this->_params;
        $fromDate = $params['from_date'];
        $toDate = $params['to_date'];
        $boxCollection = $this->vendorHelper->getBoxWiseItemCollection();
        $boxCollection->getSelect()->where(
            "order_item.created_at BETWEEN '" . $fromDate . "' AND '" . $toDate . "' "
        );
        $boxCollection->getSelect()
            ->joinLeft(
                ['vendor' => 'vendor_registrations'],
                "order_item.vendor=vendor.vendor_id",
                ['vendor_name' => 'vendor.vendor_name']
            )->joinLeft(
                ['location' => 'dropship_vendor_locations'],
                "order_item.vendor_farm = location.loc_id",
                ['location_name' => 'location.loc_name']
            );
        if ($params['ship_type'] == 'premium') {
            $premiumShipMethods = $this->getPremiumShippingMethods();
            $shipMethodQry = "'" . implode("', '", $premiumShipMethods) . "'";
            $boxCollection->getSelect()->where(
                "order_item.promise_ship_method IN ( " . $shipMethodQry . ") "
            );
        }
        if ($params['ship_type'] == 'normal') {
            $premiumShipMethods = $this->getPremiumShippingMethods();
            $shipMethodQry = "'" . implode("', '", $premiumShipMethods) . "'";
            $boxCollection->getSelect()->where(
                "order_item.promise_ship_method NOT IN ( " . $shipMethodQry . ") "
            );
        }
        $tempResponse = $vendorNames = [];
        if ($boxCollection->getSize() > 0) {
            /**
             * Get track details
             */
            $tempAllTrackIds = [];
            foreach ($boxCollection as $box) {
                if (!empty($box->getData('track_id'))) {
                    $tempAllTrackIds[] = $box->getData('track_id');
                }
            }
            $latestTrackStatusArray = $this->vendorHelper->getLatestTrackStatusByTrackId($tempAllTrackIds);
            foreach ($boxCollection as $oneBox) {
                $boxId = $oneBox->getData('box_id');
                $boxStatus = $oneBox->getData('status');
                $trackId = $oneBox->getData('track_id');
                $vendorId = $oneBox->getData('vendor');
                $vendorName = $oneBox->getData('vendor_name');
                $vendorNames[$vendorId] = $vendorName;
                $promiseDeliveryDate = $oneBox->getData('promise_delivery_date');
                $deliveryDate = ($promiseDeliveryDate != '') ? $promiseDeliveryDate : $oneBox->getData('delivery_date');
                $lastTrackStatusData = [];
                if ($trackId != '') {
                    $lastTrackStatusData = (isset ($latestTrackStatusArray[$trackId])) ? $latestTrackStatusArray[$trackId] : [];
                }
                $currentStatus = 'not_delivered';
                if (count($lastTrackStatusData) > 0) {
                    $lastStatus = $lastTrackStatusData['status'];
                    $lastTime = $lastTrackStatusData['time'];
                    $lastStatusDate = date('Y-m-d', strtotime($lastTime));
                    if ($lastStatus == 'Order Processed: Ready for UPS'
                        ||
                        $lastStatus == 'Shipper created a label, UPS has not received the package yet.'
                    ) {
                        $currentStatus = 'not_pick_up';
                    } elseif ($lastStatus == 'Delivered') {
                        if (strtotime($lastStatusDate) < strtotime($deliveryDate)) {
                            $currentStatus = 'before_dd';
                        }
                        if (strtotime($lastStatusDate) == strtotime($deliveryDate)) {
                            $currentStatus = 'on_time_dd';
                        }
                        if (strtotime($lastStatusDate) > strtotime($deliveryDate)) {
                            $currentStatus = 'after_dd';
                        }
                    }
                } else {
                    if ($boxStatus == 'invoiced' || $boxStatus == 'shipped') {
                        $currentStatus = 'on_time_dd';
                    }
                }
                if (isset($tempResponse[$vendorId][$currentStatus])) {
                    $prevBoxIds = $tempResponse[$vendorId][$currentStatus]['box_ids'];
                    $tempResponse[$vendorId][$currentStatus]['box_ids'] = $prevBoxIds . ',' . $boxId;
                    $tempResponse[$vendorId][$currentStatus]['count'] += 1;
                } else {
                    $tempResponse[$vendorId][$currentStatus]['box_ids'] = $boxId;
                    $tempResponse[$vendorId][$currentStatus]['count'] = 1;
                }
            }
        }
        /**
         * Initializing with null
         */
        $allResponseCodes = $this->getAllResultStatusCodes();
        foreach ($tempResponse as $vendorId => $trackData) {
            $tmpTrackData = [];
            $availableCodes = array_keys($trackData);
            foreach ($allResponseCodes as $oneCode) {
                if (in_array($oneCode, $availableCodes) == false) {
                    $tempResponse[$vendorId][$oneCode]['box_ids'] = null;
                    $tempResponse[$vendorId][$oneCode]['count'] = 0;
                }
            }
        }
        $finalResponse = [];

        foreach ($tempResponse as $vendorId => $trackData) {
            $vendorName = $vendorNames[$vendorId];
            $tmp = [];
            $tmp['vendor_name'] = $vendorName;
            $tmpBoxData = [];
            foreach ($trackData as $trackStatusCode => $boxData) {
                $boxIds = $boxData['box_ids'];
                $boxIdArray = explode(",", $boxIds);
                $boxRecords = $this->getBoxRecordsToDownloadByBoxId($boxIdArray);
                $tmpBoxData[$trackStatusCode]['box_ids'] = $boxIds;
                $tmpBoxData[$trackStatusCode]['count'] = $boxData['count'];
                $tmpBoxData[$trackStatusCode]['box_data'] = $boxRecords;
            }
            $tmp['track_data'][] = $tmpBoxData;
            $finalResponse[] = $tmp;
        }
        return $finalResponse;
    }

    public function getAllResultStatusCodes()
    {
        return [
            'not_pick_up',
            'before_dd',
            'on_time_dd',
            'after_dd',
            'not_delivered',
        ];
    }

    /**
     * @param $boxId
     * @return array
     * @return array
     */
    public function getBoxRecordsToDownloadByBoxId($boxId)
    {
        $boxCollection = $this->vendorHelper->getBoxWiseItemAndOrderCollection(
            null,
            null,
            null,
            $boxId
        );
        $boxCollection->getSelect()
            ->joinLeft(
                ['vendor' => 'vendor_registrations'],
                "order_item.vendor=vendor.vendor_id",
                ['vendor_name' => 'vendor.vendor_name']
            )->joinLeft(
                ['location' => 'dropship_vendor_locations'],
                "order_item.vendor_farm = location.loc_id",
                ['location_name' => 'location.loc_name']
            );
        $boxCollection->getSelect()
            ->joinLeft(
                ['ord' => 'sales_order'],
                "main_table.order_id=ord.entity_id",
                ['partner_order_id' => 'ord.partner_order_id']
            );
        $finalData = [];
        if ($boxCollection->getSize() > 0) {
            /**
             * Get track details
             */
            $tempAllTrackIds = [];
            foreach ($boxCollection as $box) {
                if (!empty($box->getData('track_id'))) {
                    $tempAllTrackIds[] = $box->getData('track_id');
                }
            }
            $firstTrackStatusArray = $this->vendorHelper->getFirstTrackStatusDataByTrackId($tempAllTrackIds);
            $latestTrackStatusArray = $this->vendorHelper->getLatestTrackStatusByTrackId($tempAllTrackIds);
            foreach ($boxCollection as $oneBox) {
                $trackId = $oneBox->getData('track_id');
                $lastTrackStatusData = (isset ($latestTrackStatusArray[$trackId])) ? $latestTrackStatusArray[$trackId] : [];
                $firstTrackStatusData = (isset ($firstTrackStatusArray[$trackId])) ? $firstTrackStatusArray[$trackId] : [];
                $orderProcessShipDate = $lastTrackStatus = $lastStatusDate = null;
                if (count($firstTrackStatusData) > 0) {
                    $firstStatus = $firstTrackStatusData['status'];
                    if ($firstStatus == 'Order Processed: Ready for UPS'
                        ||
                        $firstStatus == 'Shipper created a label, UPS has not received the package yet.'
                    ) {
                        $firstTime = $firstTrackStatusData['time'];
                        $orderProcessShipDate = date('Y-m-d', strtotime($firstTime));
                    }
                }
                if (count($lastTrackStatusData) > 0) {
                    $lastTrackStatus = $lastTrackStatusData['status'];
                    $lastTime = $lastTrackStatusData['time'];
                    $lastStatusDate = date('Y-m-d', strtotime($lastTime));
                }
                $promiseDeliveryDate = $oneBox->getData('promise_delivery_date');
                $deliveryDate = ($promiseDeliveryDate != '') ? $promiseDeliveryDate : $oneBox->getData('delivery_date');
                $finalData[] = [
                    'box_id' => $oneBox->getData('box_id'),
                    'partner_order_id' => $oneBox->getData('partner_order_id'),
                    'sku' => $oneBox->getData('sku'),
                    'track_id' => $oneBox->getData('track_id'),
                    'name' => $oneBox->getData('name'),
                    'price' => $oneBox->getData('price'),
                    'vendor_name' => $oneBox->getData('vendor_name'),
                    'location_name' => $oneBox->getData('location_name'),
                    'ship_date' => $orderProcessShipDate,
                    'latest_track_status' => $lastTrackStatus,
                    'latest_track_status_date' => $lastStatusDate,
                    'pickup_date' => $oneBox->getData('pickup_date'),
                    'promise_ship_method' => $oneBox->getData('promise_ship_method'),
                    'promise_delivery_date' => $deliveryDate,

                ];
            }
        }
        return $finalData;
    }

    /**
     * @return string[]
     */
    public function getPremiumShippingMethods()
    {
        return [
            'Two Day',
            'SecondDay',
            'NextDay'
        ];
    }

    /**
     * @return string[]
     */
    public function getNDDShippingMethods()
    {
        return [
            'UPS Ground',
            'UPS Next Day Air',
            'UPS Next Day Air Saver'
        ];
    }

    /**
     * @return string[]
     */
    public function get2DShippingMethods()
    {
        return [
            'UPS Worldwide 2',
            'UPS Worldwide 3',
            'UPS 2nd Day Air'
        ];
    }

    public function execute()
    {
        /*$response = [];
        try {
            // throw new LocalizedException(__("Invalid username or password"));
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;*/
    }
}