<?php

namespace Flordelcampo\VendorApi\Model\Vendor;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\CalendarApi\Controller\BaseController;
use Flordelcampo\VendorApi\Api\VendorInventoryManagementInterface;
use Flordelcampo\VendorApi\Api\VendorManagementInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Inventory
 * Flordelcampo\VendorApi\Model\Vendor
 */
class Inventory extends BaseController implements VendorInventoryManagementInterface
{
    const VENDOR_TYPE_DC = 'flordel_vendor_configuration/general/vendor_type_dc';
    const VENDOR_TYPE_FARM = 'flordel_vendor_configuration/general/vendor_type_farm';

    /**
     * Return All Inventory Data
     * @return ApiResponseInterface|mixed
     */
    public function getAllInventory()
    {
        $offerData = [];
        try {
            $params = $this->request->getParams();
            $hardGoodAttributeId = $this->vendorHelper->getHardGoodAttributeId();
            if (isset($params['vendor_id'])) {
                $this->validateVendor($params['vendor_id']);
            }
            $vendorId = (isset($params['vendor_id'])) ? $params['vendor_id'] : null;
            $locationId = (isset($params['loc_id'])) ? $params['loc_id'] : null;
            $offerCollection = $this->vendorHelper->getVendorInventoryOfferCollection($params);
            $offerCollection->getSelect()
                ->joinLeft(
                    ['vendor' => 'vendor_registrations'],
                    "main_table.vendor_id=vendor.vendor_id",
                    ['vendor_name' => 'vendor.vendor_name']
                )->joinLeft(
                    ['location' => 'dropship_vendor_locations'],
                    "main_table.loc_id = location.loc_id",
                    ['location_name' => 'location.loc_name']
                );
            if ($offerCollection->getSize() > 0) {
                foreach ($offerCollection as $offer) {
                    $offerId = $offer->getData('offer_id');
                    $productTypeId = $offer->getData('product_type_id');
                    /**
                     * check if offer is hard good or not
                     * if yes then get shipped count from hard good item table
                     */
                    if ($hardGoodAttributeId == $productTypeId) {
                        $boxCountForOffer = $this->vendorHelper->getHardGoodOrderCountByOfferId(
                            $offerId,
                            $vendorId,
                            $locationId
                        );
                    } else {
                        $boxCountForOffer = $this->vendorHelper->getOrderCountByOfferId(
                            $offerId,
                            $vendorId,
                            $locationId
                        );
                    }
                    $qtyShippedForOfferId = 0;
                    if (isset($boxCountForOffer[$offerId])) {
                        $qtyShippedForOfferId = $boxCountForOffer[$offerId];
                    }
                    $offerData[] = [
                        'offer_id' => $offerId,
                        'vendor_type' => $offer->getData('vendor_type'),
                        'vendor_id' => $offer->getData('vendor_id'),
                        'loc_id' => $offer->getData('loc_id'),
                        'sku' => $offer->getData('sku'),
                        'name' => $offer->getData('name'),
                        'qty_uploaded' => $offer->getData('qty_uploaded'),
                        'qty_confirmed' => $offer->getData('qty_confirmed'),
                        'inventory_qty' => $offer->getData('inventory_qty'),
                        'eod_inventory' => $offer->getData('eod_inventory'),
                        'qty_shipped' => $qtyShippedForOfferId,
                        'start_date' => $offer->getData('start_date'),
                        'expiry_date' => $offer->getData('expiry_date'),
                        'status' => $offer->getData('status'),
                        'vendor_name' => $offer->getData('vendor_name'),
                        'location_name' => $offer->getData('location_name')
                    ];
                }
            }
            $response = [
                'status' => true,
                'inventory_data' => $offerData
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return SKU wise Offers by summing up qty
     * @return ApiResponseInterface|mixed
     */
    public function getAllSkuWiseInventory()
    {
        try {
            $params = $this->request->getParams();
            $params['status'] = 'both';
            $fromDate = $params['from_date'];
            $toDate = $params['to_date'];
            $offerCollection = $this->vendorHelper->getVendorInventoryOfferCollection($params);
            $finalOfferData = [];
            if ($offerCollection->getSize() > 0) {
                $skuArray = $tempData = [];
                foreach ($offerCollection as $offer) {
                    $offerStartDate = $offer->getData('start_date');
                    $offerExpiryDate = $offer->getData('expiry_date');
                    /**
                     * Check if from date between offer start date and end date
                     * or To date between offer start date and end date
                     */
                    if ((strtotime($fromDate) >= strtotime($offerStartDate)
                            &&
                            strtotime($fromDate) <= strtotime($offerExpiryDate))
                        || (strtotime($toDate) >= strtotime($offerStartDate)
                            &&
                            strtotime($toDate) <= strtotime($offerExpiryDate))
                    ) {
                        $offerId = $offer->getData('offer_id');
                        $sku = $offer->getData('sku');
                        $skuArray[] = $sku;
                        $inventoryQty = $offer->getData('inventory_qty');
                        $nodeInventory = $offer->getData('node_inventory');
                        if (isset($tempData[$sku])) {
                            $tempData[$sku]['inventory_qty'] += $inventoryQty;
                            $tempData[$sku]['node_inventory'] += $nodeInventory;
                        } else {
                            $tempData[$sku]['sku'] = $sku;
                            $tempData[$sku]['inventory_qty'] = $inventoryQty;
                            $tempData[$sku]['node_inventory'] = $nodeInventory;
                        }
                    }
                }
                $skuArray = array_unique($skuArray);
                $orderCountSkuWiseArray = $this->vendorHelper->getOrderCountBySku($skuArray);

                foreach ($tempData as $sku => $invData) {
                    $qtyOrdered = 0;
                    if (isset($orderCountSkuWiseArray[$sku])) {
                        $qtyOrdered = $orderCountSkuWiseArray[$sku];
                    }
                    $invData['qty_ordered'] = $qtyOrdered;
                    $finalOfferData[] = $invData;
                }
            }
            $response = [
                'status' => true,
                'offer_data' => $finalOfferData
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Inventory Offer
     * @param int $offerId
     * @return ApiResponseInterface|mixed
     */
    public function getInventoryByOfferId($offerId)
    {
        try {
            $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
            if ($offer->getId() != null) {
                $response = [
                    'status' => true,
                    'inventory_data' => $offer->toArray()
                ];
            } else {
                throw new LocalizedException(__("This vendor offer doesn't exist"));
            }
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Offer Update
     *
     * @return ApiResponseInterface|mixed
     */
    public function updateInventory()
    {
        try {
            $params = $this->request->getBodyParams();
            $offer = $this->vendorHelper->getVendorInventoryOfferById($params['offer_id']);
            if ($offer->getId() != null) {
                /**
                 * Log Update before save
                 */
                $previousQtyConfirmed = $offer->getData('qty_confirmed');
                $comment = [];
                if (isset($params['qty_confirmed']) && $previousQtyConfirmed != $params['qty_confirmed']) {
                    $comment[] = 'Previous Qty Confirmed = ' . $offer->getData('qty_confirmed');
                    $comment[] = 'Updated Qty Confirmed = ' . $params['qty_confirmed'];
                }
                $previousEodInventory = $offer->getData('eod_inventory');
                if (isset($params['eod_inventory']) && $previousEodInventory != $params['eod_inventory']) {
                    $comment[] = 'Previous EOD Inventory = ' . $offer->getData('eod_inventory');
                    $comment[] = 'Updated EOD Inventory = ' . $params['eod_inventory'];
                }

                if (count($comment) > 0) {
                    $log = $this->vendorHelper->getInventoryLogModel();
                    $log->setData('offer_id', $params['offer_id']);
                    $log->setData('comment', implode('<br>', $comment));
                    $log->setData('reason', 'Updated through API');
                    $log->setData('user_name', $params['vendor_name']);
                    $log->save();
                }
                if (isset($params['qty_confirmed'])) {
                    $offer->setData('inventory_qty', $params['qty_confirmed']);
                    $offer->setData('qty_confirmed', $params['qty_confirmed']);
                }
                if (isset($params['eod_inventory'])) {
                    $offer->setData('inventory_qty', $params['eod_inventory']);
                    $offer->setData('eod_inventory', '0');
                }
                $offer->save();
                $response = [
                    'status' => true,
                    'message' => __('Offer updated successfully'),
                    'inventory_data' => $offer->toArray()
                ];
            } else {
                throw new LocalizedException(__("This vendor offer doesn't exist"));
            }
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Discard or Add qty
     * @return ApiResponseInterface|mixed
     */
    public function updateOfferQty()
    {
        $response = [];
        try {
            $params = $this->request->getBodyParams();
            /*echo '<pre>';
            print_r($params);
            die;*/
            $updateType = $params['update_type'];
            $qtyToUpdate = $params['qty'];
            $user = 'Api';
            if (isset($params['vendor_name'])) {
                $user = $params['vendor_name'];
            }
            $reason = 'Updated through API';
            if (isset($params['reason'])) {
                $reason = $params['reason'];
            }
            $offer = $this->vendorHelper->getVendorInventoryOfferById($params['offer_id']);
            if ($offer->getId() != null) {
                $currentInventory = $offer->getData('inventory_qty');
                $log = $this->vendorHelper->getInventoryLogModel();
                $log->setData('offer_id', $params['offer_id']);
                $log->setData('user_name', $user);
                $comment = [];
                switch ($updateType) {
                    case 'add':
                        $newQty = $currentInventory + $qtyToUpdate;
                        $offer->setData('inventory_qty', $newQty);
                        $offer->save();
                        /**
                         * Update Log
                         */
                        $comment[] = 'Inventory Qty Before Update = ' . $currentInventory;
                        $comment[] = 'Inventory Qty After Update = ' . $newQty;
                        $log->setData('comment', implode('</br>', $comment));
                        $log->setData('reason', $reason);
                        break;
                    case 'discard':
                        $newQty = $currentInventory - $qtyToUpdate;
                        $offer->setData('inventory_qty', $newQty);
                        $offer->save();
                        /**
                         * Update Log
                         */
                        $comment[] = 'Inventory Qty Before Update = ' . $currentInventory;
                        $comment[] = 'Inventory Qty After Update = ' . $newQty;
                        $log->setData('comment', implode('</br>', $comment));
                        $log->setData('reason', $reason);
                        break;
                }
                $log->save();
            } else {
                throw new LocalizedException(__("This vendor offer doesn't exist"));
            }
            $response = [
                'status' => true,
                'message' => __('Offer updated successfully'),
                'inventory_data' => $offer->toArray()
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Vendor Products
     * @return ApiResponseInterface|mixed
     */
    public function getVendorAssociateProduct()
    {
        try {
            $params = $this->request->getParams();
            if (isset($params['vendor_id'])) {
                $this->validateVendor($params['vendor_id']);
            }
            $vendorId = $params['vendor_id'];
            $productCollection = $this->vendorHelper->getVendorAssociateProductCollection($params['vendor_id']);
            $productData = [];
            foreach ($productCollection as $product) {
                if ($vendorId != null) {
                    /**
                     * Check for correct vendor Id mapped to product
                     */
                    $vendorString = $product->getVendor();
                    $vendorArray = explode(',', $vendorString);
                    if (!in_array($vendorId, $vendorArray)) {
                        continue;
                    }
                    $productData[] = [
                        'sku' => $product->getSku(),
                        'name' => $product->getName()
                    ];
                    /*$vendorName = $this->vendorHelper->getVendorNameByVendorId($vendorId);*/
                }
            }
            $response = [
                'status' => true,
                'product_data' => $productData
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    public function createOffer()
    {
        try {
            $dc = $this->vendorHelper->getConfig(self::VENDOR_TYPE_DC);
            $farm = $this->vendorHelper->getConfig(self::VENDOR_TYPE_FARM);
            $params = $this->request->getBodyParams();
            if (isset($params['vendor_id'])) {
                $this->validateVendor($params['vendor_id']);
            }
            $sku = $params['sku'];
            $qty = $params['qty'];
            $vendorType = $params['vendor_type'];
            /**
             * Initialize default values
             */
            if ($vendorType == $dc) {
                $defaultValuesAttributes = $this->inventoryHelper->defaultValuesAttributes();
            } else {
                $defaultValuesAttributes = $this->inventoryHelper->defaultValuesAttributesForFarm();
            }
            $offerProductAttributes = $this->inventoryHelper->offerProductAttributes();
            $offer = $this->vendorHelper->getVendorInventoryModel();
            $product = $this->catalogHelper->getProductBySku($sku);
            $offer->setData('sku', $sku);
            $offer->setData('name', $params['name']);
            if ($params['vendor_type'] == $dc) {
                $offer->setData('qty_uploaded', $qty);
            }
            if ($params['vendor_type'] == $farm) {
                $offer->setData('qty_uploaded', $qty);
                $offer->setData('inventory_qty', $qty);
                $offer->setData('qty_confirmed', $qty);

                if (isset($params['price'])) {
                    $offer->setData('price', $params['price']);
                }
                if (isset($params['qty_per_box'])) {
                    $offer->setData('qty_per_box', $params['qty_per_box']);
                }
            }
            foreach ($offerProductAttributes as $productAttribute => $offerAttribute) {
                $offer->setData($offerAttribute, $product->getData($productAttribute));
            }
            foreach ($defaultValuesAttributes as $offerAttribute => $value) {
                $offer->setData($offerAttribute, $value);
            }
            foreach ($params as $attribute => $value) {
                $offer->setData($attribute, $value);
            }
            $offer->save();
            $response = [
                'status' => true,
                'message' => __('Offer created successfully')
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Validate Vendor
     *
     * @param $vendorId
     * @return bool
     * @throws LocalizedException
     */
    public function validateVendor($vendorId)
    {
        $vendor = $this->vendorHelper->getVendorModelById($vendorId);
        if ($vendor->getId() == null) {
            throw new LocalizedException(__("This vendor doesn't exist"));
        }
        return true;
    }

    /**
     * Return Log Details
     *
     * @return ApiResponseInterface|mixed
     */
    public function getLogDetails()
    {
        $response = $finalData = [];
        try {
            $params = $this->request->getParams();
            $log = $this->vendorHelper->getInventoryLogCollection();
            $log->getSelect()
                ->joinLeft(
                    ['offer' => 'vendor_inventory_offer_table'],
                    "main_table.offer_id=offer.offer_id"
                )->joinLeft(
                    ['vendor' => 'vendor_registrations'],
                    "offer.vendor_id=vendor.vendor_id",
                    ['vendor_name' => 'vendor.vendor_name']
                )->joinLeft(
                    ['location' => 'dropship_vendor_locations'],
                    "offer.loc_id = location.loc_id",
                    ['location_name' => 'location.loc_name']
                );
            if (isset($params['from_date']) && isset($params['to_date'])) {
                $fromDate = $params['from_date'] . ' 00:00:00';
                $toDate = $params['to_date'] . ' 00:00:00';
                $log->addFieldToFilter('main_table.created_at', ['gteq' => $fromDate]);
                $log->addFieldToFilter('main_table.created_at', ['lteq' => $toDate]);
            }
            if (isset($params['vendor_id'])) {
                $log->getSelect()
                    ->where("offer.vendor_id =?", $params['vendor_id']);
            }
            if (isset($params['location_id'])) {
                $log->getSelect()
                    ->where("offer.loc_id =?", $params['location_id']);
            }
            if ($log->getSize() > 0) {
                foreach ($log as $oneLog) {
                    $finalData[] = $oneLog->getData();
                }
            }
            $response = ['status' => true, 'log_data' => $finalData];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    public function execute()
    {
        /*$response = [];
        try {
            // throw new LocalizedException(__("Invalid username or password"));
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;*/
    }
}