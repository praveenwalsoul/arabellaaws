<?php

namespace Flordelcampo\UPSDomestic\Block;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Flordelcampo\Vendor\Helper\Data as vendorHelper;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Backend\Model\Auth\Session;
use Magento\User\Model\User;

/**
 * Class Domestic
 *
 * Flordelcampo\Dashboards\Block
 */
class Domestic extends Template
{

    /**
     * @var vendorHelper
     */
    protected $vendorHelper;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var Session
     */
    protected $authSession;

    /**
     * Domestic constructor.
     * @param Context $context
     * @param vendorHelper $vendorHelper
     * @param orderHelper $orderHelper
     * @param Session $authSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        vendorHelper $vendorHelper,
        orderHelper $orderHelper,
        Session $authSession,
        array $data = []
    )
    {
        $this->vendorHelper = $vendorHelper;
        $this->orderHelper = $orderHelper;
        $this->authSession = $authSession;
        parent::__construct($context, $data);
    }

    /**
     * Ship from date
     *
     * @return false|string
     */
    public function shipFromDate()
    {
        $today = date('Y-m-d');
        return date('Y-m-d', (strtotime('-7 day', strtotime($today))));
    }

    /**
     * Ship To Date
     * @return false|string
     */
    public function shipToDate()
    {
        $today = date('Y-m-d');
        return date('Y-m-d', (strtotime('+7 day', strtotime($today))));
    }

    /**
     * Get vendors list
     *
     * @return array
     */
    public function getAllVendors1()
    {
        return $this->vendorHelper->getAllVendorListArray1();
    }

    /**
     * Store List
     *
     * @return array
     */
    public function getAllStores()
    {
        $storeArray = $this->orderHelper->getStoreListArray();
        $storeArray[''] = 'all';
        asort($storeArray);
        return $storeArray;
    }

    /**
     * Return Controller URL
     *
     * @return string
     */
    public function getFedexInfoUrl()
    {
        return $this->getUrl('dashboards/domestic/info');
    }

    /**
     * Return gift template Download URl
     * @return string
     */
    public function getTemplateDownloadUrl()
    {
        return $this->getUrl('dashboards/domestic/templates');
    }

    /**
     * Return Upload URL
     *
     * @return string
     */
    public function getUploadUrlsUps()
    {
        
        return $this->getUrl('dashboards/domestic/upload');
    }

     /**
     * Return order State change URL
     *
     * @return string
     */
    public function getStateOrderUrl()
    {
        return $this->getUrl('dashboards/extra/orderstate');
    }

    /**
     * Return box State change URL
     *
     * @return string
     */
    public function getBoxStateUrl()
    {
        return $this->getUrl('dashboards/extra/boxstate');
    }

    /**
     * Return Ups Order Collection
     * @return Collection
     */
    public function getUpsOrdersCollection()
    {
        return $this->orderHelper->getProcessingOrderCollection();
    }

    /**
     * Return current vendor user id if logged in user is vendor
     */
    public function getVendorUserId()
    {
        $vendorUserId = 0;
        try {
            $user = $this->getCurrentLoggedAdminUser();
            $currentUserEmail = $user->getEmail();
            $vendorCollection = $this->vendorHelper->getVendorCollectionByEmail($currentUserEmail);
            if ($vendorCollection != null) {
                foreach ($vendorCollection as $vendor) {
                    $vendorUserId = $vendor->getVendorId();
                }
            }
        } catch (\Exception $e) {
            $vendorUserId = 0;
        }
        return $vendorUserId;
    }

    /**
     * Return Logged in user
     * @return User|null
     */
    public function getCurrentLoggedAdminUser()
    {
        return $this->authSession->getUser();
    }
}