<?php

namespace Flordelcampo\UPSDomestic\Controller\Adminhtml\Domestic;

use Exception;
use Flordelcampo\Order\Model\Box;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\File\Csv;
use Flordelcampo\Order\Model\BoxFactory;
use Flordelcampo\Order\Helper\Data as OrderHelper;

/**
 * Class Upload
 * Flordelcampo\Dashboards\Controller\Adminhtml\Ups
 */
class Upload extends Action
{
    /**
     * @var null
     */
    private $_boxes;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var Csv
     */
    protected $csv;
    /**
     * @var BoxFactory
     */
    protected $boxFactory;
    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param Csv $csv
     * @param BoxFactory $boxFactory
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        Csv $csv,
        BoxFactory $boxFactory,
        OrderHelper $orderHelper
    )
    {
        $this->csv = $csv;
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->boxFactory = $boxFactory;
        $this->orderHelper = $orderHelper;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    public function execute()
    {
        try {
            

            if (!empty($this->getRequest()->getParam('box_ids'))) {
                $boxIds = $this->getRequest()->getParam('box_ids');
                $response = $this->changeBoxStatusToInvoiced($boxIds);
            }
            if (!empty($this->getRequest()->getFiles('gift_file'))) {
                $file = $this->getRequest()->getFiles('gift_file');
                $response = $this->updateGiftMessage($file);
            }
            if (!empty($this->getRequest()->getFiles('track_file'))) {
                $file = $this->getRequest()->getFiles('track_file');
                $response = $this->updateTrackId($file);
            }
            if (!empty($this->getRequest()->getFiles('print_status'))) {
                $file = $this->getRequest()->getFiles('print_status');
                $response = $this->changeBoxStatusToPrinted($file);
            }
            /* $this->messageManager->addSuccessMessage(
                 __('Data Updated Successfully')
             );*/
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        /*$resultRedirect = $this->resultPageFactory->create(
            ResultFactory::TYPE_REDIRECT
        );
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;*/
    }

    /**
     * Update Gift Messages
     *
     * @param $FILE
     * @throws Exception
     */
    public function updateGiftMessage($FILE)
    {
        try {
            $rawData = $this->csv->getData($FILE['tmp_name']);
            $count = 1;
            foreach ($rawData as $oneRow) {
                if ($count == 1) {
                    $count++;
                    continue;
                } else {
                    $boxId = $oneRow[0];
                    $boxModel = $this->orderHelper->getBoxModelByBoxId($boxId);
                    if ($boxModel != null) {
                        $newGiftMessage = $oneRow[3];
                        $boxModel->setGiftMessage($newGiftMessage)
                            ->save();
                    }
                }
            }
            echo "Gift Message Updated Successfully";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Update Track Ids
     *
     * @param $FILE
     * @throws Exception
     */
    public function updateTrackId($FILE)
    {
        try {
            $rawData = $this->csv->getData($FILE['tmp_name']);
            $count = 1;
            foreach ($rawData as $oneRow) {
                if ($count == 1) {
                    $count++;
                    continue;
                } else {
                    $boxId = $oneRow[0];
                    $boxModel = $this->orderHelper->getBoxModelByBoxId($boxId);
                    if ($boxModel != null) {
                        $newTrackId = $oneRow[3];
                        $boxModel->setTrackId($newTrackId)
                            ->setStatus('shipped')
                            ->save();
                    }
                    $this->updateOrderStatus($boxId);
                }
            }
            echo "Track Id Updated Successfully";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Update order status based on box status
     * @param $boxId
     */
    public function updateOrderStatus($boxId)
    {
        $boxModel = $this->orderHelper->getBoxModelByBoxId($boxId);
        $boxData = $boxModel->getData();
        $orderId = $boxData['order_id'];
        $totalBoxCollectionData = $this->orderHelper->getBoxCollectionByOrderId($orderId);
        if ($totalBoxCollectionData != null) {
            $totalBoxCount = $totalBoxCollectionData->count();
            $totalShippedBoxCollectionData = $this->orderHelper->getBoxCollectionByOrderId($orderId, 'shipped');
            $totalShippedBoxCount = $totalShippedBoxCollectionData->count();
            if ($totalBoxCount == $totalShippedBoxCount) {
                $order = $this->orderHelper->getOrderModelById($orderId);
                $order->setState('shipped')->setStatus('shipped');
                $order->save();
            }
        }
    }

    /**
     * Change status to invoiced
     * @param $box_Ids
     */
    public function changeBoxStatusToInvoiced($box_Ids)
    {
        try {
            if ($box_Ids != '' && $box_Ids != null) {
                $boxArray = explode(",", $box_Ids);
                $boxIdString = "'" . implode("', '", $boxArray) . "'";
                $query = "UPDATE order_item_box 
                          SET status = 'invoiced' 
                          WHERE box_id in ($boxIdString) and status ='shipped' ";
                         // print_r($query);die;
                $this->resourceConnection->getConnection()->query($query);
                echo "Box status Updated Successfully";
            } else {
                echo "No Box Ids Found";
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Update Box status to printed
     *
     * @param $FILE
     */
    public function changeBoxStatusToPrinted($FILE)
    {
        try {
            $rawData = $this->csv->getData($FILE['tmp_name']);
            $count = 1;
            foreach ($rawData as $oneRow) {
                if ($count == 1) {
                    $count++;
                    continue;
                } else {
                    $boxId = $oneRow[0];
                    $boxModel = $this->orderHelper->getBoxModelByBoxId($boxId);
                    if ($boxModel != null) {
                        $boxModel->setStatus('printed')
                            ->save();
                    }
                }
            }
            echo "Box status changed to Printed Successfully";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
