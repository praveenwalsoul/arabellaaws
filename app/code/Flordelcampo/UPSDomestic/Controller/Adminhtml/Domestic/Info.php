<?php

namespace Flordelcampo\UPSDomestic\Controller\Adminhtml\Domestic;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\UPSDomestic\Block\Domestic;

/**
 * Class Index
 *
 * Flordelcampo\UPSDomestic\Controller\Adminhtml\Domestic
 */
class Info extends Action
{
    /**
     * @var null
     */
    protected $_params;
    /**
     * @var
     */
    protected $_vendorUserId;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var Domestic
     */
    protected $domesticBlock;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param ResourceConnection $resourceConnection
     * @param Domestic $domesticBlock
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        ResourceConnection $resourceConnection,
        Domestic $domesticBlock
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->domesticBlock = $domesticBlock;
        parent::__construct($context);
    }

    public function execute()
    {
        //echo"hello ";die();
        $resultJson = $this->jsonFactory->create();
        try {
            $vendorUserId = $this->domesticBlock->getVendorUserId();
            $this->_vendorUserId = $vendorUserId;
            $params = $this->getRequest()->getParam('data');
            $this->_params = $params;
            if (count($params) > 0) {
                $responseData = $this->getBoxDataHtml();

                $data = [
                    'status' => true,
                    'html' => $responseData['html'],
                    'sku_html' => $responseData['sku_html'],
                    'box_ids' => $responseData['all_box_ids']
                ];
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Invalid inputs'
                ];
            }

        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * Return Box Data
     * @return array
     */
    public function getBoxDataHtml()
    {
        $inputParams = $this->_params;
        $shipFromDate = $inputParams['ship_date_from'];
        $shipToDate = $inputParams['ship_date_to'];
        $vendorId = $inputParams['vendor_id'];
        $storeId = $inputParams['store_id'];
        $queryResult = $this->getQueryResult();
        /*print_r($queryResult);die();*/
        $vendorWiseData = $this->vendorWiseQueryResult($queryResult);
        $headers = $this->getHeaders();
        $html = "<table class='admin__table-secondary customer-order-items-table'><tbody>";
        $html .= "<tr>";
        $vendorUserId = $this->_vendorUserId;
        $hideColumnsArray = $this->hideColumns();
        foreach ($headers as $colName) {
            if ($vendorUserId > 0 && in_array($colName, $hideColumnsArray)) {
                continue;
            }
            $html .= "<th>$colName</th>";
        }
        $html .= "</tr>";
        $grandTotalProcessing = $vendorWiseData['grand_total_processed_count'];
        $grandTotalPrinted = $vendorWiseData['grand_total_printed_count'];
        $grandTotalShipped = $vendorWiseData['grand_total_shipped_count'];
        $grandTotalBoxes = $vendorWiseData['grand_total_boxes_count'];
        $grandTotalBoxesSuccess = $vendorWiseData['grand_total_success_count'];
        $grandTotalBoxesError = $vendorWiseData['grand_total_error_count'];
        $grandTotalVendorPrinted = $vendorWiseData['grand_total_vendorprinted_count'];
        $grandTotalNotPicked = $vendorWiseData['grand_total_notpicked_count'];
        $vendorData = $vendorWiseData['vendor_data'];

        /**
         * Vendor wise SKU HTML
         */
        $skuHtml = $this->getVendorSkuWiseHtml($vendorWiseData);
        /**
         * Block Params
         */
        $isVendor = "false";
        $disabled = '';
        if ($vendorUserId > 0) {
            $isVendor = "true";
            $disabled = 'disabled';
        }
        $formKey = $this->domesticBlock->getFormKey();
        $TemplateUrl = $this->domesticBlock->getTemplateDownloadUrl();
        foreach ($vendorData as $oneVendor) {
            $boxes = $oneVendor['boxes'];
            $vendorId = $oneVendor['vendor_id'];
            $vendorName = $oneVendor['vendor_name'];
            $processedBox = (isset($oneVendor['processing'])) ? $oneVendor['processing'] : '0';
            $printedBox = (isset($oneVendor['printed'])) ? $oneVendor['printed'] : '0';
            $labelBox = (isset($oneVendor['Success'])) ? $oneVendor['Success'] : '0';
            $labelErro = (isset($oneVendor['Hard'])) ? $oneVendor['Hard'] : '0';
            $vendorPrinted = (isset($oneVendor['vendorPrint'])) ? $oneVendor['vendorPrint'] : '0';
            $notPicked = (isset($oneVendor['1'])) ? $oneVendor['1'] : '0';
            $shippedBox = (isset($oneVendor['shipped'])) ? $oneVendor['shipped'] : '0';
            $totalBox = (isset($oneVendor['total'])) ? $oneVendor['total'] : '0';

            $processedBoxHtml = $lebelCountForm = $lebelErrorForm = $vendorPrintedForm  = $notPrintedForm = $shippedBoxHtml = $totalBoxHtml = $printedBoxHtml = '0';
            if ($processedBox != '0') {
             $processedBoxes = $oneVendor['process_boxes'];
             $processedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $processedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $processedBox . "'></form>";
            }

            if ($printedBox != '0') {
             $printedBoxes = $oneVendor['printed_boxes'];
             $printedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $printedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $printedBox . "'></form>";
            }

            if ($labelBox != '0') {
             $successLabel = $oneVendor['SUCCESS_boxes'];
             $lebelCountForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $successLabel . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $labelBox . "'></form>";
            }

            if ($labelErro != '0') {
             $errorLabel = $oneVendor['ERROR_boxes'];
             $lebelErrorForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $errorLabel . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $labelErro . "'></form>";
            }

            if ($vendorPrinted != '0') {
             $printedVendor = $oneVendor['vendorPrinted_boxes'];
             $vendorPrintedForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $printedVendor . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $vendorPrinted . "'></form>";
            }

            if ($notPicked != '0') {
            $notPickedUp = $oneVendor['th_activity_boxes'];
            $notPrintedForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $notPickedUp . "' >
                <input type='hidden' name='type' value='not_pick' >
                <input type='submit' $disabled value='" . $notPicked . "'></form>";
            }

            if ($shippedBox != '0') {
            $shippedBoxes = $oneVendor['shipped_boxes'];
            $shippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $shippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $shippedBox . "'></form>";
            }


            if ($totalBox != '0') {
             $totalBoxes = $oneVendor['total_boxes'];
             $totalBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $totalBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $totalBox . "'></form>";
            }

            /**
             * Reprint Label
             */
           
            $reprintForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='reprint_file' >
                <input type='hidden' name='shipToDate' value='".$shipToDate."'' >
                <input type='hidden' name='shipFromDate' value='".$shipFromDate."'' >
                <input type='hidden' name='vendorId' value='".$vendorId."'' >
                <input type='hidden' name='storeId' value='".$storeId."'' >
                <input type='submit' value='Download'></form>";    


            /**
             * Domestic Generate Label File Form
             */
             
            $upsFileForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='ups_file' >
                <input type='hidden' name='shipToDate' value='".$shipToDate."'' >
                <input type='hidden' name='shipFromDate' value='".$shipFromDate."'' >
                <input type='hidden' name='vendorId' value='".$vendorId."'' >
                <input type='hidden' name='storeId' value='".$storeId."'' >
                <input type='submit' value='Generate Label'></form>";

             /**
             * All Boxes with Box Id and status
             */
            $allLebelsForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='all_boxes' >
                <input type='hidden' name='shipToDate' value='".$shipToDate."'' >
                <input type='hidden' name='shipFromDate' value='".$shipFromDate."'' >
                <input type='hidden' name='vendorId' value='".$vendorId."'' >
                <input type='hidden' name='storeId' value='".$storeId."'' >
                <input type='submit' value='Download Label'></form>";

            /**
             * All Boxes with Box Id and status
             */
             /*
            $allZebraLebelsForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='all_boxes1' >
                <input type='hidden' name='shipToDate' value='".$shipToDate."'' >
                <input type='hidden' name='shipFromDate' value='".$shipFromDate."'' >
                <input type='hidden' name='vendorId' value='".$vendorId."'' >
                <input type='hidden' name='storeId' value='".$storeId."'' >
                <input type='submit' value='Download Zebra Label'></form>"; */
            
            /**
             * Download Gift Message Form
             */
            $giftForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='gift' >
                <input type='submit' value='Download'></form>";  

             /**
             * UPs File Form
             */
            $upsFile = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='ups' >
                <input type='submit' value='Download'></form>"; 
            /**
             * Track Id
             */
            $trackFileForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='track_file' >
                <input type='submit' value='Download'></form>"; 
                
            /**
             * All Ship Method download  csv
             */
            /*$shipForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='ship_method' >
                <input type='hidden' name='shipToDate' value='".$shipToDate."'' >
                <input type='hidden' name='shipFromDate' value='".$shipFromDate."'' >
                <input type='hidden' name='vendorId' value='".$vendorId."'' >
                <input type='hidden' name='storeId' value='".$storeId."'' >
                <input type='submit' value='Download'></form>"; */
                
            /* * download all vendor csv */
            
                $shipForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='ship_method' >
                <input type='submit' value='All Ship'></form>";     
    

            $html .= "<tr>";
            $html .= "<td class='value'>" . $vendorName . "</td>";
            $html .= "<td class='value'>" . $processedBoxHtml . "</td>";
            $html .= "<td class='value'>" . $printedBoxHtml . "</td>";
            //$html .= "<td class='value'>" . $shippedBoxHtml . "</td>";
            if($isVendor == "false"){
              $html .= "<td class='value'>" . $lebelCountForm . "</td>"; 
              $html .= "<td class='value'>" . $lebelErrorForm . "</td>";
              $html .= "<td class='value'>" . $vendorPrintedForm . "</td>"; 
              $html .= "<td class='value'>" . $notPrintedForm . "</td>";
            }
            $html .= "<td class='value'>" . $shippedBoxHtml . "</td>";
            $html .= "<td class='value'>" . $totalBoxHtml . "</td>";
            $html .= "<td class='value'>" . $reprintForm . "</td>";

            if ($isVendor == "false") {
               // $html .= "<td class='value'>" . $lebelCountForm . "</td>";
                $html .= "<td class='value'>" . $upsFileForm . "</td>";
               // $html .= "<td class='value'>" . $lebelErrorForm . "</td>";
                $html .= "<td class='value'>" . $allLebelsForm . "</td>";
                //$html .= "<td class='value'>" . $allZebraLebelsForm . "</td>";
                $html .= "<td class='value'>" . $giftForm . "</td>";
                $html .= "<td class='value'>" . $upsFile . "</td>";
                $html .= "<td class='value'>" . $trackFileForm . "</td>";
                $html .= "<td class='value'>" . $shipForm . "</td>";
            }

            $html .= "</tr>";

        }
        $globalProcessedBoxHtml = $globalShippedBoxHtml = $globalTotalBoxErrorHtml = $globalTotalBoxVendorPrintedHtml = $grandTotalNotPickedUpHtml  = $globalTotalBoxHtml = $globalTotalBoxSuccessHtml = $globalPrintedBoxHtml = '0';
        if ($grandTotalProcessing != '0') {
            $globalProcessedBoxes = $vendorWiseData['grand_total_processed_boxes'];
            $globalProcessedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalProcessedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalProcessing . "'></form>";
        }

        if ($grandTotalPrinted != '0') {
            $globalPrintedBoxes = $vendorWiseData['grand_total_printed_count'];
            $globalPrintedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalPrintedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalPrinted . "'></form>";
        }
        if ($grandTotalBoxesSuccess != '0') {
            $globalTotalBoxesSuccess = $vendorWiseData['grand_total_success_boxes'];
            $globalTotalBoxSuccessHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxesSuccess . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalBoxesSuccess . "'></form>";
        }

        if ($grandTotalBoxesError != '0') {
            $globalTotalBoxesError = $vendorWiseData['grand_total_error_boxes'];
            $globalTotalBoxErrorHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxesError . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalBoxesError . "'></form>";
        }

        if ($grandTotalVendorPrinted != '0') {
            $totalVendorPrinted = $vendorWiseData['grand_total_error_boxes'];
            $globalTotalBoxVendorPrintedHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $totalVendorPrinted . "' >
                <input type='hidden' name='type' value='box_detailss' >
                <input type='submit' $disabled value='" . $grandTotalVendorPrinted . "'></form>";
        }

        if ($grandTotalNotPicked != '0') {
            $grandTotalNotPickedUp = $vendorWiseData['grand_total_notpicked_count'];
            $grandTotalNotPickedUpHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $grandTotalNotPickedUp . "' >
                <input type='hidden' name='type' value='box_detailsss' >
                <input type='submit' $disabled value='" . $grandTotalNotPicked . "'></form>";
        }

        if ($grandTotalShipped != '0') {
            $globalShippedBoxes = $vendorWiseData['grand_total_shipped_boxes'];
            $globalShippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalShippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalShipped . "'></form>";
        }
        
        /*if ($grandTotalBoxesSuccess != '0') {
            $globalTotalBoxesSuccess = $vendorWiseData['grand_total_success_boxes'];
            $globalTotalBoxSuccessHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxesSuccess . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalBoxesSuccess . "'></form>";
        }
*/
        if ($grandTotalBoxes != '0') {
            $globalTotalBoxes = $vendorWiseData['grand_total_boxes_boxes'];
            $globalTotalBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalBoxes . "'></form>";
        }


        $html .= "</tbody>";
        $html .= "<tfoot><tr>";
        $html .= "<td class='value'>Total</td>";
        $html .= "<td class='value'>" . $globalProcessedBoxHtml . "</td>";
        $html .= "<td class='value'>" . $globalPrintedBoxHtml . "</td>";
        $html .= "<td class='value'>" . $globalTotalBoxSuccessHtml . "</td>";
        $html .= "<td class='value'>" . $globalTotalBoxErrorHtml . "</td>";
        $html .= "<td class='value'>" . $globalTotalBoxVendorPrintedHtml . "</td>";
        $html .= "<td class='value'>" . $grandTotalNotPickedUpHtml . "</td>";
        $html .= "<td class='value'>" . $globalShippedBoxHtml . "</td>";
        //$html .= "<td class='value'>" . $globalTotalBoxSuccessHtml . "</td>";
        $html .= "<td class='value'>" . $globalTotalBoxHtml . "</td>";
        $html .= "<td colspan='3'></td>";
        $html .= "</tr></tfoot>";
        $html .= "</table>";

        /**
         * SKU wise html
         */
        $skuHtml .= "</tbody>";
        $skuHtml .= "<tfoot><tr>";
        $skuHtml .= "<td class='value' colspan='3'>Total</td>";
        $skuHtml .= "<td class='value'>" . $globalProcessedBoxHtml . "</td>";
        $skuHtml .= "<td class='value'>" . $globalPrintedBoxHtml . "</td>";
        $skuHtml .= "<td class='value'>" . $globalShippedBoxHtml . "</td>";
        $skuHtml .= "<td class='value'>" . $globalTotalBoxHtml . "</td>"; 
        $skuHtml .= "</tr></tfoot>";
        $skuHtml .= "</table>";

        $response = [];
        $response['html'] = $html;
        $response['sku_html'] = $skuHtml;
        $response['all_box_ids'] = $vendorWiseData['grand_total_boxes_boxes'];
        return $response;
    }

    /**
     * Return HTML for vendor
     *
     * @param $vendorWiseData
     */
    public function getVendorSkuWiseHtml($vendorWiseData)
    {
        $formKey = $this->domesticBlock->getFormKey();
        $TemplateUrl = $this->domesticBlock->getTemplateDownloadUrl();
        $vendorUserId = $this->_vendorUserId;
        $disabled = '';
        if ($vendorUserId > 0) {
            $disabled = 'disabled';
        }
        $vendorSkuData = $vendorWiseData['vendor_sku_wise_data'];
        $vendorNameArray = $vendorWiseData['vendor_name_array'];
        $skuNameArray = $vendorWiseData['sku_name_array'];

        $headers = $this->getSKuWiseHeaders();

        $skuHtml = "<table class='admin__table-secondary vendor-sku-item-table'><tbody>";
        $skuHtml .= "<tr>";
        foreach ($headers as $colName) {
            $skuHtml .= "<th>$colName</th>";
        }
        $skuHtml .= "</tr>";
        foreach ($vendorSkuData as $vendorId => $skuData) {
            //print_r($skuData);die();
            $vendorName = $vendorNameArray[$vendorId];
            foreach ($skuData as $sku => $vendorSKuData) {
                $productName = $skuNameArray[$sku];
                $processedBox = (isset($vendorSKuData['processing'])) ? $vendorSKuData['processing'] : '0';
                $printedBox = (isset($vendorSKuData['printed'])) ? $vendorSKuData['printed'] : '0';
                $labelBox = (isset($vendorSKuData['Success'])) ? $vendorSKuData['Success'] : '0';
                $labelErro = (isset($vendorSKuData['Hard'])) ? $vendorSKuData['Hard'] : '0';
                $shippedBox = (isset($vendorSKuData['shipped'])) ? $vendorSKuData['shipped'] : '0';
                $totalBox = (isset($vendorSKuData['total'])) ? $vendorSKuData['total'] : '0';

                $processedBoxHtml = $shippedBoxHtml = $totalBoxHtml = $printedBoxHtml = '0';
                if ($processedBox != '0') {
                    $processedBoxes = $vendorSKuData['process_boxes'];
                    $processedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $processedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $processedBox . "'></form>";
                }

                if ($printedBox != '0') {
                    $printedBoxes = $vendorSKuData['printed_boxes'];
                    $printedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $printedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $printedBox . "'></form>";
                }

                if ($shippedBox != '0') {
                    $shippedBoxes = $vendorSKuData['shipped_boxes'];
                    $shippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $shippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $shippedBox . "'></form>";
                }
                if ($totalBox != '0') {
                    $totalBoxes = $vendorSKuData['total_boxes'];
                    $totalBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $totalBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $totalBox . "'></form>";
                }

            $downloadSkuWiseData = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $totalBoxes . "' >
                <input type='hidden' name='type' value='box_details1' >
                <input type='submit' value='Download'></form>";
                /*if ($totalBox != '0') {*/
                    /*$totalBoxes = $vendorSKuData['total_boxes'];
                    $downloadSkuWiseData = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $totalBoxes . "' >
                <input type='hidden' name='type' value='sku_file' >
                <input type='submit' $disabled value='Download'></form>";*/
               // }*/

                $skuHtml .= "<tr>";
                $skuHtml .= "<td class='value'>" . $vendorName . "</td>";
                $skuHtml .= "<td class='value'>" . $sku . "</td>";
                $skuHtml .= "<td class='value'>" . $productName . "</td>";
                $skuHtml .= "<td class='value'>" . $processedBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $printedBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $shippedBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $totalBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $downloadSkuWiseData . "</td>";

                $skuHtml .= "</tr>";
            }

        }

        return $skuHtml;
    }

    /**
     * Vendor wise Data
     * @param $queryResult
     * @return array
     */
    public function vendorWiseQueryResult($queryResult)
    {
       
        $vendorWise = $vendorNameArray = $processedData = [];
        $globalTotal = $globalTotalShipped = $globalTotalProcessed = $globalTotalSuccess = $globalTotalError = $globalTotalVendorPrinted = $globalTotalNotPicked = $globalTotalPrinted = 0;
        $globalTotalBoxes = $globalTotalShippedBoxes = $globalTotalSuccessBoxes = $globalTotalErrorBoxes = $globalTotalVendorPrintedBoxes = $globalTotalNotPickedBoxes = $globalTotalProcessedBoxes = $globalTotalPrintedBoxes = [];
        $vendorSkuWise = $skuNameArray = $vendorNameArray = [];
        foreach ($queryResult as $oneBox) {
            $sku = $oneBox['sku'];
            $skuNameArray[$sku] = $oneBox['name'];
            $vendorWise[$oneBox['vendor']]['vendor_name'] = $oneBox['vendor_name'];
            $vendorWise[$oneBox['vendor']]['vendor_id'] = $oneBox['vendor'];
            $vendorWise[$oneBox['vendor']]['activityCount'] = $oneBox['activityCount'];
            $globalTotalBoxes[] = $oneBox['box_id'];
            $globalTotal++;
            $vendorNameArray[$oneBox['vendor']] = $oneBox['vendor_name'];
            if (isset($vendorWise[$oneBox['vendor']]['boxes'])) {
                $vendorWise[$oneBox['vendor']]['boxes'] = $vendorWise[$oneBox['vendor']]['boxes']
                    . "," . $oneBox['box_id'];
            } else {
                $vendorWise[$oneBox['vendor']]['boxes'] = $oneBox['box_id'];
            }
            if ($oneBox['status'] == 'pending' || $oneBox['status'] == 'processing') {
                $globalTotalProcessed++;
                $globalTotalProcessedBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['processing'])) {
                    $vendorWise[$oneBox['vendor']]['processing'] += 1;
                    $vendorWise[$oneBox['vendor']]['process_boxes'] = $vendorWise[$oneBox['vendor']]['process_boxes']
                        . "," . $oneBox['box_id'];

                } else {
                    $vendorWise[$oneBox['vendor']]['processing'] = 1;
                    $vendorWise[$oneBox['vendor']]['process_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['processing'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['processing'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['process_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['process_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['processing'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['process_boxes'] = $oneBox['box_id'];
                }
            }

            if ($oneBox['status'] == 'printed') {
                $globalTotalPrinted++;
                $globalTotalPrintedBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['printed'])) {
                    $vendorWise[$oneBox['vendor']]['printed'] += 1;
                    $vendorWise[$oneBox['vendor']]['printed_boxes'] = $vendorWise[$oneBox['vendor']]['printed_boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['printed'] = 1;
                    $vendorWise[$oneBox['vendor']]['printed_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['printed'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['printed'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['printed_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['printed_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['printed'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['printed_boxes'] = $oneBox['box_id'];
                }
            }

/* Success Code =======================================================================================*/



if ($oneBox['lebel_status'] == 'Success') {
                $globalTotalSuccess++;
                $globalTotalSuccessBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['Success'])) {
                    $vendorWise[$oneBox['vendor']]['Success'] += 1;
                    $vendorWise[$oneBox['vendor']]['SUCCESS_boxes'] = $vendorWise[$oneBox['vendor']]['SUCCESS_boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['Success'] = 1;
                    $vendorWise[$oneBox['vendor']]['SUCCESS_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['Success'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['Success'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['SUCCESS_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['SUCCESS_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['Success'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['SUCCESS_boxes'] = $oneBox['box_id'];
                }
            }
/* ======================================Error code ========================================== */            
            if ($oneBox['lebel_status'] == 'Hard') {
                $globalTotalError++;
                $globalTotalErrorBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['Hard'])) {
                    $vendorWise[$oneBox['vendor']]['Hard'] += 1;
                    $vendorWise[$oneBox['vendor']]['ERROR_boxes'] = $vendorWise[$oneBox['vendor']]['ERROR_boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['Hard'] = 1;
                    $vendorWise[$oneBox['vendor']]['ERROR_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['Hard'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['Hard'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['ERROR_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['ERROR_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['Hard'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['ERROR_boxes'] = $oneBox['box_id'];
                }
            }
/* =================================End Code=================================================== */

/* ===================================== VendorPrinted code ==================================== */            
            if ($oneBox['status'] == 'vendorPrint') {
                $globalTotalVendorPrinted++;
                $globalTotalVendorPrintedBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['vendorPrint'])) {
                    $vendorWise[$oneBox['vendor']]['vendorPrint'] += 1;
                    $vendorWise[$oneBox['vendor']]['vendorPrinted_boxes'] = $vendorWise[$oneBox['vendor']]['vendorPrinted_boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['vendorPrint'] = 1;
                    $vendorWise[$oneBox['vendor']]['vendorPrinted_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrint'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrint'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrinted_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrinted_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrint'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrinted_boxes'] = $oneBox['box_id'];
                }
            }
/*========================================== End code ===================================================================================================================== */
/* ===================================== NotPicked code ==================================================================================================================== */            
            if ($oneBox['th_activity'] == 'Order Processed: Ready for UPS' or $oneBox['th_activity'] == 'Shipper created a label, UPS has not received the package yet' && $oneBox['activityCount'] == '1' && $oneBox['status'] == 'vendorPrint' || $oneBox['status'] == 'shipped') {
                $globalTotalNotPicked++;
                $globalTotalNotPickedBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['1'])) {
                    $vendorWise[$oneBox['vendor']]['1'] += 1;
                    $vendorWise[$oneBox['vendor']]['th_activity_boxes'] = $vendorWise[$oneBox['vendor']]['th_activity_boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['1'] = 1;
                    $vendorWise[$oneBox['vendor']]['th_activity_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['1'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['1'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['th_activity_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['th_activity_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['1'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['th_activity_boxes'] = $oneBox['box_id'];
                }
            }
/*========================================== End code ============================================= */



            if ($oneBox['status'] == 'shipped') {
                $globalTotalShipped++;
                $globalTotalShippedBoxes[] = $oneBox['box_id'];

                if (isset($vendorWise[$oneBox['vendor']]['shipped'])) {
                    $vendorWise[$oneBox['vendor']]['shipped'] += 1;
                    $vendorWise[$oneBox['vendor']]['shipped_boxes'] = $vendorWise[$oneBox['vendor']]['shipped_boxes']
                        . "," . $oneBox['box_id'];

                } else {
                    $vendorWise[$oneBox['vendor']]['shipped'] = 1;
                    $vendorWise[$oneBox['vendor']]['shipped_boxes'] = $oneBox['box_id'];
                }
                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['shipped'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['shipped'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['shipped_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['shipped_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['shipped'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['shipped_boxes'] = $oneBox['box_id'];
                }

            }
            if (isset($vendorWise[$oneBox['vendor']]['total'])) {
                $vendorWise[$oneBox['vendor']]['total'] += 1;
                $vendorWise[$oneBox['vendor']]['total_boxes'] = $vendorWise[$oneBox['vendor']]['total_boxes']
                    . "," . $oneBox['box_id'];
            } else {
                $vendorWise[$oneBox['vendor']]['total'] = 1;
                $vendorWise[$oneBox['vendor']]['total_boxes'] = $oneBox['box_id'];
            }

            if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['total'])) {
                $vendorSkuWise[$oneBox['vendor']][$sku]['total'] += 1;
                $vendorSkuWise[$oneBox['vendor']][$sku]['total_boxes'] =
                    $vendorSkuWise[$oneBox['vendor']][$sku]['total_boxes'] . "," . $oneBox['box_id'];
            } else {
                $vendorSkuWise[$oneBox['vendor']][$sku]['total'] = 1;
                $vendorSkuWise[$oneBox['vendor']][$sku]['total_boxes'] = $oneBox['box_id'];
            }
        }

        $processedData['grand_total_processed_count'] = $globalTotalProcessed;
        $processedData['grand_total_printed_count'] = $globalTotalPrinted;
        $processedData['grand_total_success_count'] = $globalTotalSuccess;
        $processedData['grand_total_error_count'] = $globalTotalError;
        $processedData['grand_total_vendorprinted_count'] = $globalTotalVendorPrinted;
        $processedData['grand_total_notpicked_count'] = $globalTotalNotPicked;
        $processedData['grand_total_shipped_count'] = $globalTotalShipped;
        $processedData['grand_total_boxes_count'] = $globalTotal;

        $processedData['grand_total_processed_boxes'] = implode(",", $globalTotalProcessedBoxes);
        $processedData['grand_total_printed_boxes'] = implode(",", $globalTotalPrintedBoxes);
        $processedData['grand_total_success_boxes'] = implode(",", $globalTotalSuccessBoxes);
        $processedData['grand_total_error_boxes'] = implode(",", $globalTotalErrorBoxes);
        $processedData['grand_total_vendorprinted_boxes'] = implode(",", $globalTotalVendorPrintedBoxes);
        $processedData['grand_total_notpicked_boxes'] = implode(",", $globalTotalNotPickedBoxes);

        $processedData['grand_total_shipped_boxes'] = implode(",", $globalTotalShippedBoxes);
        $processedData['grand_total_boxes_boxes'] = implode(",", $globalTotalBoxes);
        $processedData['vendor_data'] = $vendorWise;
        $processedData['sku_name_array'] = $skuNameArray;
        $processedData['vendor_sku_wise_data'] = $vendorSkuWise;
        $processedData['vendor_name_array'] = $vendorNameArray;
        return $processedData;
    }

    /**
     * Query Result
     * @return array
     */
    public function getQueryResult()
    {
        $inputParams = $this->_params;
        $shipFromDate = $inputParams['ship_date_from'];
        $shipToDate = $inputParams['ship_date_to'];
        $vendorId = $inputParams['vendor_id'];
        $storeId = $inputParams['store_id'];
        $vendorQuery = '';
        if ($vendorId != '0') {
            $vendorQuery .= "and item.vendor='$vendorId'";
        }
        if ($storeId != '') {
            $vendorQuery .= "and item.store_id='$storeId'";
        }

        $query = "SELECT box.box_id,box.item_id,item.name,box.status,box.lebel_status,item.vendor,item.sku,item.product_id,vend.vendor_name , track.th_activity as th_activity, track.th_box_id as trackboxId,(SELECT COUNT(track1.th_box_id) from track_history_new track1 where track1.th_box_id = box.box_id) as activityCount
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN vendor_registrations as vend 
                  on item.vendor = vend.vendor_id
                  LEFT JOIN track_history_new as track 
                   on box.box_id = track.th_box_id
                  WHERE (item.pickup_date BETWEEN '$shipFromDate' and '$shipToDate' ) and box.status in ('pending','shipped','printed','vendorPrint') 
                  and item.shipping_method in ('UPS Ground','UPS Next Day Air','UPS 2nd Day Air','UPS Next Day Air Saver') $vendorQuery Group by box.box_id
                  ORDER by item.name ASC ";
                  //print_r($query);die('nmnmnm');
                //$q = $this->resourceConnection->getConnection()->fetchAll($query);
               // print_r($q);die('hello');
        return $this->resourceConnection->getConnection()->fetchAll($query);
    }
    

    /**
     * Get Table name using direct query
     *
     * @param $tableName
     * @return string
     */
    public function getTableName($tableName)
    {
        /* Create Connection */
        $connection = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName($tableName);
        return $tableName;
    }

    /**
     * Column Headers
     * @return string[]
     */
    public function getHeaders()
    {
        return [
            'Vendor Name',
            'New Orders',
            'Email Sent to Farm',
            'Label Count',
            'Label Error',
            'Vendor Printed',
            'Not Picked Up',
            'Shipped From Farm',
            'Total',
            'Reprint Label',
            'UPS Label',
            'Download Labels',
            //'Download Zebra Labels',
            'Gift Message Template',
            'UPS File',
            'Track Id Template',
            'Ship Template'
        ];
    }

    /**
     * Return SKU wise result
     *
     * @return string[]
     */
    public function getSKuWiseHeaders()
    {
        return [
            'Vendor Name',
            'SKU',
            'Product Name',
            'New Orders',
            'Email Sent to Farm',
            'Shipped From Farm',
            'Total',
            'Download Sku Wise'
        ];
    }

    /**
     * Hide these items for vendors
     * @return string[]
     */
    public function hideColumns()
    {
        return [
            'Gift Message Template',
            'UPS File',
            'Track Id Template',
            'All'
        ];
    }
}
