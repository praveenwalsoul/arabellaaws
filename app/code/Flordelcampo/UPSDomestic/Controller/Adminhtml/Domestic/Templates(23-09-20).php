<?php

namespace Flordelcampo\UPSDomestic\Controller\Adminhtml\Domestic;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordelcampo\Catalog\Helper\Data as CategoryHelper;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Directory\Model\CurrencyFactory;

//require_once('lib/PDFlebel/fedex-common.php');
require_once('lib/fpdf182/fpdf.php');

/**
 * Class Templates
 * Flordelcampo\Dashboards\Controller\Adminhtml\Ups
 */
class Templates extends Action
{


    /**
     * @var CurrencyFactory
     */
    private $currencyCode;
    /**
     * @var null
     */
    private $_boxes;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_params;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var CategoryHelper
     */
    protected $categoryHelper;
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;
    protected $dir;
    protected $storeManager;


    protected $AccessLicenseNumber;
    protected $UserId;
    protected $Password;
    protected $shipperNumber;
    protected $credentials;
    public $dimensionsUnits = "IN";
    public $weightUnits = "LBS";
    public $customerContext;
    public $shipperName;
    public $shipperPhoneNumber;
    public $shipperAddressLine1;
    public $shipperCity;
    public $shipperStateProvinceCode;
    public $shipperPostalCode;
    public $shipperCountryCode;
    public $shiptoCompanyName;
    public $shiptoAttentionName;
    public $shiptoPhoneNumber;
    public $shiptoAddressLine1;
    public $shiptoCity;
    public $shiptoStateProvinceCode;
    public $shiptoPostalCode;
    public $shiptoCountryCode;
    public $shipfromCompanyName;
    public $shipfromAttentionName;
    public $shipfromPhoneNumber;
    public $shipfromAddressLine1;
    public $shipfromCity;
    public $shipfromStateProvinceCode;
    public $shipfromPostalCode;
    public $shipfromCountryCode;
    public $serviceCode;
    public $serviceDescription;
    public $packageWeight;
    public $shipmentDigest;
    public $packagingTypeCode;
    public $packagingDescription;
    public $packagingReferenceNumberCode;
    public $packagingReferenceNumberValue;
    public $trackingNumber;
    public $shipmentIdentificationNumber;
    public $graphicImage;
    public $htmlImage;
    public $shipmentDescription;
    public $shipperAttentionName;
    public $orderPrice;
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */

    private $storeConfig;


    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param OrderHelper $orderHelper
     * @param CategoryHelper $categoryHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        OrderHelper $orderHelper,
        CategoryHelper $categoryHelper,
       // StoreManagerInterface $storeManager,
        CurrencyFactory $currencyFactory,
        StoreManagerInterface $storeConfig,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem\DirectoryList $dir
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->fileFactory = $fileFactory;
        $this->storeConfig = $storeConfig;
        $this->orderHelper = $orderHelper;
        $this->categoryHelper = $categoryHelper;
        $this->_dir = $dir;
       // $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->currencyCode = $currencyFactory->create();
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        

        $resultPage = $this->resultPageFactory->create();
        $boxes = $this->getRequest()->getParam('boxes');
        $this->_boxes = $boxes;
        $templateType = $this->getRequest()->getParam('type');
        switch ($templateType) {
            case 'box_details':
                $this->showBoxDetails();
                break;
            case 'box_details1':
                $this->downloadSkuWisePdfFile();
                break;    
            case 'gift':
                $this->downloadGiftTemplate();
                break;
            case 'ups_file':
                $this->generateUpsLabel();
                break;
            case 'ups':
                $this->downloadUpsFileTemplate();
                break;
           case 'track_file':
                $this->downloadTrackFileTemplate();
                break;        
            
            case 'all_boxes':
                $this->downloadAllLebelPdfTemplate(); 
                break;
            case 'all_boxes1':
                $this->downloadZebraLebelPdfTemplate(); 
                break; 
            case 'ship_method':
                $this->downloadAllShipTemplate();
                break;      
            default:
        }
        exit();
        //return $resultPage;
    }
    
 public function setCredentials($access, $user, $pass, $shipper)
    {
        $this->AccessLicenseNumber = $access;
        $this->UserID = $user;
        $this->Password = $pass;
        $this->shipperNumber = $shipper;
        $this->credentials = 1;
        return $this->credentials;
    }

    
    public function downloadSkuWisePdfFile()
        {

            $boxSql = $this->getSqlBoxesString();
            $query1 = "SELECT box.box_id,box.item_id, box.order_id, address.firstname, address.lastname,
            box.lebel_status,box.lebel_error,box.ImageUrl,box.status,item.sku, item.order_id,
            item.name,vend.vendor_name,box.track_id,item.pickup_date,item.delivery_date,box.gift_message
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN sales_order_address as address
                  on box.order_id = address.parent_id
                  LEFT JOIN vendor_registrations as vend on item.vendor = vend.vendor_id
                  WHERE box.box_id in ($boxSql)  and address.address_type in ('billing') group by box.ImageUrl
                  ORDER by item.name ASC";
                  
                  
           
                  
            $queryResult = $this->resourceConnection->getConnection()->fetchAll($query1);
           
            $pdf = new \FPDF();             
                    foreach ($queryResult as $v) {
                        if (isset($v['ImageUrl'])) {
                              
                                $pdf->AddPage();
                                $pdf->Image($v['ImageUrl'],20,60,180,200,'PNG');
                                if($v['gift_message']!== NULL){
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100);
                                $pdf->Multicell(0,10, $v['gift_message'],0,'C');
                                }else{
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100); 
                                $pdf->Multicell(0,10, $v['firstname'].' '.$v['lastname'],0,'C');
                                }
                            }
                       
                         }
                                $pdf->Output();
              }

    /**
     * Show Box Details
     */
    public function showBoxDetails()
    {
        $boxSql = $this->getSqlBoxesString();
        $query = "SELECT box.box_id,box.item_id,box.lebel_status,box.lebel_error,box.status,item.sku,item.name,vend.vendor_name,box.track_id,item.pickup_date,item.delivery_date,box.gift_message
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN vendor_registrations as vend on item.vendor = vend.vendor_id
                  WHERE box.box_id in ($boxSql) /* and box.lebel_status = 'SUCCESS' */
                  ORDER by item.name ASC";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        //print_r($queryResult);die();


        /**
         * SKU wise
         */
        $skuWiseArray = [];
        $totalCount = 0;
        foreach ($queryResult as $key => $result) {
            if (isset($skuWiseArray[$result['sku']])) {
                $skuWiseArray[$result['sku']]['count'] += 1;
                
            } else {
                $skuWiseArray[$result['sku']] = [
                    'sku' => $result['sku'],
                    'name' => $result['name'],
                    'lebel' => $result['lebel_status'],
                    'count' => 1];
                    
            }
            $totalCount++;

        }
        /**
         * Box wise HTML
         */
        echo "<h2>SKU Summary</h2>";
        $html = "<table border='2'><tbody>";
        $html .= "<tr>";
        $html .= "<th>SKU</th>";
        $html .= "<th>Product Name</th>";
        $html .= "<th>Label Status</th>";
        $html .= "<th>Box Count</th>";
        $html .= "</tr>";
        foreach ($skuWiseArray as $sku => $result) {
            $html .= "<tr>";
            foreach ($result as $key => $value) {
                $html .= "<td class='value'>" . $value . "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody>";
        $html .= "<tfoot>";
        $html .= "<tr>";
        $html .= "<td colspan='3'>Total</td>";
        $html .= "<td>".$totalCount."</td>";
        $html .= "</tr>";
        $html .= "</tfoot>";

        $html .= "</table>";
        $html .= "</br>";
        echo $html;
        $headers = $this->getBoxHeaders();
        echo "<h2>Box Details</h2>";
        $html = "<table border='2'><tbody>";
        $html .= "<tr>";
        foreach ($headers as $colName) {
            $html .= "<th>$colName</th>";
        }
        $html .= "</tr>";
        foreach ($queryResult as $key => $result) {
            $html .= "<tr>";
            foreach ($result as $key => $value) {
                $html .= "<td class='value'>" . $value . "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody></table>";
        echo $html;
        exit();
    }

    /**
     * Download Gift template
     */
    public function downloadGiftTemplate()
    {

        $data = [];
        $fileName = 'gift_message_upload_template.csv';
        $data[] = ['box_id', 'sku', 'name', 'gift_message', 'shipping_name', 'order number', 'partner_order_id'];

        $boxSql = $this->getSqlBoxesString();

        /**
         * Get distinct order ids
         */
        $query = "SELECT DISTINCT box.order_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
        }
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $shippingAddress = $order->getShippingAddress();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
        }

        /**
         * Box data
         */
        $query = "SELECT box.box_id,item.sku,item.name,box.gift_message,box.order_id,ord.increment_id,ord.partner_order_id
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item on box.item_id = item.item_id
                  LEFT JOIN sales_order as ord on box.order_id = ord.entity_id
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $key => $result) {
            $queryData = [];
            foreach ($result as $index => $value) {
                if ($index == 'order_id') {
                    $value = $boxOrderData[$value]['ship_name'];
                }
                $queryData[] = $value;
            }
            $data[] = $queryData;
        }
        
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $fp = fopen('php://output', 'w');
        foreach ($data as $row) {
            fputcsv($fp, $row);
        }
        
        fclose($fp);
        exit();
    }
    
    /**
     * @return string
     */
    public function getSymbol()
    {
        $currentCurrency = $this->storeConfig->getStore()->getCurrentCurrencyCode();
        $currency = $this->currencyCode->load($currentCurrency);
        return $currency->getCurrencySymbol();
    }
    
    
    /*New  Code-----------------------------------------------------------------FOR GENERATE LABEL IMAGE---------------===============================================-------------------*/

   /**
     * @return generate ups label
     */

    public function generateUpsLabel()
    {
        //echo"hello";die;
        
        
        define('DS', '/');
        /*
        if ($this->credentials != 1) {
            return array('error' => array('cod' => 1, 'message' => 'Not correct registration data'), 'success' => 0);
        }*/
        /* if(is_dir($filename)){} */
        //echo"hello";die;
        $path_upsdir = $this->_dir->getPath('media') . DS . 'upslabel' . DS;
        //$path_upsdir = $this->_dir->getPath('media').'/upslabel/';
       // echo $path_upsdir; die();
        if (!is_dir($path_upsdir)) {
            mkdir($path_upsdir, 0777);
            mkdir($path_upsdir . DS . "label" . DS, 0777);
            mkdir($path_upsdir . DS . "test_xml" . DS, 0777);
        }
        //echo $path_upsdir; die();
        
        
         $shipFromDate = $this->getRequest()->getParam('shipFromDate');
            $shipToDate = $this->getRequest()->getParam('shipToDate');
            $vendorId = $this->getRequest()->getParam('vendorId');
            $storeId = $this->getRequest()->getParam('storeId');
    
            $vendorQuery = '';
            if ($vendorId != '0') {
                $vendorQuery .= "and item.vendor='$vendorId'";
            }
            if ($storeId != '') {
                $vendorQuery .= "and item.store_id='$storeId'";
            }
            
             
                           
            $queryNew =" SELECT box.box_id,box.gift_message,box.order_id,address.street,address.region,address.parent_id,
                        address.city, address.firstname, address.lastname, address.telephone, address.company, address.country_id,
                        address.postcode, sales.partner_order_id, sales.increment_id, region.code,locregion.code as vcode, 
                        box.item_id,item.name,locregion.code as locregion, item.base_row_total_incl_tax,
                        item.weight,box.status,item.vendor,item.sku,item.product_id, item.pickup_date,item.shipping_method,vend.vendor_name,
                        vend.address as vaddress, vend.city as vcity, vend.telephone as vtelephone, vend.last_name as vlast_name,
                        vend.street as vstreet, vend.state as vstate,
                        vend.zipcode as vzipcode, vend.region as vregioncode,
                        as_width.value as pwidth, as_weight.value as pweight, as_height.value as pheight, as_length.value as plength, 
                        loc.loc_name as locname, loc.fedex_ship_acc_number as locshipacc, loc.fedex_bill_acc_number as locbill, loc.loc_id as locId, 
                        loc.loc_cust_disp_name as loccustomername,loc.loc_address as locaddress,loc.loc_street as locstreet,loc.loc_city as loccity, 
                        loc.loc_postcode as locpostcode, loc.loc_phone as locphone,loc.loc_state as locstate,loc.loc_country as loccountry, item.vendor_farm, vshipping.shipping_method_name as vendorshipmethod, 
                        vshipping.shipping_code as vendorshipcode
                        FROM `order_item_box` as box
                        LEFT JOIN `sales_order_address` as address 
                        on box.order_id = address.parent_id
                        
                        LEFT JOIN `directory_country_region` as region 
                        on address.region_id = region.region_id
                        
                        LEFT JOIN sales_order as sales
                        on box.entity_id = sales.entity_id
                        
                        LEFT JOIN sales_order_item as item
                        on box.item_id = item.item_id
                        
                        LEFT JOIN vendor_shipping as vshipping
                        on vshipping.shipping_method_name = item.shipping_method
    
                        LEFT JOIN dropship_vendor_locations as loc
                        on   loc.loc_id = item.vendor_farm
    
                        LEFT JOIN catalog_product_entity_varchar as as_width 
                        on as_width.entity_id = item.product_id and as_width.attribute_id = 156
                        
                        LEFT JOIN catalog_product_entity_varchar as as_weight 
                        on as_weight.entity_id = item.product_id and as_weight.attribute_id = 157
                        
                        LEFT JOIN catalog_product_entity_varchar as as_height 
                        on as_height.entity_id = item.product_id and as_height.attribute_id = 155
                        
                        LEFT JOIN catalog_product_entity_varchar as as_length 
                        on as_length.entity_id = item.product_id and as_length.attribute_id = 158
                        
                        JOIN vendor_registrations as vend 
                         on item.vendor = vend.vendor_id
                         
                        /*LEFT JOIN `directory_country_region` as vregion 
                        on vend.region = vregion.region_id*/
                        
                        LEFT JOIN `directory_country_region` as locregion 
                        on loc.loc_state = locregion.region_id
                        
                        WHERE (item.pickup_date BETWEEN '$shipFromDate' and '$shipToDate' ) and box.status in ('printed')
                        and item.shipping_method in ('UPS Ground','UPS Next Day Air') $vendorQuery
                        and address.address_type = 'shipping'
                        and (box.lebel_status is NULL or (box.lebel_status != 'SUCCESS' and box.lebel_status != 'WARNING'))
                        ORDER by item.name ASC ";              
                        
                
                      $queryData = $this->resourceConnection->getConnection()->fetchAll($queryNew);
                      //echo"<pre>";
                      //print_r($queryData);die();
                    
           foreach($queryData as $upsData){
            
        
        $path = $this->_dir->getPath('media') . DS . 'upslabel' . DS . "label" . DS;
        $path_xml = $this->_dir->getPath('media') . DS . 'upslabel' . DS . "test_xml" . DS;
        $this->customerContext = 'felixdolittle';
        
        $company = $upsData['company'];
        $street = $upsData['street'];
        $region = $upsData['region'];
        $city = $upsData['city'];
        $firstname = $upsData['firstname'];
        $lastname = $upsData['lastname'];
        $telephone = $upsData['telephone'];
        $country_id = $upsData['country_id'];
        $postcode = $upsData['postcode'];
        $code = $upsData['code'];
        $vcode = $upsData['vcode'];
        $locregion = $upsData['locregion'];
        $base_row_total_incl_tax = $upsData['base_row_total_incl_tax'];
        $weight = $upsData['weight'];
        $shipping_method = $upsData['shipping_method'];
        $vendor_name = $upsData['vendor_name'];
        $vaddress = $upsData['vaddress'];
        $vcity = $upsData['vcity'];
        $vtelephone = $upsData['vtelephone'];
        $vlast_name = $upsData['vlast_name'];
        $vstreet = $upsData['vstreet'];
        $vstate = $upsData['vstate'];
        $vzipcode = $upsData['vzipcode'];
        $locname = $upsData['locname'];
        $loccustomername = $upsData['loccustomername'];
        $locaddress = $upsData['locaddress'];
        $locstreet = $upsData['locstreet'];
        $loccity = $upsData['loccity'];
        $locpostcode = $upsData['locpostcode'];
        $locphone = $upsData['locphone'];
        $loccountry = $upsData['loccountry'];
        $vendorshipmethod = $upsData['vendorshipmethod'];
        $vendorshipcode = $upsData['vendorshipcode'];
        
        
        $data = "<?xml version=\"1.0\"?>
        <AccessRequest xml:lang=\"en-US\">
          <AccessLicenseNumber>BD8877C2C83B4B16</AccessLicenseNumber>
          <UserId>pprakash2020</UserId>
          <Password>Fdc@9900498065</Password>
        </AccessRequest> 
        <?xml version=\"1.0\"?>
        <ShipmentConfirmRequest xml:lang=\"en-US\">
          <Request>
            <TransactionReference>
              <CustomerContext>Customer Comment</CustomerContext>
              <XpciVersion/>
            </TransactionReference>
            <RequestAction>ShipConfirm</RequestAction>
            <RequestOption>validate</RequestOption>
          </Request>
          <LabelSpecification>
            <LabelPrintMethod>
              <Code>GIF</Code>
              <Description>gif file</Description>
            </LabelPrintMethod>
            <HTTPUserAgent>Mozilla/4.5</HTTPUserAgent>
            <LabelImageFormat>
              <Code>GIF</Code>
              <Description>gif</Description>
            </LabelImageFormat>
          </LabelSpecification>
          <Shipment>
           <RateInformation>
              <NegotiatedRatesIndicator/> 
            </RateInformation> 
          <Description/>
            <Shipper>
              <Name>" .$vendor_name . "</Name>
              <PhoneNumber>". $vtelephone ."</PhoneNumber>
              <ShipperNumber>9V1041</ShipperNumber>
            <TaxIdentificationNumber>1234567877</TaxIdentificationNumber>
              <Address>
              <AddressLine1>".$locaddress. "</AddressLine1>
              <City>".$loccity."</City>
              <StateProvinceCode>".$locregion."</StateProvinceCode>
              <PostalCode>".$locpostcode."</PostalCode>
              <PostcodeExtendedLow></PostcodeExtendedLow>
              <CountryCode>".$loccountry."</CountryCode>
             </Address>
            </Shipper>
          <ShipTo>
             <CompanyName>" .$company . "</CompanyName>
              <AttentionName>" .$firstname . "</AttentionName>
              <PhoneNumber>". $telephone ."</PhoneNumber>
              <Address>
                <AddressLine1>". $street ."</AddressLine1>
                <City>". $city ."</City>
                <StateProvinceCode>". $code ."</StateProvinceCode>
                <PostalCode>". $postcode ."</PostalCode>
                <CountryCode>". $country_id ."</CountryCode>
              </Address>
            </ShipTo>
            <ShipFrom>
              <CompanyName>" .$company . "</CompanyName>
              <AttentionName>" .$vendor_name . "</AttentionName>
              <PhoneNumber>". $vtelephone ."</PhoneNumber>
            <TaxIdentificationNumber>1234567877</TaxIdentificationNumber>
              <Address>
                <AddressLine1>".$locaddress. "</AddressLine1>
                <City>".$loccity."</City>
              <StateProvinceCode>".$locregion."</StateProvinceCode>
              <PostalCode>".$locpostcode."</PostalCode>
              <CountryCode>".$loccountry."</CountryCode>
              </Address>
            </ShipFrom>
             <PaymentInformation>
              <Prepaid>
                <BillShipper>
                  <AccountNumber>9V1041</AccountNumber>
                </BillShipper>
              </Prepaid>
            </PaymentInformation>
            <Service>
              <Code>". $vendorshipcode ."</Code>
              <Description>". $shipping_method ."</Description>
            </Service>
            <Package>
              <PackagingType>
                <Code>02</Code>
                <Description>Customer Supplied</Description>
              </PackagingType>
              <Description>Package Description</Description>
            <ReferenceNumber>
              <Code>00</Code>
            <Value>Package</Value>
            </ReferenceNumber>
              <PackageWeight>
                <UnitOfMeasurement/>
                <Weight>". $weight ."</Weight>
              </PackageWeight>
              <LargePackageIndicator/>
              <AdditionalHandling>0</AdditionalHandling>
            </Package>
          </Shipment>
        </ShipmentConfirmRequest>";
       // print_r($data);die;
        $file = file_put_contents($path_xml . "ShipConfirmRequest.xml", $data);
        //return $data;
       // $cie = 'wwwcie';
        
        /*$testing = $this->scopeConfig->getValue('upslabel/testmode/testing', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (0 == $testing) {
            $cie = 'onlinetools';
        }*/
        $cie = 'onlinetools';
       // $cie = 'wwwcie';
        
        $ch = curl_init('https://' . $cie . '.ups.com/ups.app/xml/ShipConfirm');
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        $result = strstr($result, '<?xml');
       // print_r($result);die();
        /*if ($result) {
            $file = file_put_contents($path_xml . "ShipConfirmResponse.xml", $result);
        }
        return $result;*/
        
        $xml = simplexml_load_string($result);
        //print_r($xml);die('hello');
        if ($xml->Response->ResponseStatusCode[0] == 1) {
           // print_r($xml->Response->ResponseStatusCode);die('hello');
            $this->shipmentDigest = $xml->ShipmentDigest[0];
            $data = "<?xml version=\"1.0\" ?>
<AccessRequest xml:lang='en-US'>
<AccessLicenseNumber>BD8877C2C83B4B16</AccessLicenseNumber>
<UserId>pprakash2020</UserId>
<Password>Fdc@9900498065</Password>
</AccessRequest>
<?xml version=\"1.0\" ?>
<ShipmentAcceptRequest>
<Request>
<TransactionReference>
<CustomerContext>" . $this->customerContext . "</CustomerContext>
<XpciVersion>1.0001</XpciVersion>
</TransactionReference>
<RequestAction>ShipAccept</RequestAction>
</Request>
<ShipmentDigest>" . $this->shipmentDigest . "</ShipmentDigest>
</ShipmentAcceptRequest>";
            $file = file_put_contents($path_xml . "ShipAcceptRequest.xml", $data);

            $ch = curl_init('https://' . $cie . '.ups.com/ups.app/xml/ShipAccept');
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            //print_r($result);die('ggggg');
            $result = strstr($result, '<?xml');
           // print_r($result);die('dadad');
            if ($result) {
                $file = file_put_contents($path_xml . "ShipAcceptResponse.xml", $result);
            }
            $xml = simplexml_load_string($result);
           // echo"<pre>";
            //print_r($xml);die('cahl gya');
            $this->trackingNumber = $xml->ShipmentResults[0]->PackageResults[0]->TrackingNumber[0];
            $this->shipmentIdentificationNumber = $xml->ShipmentResults[0]->ShipmentIdentificationNumber[0];
            $this->graphicImage = base64_decode($xml->ShipmentResults[0]->PackageResults[0]->LabelImage[0]->GraphicImage[0]);
            
            //print_r($img);die('test');
            $file = fopen($path . 'label' . $this->trackingNumber . '.gif', 'w');
            //print_r($file);die('test');
            fwrite($file, $this->graphicImage);
            fclose($file);
            $this->htmlImage = base64_decode($xml->ShipmentResults[0]->PackageResults[0]->LabelImage[0]->HTMLImage[0]);
            $file = file_put_contents($path . $this->trackingNumber . ".html", $this->htmlImage);
            $file = file_put_contents($path_xml . "HTML_image.html", $this->htmlImage);
            //print_r($file);die('test');

            if ($this->orderPrice > 999) {
                $htmlHVReport = '<html xmlns:o="urn:schemas-microsoft-com:office:office"
                xmlns:w="urn:schemas-microsoft-com:office:word"
                xmlns="http://www.w3.org/TR/REC-html40">
                
                <head>
                <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
                <meta name=ProgId content=Word.Document>
                <meta name=Generator content="Microsoft Word 11">
                <meta name=Originator content="Microsoft Word 11">
                <link rel=File-List href="sample%20UPS%20CONTROL%20LOG_files/filelist.xml">
                <title>UPS CONTROL LOG </title>
                <!--[if gte mso 9]><xml>
                 <o:DocumentProperties>
                  <o:Author>xlm8zff</o:Author>
                  <o:LastAuthor>xlm8zff</o:LastAuthor>
                  <o:Revision>2</o:Revision>
                  <o:TotalTime>2</o:TotalTime>
                  <o:Created>2010-09-27T12:53:00Z</o:Created>
                  <o:LastSaved>2010-09-27T12:53:00Z</o:LastSaved>
                  <o:Pages>1</o:Pages>
                  <o:Words>116</o:Words>
                  <o:Characters>662</o:Characters>
                  <o:Company>UPS</o:Company>
                  <o:Lines>5</o:Lines>
                  <o:Paragraphs>1</o:Paragraphs>
                  <o:CharactersWithSpaces>777</o:CharactersWithSpaces>
                  <o:Version>11.9999</o:Version>
                 </o:DocumentProperties>
                </xml><![endif]--><!--[if gte mso 9]><xml>
                 <w:WordDocument>
                  <w:SpellingState>Clean</w:SpellingState>
                  <w:GrammarState>Clean</w:GrammarState>
                  <w:PunctuationKerning/>
                  <w:ValidateAgainstSchemas/>
                  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
                  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
                  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
                  <w:Compatibility>
                   <w:BreakWrappedTables/>
                   <w:SnapToGridInCell/>
                   <w:WrapTextWithPunct/>
                   <w:UseAsianBreakRules/>
                   <w:DontGrowAutofit/>
                  </w:Compatibility>
                  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
                 </w:WordDocument>
                </xml><![endif]--><!--[if gte mso 9]><xml>
                 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
                 </w:LatentStyles>
                </xml><![endif]-->
                <style>
                <!--
                 /* Style Definitions */
                 p.MsoNormal, li.MsoNormal, div.MsoNormal
                    {mso-style-parent:"";
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    font-size:10.0pt;
                    mso-bidi-font-size:12.0pt;
                    font-family:Arial;
                    mso-fareast-font-family:"Times New Roman";}
                span.GramE
                    {mso-style-name:"";
                    mso-gram-e:yes;}
                @page Section1
                    {size:8.5in 11.0in;
                    margin:1.0in 1.25in 1.0in 1.25in;
                    mso-header-margin:.5in;
                    mso-footer-margin:.5in;
                    mso-paper-source:0;}
                div.Section1
                    {page:Section1;}
                -->
                </style>
                <!--[if gte mso 10]>
                <style>
                 /* Style Definitions */
                 table.MsoNormalTable
                    {mso-style-name:"Table Normal";
                    mso-tstyle-rowband-size:0;
                    mso-tstyle-colband-size:0;
                    mso-style-noshow:yes;
                    mso-style-parent:"";
                    mso-padding-alt:0in 5.4pt 0in 5.4pt;
                    mso-para-margin:0in;
                    mso-para-margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    font-size:10.0pt;
                    font-family:"Times New Roman";
                    mso-ansi-language:#0400;
                    mso-fareast-language:#0400;
                    mso-bidi-language:#0400;}
                </style>
                <![endif]-->
                </head>
                <body lang=EN-US style=\'tab-interval:.5in\'>
                
                <div class=Section1>
                
                <p class=MsoNormal>UPS CONTROL <span class=GramE>LOG</span></p>
                
                <p class=MsoNormal>DATE: ' . date('d') . ' ' . date('M') . ' ' . date('Y') . ' UPS SHIPPER NO. ' . $this->shipperNumber . ' </p>
                <br />
                <br />
                <p class=MsoNormal>TRACKING # PACKAGE ID REFRENCE NUMBER DECLARED VALUE
                CURRENCY </p>
                <p class=MsoNormal>--------------------------------------------------------------------------------------------------------------------------
                </p>
                <br /><br />
                <p class=MsoNormal>' . $this->trackingNumber . ' <span class=GramE>' . $this->packagingReferenceNumberValue . ' ' . round($this->orderPrice, 2) . '</span> ' . $this->getSymbol() . ' </p>
                <br /><br />
                <p class=MsoNormal>Total Number of Declared Value Packages = 1 </p>
                <p class=MsoNormal>--------------------------------------------------------------------------------------------------------------------------
                </p>
                <br /><br />
                <p class=MsoNormal>RECEIVED BY_________________________PICKUP
                TIME__________________PKGS_______ </p>
                </div>
                </body>
                </html>';
                        file_put_contents($path . "HVR" . $this->trackingNumber . ".html", $htmlHVReport);
                      
                    }
                    return array(
                        'img_name' => 'label' . $this->trackingNumber . '.gif',
                        'digest' => '' . $this->shipmentDigest . '',
                        'trackingnumber' => '' . $this->trackingNumber . '',
                        'shipidnumber' => '' . $this->shipmentIdentificationNumber . '',
                    );
                } else {
                    $error = '<h1>Error</h1> <ul>';
                    $errorss = $xml->Response->Error[0];
                    $error .= '<li>Error Severity : ' . $errorss->ErrorSeverity . '</li>';
                    $error .= '<li>Error Code : ' . $errorss->ErrorCode . '</li>';
                    $error .= '<li>Error Description : ' . $errorss->ErrorDescription . '</li>';
                    $error .= '</ul>';
                    $error .= '<textarea>' . $result . '</textarea>';
                    $error .= '<textarea>' . $data . '</textarea>';
                    return array('error' => $error);
                    //return print_r($xml->Response->Error);
                }
                
           }
        
    }
    
    
  /* == =============================================End New Code========================================================================================================*/ 

    public function addPackageLineItem1(){
    $packageLineItem = array(
        'SequenceNumber'=>1,
        'GroupPackageCount'=>1,
        'Weight' => array(
            'Value' => 50.0,
            'Units' => 'LB'
        ),
        'Dimensions' => array(
            'Length' => 108,
            'Width' => 5,
            'Height' => 5,
            'Units' => 'IN'
        ),
    //  'SpecialServicesRequested' => addSpecialServices(),
        /*'CustomerReferences' => array(
            '0' => array(
                'CustomerReferenceType' => 'CUSTOMER_REFERENCE', // valid values CUSTOMER_REFERENCE, INVOICE_NUMBER, P_O_NUMBER and SHIPMENT_INTEGRITY
                'Value' => 'GR4567892'
            ), 
            '1' => array(
                'CustomerReferenceType' => 'INVOICE_NUMBER', 
                'Value' => 'INV4567892'
            ),
            '2' => array(
                'CustomerReferenceType' => 'P_O_NUMBER', 
                'Value' => 'PO4567892'
            )
        )*/
        
    );
    return $packageLineItem;
}
    
    
   public function addShipper(){
    $shipper = array(
        'Contact' => array(
            'PersonName' => 'Sender Name',
            'CompanyName' => 'Sender Company Name',
            'PhoneNumber' => '1234567890'
        ),
        'Address' => array(
            'StreetLines' => array('Address Line 1'),
            'City' => 'Austin',
            'StateOrProvinceCode' => 'TX',
            'PostalCode' => '73301',
            'CountryCode' => 'US'
        )
    );
    return $shipper;
}
public function addShippingChargesPayment(){
    $shippingChargesPayment = array(
        'PaymentType' => 'THIRD_PARTY',
        'Payor' => array(
            'ResponsibleParty' => array(
                'AccountNumber' => getProperty('billaccount'),
                'Contact' => null,
                'Address' => array(
                    'CountryCode' => 'US'
                )
            )
        )
    );
    return $shippingChargesPayment;
}

public function addRecipient(){
    $recipient = array(
        'Contact' => array(
            'PersonName' => 'Recipient Name',
            'CompanyName' => 'Recipient Company Name',
            'PhoneNumber' => '1234567890'
        ),
        'Address' => array(
            'StreetLines' => array('Address Line 1'),
            'City' => 'Herndon',
            'StateOrProvinceCode' => 'VA',
            'PostalCode' => '20171',
            'CountryCode' => 'US',
            'Residential' => true
        )
    );
    return $recipient;                                      
}

public function addLabelSpecification(){
    $labelSpecification = array(
        'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
        'ImageType' => 'PNG',  // valid values DPL, EPL2, PDF, ZPLII and PNG
        'LabelStockType' => 'PAPER_4X6' // default value is PAPER_7X4.75
    );
    return $labelSpecification;
}
public function addSpecialServices(){
    $specialServices = array(
        'SpecialServiceTypes' => array('COD'),
        'CodDetail' => array(
            'CodCollectionAmount' => array(
                'Currency' => 'USD', 
                'Amount' => 150
            ),
            'CollectionType' => 'ANY' // ANY, GUARANTEED_FUNDS
        )
    );
    return $specialServices; 
}
public function addPackageLineItem11(){
    $packageLineItem = array(
        'SequenceNumber'=>1,
        'GroupPackageCount'=>1,
        'Weight' => array(
            'Value' => 50.0,
            'Units' => 'LB'
        ),
        'Dimensions' => array(
            'Length' => 108,
            'Width' => 5,
            'Height' => 5,
            'Units' => 'IN'
        ),
    //  'SpecialServicesRequested' => addSpecialServices(),
        /*'CustomerReferences' => array(
            '0' => array(
                'CustomerReferenceType' => 'CUSTOMER_REFERENCE', // valid values CUSTOMER_REFERENCE, INVOICE_NUMBER, P_O_NUMBER and SHIPMENT_INTEGRITY
                'Value' => 'GR4567892'
            ), 
            '1' => array(
                'CustomerReferenceType' => 'INVOICE_NUMBER', 
                'Value' => 'INV4567892'
            ),
            '2' => array(
                'CustomerReferenceType' => 'P_O_NUMBER', 
                'Value' => 'PO4567892'
            )
        )*/
        
    );
    return $packageLineItem;
}
    
    
   
   
    /**
     * Download UPS file Template
     * @throws NoSuchEntityException
     */
    public function downloadUpsFileTemplate()
    {
        //echo"hell jee";die();
        $csvFileData = [];
        $upsHeaders = $this->getUpsFileHeaders();
        $csvFileData[] = $upsHeaders;
        $boxSql = $this->getSqlBoxesString();

        $filterQuery = "and box.status in ('pending','processing')";
        $query = "SELECT box.order_id,box.item_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql) $filterQuery";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = $itemArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
            $itemArray[] = $row['item_id'];
        }
        $orderArray = array_unique($orderArray);
        $itemArray = array_unique($itemArray);

        /**
         * Logic for most sold products item ids in order wise
         */
        $itemIdString = "'" . implode("', '", $itemArray) . "'";
        $query = "SELECT item.sku,sum(item.qty_ordered) as qty_sold
                  FROM `sales_order_item` as item
                  WHERE item.item_id in ($itemIdString)
                  GROUP by item.sku
                  ORDER by qty_sold DESC";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $skuArray = [];
        foreach ($queryResult as $qryResult) {
            $skuArray[] = $qryResult['sku'];
        }
        $skuString = "'" . implode("', '", $skuArray) . "'";

        /**
         * Get order details
         */
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $partnerId = $order->getPartnerOrderId();
            $shippingAddress = $order->getShippingAddress();
            $billingAddress = $order->getBillingAddress();
            $billingName = $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $shippingCity = $shippingAddress->getCity();
            $shippingStreet = $shippingAddress->getStreet();
            $shippingPostCode = $shippingAddress->getPostcode();
            $shippingTelephone = $shippingAddress->getTelephone();
            $shippingState = $shippingAddress->getRegionCode();

            $shippingAddressLine1 = (isset($shippingStreet[0])) ? $shippingStreet[0] : '';
            $shippingAddressLine2 = (isset($shippingStreet[1])) ? $shippingStreet[1] : '';
            $shippingAddressLine3 = (isset($shippingStreet[2])) ? $shippingStreet[2] : '';
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
            $boxOrderData[$orderId]['ship_line_1'] = $shippingAddressLine1;
            $boxOrderData[$orderId]['ship_line_2'] = $shippingAddressLine2;
            $boxOrderData[$orderId]['ship_line_3'] = $shippingAddressLine3;
            $boxOrderData[$orderId]['city'] = $shippingCity;
            $boxOrderData[$orderId]['state'] = $shippingState;

            $postCodeArray = explode("-", $shippingPostCode);
            if (count($postCodeArray) > 1) {
                $postCode = $shippingPostCode;
            } else {
                $postCode = $postCodeArray[0];
                if (strlen($postCode) < 5) {
                    $padLength = 5 - strlen($postCode);
                    $postCode = str_pad($postCode, 5, "0", STR_PAD_LEFT);
                    $postCode = "=" . $postCode;
                }
            }
            $boxOrderData[$orderId]['post_code'] = (string)$postCode;
            $boxOrderData[$orderId]['country'] = 'US';
            $boxOrderData[$orderId]['phone'] = $shippingTelephone;
            $boxOrderData[$orderId]['partner_order_id'] = $partnerId;
            $boxOrderData[$orderId]['billing_name'] = $billingName;
        }

        /**
         * Actual logic
         * Box details
         */
        $query = "SELECT box.box_id,box.order_id,box.item_id,box.status,box.gift_message,item.vendor,item.sku,item.product_id,item.name 
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  WHERE box.box_id in ($boxSql) $filterQuery
                  ORDER by FIELD(item.sku,$skuString) ";

        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $oneBox) {
            $upsBoxWiseData = [];
            $sku = $oneBox['sku'];
            $orderId = $oneBox['order_id'];
            $product = $this->categoryHelper->getProductBySku($sku);
            $productId = $product->getId();
            $productName = $product->getName();
            $boxLength = $product->getBoxLength();
            $boxWidth = $product->getBoxWidth();
            $boxHeight = $product->getBoxHeight();
            $boxWeight = $product->getBoxWeight();
            $poPrice = $product->getPoPrice();
            $group = $product->getGroup();
            $attr = $product->getResource()->getAttribute('group');
            $groupName = '';
            if ($attr->usesSource()) {
                $groupName = $attr->getSource()->getOptionText($group);
            }
            $giftMessage = (trim($oneBox['gift_message']) == '') ?
                $boxOrderData[$orderId]['billing_name'] : $oneBox['gift_message'];

            $giftMessage = str_replace("?", "", $giftMessage);
            $upsBoxWiseData = [
                $boxOrderData[$orderId]['ship_name'],
                $boxOrderData[$orderId]['ship_name'],
                $boxOrderData[$orderId]['ship_line_1'],
                $boxOrderData[$orderId]['ship_line_2'],
                $boxOrderData[$orderId]['ship_line_3'],
                $boxOrderData[$orderId]['city'],
                $boxOrderData[$orderId]['state'],
                $boxOrderData[$orderId]['post_code'],
                $boxOrderData[$orderId]['country'],
                $boxOrderData[$orderId]['phone'],
                $giftMessage,
                $boxOrderData[$orderId]['partner_order_id'],
                $productName,
                '1',
                $productName,
                'Fresh Flowers',
                $boxLength,
                $boxWidth,
                $boxHeight,
                $boxWeight,
                $poPrice,
                'ES',
                'CP',
                $groupName,
                'USD',
                'CO',
                'Box',
                'Isha Flowers',
                'Zach Hemple',
                '4862 Cadiz Circle',
                'Palm Beach Gardens',
                'FL',
                'US',
                '33418',
                '8562819187',
                '85086E'
            ];
            $csvFileData[] = $upsBoxWiseData;
        }

        /**
         * Download file
         */
         
        $fileName = "ups_file_template.csv";
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        $fp = fopen('php://output', 'w');
        foreach ($csvFileData as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
        exit();

    }

    /**
         * Download file
         */

    public function downloadTrackFileTemplate()
    {
        
        $data = [];
        $fileName = 'track_id_upload_template.csv';
        $data[] = ['box_id', 'sku', 'name', 'track_id', 'shipping_name', 'order number', 'partner_order_id'];

        $boxSql = $this->getSqlBoxesString();

        /**
         * Get distinct order ids
         */
        $query = "SELECT DISTINCT box.order_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
        }
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $shippingAddress = $order->getShippingAddress();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
        }

        /**
         * Box data
         */
        $query = "SELECT box.box_id,item.sku,item.name,box.track_id,box.order_id,ord.increment_id,ord.partner_order_id
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item on box.item_id = item.item_id
                  LEFT JOIN sales_order as ord on box.order_id = ord.entity_id
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $key => $result) {
            $queryData = [];
            foreach ($result as $index => $value) {
                if ($index == 'order_id') {
                    $value = $boxOrderData[$value]['ship_name'];
                }
                $queryData[] = $value;
            }
            $data[] = $queryData;
        }
        
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $fp = fopen('php://output', 'w');
        foreach ($data as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
        exit();
    }

    /**
     * Download Track Id upload Template
     */
    
    /**
     * Return Headers
     *
     * @return array
     */
    public function getBoxHeaders()
    {
        return [
            'Box Id',
            'Item Id',
            'Label Status',
            'Error Message',
            'Box Status',
            'Sku',
            'Product Name',
            'Vendor Name',
            'Track id',
            'Pickup Date',
            'Delivery Date',
            'Gift Message',
        ];
    }

    /**
     * Return Ups File Array
     *
     * @return array
     */
    public function getUpsFileHeaders()
    {
        return [
            'Companyshipto',
            'contactshipto',
            'address1shipto',
            'address2shipto',
            'address3shipto',
            'cityshipto',
            'stateshipto',
            'zipshipto',
            'Countryshipto',
            'phoneshipto',
            'Message',
            'Reference1',
            'Reference2',
            'Quantity',
            'Item',
            'ProdDesc',
            'Length',
            'width',
            'height',
            'WeightKg',
            'DclValue',
            'Service',
            'PkgType',
            'GenDesc',
            'Currency',
            'Origin',
            'UOM',
            'TPComp',
            'TPAttn',
            'TPAdd1',
            'TPCity',
            'TPState',
            'TPCtry',
            'TPZip',
            'TPPhone',
            'TPAcct'
        ];
    }

    /**
     * Get Sql Query
     * @return string
     */
    public function getSqlBoxesString()
    {
        $boxArray = explode(",", $this->_boxes);
        return "'" . implode("', '", $boxArray) . "'";
    }

    /**
     * Return Order object by ids
     * @param $orderArray
     * @return Collection
     */
    public function getOrderCollectionByIds($orderArray)
    {
        return $this->orderHelper->getOrderCollectionByIds($orderArray);
    }

    /**
     * Download Label file Template with Box Id and Box status
     * @throws NoSuchEntityException
     */
    public function downloadAllLebelPdfTemplate()
    {

        $shipFromDate = $this->getRequest()->getParam('shipFromDate');
        $shipToDate = $this->getRequest()->getParam('shipToDate');
        $vendorId = $this->getRequest()->getParam('vendorId');
        $storeId = $this->getRequest()->getParam('storeId');

        $vendorQuery = '';
        if ($vendorId != '0') {
            $vendorQuery .= "and item.vendor='$vendorId'";
        }
        if ($storeId != '') {
            $vendorQuery .= "and item.store_id='$storeId'";
        }

        $query = "SELECT box.box_id, address.firstname, address.lastname, box.gift_message, box.order_id,box.ImageUrl
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN vendor_registrations as vend 
                  on item.vendor = vend.vendor_id
                  LEFT JOIN sales_order_address as address
                  on box.order_id = address.parent_id
                  WHERE box.status in ('printed') /* and (item.pickup_date BETWEEN '$shipFromDate' and '$shipToDate' ) */ and address.address_type in('billing') 
                  and item.shipping_method in ('FEDEX_GROUND','STANDARD_OVERNIGHT','PRIORITY_OVERNIGHT') $vendorQuery 
                  and  (box.lebel_status = 'SUCCESS' or box.lebel_status = 'WARNING') 
                  ORDER by item.name ASC";
                  $result= $this->resourceConnection->getConnection()->fetchAll($query);
                 //echo "<pre>";
                 // print_r($result);die('test');
                  $Img = array();
                  $pdf = new \FPDF();             
                    foreach ($result as $v) {
                                
                                $pdf->AddPage();
                                $pdf->Image($v['ImageUrl'],20,60,180,200,'PNG');
                                if($v['gift_message']!== NULL){
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100);
                                $pdf->Multicell(0,10, $v['gift_message'],0,'C');//0, true, 'R'
                                }else{
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100); 
                                $pdf->Multicell(0,10, $v['firstname'].' '.$v['lastname'],0,'C');
                                }
                       
                             }
                                $pdf->Output();
                        }

    
    public function downloadZebraLebelPdfTemplate()
    {
        
        //echo"fdfdf";die();
        $shipFromDate = $this->getRequest()->getParam('shipFromDate');
        $shipToDate = $this->getRequest()->getParam('shipToDate');
        $vendorId = $this->getRequest()->getParam('vendorId');
        $storeId = $this->getRequest()->getParam('storeId');

        $vendorQuery = '';
        if ($vendorId != '0') {
            $vendorQuery .= "and item.vendor='$vendorId'";
        }
        if ($storeId != '') {
            $vendorQuery .= "and item.store_id='$storeId'";
        }

        $query = "SELECT box.box_id, address.firstname, address.lastname, box.gift_message,box.order_id,box.ImageUrl
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN vendor_registrations as vend 
                  on item.vendor = vend.vendor_id
                  LEFT JOIN sales_order_address as address
                  on box.order_id = address.parent_id
                  WHERE box.status in ('printed') and address.address_type in('billing') and item.shipping_method in ('FEDEX_GROUND','STANDARD_OVERNIGHT','PRIORITY_OVERNIGHT','Fedex')
                  $vendorQuery and  (box.lebel_status = 'SUCCESS' or box.lebel_status = 'WARNING') 
                  ORDER by item.name ASC";
                  $result = $this->resourceConnection->getConnection()->fetchAll($query);
                 // echo"<pre>";
                 // print_r($result);die('hello sanoj');
                  $Img = array();
                  $pdf = new \FPDF();             
                    foreach ($result as $v) {
                                
                                $pdf->AddPage();
                                $pdf->Image($v['ImageUrl'],20,60,180,200,'PNG');
                                if($v['gift_message']!== NULL){
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100);
                                $pdf->Multicell(0,10, $v['gift_message'],0,'C');
                                }else{
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100);
                                $pdf->Multicell(0,10, $v['firstname'].' '.$v['lastname'],0,'C');
                                }
                       
                             }
                                $pdf->Output();
                        }
                                
             /**
             * Download UPS file Template with Box Id and Box status
             * @throws NoSuchEntityException
             */
            public function downloadAllShipTemplate(){
                
                $data = [];
                $fileName = 'ship_method_template.csv';
                $data[] = ['box_id','track_id', 'th_activity', 'itemvendorId', 'locationpostcode', 'partner_order_id', 'postcode','increment_id','box_status','shipping_method','sku'];
                
                $shipFromDate = $this->getRequest()->getParam('shipFromDate');
                $shipToDate = $this->getRequest()->getParam('shipToDate');
                $vendorId = $this->getRequest()->getParam('vendorId');
                $storeId = $this->getRequest()->getParam('storeId');
        
                $vendorQuery = '';
                if ($vendorId != '0') {
                    $vendorQuery .= "and item.vendor='$vendorId'";
                }
                if ($storeId != '') {
                    $vendorQuery .= "and item.store_id='$storeId'";
                }
        
                
                $query = "SELECT box.box_id, box.track_id,track.th_activity, item.vendor as itemvendorId, location.loc_postcode as locationpostcode,
                sales.partner_order_id, address.postcode, sales.increment_id,box.status as box_status,item.shipping_method,item.sku
                          FROM `order_item_box` as box
        
                          LEFT JOIN sales_order_item as item 
                           on box.item_id = item.item_id
        
                           LEFT JOIN sales_order as sales
                            on box.entity_id = sales.entity_id
        
                           LEFT JOIN `track_history` as track 
                          on box.box_id = track.th_box_id 
        
                          LEFT JOIN vendor_registrations as vend 
                          on item.vendor = vend.vendor_id
                          
                          LEFT JOIN dropship_vendor_locations as location 
                          on item.vendor = location.vendor_id
        
                          LEFT JOIN sales_order_address as address
                          on box.order_id = address.parent_id
        
                          WHERE box.status in ('printed')  and address.address_type in('billing') 
                          and item.shipping_method in ('STANDARD_OVERNIGHT','PRIORITY_OVERNIGHT') $vendorQuery 
                          and  (box.lebel_status = 'SUCCESS' or box.lebel_status = 'WARNING') 
                          ORDER by item.name ASC";          
                          $result= $this->resourceConnection->getConnection()->fetchAll($query);
                          //echo "<pre>";
                         //print_r($result);die('test');
                         
                         foreach ($result as  $value) {
                               # code...
                        $data[] = $value;
                     }
                     
        
                    header('Content-Type: application/csv; charset=UTF-8');
                    header('Content-Disposition: attachment; filename="' . $fileName . '"');
                    $fp = fopen('php://output', 'w');
                    foreach ($data as $row) {
                        fputcsv($fp, $row);
                    }
                    fclose($fp);
                    exit();  

    
            }       

        
}

