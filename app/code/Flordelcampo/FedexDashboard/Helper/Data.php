<?php

namespace Flordelcampo\FedexDashboard\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;


/**
 * Class Data
 *
 * Flordelcampo\Dashboards\Helper
 */
class Data extends AbstractHelper
{

    /**
     * Data constructor.
     * @param Context $context
     * @param AddressFactory $upsAddressFactory
     */
    public function __construct(
        Context $context
        
    ) {
        parent::__construct($context);
    }
}
