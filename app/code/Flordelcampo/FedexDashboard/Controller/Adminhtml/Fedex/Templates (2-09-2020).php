<?php

namespace Flordelcampo\FedexDashboard\Controller\Adminhtml\Fedex;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordelcampo\Catalog\Helper\Data as CategoryHelper;
use Magento\Sales\Model\ResourceModel\Order\Collection;

require_once('lib/PDFlebel/fedex-common.php');
require_once('lib/fpdf182/fpdf.php');

/**
 * Class Templates
 * Flordelcampo\Dashboards\Controller\Adminhtml\Ups
 */
class Templates extends Action
{
    /**
     * @var null
     */
    private $_boxes;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_params;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var CategoryHelper
     */
    protected $categoryHelper;
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;
    protected $dir;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param OrderHelper $orderHelper
     * @param CategoryHelper $categoryHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        OrderHelper $orderHelper,
        CategoryHelper $categoryHelper,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem\DirectoryList $dir
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->fileFactory = $fileFactory;
        $this->orderHelper = $orderHelper;
        $this->categoryHelper = $categoryHelper;
        $this->_dir = $dir;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        

        $resultPage = $this->resultPageFactory->create();
        $boxes = $this->getRequest()->getParam('boxes');
        $this->_boxes = $boxes;
        $templateType = $this->getRequest()->getParam('type');
        switch ($templateType) {
            case 'box_details':
                $this->showBoxDetails();
                break;
            case 'box_details1':
                $this->downloadSkuWisePdfFile();
                break;    
            case 'gift':
                $this->downloadGiftTemplate();
                break;
            case 'ups_file':
                $this->downloadFedexLebelTemplateTest();
                break;
            case 'track_file':
                $this->downloadUpsFileTemplate();
                break;
            
            case 'all_boxes':
                $this->downloadAllLebelPdfTemplate(); 
                break;
            case 'all_boxes1':
                $this->downloadZebraLebelPdfTemplate(); 
                break;    
            default:
        }
        exit();
        //return $resultPage;
    }


    
    public function downloadSkuWisePdfFile()
        {

            $boxSql = $this->getSqlBoxesString();
            $query = "SELECT box.box_id,box.item_id, address.firstname, address.lastname,
            box.lebel_status,box.lebel_error,box.ImageUrl,box.status,item.sku, item.order_id,
            item.name,vend.vendor_name,box.track_id,item.pickup_date,item.delivery_date,box.gift_message
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN sales_order_address as address
                  on box.order_id = address.parent_id
                  LEFT JOIN vendor_registrations as vend on item.vendor = vend.vendor_id
                  WHERE box.box_id in ($boxSql)  and address.address_type in ('billing')
                  ORDER by item.name ASC";
            $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
            $pdf = new \FPDF();             
                    foreach ($queryResult as $v) {
                                
                                $pdf->AddPage();
                                $pdf->Image($v['ImageUrl'],20,60,180,200,'PNG');
                                if($v['gift_message']!== NULL){
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100);
                                $pdf->Multicell(0,10, $v['gift_message'],0,'C');
                                }else{
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100); 
                                $pdf->Multicell(0,10, $v['firstname'].' '.$v['lastname'],0,'C');
                                }
                       
                             }
                                $pdf->Output();
              }

    /**
     * Show Box Details
     */
    public function showBoxDetails()
    {
        $boxSql = $this->getSqlBoxesString();
        $query = "SELECT box.box_id,box.item_id,box.lebel_status,box.lebel_error,box.status,item.sku,item.name,vend.vendor_name,box.track_id,item.pickup_date,item.delivery_date,box.gift_message
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN vendor_registrations as vend on item.vendor = vend.vendor_id
                  WHERE box.box_id in ($boxSql) /* and box.lebel_status = 'SUCCESS' */
                  ORDER by item.name ASC";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        //print_r($queryResult);die();


        /**
         * SKU wise
         */
        $skuWiseArray = [];
        $totalCount = 0;
        foreach ($queryResult as $key => $result) {
            if (isset($skuWiseArray[$result['sku']])) {
                $skuWiseArray[$result['sku']]['count'] += 1;
                //$skuWiseArray[$result['lebel_status']]['count'] += 1;
            } else {
                $skuWiseArray[$result['sku']] = [
                    'sku' => $result['sku'],
                    'name' => $result['name'],
                    'lebel' => $result['lebel_status'],
                    'count' => 1];
                    
            }
            $totalCount++;

        }
        /**
         * Box wise HTML
         */
        echo "<h2>SKU Summary</h2>";
        $html = "<table border='2'><tbody>";
        $html .= "<tr>";
        $html .= "<th>SKU</th>";
        $html .= "<th>Product Name</th>";
        $html .= "<th>Label Status</th>";
        $html .= "<th>Box Count</th>";
        $html .= "</tr>";
        foreach ($skuWiseArray as $sku => $result) {
            $html .= "<tr>";
            foreach ($result as $key => $value) {
                $html .= "<td class='value'>" . $value . "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody>";
        $html .= "<tfoot>";
        $html .= "<tr>";
        $html .= "<td colspan='3'>Total</td>";
        $html .= "<td>".$totalCount."</td>";
        $html .= "</tr>";
        $html .= "</tfoot>";

        $html .= "</table>";
        $html .= "</br>";
        echo $html;
        $headers = $this->getBoxHeaders();
        echo "<h2>Box Details</h2>";
        $html = "<table border='2'><tbody>";
        $html .= "<tr>";
        foreach ($headers as $colName) {
            $html .= "<th>$colName</th>";
        }
        $html .= "</tr>";
        foreach ($queryResult as $key => $result) {
            $html .= "<tr>";
            foreach ($result as $key => $value) {
                $html .= "<td class='value'>" . $value . "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody></table>";
        echo $html;
        exit();
    }

    /**
     * Download Gift template
     */
    public function downloadGiftTemplate()
    {

        $data = [];
        $fileName = 'gift_message_upload_template.csv';
        $data[] = ['box_id', 'sku', 'name', 'gift_message', 'shipping_name', 'order number', 'partner_order_id'];

        $boxSql = $this->getSqlBoxesString();

        /**
         * Get distinct order ids
         */
        $query = "SELECT DISTINCT box.order_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
        }
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $shippingAddress = $order->getShippingAddress();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
        }

        /**
         * Box data
         */
        $query = "SELECT box.box_id,item.sku,item.name,box.gift_message,box.order_id,ord.increment_id,ord.partner_order_id
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item on box.item_id = item.item_id
                  LEFT JOIN sales_order as ord on box.order_id = ord.entity_id
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $key => $result) {
            $queryData = [];
            foreach ($result as $index => $value) {
                if ($index == 'order_id') {
                    $value = $boxOrderData[$value]['ship_name'];
                }
                $queryData[] = $value;
            }
            $data[] = $queryData;
        }
        
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $fp = fopen('php://output', 'w');
        foreach ($data as $row) {
            fputcsv($fp, $row);
        }
        
        fclose($fp);
        exit();
    }
    
    /*New Sonoj Code----------------------------------------------------------------------------------------------------------*/
    

    public function downloadFedexLebelTemplateTest(){
        echo"hello";die('ttt');
        $path_to_wsdl = $this->_dir->getPath('lib_internal').'/wsdl/ShipService_v15.wsdl';
        ini_set("soap.wsdl_cache_enabled", "0");
        
        $client = new \SoapClient($path_to_wsdl, ['trace' => true]);
        
                $request['WebAuthenticationDetail'] = array(
            'UserCredential' =>array(
                'Key' => getProperty('key'), 
                'Password' => getProperty('password')
            )
        );
        $request['ClientDetail'] = array(
            'AccountNumber' => getProperty('shipaccount'), 
            'MeterNumber' => getProperty('meter')
        );
        $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Ground Domestic Shipping Request using PHP ***');
        $request['Version'] = array(
            'ServiceId' => 'ship', 
            'Major' => '15', 
            'Intermediate' => '0', 
            'Minor' => '0'
        );


        $shipFromDate = $this->getRequest()->getParam('shipFromDate');
        $shipToDate = $this->getRequest()->getParam('shipToDate');
        $vendorId = $this->getRequest()->getParam('vendorId');
        $storeId = $this->getRequest()->getParam('storeId');

        $vendorQuery = '';
        if ($vendorId != '0') {
            $vendorQuery .= "and item.vendor='$vendorId'";
        }
        if ($storeId != '') {
            $vendorQuery .= "and item.store_id='$storeId'";
        }
        
             $query =" SELECT box.box_id,box.gift_message,box.order_id,address.street,address.region,
                    address.city, address.firstname, address.lastname, address.company, address.country_id,
                    address.postcode, sales.partner_order_id, region.code,vregion.code as vcode, box.item_id,item.name, item.base_row_total_incl_tax,
                    item.weight,box.status,item.vendor,item.sku,item.product_id, item.pickup_date,item.shipping_method,vend.vendor_name,
                    vend.address as vaddress, vend.city as vcity, vend.telephone as vtelephone, vend.last_name as vlast_name,
                    vend.street as vstreet, vend.state as vstate,
                    vend.zipcode as vzipcode, vend.region as vregioncode,
                    as_width.value as pwidth, as_weight.value as pweight, as_height.value as pheight, as_length.value as plength
                    FROM `order_item_box` as box
                    LEFT JOIN `sales_order_address` as address on box.order_id = address.parent_id
                    LEFT JOIN `directory_country_region` as region 
                    on address.region_id = region.region_id
                    LEFT JOIN sales_order as sales
                    on box.entity_id = sales.entity_id
                    LEFT JOIN sales_order_item as item
                    on box.item_id = item.item_id
                    LEFT JOIN catalog_product_entity_varchar as as_width on as_width.entity_id = item.product_id and as_width.attribute_id = 156
                    LEFT JOIN catalog_product_entity_varchar as as_weight on as_weight.entity_id = item.product_id and as_weight.attribute_id = 157
                    LEFT JOIN catalog_product_entity_varchar as as_height on as_height.entity_id = item.product_id and as_height.attribute_id = 155
                    LEFT JOIN catalog_product_entity_varchar as as_length on as_length.entity_id = item.product_id and as_length.attribute_id = 158
                     JOIN vendor_registrations as vend on item.vendor = vend.vendor_id
                     LEFT JOIN `directory_country_region` as vregion 
                    on vend.region = vregion.region_id
                    WHERE (item.pickup_date BETWEEN '$shipFromDate' and '$shipToDate' ) and box.status in ('printed')
                    and item.shipping_method in ('FEDEX_GROUND','PRIORITY_OVERNIGHT','STANDARD_OVERNIGHT','Fedex') $vendorQuery
                    and address.address_type = 'shipping' and (box.lebel_status is NULL or (box.lebel_status != 'SUCCESS' and box.lebel_status != 'WARNING'))
                    ORDER by item.name ASC ";

                  
                  $result = $this->resourceConnection->getConnection()->fetchAll($query);
                  //echo"<pre>";
                  //print_r($result);die();
                
               $pickup_date = date('c');
                foreach ($result as $value) {
                  

        $request['RequestedShipment'] = array(
            'ShipTimestamp' => $pickup_date,
            'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
            'ServiceType' =>  $value['shipping_method'], //'FEDEX_GROUND', // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
            'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
            /*'TotalWeight' => array(
                'Value' => 50.0, 
                'Units' => 'LB' // valid values LB and KG
            ),*/
            
            //'Recipient' =>$this-> addShipper(),
            'Recipient' => array(
                            'Contact' => array(
                                'PersonName' => $value['firstname'].' '.$value['lastname'], //'Sender Name Sanoj Yadav2',
                                //'CompanyName' => 'Order No '.$value['order_id'],
                                'CompanyName' =>$value['company'],
                                'PhoneNumber' => '1234567890'
                            ),
                            'Address' => array(
                                'StreetLines' => array($value['street']),
                                'City' => $value['city'],
                                'StateOrProvinceCode' =>$value['code'],
                                'PostalCode' => $value['postcode'],
                                'CountryCode' => $value['country_id']
                            )
                           
                        ),
            'Shipper' => array(
                            'Contact' => array(
                                'PersonName' => $value['vendor_name'].' '.$value['vlast_name'], //'Sender Name Sanoj Yadav2',
                                'CompanyName' => $value['company'],
                                //'Telephone' => $value['vtelephone']
                                'PhoneNumber' =>$value['vtelephone'] //'1234567890'
                            ),
                            'Address' => array(
                                'StreetLines' => array($value['vaddress']),
                                'City' => $value['vcity'],
                                //'State' => $value['vstate'],
                                'StateOrProvinceCode' => $value['vcode'],
                                'PostalCode' => $value['vzipcode'],
                                'CountryCode' =>$value['country_id']
                            )
                           
                        ),
            //'Shipper' =>$this-> addShipper(),
            'ShippingChargesPayment' => $this->addShippingChargesPayment(),
            //'SpecialServicesRequested' => addSpecialServices(),
            'LabelSpecification' =>$this-> addLabelSpecification(), 
            'PackageCount' => 1,                                       
            'RequestedPackageLineItems' => array(
                '0' =>array(
                'SequenceNumber'=>1,
                'GroupPackageCount'=>1,
                'Weight' => array(
                    'Value' =>$value['pweight'], //20.0,
                    'Units' => 'LB'
                ),
                'Dimensions' => array(
                    'Length' => $value['plength'],//105,
                    'Width' => $value['pwidth'],//5,
                    'Height' => $value['pheight'],//5,
                    'Units' => 'IN'
                )
            )

         ),
           
       );
       
       
       /*static code=================================================================================================================*/
       /*
       
         $request['RequestedShipment'] = array(
            'ShipTimestamp' => $pickup_date,
            'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
            'ServiceType' => 'FEDEX_GROUND',// $value['shipping_method'], //'FEDEX_GROUND', // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
            'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
            /*'TotalWeight' => array(
                'Value' => 50.0, 
                'Units' => 'LB' // valid values LB and KG
            ),*/
            
            //'Recipient' =>$this-> addShipper(),
          /*  'Recipient' => array(
                            'Contact' => array(
                                'PersonName' => 'Sender Name Sanoj Yadav2',
                                
                                'CompanyName' =>'Sender Company Name', 
                                'PhoneNumber' => '1234567890'
                            ),
                            'Address' => array(
                                'StreetLines' => array('Address Line 1'),
                                'City' => 'Austin',
                                'StateOrProvinceCode' =>'TX', 
                                'PostalCode' => '73301',
                                'CountryCode' => 'US',
                            )
                           
                        ),
            'Shipper' => array(
                            'Contact' => array(
                                'PersonName' => 'Recipient Name',
                                'CompanyName' =>'Recipient Name',
                                
                                'PhoneNumber' =>'Recipient Name',
                            ),
                            'Address' => array(
                                'StreetLines' => array('dsdsds'),
                                'City' => 'test',
                                
                                'StateOrProvinceCode' =>'test',
                                'PostalCode' => 'test', 
                                'CountryCode' =>'test',
                            )
                           
                        ),*/
            //'Shipper' =>$this-> addShipper(),
            /*'ShippingChargesPayment' => $this->addShippingChargesPayment(),
            //'SpecialServicesRequested' => addSpecialServices(),
            'LabelSpecification' =>$this-> addLabelSpecification(), 
            'PackageCount' => 1,                                       
            'RequestedPackageLineItems' => array(
                '0' =>array(
                'SequenceNumber'=>1,
                'GroupPackageCount'=>1,
                'Weight' => array(
                    'Value' =>20.0,//$value['pweight'], //20.0,
                    'Units' => 'LB'
                ),
                'Dimensions' => array(
                    'Length' => 105,//$value['plength'],//105,
                    'Width' => 5,//$value['pwidth'],//5,
                    'Height' =>5,// $value['pheight'],//5,
                    'Units' => 'IN'
                )
            )

         ),
           
       );*/
       
       /*end====================================================================================================================*/
                                                                                                                               
        try {
           // echo"<pre>";
            //print_r($request);die('helll');
            // use client to call soap method
            if(setEndpoint('changeEndpoint')){
        $newLocation = $client->__setLocation(setEndpoint('endpoint'));
    }
    $response = $client->processShipment($request); // FedEx web service invocation
   // echo '<pre>';print_r($response);die('hikkkk');
    
    if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR'){
      
            
            $rand = rand(10,100);
            $path = 'pub/media/LebelImage/'.$response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber.'_'.$rand.'shipextlabel.png';
           // printSuccess($client, $response);
           // echo "<pre>";
           // echo $response->Notifications->Severity. '  ';
           // print_r($response); die('test');
         
            $nul ='NULL';
            if($response->HighestSeverity =='SUCCESS'){
                echo"success";
               // print_r($response);die();
                $update_query = "update order_item_box set track_id ='".$response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber."',lebel_error ='".$nul."',lebel_status ='".$response->Notifications->Severity."', ImageUrl = '".$path."' WHERE order_id = '".$value['order_id']."'";
            }else{
                //print_r($response);die();
                if($response->HighestSeverity =='WARNING'){
                    $update_query = "update order_item_box set track_id ='".$response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber."',lebel_error ='".$response->Notifications[0]->Message."',lebel_status ='".$response->Notifications[0]->Severity."', ImageUrl = '".$path."' WHERE order_id = '".$value['order_id']."'";

                }else{

                 $update_query = "update order_item_box set track_id ='".$response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber."',lebel_error ='".$response->Notifications[0]->Message."',lebel_status ='".$response->Notifications[0]->Severity."', ImageUrl = '".$path."' WHERE order_id = '".$value['order_id']."'";
                }

            }

                $this->resourceConnection->getConnection()->query($update_query);

                $fp = fopen($path, 'wb');   
                //echo $fp; die('jjjjj');
            fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
            fclose($fp);
            echo"<pre>";
               print_r($response);die('kkkkkk');//. '  ';
            echo ' '.$response->Notifications->Severity.' order id- '.$value['order_id'].' - '.$response->Notifications->Message."<br>";

            
             
        }else{
            printError($client, $response);//echo"dekho yaaayuu";
            //print_r($response);die('dekho sanoj mai eeror hu');
            if(is_array($response->Notifications)){
                echo ' '.$response->HighestSeverity.' order id-'.$value['order_id'].' - '.$response->Notifications[0]->Message."<br>";
            
            $update_query1 = "update order_item_box set lebel_error ='".$response->Notifications[0]->Message."',lebel_status ='".$response->Notifications[0]->Severity."' WHERE order_id = '".$value['order_id']."'";
            } else{
                //print_r($response);die();
                echo ' '.$response->HighestSeverity.' order id-'.$value['order_id'].' - '.$response->Notifications->Message."<br>";
            
            $update_query1 = "update order_item_box set lebel_error ='".$response->Notifications->Message."',lebel_status ='".$response->Notifications->Severity."' WHERE order_id = '".$value['order_id']."'";
            }
            $this->resourceConnection->getConnection()->query($update_query1);
        }


        } catch (\SoapFault $e) {
            // handle exception message
        }
        
    }
    }
    
    
    /*==================================================================================================================*/ 

    public function addPackageLineItem1(){
	$packageLineItem = array(
		'SequenceNumber'=>1,
		'GroupPackageCount'=>1,
		'Weight' => array(
			'Value' => 50.0,
			'Units' => 'LB'
		),
		'Dimensions' => array(
			'Length' => 108,
			'Width' => 5,
			'Height' => 5,
			'Units' => 'IN'
		),
	//	'SpecialServicesRequested' => addSpecialServices(),
		/*'CustomerReferences' => array(
			'0' => array(
				'CustomerReferenceType' => 'CUSTOMER_REFERENCE', // valid values CUSTOMER_REFERENCE, INVOICE_NUMBER, P_O_NUMBER and SHIPMENT_INTEGRITY
				'Value' => 'GR4567892'
			), 
			'1' => array(
				'CustomerReferenceType' => 'INVOICE_NUMBER', 
				'Value' => 'INV4567892'
			),
			'2' => array(
				'CustomerReferenceType' => 'P_O_NUMBER', 
				'Value' => 'PO4567892'
			)
		)*/
		
	);
	return $packageLineItem;
}
    
    
   public function addShipper(){
	$shipper = array(
		'Contact' => array(
			'PersonName' => 'Sender Name',
			'CompanyName' => 'Sender Company Name',
			'PhoneNumber' => '1234567890'
		),
		'Address' => array(
			'StreetLines' => array('Address Line 1'),
			'City' => 'Austin',
			'StateOrProvinceCode' => 'TX',
			'PostalCode' => '73301',
			'CountryCode' => 'US'
		)
	);
	return $shipper;
}
public function addShippingChargesPayment(){
	$shippingChargesPayment = array(
		'PaymentType' => 'SENDER',
        'Payor' => array(
			'ResponsibleParty' => array(
				'AccountNumber' => getProperty('billaccount'),
				'Contact' => null,
				'Address' => array(
					'CountryCode' => 'US'
				)
			)
		)
	);
	return $shippingChargesPayment;
}

public function addRecipient(){
	$recipient = array(
		'Contact' => array(
			'PersonName' => 'Recipient Name',
			'CompanyName' => 'Recipient Company Name',
			'PhoneNumber' => '1234567890'
		),
		'Address' => array(
			'StreetLines' => array('Address Line 1'),
			'City' => 'Herndon',
			'StateOrProvinceCode' => 'VA',
			'PostalCode' => '20171',
			'CountryCode' => 'US',
			'Residential' => true
		)
	);
	return $recipient;	                                    
}

public function addLabelSpecification(){
	$labelSpecification = array(
		'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
		'ImageType' => 'PNG',  // valid values DPL, EPL2, PDF, ZPLII and PNG
		'LabelStockType' => 'PAPER_4X6' // default value is PAPER_7X4.75
	);
	return $labelSpecification;
}
public function addSpecialServices(){
	$specialServices = array(
		'SpecialServiceTypes' => array('COD'),
		'CodDetail' => array(
			'CodCollectionAmount' => array(
				'Currency' => 'USD', 
				'Amount' => 150
			),
			'CollectionType' => 'ANY' // ANY, GUARANTEED_FUNDS
		)
	);
	return $specialServices; 
}
public function addPackageLineItem11(){
	$packageLineItem = array(
		'SequenceNumber'=>1,
		'GroupPackageCount'=>1,
		'Weight' => array(
			'Value' => 50.0,
			'Units' => 'LB'
		),
		'Dimensions' => array(
			'Length' => 108,
			'Width' => 5,
			'Height' => 5,
			'Units' => 'IN'
		),
	//	'SpecialServicesRequested' => addSpecialServices(),
		/*'CustomerReferences' => array(
			'0' => array(
				'CustomerReferenceType' => 'CUSTOMER_REFERENCE', // valid values CUSTOMER_REFERENCE, INVOICE_NUMBER, P_O_NUMBER and SHIPMENT_INTEGRITY
				'Value' => 'GR4567892'
			), 
			'1' => array(
				'CustomerReferenceType' => 'INVOICE_NUMBER', 
				'Value' => 'INV4567892'
			),
			'2' => array(
				'CustomerReferenceType' => 'P_O_NUMBER', 
				'Value' => 'PO4567892'
			)
		)*/
		
	);
	return $packageLineItem;
}
    
    
   
    public function downloadFedexLebelTemplate(){
        


        /** @var Page $resultPage */
        $path_to_wsdl = $this->_dir->getPath('lib_internal').'/wsdl/ShipService_v15.wsdl';
        //$test = $this->_dir->getRoot();
        //echo $test;die();
        
        //define('SHIP_LABEL', 'pub/media/LebelImage/shipextlabel.png');  // PDF label file. Change to file-extension .png for creating a PNG label (e.g. shiplabel.png)
        
        define('SHIP_CODLABEL', 'CODgroundreturnlabel.png');  // PDF label file. Change to file-extension ..png for creating a PNG label (e.g. CODgroundreturnlabel.png)
        //$lebelImage = $this->_dir->getRoot();
        

        ini_set("soap.wsdl_cache_enabled", "0");


        $client = new \SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
        
        
        $request['WebAuthenticationDetail'] = array(
            'UserCredential' =>array(
                'Key' => getProperty('key'), 
                'Password' => getProperty('password')
            )
        );
        $request['ClientDetail'] = array(
            'AccountNumber' => getProperty('shipaccount'), 
            'MeterNumber' => getProperty('meter')
        );
        $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Ground Domestic Shipping Request using PHP ***');
        $request['Version'] = array(
            'ServiceId' => 'ship', 
            'Major' => '15', 
            'Intermediate' => '0', 
            'Minor' => '0'
        );

//new code
        $shipFromDate = $this->getRequest()->getParam('shipFromDate');
        $shipToDate = $this->getRequest()->getParam('shipToDate');
        $vendorId = $this->getRequest()->getParam('vendorId');
        $storeId = $this->getRequest()->getParam('storeId');

        $vendorQuery = '';
        if ($vendorId != '0') {
            $vendorQuery .= "and item.vendor='$vendorId'";
        }
        if ($storeId != '') {
            $vendorQuery .= "and item.store_id='$storeId'";
        }
        
             $query =" SELECT box.box_id,box.gift_message,box.order_id,address.street,address.region,
                    address.city, address.firstname, address.lastname, address.company, address.country_id,
                    address.postcode, sales.partner_order_id, region.code, box.item_id,item.name, item.base_row_total_incl_tax,
                    item.weight,box.status,item.vendor,item.sku,item.product_id, item.pickup_date,item.shipping_method,vend.vendor_name,
                    vend.address as vaddress, vend.city as vcity, vend.telephone as vtelephone, vend.last_name as vlast_name,
                    vend.street as vstreet, vend.state as vstate,
                    vend.zipcode as vzipcode, vend.region as vregioncode,
                    as_width.value as pwidth, as_weight.value as pweight, as_height.value as pheight, as_length.value as plength
                    FROM `order_item_box` as box
                    LEFT JOIN `sales_order_address` as address on box.order_id = address.parent_id
                    LEFT JOIN `directory_country_region` as region on address.region_id = region.region_id
                    LEFT JOIN sales_order as sales
                    on box.entity_id = sales.entity_id
                    LEFT JOIN sales_order_item as item
                    on box.item_id = item.item_id
                    LEFT JOIN catalog_product_entity_varchar as as_width on as_width.entity_id = item.product_id and as_width.attribute_id = 156
                    LEFT JOIN catalog_product_entity_varchar as as_weight on as_weight.entity_id = item.product_id and as_weight.attribute_id = 157
                    LEFT JOIN catalog_product_entity_varchar as as_height on as_height.entity_id = item.product_id and as_height.attribute_id = 155
                    LEFT JOIN catalog_product_entity_varchar as as_length on as_length.entity_id = item.product_id and as_length.attribute_id = 158
                    LEFT JOIN vendor_registrations as vend on item.vendor = vend.vendor_id
                    WHERE (item.pickup_date BETWEEN '$shipFromDate' and '$shipToDate' ) and box.status in ('printed')
                    and item.shipping_method in ('FEDEX_GROUND','PRIORITY_OVERNIGHT','STANDARD_OVERNIGHT','Fedex')
                    and address.address_type = 'shipping' and (box.lebel_status is NULL or (box.lebel_status != 'SUCCESS' and box.lebel_status != 'WARNING'))
                    ORDER by item.name ASC ";

                  
                  $result = $this->resourceConnection->getConnection()->fetchAll($query);
                 // echo"<pre>";
                 // print_r($result);die();
                
                $pickup_date = date('c');
                foreach ($result as $value) {
                  

        $request['RequestedShipment'] = array(
            'ShipTimestamp' => $pickup_date,
            'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
            'ServiceType' =>  $value['shipping_method'], //'FEDEX_GROUND', // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
            'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
            /*'TotalWeight' => array(
                'Value' => 50.0, 
                'Units' => 'LB' // valid values LB and KG
            ),*/
            
            //'Recipient' =>$this-> addShipper(),
            'Recipient' => array(
                            'Contact' => array(
                                'PersonName' => $value['firstname'].' '.$value['lastname'], //'Sender Name Sanoj Yadav2',
                                //'CompanyName' => 'Order No '.$value['order_id'],
                                'CompanyName' => $value['company'],
                                'PhoneNumber' => '1234567890'
                            ),
                            'Address' => array(
                                'StreetLines' => array($value['street']),
                                'City' => $value['city'],
                                'StateOrProvinceCode' => $value['code'],
                                'PostalCode' => $value['postcode'],
                                'CountryCode' => $value['country_id']
                            )
                           
                        ),
            'Shipper' => array(
                            'Contact' => array(
                                'PersonName' => $value['vendor_name'].' '.$value['vlast_name'], //'Sender Name Sanoj Yadav2',
                                'CompanyName' => $value['company'],
                                //'Telephone' => $value['vtelephone']
                                'PhoneNumber' => $value['vtelephone']//'1234567890'
                            ),
                            'Address' => array(
                                'StreetLines' => array($value['vaddress']),
                                'City' => $value['vcity'],
                                //'State' => $value['vstate'],
                                'StateOrProvinceCode' => $value['vcode'],
                                'PostalCode' => $value['vzipcode'],
                                'CountryCode' => $value['country_id']
                            )
                           
                        ),
            //'Shipper' =>$this-> addShipper(),
            'ShippingChargesPayment' => $this->addShippingChargesPayment(),
            //'SpecialServicesRequested' => addSpecialServices(),
            'LabelSpecification' =>$this-> addLabelSpecification(), 
            'PackageCount' => 1,                                       
            'RequestedPackageLineItems' => array(
                '0' =>array(
                'SequenceNumber'=>1,
                'GroupPackageCount'=>1,
                'Weight' => array(
                    'Value' =>$value['pweight'], //20.0,
                    'Units' => 'LB'
                ),
                'Dimensions' => array(
                    'Length' => $value['plength'],//105,
                    'Width' => $value['pwidth'],//5,
                    'Height' => $value['pheight'],//5,
                    'Units' => 'IN'
                )
            )

         ),
           
       );
       
                                                                                                                               
    try {
       
        
        if(setEndpoint('changeEndpoint')){
            $newLocation = $client->__setLocation(setEndpoint('endpoint'));
            
        }   
        
        $response = $client->processShipment($request); // FedEx web service invocation
      
        if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR'){
      
            
            $rand = rand(10,100);
            $path = 'pub/media/LebelImage/'.$response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber.'_'.$rand.'shipextlabel.png';
           // printSuccess($client, $response);
           // echo "<pre>";
           // echo $response->Notifications->Severity. '  ';
            //print_r($response); die('test');
         
            $nul ='NULL';
            if($response->HighestSeverity =='SUCCESS'){
                echo"success";
               // print_r($response);die();
                $update_query = "update order_item_box set track_id ='".$response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber."',lebel_error ='".$nul."',lebel_status ='".$response->Notifications->Severity."', ImageUrl = '".$path."' WHERE order_id = '".$value['order_id']."'";
            }else{
                //print_r($response);die();
                if($response->HighestSeverity =='WARNING'){
                    $update_query = "update order_item_box set track_id ='".$response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber."',lebel_error ='".$response->Notifications[0]->Message."',lebel_status ='".$response->Notifications[0]->Severity."', ImageUrl = '".$path."' WHERE order_id = '".$value['order_id']."'";

                }else{

                 $update_query = "update order_item_box set track_id ='".$response->CompletedShipmentDetail->MasterTrackingId->TrackingNumber."',lebel_error ='".$response->Notifications[0]->Message."',lebel_status ='".$response->Notifications[0]->Severity."', ImageUrl = '".$path."' WHERE order_id = '".$value['order_id']."'";
                }

            }

                $this->resourceConnection->getConnection()->query($update_query);

                $fp = fopen($path, 'wb');   
            fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
            fclose($fp);
              //  print_r($response);//. '  ';
            echo ' '.$response->Notifications->Severity.' order id- '.$value['order_id'].' - '.$response->Notifications->Message."<br>";

            
             
        }else{
            printError($client, $response);//echo"dekho yaaayuu";
            //print_r($response);
            if(is_array($response->Notifications)){
                echo ' '.$response->HighestSeverity.' order id-'.$value['order_id'].' - '.$response->Notifications[0]->Message."<br>";
            
            $update_query1 = "update order_item_box set lebel_error ='".$response->Notifications[0]->Message."',lebel_status ='".$response->Notifications[0]->Severity."' WHERE order_id = '".$value['order_id']."'";
            } else{
                //print_r($response);die();
                echo ' '.$response->HighestSeverity.' order id-'.$value['order_id'].' - '.$response->Notifications->Message."<br>";
            
            $update_query1 = "update order_item_box set lebel_error ='".$response->Notifications->Message."',lebel_status ='".$response->Notifications->Severity."' WHERE order_id = '".$value['order_id']."'";
            }
            $this->resourceConnection->getConnection()->query($update_query1);
        }

        writeToLog($client);    // Write to log file
    } catch (SoapFault $exception) {
        
        printFault($exception, $client);
         echo"helllllllooooooo";
         echo"helllllllooooooo";
        echo $e->faultcode;
        var_dump($exception->getCode());
        var_dump($e->getCode());
        echo $$exception->faultcode;
       //print_r($e);
      /* echo"<pre>";
       echo"hello Dost";
        echo $exception->getMessage();die();*/
    }
   }
  }


    /**
     * Download UPS file Template
     * @throws NoSuchEntityException
     */
    public function downloadUpsFileTemplate()
    {
        //echo"hell jee";die();
        $csvFileData = [];
        $upsHeaders = $this->getUpsFileHeaders();
        $csvFileData[] = $upsHeaders;
        $boxSql = $this->getSqlBoxesString();

        $filterQuery = "and box.status in ('pending','processing')";
        $query = "SELECT box.order_id,box.item_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql) $filterQuery";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = $itemArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
            $itemArray[] = $row['item_id'];
        }
        $orderArray = array_unique($orderArray);
        $itemArray = array_unique($itemArray);

        /**
         * Logic for most sold products item ids in order wise
         */
        $itemIdString = "'" . implode("', '", $itemArray) . "'";
        $query = "SELECT item.sku,sum(item.qty_ordered) as qty_sold
                  FROM `sales_order_item` as item
                  WHERE item.item_id in ($itemIdString)
                  GROUP by item.sku
                  ORDER by qty_sold DESC";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $skuArray = [];
        foreach ($queryResult as $qryResult) {
            $skuArray[] = $qryResult['sku'];
        }
        $skuString = "'" . implode("', '", $skuArray) . "'";

        /**
         * Get order details
         */
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $partnerId = $order->getPartnerOrderId();
            $shippingAddress = $order->getShippingAddress();
            $billingAddress = $order->getBillingAddress();
            $billingName = $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $shippingCity = $shippingAddress->getCity();
            $shippingStreet = $shippingAddress->getStreet();
            $shippingPostCode = $shippingAddress->getPostcode();
            $shippingTelephone = $shippingAddress->getTelephone();
            $shippingState = $shippingAddress->getRegionCode();

            $shippingAddressLine1 = (isset($shippingStreet[0])) ? $shippingStreet[0] : '';
            $shippingAddressLine2 = (isset($shippingStreet[1])) ? $shippingStreet[1] : '';
            $shippingAddressLine3 = (isset($shippingStreet[2])) ? $shippingStreet[2] : '';
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
            $boxOrderData[$orderId]['ship_line_1'] = $shippingAddressLine1;
            $boxOrderData[$orderId]['ship_line_2'] = $shippingAddressLine2;
            $boxOrderData[$orderId]['ship_line_3'] = $shippingAddressLine3;
            $boxOrderData[$orderId]['city'] = $shippingCity;
            $boxOrderData[$orderId]['state'] = $shippingState;

            $postCodeArray = explode("-", $shippingPostCode);
            if (count($postCodeArray) > 1) {
                $postCode = $shippingPostCode;
            } else {
                $postCode = $postCodeArray[0];
                if (strlen($postCode) < 5) {
                    $padLength = 5 - strlen($postCode);
                    $postCode = str_pad($postCode, 5, "0", STR_PAD_LEFT);
                    $postCode = "=" . $postCode;
                }
            }
            $boxOrderData[$orderId]['post_code'] = (string)$postCode;
            $boxOrderData[$orderId]['country'] = 'US';
            $boxOrderData[$orderId]['phone'] = $shippingTelephone;
            $boxOrderData[$orderId]['partner_order_id'] = $partnerId;
            $boxOrderData[$orderId]['billing_name'] = $billingName;
        }

        /**
         * Actual logic
         * Box details
         */
        $query = "SELECT box.box_id,box.order_id,box.item_id,box.status,box.gift_message,item.vendor,item.sku,item.product_id,item.name 
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  WHERE box.box_id in ($boxSql) $filterQuery
                  ORDER by FIELD(item.sku,$skuString) ";

        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $oneBox) {
            $upsBoxWiseData = [];
            $sku = $oneBox['sku'];
            $orderId = $oneBox['order_id'];
            $product = $this->categoryHelper->getProductBySku($sku);
            $productId = $product->getId();
            $productName = $product->getName();
            $boxLength = $product->getBoxLength();
            $boxWidth = $product->getBoxWidth();
            $boxHeight = $product->getBoxHeight();
            $boxWeight = $product->getBoxWeight();
            $poPrice = $product->getPoPrice();
            $group = $product->getGroup();
            $attr = $product->getResource()->getAttribute('group');
            $groupName = '';
            if ($attr->usesSource()) {
                $groupName = $attr->getSource()->getOptionText($group);
            }
            $giftMessage = (trim($oneBox['gift_message']) == '') ?
                $boxOrderData[$orderId]['billing_name'] : $oneBox['gift_message'];

            $giftMessage = str_replace("?", "", $giftMessage);
            $upsBoxWiseData = [
                $boxOrderData[$orderId]['ship_name'],
                $boxOrderData[$orderId]['ship_name'],
                $boxOrderData[$orderId]['ship_line_1'],
                $boxOrderData[$orderId]['ship_line_2'],
                $boxOrderData[$orderId]['ship_line_3'],
                $boxOrderData[$orderId]['city'],
                $boxOrderData[$orderId]['state'],
                $boxOrderData[$orderId]['post_code'],
                $boxOrderData[$orderId]['country'],
                $boxOrderData[$orderId]['phone'],
                $giftMessage,
                $boxOrderData[$orderId]['partner_order_id'],
                $productName,
                '1',
                $productName,
                'Fresh Flowers',
                $boxLength,
                $boxWidth,
                $boxHeight,
                $boxWeight,
                $poPrice,
                'ES',
                'CP',
                $groupName,
                'USD',
                'CO',
                'Box',
                'Isha Flowers',
                'Zach Hemple',
                '4862 Cadiz Circle',
                'Palm Beach Gardens',
                'FL',
                'US',
                '33418',
                '8562819187',
                '85086E'
            ];
            $csvFileData[] = $upsBoxWiseData;
        }

        /**
         * Download file
         */
         
        $fileName = "ups_file_template.csv";
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        $fp = fopen('php://output', 'w');
        foreach ($csvFileData as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
        exit();

    }

    /**
         * Download file
         */

    public function downloadTrackFileTemplate()
    {
        
        $data = [];
        $fileName = 'track_id_upload_template.csv';
        $data[] = ['box_id', 'sku', 'name', 'track_id', 'shipping_name', 'order number', 'partner_order_id'];

        $boxSql = $this->getSqlBoxesString();

        /**
         * Get distinct order ids
         */
        $query = "SELECT DISTINCT box.order_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
        }
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $shippingAddress = $order->getShippingAddress();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
        }

        /**
         * Box data
         */
        $query = "SELECT box.box_id,item.sku,item.name,box.track_id,box.order_id,ord.increment_id,ord.partner_order_id
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item on box.item_id = item.item_id
                  LEFT JOIN sales_order as ord on box.order_id = ord.entity_id
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $key => $result) {
            $queryData = [];
            foreach ($result as $index => $value) {
                if ($index == 'order_id') {
                    $value = $boxOrderData[$value]['ship_name'];
                }
                $queryData[] = $value;
            }
            $data[] = $queryData;
        }
        
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $fp = fopen('php://output', 'w');
        foreach ($data as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
        exit();
    }

    /**
     * Download Track Id upload Template
     */
    
    /**
     * Return Headers
     *
     * @return array
     */
    public function getBoxHeaders()
    {
        return [
            'Box Id',
            'Item Id',
            'Label Status',
            'Error Message',
            'Box Status',
            'Sku',
            'Product Name',
            'Vendor Name',
            'Track id',
            'Pickup Date',
            'Delivery Date',
            'Gift Message',
        ];
    }

    /**
     * Return Ups File Array
     *
     * @return array
     */
    public function getUpsFileHeaders()
    {
        return [
            'Companyshipto',
            'contactshipto',
            'address1shipto',
            'address2shipto',
            'address3shipto',
            'cityshipto',
            'stateshipto',
            'zipshipto',
            'Countryshipto',
            'phoneshipto',
            'Message',
            'Reference1',
            'Reference2',
            'Quantity',
            'Item',
            'ProdDesc',
            'Length',
            'width',
            'height',
            'WeightKg',
            'DclValue',
            'Service',
            'PkgType',
            'GenDesc',
            'Currency',
            'Origin',
            'UOM',
            'TPComp',
            'TPAttn',
            'TPAdd1',
            'TPCity',
            'TPState',
            'TPCtry',
            'TPZip',
            'TPPhone',
            'TPAcct'
        ];
    }

    /**
     * Get Sql Query
     * @return string
     */
    public function getSqlBoxesString()
    {
        $boxArray = explode(",", $this->_boxes);
        return "'" . implode("', '", $boxArray) . "'";
    }

    /**
     * Return Order object by ids
     * @param $orderArray
     * @return Collection
     */
    public function getOrderCollectionByIds($orderArray)
    {
        return $this->orderHelper->getOrderCollectionByIds($orderArray);
    }

    /**
     * Download Label file Template with Box Id and Box status
     * @throws NoSuchEntityException
     */
    public function downloadAllLebelPdfTemplate()
    {

        $shipFromDate = $this->getRequest()->getParam('shipFromDate');
        $shipToDate = $this->getRequest()->getParam('shipToDate');
        $vendorId = $this->getRequest()->getParam('vendorId');
        $storeId = $this->getRequest()->getParam('storeId');

        $vendorQuery = '';
        if ($vendorId != '0') {
            $vendorQuery .= "and item.vendor='$vendorId'";
        }
        if ($storeId != '') {
            $vendorQuery .= "and item.store_id='$storeId'";
        }

        $query = "SELECT box.box_id, address.firstname, address.lastname, box.gift_message,box.order_id,box.ImageUrl
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN vendor_registrations as vend 
                  on item.vendor = vend.vendor_id
                  LEFT JOIN sales_order_address as address
                  on box.order_id = address.parent_id
                  WHERE box.status in ('printed') and address.address_type in('billing') and item.shipping_method in ('FEDEX_GROUND','STANDARD_OVERNIGHT','PRIORITY_OVERNIGHT') $vendorQuery and  (box.lebel_status = 'SUCCESS' or box.lebel_status = 'WARNING') 
                  ORDER by item.name ASC";
                  $result= $this->resourceConnection->getConnection()->fetchAll($query);
                  //print_r($result);die('test');
                  $Img = array();
                  $pdf = new \FPDF();             
                    foreach ($result as $v) {
                                
                                $pdf->AddPage();
                                $pdf->Image($v['ImageUrl'],20,60,180,200,'PNG');
                                if($v['gift_message']!== NULL){
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100);
                                $pdf->Multicell(0,10, $v['gift_message'],0,'C');//0, true, 'R'
                                }else{
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100); 
                                $pdf->Multicell(0,10, $v['firstname'].' '.$v['lastname'],0,'C');
                                }
                       
                             }
                                $pdf->Output();
                        }

    
    public function downloadZebraLebelPdfTemplate()
    {
        
        //echo"fdfdf";die();
        $shipFromDate = $this->getRequest()->getParam('shipFromDate');
        $shipToDate = $this->getRequest()->getParam('shipToDate');
        $vendorId = $this->getRequest()->getParam('vendorId');
        $storeId = $this->getRequest()->getParam('storeId');

        $vendorQuery = '';
        if ($vendorId != '0') {
            $vendorQuery .= "and item.vendor='$vendorId'";
        }
        if ($storeId != '') {
            $vendorQuery .= "and item.store_id='$storeId'";
        }

        $query = "SELECT box.box_id, address.firstname, address.lastname, box.gift_message,box.order_id,box.ImageUrl
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN vendor_registrations as vend 
                  on item.vendor = vend.vendor_id
                  LEFT JOIN sales_order_address as address
                  on box.order_id = address.parent_id
                  WHERE box.status in ('printed') and address.address_type in('billing') and item.shipping_method in ('FEDEX_GROUND','STANDARD_OVERNIGHT','PRIORITY_OVERNIGHT','Fedex') $vendorQuery and  (box.lebel_status = 'SUCCESS' or box.lebel_status = 'WARNING') 
                  ORDER by item.name ASC";
                  $result = $this->resourceConnection->getConnection()->fetchAll($query);
                 // echo"<pre>";
                 // print_r($result);die();
                  $Img = array();
                  $pdf = new \FPDF();             
                    foreach ($result as $v) {
                                
                                $pdf->AddPage();
                                $pdf->Image($v['ImageUrl'],20,60,180,200,'PNG');
                                if($v['gift_message']!== NULL){
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100);
                                $pdf->Multicell(0,10, $v['gift_message'],0,'C');
                                }else{
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','',30);
                                $pdf->SetTopMargin(100);
                                $pdf->Multicell(0,10, $v['firstname'].' '.$v['lastname'],0,'C');
                                }
                       
                             }
                                $pdf->Output();
                        }

        
}

