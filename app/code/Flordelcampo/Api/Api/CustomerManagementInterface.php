<?php

namespace Flordelcampo\Api\Api;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Magento\Framework\Webapi\Rest\Response;
use phpDocumentor\Reflection\DocBlock\Tags\Param;

/**
 * Interface CustomerManagementInterface
 */
interface CustomerManagementInterface
{
    /**
     * Return Customer Payment Methods
     * @param integer $customerId
     * @return ApiResponseInterface
     */
    public function getCustomerPaymentMethods($customerId);

    /**
     * Add Address
     *
     * @return ApiResponseInterface
     */
    public function addAddress();

    /**
     * Update Address API
     * @return ApiResponseInterface
     */
    public function updateAddress();

    /**
     * Return Zip code by IP address
     * @return ApiResponseInterface
     */
    public function getZipCodeByIp();

    /**
     * Reset Password
     * @return ApiResponseInterface
     */
    public function resetPassword();

    /**
     * Return Country List
     *
     * @return ApiResponseInterface
     */
    public function getCountryList();

    /**
     * Return Stores and Website list
     *
     * @return ApiResponseInterface
     */
    public function getStoresAndWebsiteList();
    
    /**
     * Return Customer Details for Stores and Website
     *
     * @return ApiResponseInterface
     */
    public function getCustomerForWebsiteAndStore();

}