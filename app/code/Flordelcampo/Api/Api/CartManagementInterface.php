<?php

namespace Flordelcampo\Api\Api;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Magento\Framework\Webapi\Rest\Response;

/**
 * Interface CartManagementInterface
 * Flordelcampo\Api\Api
 */
interface CartManagementInterface
{
    /**
     * Add to cart API
     *
     * @return ApiResponseInterface
     */
    public function addToCart();

    /**
     * Get Cart List
     *
     * @return ApiResponseInterface
     */
    public function getCartList();

    /**
     * Update Cart Items
     *
     * @param integer $itemId
     * @return ApiResponseInterface
     */
    public function updateCart($itemId);

    /**
     * Delete Cart Item by Item ID
     * @param integer $cartItemId
     * @return ApiResponseInterface
     */
    public function deleteCartItemByItemId($cartItemId);

    /**
     * Apply Coupon Code
     * @param string $couponCode
     * @return ApiResponseInterface
     */
    public function applyCouponCode($couponCode);

    /**
     * Remove Coupon Code From Cart
     *
     * @return ApiResponseInterface
     */
    public function removeCouponCode();

    /**
     * Add to cart API For Guest
     *
     * @return ApiResponseInterface
     */
    public function addToCartForGuest();

    /**
     * Get Cart List for Guest
     *
     * @param string $maskId
     * @return ApiResponseInterface
     */
    public function getGuestCartList($maskId);

    /**
     * Update Cart Items
     *
     * @param integer $itemId
     * @return ApiResponseInterface
     */
    public function updateCartForGuest($itemId);

    /**
     * Delete Cart Item by Item ID
     * @param integer cartItemId
     * @return ApiResponseInterface
     */
    public function deleteCartItemByItemIdForGuest($cartItemId);

    /**
     * @return boolean
     */
    public function assignCustomer();

    /**
     * Delete All cart items
     * @return ApiResponseInterface
     */
    public function deleteAllCartItems();

    /**
     * Add Gift Message to Item
     * @return ApiResponseInterface
     */
    public function addGiftMessage();
}