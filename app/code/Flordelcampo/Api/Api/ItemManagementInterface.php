<?php

namespace Flordelcampo\Api\Api;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;

interface ItemManagementInterface
{
    /**
     * Placer Order and return order Id
     * @return ApiResponseInterface
     */
    public function addPrescriptionIdToItem();

    /**
     * Return All Box Status List
     * @return ApiResponseInterface
     */
    public function getAllBoxStatusList();

    /**
     * Return Master Order Data Box Wise
     * @return mixed
     */
    public function getMasterOrderDataReport();

}