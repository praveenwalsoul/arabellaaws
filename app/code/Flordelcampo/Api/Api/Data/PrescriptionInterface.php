<?php

namespace Flordelcampo\Api\Api\Data;

/**
 * Interface PrescriptionInterface
 * Flordelcampo\Api\Api\Data
 */
interface PrescriptionInterface
{
    const PRESCRIPTION_ID = 'prescription_id';
    const PRESCRIPTION_URL = 'prescription_url';
    const TYPE = 'type';
    const UPLOADED_DATE = 'date';
    const UPLOADED_BY = 'uploaded_by';
    const USERNAME = 'username';

    /**
     * @return mixed
     */
    public function getPrescriptionId();

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @param $type
     * @return mixed
     */
    public function setType($type);

    /**
     * @return mixed
     */
    public function getUploadedDate();

    /**
     * @param $date
     * @return mixed
     */
    public function setUploadedDate($date);

    /**
     * @return mixed
     */
    public function getUploadedBy();

    /**
     * @param $uploadedBy
     * @return mixed
     */
    public function setUploadedBy($uploadedBy);

    /**
     * @return mixed
     */
    public function getUsername();

    /**
     * @param $username
     * @return mixed
     */
    public function setUsername($username);

    /**
     * @return mixed
     */
    public function getPrescriptionUrl();

    /**
     * @param $prescriptionId
     * @return mixed
     */
    public function setPrescriptionId($prescriptionId);

    /**
     * @param $prescriptionUrl
     * @return mixed
     */
    public function setPrescriptionUrl($prescriptionUrl);

}