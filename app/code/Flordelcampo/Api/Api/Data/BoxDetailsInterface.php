<?php

namespace Flordelcampo\Api\Api\Data;

/**
 * Interface BoxDetailsInterface
 * Flordelcampo\Api\Api\Data
 */
interface BoxDetailsInterface
{
    const BOX_ID = 'box_id';
    const BOX_STATUS = 'box_status';
    const BOX_TRACKING_ID = 'tracking_number';
    const PRODUCT_NAME = 'product_name';
    const IMAGE = 'image';
    const AMOUNT = 'amount';

    /**
     * @return mixed
     */
    public function getBoxId();

    /**
     * @return mixed
     */
    public function getBoxStatus();

    /**
     * @return mixed
     */
    public function getTrackingNumber();

    /**
     * @return mixed
     */
    public function getProductName();

    /**
     * @return mixed
     */
    public function getImage();
    /**
     * @return mixed
     */
    public function getAmount();

    /**
     * @param $amount
     * @return mixed
     */
    public function setAmount($amount);

    /**
     * @param $image
     * @return mixed
     */
    public function setImage($image);

    /**
     * @param $productName
     * @return mixed
     */
    public function setProductName($productName);

    /**
     * @param $trackingNumber
     * @return mixed
     */
    public function setTrackingNumber($trackingNumber);

    /**
     * @param $boxStatus
     * @return mixed
     */
    public function setBoxStatus($boxStatus);

    /**
     * @param $boxId
     * @return mixed
     */
    public function setBoxId($boxId);

}