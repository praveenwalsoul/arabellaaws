<?php

namespace Flordelcampo\Api\Api\Data;

/**
 * Interface CustomOrderAttributeInterface
 * Flordelcampo\Api\Api\Data
 */
interface CustomOrderAttributeInterface
{
    const PRESCRIPTION_ID = 'prescription_id';
    const PRESCRIPTION_URL = 'prescription_url';

    /**
     * @return $this
     */
    public function getPrescriptionId();

    /**
     * @return $this
     */
    public function getPrescriptionUrl();

    /**
     * @param $prescriptionId
     * @return $this
     */
    public function setPrescriptionId($prescriptionId);

    /**
     * @param $prescriptionUrl
     * @return $this
     */
    public function setPrescriptionUrl($prescriptionUrl);

}