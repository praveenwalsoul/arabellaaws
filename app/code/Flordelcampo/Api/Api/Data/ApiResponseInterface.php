<?php

namespace Flordelcampo\Api\Api\Data;

/**
 * Interface ApiResponseInterface
 * Flordelcampo\Api\Api\Data
 */
interface ApiResponseInterface
{
    const RESPONSE_ATTRIBUTES = 'response';

    /**
     * Get Response
     * @return mixed
     */
    public function getResponse();

    /**
     * Set Response
     * @param $response
     * @return mixed
     */
    public function setResponse($response);

}
