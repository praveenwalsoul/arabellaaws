<?php

namespace Flordelcampo\Api\Api\Data;

/**
 * Interface ItemDetailsInterface
 * Flordelcampo\Api\Api\Data
 */
interface ItemDetailsInterface
{
    const ORDER_ITEM_STATUS = 'order_item_status';
    const DELIVERY_DATE = 'delivery_date';
    const VENDOR = 'vendor';
    const TIME_SLOT_FROM = 'time_slot_from';
    const TIME_SLOT_TO = 'time_slot_to';
    const CONSULTATION_TYPE = 'consultation_type';
    const VENDOR_TYPE = 'vendor_type';
    const AGE = 'age';
    const SEX = 'sex';
    const PATIENT_NAME = 'patient_name';
    const VENDOR_TYPE_LABEL = 'vendor_type_label';

    /**
     * @return mixed
     */
    public function getOrderItemStatus();

    /**
     * @return mixed
     */
    public function getDeliveryDate();

    /**
     * @return mixed
     */
    public function getVendor();

    /**
     * @return mixed
     */
    public function getTimeSlotFrom();

    /**
     * @return mixed
     */
    public function getTimeSlotTo();

    /**
     * @return mixed
     */
    public function getConsultationType();

    /**
     * @return mixed
     */
    public function getVendorType();

    /**
     * @return mixed
     */
    public function getAge();

    /**
     * @return mixed
     */
    public function getSex();

    /**
     * @return mixed
     */
    public function getPatientName();

    /**
     * @return mixed
     */
    public function getVendorTypeLabel();

    /**
     * @param $orderItemStatus
     * @return mixed
     */
    public function setOrderItemStatus($orderItemStatus);

    /**
     * @param $vendorTypeLabel
     * @return mixed
     */
    public function setVendorTypeLabel($vendorTypeLabel);

    /**
     * @param $deliveryDate
     * @return mixed
     */
    public function setDeliveryDate($deliveryDate);

    /**
     * @param $vendor
     * @return mixed
     */
    public function setVendor($vendor);

    /**
     * @param $timeSlotFrom
     * @return mixed
     */
    public function setTimeSlotFrom($timeSlotFrom);

    /**
     * @param $timeSlotTo
     * @return mixed
     */
    public function setTimeSlotTo($timeSlotTo);

    /**
     * @param $consultationType
     * @return mixed
     */
    public function setConsultationType($consultationType);

    /**
     * @param $vendorType
     * @return mixed
     */
    public function setVendorType($vendorType);

    /**
     * @param $age
     * @return mixed
     */
    public function setAge($age);

    /**
     * @param $sex
     * @return mixed
     */
    public function setSex($sex);

    /**
     * @param $patientName
     * @return mixed
     */
    public function setPatientName($patientName);
}