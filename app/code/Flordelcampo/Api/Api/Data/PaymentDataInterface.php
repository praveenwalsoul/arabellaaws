<?php

namespace Flordelcampo\Api\Api\Data;

/**
 * Interface PaymentDataInterface
 * Flordelcampo\Api\Api\Data
 */
interface PaymentDataInterface
{
    const PAYMENT_METHOD = 'payment_method';
    const CARD_NUMBER = 'card_number';
    const CARD_TYPE = 'card_type';

    /**
     * @return mixed
     */
    public function getPaymentMethod();

    /**
     * @return mixed
     */
    public function getCardNumber();

    /**
     * @return mixed
     */
    public function getCardType();

    /**
     * @param $cardType
     * @return mixed
     */
    public function setCardType($cardType);

    /**
     * @param $paymentMethod
     * @return mixed
     */
    public function setPaymentMethod($paymentMethod);

    /**
     * @param $cardNumber
     * @return mixed
     */
    public function setCardNumber($cardNumber);
}