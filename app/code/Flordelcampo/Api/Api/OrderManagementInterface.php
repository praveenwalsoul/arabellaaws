<?php

namespace Flordelcampo\Api\Api;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;

interface OrderManagementInterface
{
    /**
     * Placer Order and return order Id
     * @return ApiResponseInterface
     */
    public function placeOrder();

    /**
     * Cancel Box API
     * @return ApiResponseInterface
     */
    public function cancelBox();

    /**
     * Update Box or Item or Order Status
     * @return ApiResponseInterface
     */
    public function updateStatus();

}