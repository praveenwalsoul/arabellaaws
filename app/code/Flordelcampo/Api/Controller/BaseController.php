<?php

namespace Flordelcampo\Api\Controller;

use Flordelcampo\Api\Api\Data\PrescriptionInterfaceFactory;
use Magento\Catalog\Model\Product;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\CouponManagement;
use Magento\Quote\Model\ResourceModel\Quote as QuoteResourceModel;
use Magento\Store\Model\StoreManagerInterface;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Api\Helper\Data as ApiHelper;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\Quote;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Quote\Model\QuoteRepository;
use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\Api\Api\Data\ApiResponseInterfaceFactory;
use Magento\Quote\Model\QuoteIdMask;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Quote\Model\QuoteManagement;
use Magento\Quote\Api\Data\PaymentInterface;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordelcampo\Api\Api\Data\PaymentDataInterface;
use Flordelcampo\Api\Api\Data\PaymentDataInterfaceFactory;
use Flordelcampo\Api\Api\Data\BoxDetailsInterfaceFactory;
use Flordelcampo\Api\Api\Data\ItemDetailsInterfaceFactory;
use Flordelcampo\CalendarApi\Helper\Data as CalendarHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\Order\Model\Order as OrderCreate;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

/**
 * Class BaseController
 * Flordelcampo\Api\Controller
 */
abstract class BaseController extends \Magento\Framework\App\Action\Action
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var ApiHelper
     */
    protected $apiHelper;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;
    /**
     * @var
     */
    protected $quoteResourceModel;
    /**
     * @var Quote
     */
    protected $quote;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepositoryInterface;
    /**
     * @var CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var CartManagementInterface
     */
    protected $cartManagementInterface;
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;
    /**
     * @var ProductRepository
     */
    protected $productRepository;
    /**
     * @var Product
     */
    protected $product;
    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;
    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;
    /**
     * @var CouponManagement
     */
    protected $couponManagement;
    /**
     * @var ApiResponseInterface
     */
    protected $apiResponse;
    /**
     * @var ApiResponseInterfaceFactory
     */
    protected $responseFactory;
    /**
     * @var QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;
    /**
     * @var QuoteManagement
     */
    protected $orderManagement;
    /**
     * @var PaymentInterface
     */
    protected $paymentInterface;
    /**
     * @var BuilderInterface
     */
    protected $transactionBuilder;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var PaymentDataInterface
     */
    protected $paymentDetailsInterfaceFactory;
    /**
     * @var PaymentDataInterface
     */
    protected $paymentData;
    /**
     * @var PaymentDataInterfaceFactory
     */
    protected $paymentDataInterfaceFactory;
    /**
     * @var BoxDetailsInterfaceFactory
     */
    protected $boxDetailsInterfaceFactory;
    /**
     * @var CalendarHelper
     */
    protected $calendarHelper;
    /**
     * @var ItemDetailsInterfaceFactory
     */
    protected $itemDetailsInterfaceFactory;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var OrderCreate
     */
    protected $orderCreate;
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;
    /**
     * @var PrescriptionInterfaceFactory
     */
    protected $prescriptionInterfaceFactory;

    /**
     * BaseController constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param ApiHelper $apiHelper
     * @param CustomerHelper $customerHelper
     * @param Request $request
     * @param QuoteFactory $quoteFactory
     * @param Quote $quote
     * @param QuoteResourceModel $quoteResourceModel
     * @param ResourceConnection $resourceConnection
     * @param CartRepositoryInterface $cartRepositoryInterface
     * @param CartManagementInterface $cartManagementInterface
     * @param CustomerFactory $customerFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param StoreRepositoryInterface $storeRepository
     * @param ProductRepository $productRepository
     * @param Product $product
     * @param QuoteRepository $quoteRepository
     * @param AddressRepositoryInterface $addressRepository
     * @param CouponManagement $couponManagement
     * @param ApiResponseInterfaceFactory $responseFactory
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param QuoteManagement $orderManagement
     * @param PaymentInterface $paymentInterface
     * @param BuilderInterface $builderInterface
     * @param OrderHelper $orderHelper
     * @param PaymentDataInterface $paymentData
     * @param PaymentDataInterfaceFactory $paymentDataInterfaceFactory
     * @param BoxDetailsInterfaceFactory $boxDetailsInterfaceFactory
     * @param ItemDetailsInterfaceFactory $itemDetailsInterfaceFactory
     * @param CalendarHelper $calendarHelper
     * @param VendorHelper $vendorHelper
     * @param OrderCreate $orderCreate
     * @param AttributeRepositoryInterface $attributeRepository
     * @param CatalogHelper $catalogHelper
     * @param PrescriptionInterfaceFactory $prescriptionInterfaceFactory
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        ApiHelper $apiHelper,
        CustomerHelper $customerHelper,
        Request $request,
        QuoteFactory $quoteFactory,
        Quote $quote,
        QuoteResourceModel $quoteResourceModel,
        ResourceConnection $resourceConnection,
        CartRepositoryInterface $cartRepositoryInterface,
        CartManagementInterface $cartManagementInterface,
        CustomerFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        StoreRepositoryInterface $storeRepository,
        ProductRepository $productRepository,
        Product $product,
        QuoteRepository $quoteRepository,
        AddressRepositoryInterface $addressRepository,
        CouponManagement $couponManagement,
        ApiResponseInterfaceFactory $responseFactory,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        QuoteManagement $orderManagement,
        PaymentInterface $paymentInterface,
        BuilderInterface $builderInterface,
        OrderHelper $orderHelper,
        PaymentDataInterface $paymentData,
        PaymentDataInterfaceFactory $paymentDataInterfaceFactory,
        BoxDetailsInterfaceFactory $boxDetailsInterfaceFactory,
        ItemDetailsInterfaceFactory $itemDetailsInterfaceFactory,
        CalendarHelper $calendarHelper,
        VendorHelper $vendorHelper,
        OrderCreate $orderCreate,
        AttributeRepositoryInterface $attributeRepository,
        CatalogHelper $catalogHelper,
        PrescriptionInterfaceFactory $prescriptionInterfaceFactory
    ) {
        $this->storeManager = $storeManager;
        $this->apiHelper = $apiHelper;
        $this->customerHelper = $customerHelper;
        $this->request = $request;
        $this->quoteFactory = $quoteFactory;
        $this->quote = $quote;
        $this->quoteResourceModel = $quoteResourceModel;
        $this->resourceConnection = $resourceConnection;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->storeRepository = $storeRepository;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->quoteRepository = $quoteRepository;
        $this->addressRepository = $addressRepository;
        $this->couponManagement = $couponManagement;
        $this->responseFactory = $responseFactory;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->orderManagement = $orderManagement;
        $this->paymentInterface = $paymentInterface;
        $this->transactionBuilder = $builderInterface;
        $this->orderHelper = $orderHelper;
        $this->paymentData = $paymentData;
        $this->paymentDataInterfaceFactory = $paymentDataInterfaceFactory;
        $this->boxDetailsInterfaceFactory = $boxDetailsInterfaceFactory;
        $this->itemDetailsInterfaceFactory = $itemDetailsInterfaceFactory;
        $this->calendarHelper = $calendarHelper;
        $this->vendorHelper = $vendorHelper;
        $this->orderCreate = $orderCreate;
        $this->attributeRepository = $attributeRepository;
        $this->catalogHelper = $catalogHelper;
        $this->prescriptionInterfaceFactory = $prescriptionInterfaceFactory;
        parent::__construct($context);
    }
}
