<?php

namespace Flordelcampo\Api\Model\Customer;

use Flordelcampo\Api\Api\CustomerManagementInterface;
use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Magento\Framework\App;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Webapi\Rest\Response;
use Magento\Store\Model\StoreManagerInterface;
use Flordelcampo\Customer\Helper\Data;
use Flordelcampo\Api\Helper\Data as ApiHelper;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Customer\Model\AccountManagement;
use Magento\Framework\Math\Random;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\Order\Helper\Data as OrderHelper;

/**
 * Class CustomerManagement
 * Flordelcampo\Api\Model\Customer
 */
class CustomerManagement implements CustomerManagementInterface
{
    /**
     * @var Response
     */
    protected $response;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var Data
     */
    protected $customerHelper;
    /**
     * @var AddressInterfaceFactory
     */
    protected $addressInterfaceFactory;
    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepositoryInterface;
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ApiHelper
     */
    protected $apiHelper;
    /**
     * @var AccountManagement
     */
    protected $accountManagement;
    /**
     * @var Random
     */
    protected $random;
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * CustomerManagement constructor.
     * @param Response $response
     * @param StoreManagerInterface $storeManager
     * @param Data $customerHelper
     * @param Request $request
     * @param AddressInterfaceFactory $addressInterfaceFactory
     * @param AddressRepositoryInterface $addressRepositoryInterface
     * @param ApiHelper $apiHelper
     * @param AccountManagement $accountManagement
     * @param Random $random
     * @param CustomerRepositoryInterface $customerRepository
     * @param VendorHelper $vendorHelper
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        Response $response,
        StoreManagerInterface $storeManager,
        Data $customerHelper,
        Request $request,
        AddressInterfaceFactory $addressInterfaceFactory,
        AddressRepositoryInterface $addressRepositoryInterface,
        ApiHelper $apiHelper,
        AccountManagement $accountManagement,
        Random $random,
        CustomerRepositoryInterface $customerRepository,
        VendorHelper $vendorHelper,
        OrderHelper $orderHelper
    )
    {
        $this->response = $response;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->customerHelper = $customerHelper;
        $this->addressInterfaceFactory = $addressInterfaceFactory;
        $this->addressRepositoryInterface = $addressRepositoryInterface;
        $this->apiHelper = $apiHelper;
        $this->accountManagement = $accountManagement;
        $this->random = $random;
        $this->customerRepository = $customerRepository;
        $this->vendorHelper = $vendorHelper;
        $this->orderHelper = $orderHelper;
    }

    /**
     * Return Customer Payment Methods
     *
     * @param integer $customerId
     * @return ApiResponseInterface|array
     */
    public function getCustomerPaymentMethods($customerId)
    {
        try {
            $storeId = $this->getStoreId();
            $data = $this->apiHelper->getCustomerPaymentMethodsByCustomerId($customerId, $storeId);
            $response = [
                "status" => true,
                "message" => $data
            ];
        } catch (\Exception $e) {
            $response = [
                "status" => false,
                "message" => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Add New Address
     *
     * @return ApiResponseInterface|array
     */
    public function addAddress()
    {
        $response = $result = [];
        try {
            $requestParams = $this->request->getBodyParams();
            $customerData = $requestParams['customer'];
            $customer = $this->apiHelper->getCustomerByRequestHeaders($this->request->getHeaders()->toArray());
            $addressData = $customerData['address'];
            $address = $this->addressInterfaceFactory->create();
            $address->setFirstname($addressData['firstname']);
            $address->setLastname($addressData['lastname']);
            $address->setCompany($addressData['company']);
            $address->setStreet($addressData['street']);
            $address->setCity($addressData['city']);
            $address->setRegionId($addressData['region_id']);
            $address->setCountryId($addressData['country_id']);
            $address->setTelephone($addressData['telephone']);
            $address->setPostcode($addressData['postcode']);
            $address->setIsDefaultBilling($addressData['default_billing']);
            $address->setIsDefaultShipping($addressData['default_shipping']);
            $address->setCustomerId($customer->getId());

            /**
             * Add Age and Sex if exists
             */
            if (isset($addressData['age'])) {
                $address->setCustomAttribute('age', $addressData['age']);
            }
            if (isset($addressData['sex'])) {
                $address->setCustomAttribute('sex', $addressData['sex']);
            }
            $address = $this->addressRepositoryInterface->save($address);
            $response = [
                'status' => true,
                'message' => __('Address added successfully'),
                'address_id' => (int)$address->getId()
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Update Address
     * @return ApiResponseInterface|array
     */
    public function updateAddress()
    {
        $response = $result = [];
        try {
            $requestParams = $this->request->getBodyParams();
            $customerData = $requestParams['customer'];
            $customer = $this->apiHelper->getCustomerByRequestHeaders($this->request->getHeaders()->toArray());
            $addressData = $customerData['address'];
            $address = $this->addressRepositoryInterface->getById($addressData['address_id']);
            $address->setFirstname($addressData['firstname']);
            $address->setLastname($addressData['lastname']);
            $address->setCompany($addressData['company']);
            $address->setStreet($addressData['street']);
            $address->setCity($addressData['city']);
            $address->setRegionId($addressData['region_id']);
            $address->setCountryId($addressData['country_id']);
            $address->setTelephone($addressData['telephone']);
            $address->setPostcode($addressData['postcode']);
            $address->setIsDefaultBilling($addressData['default_billing']);
            $address->setIsDefaultShipping($addressData['default_shipping']);
            /**
             * Add Age and Sex if exists
             */
            if (isset($addressData['age'])) {
                $address->setCustomAttribute('age', $addressData['age']);
            }
            if (isset($addressData['sex'])) {
                $address->setCustomAttribute('sex', $addressData['sex']);
            }
            $this->addressRepositoryInterface->save($address);
            $response = [
                'status' => true,
                'message' => __('Address updated successfully')
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Zip Code By IP address
     * @return ApiResponseInterface|array
     */
    public function getZipCodeByIp()
    {
        try {
            $data = [];
            $response = $result = [];
            $ipAddress = $this->request->getParam('ip');
            $json = file_get_contents("http://ipinfo.io/{$ipAddress}/json");
            $details = json_decode($json);
            if (isset($details->postal)) {
                $data['zipcode'] = $details->postal;
                $response = [
                    "status" => true,
                    "data" => $data
                ];
            } else {
                $response = [
                    "status" => false,
                    "message" => __('Invalid IP Address')
                ];
            }
        } catch (\Exception $e) {
            $response = [
                "status" => false,
                "message" => __('Invalid IP Address')
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Reset Password
     *
     * @return ApiResponseInterface|void
     */
    public function resetPassword()
    {
        try {
            $params = $this->request->getBodyParams();
            if (isset($params['email'])) {
                $email = $params['email'];
                $newPassword = $params['newPassword'];
                $customer = $this->customerHelper->getCustomerByEmail($email);
                if (!$customer->getId()) {
                    throw new LocalizedException(__("There is not account associated with this email"));
                }
                /**
                 * Generate Customer RP token
                 */
                $customer = $this->customerRepository->getById($customer->getId());
                $newPasswordToken = $this->random->getUniqueHash();
                $this->accountManagement->changeResetPasswordLinkToken($customer, $newPasswordToken);

                /**
                 * Reset password
                 */
                $this->accountManagement->resetPassword($email, $newPasswordToken, $newPassword);
                $response = ['status' => true, 'message' => __('Password updated successfully')];
            } else {
                throw new LocalizedException(__("There is not account associated with this Email"));
            }

        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return All Country List
     * @return ApiResponseInterface|mixed
     */
    public function getCountryList()
    {
        try {
            $list = $this->vendorHelper->getAllCountryArray();
            $response = [
                "status" => true,
                "country_list" => $list
            ];

        } catch (\Exception $e) {
            $response = [
                "status" => false,
                "message" => __('No record found')
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Stores and Website List
     * @return ApiResponseInterface|mixed
     */
    public function getStoresAndWebsiteList()
    {
        try {
            $storeList = $this->orderHelper->getAllStoresListWithNameArray();
            $websiteList = $this->orderHelper->getAllWebsiteListWithNameArray();
            $response = [
                "status" => true,
                "store_list" => $storeList,
                "website_list" => $websiteList
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Customer List for Website
     * @return ApiResponseInterface|mixed
     */
    public function getCustomerForWebsiteAndStore()
    {
        try {
            $params = $this->request->getParams();
            $websiteId = $params['website_id'];
            $storeId = $params['store_id'];
            if ($storeId != null || !empty($storeId)) {
                $customerCollection = $this->customerHelper->getCustomerCollectionForStore($storeId);
            } else {
                $storeIdArray = [];
                $websiteIdWithStoreIds = $this->orderHelper->getAllStoreIdsForWebsiteId($websiteId);
                foreach ($websiteIdWithStoreIds as $websiteId => $storeArray) {
                    foreach ($storeArray as $storeId) {
                        $storeIdArray[] = $storeId;
                    }
                }
                $customerCollection = $this->customerHelper->getCustomerCollectionForStore($storeIdArray);
            }
            $customerData = [];
            if ($customerCollection->getSize() > 0) {
                foreach ($customerCollection as $customer) {
                    $mobileNumber = $this->customerHelper->getCustomerMobileNumberById($customer->getId());
                    $billingAddress = $this->customerHelper->getCustomerDefaultBillingAddressArrayByCustomerId($customer->getId());
                    $shippingAddress = $this->customerHelper->getCustomerDefaultShippingAddressArrayByCustomerId($customer->getId());
                    $customerData[] = [
                        'customer_id' => $customer->getId(),
                        'name' => $customer->getName(),
                        'email' => $customer->getEmail(),
                        'mobile_number' => $mobileNumber,
                        'billing_address' => $billingAddress,
                        'shipping_address' => $shippingAddress,
                    ];
                }
            }
            $response = [
                "status" => true,
                "customer_data" => $customerData
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Store ID
     * @return int
     * @throws NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

}