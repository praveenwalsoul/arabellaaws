<?php

namespace Flordelcampo\Api\Model\Plugin\Cart;

use Closure;
use Flordelcampo\Api\Model\OrderManagement\Razorpay;
use Magento\Checkout\Api\Data\PaymentDetailsInterface;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Api\ShippingInformationManagementInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\CartItemRepositoryInterface;
use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\ItemFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\ResourceModel\Quote as QuoteResourceModel;
use Flordelcampo\Api\Helper\Data as ApiHelper;
use Flordelcampo\Api\Model\Customer\CustomerManagement;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\State;

/**
 * Class CartPlugin
 * Flordelcampo\Api\Model\Plugin\Cart
 */
class CartPlugin
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Quote
     */
    protected $quote;

    /**
     * @var string[]
     */
    private $_customAttributesArray = [
        'vendor',
        'vendor_farm',
        'delivery_date',
        'pickup_date',
        'shipping_method',
        'time_slot_from',
        'time_slot_to'
    ];
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var QuoteResourceModel
     */
    protected $quoteResourceModel;
    /**
     * @var ApiHelper
     */
    protected $apiHelper;
    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;
    /**
     * @var CustomerManagement
     */
    protected $customerManagement;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var Session
     */
    protected $customerSession;
    /**
     * @var Razorpay
     */
    protected $razorpay;
    /**
     * @var State
     */
    protected $state;

    /**
     * CartPlugin constructor.
     * @param Request $request
     * @param Quote $quote
     * @param ResourceConnection $resourceConnection
     * @param QuoteResourceModel $quoteResourceModel
     * @param ApiHelper $apiHelper
     * @param QuoteFactory $quoteFactory
     * @param CustomerManagement $customerManagement
     * @param StoreManagerInterface $storeManager
     * @param Session $customerSession
     * @param Razorpay $razorpay
     * @param State $state
     */
    public function __construct(
        Request $request,
        Quote $quote,
        ResourceConnection $resourceConnection,
        QuoteResourceModel $quoteResourceModel,
        ApiHelper $apiHelper,
        QuoteFactory $quoteFactory,
        CustomerManagement $customerManagement,
        StoreManagerInterface $storeManager,
        Session $customerSession,
        Razorpay $razorpay,
        State $state
    )
    {
        $this->request = $request;
        $this->quote = $quote;
        $this->resourceConnection = $resourceConnection;
        $this->quoteResourceModel = $quoteResourceModel;
        $this->apiHelper = $apiHelper;
        $this->quoteFactory = $quoteFactory;
        $this->customerManagement = $customerManagement;
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
        $this->razorpay = $razorpay;
        $this->state = $state;
    }

    /**
     * Add Custom attributes to Get cart API
     *
     * @param CartManagementInterface $subject
     * @param CartInterface $cart
     * @return mixed
     */
    public function afterGetCartForCustomer(
        CartManagementInterface $subject,
        CartInterface $cart
    )
    {
        $this->addExtensionAttributes($cart);
        return $cart;
    }

    /**
     * Save Custom Attributes to cart
     *
     * @param CartItemRepositoryInterface $subject
     * @param CartItemInterface $cartItem
     * @return CartItemInterface
     */
    public function afterSave(
        CartItemRepositoryInterface $subject,
        CartItemInterface $cartItem
    ) {
        $requestParams = $this->request->getBodyParams();
        $quoteId = $cartItem->getQuoteId();
        $quoteItemId = $cartItem->getItemId();
        try {
            if (isset($requestParams['cartItem']['extension_attributes'])) {
                $quoteItem = $this->getQuoteItem($quoteId, $quoteItemId);
                $extensionAttribute = $cartItem->getExtensionAttributes();
                if ($quoteItem != null) {
                    $customAttributes = $requestParams['cartItem']['extension_attributes'];
                    foreach ($customAttributes as $attribute => $value) {
                        $quoteItem->setData($attribute, $value)->save();
                        $extensionAttribute->setData($attribute, $value);
                    }
                }
                $cartItem->setExtensionAttributes($extensionAttribute);
            }
        } catch (\Exception $e) {
            return $cartItem;
        }
        return $cartItem;
    }

    /**
     * @param ShippingInformationManagementInterface $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @return array
     */
    public function beforeSaveAddressInformation(
        ShippingInformationManagementInterface $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        if ($this->state->getAreaCode() == 'frontend') {
            /**
             * webapi_rest
             */
            return [$cartId, $addressInformation];
        };
        /**
         * Set Shipping Method
         */
        try {
            $quote = $this->quote->load($cartId);
            $carrierCode = $addressInformation->getShippingCarrierCode();
            $methodCode = $addressInformation->getShippingMethodCode();
            $shippingMethod = $carrierCode . '_' . $methodCode;

            $billingAddress = $addressInformation->getBillingAddress();
            if ($billingAddress) {
                if (!$billingAddress->getCustomerAddressId()) {
                    $billingAddress->setCustomerAddressId(null);
                }
                $quote->setBillingAddress($billingAddress);
            }
            $shippingAddress = $addressInformation->getShippingAddress();
            if ($shippingAddress) {
                if (!$shippingAddress->getCustomerAddressId()) {
                    $shippingAddress->setCustomerAddressId(null);
                } else {
                    $shippingAddress->setCustomerAddressId($shippingAddress->getCustomerAddressId());
                }
                $quote->setShippingAddress($shippingAddress);
            }
            $shippingAddress = $quote->getShippingAddress();
            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates()
                ->setShippingMethod($shippingMethod)->save();
            $quote->collectTotals()->save();
        } catch (\Exception $e) {
            return [$cartId, $addressInformation];
        }
        return [$cartId, $addressInformation];
    }

    /**
     * Set Reserved Order Id and Customer Payment Details
     * @param ShippingInformationManagementInterface $subject
     * @param PaymentDetailsInterface $paymentDetails
     * @return PaymentDetailsInterface
     */
    public function afterSaveAddressInformation(
        ShippingInformationManagementInterface $subject,
        PaymentDetailsInterface $paymentDetails
    )
    {
        try {
            $totalsObject = $paymentDetails->getTotals();
            $itemsObject = $totalsObject->getItems();
            $itemId = null;
            foreach ($itemsObject as $oneItem) {
                $itemId = $oneItem->getItemId();
                break;
            }
            $quote = $this->getQuoteByItemId($itemId);
            if ($quote->getId() != null) {
                $totalExt = $totalsObject->getExtensionAttributes();
                $reservedOrderId = $this->getReserveOrderByQuoteId($quote->getId());
                $quote->setReservedOrderId($reservedOrderId)->save();
                /**
                 * If method is razor pay then get Razor pay order id
                 * For current Reserved order Id
                 */
                $inputParams = $this->request->getBodyParams();
                if (isset($inputParams['payment']['method'])) {
                    $paymentMethod = $inputParams['payment']['method'];
                    switch ($paymentMethod) {
                        case 'razorpay':
                            $this->customerSession->setReservedOrderId($reservedOrderId);
                            $this->customerSession->setCartTotalAmount($quote->getGrandTotal());
                            $this->customerSession->setCurrencyCode($quote->getQuoteCurrencyCode());
                            $razorPayOrderId = $this->razorpay->getRazorPayOrderId();
                            $totalExt->setRazorpayOrderId($razorPayOrderId);
                            break;
                        case 'cashondelivery':
                            // TODO:
                            break;
                    }
                }
                $totalExt->setReservedOrderId($reservedOrderId);
                $customerId = $quote->getCustomerId();
                $storeId = $this->getStoreId();
                $paymentMethods = $this->apiHelper->getCustomerPaymentMethodsByCustomerId($customerId, $storeId);
                $totalExt->setPaymentMethods($paymentMethods);
                $totalsObject->setExtensionAttributes($totalExt);
            }
        } catch (\Exception $e) {
            return $paymentDetails;
        }
        return $paymentDetails;
    }

    /**
     * Add Extension attributes
     * @param CartInterface $cart
     * @return CartPlugin
     */
    public function addExtensionAttributes(CartInterface $cart)
    {
        foreach ($cart->getItems() as $oneItem) {
            $quoteItemId = $oneItem->getItemId();
            $quoteId = $oneItem->getQuoteId();
            $quoteItem = $this->getQuoteItem($quoteId, $quoteItemId);
            $extensionAttribute = $oneItem->getExtensionAttributes();
            foreach ($this->_customAttributesArray as $attribute) {
                $extensionAttribute->setData($attribute, $quoteItem->getData($attribute));
            }
            $oneItem->setExtensionAttributes($extensionAttribute);
        }
        return $this;
    }

    /**
     * @param $quoteId
     * @param $quoteItemId
     * @return Item|null
     */
    public function getQuoteItem($quoteId, $quoteItemId)
    {
        $quote = $this->quote->load($quoteId);
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($item->getId() == $quoteItemId) {
                return $item;
            }
        }
        return null;
    }

    /**
     * Return Quote By QuoteId
     * @param $quoteId
     * @return Quote
     */
    public function getQuoteByQuoteId($quoteId)
    {
        return $this->quote->load($quoteId);
    }

    /**
     * Return Quote by Quote Item ID
     * @param $quoteItemId
     * @return Quote|null
     */
    public function getQuoteByItemId($quoteItemId)
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                'quote_item',
                ['quote_id']
            )->where('item_id = ?', $quoteItemId);

        $quoteId = $connection->fetchOne($select);
        if (!empty($quoteId)) {
            return $this->quote->load($quoteId);
        }
        return null;
    }

    /**
     * Return Reserve Order ID by Quote ID
     * @param $quoteId
     * @return string|null
     */
    public function getReserveOrderByQuoteId($quoteId)
    {
        try {
            $quote = $this->getQuoteByQuoteId($quoteId);
            if ($quote->getId() != null) {
                if ($quote->getReservedOrderId() != null) {
                    return $quote->getReservedOrderId();
                } else {
                    return $this->quoteResourceModel->getReservedOrderId($quote);
                }

            }
        } catch (\Exception $e) {
            return null;
        }
        return null;
    }

    /**
     * Return Store ID
     * @return int
     * @throws NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }
}
