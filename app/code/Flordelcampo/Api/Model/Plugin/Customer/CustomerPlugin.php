<?php

namespace Flordelcampo\Api\Model\Plugin\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Integration\Api\CustomerTokenServiceInterface;
use Magento\Framework\App\ResourceConnection;
use Flordelcampo\Api\Model\CartManagement\Cart;
use Magento\Framework\App\Request\Http as Request;
use Magento\Framework\App\State;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Magento\Framework\Webapi\Rest\Request as ApiRequest;

/**
 * Class CustomerPlugin
 * Flordelcampo\Api\Model\Plugin\Customer
 */
class CustomerPlugin
{
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var Cart
     */
    protected $cart;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var State
     */
    protected $state;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var ApiRequest
     */
    protected $apiRequest;

    /**
     * CustomerPlugin constructor.
     * @param ResourceConnection $resourceConnection
     * @param Cart $cart
     * @param Request $request
     * @param State $state
     * @param CustomerHelper $customerHelper
     * @param ApiRequest $apiRequest
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        Cart $cart,
        Request $request,
        State $state,
        CustomerHelper $customerHelper,
        ApiRequest $apiRequest
    )
    {
        $this->resourceConnection = $resourceConnection;
        $this->cart = $cart;
        $this->request = $request;
        $this->apiRequest = $apiRequest;
        $this->state = $state;
        $this->customerHelper = $customerHelper;
    }

    /**
     * Delete record from oauth_token_request_log for the userName
     * @param CustomerTokenServiceInterface $subject
     * @param $userName
     * @param $password
     * @return array
     * @throws LocalizedException
     */
    public function beforeCreateCustomerAccessToken(
        CustomerTokenServiceInterface $subject,
        $userName,
        $password
    )
    {

        $params = $this->apiRequest->getBodyParams();
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                'oauth_token_request_log',
                ['failures_count']
            )->where('user_name = ?', $userName);
        $failureCount = $connection->fetchOne($select);
        if ($failureCount != null && $failureCount > 5) {
            $write = $this->resourceConnection->getConnection('core_write');
            $write->delete(
                'oauth_token_request_log',
                ["user_name = '" . $userName . "' "]
            );
        }
        if (isset($params['login_type']) && $params['login_type'] == 'account') {
            $customer = $this->customerHelper->getCustomerByEmail($userName);
            if ($this->getAccountGroupID() !== $customer->getGroupId()) {
                throw new LocalizedException(__('You dont have access ,Please contact support team'));
            }
        }
        return [$userName, $password];
    }

    /**
     * Return Account Group ID
     * @return mixed
     */
    public function getAccountGroupID()
    {
        return $this->customerHelper->getAccountGroupID();
    }

    /**
     * Add Customer Cart Data to customer API
     * @param CustomerRepositoryInterface $subject
     * @param CustomerInterface $customerInterface
     * @return CustomerInterface|void
     */
    public function afterGetById(
        CustomerRepositoryInterface $subject,
        CustomerInterface $customerInterface
    )
    {
        try {
            if ($this->state->getAreaCode() == 'frontend') {
                /**
                 * webapi_rest
                 */
                return $customerInterface;
            };

            $customerExt = $customerInterface->getExtensionAttributes();
            /**
             * for web request checking
             */
            if ($this->request->getControllerName() == 'account') {
                $customerExt->setItems(null);
                return $customerInterface;
            } else {
                if ($customerInterface->getId() != null) {
                    $cartItems = $this->cart->getItemsByCustomerId($customerInterface->getId());
                    $customerExt->setItems($cartItems);
                    $customerInterface->setExtensionAttributes($customerExt);
                }
            }
        } catch (\Exception $e) {
            return $customerInterface;
        }
        return $customerInterface;
    }
}