<?php

namespace Flordelcampo\Api\Model\Plugin\Order;

use Flordelcampo\Api\Api\Data\CustomOrderAttributeInterface;
use Flordelcampo\Api\Api\Data\PaymentDataInterface;
use Flordelcampo\Api\Api\Data\PrescriptionInterface;
use Flordelcampo\Api\Controller\BaseController;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Class OrderPlugin
 * Flordelcampo\Api\Model\Plugin\Order
 */
class OrderPlugin extends BaseController
{
    private $_order = null;

    /**
     * @param OrderRepositoryInterface $subject
     * @param OrderSearchResultInterface $searchResult
     * @return OrderSearchResultInterface
     * @throws NoSuchEntityException
     */
    public function afterGetList(
        OrderRepositoryInterface $subject,
        OrderSearchResultInterface $searchResult
    )
    {
        foreach ($searchResult->getItems() as $order) {
            $this->addOrderExtensionAttributes($order);
        }
        return $searchResult;
    }

    /**
     * Add extension attributes to order
     *
     * @param OrderInterface $order
     * @return $this
     * @throws NoSuchEntityException
     */
    public function addOrderExtensionAttributes(OrderInterface $order)
    {
        /**
         * Add items Extension attributes
         */
        foreach ($order->getItems() as $item) {
            $this->addItemExtensionAttribute($item);
        }
        $orderExt = $order->getExtensionAttributes();
        $paymentData = $this->getPaymentData($order);
        $prescriptionData = $this->getPrescriptionDetails($order);
        $prescriptionDetails = $this->getPrescriptionDataForOrder($order);
        $orderExt->setPaymentDetails($paymentData);
        $orderExt->setPrescriptionDetails($prescriptionDetails);
        /*Commenting this code as we have set as interface
        $orderExt->setPrescriptionId($prescriptionData['id']);
        $orderExt->setPrescriptionUrl($prescriptionData['url']);*/
        $order->setExtensionAttributes($orderExt);
        return $this;
    }

    /**
     * @param OrderItemInterface $item
     * @return $this
     * @throws NoSuchEntityException
     */
    public function addItemExtensionAttribute(OrderItemInterface $item)
    {
        $itemExt = $item->getExtensionAttributes();

        $imageUrl = $this->getProductImageBySku($item->getSku());
        $boxData = $this->getBoxData($item);
        $itemData = $this->getItemData($item);
        $prescriptionData = $this->getPrescriptionDataForItem($item);
        $itemExt->setImageUrl($imageUrl);
        $itemExt->setItemBoxes($boxData);
        $itemExt->setCustomItemAttributes($itemData);
        $itemExt->setPrescriptionDetails($prescriptionData);
        return $this;
    }

    /**
     * @param $sku
     * @return string|null
     * @throws NoSuchEntityException
     */
    public function getProductImageBySku($sku)
    {
        $productRepo = $this->productRepository->get($sku);
        $imageUrl = null;
        if ($productRepo->getImage() != null) {
            $imageUrl = $productRepo->getMediaConfig()->getMediaUrl($productRepo->getImage());
        }
        return $imageUrl;
    }

    /**
     * @param OrderItemInterface $item
     * @return null
     */
    public function getItemData(OrderItemInterface $item)
    {
        $itemModel = $this->orderHelper->getOrderItemsModelByItemId($item->getItemId());
        if ($itemModel->getId() != null) {
            /**
             * Age and Sex
             */
            $addressId = $itemModel->getData('address_id');
            try {
                $address = $this->addressRepository->getById($addressId);
                $ageObj = $address->getCustomAttribute('age');
                $sexObj = $address->getCustomAttribute('sex');
                $age = ($ageObj != null) ? $ageObj->getValue() : null;
                $sex = ($sexObj != null) ? $sexObj->getValue() : null;
                $patientName = $address->getFirstname();
            } catch (\Exception $e) {
                $age = $patientName = $sex = null;
            }
            $vendorTypeId = $itemModel->getData('vendor_type');
            $vendorTypeLabel = $this->vendorHelper->getVendorTypeNameById($vendorTypeId);

            $customItem = $this->itemDetailsInterfaceFactory->create();
            $customItem->setOrderItemStatus($itemModel->getData('order_item_status'));
            $customItem->setDeliveryDate($itemModel->getData('delivery_date'));
            $customItem->setVendor($itemModel->getData('vendor'));
            $customItem->setTimeSlotFrom($itemModel->getData('time_slot_from'));
            $customItem->setTimeSlotTo($itemModel->getData('time_slot_to'));
            $customItem->setConsultationType($itemModel->getData('consultation_type'));
            $customItem->setVendorType($itemModel->getData('vendor_type'));
            $customItem->setVendorTypeLabel($vendorTypeLabel);
            $customItem->setAge($age);
            $customItem->setSex($sex);
            $customItem->setPatientName($patientName);
            /* $itemData[] = $customItem;*/
            return $customItem;
        }
        return null;
    }

    /**
     * @param OrderItemInterface $item
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function getBoxData(OrderItemInterface $item)
    {
        $boxCollection = $this->orderHelper->getBoxCollectionByItemId($item->getItemId());
        if ($boxCollection != null) {
            $boxData = [];
            foreach ($boxCollection as $oneBox) {
                $box = $this->boxDetailsInterfaceFactory->create();
                $box->setBoxId($oneBox->getData('box_id'));
                $box->setBoxStatus(ucfirst($oneBox->getData('status')));
                $box->setTrackingNumber($oneBox->getData('track_id'));
                $imageUrl = $this->getProductImageBySku($item->getSku());
                $box->setProductName($item->getName());
                $box->setImage($imageUrl);
                $boxData[] = $box;
            }
            return $boxData;
        }
        return null;
    }

    /**
     * @param OrderItemInterface $item
     * @return array
     */
    public function getPrescriptionDataForItem(OrderItemInterface $item)
    {
        $prescriptionData = [];
        try {
            $prescriptionIdArray = explode(",", $item->getData('prescription_id'));
            if (count($prescriptionIdArray) > 0) {
                $mediaUrl = $this->orderHelper->getPrescriptionMediaUrl();
                foreach ($prescriptionIdArray as $id) {
                    $prescriptionFactory = $this->prescriptionInterfaceFactory->create();
                    $prescription = $this->orderHelper->getPrescriptionModelById($id);
                    $fileUrl = $mediaUrl . $prescription->getData('filename');
                    $prescriptionFactory->setPrescriptionId($id);
                    $prescriptionFactory->setPrescriptionUrl($fileUrl);
                    $prescriptionFactory->setType($prescription->getData('type'));
                    $prescriptionFactory->setUploadedDate($prescription->getData('created_at'));
                    $prescriptionFactory->setUploadedBy($prescription->getData('uploaded_by'));
                    $prescriptionFactory->setUsername($prescription->getData('username'));
                    $prescriptionData[] = $prescriptionFactory;
                }
            }
        } catch (\Exception $e) {
            return [];
        }
        return $prescriptionData;
    }

    /**
     * @param OrderInterface $order
     * @return PrescriptionInterface|null
     */
    public function getPrescriptionDataForOrder(OrderInterface $order)
    {
        try {
            $prescriptionFactory = $this->prescriptionInterfaceFactory->create();
            $prescriptionId = $order->getData('prescription_id');
            if ($prescriptionId != '') {
                $prescriptionIdUrlArray = $this->orderHelper->getPrescriptionUrlById($prescriptionId);
                if (isset($prescriptionIdUrlArray[$prescriptionId])) {
                    $prescriptionFactory->setPrescriptionId($prescriptionId);
                    $prescriptionFactory->setPrescriptionUrl($prescriptionIdUrlArray[$prescriptionId]);
                }
            }
        } catch (\Exception $e) {
            return null;
        }
        return $prescriptionFactory;
    }

    /**
     * Return Custom Attributes Extension
     * @param OrderInterface $order
     * @return array
     */
    public function getPrescriptionDetails(OrderInterface $order)
    {
        $prescriptionData = [];
        /*$customAttributes = $this->customOrderAttributeInterfaceFactory->create();*/
        $prescriptionId = $order->getData('prescription_id');
        $prescriptionData['id'] = $prescriptionId;
        $prescriptionUrlData = $this->orderHelper->getPrescriptionUrlById($prescriptionId);
        $url = null;
        if (isset($prescriptionUrlData[$prescriptionId])) {
            $url = $prescriptionUrlData[$prescriptionId];
        }
        $prescriptionData['url'] = $url;
        return $prescriptionData;
    }

    /**
     * Send Payment Details
     *
     * @param OrderInterface $order
     * @return PaymentDataInterface
     */
    public function getPaymentData(OrderInterface $order)
    {
        $paymentData = $this->paymentDataInterfaceFactory->create();
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = $method->getTitle();
        $cardType = $payment->getCcType();

        $additionalInfo = $payment->getAdditionalInformation();
        if ($additionalInfo != null) {
            if (isset($additionalInfo['raw_details_info']['accountNumber'])) {
                $paymentData->setCardNumber($additionalInfo['raw_details_info']['accountNumber']);
            }
        }
        $paymentData->setPaymentMethod($methodTitle);
        $paymentData->setCardType($cardType);
        return $paymentData;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
    }
}