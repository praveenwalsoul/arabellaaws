<?php

namespace Flordelcampo\Api\Model\CartManagement;

use Flordelcampo\Api\Api\CartManagementInterface;
use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\Api\Controller\BaseController;
use Magento\Customer\Model\Customer;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote;
use Magento\Store\Api\Data\StoreInterface;

/**
 * Class Cart
 * Flordelcampo\Api\Model\CartManagement
 */
class Cart extends BaseController implements CartManagementInterface
{
    /**
     * @var null
     */
    private $_inputParams = null;

    /**
     * @var string[]
     */
    private $_customAttributesArray = [
        'vendor',
        'vendor_farm',
        'delivery_date',
        'pickup_date',
        'shipping_method',
        'time_slot_from',
        'time_slot_to',
        'consultation_type',
        'pin_code',
        'address_id',
        'gift_message',
        'enquiryperson',
        'enquiryemail',
        'enquirphone',
        'organizedby',
        'venue',
        'confdate'
    ];
    const FLAT_RATE_PRICE = 'carriers/flatrate/price';

    /**
     * Add to cart
     *
     * @return array|ApiResponseInterface
     */
    public function addToCart()
    {
        $response = $result = $data = [];
        try {
            $this->validateParams();
            $params = $this->_inputParams;
            $customer = $this->getCustomerByToken();
            /**
             * Get Active quote Id or create new quote
             */
            $cartId = $this->getActiveQuoteForCustomer($customer);
            $quote = $this->cartRepositoryInterface->getActive($cartId);

            $sku = $params['cartItem']['sku'];

            $qty = (int)$params['cartItem']['qty'];
            if ($qty == 0 || $qty == '') {
                $qty = 1;
            }
            $productRepo = $this->productRepository->get($sku);
            $product = $this->product->load($productRepo->getId());

            $quote->addProduct($product, (int)$qty);
            $this->cartRepositoryInterface->save($quote);
            $quote = $this->quoteRepository->get($quote->getId());

            $quoteItem = $this->getLatestQuoteItem($quote);

            /**
             * Update cart price based on Consultation Type
             */
            if (isset($params['cartItem']['custom_attributes']['consultation_type'])) {
                $consultationType = $params['cartItem']['custom_attributes']['consultation_type'];
                if (strtolower($consultationType) == 'online') {
                    $onlinePrice = $product->getData('online_price');
                    if ($onlinePrice != 0 && $onlinePrice != '') {
                        $quoteItem->setCustomPrice($onlinePrice);
                        $quoteItem->setOriginalCustomPrice($onlinePrice);
                        $quoteItem->getProduct()->setIsSuperMode(true);
                        $quoteItem->save();
                    }
                }
            }

            /**
             * Add Attribute set Id to quote
             */
            $vendorType = 'vendor_type';
            if ($quoteItem->hasData($vendorType)) {
                $quoteItem->setData($vendorType, $productRepo->getAttributeSetId());
            }
            $prescriptionReq = 'prescription_required';
            if ($quoteItem->hasData($prescriptionReq)) {
                $quoteItem->setData($prescriptionReq, $productRepo->getPrescription());
            }
            /**
             * Add Custom Product Attributes to quote
             */
            $customProductAttributesArray = [
                'enquiryperson',
                'enquiryemail',
                'enquirphone',
                'organizedby',
                'venue',
                'confdate'
            ];
            foreach ($customProductAttributesArray as $productAttributeCode) {
                if ($quoteItem->hasData($productAttributeCode)) {
                    $quoteItem->setData($productAttributeCode, $productRepo->getData($productAttributeCode));
                }
            }

            /**
             * Add custom attributes to quote
             */
            if (isset($params['cartItem']['custom_attributes'])) {
                $customAttributesArray = $params['cartItem']['custom_attributes'];
                foreach ($customAttributesArray as $attribute => $value) {
                    if ($quoteItem->hasData($attribute)) {
                        $quoteItem->setData($attribute, $value);
                    }
                }
                $quoteItem->save();
            }
            $quote->save();
            $quote->collectTotals()->save();
            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $itemsArray[] = $itemData = $this->getQuoteItemData($quoteItem);
            }
            /*$itemData = $this->getQuoteItemData($quoteItem);*/
            $totalData = $this->getTotalsByQuote($quote);

            $response = [
                'status' => true,
                'message' => __('Item added to cart successfully'),
                'quote_id' => $quote->getId(),
                'items' => $itemsArray,
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Get Cart List
     * @return array|ApiResponseInterface
     */
    public function getCartList()
    {
        $response = $result = $data = [];
        try {

            $customer = $this->getCustomerByToken();
            /**
             * check active cart is there for customer
             */
            $quoteCollection = $this->quoteFactory->create()->getCollection()
                ->addFieldToFilter('customer_id', $customer->getId())
                ->addFieldToFilter('is_active', '1');
            if ($quoteCollection->getSize() == 0) {
                throw new LocalizedException(__("This customer doesn't have any cart"));
            }
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customer->getId());
            $quote = $this->quoteRepository->get($quote->getId());

            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $itemsArray[] = $itemData = $this->getQuoteItemData($quoteItem);

            }
            $totalData = $this->getTotalsByQuote($quote);

            $response = [
                'status' => true,
                'quote_id' => $quote->getId(),
                'items' => $itemsArray,
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Update Cart
     *
     * @param integer $itemId
     * @return array|ApiResponseInterface
     */
    public function updateCart($itemId)
    {
        $response = $result = $data = [];
        try {
            $this->validateParams();
            $params = $this->_inputParams;
            $customer = $this->getCustomerByToken();
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customer->getId());
            $quote = $this->quoteRepository->get($quote->getId());
            $qty = (int)$params['cartItem']['qty'];
            if ($qty == 0 || $qty == '') {
                $qty = 1;
            }
            $quoteItem = $quote->getItemById($itemId);
            if ($quoteItem == null) {
                throw new LocalizedException(__("This item doesn't exist in the cart"));
            }
            $quoteItem->setQty($qty)->save();
            if (isset($params['cartItem']['custom_attributes'])) {
                $quoteItem = $quote->getItemById($itemId);
                $customAttributesArray = $params['cartItem']['custom_attributes'];
                foreach ($customAttributesArray as $attribute => $value) {
                    if ($quoteItem->hasData($attribute)) {
                        $quoteItem->setData($attribute, $value);
                    }
                }
                $quoteItem->save();
            }
            $quote->save();
            $quote->collectTotals()->save();
            /*$itemData = $this->getQuoteItemData($quoteItem);*/
            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $itemsArray[] = $itemData = $this->getQuoteItemData($quoteItem);

            }
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => true,
                'message' => __('Cart updated successfully'),
                'quote_id' => $quote->getId(),
                'items' => $itemsArray,
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Add Gift Message
     * @return ApiResponseInterface|mixed
     */
    public function addGiftMessage()
    {
        try {
            $inputParams = $this->request->getBodyParams();
            if (isset($inputParams['item_id'])) {
                if (isset($inputParams['gift_message'])) {
                    $itemId = $inputParams['item_id'];
                    $msg = $inputParams['gift_message'];
                    $quote = $this->getQuoteByItemId($itemId);
                    $quoteItem = $quote->getItemById($itemId);
                    if ($quoteItem->hasData('gift_message')) {
                        $quoteItem->setData('gift_message', $msg);
                    }
                    $quoteItem->save();
                }
            } else {
                throw new LocalizedException(__("This item doesn't exist in the cart"));
            }
            $response = [
                'status' => true,
                'message' => __('Gift message updated successfully')
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Delete All cart items
     *
     * @return ApiResponseInterface|array
     */
    public function deleteAllCartItems()
    {
        $response = $result = $data = [];
        try {
            $customer = $this->getCustomerByToken();
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customer->getId());
            $quote = $this->quoteRepository->getActive($quote->getId());

            foreach ($quote->getAllVisibleItems() as $item) {
                try {
                    $quote->removeItem($item->getItemId());
                    $this->quoteRepository->save($quote);
                } catch (\Exception $e) {
                    continue;
                }
            }
            $quote->save();
            $quote->collectTotals()->save();
            $response = [
                'status' => true,
                'message' => __('Cart items deleted successfully'),
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Delete Cart by Item Id
     * @param int $cartItemId
     * @return array|ApiResponseInterface
     */
    public function deleteCartItemByItemId($cartItemId)
    {
        $response = $result = $data = [];
        try {
            $customer = $this->getCustomerByToken();
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customer->getId());
            $quote = $this->quoteRepository->getActive($quote->getId());
            $quoteItem = $quote->getItemById($cartItemId);
            if (!$quoteItem) {
                throw new NoSuchEntityException(
                    __("This item doesn't exist in the cart")
                );
                /*throw new NoSuchEntityException(
                    __('The Cart doesn\'t contain the %1 item.', $cartItemId)
                );*/
            }
            try {
                $quote->removeItem($cartItemId);
                $this->quoteRepository->save($quote);
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__("The item couldn't be removed from the quote."));
            }
            $quote->save();
            $quote->collectTotals()->save();
            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $itemsArray[] = $itemData = $this->getQuoteItemData($quoteItem);

            }
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => true,
                'message' => __('Item deleted successfully'),
                'items' => $itemsArray,
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Apply Coupon code to cart
     *
     * @param string $couponCode
     * @return ApiResponseInterface|array
     */
    public function applyCouponCode($couponCode)
    {
        $response = $result = $data = [];
        $quote = null;
        try {
            $customer = $this->getCustomerByToken();
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customer->getId());
            $isSuccess = $this->couponManagement->set($quote->getId(), $couponCode);
            $quote = $this->quoteRepository->get($quote->getId());
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => true,
                'message' => __('Coupon applied successfully'),
                'quote_id' => $quote->getId(),
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
                'quote_id' => $quote->getId(),
                'totals' => $totalData
            ];
        } catch (\Exception $e) {
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
                'quote_id' => $quote->getId(),
                'totals' => $totalData
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Remove coupon code from the cart
     * @return array|ApiResponseInterface
     */
    public function removeCouponCode()
    {
        $response = $result = $data = [];
        try {
            $customer = $this->getCustomerByToken();
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customer->getId());
            $isSuccess = $this->couponManagement->remove($quote->getId());
            $quote = $this->quoteRepository->get($quote->getId());
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => true,
                'message' => __('Coupon removed successfully'),
                'quote_id' => $quote->getId(),
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Get cart Details for Guest
     * @param string $maskID
     * @return ApiResponseInterface|array
     */
    public function getGuestCartList($maskID)
    {
        $response = $result = $data = [];
        try {
            $cartId = $this->validateMaskIdAndReturnQuoteId($maskID);
            $quote = $this->quoteRepository->get($cartId);
            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $itemsArray[] = $itemData = $this->getQuoteItemData($quoteItem);
            }
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => true,
                'quote_id' => $quote->getId(),
                'mask_id' => $maskID,
                'items' => $itemsArray,
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Add to cart for guest
     *
     * @return array|ApiResponseInterface|string
     */
    public function addToCartForGuest()
    {
        $response = $result = $data = [];
        try {
            $this->validateParams();
            $params = $this->_inputParams;

            if (isset($params['cartItem']['mask_id'])) {
                $maskId = trim($params['cartItem']['mask_id']);
                if ($maskId == null || $maskId == '') {
                    $maskId = $this->createMaskIdForGuestCart();
                }
            } else {
                $maskId = $this->createMaskIdForGuestCart();
            }

            /**
             * Even if mask id is invalid create new mask id
             */
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskId, 'masked_id');
            if ($quoteIdMask->getQuoteId() != null) {
                $cartId = $quoteIdMask->getQuoteId();
            } else {
                $maskId = $this->createMaskIdForGuestCart();
            }
            $cartId = $this->validateMaskIdAndReturnQuoteId($maskId);
            /**
             * Add to cart
             */
            $quote = $this->cartRepositoryInterface->getActive($cartId);
            $sku = $params['cartItem']['sku'];
            $qty = (int)$params['cartItem']['qty'];
            if ($qty == 0 || $qty == '') {
                $qty = 1;
            }
            $productRepo = $this->productRepository->get($sku);
            $product = $this->product->load($productRepo->getId());
            $quote->addProduct($product, (int)$qty);
            $this->cartRepositoryInterface->save($quote);
            $quote = $this->quoteRepository->get($quote->getId());

            /**
             * Add custom attributes to quote
             */
            $quoteItem = $this->getLatestQuoteItem($quote);
            /**
             * Add Attribute set Id to quote
             */
            $vendorType = 'vendor_type';
            if ($quoteItem->hasData($vendorType)) {
                $quoteItem->setData($vendorType, $productRepo->getAttributeSetId());
            }
            /**
             * Add Custom Product Attributes to quote
             */
            $customProductAttributesArray = [
                'enquiryperson',
                'enquiryemail',
                'enquirphone',
                'organizedby',
                'venue',
                'confdate'
            ];
            foreach ($customProductAttributesArray as $productAttributeCode) {
                if ($quoteItem->hasData($productAttributeCode)) {
                    $quoteItem->setData($productAttributeCode, $productRepo->getData($productAttributeCode));
                }
            }

            if (isset($params['cartItem']['custom_attributes'])) {
                $customAttributesArray = $params['cartItem']['custom_attributes'];
                foreach ($customAttributesArray as $attribute => $value) {
                    if ($quoteItem->hasData($attribute)) {
                        $quoteItem->setData($attribute, $value);
                    }
                }
                $quoteItem->save();
            }
            $quote->save();
            $quote->collectTotals()->save();
            /*$itemData = $this->getQuoteItemData($quoteItem);*/
            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $itemsArray[] = $itemData = $this->getQuoteItemData($quoteItem);

            }
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => true,
                'message' => __('Item added to cart successfully'),
                'mask_id' => $maskId,
                'quote_id' => $quote->getId(),
                'items' => $itemsArray,
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Delete cart item for guest
     * @param integer $cartItemId
     * @return ApiResponseInterface|array
     */
    public function deleteCartItemByItemIdForGuest($cartItemId)
    {
        $response = $result = $data = [];
        try {
            $quote = $this->getQuoteByItemId($cartItemId);
            $quote = $this->quoteRepository->getActive($quote->getId());
            $quoteItem = $quote->getItemById($cartItemId);
            if (!$quoteItem) {
                throw new NoSuchEntityException(
                    __("This item doesn't exist in the cart")
                );
            }
            try {
                $quote->removeItem($cartItemId)->save();
                /*$this->quoteRepository->save($quote);*/
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__("The item couldn't be removed from the quote."));
            }
            $quote->save();
            $quote->collectTotals()->save();
            $totalData = $this->getTotalsByQuote($quote);
            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $itemsArray[] = $itemData = $this->getQuoteItemData($quoteItem);

            }
            $response = [
                'status' => true,
                'message' => __('Item deleted successfully'),
                'items' => $itemsArray,
                'totals' => $totalData
            ];

        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;

    }

    /**
     * Update Cart for guest
     * @param integer $itemId
     * @return array|ApiResponseInterface
     */
    public function updateCartForGuest($itemId)
    {
        $response = $result = $data = [];
        try {
            $this->validateParams();
            $params = $this->_inputParams;

            if (isset($params['cartItem']['mask_id'])) {
                $maskId = trim($params['cartItem']['mask_id']);
                if ($maskId == null && $maskId == '') {
                    throw new LocalizedException(__("This item doesn't exist in the cart"));
                }
            } else {
                throw new LocalizedException(__("This item doesn't exist in the cart"));
            }
            $quoteId = $this->validateMaskIdAndReturnQuoteId($maskId);
            $quote = $this->quoteRepository->get($quoteId);
            $qty = (int)$params['cartItem']['qty'];
            if ($qty == 0 || $qty == '') {
                $qty = 1;
            }
            $quoteItem = $quote->getItemById($itemId);
            if ($quoteItem == null) {
                throw new LocalizedException(__("This item doesn't exist in the cart"));
            }
            $quoteItem->setQty($qty)->save();
            if (isset($params['cartItem']['custom_attributes'])) {
                $quoteItem = $quote->getItemById($itemId);
                $customAttributesArray = $params['cartItem']['custom_attributes'];
                foreach ($customAttributesArray as $attribute => $value) {
                    if ($quoteItem->hasData($attribute)) {
                        $quoteItem->setData($attribute, $value);
                    }
                }
                $quoteItem->save();
            }
            $quote->save();
            $quote->collectTotals()->save();
            /*$itemData = $this->getQuoteItemData($quoteItem);*/
            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $itemsArray[] = $itemData = $this->getQuoteItemData($quoteItem);

            }
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'status' => true,
                'message' => __('Cart updated successfully'),
                'quote_id' => $quote->getId(),
                'items' => $itemsArray,
                'totals' => $totalData
            ];

        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Convert Guest Cart into Customer cart
     *
     * @return bool|void
     */
    public function assignCustomer()
    {
        try {
            $params = $this->request->getBodyParams();
            if (isset($params['mask_id']) && $params['mask_id'] != null) {
                $maskId = $params['mask_id'];
                $quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskId, 'masked_id');
                if ($quoteIdMask->getQuoteId() != null) {
                    $customer = $this->getCustomerByToken();
                    $quoteId = $quoteIdMask->getQuoteId();
                    $customerId = $customer->getId();
                    $storeId = $this->getStore()->getId();
                    return $this->cartManagementInterface->assignCustomer($quoteId, $customerId, $storeId);
                }

            } else {
                return true;
            }
        } catch (LocalizedException $e) {
            return true;
        } catch (\Exception $e) {
            return true;
        }
        return true;
    }

    /**
     * Validate input params
     * @return bool
     * @throws LocalizedException
     */
    public function validateParams()
    {
        $params = $this->request->getBodyParams();
        if (isset($params['cartItem'])) {
            if (isset($params['cartItem']['sku']) && isset($params['cartItem']['qty'])) {
                $this->_inputParams = $params;
                return true;
            }
        }
        throw new LocalizedException(__("Invalid input parameters."));
    }

    /**
     * Get customer Data by token
     * @return Customer
     * @throws LocalizedException
     */
    public function getCustomerByToken()
    {
        return $this->apiHelper->getCustomerByRequestHeaders($this->request->getHeaders()->toArray());
    }

    /**
     * Get store
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }

    /**
     * Return Active quote if exist or else create new for customer
     * @param $customer
     * @return int
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException|LocalizedException
     */
    public function getActiveQuoteForCustomer($customer)
    {
        try {
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customer->getId());
        } catch (NoSuchEntityException $e) {
            $customer = $this->customerRepository->getById($customer->getId());
            $quote = $this->quoteFactory->create();
            $quote->setStoreId($this->getStore()->getId());
            $quote->setCustomer($customer);
            $quote->setCustomerIsGuest(0);
        }
        try {
            $this->cartRepositoryInterface->save($quote);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__("The quote can't be created."));
        }
        return (int)$quote->getId();
    }

    /**
     * Return Latest Quote Item Object By id
     * @param $quote
     *
     * @return mixed
     */
    public function getLatestQuoteItem($quote)
    {
        $items = $quote->getAllItems();
        $maxId = 0;
        foreach ($items as $item) {
            if ($item->getId() > $maxId) {
                $maxId = $item->getId();
            }
        }
        $lastItemId = $maxId;
        return $quote->getItemById($lastItemId);
    }

    /**
     * Return Quote Data
     * @param $quoteItem
     * @return array
     * @throws NoSuchEntityException
     */
    public function getQuoteItemData($quoteItem)
    {
        $data = [];
        $sku = $quoteItem->getSku();
        $productId = $quoteItem->getProductId();
        $productRepo = $this->productRepository->get($sku);
        $productObj = $this->product->load($productId);
        $images = $productObj->getMediaGalleryImages();
        $imageUrl = null;
        foreach($images as $child){
            $imageUrl= $child->getUrl();
            break;
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $attributeSetRepository = $objectManager->getInstance()->get(\Magento\Eav\Api\AttributeSetRepositoryInterface::class);
        $data['item_id'] = $quoteItem->getId();
        $data['quote_id'] = $quoteItem->getQuoteId();
        $data['sku'] = $sku;
        $data['product_id'] = $productId;
        $data['product_type'] = $quoteItem->getProductType();

        $vendorType = 'vendor_type';
        $cartAttributeSetId = $quoteItem->getData($vendorType);
        $attributeSet = $attributeSetRepository->get($cartAttributeSetId);
        $data['attribute_set'] = [
            'id' => $cartAttributeSetId,
            'name' => $attributeSet->getAttributeSetName()
        ];

        $prescriptionReq = 'prescription_required';
        if ($quoteItem->hasData($prescriptionReq)) {
            $data['prescription_required'] = $quoteItem->getData($prescriptionReq);
        }
        $data['qty'] = (int)$quoteItem->getQty();
        $data['name'] = $quoteItem->getName();
        
        /*if ($productRepo->getImage() != null) {
            $imageUrl = $productRepo->getMediaConfig()->getMediaUrl($productRepo->getImage());
        }*/
        $data['image'] = $imageUrl;
        $data['price'] = number_format($quoteItem->getPrice(), 2);
        $data['row_total'] = number_format($quoteItem->getRowTotal(), 2);
        $customAttributesData = [];
        foreach ($this->_customAttributesArray as $attribute) {
            $customAttributesData[$attribute] = $quoteItem->getData($attribute);
        }
        $data['custom_attributes'] = $customAttributesData;
        return $data;
    }

    /**
     * Return Totals of cart
     * @param Quote $quote
     * @return array
     */
    public function getTotalsByQuote(\Magento\Quote\Model\Quote $quote)
    {
        $totals = [];
        $discountAmount = 0;
        if ($quote->getAllVisibleItems() == null) {
            return [];
        }
        foreach ($quote->getAllVisibleItems() as $item) {
            $discountAmount += $item->getDiscountAmount();
        }
        /**
         * Get Flat rate shipping price per item
         */
        $shippingPricePerItem = $this->calendarHelper->getConfig(self::FLAT_RATE_PRICE);
        $shippingAmount = $quote->getShippingAddress()->getShippingAmount();
        $totals['items_count'] = (int)$quote->getData('items_count');
        $totals['items_qty'] = (int)$quote->getData('items_qty');
        $totals['subtotal'] = number_format($quote->getData('subtotal'), 2);
        $totals['discount'] = number_format($discountAmount, 2);
        $totals['coupon_code'] = $quote->getData('coupon_code');
        $totals['shipping_amount_per_item'] = $shippingPricePerItem;
        $totals['shipping_amount'] = number_format($shippingAmount, 2);
        $totals['grand_total'] = number_format($quote->getData('grand_total'), 2);
        return $totals;
    }

    /**
     * Return Customer Cart Details By Customer ID
     * @param $customerId
     * @return ApiResponseInterface|array
     */
    public function getItemsByCustomerId($customerId)
    {
        $result = $response = [];
        try {
            $quoteCollection = $this->quoteFactory->create()->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('is_active', '1');
            if ($quoteCollection->getSize() == 0) {
                return [];
            }
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customerId);
            $quote = $this->quoteRepository->get($quote->getId());
            if ($quote->getAllVisibleItems() == null) {
                return [];
            }
            $itemsArray = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItem = $quote->getItemById($item->getId());
                $data = [];
                $sku = $quoteItem->getSku();
                $productId = $quoteItem->getId();
                $productRepo = $this->productRepository->get($sku);
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $attributeSetRepository = $objectManager->getInstance()->get(\Magento\Eav\Api\AttributeSetRepositoryInterface::class);
                $data['item_id'] = $quoteItem->getId();
                $data['quote_id'] = $quoteItem->getQuoteId();
                $data['sku'] = $sku;
                $data['product_id'] = $productId;
                $data['product_type'] = $quoteItem->getProductType();

                $vendorType = 'vendor_type';
                $cartAttributeSetId = $quoteItem->getData($vendorType);
                $attributeSet = $attributeSetRepository->get($cartAttributeSetId);
                $data['attribute_set'] = [
                    'id' => $cartAttributeSetId,
                    'name' => $attributeSet->getAttributeSetName()
                ];
                $prescriptionReq = 'prescription_required';
                if ($quoteItem->hasData($prescriptionReq)) {
                    $data['prescription_required'] = $quoteItem->getData($prescriptionReq);
                }
                $data['qty'] = (int)$quoteItem->getQty();
                $data['name'] = $quoteItem->getName();
                $imageUrl = null;
                if ($productRepo->getImage() != null) {
                    $imageUrl = $productRepo->getMediaConfig()->getMediaUrl($productRepo->getImage());
                }
                $data['image'] = $imageUrl;
                $data['price'] = number_format($quoteItem->getPrice(), 2);
                $data['row_total'] = number_format($quoteItem->getRowTotal(), 2);
                /* $customAttributesData = [];
                 foreach ($this->_customAttributesArray as $attribute) {
                     $customAttributesData[$attribute] = $quoteItem->getData($attribute);
                 }
                 $data['custom_attributes'] = $customAttributesData;*/
                $itemsArray[] = $data;
            }
            $totalData = $this->getTotalsByQuote($quote);
            $response = [
                'quote_id' => $quote->getId(),
                'cartItems' => $itemsArray,
                'totals' => $totalData
            ];
        } catch (LocalizedException $e) {
            return [];
        } catch (\Exception $e) {
            return [];
        }

        $result['response'] = $response;
        return $result;
    }

    /**
     * Crete a new mask ID
     * @return string
     * @throws NoSuchEntityException
     */
    public function createMaskIdForGuestCart()
    {
        $quote = $this->quoteFactory->create();
        $quote->setStoreId($this->getStore()->getId())->save();
        $quoteIdMask = $this->quoteIdMaskFactory->create();
        $quoteIdMask->setQuoteId($quote->getId())->save();
        return $quoteIdMask->getMaskedId();
    }

    /**
     * Validate Mask id and return Quote Id
     * @param $maskId
     * @return mixed
     * @throws LocalizedException
     */
    public function validateMaskIdAndReturnQuoteId($maskId)
    {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskId, 'masked_id');
        if ($quoteIdMask->getQuoteId() != null) {
            return $quoteIdMask->getQuoteId();
        } else {
            throw new LocalizedException(__("Item doesn't exist for this cart"));
        }
    }

    /**
     * Return Quote by Quote Item ID
     * @param $quoteItemId
     * @return Quote|null
     * @throws LocalizedException
     */
    public function getQuoteByItemId($quoteItemId)
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                'quote_item',
                ['quote_id']
            )->where('item_id = ?', $quoteItemId);

        $quoteId = $connection->fetchOne($select);
        if (!empty($quoteId)) {
            return $this->quote->load($quoteId);
        } else {
            throw new LocalizedException(__("This item doesn't exist in the cart"));
        }
    }

    public function execute()
    {
        /*$response = [];
        try {
            // throw new LocalizedException(__("Invalid username or password"));
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;*/
    }

}