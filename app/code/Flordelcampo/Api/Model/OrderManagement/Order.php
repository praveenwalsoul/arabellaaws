<?php

namespace Flordelcampo\Api\Model\OrderManagement;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\Api\Api\OrderManagementInterface;
use Flordelcampo\Api\Controller\BaseController;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Order
 * Flordelcampo\Api\Model\OrderManagement
 */
class Order extends BaseController implements OrderManagementInterface
{
    /**
     * @var null
     */
    private $_inputParams = null;
    /**
     * @var null
     */
    private $_paymentMethod = null;

    /**
     * Place Order
     * @return array|mixed|void
     */
    public function placeOrder()
    {
        try {
            $result = [];
            $params = $this->request->getBodyParams();
            $this->_inputParams = $params;
            $customerId = $this->getCustomerIdByToken();
            $quote = $this->cartRepositoryInterface->getActiveForCustomer($customerId);

            $paymentMethod = $this->getCurrentPaymentMethod();
            $quote = $this->quote->load($quote->getId());
            /**
             * Initially place order with cash on delivery
             * And then set respective payment method
             */
            $quote->getPayment()->importData(['method' => 'cashondelivery']);
            $order = $this->orderManagement->submit($quote);
            $order->setState("processing")->setStatus("processing");
            $order->setEmailSent(1);
            /**
             * Save Prescription Id
             */
            if (isset($params['prescription_id'])) {
                $order->setData('prescription_id', $params['prescription_id']);
                foreach ($order->getAllVisibleItems() as $item) {
                    $item->setData('prescription_id', $params['prescription_id'])->save();
                }
            }
            $order->save();
            switch ($paymentMethod) {
                case 'authorizenet_acceptjs':
                    $this->saveTransactionForAuthorizeNet($order);
                    break;
                case 'razorpay':
                    $this->saveTransactionForRazorPay($order);
                    break;
                default:
            }
            $response = [
                "status" => true,
                "message" => __('Order created successfully'),
                "order_number" => $order->getIncrementId()
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => __('Order is not placed. Please contact customer support')
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Current Payment Method
     *
     * @return mixed|null
     * @throws LocalizedException
     */
    public function getCurrentPaymentMethod()
    {
        $params = $this->_inputParams;
        $paymentParams = $params['payment'];
        if (isset($paymentParams['method'])) {
            if ($paymentParams['method'] != '' && $paymentParams['method'] != null) {
                $this->_paymentMethod = $paymentParams['method'];
                return $paymentParams['method'];
            }
        } else {
            throw new LocalizedException(__('Payment method is empty'));
        }
    }

    /**
     * Save Authorize Net Transaction
     * @param $order
     * @return bool
     */
    public function saveTransactionForAuthorizeNet(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        try {
            $params = $this->_inputParams;
            $paymentMethod = $this->_paymentMethod;
            $paymentParams = (isset($params['payment']['transactionResponse']))
                ? $params['payment']['transactionResponse'] : [];
            $transactionId = (isset($paymentParams['transId'])) ? $paymentParams['transId'] : null;
            $avsResultCode = (isset($paymentParams['avsResultCode'])) ? $paymentParams['avsResultCode'] : null;
            $accountType = (isset($paymentParams['accountType'])) ? $paymentParams['accountType'] : null;
            $accountNumber = (isset($paymentParams['accountNumber'])) ? $paymentParams['accountNumber'] : null;
            $refId = (isset($paymentParams['refId'])) ? $paymentParams['refId'] : null;
            if ($accountNumber != null) {
                $accountNumber = substr($accountNumber, -4);
            }
            /**
             * Save Payment
             */
            $payment = $order->getPayment();
            $payment->setMethod($paymentMethod);
            $payment->setCcLast4($accountNumber);
            $payment->setCcAvsStatus($avsResultCode);
            $payment->setCcType($accountType);
            $payment->setPoNumber($refId);
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array)$paymentParams]
            );
            $trans = $this->transactionBuilder;
            $transaction = $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($transactionId)
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array)$paymentParams]
                )
                ->setFailSafe(true)
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);
            $payment->save();
            $order->save();
            return true;
        } catch (\Exception $e) {
            return true;
        }
    }

    /**
     * @param $order
     * @return bool
     */
    public function saveTransactionForRazorPay(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        try {
            $params = $this->_inputParams;
            $paymentParams = $params['payment'];
            $transactionId = (isset($paymentParams['txn_id'])) ? $paymentParams['txn_id'] : null;
            $paymentMethod = $this->_paymentMethod;
            /**
             * Save Payment
             */
            $payment = $order->getPayment();
            $payment->setMethod($paymentMethod);
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array)$paymentParams]
            );
            $trans = $this->transactionBuilder;
            $transaction = $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($transactionId)
                ->setAdditionalInformation(
                    [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array)$paymentParams]
                )
                ->setFailSafe(true)
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);
            $payment->save();
            $order->save();
            return true;
        } catch (\Exception $e) {
            return true;
        }
    }

    /**
     * Get customer Data by token
     * @return mixed|integer
     * @throws LocalizedException
     */
    public function getCustomerIdByToken()
    {
        $customer = $this->apiHelper->getCustomerByRequestHeaders($this->request->getHeaders()->toArray());
        return $customer->getId();
    }

    /**
     * Cancel Box Id
     * @return mixed
     */
    public function cancelBox()
    {
        try {
            $params = $this->request->getBodyParams();
            if (isset($params['box_id'])) {
                $boxIdArray = explode(',', $params['box_id']);
                foreach ($boxIdArray as $boxId) {
                    try {
                        $this->orderHelper->updateStatusByIdAndType('box', $boxId, 'cancelled');
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
            $response = [
                'status' => true,
                'message' => __("Cancelled successfully")
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Update status
     *
     * @return ApiResponseInterface|mixed
     */
    public function updateStatus()
    {
        try {
            $params = $this->request->getBodyParams();
            if (isset($params['type']) && isset($params['id']) && isset($params['status'])) {
                $type = $params['type'];
                $statusToUpdate = $params['status'];
                $idArray = explode(',', $params['id']);
                foreach ($idArray as $id) {
                    try {
                        $this->orderHelper->updateStatusByIdAndType($type, $id, $statusToUpdate);
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            } else {
                throw new LocalizedException(__("Invalid input params"));
            }
            $response = [
                'status' => true,
                'message' => __("Status updated successfully")
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    public function execute()
    {
        /*$response = [];
        try {
            // throw new LocalizedException(__("Invalid username or password"));
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;*/
        // TODO: Implement execute() method.
    }
}
