<?php

namespace Flordelcampo\Api\Model\OrderManagement;

use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;
use Razorpay\Magento\Controller\BaseController;
use Razorpay\Magento\Model\Config;

/**
 * Class Razorpay
 *
 * Flordelcampo\Api\Model\OrderManagement
 */
class Razorpay extends BaseController
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * Razorpay constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param Session $checkoutSession
     * @param Config $config
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        Session $checkoutSession,
        Config $config
    )
    {
        $this->config = $config;
        $this->customerSession = $customerSession;
        parent::__construct($context, $customerSession, $checkoutSession, $config);
    }

    /**
     * Return Razorpay Order Id for current order
     * @throws LocalizedException
     */
    public function getRazorPayOrderId()
    {
        $reservedOrderId = $this->customerSession->getReservedOrderId();
        $grandTotal = $this->customerSession->getCartTotalAmount();
        $currencyCode = $this->customerSession->getCurrencyCode();
        $amount = (int)(round($grandTotal, 2) * 100);
        $payment_action = $this->config->getPaymentAction();
        if ($payment_action === 'authorize') {
            $payment_capture = 0;
        } else {
            $payment_capture = 1;
        }
        $data = [
            'order_id' => $reservedOrderId,
            'amount' => $amount,
            'currency_code' => $currencyCode,
            'payment_action' => $payment_capture
        ];
        try {
            $order = $this->rzp->order->create([
                'amount' => $amount,
                'receipt' => $reservedOrderId,
                'currency' => $currencyCode,
                'payment_capture' => $payment_capture
            ]);
            if (null !== $order && !empty($order->id)) {
                $this->customerSession->unsReservedOrderId();
                $this->customerSession->unsCartTotalAmount();
                $this->customerSession->unsCurrencyCode();
                return $order->id;
            }
        } catch (\Razorpay\Api\Errors\Error $e) {
            return null;
        }
    }

    public function execute()
    {
        // TODO: Implement execute() method.
    }
}