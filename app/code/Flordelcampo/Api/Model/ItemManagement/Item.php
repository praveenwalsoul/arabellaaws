<?php

namespace Flordelcampo\Api\Model\ItemManagement;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\Api\Api\ItemManagementInterface;
use Flordelcampo\Api\Controller\BaseController;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Item
 * Flordelcampo\Api\Model\ItemManagement
 */
class Item extends BaseController implements ItemManagementInterface
{
    /**
     * Add Prescription Id to Item Table
     * @return ApiResponseInterface|void
     */
    public function addPrescriptionIdToItem()
    {
        try {
            $params = $this->request->getBodyParams();
            if (isset($params['item_id'])) {
                $itemIdArray = explode(",", $params['item_id']);
                foreach ($itemIdArray as $itemId) {
                    try {
                        $item = $this->orderHelper->getOrderItemsModelByItemId($itemId);
                        if ($item != null && $item->getId() != null) {
                            if (is_numeric($params['prescription_id'])) {
                                $currentPrescriptionId = $item->getData('prescription_id');
                                if ($currentPrescriptionId == '') {
                                    $item->setData('prescription_id', $params['prescription_id'])->save();
                                } else {
                                    $currentPrescriptionArray = explode(",", $currentPrescriptionId);
                                    if (in_array($params['prescription_id'], $currentPrescriptionArray) == false) {
                                        $newId = $currentPrescriptionId . ',' . $params['prescription_id'];
                                        $item->setData('prescription_id', $newId)->save();
                                    }
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            } else {
                throw new LocalizedException(__("Item Id should not be empty"));
            }
            $response = [
                'status' => true,
                'message' => __('Prescription added successfully'),
            ];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Box Status List
     * @return ApiResponseInterface|mixed
     */
    public function getAllBoxStatusList()
    {
        try {
            $box = $this->_objectManager->create('Flordelcampo\Order\Model\Box');
            $statusArray = $box->getAllBoxStatusArray();
            $boxStatusArray = [];
            foreach ($statusArray as $code => $label) {
                $boxStatusArray[] = [
                    'code' => $code,
                    'label' => $label,
                ];
            }
            $response = ['status' => true, 'list' => $boxStatusArray];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;

    }

    /**
     * Return Mass Order Report
     * @return mixed
     */
    public function getMasterOrderDataReport()
    {
        try {
            $params = $this->request->getParams();
            $finalReport = [];
            $orderAttributes = ['created_at', 'store_id', 'customer_id', 'created_at',
                'increment_id', 'partner_order_id'];
            $itemAttributes = ['sku', 'name', 'price', 'qty_per_box', 'po_unit_price', 'order_item_status',
                'delivery_date', 'pickup_date', 'vendor', 'vendor_farm', 'shipping_method', 'offer_avail_id',
                'promise_delivery_date', 'promise_ship_method', 'partner_po'];
            $boxAttributes = ['box_id', 'order_id', 'status', 'track_id', 'gift_message'];
            if (isset($params['from_date']) && isset($params['to_date'])) {
                $fromDate = $params['from_date'] . ' 00:00:00';
                $toDate = $params['to_date'] . ' 23:59:59';
                $orderCollection = $this->orderHelper->getOrderCollection();
                $orderCollection->addFieldToFilter('created_at', ['gteq' => $fromDate]);
                $orderCollection->addFieldToFilter('created_at', ['lteq' => $toDate]);
                if (isset($params['order_id'])) {
                    $orderCollection->addFieldToFilter('entity_id', ['eq' => $params['order_id']]);
                }
                $orderCollection->addFieldToSelect($orderAttributes);
                if ($orderCollection->getSize() > 0) {
                    /**
                     * Get all track ids status
                     */
                    $tempItemIds = [];
                    $tempBoxAttribute = ['box_id', 'order_id', 'track_id'];
                    foreach ($orderCollection as $order) {
                        foreach ($order->getItems() as $item) {
                            $tempItemIds[] = $item->getId();
                        }
                    }
                    $boxCollection = $this->orderHelper->getBoxCollectionByItemId($tempItemIds);
                    $boxCollection->addFieldToSelect($tempBoxAttribute);
                    $tempAllTrackIds = [];
                    foreach ($boxCollection as $box) {
                        if (!empty($box->getData('track_id'))) {
                            $tempAllTrackIds[] = $box->getData('track_id');
                        }
                    }
                    $firstTrackStatusArray = $this->vendorHelper->getFirstTrackStatusDataByTrackId($tempAllTrackIds);
                    $latestTrackStatusArray = $this->vendorHelper->getLatestTrackStatusByTrackId($tempAllTrackIds);
                    foreach ($orderCollection as $order) {
                        $orderData['store_name']=$order->getData('store_name');
                        $orderData['partner_order_id']=$order->getData('partner_order_id');
                        $orderData['store_id']=$order->getData('store_id');
                        $shippingAddress = $order->getShippingAddress()->toArray();
                        $orderData['shipping_address']['firstname'] = $shippingAddress['firstname'];
                        $orderData['shipping_address']['lastname'] = $shippingAddress['lastname'];
                        $orderData['shipping_address']['telephone'] = $shippingAddress['telephone'];
                        $orderData['shipping_address']['street'] = explode("\n", $shippingAddress['street']);
                        $orderData['shipping_address']['city'] = $shippingAddress['city'];
                        $orderData['shipping_address']['region'] = $shippingAddress['region'];
                        $orderData['shipping_address']['country_id'] = $shippingAddress['country_id'];
                        $orderData['shipping_address']['postcode'] = $shippingAddress['postcode'];
                        $orderData['shipping_address']['email'] = $shippingAddress['email'];

                        $billingAddress = $order->getBillingAddress()->toArray();
                        $orderData['billing_address']['firstname'] = $billingAddress['firstname'];
                        $orderData['billing_address']['lastname'] = $billingAddress['lastname'];
                        $orderData['billing_address']['telephone'] = $billingAddress['telephone'];
                        $orderData['billing_address']['street'] = explode("\n", $billingAddress['street']);
                        $orderData['billing_address']['city'] = $billingAddress['city'];
                        $orderData['billing_address']['region'] = $billingAddress['region'];
                        $orderData['billing_address']['country_id'] = $billingAddress['country_id'];
                        $orderData['billing_address']['postcode'] = $billingAddress['postcode'];
                        $orderData['billing_address']['email'] = $billingAddress['email'];

                        $allItemIds = [];
                        foreach ($order->getItems() as $item) {
                            $allItemIds[] = $item->getId();
                        }
                        $itemCollection = $this->orderHelper->getOrderItemCollection();
                        $itemCollection->addFieldToFilter('main_table.item_id', ['in' => $allItemIds]);
                        $itemCollection->addFieldToSelect($itemAttributes);
                        $itemCollection->getSelect()
                            ->joinLeft(
                                ['vendor' => 'vendor_registrations'],
                                "main_table.vendor=vendor.vendor_id",
                                ['vendor_name' => 'vendor.vendor_name']
                            )->joinLeft(
                                ['location' => 'dropship_vendor_locations'],
                                "main_table.vendor_farm = location.loc_id",
                                ['location_name' => 'location.loc_name']
                            );
                        $itemsData = [];
                        if ($itemCollection->getSize() > 0) {
                            foreach ($itemCollection as $item) {
                                $itemArray = $item->toArray();
                                $sku = $itemArray['sku'];
                                $product = $this->catalogHelper->getProductBySku($sku);
                                $itemArray['asin'] = $product->getAsin();

                                $boxCollection = $this->orderHelper->getBoxCollectionByItemId($item->getId());
                                $boxCollection->addFieldToSelect($boxAttributes);

                                foreach ($boxCollection as $box) {
                                    $temp=[];
                                    $boxData = $box->toArray();
                                    $trackId = $boxData['track_id'];
                                    $boxActualShipDate = null;
                                    $lastTrackStatus = $lastStatusDate = null;
                                    $firstTrackStatusData = (isset ($firstTrackStatusArray[$trackId])) ? $firstTrackStatusArray[$trackId] : [];
                                    if (count($firstTrackStatusData) > 0) {
                                        $firstStatus = $firstTrackStatusData['status'];
                                        $firstTime = $firstTrackStatusData['time'];
                                        if ($firstStatus == 'Order Processed: Ready for UPS'
                                            ||
                                            $firstStatus == 'Shipper created a label, UPS has not received the package yet.'
                                        ) {
                                            $boxActualShipDate = date('Y-m-d', strtotime($firstTime));
                                        }
                                    }
                                    $lastTrackStatusData = (isset ($latestTrackStatusArray[$trackId])) ? $latestTrackStatusArray[$trackId] : [];
                                    if (count($lastTrackStatusData) > 0) {
                                        $lastTrackStatus = $lastTrackStatusData['status'];
                                        $lastTime = $lastTrackStatusData['time'];
                                        $lastStatusDate = date('Y-m-d', strtotime($lastTime));
                                    }
                                    $boxData['last_track_status'] = $lastTrackStatus;
                                    $boxData['last_track_status_date'] = $lastStatusDate;
                                    $boxData['box_ship_date'] = $boxActualShipDate;
                                    $itemArray['box_data'] = $boxData;
                                    $orderData['item_data'] = $itemArray;
                                    $temp['order'] = $orderData;
                                    $finalReport[] = $temp;
                                }
                            }
                        }
                    }
                }
            } else {
                throw new LocalizedException(__("Provide valid from date and to date"));
            }
            $response = ['status' => true, 'order_data' => $finalReport];
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    public function execute()
    {
        /*$response = [];
        try {
            // throw new LocalizedException(__("Invalid username or password"));
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;*/
        // TODO: Implement execute() method.
    }
}