<?php

namespace Flordelcampo\Api\Model\Data;

use Flordelcampo\Api\Api\Data\PrescriptionInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Class PrescriptionDetails
 * Flordelcampo\Api\Model\Data
 */
class PrescriptionDetails extends AbstractExtensibleModel implements PrescriptionInterface
{
    /**
     * @return mixed|null
     */
    public function getPrescriptionId()
    {
        return $this->getData(self::PRESCRIPTION_ID);
    }

    /**
     * @return mixed|null
     */
    public function getPrescriptionUrl()
    {
        return $this->getData(self::PRESCRIPTION_URL);
    }

    /**
     * @param $prescriptionId
     * @return PrescriptionDetails|mixed
     */
    public function setPrescriptionId($prescriptionId)
    {
        return $this->setData(self::PRESCRIPTION_ID, $prescriptionId);
    }

    /**
     * @param $prescriptionUrl
     * @return PrescriptionDetails|mixed
     */
    public function setPrescriptionUrl($prescriptionUrl)
    {
        return $this->setData(self::PRESCRIPTION_URL, $prescriptionUrl);
    }

    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    public function getUploadedDate()
    {
        return $this->getData(self::UPLOADED_DATE);
    }

    public function setUploadedDate($date)
    {
        return $this->setData(self::UPLOADED_DATE, $date);
    }

    public function getUploadedBy()
    {
        return $this->getData(self::UPLOADED_BY);
    }

    public function setUploadedBy($uploadedBy)
    {
        return $this->setData(self::UPLOADED_BY, $uploadedBy);
    }

    public function getUsername()
    {
        return $this->getData(self::USERNAME);
    }

    public function setUsername($username)
    {
        return $this->setData(self::USERNAME, $username);
    }
}