<?php

namespace Flordelcampo\Api\Model\Data;

use Flordelcampo\Api\Api\Data\BoxDetailsInterface;
use Flordelcampo\Api\Api\Data\ItemDetailsInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Class BoxDetails
 * Flordelcampo\Api\Model\Data
 */
class ItemDetails extends AbstractExtensibleModel implements ItemDetailsInterface
{
    /**
     * @return mixed|null
     */
    public function getOrderItemStatus()
    {
        return $this->getData(self::ORDER_ITEM_STATUS);
    }

    /**
     * @return mixed|null
     */
    public function getDeliveryDate()
    {
        return $this->getData(self::DELIVERY_DATE);
    }

    /**
     * @return mixed|null
     */
    public function getVendor()
    {
        return $this->getData(self::VENDOR);
    }

    /**
     * @return mixed|null
     */
    public function getTimeSlotFrom()
    {
        return $this->getData(self::TIME_SLOT_FROM);
    }

    /**
     * @return mixed|null
     */
    public function getTimeSlotTo()
    {
        return $this->getData(self::TIME_SLOT_TO);
    }

    /**
     * @return mixed|null
     */
    public function getConsultationType()
    {
        return $this->getData(self::CONSULTATION_TYPE);
    }

    /**
     * @return mixed|null
     */
    public function getVendorType()
    {
        return $this->getData(self::VENDOR_TYPE);
    }

    /**
     * @return mixed|null
     */
    public function getAge()
    {
        return $this->getData(self::AGE);
    }

    /**
     * @return mixed|null
     */
    public function getSex()
    {
        return $this->getData(self::SEX);
    }

    /**
     * @return mixed|null
     */
    public function getPatientName()
    {
        return $this->getData(self::PATIENT_NAME);
    }

    /**
     * @return mixed|null
     */
    public function getVendorTypeLabel()
    {
        return $this->getData(self::VENDOR_TYPE_LABEL);
    }

    /**
     * @param $orderItemStatus
     * @return ItemDetails|mixed
     */
    public function setOrderItemStatus($orderItemStatus)
    {
        return $this->setData(self::ORDER_ITEM_STATUS, $orderItemStatus);
    }

    /**
     * @param $vendorTypeLabel
     * @return ItemDetails|mixed
     */
    public function setVendorTypeLabel($vendorTypeLabel)
    {
        return $this->setData(self::VENDOR_TYPE_LABEL, $vendorTypeLabel);
    }

    /**
     * @param $deliveryDate
     * @return ItemDetails|mixed
     */
    public function setDeliveryDate($deliveryDate)
    {
        return $this->setData(self::DELIVERY_DATE, $deliveryDate);
    }

    /**
     * @param $vendor
     * @return ItemDetails|mixed
     */
    public function setVendor($vendor)
    {
        return $this->setData(self::VENDOR, $vendor);
    }

    /**
     * @param $timeSlotFrom
     * @return ItemDetails|mixed
     */
    public function setTimeSlotFrom($timeSlotFrom)
    {
        return $this->setData(self::TIME_SLOT_FROM, $timeSlotFrom);
    }

    /**
     * @param $timeSlotTo
     * @return ItemDetails|mixed
     */
    public function setTimeSlotTo($timeSlotTo)
    {
        return $this->setData(self::TIME_SLOT_TO, $timeSlotTo);
    }

    /**
     * @param $consultationType
     * @return ItemDetails|mixed
     */
    public function setConsultationType($consultationType)
    {
        return $this->setData(self::CONSULTATION_TYPE, $consultationType);
    }

    /**
     * @param $vendorType
     * @return ItemDetails|mixed
     */
    public function setVendorType($vendorType)
    {
        return $this->setData(self::VENDOR_TYPE, $vendorType);
    }

    /**
     * @param $age
     * @return ItemDetails|mixed
     */
    public function setAge($age)
    {
        return $this->setData(self::AGE, $age);
    }

    /**
     * @param $sex
     * @return ItemDetails|mixed
     */
    public function setSex($sex)
    {
        return $this->setData(self::SEX, $sex);
    }

    /**
     * @param $patientName
     * @return ItemDetails|mixed
     */
    public function setPatientName($patientName)
    {
        return $this->setData(self::PATIENT_NAME, $patientName);
    }
}