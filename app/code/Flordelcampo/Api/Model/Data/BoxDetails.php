<?php

namespace Flordelcampo\Api\Model\Data;

use Flordelcampo\Api\Api\Data\BoxDetailsInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Class BoxDetails
 * Flordelcampo\Api\Model\Data
 */
class BoxDetails extends AbstractExtensibleModel implements BoxDetailsInterface
{

    /**
     * @return mixed|null
     */
    public function getBoxId()
    {
        return $this->getData(self::BOX_ID);
    }

    /**
     * @return mixed|null
     */
    public function getBoxStatus()
    {
        return $this->getData(self::BOX_STATUS);
    }

    /**
     * @return mixed|null
     */
    public function getTrackingNumber()
    {
        return $this->getData(self::BOX_TRACKING_ID);
    }

    /**
     * @return mixed|null
     */
    public function getProductName()
    {
        return $this->getData(self::PRODUCT_NAME);
    }

    /**
     * @return mixed|null
     */
    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * @return mixed|null
     */
    public function getAmount()
    {
        return $this->getData(self::AMOUNT);
    }

    /**
     * @param $amount
     * @return BoxDetails|mixed
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }

    /**
     * @param $image
     * @return BoxDetails|mixed
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * @param $productName
     * @return BoxDetails|mixed
     */
    public function setProductName($productName)
    {
        return $this->setData(self::PRODUCT_NAME, $productName);
    }

    /**
     * @param $trackingNumber
     * @return BoxDetails|mixed
     */
    public function setTrackingNumber($trackingNumber)
    {
        return $this->setData(self::BOX_TRACKING_ID, $trackingNumber);
    }

    /**
     * @param $boxStatus
     * @return BoxDetails|mixed
     */
    public function setBoxStatus($boxStatus)
    {
        return $this->setData(self::BOX_STATUS, $boxStatus);
    }

    /**
     * @param $boxId
     * @return BoxDetails|mixed
     */
    public function setBoxId($boxId)
    {
        return $this->setData(self::BOX_ID, $boxId);
    }
}