<?php

namespace Flordelcampo\Api\Model\Data;

use Flordelcampo\Api\Api\Data\CustomOrderAttributeInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Class CustomOrderAttribute
 * Flordelcampo\Api\Model\Data
 */
class CustomOrderAttribute extends AbstractExtensibleModel implements CustomOrderAttributeInterface
{
    /**
     * @return $this|null
     */
    public function getPrescriptionId()
    {
        return $this->getData(self::PRESCRIPTION_ID);
    }

    /**
     * @return $this|null
     */
    public function getPrescriptionUrl()
    {
        return $this->getData(self::PRESCRIPTION_URL);
    }

    /**
     * @param $prescriptionId
     * @return CustomOrderAttribute|mixed
     */
    public function setPrescriptionId($prescriptionId)
    {
        return $this->setData(self::PRESCRIPTION_ID, $prescriptionId);
    }

    /**
     * @param $prescriptionUrl
     * @return CustomOrderAttribute|mixed
     */
    public function setPrescriptionUrl($prescriptionUrl)
    {
        return $this->setData(self::PRESCRIPTION_URL, $prescriptionUrl);
    }
}
