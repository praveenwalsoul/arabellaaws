<?php

namespace Flordelcampo\Api\Model\Data;

use Flordelcampo\Api\Api\Data\PaymentDataInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Class PaymentDetails
 * Flordelcampo\Api\Model\Data
 */
class PaymentData extends AbstractExtensibleModel implements PaymentDataInterface
{

    /**
     * @return $this|null
     */
    public function getPaymentMethod()
    {
        return $this->getData(self::PAYMENT_METHOD);
    }

    /**
     * @return $this|null
     */
    public function getCardNumber()
    {
        return $this->getData(self::CARD_NUMBER);
    }

    /**
     * @return $this|null
     */
    public function getCardType()
    {
        return $this->getData(self::CARD_TYPE);
    }

    /**
     * @param $cardType
     * @return PaymentData|mixed
     */
    public function setCardType($cardType)
    {
        return $this->setData(self::CARD_TYPE, $cardType);
    }

    /**
     * @param $paymentMethod
     * @return PaymentData|mixed
     */
    public function setPaymentMethod($paymentMethod)
    {
        return $this->setData(self::PAYMENT_METHOD, $paymentMethod);
    }

    /**
     * @param $cardNumber
     * @return PaymentData|mixed
     */
    public function setCardNumber($cardNumber)
    {
        return $this->setData(self::CARD_NUMBER, $cardNumber);
    }
}
