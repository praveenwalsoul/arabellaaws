<?php

namespace Flordelcampo\Api\Model\Response;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

class ApiResponse extends AbstractExtensibleModel implements ApiResponseInterface
{

    /**
     * @return $this|array
     */
    public function getResponse()
    {
        return $this->getData(self::RESPONSE_ATTRIBUTES);
    }

    /**
     * @param $response
     * @return ApiResponse|mixed
     */
    public function setResponse($response)
    {
        return $this->setData(self::RESPONSE_ATTRIBUTES, $response);
    }

}
