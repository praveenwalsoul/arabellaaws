<?php

namespace Flordelcampo\Api\Helper;

use Magento\Customer\Model\Customer;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Integration\Model\ResourceModel\Oauth\Token\CollectionFactory as TokenCollectionFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;

/**
 * Class Data
 * @package Flordelcampo\Api\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var TokenCollectionFactory
     */
    protected $tokenModelCollectionFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;

    public function __construct(
        TokenCollectionFactory $tokenModelCollectionFactory,
        Context $context,
        CustomerHelper $customerHelper
    )
    {
        $this->tokenModelCollectionFactory = $tokenModelCollectionFactory;
        $this->customerHelper = $customerHelper;
        parent::__construct($context);
    }


    /**
     * @param $token
     * @return Customer
     * @throws LocalizedException
     */
    public function getCustomerByToken($token)
    {
        $customerId = null;
        if ($token != null) {
            $tokenCollection = $this->tokenModelCollectionFactory->create()
                ->addFieldToFilter('token', $token);
            if ($tokenCollection->getSize() == 0) {
                throw new LocalizedException(__('Invalid token. please check'));
            } else {
                foreach ($tokenCollection as $auth) {
                    $customerId = $auth->getCustomerId();
                }
                if ($customerId != null) {
                    $customer = $this->customerHelper->getCustomerById($customerId);
                    if ($customer->getId()) {
                        return $customer;
                    } else {
                        throw new LocalizedException(__("Customer doesn't exist"));
                    }
                } else {
                    throw new LocalizedException(__("Customer doesn't exist for this token"));
                }
            }
        } else {
            throw new LocalizedException(__('Invalid token. please check'));
        }
    }

    /**
     * @param $headers
     * @return Customer
     * @throws LocalizedException
     */
    public function getCustomerByRequestHeaders($headers)
    {
        $token = $customerId = null;
        if (isset($headers['Authorization'])) {
            $authArray = explode(" ", $headers['Authorization']);
            if (isset($authArray[1])) {
                $token = $authArray[1];
            }
            if ($token != null) {
                return $this->getCustomerByToken($token);
            } else {
                throw new LocalizedException(__('Invalid token. please check'));
            }
        } else {
            throw new LocalizedException(__('Invalid Authentication'));
        }
    }

    /**
     * Return customer Payment Methods by Customer Id
     * @param $customerId
     * @param $storeId
     * @return array
     * @throws LocalizedException
     */
    public function getCustomerPaymentMethodsByCustomerId($customerId, $storeId)
    {
        $data = [];
        $customer = $this->customerHelper->getCustomerById($customerId);
        if (!$customer->getId()) {
            throw new LocalizedException(__("Customer doesn't exist"));
        }
        $allPaymentMethodList = $this->customerHelper->getAllPaymentMethodsByStoreId($storeId);
        $paymentModel = $this->customerHelper->getCustomerPaymentSettingsById($customerId);
        if ($paymentModel != null) {
            $paymentMethodString = $paymentModel->getPaymentMethod();
            if ($paymentMethodString != null) {
                $paymentMethodsArray = explode(",", $paymentMethodString);
                foreach ($paymentMethodsArray as $method) {
                    $data[] = [
                        'code' => $method,
                        'title' => $allPaymentMethodList[$method]
                    ];
                }
                return $data;
            } else {
                return $this->customerHelper->getDefaultPaymentMethodForArabella($storeId);
            }
        } else {
            return $this->customerHelper->getDefaultPaymentMethodForArabella($storeId);
        }
    }
}