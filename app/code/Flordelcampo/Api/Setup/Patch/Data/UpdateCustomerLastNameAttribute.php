<?php

namespace Flordelcampo\Api\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Customer\Setup\CustomerSetupFactory;

/**
 * Class UpdateCustomerLastNameAttribute
 * Flordelcampo\Api\Setup\Patch\Data
 */
class UpdateCustomerLastNameAttribute implements DataPatchInterface
{

    /**
     * Moduele Data
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * EAV setup
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * TO Apply
     *
     * @return DataPatchInterface|void
     *
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );
        $customerSetup = $this->customerSetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );
        $attributeCode = 'lastname';
        try {
            $eavSetup->updateAttribute('customer', $attributeCode, 'is_required', 0);
        } catch (\Exception $e) {
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Alias
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }
}