<?php

namespace Flordelcampo\Api\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Customer\Setup\CustomerSetupFactory;

/**
 * Class AddMobileNumberAttribute
 * @package Flordelcampo\Api\Setup\Patch\Data
 */
class AddMobileNumberAttribute implements DataPatchInterface
{

    /**
     * Moduele Data
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * EAV setup
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * TO Apply
     *
     * @return DataPatchInterface|void
     *
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );
        $customerSetup = $this->customerSetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );

        $attributeCode = 'customer_mobile_number';
        $attributeLabel = 'Mobile Number';
        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            $attributeCode,
            [
                'type' => 'varchar',
                'label' => $attributeLabel,
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => true,
                'position' => 300,
                'system' => false,
                'default' => '',
                'backend' => ''
            ]
        );
        // show the attribute in the following forms
        $attribute = $customerSetup
            ->getEavConfig()
            ->getAttribute(
                \Magento\Customer\Model\Customer::ENTITY,
                $attributeCode
            )
            ->addData(
                [
                    'used_in_forms' => [
                        'adminhtml_customer'
                    ]
                ]
            );

        $attribute->save();
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Alias
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }
}