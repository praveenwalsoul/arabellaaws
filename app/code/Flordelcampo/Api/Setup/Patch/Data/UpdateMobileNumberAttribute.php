<?php

namespace Flordelcampo\Api\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Customer\Setup\CustomerSetupFactory;

/**
 * Class AddMobileNumberAttribute
 * @package Flordelcampo\Api\Setup\Patch\Data
 */
class UpdateMobileNumberAttribute implements DataPatchInterface
{

    /**
     * Moduele Data
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * EAV setup
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * TO Apply
     *
     * @return DataPatchInterface|void
     *
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );
        $customerSetup = $this->customerSetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );

        $attributeCode = 'customer_mobile_number';

        try {
            $eavSetup->updateAttribute('customer', 'customer_mobile_number', 'is_required', 1);
        } catch (\Exception $e) {
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Alias
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }
}