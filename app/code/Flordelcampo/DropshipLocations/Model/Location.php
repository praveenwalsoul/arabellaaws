<?php
namespace Flordelcampo\DropshipLocations\Model;

class Location extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\DropshipLocations\Model\ResourceModel\Location');
    }
}
?>