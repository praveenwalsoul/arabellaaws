<?php
namespace Flordelcampo\DropshipLocations\Model\ResourceModel;

class Location extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('dropship_vendor_locations', 'loc_id');
    }
}
?>