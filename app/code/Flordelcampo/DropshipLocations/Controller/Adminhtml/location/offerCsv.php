<?php
namespace Flordelcampo\DropshipLocations\Controller\Adminhtml\location;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;


class offerCsv extends \Magento\Backend\App\Action
{
protected $_fileFactory;
protected $directory;
protected $prdCollection;
protected $request;
protected $vendorFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        //\Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Filesystem $filesystem,
        \Flordelcampo\VendorRegistration\Model\ResourceModel\VendorFactory $vendorFactory,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $prdCollection
    ) {
        
        $this->_fileFactory = $fileFactory;
        $this->request = $request;
        $this->vendorFactory = $vendorFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->prdCollection = $prdCollection;
        parent::__construct($context);
    }
 
    public function execute()
    {
        //$vendorId =$this->request->getParam('contact_id');
        //echo"sanoj";
          //$test=$this->getLocation();
         // print_r($test);
        
        //echo"hkkhkh";
        //print_r($vendorId);die;
        
        $collection = $this->prdCollection->addAttributeToSelect('*');
        /*
        ->addAttributeToFilter(
                        [
                            ['attribute'=>'vendor_farms','finset'=> $vendorId]
                        ]
                    )
                   ->load(); 
                   */

        $name = date('d_m_Y_H_i_s');
        $filepath = 'export/custom' . $name . '.csv';
        $this->directory->create('export');
        /* Open file */
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();
        $columns = $this->getColumnHeader();
        foreach ($columns as $column) {
            $header[] = $column;
        }
        /* Write Header */
        $stream->writeCsv($header);
 
        foreach ($collection as $item) {
            $itemData = [];
            $itemData[] = $item->getSku();
            $itemData[] = $item->getName();
            $stream->writeCsv($itemData);
        }
 
        $content = [];
        $content['type'] = 'filename'; // must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; //remove csv from var folder
 
        $csvfilename = 'Product.csv';
        return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
        
    }
 
    /* Header Columns */
    public function getColumnHeader() {
        $headers = ['Sku','Product name'];
        return $headers;
    }
    /* Get paramId Columns */
    public function getIddata()
    {
    // use 
    $this->request->getParams(); // all params
        return $this->request->getParam('id');
    }
    
    public function getLocation()
    {
        $model = $this->vendorFactory->create();
        $collection1 = $model->getCollection();
        
        foreach($collection1 as $item){
            echo "<pre>";
            print_r($item->getData());
            echo "</pre>";
        } 
        
        return $collection1;
    } 
}