<?php
namespace Flordelcampo\DropshipLocations\Controller\Adminhtml\location;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;


class Load extends \Magento\Backend\App\Action
{

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        print_r($data);die;
        $message = 'Something went wrong while saving the Location.';
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('Flordelcampo\DropshipLocations\Model\Location');

            $id = $this->getRequest()->getParam('loc_id');
            if ($id) {
                $model->load($id);
                $model->setCreatedAt(date('Y-m-d H:i:s'));
            }
            $model->setData($data);

            try {
                $model->save();
                echo $message = 'success';exit;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                echo $message = $e->getMessage();exit;
            } catch (\RuntimeException $e) {
                echo $message = $e->getMessage();exit;
            } catch (\Exception $e) {
                echo $message;exit;
            }
        }
        echo $message;
    }
}