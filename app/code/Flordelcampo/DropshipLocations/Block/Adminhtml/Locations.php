<?php
namespace Flordelcampo\DropshipLocations\Block\Adminhtml;
use Magento\Backend\Block\Template;

class Locations extends Template
{
    public function __construct(
    \Magento\Backend\Block\Template\Context $context,
    \Flordelcampo\DropshipLocations\Model\Location $locationFactory,
    array $data = []
) {
    $this->locationFactory = $locationFactory;
    parent::__construct($context, $data);
}
    
    public function getLocation()
    {
        $model = $this->locationFactory->create();
        $collection = $model->getCollection();
        /*
        foreach($collection as $item){
            echo "<pre>";
            print_r($item->getData());
            echo "</pre>";
        } */
        
        return $collection;
    }
    public function getCustomerStote()
{
    $options[0] =  ['value' => 'Ka', 'label' => 'Ka'];
    $options[1] =  ['value' => 'kb', 'label' => 'kabloom'];
    $options[2] =  ['value' => 'cu', 'label' => 'custom'];
    return $options;
}
}