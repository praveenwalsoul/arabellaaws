<?php

namespace Flordelcampo\DropshipLocations\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context){

       $installer = $setup;
        $installer->startSetup();

        // Get tutorial_simplenews table
  
        if (version_compare($context->getVersion(), '1.1.0') < 0){

            //$installer->run('create table user_registration(id int not null auto_increment, name varchar(255), city varchar(255), country varchar(200), status varchar(100) primary key(id))');
        $installer->run('CREATE TABLE `dropship_vendor_locations` (
          loc_id int not null auto_increment,
          vendor_id int not null,
          `loc_name` varchar(255) DEFAULT NULL,
          `loc_cust_disp_name` varchar(255) DEFAULT NULL,
          `kn_vendor_loc_id` varchar(255) DEFAULT NULL,
          `loc_rep` varchar(255) DEFAULT NULL,
          `loc_alt_sales_rep` varchar(255) DEFAULT NULL,
          `loc_address` varchar(255) DEFAULT NULL,
          `loc_street` varchar(255) DEFAULT NULL,
          `loc_city` varchar(255) DEFAULT NULL,
          `loc_state` varchar(30) DEFAULT NULL,
          `loc_postcode` varchar(255) DEFAULT NULL,
          `loc_phone` varchar(255) DEFAULT NULL,
          `loc_fax` varchar(255) DEFAULT NULL,
          `loc_email` varchar(255) DEFAULT NULL,
          `bk_cust_grp` varchar(255) DEFAULT NULL,
          `created_at` TIMESTAMP NOT NULL,
          primary key(loc_id))
        ');
    }
  
    $installer->endSetup();
  }

}