<?php

namespace Flordelcampo\ShippingRate\Block\Adminhtml;

class Shippingrate extends \Magento\Backend\Block\Widget\Container
{
    /**
     * @var string
     */
    protected $_template = 'shippingrate/shippingrate.phtml';

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Widget\Context $context,array $data = [])
    {
        parent::__construct($context, $data);
    }

    /**
     * Prepare button and grid
     *
     * @return \Magento\Catalog\Block\Adminhtml\Product
     */
    protected function _prepareLayout()
    {

        
        $addButtonProps = [
            'id' => 'add_new',
            'label' => __('Add New'),
            'class' => 'add',
            'button_class' => '',
            'class_name' => 'Magento\Backend\Block\Widget\Button\SplitButton',
            'options' => $this->_getAddButtonOptions(),
        ];
        $this->buttonList->add('add_new', $addButtonProps);
        

        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid', 'flordelcampo.shippingrate.grid')
        );
        return parent::_prepareLayout();
    }

    /**
     *
     *
     * @return array
     */
    protected function _getAddButtonOptions()
    {

        $splitButtonOptions[] = [
            'label' => __('Add New'),
            'onclick' => "setLocation('" . $this->_getCreateUrl() . "')"
        ];

        return $splitButtonOptions;
    }

    /**
     *
     *
     * @param string $type
     * @return string
     */
    protected function _getCreateUrl()
    {
        return $this->getUrl(
            'shippingrate/*/new'
        );
    }

    /**
     * Render grid
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }
     public function getCalculationType()
        {
            $data_array=array(); 
            $data_array[0]='Box Dimension';
            $data_array[1]='Box Type';
            return($data_array);
        }
         public function getValueArray0()
        {
            $arryData = $this->getCalculationType();
            $data_array=array();
            foreach($arryData as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
            }
            return($data_array);

        }
    
     public function getCostType()
        {
            $data_array=array(); 
            $data_array[0]='Anti Dumping Duty';
            $data_array[1]='Box Handling fee';
            $data_array[2]='Holiday Fuel Surcharge';
            $data_array[3]='Freight Surcharge';
            $data_array[4]='Truck Charge';
            $data_array[5]='Airline Cargo Cost';
            return($data_array);
        }
         public function getValueArray2()
        {
          $arryData = $this->getCostType();
            $data_array=array();
            foreach($arryData as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
            }
            return($data_array);

        }
         public function getCalculationMethod()
        {
            $data_array=array(); 
            $data_array[0]='Full Box Equivalent';
            $data_array[1]='Kilo';
            $data_array[2]='Product Cost by %';
            return($data_array);
        }
         public function getValueArray3()
        {
            $arryData = $this->getCalculationMethod();
            $data_array=array();
            foreach($arryData as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
            }
            return($data_array);

        }
         public function getCalculationFactor()
        {
            $data_array=array(); 
            $data_array[0]='Fixed';
            $data_array[1]='Percentage';
            return($data_array);
        }
         public function getValueArray4()
        {
            $arryData = $this->getCalculationFactor();
            $data_array=array();
            foreach($arryData as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
            }
            return($data_array);

        }
         public function getOptionArray7()
        {
            $data_array=array(); 
            $data_array[0]='Yes';
            $data_array[1]='No';
            return($data_array);
        }
         public function getValueArray7()
        {
            $arryData = $this->getOptionArray7();
            $data_array=array();
            foreach($arryData as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
            }
            return($data_array);

        }

}