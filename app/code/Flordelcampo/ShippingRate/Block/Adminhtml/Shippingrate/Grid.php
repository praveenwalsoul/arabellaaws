<?php
namespace Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\ShippingRate\Model\shippingrateFactory
     */
    protected $_shippingrateFactory;

    /**
     * @var \Flordelcampo\ShippingRate\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\ShippingRate\Model\shippingrateFactory $shippingrateFactory
     * @param \Flordelcampo\ShippingRate\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\ShippingRate\Model\ShippingrateFactory $ShippingrateFactory,
        \Flordelcampo\ShippingRate\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_shippingrateFactory = $ShippingrateFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('shipping_rate_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_shippingrateFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'shipping_rate_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'shipping_rate_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		

						$this->addColumn(
							'calculation_type',
							[
								'header' => __('Calculation Type'),
								'index' => 'calculation_type',
								'type' => 'options',
								'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray0()
							]
						);

						
				$this->addColumn(
					'conversion_factor',
					[
						'header' => __('Conversion Factor'),
						'index' => 'conversion_factor',
					]
				);
				

						$this->addColumn(
							'cost_type',
							[
								'header' => __('Cost Type'),
								'index' => 'cost_type',
								'type' => 'options',
								'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray2()
							]
						);

						

						$this->addColumn(
							'calculation_method',
							[
								'header' => __('Calculation Method'),
								'index' => 'calculation_method',
								'type' => 'options',
								'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray3()
							]
						);

						

						$this->addColumn(
							'calculation_factor',
							[
								'header' => __('Calculation Factor'),
								'index' => 'calculation_factor',
								'type' => 'options',
								'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray4()
							]
						);

						
				$this->addColumn(
					'cost',
					[
						'header' => __('Cost'),
						'index' => 'cost',
					]
				);
                
                $this->addColumn(
                    'zipcode',
                    [
                        'header' => __('Zipcode'),
                        'index' => 'zipcode',
                    ]
                ); 
				
				$this->addColumn(
					'effective_date',
					[
						'header' => __('Effective Date'),
						'index' => 'effective_date',
						'type'      => 'datetime',
					]
				);

					

						$this->addColumn(
							'active',
							[
								'header' => __('Active'),
								'index' => 'active',
								'type' => 'options',
								'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray7()
							]
						);

						


		
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'shipping_rate_id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
		

		
		   $this->addExportType($this->getUrl('shippingrate/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('shippingrate/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('shipping_rate_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_ShippingRate::shippingrate/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('shippingrate');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('shippingrate/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('shippingrate/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('shippingrate/*/index', ['_current' => true]);
    }

    /**
     * @param \Flordelcampo\ShippingRate\Model\shippingrate|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'shippingrate/*/edit',
            ['shipping_rate_id' => $row->getId()]
        );
		
    }

	
		static public function getOptionArray0()
		{
            $data_array=array(); 
			$data_array[0]='Box Dimension';
			$data_array[1]='Box Type';
            return($data_array);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(\Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray2()
		{
            $data_array=array(); 
			$data_array[0]='Anti Dumping Duty';
			$data_array[1]='Box Handling fee';
			$data_array[2]='Holiday Fuel Surcharge';
			$data_array[3]='Freight Surcharge';
			$data_array[4]='Truck Charge';
            $data_array[5]='Airline Cargo Cost';
            return($data_array);
		}
		static public function getValueArray2()
		{
            $data_array=array();
			foreach(\Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray2() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray3()
		{
            $data_array=array(); 
			$data_array[0]='Full Box Equivalent';
			$data_array[1]='Kilo';
			$data_array[2]='Product Cost by %';
            return($data_array);
		}
		static public function getValueArray3()
		{
            $data_array=array();
			foreach(\Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray3() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]='Fixed';
			$data_array[1]='Percentage';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(\Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray7()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray7()
		{
            $data_array=array();
			foreach(\Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray7() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		

}