<?php

namespace Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Edit\Tab;

/**
 * Shippingrate edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Flordelcampo\ShippingRate\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Flordelcampo\ShippingRate\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Flordelcampo\ShippingRate\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('shippingrate');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('shipping_rate_id', 'hidden', ['name' => 'shipping_rate_id']);
        }

		

        $fieldset->addField(
            'calculation_type',
            'select',
            [
                'label' => __('Calculation Type'),
                'title' => __('Calculation Type'),
                'name' => 'calculation_type',
				
                'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray0(),
                'disabled' => $isElementDisabled
            ]
        );

						
        $fieldset->addField(
            'conversion_factor',
            'text',
            [
                'name' => 'conversion_factor',
                'label' => __('Conversion Factor'),
                'title' => __('Conversion Factor'),
				
                'disabled' => $isElementDisabled
            ]
        );
					

        $fieldset->addField(
            'cost_type',
            'select',
            [
                'label' => __('Cost Type'),
                'title' => __('Cost Type'),
                'name' => 'cost_type',
				
                'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray2(),
                'disabled' => $isElementDisabled
            ]
        );

	   $fieldset->addField(
            'zipcode',
            'text',
            [
                'name' => 'zipcode',
                'label' => __('Zipcode'),
                'title' => __('Zipcode'),
                
                'disabled' => $isElementDisabled
            ]
        );		

        $fieldset->addField(
            'calculation_method',
            'select',
            [
                'label' => __('Calculation Method'),
                'title' => __('Calculation Method'),
                'name' => 'calculation_method',
				
                'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray3(),
                'disabled' => $isElementDisabled
            ]
        );

						

        $fieldset->addField(
            'calculation_factor',
            'select',
            [
                'label' => __('Calculation Factor'),
                'title' => __('Calculation Factor'),
                'name' => 'calculation_factor',
				
                'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray4(),
                'disabled' => $isElementDisabled
            ]
        );

						
        $fieldset->addField(
            'cost',
            'text',
            [
                'name' => 'cost',
                'label' => __('Cost'),
                'title' => __('Cost'),
				
                'disabled' => $isElementDisabled
            ]
        );
					

        $dateFormat = $this->_localeDate->getDateFormat(
            \IntlDateFormatter::MEDIUM
        );
        $timeFormat = $this->_localeDate->getTimeFormat(
            \IntlDateFormatter::MEDIUM
        );

        $fieldset->addField(
            'effective_date',
            'date',
            [
                'name' => 'effective_date',
                'label' => __('Effective Date'),
                'title' => __('Effective Date'),
                    'date_format' => $dateFormat,
                    //'time_format' => $timeFormat,
				
                'disabled' => $isElementDisabled
            ]
        );


						

        $fieldset->addField(
            'active',
            'select',
            [
                'label' => __('Active'),
                'title' => __('Active'),
                'name' => 'active',
				
                'options' => \Flordelcampo\ShippingRate\Block\Adminhtml\Shippingrate\Grid::getOptionArray7(),
                'disabled' => $isElementDisabled
            ]
        );

						

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
