<?php
namespace Flordelcampo\ShippingRate\Model;

class Shippingrate extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\ShippingRate\Model\ResourceModel\Shippingrate');
    }
}
?>