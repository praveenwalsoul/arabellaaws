<?php
namespace Flordelcampo\ShippingRate\Model\ResourceModel;

class Shippingrate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendor_shipping_rate', 'shipping_rate_id');
    }
}
?>