<?php

namespace Flordelcampo\ShippingRate\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0){

		$installer->run('CREATE TABLE `vendor_shipping_rate` (
          shipping_rate_id int not null auto_increment,
          `store_id` varchar(255) DEFAULT NULL,
          `calculation_type` varchar(255) DEFAULT NULL,
          `conversion_factor` varchar(255) DEFAULT NULL,
          `zipcode` varchar(255) DEFAULT NULL,
          `cost_type` varchar(255) DEFAULT NULL,
          `calculation_method` varchar(255) DEFAULT NULL,
          `calculation_factor` varchar(255) DEFAULT NULL,
          `cost` varchar(255) DEFAULT NULL,
          `effective_date` TIMESTAMP NOT NULL,
          `active` varchar(255) DEFAULT NULL,
          primary key(shipping_rate_id))');


		//demo
//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//$scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
//$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/updaterates.log');
//$logger = new \Zend\Log\Logger();
//$logger->addWriter($writer);
//$logger->info('updaterates');
//demo 

		}

        $installer->endSetup();

    }
}