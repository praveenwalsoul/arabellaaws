<?php
namespace Flordelcampo\VendorDistribution\Model;

class Distribution extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorDistribution\Model\ResourceModel\Distribution');
    }
}
?>