<?php

namespace Flordelcampo\VendorDistribution\Model\ResourceModel\Distribution;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorDistribution\Model\Distribution', 'Flordelcampo\VendorDistribution\Model\ResourceModel\Distribution');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>