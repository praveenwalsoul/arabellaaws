<?php
namespace Flordelcampo\VendorDistribution\Model\ResourceModel;

class Distribution extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendor_distribution_centre', 'distribution_centre_id');
    }
}
?>