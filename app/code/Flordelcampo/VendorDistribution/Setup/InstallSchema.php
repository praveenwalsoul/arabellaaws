<?php

namespace Flordelcampo\VendorDistribution\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0){

		$installer->run('CREATE TABLE `vendor_distribution_centre` (
          distribution_centre_id int not null auto_increment,
          `distribution_centre_name` varchar(255) DEFAULT NULL,
          `contact_name` varchar(255) DEFAULT NULL,
          `address1` varchar(255) DEFAULT NULL,
          `address2` varchar(255) DEFAULT NULL,
          `city` varchar(255) DEFAULT NULL,
          `state` varchar(255) DEFAULT NULL,
          `zip_code` varchar(255) DEFAULT NULL,
          `country` varchar(255) DEFAULT NULL,
          `phone` varchar(255) DEFAULT NULL,
          `email` varchar(255) DEFAULT NULL,
          primary key(distribution_centre_id))');


		//demo
//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//$scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
//$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/updaterates.log');
//$logger = new \Zend\Log\Logger();
//$logger->addWriter($writer);
//$logger->info('updaterates');
//demo 

		}

        $installer->endSetup();

    }
}