<?php
namespace Flordelcampo\VendorDistribution\Block\Adminhtml\Distribution;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\VendorDistribution\Model\distributionFactory
     */
    protected $_distributionFactory;

    /**
     * @var \Flordelcampo\VendorDistribution\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\VendorDistribution\Model\distributionFactory $distributionFactory
     * @param \Flordelcampo\VendorDistribution\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\VendorDistribution\Model\DistributionFactory $DistributionFactory,
        \Flordelcampo\VendorDistribution\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_distributionFactory = $DistributionFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('distribution_centre_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_distributionFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'distribution_centre_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'distribution_centre_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'distribution_centre_name',
					[
						'header' => __('Distribution Centre Name'),
						'index' => 'distribution_centre_name',
					]
				);
				/*
				$this->addColumn(
					'contact_name',
					[
						'header' => __('Contact Name'),
						'index' => 'contact_name',
					]
				);
				
				$this->addColumn(
					'address1',
					[
						'header' => __('Address1'),
						'index' => 'address1',
					]
				);
				
				$this->addColumn(
					'address2',
					[
						'header' => __('Address2'),
						'index' => 'address2',
					]
				);
				
				$this->addColumn(
					'city',
					[
						'header' => __('City'),
						'index' => 'city',
					]
				);
				
				$this->addColumn(
					'state',
					[
						'header' => __('State'),
						'index' => 'state',
					]
				);
				
				$this->addColumn(
					'zip_code',
					[
						'header' => __('Zip Code'),
						'index' => 'zip_code',
					]
				);
				
				$this->addColumn(
					'country',
					[
						'header' => __('Country'),
						'index' => 'country',
					]
				);
				
				$this->addColumn(
					'phone',
					[
						'header' => __('Phone'),
						'index' => 'phone',
					]
				);
				
				$this->addColumn(
					'email',
					[
						'header' => __('Email'),
						'index' => 'email',
					]
				);
                */
				


		
        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit'
                        ],
                        'field' => 'distribution_centre_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );
		

		
		   $this->addExportType($this->getUrl('vendordistribution/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('vendordistribution/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('distribution_centre_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_VendorDistribution::distribution/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('distribution');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('vendordistribution/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('vendordistribution/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('vendordistribution/*/index', ['_current' => true]);
    }

    /**
     * @param \Flordelcampo\VendorDistribution\Model\distribution|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'vendordistribution/*/edit',
            ['distribution_centre_id' => $row->getId()]
        );
		
    }

	

}