<?php

namespace Flordelcampo\VendorCostchannel\Block\Adminhtml\Costchannel\Edit\Tab;

/**
 * Costchannel edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    protected $website;
    protected $vendortype;
    protected $shipmethod;

    /**
     * @var \Flordelcampo\VendorCostchannel\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Flordelcampo\VendorCostchannel\Model\Config\Source\Website $website,
        \Flordelcampo\VendorCostchannel\Model\Config\Source\Vendortype $vendortype,
        \Flordelcampo\VendorCostchannel\Model\Config\Source\Shipmethod $shipmethod,
        \Magento\Store\Model\System\Store $systemStore,
        \Flordelcampo\VendorCostchannel\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        $this->website = $website;
        $this->vendortype = $vendortype;
        $this->shipmethod = $shipmethod;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Flordelcampo\VendorCostchannel\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('costchannel');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('cost_channel_id', 'hidden', ['name' => 'cost_channel_id']);
        }

		$web = $this->website->toOptionArray();
        $vendortype = $this->vendortype->toOptionArray();
        $shipMehod = $this->shipmethod->toOptionArray();
    
        $fieldset->addField(
            'store_id',
            'multiselect',
            [
                'label' => __('Website'),
                'title' => __('Website'),
                'name' => 'store_id',
				'values' => $web
                //'options' => \Flordelcampo\VendorCostchannel\Block\Adminhtml\Costchannel\Grid::getValueArray0(),
                //'disabled' => $isElementDisabled
            ]
        );
						
        $fieldset->addField(
            'cost_channel_name',
            'text',
            [
                'name' => 'cost_channel_name',
                'label' => __('Cost Channel Name'),
                'title' => __('Cost Channel Name'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'cost_channel_type',
            'select',
            [
                'name' => 'cost_channel_type',
                'label' => __('Cost Channel Type'),
                'title' => __('Cost Channel Type'),
				'values' => $vendortype
               // 'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'shipping_method',
            'select',
            [
                'name' => 'shipping_method',
                'label' => __('Shipping Mehod'),
                'title' => __('Shipping Mehod'),
                'values' => $shipMehod
                //'disabled' => $isElementDisabled
            ]
        ); 
					

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
