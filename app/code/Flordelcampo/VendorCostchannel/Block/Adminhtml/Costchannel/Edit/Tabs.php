<?php
namespace Flordelcampo\VendorCostchannel\Block\Adminhtml\Costchannel\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('costchannel_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Costchannel Information'));
    }
}