<?php
namespace Flordelcampo\VendorCostchannel\Block\Adminhtml\Costchannel;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\VendorCostchannel\Model\costchannelFactory
     */
    protected $_costchannelFactory;

    /**
     * @var \Flordelcampo\VendorCostchannel\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\VendorCostchannel\Model\costchannelFactory $costchannelFactory
     * @param \Flordelcampo\VendorCostchannel\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\VendorCostchannel\Model\CostchannelFactory $CostchannelFactory,
        \Flordelcampo\VendorCostchannel\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_costchannelFactory = $CostchannelFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('cost_channel_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_costchannelFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'cost_channel_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'cost_channel_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
		
				$this->addColumn(
					'cost_channel_name',
					[
						'header' => __('Cost Channel Name'),
						'index' => 'cost_channel_name',
					]
				);
                $this->addColumn(
                    'store_id',
                    [
                        //'header' => __('Cost Channel Name'),
                        'index' => 'store_id',
                        'header' => __('Store Views'),                        
                        'type' => 'store',
                        'store_all' => true,
                        'store_view' => true,
                        'renderer'=>  'Flordelcampo\VendorCostchannel\Block\Adminhtml\Costchannel\Edit\Tab\Renderer\Store',
                        'filter_condition_callback' => [$this, '_filterStoreCondition']
                    ]
                );
				
				$this->addColumn(
					'cost_channel_type',
					[
						'header' => __('Cost Channel Type'),
						'index' => 'cost_channel_type',
					]
				);
                
                $this->addColumn(
                    'shipping_method',
                    [
                        'header' => __('Shipping Method'),
                        'index' => 'shipping_method',
                    ]
                ); 
				


		
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'cost_channel_id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
		

		
		   $this->addExportType($this->getUrl('vendorcostchannel/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('vendorcostchannel/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('cost_channel_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_VendorCostchannel::costchannel/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('costchannel');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('vendorcostchannel/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('vendorcostchannel/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('vendorcostchannel/*/index', ['_current' => true]);
    }
    protected function _filterStoreCondition($collection, $column){

         if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addFieldToFilter('store_id', array('finset' => $value));
    }

    /**
     * @param \Flordelcampo\VendorCostchannel\Model\costchannel|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'vendorcostchannel/*/edit',
            ['cost_channel_id' => $row->getId()]
        );
		
    }

	
		static public function getOptionArray0()
		{
            $data_array=array(); 
			$data_array[0]='Website1';
			$data_array[1]='Website1';
            return($data_array);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorCostchannel\Block\Adminhtml\Costchannel\Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		

}