<?php

namespace Flordelcampo\VendorCostchannel\Model\ResourceModel\Costchannel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorCostchannel\Model\Costchannel', 'Flordelcampo\VendorCostchannel\Model\ResourceModel\Costchannel');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>