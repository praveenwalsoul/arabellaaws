<?php
namespace Flordelcampo\VendorCostchannel\Model\ResourceModel;

class Costchannel extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendor_cost_channel', 'cost_channel_id');
    }
}
?>