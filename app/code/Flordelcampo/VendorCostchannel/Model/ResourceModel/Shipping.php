<?php
namespace Flordelcampo\VendorCostchannel\Model\ResourceModel;

class Shipping extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendor_shipping', 'shipping_method_id');
    }
}
?>