<?php
namespace Flordelcampo\VendorCostchannel\Model\Config\Source;

class Shipmethod implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \WDT\Faq\Model\ResourceModel\Category\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var array|null
     */
    protected $_options;

    /**
     * @param \WDT\Faq\Model\ResourceModel\Category\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Flordelcampo\VendorCostchannel\Model\ResourceModel\Shipping\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        
    }

    /**
     * @return array
     */
    public function toOptionArray() {
        if ($this->_options === null) {
            $collection = $this->collectionFactory->create();

            $this->_options = [['label' => '', 'value' => '']];

            foreach ($collection as $ship) {
                $this->_options[] = [
                    'value' => $ship->getShippingMethodName(),
                    'label' => $ship->getShippingMethodName() //$ship->getShippingMethodId()
                ];
            }
        }

        return $this->_options;
    }
}