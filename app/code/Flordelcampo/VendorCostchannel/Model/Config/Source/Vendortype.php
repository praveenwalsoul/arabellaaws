<?php
namespace Flordelcampo\VendorCostchannel\Model\Config\Source;

class Vendortype implements \Magento\Framework\Option\ArrayInterface
{
    protected $_storeManager;

    public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->_storeManager = $storeManager;
    }
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label'=>'Vendor','value'=>'Vendor'],
            ['label'=>'Customer','value'=>'Customer'],
            ['label'=>'Both','value'=>'Both']
            
        ];
       
        return $this->_options;
    }
     public function toArray()
    {
        return [];
    } 
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
      
   

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */

     public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    } 

    
}
  
    

   