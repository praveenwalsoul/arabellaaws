<?php

namespace Flordelcampo\VendorShipping\Block\Adminhtml\Shipping;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\VendorShipping\Model\shippingFactory
     */
    protected $_shippingFactory;

    /**
     * @var \Flordelcampo\VendorShipping\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\VendorShipping\Model\shippingFactory $shippingFactory
     * @param \Flordelcampo\VendorShipping\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\VendorShipping\Model\ShippingFactory $ShippingFactory,
        \Flordelcampo\VendorShipping\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    )
    {
        $this->_shippingFactory = $ShippingFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('shipping_method_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_shippingFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'shipping_method_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'shipping_method_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn(
            'store_id',
            [
                //'header' => __('Cost Channel Name'),
                'index' => 'store_id',
                'header' => __('Store Views'),
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'renderer' => 'Flordelcampo\VendorShipping\Block\Adminhtml\Shipping\Edit\Tab\Renderer\Store',
                'filter_condition_callback' => [$this, '_filterStoreCondition']
            ]
        );

        $this->addColumn(
            'vendor_id',
            [
                'header' => __('Vendor Name'),
                'index' => 'vendor_id',
            ]
        );

        $this->addColumn(
            'shipping_method_name',
            [
                'header' => __('Shipping Method Name'),
                'index' => 'shipping_method_name',
            ]
        );

        $this->addColumn(
            'shipping_code',
            [
                'header' => __('Shipping Code'),
                'index' => 'shipping_code',
            ]
        );

        $this->addColumn(
            'shipping_method_username',
            [
                'header' => __('shipping Method User Name'),
                'index' => 'shipping_method_username',
            ]
        );

        $this->addColumn(
            'shipping_account_number',
            [
                'header' => __('Shipping A/C No'),
                'index' => 'shipping_account_number',
            ]
        );

        $this->addColumn(
            'shipping_method_production_number',
            [
                'header' => __('Method Production No'),
                'index' => 'shipping_method_production_number',
            ]
        );

        $this->addColumn(
            'pref_ship_methods',
            [
                'header' => __('Preferable Shipping Methods'),
                'index' => 'pref_ship_methods',
            ]
        );

        $this->addExportType($this->getUrl('vendorshipping/*/exportCsv', ['_current' => true]), __('CSV'));
        $this->addExportType($this->getUrl('vendorshipping/*/exportExcel', ['_current' => true]), __('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('shipping_method_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_VendorShipping::shipping/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('shipping');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('vendorshipping/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('vendorshipping/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }

    protected function _filterStoreCondition($collection, $column)
    {

        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addFieldToFilter('store_id', array('finset' => $value));
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('vendorshipping/*/index', ['_current' => true]);
    }

    /**
     * @param \Flordelcampo\VendorShipping\Model\shipping|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {

        return $this->getUrl(
            'vendorshipping/*/edit',
            ['shipping_method_id' => $row->getId()]
        );

    }


}