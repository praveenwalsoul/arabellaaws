<?php

namespace Flordelcampo\VendorShipping\Block\Adminhtml\Shipping\Edit\Tab;

/**
 * Shipping edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Flordelcampo\VendorShipping\Model\Status
     */
    protected $_status;
    protected $vendors;
    protected $vendorName;
    protected $website;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Flordelcampo\VendorRegistration\Model\Vendor $vendorFactory,
        \Flordelcampo\VendorShipping\Model\Config\Source\VendorName $vendorName,
        \Flordelcampo\VendorShipping\Model\Config\Source\Website $website,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Flordelcampo\VendorShipping\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        $this->vendorName = $vendorName;
        $this->website = $website;
        $this->vendors = $vendorFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Flordelcampo\VendorShipping\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('shipping');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('shipping_method_id', 'hidden', ['name' => 'shipping_method_id']);
        }
        
        $vend = $this->vendorName->toOptionArray(); 
        $web = $this->website->toOptionArray();
       
        $fieldset->addField(
            'store_id',
            'multiselect',
            [
                'label' => __('Website'),
                'title' => __('Website'),
                'name' => 'store_id',
                'values' => $web,
                'disabled' => false
                //'options' => \Flordelcampo\VendorShipping\Block\Adminhtml\Shipping\Grid::getValueArray0(),
                //'disabled' => $isElementDisabled
            ]
        );
                        
        $fieldset->addField(
            'vendor_id',
            'multiselect',
            [
                'label' => __('vendor Name'),
                'title' => __('vendor Name'),
                'name' => 'vendor_id',
                'values' => $vend
                //'values' =>\Flordelcampo\VendorShipping\Block\Adminhtml\Shipping\Grid::getAllOptions(),
                //'disabled' => $isElementDisabled
            ]
        );
                        
        $fieldset->addField(
            'shipping_method_name',
            'text',
            [
                'name' => 'shipping_method_name',
                'label' => __('Shipping Method Name'),
                'title' => __('Shipping Method Name'),
                
                'disabled' => $isElementDisabled
            ]
        );
                    
        $fieldset->addField(
            'shipping_code',
            'text',
            [
                'name' => 'shipping_code',
                'label' => __('Shipping Method Code'),
                'title' => __('Shipping Method Code'),
                
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'pref_ship_methods',
            'text',
            [
                'name' => 'pref_ship_methods',
                'label' => __('Preferable Shipping Methods'),
                'title' => __('Preferable Shipping Methods'),

                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'shipping_method_username',
            'text',
            [
                'name' => 'shipping_method_username',
                'label' => __('Shipping Method User'),
                'title' => __('Shipping Method User'),
                
                'disabled' => $isElementDisabled
            ]
        );
                    
        $fieldset->addField(
            'shipping_account_number',
            'text',
            [
                'name' => 'shipping_account_number',
                'label' => __('Shipping A/C no'),
                'title' => __('Shipping A/C no'),
                
                'disabled' => $isElementDisabled
            ]
        );
                    
        $fieldset->addField(
            'shipping_method_production_number',
            'text',
            [
                'name' => 'shipping_method_production_number',
                'label' => __('Production Name'),
                'title' => __('Production Name'),
                
                'disabled' => $isElementDisabled
            ]
        );
                    
        $fieldset->addField(
            'shipping_method_api_key',
            'text',
            [
                'name' => 'shipping_method_api_key',
                'label' => __('Api Key'),
                'title' => __('Api Key'),
                
                'disabled' => $isElementDisabled
            ]
        );
                    

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
        return array(
                    '_self' => "Self",
                    '_blank' => "New Page",
                    );
    }
}
