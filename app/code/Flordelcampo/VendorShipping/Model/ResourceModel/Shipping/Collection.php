<?php

namespace Flordelcampo\VendorShipping\Model\ResourceModel\Shipping;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorShipping\Model\Shipping', 'Flordelcampo\VendorShipping\Model\ResourceModel\Shipping');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>