<?php
namespace Flordelcampo\VendorShipping\Model\Config\Source;
//use Flordelcampo\VendorShipping\Model\Config\Source\AbstractSource;
//class VendorName extends AbstractSource implements \Magento\Framework\Option\ArrayInterface
class Website implements \Magento\Framework\Option\ArrayInterface
{
    protected $_storeManager;

    public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->_storeManager = $storeManager;
    }
    /**
     * Get all options
     *
     * @return array
     */
    private function getStoreData(){
    $storeManagerDataList = $this->_storeManager->getStores();
    
     $options = array();
     foreach ($storeManagerDataList as $key => $value) {
               $options[] = ['label' => $value['name'].' - '.$value['code'], 'value' => $key];
     }
     return $options;

    
}
    
    public function toArray()
    {
        return [];
    } 

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */

    public function toOptionArray()
    {
        return $this->getStoreData();
    }
    /*
    public function toOptionArray($value)
    {
        
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    } */
}