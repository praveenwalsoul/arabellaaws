<?php
namespace Flordelcampo\VendorShipping\Model;

class Shipping extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorShipping\Model\ResourceModel\Shipping');
    }
}
?>