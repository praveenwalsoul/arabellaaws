<?php

namespace Flordelcampo\VendorShipping\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0){

		$installer->run('CREATE TABLE `vendor_shipping` (
          shipping_method_id int not null auto_increment,
          `store_id` varchar(255) DEFAULT NULL,
          `vendor_id` varchar(255) DEFAULT NULL,
          `shipping_method_name` varchar(255) DEFAULT NULL,
          `shipping_code` varchar(255) DEFAULT NULL,
          `shipping_method_username` varchar(255) DEFAULT NULL,
          `shipping_account_number` varchar(255) DEFAULT NULL,
          `shipping_method_production_number` varchar(255) DEFAULT NULL,
          `shipping_method_api_key` varchar(255) DEFAULT NULL,
          primary key(shipping_method_id))');

		}

        $installer->endSetup();

    }
}