<?php

namespace Flordelcampo\Customer\Controller\Adminhtml\Store;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Customer\Helper\Data;

class Update extends Action
{
    /**
     * @var null
     */
    private $_customerId;
    /**
     * @var null
     */
    private $_storeID;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        Data $helper
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonFactory = $jsonFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * Update or Add Store Data
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();

        try {
            $params = $this->getRequest()->getParams();
            $customerId = $params['customer_id'];
            $storeId = $params['store_id'];
            $this->_customerId = $customerId;
            $this->_storeID = $storeId;
            /*echo '<pre>'; print_r($params); die;*/
            $vendorPrefCheck = (isset($params['vendor_preference'])) ? '1' : '0';
            $storeData = [
                'store_activate' => $params['store_activate'],
                'store_id' => $params['store_id'],
                'store_name' => $params['store_name'],
                'primary_sales_rep' => $params['sales_rep'],
                'store_email' => $params['store_email'],
                'distribution_center' => $params['distribution_center'],
                'cost_channel' => $params['cost_channel'],
                'customer_id' => $params['customer_id'],
                'vendor_preference' => $vendorPrefCheck
            ];
            $addressData = [
                'company' => $params['store_name'],
                'street' => $params['street'],
                'city' => $params['city'],
                'postcode' => $params['zip_code'],
                'telephone' => $params['telephone'],
                'country_id' => $params['country'],
                'region_id' => $params['region']
            ];
            $salesRep = $params['sales_rep'];
            $customerStoreFactory = $this->helper->getCustomerStoreByStoreId($customerId, $storeId);
            $addressFactory = $this->helper->getCustomerAddressById($storeId);
            $customerFactory = $this->helper->getCustomerById($customerId);
            /**
             * Update Address Data
             */
            foreach ($addressData as $field => $value) {
                $addressFactory->setData($field, $value);
            }
            $addressFactory->save();
            /**
             * Update Customer Sales Rep
             */
            $customerFactory->setCustSalesRep($salesRep)->save();

            /**
             * Update Store Setting or Add Store Data
             */
            if ($customerStoreFactory != null) {
                foreach ($storeData as $field => $value) {
                    $customerStoreFactory->setData($field, $value);
                }
                $customerStoreFactory->save();
            } else {
                $customerStoreModel = $this->helper->getCustomerStoreModel();
                foreach ($storeData as $field => $value) {
                    $customerStoreModel->setData($field, $value);
                }
                $customerStoreModel->save();
            }

            $data = [
                'status' => true,
                'message' => 'Store Updated Successfully'
            ];

        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }
}
