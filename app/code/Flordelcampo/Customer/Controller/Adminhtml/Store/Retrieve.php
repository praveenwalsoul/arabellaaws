<?php

namespace Flordelcampo\Customer\Controller\Adminhtml\Store;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\Address;
use Magento\Customer\Model\Customer;
use Magento\Directory\Model\Country;
use Magento\Directory\Model\CountryFactory;
use Magento\Directory\Model\ResourceModel\Country\Collection;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;

/**
 * Class StoreSetting
 *
 * Flordelcampo\Customer\Controller\Adminhtml\Store
 */
class Retrieve extends Action
{
    /**
     * @var null
     */
    private $_customerId;
    /**
     * @var null
     */
    private $_addressID;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var Country
     */
    protected $country;
    /**
     * @var CollectionFactory
     */
    protected $countryCollectionFactory;
    /**
     * @var CountryFactory
     */
    protected $countryFactory;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var CustomerHelper
     */
    protected $helper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    /**
     * Retrieve constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param Country $country
     * @param CollectionFactory $countryCollectionFactory
     * @param CountryFactory $countryFactory
     * @param CustomerHelper $helper
     * @param CatalogHelper $catalogHelper
     * @param VendorHelper $vendorHelper
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Country $country,
        CollectionFactory $countryCollectionFactory,
        CountryFactory $countryFactory,
        CustomerHelper $helper,
        CatalogHelper $catalogHelper,
        VendorHelper $vendorHelper,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->jsonFactory = $jsonFactory;
        $this->country = $country;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->countryFactory = $countryFactory;
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
        $this->catalogHelper = $catalogHelper;
        $this->vendorHelper = $vendorHelper;
        parent::__construct($context);
    }

    /**
     * Fetch All Tabs Data
     *
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $customerId = $this->getRequest()->getParam('customerId');
        $addressId = $this->getRequest()->getParam('addressId');
        $this->_customerId = $customerId;
        $this->_addressID = $addressId;
        try {

            $storeHtml = $this->storeDetailsHtml();
            $logisticNewForm = $this->getLogisticAddNewHtmlForm();
            $logisticData = $this->getLogisticDataHtml();
            $vendorPreferenceNewForm = $this->getAddNewVendorPreferenceHtml();
            $vendorPreferenceData = $this->getVendorPreferenceData();
            $data = [
                'status' => true,
                'store' => $storeHtml,
                'newLogisticForm' => $logisticNewForm,
                'logisticData' => $logisticData,
                'newVendorPreferenceForm' => $vendorPreferenceNewForm,
                'vendorPreferenceData' => $vendorPreferenceData
            ];

        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    public function getVendorPreferenceData()
    {
        $customerId = $this->_customerId;
        $storeId = $this->_addressID;
        $vendorList = $this->vendorHelper->getAllVendorListArray();
        $categoryList = $this->catalogHelper->getAllCategoryListArray();
        $storeVisibilityList = $this->getStoreVisibilityOptionArray();
        $preferenceCollection = $this->helper->getCustomerPreferenceCollectionByStoreId($customerId, $storeId);
        $html = '';
        if ($preferenceCollection != null) {
            foreach ($preferenceCollection as $preference) {
                $prefEntityId = $preference->getEntityId();
                $currentVendor = $preference->getVendorId();
                $currentCategoryIds = $preference->getCategoryIds();
                $currentStoreAvailability = $preference->getStoreAvailability();
                $currentCategoryArray = explode(',', $currentCategoryIds);

                /**
                 * Vendor Data
                 */
                $vendorHtml = '';
                $vendorHtml .= "<select type='select' name='preference_vendor' class='update-vendor-preference-$prefEntityId' >";
                $count = 1;
                foreach ($vendorList as $id => $value) {
                    if ($id == $currentVendor) {
                        $vendorHtml .= "<option value='$id' selected='selected'>$value</option>";
                        $count++;
                    } else {
                        $vendorHtml .= "<option value='$id'>$value</option>";
                    }
                }
                $vendorHtml .= "</select>";

                /**
                 *  Category Multi Select
                 */
                $categoryHtml = '';
                $categoryHtml .= "<select type='select' name='preference_category' class='update-vendor-preference-$prefEntityId' multiple>";
                $count = 1;
                foreach ($categoryList as $id => $value) {
                    if (in_array($id, $currentCategoryArray) == true) {
                        $categoryHtml .= "<option value='$id' selected='selected'>$value</option>";
                    } else {
                        $categoryHtml .= "<option value='$id'>$value</option>";
                    }

                }
                $categoryHtml .= "</select>";

                /**
                 * Store Visibility
                 */
                $storeVisibilityHtml = '';
                $storeVisibilityHtml .= "<select type='select' name='preference_store_visibility' class='update-vendor-preference-$prefEntityId' >";
                $count = 1;
                foreach ($storeVisibilityList as $id => $value) {
                    if ($id == $currentStoreAvailability) {
                        $storeVisibilityHtml .= "<option value='$id' selected='selected'>$value</option>";
                        $count++;
                    } else {
                        $storeVisibilityHtml .= "<option value='$id'>$value</option>";
                    }
                }
                $storeVisibilityHtml .= "</select>";

                $hiddenFields = '';
                $hiddenFields .= "<input type='hidden' name='customer_id' class='update-vendor-preference-$prefEntityId' value='$customerId' />";
                $hiddenFields .= "<input type='hidden' name='store_id' class='update-vendor-preference-$prefEntityId' value='$storeId'/>";
                $hiddenFields .= "<input type='hidden' name='preference_entity_id' class='update-vendor-preference-$prefEntityId' value='$prefEntityId'/>";
                $saveHtml = "<button type='button' data-pref-entity-id='$prefEntityId' class='save primary update-vendor-preference-button'>Update</button>";
                $html .= "<tr>";
                $html .= "<td>" . $vendorHtml . "</td>";
                $html .= "<td>" . $categoryHtml . "</td>";
                $html .= "<td>" . $storeVisibilityHtml . "</td>";
                $html .= "<td>" . $saveHtml . "</td>";
                $html .= "</tr>";
                $html .= $hiddenFields;

            }

        }
        return $html;
    }

    /**
     * Get Vendor Preference Data
     * @return string
     */
    public function getAddNewVendorPreferenceHtml()
    {
        $customerId = $this->_customerId;
        $storeId = $this->_addressID;
        $vendorList = $this->vendorHelper->getAllVendorListArray();
        $categoryList = $this->catalogHelper->getAllCategoryListArray();
        $storeVisibilityList = $this->getStoreVisibilityOptionArray();

        /**
         * Vendor Data
         */
        $vendorHtml = '';
        $vendorHtml .= "<select type='select' name='preference_vendor' class='new-vendor-preference' >";
        $count = 1;
        foreach ($vendorList as $id => $value) {
            if ($id == '0') {
                $vendorHtml .= "<option value='$id' selected='selected'>$value</option>";
                $count++;
            } else {
                $vendorHtml .= "<option value='$id'>$value</option>";
            }
        }
        $vendorHtml .= "</select>";

        /**
         *  Category Multi Select
         */
        $categoryHtml = '';
        $categoryHtml .= "<select type='select' name='preference_category' class='new-vendor-preference' multiple>";
        $count = 1;
        foreach ($categoryList as $id => $value) {
            $categoryHtml .= "<option value='$id'>$value</option>";
        }
        $categoryHtml .= "</select>";

        /**
         * Store Visibility
         */
        $storeVisibilityHtml = '';
        $storeVisibilityHtml .= "<select type='select' name='preference_store_visibility' class='new-vendor-preference' >";
        $count = 1;
        foreach ($storeVisibilityList as $id => $value) {
            if ($id == '0') {
                $storeVisibilityHtml .= "<option value='$id' selected='selected'>All</option>";
                $count++;
            } else {
                $storeVisibilityHtml .= "<option value='$id'>$value</option>";
            }
        }
        $storeVisibilityHtml .= "</select>";
        $hiddenFields = '';
        $hiddenFields .= "<input type='hidden' name='customer_id' class='new-vendor-preference' value='$customerId' />";
        $hiddenFields .= "<input type='hidden' name='store_id' class='new-vendor-preference' value='$storeId'/>";
        $saveHtml = "<button type='button' class='save primary add-vendor-preference-button'>Add New</button>";
        $html = "";
        $html .= "<tr>";
        $html .= "<td>" . $vendorHtml . "</td>";
        $html .= "<td>" . $categoryHtml . "</td>";
        $html .= "<td>" . $storeVisibilityHtml . "</td>";
        $html .= "<td>" . $saveHtml . "</td>";
        $html .= "</tr>";
        $html .= $hiddenFields;
        return $html;
    }

    /**
     * Existing Logistics Data HTML
     * @return string
     */
    public function getLogisticDataHtml()
    {
        $customerId = $this->_customerId;
        $storeId = $this->_addressID;
        $html = '';
        $logisticCollection = $this->helper->getCustomerLogisticCollectionByStoreId($customerId, $storeId);
        if ($logisticCollection != null) {
            foreach ($logisticCollection as $logistic) {
                $logisticEntityId = $logistic->getEntityId();
                $currentLogisticName = $logistic->getLogisticId();
                $currentActive = $logistic->getActivate();
                $currentAccNumber = $logistic->getAccNumber();
                $currentTruckOwn = $logistic->getTruckOwnByCustomer();
                $currentZone = $logistic->getZone();
                $currentLeadTime = $logistic->getLeadTime();
                $currentAirPort = $logistic->getAirPort();
                $currentConsineeId = $logistic->getConsineeId();
                $currentCheckBoxDays = $logistic->getLogisticDays();
                $currentDaysArray = explode(',', $currentCheckBoxDays);
                /**
                 * Checkbox
                 */
                $logisticDaysHtml = '';
                $daysArray = $this->getDaysArray();
                foreach ($daysArray as $id => $label) {
                    if (in_array($id, $currentDaysArray) == true) {
                        $logisticDaysHtml .= "<tr><td class='label'>" . $label . "</td><td class='value'><input type='checkbox' class='logistic-checkbox-$logisticEntityId' checked='checked' value='$id'></td></tr>";
                    } else {
                        $logisticDaysHtml .= "<tr><td class='label'>" . $label . "</td><td class='value'><input type='checkbox' class='logistic-checkbox-$logisticEntityId' value='$id'></td></tr>";

                    }
                }

                /**
                 * Activate
                 */
                $yesNoArray = $this->getYesNoArray();
                $activateHtml = '';
                $activateHtml .= "<select type='select' name='logistic_activate' >";
                foreach ($yesNoArray as $id => $value) {
                    if ($id == $currentActive) {
                        $activateHtml .= "<option value='$id' selected='selected'>$value</option>";
                    } else {
                        $activateHtml .= "<option value='$id'>$value</option>";
                    }
                }
                $activateHtml .= "</select>";

                /*
                 * Logistic Name
                 */
                $logisticNamesHtml = '';
                $logisticsData = $this->getAllLogisticsData();
                $logisticNamesHtml .= "<select type='select' name='logistic_name'>";
                foreach ($logisticsData as $id => $value) {
                    if ($currentLogisticName == $id) {
                        $logisticNamesHtml .= "<option value='$id' selected='selected'>$value</option>";
                    } else {
                        $logisticNamesHtml .= "<option value='$id'>$value</option>";
                    }
                }
                $logisticNamesHtml .= "</select>";

                /**
                 * Truck Owned By customer
                 */
                $truckOwnByCustomerHtml = '';
                $truckOwnByCustomerHtml .= "<select type='select' name='logistic_own' >";
                foreach ($yesNoArray as $id => $value) {
                    if ($id == $currentTruckOwn) {
                        $truckOwnByCustomerHtml .= "<option value='$id' selected='selected'>$value</option>";
                    } else {
                        $truckOwnByCustomerHtml .= "<option value='$id'>$value</option>";
                    }
                }
                $truckOwnByCustomerHtml .= "</select>";

                /**
                 * Hidden Input Field for Logistic Days
                 */
                $hiddenFields = "<input type='hidden' name='logistic_days' id='logistic-days-$logisticEntityId'/>";
                $hiddenFields .= "<input type='hidden' name='customer_id' value='$customerId' />";
                $hiddenFields .= "<input type='hidden' name='store_id' value='$storeId'/>";
                $hiddenFields .= "<input type='hidden' name='logistic_entity_id' value='$logisticEntityId'/>";

                $accountNumberHtml = "<input type='text' name='logistic_acc_number' value='$currentAccNumber' />";
                $zoneHtml = "<input type='text' name='logistic_zone' value='$currentZone'/>";
                $leadTimeHtml = "<input type='text' name='logistic_lead_time' value='$currentLeadTime'/>";
                $airPortHtml = "<input type='text' name='logistic_airport value'$currentAirPort''/>";
                $consineeHtml = "<input type='text' name='logistic_consinee' value='$currentConsineeId'/>";
                $updateHtml = "<button type='button' data-logistic-entity-id='$logisticEntityId' class='save primary update-logistic-button'>Update Logistic</button>";

                $html .= "<div class='customer-logistic-div'><form id='logistic-update-form-$logisticEntityId'><table class='admin__table-secondary customer-logistic-data-table' style='border: 1px solid;'><tbody>";
                $html .= "<tr><td class='label'>Active</td><td class='value'>" . $activateHtml . "</td> </tr>";
                $html .= "<tr><td class='label'>Logistic Name</td><td class='value'>" . $logisticNamesHtml . "</td> </tr>";
                $html .= "<tr><td class='label'>Account Number</td><td class='value'>" . $accountNumberHtml . "</td> </tr>";
                $html .= "<tr><td class='label'>Truck Owned By Customer</td><td class='value'>" . $truckOwnByCustomerHtml . "</td> </tr>";
                $html .= "<tr><td class='label'>Zone</td><td class='value'>" . $zoneHtml . "</td> </tr>";
                $html .= "<tr><td class='label'>Lead Time</td><td class='value'>" . $leadTimeHtml . "</td> </tr>";
                $html .= "<tr><td class='label'>Airport</td><td class='value'>" . $airPortHtml . "</td> </tr>";
                $html .= "<tr><td class='label'>Consinee Id</td><td class='value'>" . $consineeHtml . "</td> </tr>";
                $html .= $logisticDaysHtml;
                $html .= "<tr><td class='label'></td><td class='value'>" . $updateHtml . "</td></tr>";

                $html .= "</tbody></table>";
                $html .= $hiddenFields;
                $html .= "</form></div>";
            }

        }
        return $html;
    }

    /**
     * New Logistic Add Form HTML
     * @return string
     */
    public function getLogisticAddNewHtmlForm()
    {
        $customerId = $this->_customerId;
        $storeId = $this->_addressID;
        /**
         * Logistic Names
         */
        $logisticNamesHtml = '';
        $logisticsData = $this->getAllLogisticsData();
        $logisticNamesHtml .= "<select type='select' name='logistic_new_name' id='logistic_new_name'>";
        foreach ($logisticsData as $id => $value) {
            $logisticNamesHtml .= "<option value='$id'>$value</option>";
        }
        $logisticNamesHtml .= "</select>";

        /**
         * Truck Owned By customer
         */
        $truckOwnByCustomerHtml = '';
        $yesNoArray = $this->getYesNoArray();
        $truckOwnByCustomerHtml .= "<select type='select' name='logistic_new_own' id='logistic_new_own'>";
        foreach ($yesNoArray as $id => $value) {
            if ($id == '1') {
                $truckOwnByCustomerHtml .= "<option value='$id' selected='selected'>$value</option>";
            } else {
                $truckOwnByCustomerHtml .= "<option value='$id'>$value</option>";
            }
        }
        $truckOwnByCustomerHtml .= "</select>";

        /**
         * Activate
         */
        $activateHtml = '';
        $activateHtml .= "<select type='select' name='logistic_new_activate' id='logistic_new_activate'>";
        foreach ($yesNoArray as $id => $value) {
            if ($id == '1') {
                $activateHtml .= "<option value='$id' selected='selected'>$value</option>";
            } else {
                $activateHtml .= "<option value='$id'>$value</option>";
            }
        }
        $activateHtml .= "</select>";

        /**
         * Checkbox
         */
        $logisticDaysHtml = '';
        $daysArray = $this->getDaysArray();
        foreach ($daysArray as $id => $label) {
            $logisticDaysHtml .= "<tr><td class='label'>" . $label . "</td><td class='value'><input type='checkbox' class='logistic-checkbox' value='$id'></td></tr>";
        }
        /**
         * Account Information , Zone ,Lead Time , Airport , Consinee ID
         */
        $accountNumberHtml = "<input type='text' value='' name='logistic_new_acc_number'/>";
        $zoneHtml = "<input type='text' value='' name='logistic_new_zone'/>";
        $leadTimeHtml = "<input type='text' value='' name='logistic_new_lead_time'/>";
        $airPortHtml = "<input type='text' value='' name='logistic_new_airport'/>";
        $consineeHtml = "<input type='text' value='' name='logistic_new_consinee'/>";
        $saveHtml = "<button type='button' class='save primary save-logistic-button'>Save Logistic</button>";

        $hiddenFields = "<input type='hidden' name='customer_id' value='$customerId' />";
        $hiddenFields .= "<input type='hidden' name='store_id' value='$storeId'/>";


        $html = '';
        $html .= "<table class='admin__table-secondary customer-logistic-new-form' style='display:none;border: 1px solid;'><tbody>";
        $html .= "<tr><td class='label'>Active</td><td class='value'>" . $activateHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Logistic Name</td><td class='value'>" . $logisticNamesHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Account Number</td><td class='value'>" . $accountNumberHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Truck Owned By Customer</td><td class='value'>" . $truckOwnByCustomerHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Zone</td><td class='value'>" . $zoneHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Lead Time</td><td class='value'>" . $leadTimeHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Airport</td><td class='value'>" . $airPortHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Consinee Id</td><td class='value'>" . $consineeHtml . "</td></tr>";
        $html .= $logisticDaysHtml;
        $html .= "<tr><td class='label'></td><td class='value'>" . $saveHtml . "</td></tr>";
        $html .= "</tbody></table>";
        $html .= $hiddenFields;

        return $html;
    }

    /**
     * Store Setting Details
     * @return string
     */
    public function storeDetailsHtml()
    {
        $customerId = $this->_customerId;
        $storeID = $this->_addressID;
        $customerAddress = $this->getCustomerAddress();
        $street = implode(',', $customerAddress->getStreet());
        $city = $customerAddress->getCity();
        $currentRegion = $customerAddress->getRegionId();
        $zip_code = $customerAddress->getPostcode();
        $telephone = $customerAddress->getTelephone();
        $currentCountryId = $customerAddress->getCountryId();
        $storeName = $customerAddress->getCompany();

        $storeIdHtml = "<input type='text' value='$storeID' disabled='disabled'/>";
        $storeNameHtml = "<input type='text' value='$storeName' name='store_name'/>";
        $streetHtml = "<input type='text' value='$street' id='street' name='street'/>";
        $zipCodeHtml = "<input type='text' value='$zip_code' id='zip_code' name='zip_code'/>";
        $telephoneHtml = "<input type='text' value='$telephone' id='telephone' name='telephone'/>";
        $cityHtml = "<input type='text' value='$city' id='city' name='city'/>";

        /**
         * Custom Table Dependency Data
         */
        $customerStore = $this->helper->getCustomerStoreByStoreId($customerId, $storeID);
        $currentStoreEmail = $currentVendorPreference = $currentStoreActivate = $currentCostChannel='';
        $currentCostChannel=$currentDistributionCenter='';
        if ($customerStore != null) {
            $currentStoreEmail = $customerStore->getStoreEmail();
            $currentVendorPreference = $customerStore->getVendorPreference();
            $currentStoreActivate = $customerStore->getStoreActivate();
            $currentDistributionCenter = $customerStore->getDistributionCenter();
            $currentCostChannel = $customerStore->getCostChannel();
        }

        $storeEmailHtml = "<input type='text' value='$currentStoreEmail' id='store_email' name='store_email'/>";

        /**
         * Sales Rep
         */
        $currentSalesRep = $this->getCustomer()->getCustSalesRep();
        $salesRep = $this->helper->getSalesRepCustomerCollection();
        $salesRepHtml = "<select type='select' name='sales_rep' id='sales_rep'>";
        foreach ($salesRep as $oneItem) {
            $salesRepId = $oneItem['value'];
            $salesRepEmail = ($salesRepId == '') ? 'Please Select' : $oneItem['label'];
            if ($currentSalesRep == $salesRepId) {
                $salesRepHtml .= "<option value='$salesRepId' selected='selected'>$salesRepEmail</option>";
            } else {
                $salesRepHtml .= "<option value='$salesRepId'>$salesRepEmail</option>";
            }
        }
        $salesRepHtml .= "</select>";

        /**
         * Address Regions
         */
        $regions = $this->getRegionsOfCountry();
        $regionHtml = "<select type='select' name='region' id='region'>";
        foreach ($regions as $regionId => $regionName) {
            if ($currentRegion == $regionId) {
                $regionHtml .= "<option value='$regionId' selected='selected'>$regionName</option>";
            } else {
                $regionHtml .= "<option value='$regionId'>$regionName</option>";
            }
        }
        $regionHtml .= "</select>";

        /**
         * Country HTML
         */
        $countryData = $this->getAllCountries();
        $countryHtml = "<select type='select' name='country' id='country'>";
        foreach ($countryData as $countryId => $countryName) {
            if ($currentCountryId == $countryId) {
                $countryHtml .= "<option value='$countryId' selected='selected'>$countryName</option>";
            } else {
                $countryHtml .= "<option value='$countryId'>$countryName</option>";
            }
        }
        $countryHtml .= "</select>";
        /**
         * Distribution center
         */
        $distributionCenter = $this->getDistributionCenterArray();
        $distributionCenterHtml = "<select type='select' name='distribution_center' id='distribution_center'>";
        foreach ($distributionCenter as $distributionId => $centerName) {
            if ($currentDistributionCenter == $distributionId) {
                $distributionCenterHtml .= "<option value='$distributionId' selected='selected'>$centerName</option>";
            } else {
                $distributionCenterHtml .= "<option value='$distributionId'>$centerName</option>";
            }
        }
        $distributionCenterHtml .= "</select>";
        /**
         * Cost Channel center
         */
        $costChannel = $this->getCostChannelArray();
        $costChannelHtml = "<select type='select' name='cost_channel' id='cost_channel'>";
        foreach ($costChannel as $ccId => $ccName) {
            if ($currentCostChannel == $ccId) {
                $costChannelHtml .= "<option value='$ccId' selected='selected'>$ccName</option>";
            } else {
                $costChannelHtml .= "<option value='$ccId'>$ccName</option>";
            }
        }
        $costChannelHtml .= "</select>";

        /**
         * Vendor Preference
         */
        $vendorPrefHtml = '';
        if ($currentVendorPreference == '0') {
            $vendorPrefHtml .= " <input type='checkbox' name='vendor_preference' />";
        } else {
            $vendorPrefHtml .= " <input type='checkbox' name='vendor_preference' checked />";
        }

        /**
         * Activate Store
         */
        $activateHtml = '';
        $yesNoArray = $this->getYesNoArray();
        $activateHtml .= "<select type='select' name='store_activate' id='store_activate'>";
        foreach ($yesNoArray as $id => $value) {
            if ($currentStoreActivate == $id || $currentStoreActivate=='' ) {
                $activateHtml .= "<option value='$id' selected='selected'>$value</option>";
            } else {
                $activateHtml .= "<option value='$id'>$value</option>";
            }
        }
        $activateHtml .= "</select>";

        $hiddenFields = "<input type='hidden' name='customer_id' value='$customerId' />";
        $hiddenFields .= "<input type='hidden' name='store_id' value='$storeID'/>";

        /**
         *  Create HTML for Stores
         */
        $html = "<table class='admin__table-secondary customer-stores-setting'><tbody>";
        $html .= "<tr><td class='label'>Store ID</td><td class='value'>" . $storeIdHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Store Name</td><td class='value'>" . $storeNameHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Activate Store</td><td class='value'>" . $activateHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Sales Rep</td><td class='value'>" . $salesRepHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Street</td><td class='value'>" . $streetHtml . "</td></tr>";
        $html .= "<tr><td class='label'>City</td><td class='value'>" . $cityHtml . "</td></tr>";
        $html .= "<tr><td class='label'>State</td><td class='value'>" . $regionHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Zip Code</td><td class='value'>" . $zipCodeHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Telephone</td><td class='value'>" . $telephoneHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Store Email</td><td class='value'>" . $storeEmailHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Country</td><td class='value'>" . $countryHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Distribution Center</td><td class='value'>" . $distributionCenterHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Cost Channel</td><td class='value'>" . $costChannelHtml . "</td></tr>";
        $html .= "<tr><td class='label'>Vendor Preference</td><td class='value'>" . $vendorPrefHtml . "</td></tr>";
        $html .= "</tbody></table>";
        $html .= $hiddenFields;
        return $html;
    }

    /**
     * List Of logistics
     * @return array
     */
    public function getAllLogisticsData()
    {
        return [
            'FL' => 'Florida Beauty',
            'AM' => 'Armellini'

        ];
    }

    /**
     * Return Yes or No array
     * @return array
     */
    public function getYesNoArray()
    {
        return [
            '0' => 'No',
            '1' => 'Yes'
        ];
    }

    /**
     * Return Store Visibility Option Array
     */
    public function getStoreVisibilityOptionArray()
    {
        return [
            '0' => 'All',
            '1' => 'Only For Me',
            '2' => 'Only For Limited'
        ];
    }

    /**
     * Return Yes or No array
     * @return array
     */
    public function getDaysArray()
    {
        return [
            'sun' => 'Sunday',
            'mon' => 'Monday',
            'tue' => 'Tuesday',
            'wed' => 'Wednesday',
            'thu' => 'Thursday',
            'fri' => 'Friday'
        ];
    }

    /**
     * Return Distribution Center Array
     * @return array
     */
    public function getDistributionCenterArray()
    {
        return [
            '' => 'Please Select',
            'BO' => 'Boston'
        ];
    }

    /**
     * Return Cost Channel Array
     * @return array
     */
    public function getCostChannelArray()
    {
        return [
            '' => 'Please Select',
            'MB' => 'Miami - Boston'
        ];
    }

    /**
     * Return Customer Object
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->helper->getCustomerById($this->_customerId);
    }

    /**
     * Get Customer Address Data
     * @return Address
     */
    public function getCustomerAddress()
    {
        return $this->helper->getCustomerAddressById($this->_addressID);
    }

    /**
     * Get All regions of USA
     * @return array
     */
    public function getRegionsOfCountry()
    {
        $regionsData = [];
        $countryCode = 'USA';
        $regionCollection = $this->country->loadByCode($countryCode)->getRegions();
        $regions = $regionCollection->loadData();
        $count = 1;
        foreach ($regions as $region) {
            $regionsData[$region->getRegionId()] = $region->getName();
        }
        return $regionsData;
    }

    /**
     * Get ALl Country
     * @return Collection
     */
    public function getCountryCollection()
    {
        return $this->countryCollectionFactory->create()->loadByStore();
    }

    /**
     * Get Country List
     *
     * @return array
     */
    public function getAllCountries()
    {
        $countryArray = [];
        $options = $this->getCountryCollection()
            ->setForegroundCountries($this->getTopDestinations())
            ->toOptionArray();
        $count = 1;
        foreach ($options as $country) {
            if ($count == 1) {
                $countryArray[''] = 'Please Select';
                $count++;
                continue;
            }
            $countryArray[$country['value']] = $country['label'];
        }
        return $countryArray;
    }

    /**
     * Retrieve list of top destinations countries
     *
     * @return array
     */
    protected function getTopDestinations()
    {
        $destinations = (string)$this->scopeConfig->getValue(
            'general/country/destinations',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }

}
