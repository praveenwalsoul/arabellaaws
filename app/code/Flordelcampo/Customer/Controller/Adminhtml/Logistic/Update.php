<?php

namespace Flordelcampo\Customer\Controller\Adminhtml\Logistic;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Customer\Helper\Data;

class Update extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        Data $helper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->helper = $helper;
        $this->resultPageFactory = $resultPageFactory;

        parent::__construct($context);
    }

    /**
     * Add New Logistic
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();

        try {
            $params = $this->getRequest()->getParams();
            /*echo '<pre>';
            print_r($params);
            */
            $logisticEntityId = $params['logistic_entity_id'];
            $logisticNewData = [
                'acc_number' => $params['logistic_acc_number'],
                'truck_own_by_customer' => $params['logistic_own'],
                'zone' => $params['logistic_zone'],
                'lead_time' => $params['logistic_lead_time'],
                'air_port' => $params['logistic_airport_value'],
                'logistic_days' => $params['logistic_days'],
                'activate' => $params['logistic_activate'],
                'consinee_id' => $params['logistic_consinee'],
                'customer_id' => $params['customer_id'],
                'store_id' => $params['store_id'],
                'logistic_id' => $params['logistic_name']
            ];
            $customerLogisticModel = $this->helper->getLogisticModelByEntityId($logisticEntityId);
            if ($customerLogisticModel) {
                foreach ($logisticNewData as $field => $value) {
                    $customerLogisticModel->setData($field, $value);
                }
                $customerLogisticModel->save();
            }

            $data = [
                'status' => true,
                'message' => 'Logistic Updated Successfully'
            ];
        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }
}

