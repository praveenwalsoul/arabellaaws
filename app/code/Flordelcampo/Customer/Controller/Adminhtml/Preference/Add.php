<?php

namespace Flordelcampo\Customer\Controller\Adminhtml\Preference;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Customer\Helper\Data;

class Add extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        Data $helper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->helper = $helper;
        $this->resultPageFactory = $resultPageFactory;

        parent::__construct($context);
    }

    /**
     * Add New Logistic
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();

        try {
            $params = $this->getRequest()->getParams();
            $preferenceNewData =[
                'customer_id'=>$params['customer_id'],
                'store_id'=>$params['store_id'],
                'vendor_id'=>$params['preference_vendor'],
                'store_availability'=>$params['preference_store_visibility'],
                'category_ids'=>$params['preference_category']

            ];

            $customerPreferenceModel = $this->helper->getCustomerVendorPreferenceModel();
            foreach ($preferenceNewData as $field => $value) {
                $customerPreferenceModel->setData($field, $value);
            }
            $customerPreferenceModel->save();
            $data = [
                'status' => true,
                'message' => 'Preference Added Successfully'
            ];
        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }
}

