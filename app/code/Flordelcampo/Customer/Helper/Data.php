<?php

namespace Flordelcampo\Customer\Helper;

use Flordelcampo\Customer\Model\Logistics;
use Flordelcampo\Customer\Model\Payment;
use Flordelcampo\Customer\Model\Stores;
use Flordelcampo\Customer\Model\VendorPreference;
use Magento\Catalog\Helper\Category;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Address;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use \Magento\Framework\App\Helper\AbstractHelper;
use Flordelcampo\Customer\Model\PaymentFactory;
use Flordelcampo\Customer\Model\LogisticsFactory;
use Flordelcampo\Customer\Model\StoresFactory;
use Flordelcampo\Customer\Model\VendorPreferenceFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Payment\Model\PaymentMethodList;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Data
 *
 * Flordelcampo\Customer\Helper
 */
class Data extends AbstractHelper
{
    const ACCOUNT_GROUP = 'flordel_vendor_configuration/general/account_group';

    /**
     * Collection
     *
     * @var Collection
     */
    private $_customerGroup;
    /**
     * Customer Factory
     *
     * @var CustomerFactory
     */
    private $_customerFactory;
    /**
     * Customer Object
     *
     * @var Customer
     */
    private $_customer;
    /**
     * Payment Factory
     *
     * @var PaymentFactory
     */
    protected $paymentFactory;
    /**
     * @var AddressFactory
     */
    protected $addressFactory;
    /**
     * @var LogisticsFactory
     */
    protected $logisticFactory;
    /**
     * @var StoresFactory
     */
    protected $storeFactory;
    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionFactory;
    /**
     * @var Category
     */
    protected $categoryHelper;
    /**
     * @var VendorPreferenceFactory
     */
    protected $preferenceFactory;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var PaymentMethodList
     */
    protected $paymentMethodList;
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Data constructor.
     *
     * @param Collection $customerGroup Customer Group
     * @param CustomerFactory $customerFactory Customer Factory
     * @param Customer $customers Customers
     * @param PaymentFactory $paymentFactory Payment Factory
     * @param AddressFactory $addressFactory Address Factory
     * @param LogisticsFactory $logisticFactory
     * @param StoresFactory $storeFactory
     * @param VendorPreferenceFactory $preferenceFactory
     * @param CollectionFactory $categoryCollectionFactory
     * @param Category $categoryHelper
     * @param StoreManagerInterface $storeManager
     * @param PaymentMethodList $paymentMethodList
     * @param CustomerRepositoryInterface $customerRepository
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Collection $customerGroup,
        CustomerFactory $customerFactory,
        Customer $customers,
        PaymentFactory $paymentFactory,
        AddressFactory $addressFactory,
        LogisticsFactory $logisticFactory,
        StoresFactory $storeFactory,
        VendorPreferenceFactory $preferenceFactory,
        CollectionFactory $categoryCollectionFactory,
        Category $categoryHelper,
        StoreManagerInterface $storeManager,
        PaymentMethodList $paymentMethodList,
        CustomerRepositoryInterface $customerRepository,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_customerGroup = $customerGroup;
        $this->_customerFactory = $customerFactory;
        $this->_customer = $customers;
        $this->paymentFactory = $paymentFactory;
        $this->addressFactory = $addressFactory;
        $this->logisticFactory = $logisticFactory;
        $this->storeFactory = $storeFactory;
        $this->preferenceFactory = $preferenceFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryHelper = $categoryHelper;
        $this->storeManager = $storeManager;
        $this->paymentMethodList = $paymentMethodList;
        $this->customerRepository = $customerRepository;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Return Configuration account group ID
     * @return mixed
     */
    public function getAccountGroupID()
    {
        return $this->scopeConfig->getValue(
            self::ACCOUNT_GROUP,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Return Customer Object
     *
     * @param $customerId
     * @return Customer
     */
    public function getCustomerById($customerId)
    {
        return $this->_customerFactory->create()->load($customerId);
    }

    /**
     * @param $id
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerRepositoryById($id)
    {
        return $this->customerRepository->getById($id);
    }

    /**
     * @param $email
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerRepositoryByEmail($email)
    {
        return $this->customerRepository->get($email);
    }

    /**
     * Return Customer Default Billing Address
     * @param $customerId
     * @return array
     */
    public function getCustomerDefaultBillingAddressArrayByCustomerId($customerId)
    {
        $billingAddress = [];
        $customer = $this->getCustomerById($customerId);
        $billingAddressId = $customer->getDefaultBilling();
        if ($billingAddressId != null) {
            $address = $this->getCustomerAddressById($billingAddressId);
            $addressArray = $address->toArray();
            $addressArray['street'] = explode("\n", $addressArray['street']);
            return $addressArray;
        }
        return $billingAddress;
    }

    /**
     * Return Customer Default Shipping Address
     * @param $customerId
     * @return array
     */
    public function getCustomerDefaultShippingAddressArrayByCustomerId($customerId)
    {
        $shippingAddress = [];
        $customer = $this->getCustomerById($customerId);
        $shippingAddressId = $customer->getDefaultShipping();
        if ($shippingAddressId != '' && !empty($shippingAddressId)) {
            $address = $this->getCustomerAddressById($shippingAddressId);
            $addressArray = $address->toArray();
            $addressArray['street'] = explode("\n", $addressArray['street']);
            return $addressArray;
        }
        return $shippingAddress;
    }

    /**
     * Return Customer Customer Attribute Mobile Number BY Id
     * @param $customerId
     * @return mixed|null
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerMobileNumberById($customerId)
    {
        $mobileNumber = null;
        $customerRepo = $this->getCustomerRepositoryById($customerId);
        if ($customerRepo->getCustomAttribute('customer_mobile_number') != null) {
            $mobileNumber = $customerRepo->getCustomAttribute('customer_mobile_number')->getValue();
        }
        return $mobileNumber;
    }

    /**
     * Return Customer Object By Email Id
     * @param $emailId
     * @return Customer
     */
    public function getCustomerByEmail($emailId)
    {
        $websiteId = $this->getWebsiteId();
        $websiteId = 1;
        return $this->_customerFactory->create()
            ->setWebsiteId($websiteId)
            ->loadByEmail($emailId);
    }

    public function getWebsiteId()
    {
        return $this->storeManager->getStore()->getWebsiteId();
    }

    /**
     * Return Customer Sales Rep
     * @param $customerId
     *
     * @return string|null
     */
    public function getCustomerSalesRep($customerId)
    {
        $customer = $this->_customerFactory->create()->load($customerId);
        $salesRep = null;
        if (!empty($customer->getCustSalesRep())) {
            $salesRep = $customer->getCustSalesRep();
        }

        return $salesRep;
    }

    /**
     * Return All customer groups
     *
     * @return array
     */
    public function getCustomerGroups()
    {
        /*echo '<pre>'; print_r($this->_customerGroup->getData());*/
        return $this->_customerGroup->toOptionArray();
    }

    /**
     * Return Customer collection data
     *
     * @return mixed
     */
    public function getCustomerCollection()
    {
        return $this->_customer->getCollection()
            ->addAttributeToSelect("*")
            ->load();
    }

    /**
     * Return Customer collection for store
     * @param $storeId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getCustomerCollectionForStore($storeId)
    {
        $customerCollection = $this->_customerFactory->create()->getCollection();
        if (is_array($storeId)) {
            $customerCollection->addFieldToFilter("store_id", ['in' => $storeId]);
        } else {
            $customerCollection->addFieldToFilter("store_id", $storeId);
        }
        return $customerCollection;
    }

    /**
     * Returns customer who belongs to sales_rep group
     *
     * @return array
     */
    public function getSalesRepCustomerCollection()
    {
        $salesRepCollection = $this->_customerFactory->create()->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter("group_id", 4)->load();

        $filterArray[] = ['value' => '', 'label' => __('Please Select')];
        foreach ($salesRepCollection as $customer) {
            $filterArray[] = [
                'value' => $customer->getId(),
                'label' => $customer->getEmail()
            ];

        }
        return $filterArray;
    }

    /**
     * @param $customerId
     *
     * @return Payment|null
     */
    public function getCustomerPaymentSettingsById($customerId)
    {
        $paymentModel = $this->paymentFactory->create()->load($customerId, 'customer_id');
        if (isset($paymentModel) && $paymentModel->getCustomerId()) {
            return $paymentModel;
        } else {
            return null;
        }
    }

    /**
     * @param $addressId
     *
     * @return Address
     */
    public function getCustomerAddressById($addressId)
    {
        return $this->addressFactory->create()->load($addressId);
    }

    /**
     * @param $customerId
     *
     * @return Logistics|null
     */
    public function getCustomerLogisticById($customerId)
    {
        $logisticModel = $this->logisticFactory->create()->load($customerId, 'customer_id');
        if (isset($logisticModel) && $logisticModel->getCustomerId()) {
            return $logisticModel;
        } else {
            return null;
        }
    }

    /**
     * @param $customerId
     * @param $storeId
     * @return AbstractCollection|null
     */
    public function getCustomerLogisticCollectionByStoreId($customerId, $storeId)
    {
        $logisticModel = $this->logisticFactory->create();
        $logisticCollection = $logisticModel->getCollection();
        $logisticCollection->addFieldToFilter('customer_id', $customerId);
        $logisticCollection->addFieldToFilter('store_id', $storeId);
        if (count($logisticCollection) > 0) {
            return $logisticCollection;
        } else {
            return null;
        }
    }

    /**
     * @param $customerId
     * @param $storeId
     * @return Stores|null
     */
    public function getCustomerStoreByStoreId($customerId, $storeId)
    {
        $storeModel = $this->storeFactory->create();
        $storeCollection = $storeModel->getCollection();
        $storeCollection->addFieldToFilter('customer_id', $customerId);
        $storeCollection->addFieldToFilter('store_id', $storeId);
        if (count($storeCollection) > 0) {
            $storeSettingEntityId = '';
            foreach ($storeCollection as $collection) {
                $storeSettingEntityId = $collection->getEntityId();
            }
            return $this->storeFactory->create()->load($storeSettingEntityId);
        } else {
            return null;
        }
    }

    /**
     * Return Active Customer Stores Data
     * @param $customerId
     * @return array
     */
    public function getActiveCustomerStoresByCustomerId($customerId)
    {
        $customer = $this->getCustomerById($customerId);
        $customerAddress = [];
        if ($customer->getAddresses() != null) {
            foreach ($customer->getAddresses() as $address) {
                $storeId = $address->getId();
                $customerStore = $this->getCustomerStoreByStoreId($customerId, $storeId);
                /**
                 * If customer store in null means by default store is active
                 * If customer update store then it will updated in the store table
                 */
                if ($customerStore == null || $customerStore->getStoreActivate() == '1') {
                    $customerAddress[$address->getId()] = $address->getCompany();
                }
            }
        }
        return $customerAddress;
    }

    /**
     * Return Logistic Model By Its entity Id
     * @param $logisticEntityId
     * @return Logistics
     */
    public function getLogisticModelByEntityId($logisticEntityId)
    {
        return $this->logisticFactory->create()->load($logisticEntityId);
    }

    /**
     * Return Preference Model By Its Entity ID
     *
     * @param $preferenceEntityId
     * @return VendorPreference
     */
    public function getPreferenceModelByEntityId($preferenceEntityId)
    {
        return $this->preferenceFactory->create()->load($preferenceEntityId);
    }

    /**
     * Return Customer Store Model Object
     * @return Stores
     */
    public function getCustomerStoreModel()
    {
        return $this->storeFactory->create();
    }

    /**
     * Return Customer Logistics Model Object
     * @return Logistics
     */
    public function getCustomerLogisticModel()
    {
        return $this->logisticFactory->create();
    }

    /**
     * Return Customer Vendor Preference Model Object
     * @return VendorPreference
     */
    public function getCustomerVendorPreferenceModel()
    {
        return $this->preferenceFactory->create();
    }

    /**
     * Preference Collection By Store and Customer
     *
     * @param $customerId
     * @param $storeId
     * @return AbstractCollection|null
     */
    public function getCustomerPreferenceCollectionByStoreId($customerId, $storeId)
    {
        $preferenceModel = $this->preferenceFactory->create();
        $preferenceCollection = $preferenceModel->getCollection();
        $preferenceCollection->addFieldToFilter('customer_id', $customerId);
        $preferenceCollection->addFieldToFilter('store_id', $storeId);
        if (count($preferenceCollection) > 0) {
            return $preferenceCollection;
        } else {
            return null;
        }
    }

    /**
     * Return ALl Payment Methods
     * @param $storeId
     * @return array
     */
    public function getAllPaymentMethodsByStoreId($storeId)
    {
        $methods = $this->paymentMethodList->getList($storeId);
        $payMethods = [];
        foreach ($methods as $method) {
            $payMethods[$method->getCode()] = $method->getTitle();
        }
        return $payMethods;
    }

    /**
     * Return Active Payment Methods
     * @param $storeId
     * @return array
     */
    public function getAllActivePaymentMethodsByStoreId($storeId)
    {
        $methods = $this->paymentMethodList->getActiveList($storeId);
        $payMethods = [];
        foreach ($methods as $method) {
            $payMethods[] = [
                'value' => $method->getCode(),
                'label' => $method->getTitle()
            ];
        }
        return $payMethods;
    }

    /**
     * Return Default Payment Method
     * @param $storeId
     * @return array
     */
    public function getDefaultPaymentMethodForArabella($storeId)
    {
        $methods = $this->paymentMethodList->getList($storeId);
        $payMethods = [];
        foreach ($methods as $method) {
            if ($method->getCode() == 'authorizenet_acceptjs') {
                $payMethods[] = [
                    'code' => $method->getCode(),
                    'title' => $method->getTitle()
                ];
                break;
            }
        }
        return $payMethods;
    }
}