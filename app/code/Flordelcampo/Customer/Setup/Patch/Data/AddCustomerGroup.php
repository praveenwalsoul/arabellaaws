<?php

namespace Flordelcampo\Customer\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;
use Magento\Customer\Model\GroupFactory;

/**
 * Class AddCustomerGroup
 *
 * Flordelcampo\Customer\Setup\Patch\Data
 */
class AddCustomerGroup implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;
    /**
     * @var GroupFactory
     */
    protected $groupFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        GroupFactory $groupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->groupFactory = $groupFactory;
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $group = $this->groupFactory->create();
        $group->setCode('Sales Rep')
            ->setTaxClassId(3)
            ->save();

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get Alias
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

}