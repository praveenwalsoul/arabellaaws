<?php

namespace Flordelcampo\Customer\Setup\Patch\Data;

use Exception;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class CreateCustomer
 *
 * Flordelcampo\Customer\Setup\Patch\Data
 */
class CreateCustomer implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;
    /**
     * @var CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * CreateCustomer constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerFactory $customerFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerFactory $customerFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * Create Customer
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        try {
            $websiteId = $this->storeManager->getStore()->getWebsiteId();
            /**
             * Amazon customer
             */
            $customer = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail("amazon@arabellabouqets.com");
            $customer->setFirstname("Amazon");
            $customer->setLastname("Flordel");
            $customer->setPassword("amazon@123");
            $customer->save();

            /**
             * Walmart customer
             */
            $customer = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail("walmart@arabellabouquets.com");
            $customer->setFirstname("Walmart");
            $customer->setLastname("Flordel");
            $customer->setPassword("walmart@123");
            $customer->save();

        } catch (Exception $e) {
            return '';
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }
}