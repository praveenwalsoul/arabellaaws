<?php

namespace Flordelcampo\Customer\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\Customer\Model\ResourceModel\VendorPreference as ResourcePreferenceModel;

/**
 * Class Payment
 *
 * Flordelcampo\Customer\Model
 */
class VendorPreference extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourcePreferenceModel::class);
    }

}