<?php

namespace Flordelcampo\Customer\Model\Config\Source;

use Flordelcampo\Customer\Helper\Data;

/**
 * Class Options
 *
 * @package Flordelcampo\Customer\Model\Config\Source
 */
class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Helper Data
     *
     * @var Data
     */
    protected $helper;

    /**
     * Options constructor.
     *
     * @param Data $helper Helper
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {

        if ($this->_options === null) {
            $this->_options = $this->helper->getSalesRepCustomerCollection();
        }
        return $this->_options;
    }

    /**
     * Get text of the option value
     *
     * @param string|integer $value Boolean
     *
     * @return string|bool
     */
    public function getOptionValue($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}