<?php

namespace Flordelcampo\Customer\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\Customer\Model\ResourceModel\Stores as ResourceStoresModel;

/**
 * Class Payment
 *
 * Flordelcampo\Customer\Model
 */
class Stores extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceStoresModel::class);
    }

}