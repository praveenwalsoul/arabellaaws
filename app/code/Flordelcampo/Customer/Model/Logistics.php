<?php

namespace Flordelcampo\Customer\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\Customer\Model\ResourceModel\Logistics as ResourceLogisticsModel;

/**
 * Class Payment
 *
 * Flordelcampo\Customer\Model
 */
class Logistics extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceLogisticsModel::class);
    }

}