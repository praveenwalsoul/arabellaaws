<?php

namespace Flordelcampo\Customer\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\Customer\Model\ResourceModel\Payment as ResourcePaymentModel;

/**
 * Class Payment
 *
 * Flordelcampo\Customer\Model
 */
class Payment extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourcePaymentModel::class);
    }

}