<?php

namespace Flordelcampo\Customer\Model\ResourceModel\VendorPreference;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\Customer\Model\VendorPreference;
use Flordelcampo\Customer\Model\ResourceModel\VendorPreference as ResourcePreferenceModel;

/**
 * Class Collection
 *
 * @package Flordelcampo\Customer\Model\ResourceModel\VendorPreference
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            VendorPreference::class,
            ResourcePreferenceModel::class
        );
    }

}
