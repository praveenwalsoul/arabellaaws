<?php

namespace Flordelcampo\Customer\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class VendorPreference
 *
 * @package Flordelcampo\Customer\Model\ResourceModel
 */
class VendorPreference extends AbstractDb
{
    /**
     * Post constructor.
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Stores Table
     */
    protected function _construct()
    {
        $this->_init('customer_vendor_preferences', 'entity_id');
    }

}