<?php

namespace Flordelcampo\Customer\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Payment
 *
 * @package Flordelcampo\Customer\Model\ResourceModel
 */
class Payment extends AbstractDb
{

    /**
     * Post constructor.
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Box Table
     */
    protected function _construct()
    {
        $this->_init('customer_payment_settings', 'entity_id');
    }

}