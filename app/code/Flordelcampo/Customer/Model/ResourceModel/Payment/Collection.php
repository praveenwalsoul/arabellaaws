<?php

namespace Flordelcampo\Customer\Model\ResourceModel\Payment;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\Customer\Model\Payment;
use Flordelcampo\Customer\Model\ResourceModel\Payment as ResourceModelPayment;

/**
 * Class Collection
 *
 * Flordelcampo\Customer\Model\ResourceModel\Payment
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Payment::class,
            ResourceModelPayment::class
        );
    }

}
