<?php

namespace Flordelcampo\Customer\Model\ResourceModel\Stores;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\Customer\Model\Stores;
use Flordelcampo\Customer\Model\ResourceModel\Stores as ResourceModelStores;

/**
 * Class Collection
 *
 * Flordelcampo\Customer\Model\ResourceModel\Stores
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Stores::class,
            ResourceModelStores::class
        );
    }

}
