<?php

namespace Flordelcampo\Customer\Model\ResourceModel\Logistics;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\Customer\Model\Logistics;
use Flordelcampo\Customer\Model\ResourceModel\Logistics as ResourceModelLogistics;

/**
 * Class Collection
 *
 * @package Flordelcampo\Customer\Model\ResourceModel\Logistics
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Logistics::class,
            ResourceModelLogistics::class
        );
    }

}
