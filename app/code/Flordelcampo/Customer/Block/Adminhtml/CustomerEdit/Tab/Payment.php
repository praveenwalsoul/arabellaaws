<?php

namespace Flordelcampo\Customer\Block\Adminhtml\CustomerEdit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Ui\Component\Layout\Tabs\TabInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Config\Model\Config\Source\Yesno;
use Flordelcampo\Customer\Helper\Data;

class Payment extends Generic implements TabInterface
{
    /**
     * Template
     *
     * @var string
     */
    /* protected $_template = 'tab/customer_payment.phtml';*/

    /**
     * Registry
     *
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * @var Yesno
     */
    protected $yesNo;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * View constructor.
     * @param Context $context
     * @param FormFactory $formFactory
     * @param Registry $registry
     * @param Yesno $yesNo
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        FormFactory $formFactory,
        Registry $registry,
        Yesno $yesNo,
        Data $helper,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        $this->yesNo = $yesNo;
        $this->_formFactory = $formFactory;
        $this->helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    public function _prepareForm()
    {
        /**
         * Load Customer Payment Data if exist
         */
        $paymentModel = $this->helper->getCustomerPaymentSettingsById($this->getCustomerId());
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('customer_payment_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Payment Information')]
        );
        $fieldset->addField(
            'hold_payment',
            'select',
            [
                'name' => 'customer_payment_settings[hold_payment]',
                'label' => __('Hold Payment'),
                'title' => __('Hold Payment'),
                'values' => $this->yesNo->toOptionArray(),
                'data-form-part' => $this->getData('target_form'),
                'value' => ($paymentModel != null) ? $paymentModel->getHoldPayment() : ''
            ]
        );

        /*foreach ($stores as $store) {
            if ($store->getIsActive() && $store->getStoreId()) {
                $storeList[] = [
                    'value' => $store->getStoreId(),
                    'label' => $store->getName()
                ];
            }
        }*/
        $currencyList[] = ['value' => 'USD', 'label' => 'United Stated'];
        $currencyList[] = ['value' => 'INR', 'label' => 'India'];

        $fieldset->addField(
            'currency_code',
            'select',
            [
                'name' => 'customer_payment_settings[currency_code]',
                'label' => __('Currency_code'),
                'title' => __('Currency_code'),
                'values' => $currencyList,
                'data-form-part' => $this->getData('target_form'),
                'note' => __('Select Currency Code'),
                'value' => ($paymentModel != null) ? $paymentModel->getCurrencyCode() : ''
            ]
        );

        $customer = $this->helper->getCustomerById($this->getCustomerId());
        $paymentMethod = $this->helper->getAllActivePaymentMethodsByStoreId($customer->getStoreId());
        /* $paymentMethod[] = ['value' => 'paypal', 'label' => 'Pay Pal'];
         $paymentMethod[] = ['value' => 'creditcard', 'label' => 'Credit Card'];*/

        $fieldset->addField(
            'payment_method',
            'multiselect',
            [
                'name' => 'customer_payment_settings[payment_method]',
                'label' => __('Payment Method'),
                'title' => __('Payment Method'),
                'values' => $paymentMethod,
                'data-form-part' => $this->getData('target_form'),
                'note' => __('Select Payment Methods'),
                'value' => ($paymentModel != null) ? $paymentModel->getPaymentMethod() : ''
            ]
        );

        $fieldset->addField(
            'credit_limit',
            'text',
            [
                'name' => 'customer_payment_settings[credit_limit]',
                'label' => __('Credit Limit'),
                'title' => __('Credit Limit'),
                'data-form-part' => $this->getData('target_form'),
                'note' => '',
                'required' => false,
                'class' => 'validate-number',
                'value' => ($paymentModel != null) ? $paymentModel->getCreditLimit() : ''
            ]
        );

        $fieldset->addField(
            'credit_balance',
            'text',
            [
                'name' => 'customer_payment_settings[credit_balance]',
                'label' => __('Credit Balance'),
                'title' => __('Credit Balance'),
                'data-form-part' => $this->getData('target_form'),
                'note' => '',
                'required' => false,
                'class' => 'validate-number',
                'value' => ($paymentModel != null) ? $paymentModel->getCreditBalance() : ''
            ]
        );

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Payment Settings');
    }

    /**
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Payment Settings');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return false;
        }
        return true;
    }

    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

}