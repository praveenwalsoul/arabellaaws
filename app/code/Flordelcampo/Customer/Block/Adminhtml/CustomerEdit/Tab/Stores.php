<?php

namespace Flordelcampo\Customer\Block\Adminhtml\CustomerEdit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Customer\Model\Customer;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Ui\Component\Layout\Tabs\TabInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Config\Model\Config\Source\Yesno;
use Flordelcampo\Customer\Helper\Data;

class Stores extends Generic implements TabInterface
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'tab/customer_stores.phtml';

    /**
     * Registry
     *
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * @var Yesno
     */
    protected $yesNo;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * View constructor.
     * @param Context $context
     * @param FormFactory $formFactory
     * @param Registry $registry
     * @param Yesno $yesNo
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        FormFactory $formFactory,
        Registry $registry,
        Yesno $yesNo,
        Data $helper,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->yesNo = $yesNo;
        $this->_formFactory = $formFactory;
        $this->helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * Return Customer Object
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->helper->getCustomerById($this->getCustomerId());
    }

    /**
     * Return Store Retrieve URL
     *
     * @return string
     */
    public function getStoreRetrieveUrl()
    {
        return $this->getUrl('customerstore/store/retrieve');
    }

    /**
     * Get Store Update URL
     *
     * @return string
     */
    public function getStoreUpdateUrl()
    {
        return $this->getUrl('customerstore/store/update');
    }

    /**
     * Return Logistic Update URL
     *
     * @return string
     */
    public function getLogisticUpdateUrl()
    {
        return $this->getUrl('customerstore/logistic/update');
    }
    /**
     * Return Vendor Preference Add URL
     *
     * @return string
     */
    public function getVendorPreferenceAddUrl()
    {
        return $this->getUrl('customerstore/preference/add');
    }
    /**
     * Return Vendor Preference Update URL
     *
     * @return string
     */
    public function getVendorPreferenceUpdateUrl()
    {
        return $this->getUrl('customerstore/preference/update');
    }

    /**
     * Return Logistic Save URL
     *
     * @return string
     */
    public function getLogisticSaveUrl()
    {
        return $this->getUrl('customerstore/logistic/save');
    }

    /**
     * Return Vendor Preference Update URL
     * @return string
     */
    public function getUpdateVendorPreferenceUrl()
    {
        return $this->getUrl('customerstore/preference/update');
    }

    public function getAddNewPreferenceUrl()
    {
        return $this->getUrl('customerstore/preference/add');
    }
    /**
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Stores Settings');
    }

    /**
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Stores Settings');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            return true;
        }
        return false;
    }

    /**
     * Form Key
     * @return string
     */
    public function getFormKey()
    {
        return parent::getFormKey();
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return false;
        }
        return true;
    }

    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

}