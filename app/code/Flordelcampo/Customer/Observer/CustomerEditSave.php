<?php

namespace Flordelcampo\Customer\Observer;

use Exception;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Flordelcampo\Customer\Model\PaymentFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Flordelcampo\Customer\Helper\Data;

/**
 * Save Customer
 *
 * @package Flordelcampo\Customer\Observer
 */
class CustomerEditSave implements ObserverInterface
{
    /**
     * Payment Factory
     *
     * @var PaymentFactory
     */
    protected $paymentFactory;
    /**
     * Message
     *
     * @var ManagerInterface
     */
    protected $messageManager;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * CustomerEditSave constructor.
     *
     * @param PaymentFactory $paymentFactory
     * @param ManagerInterface $messageManager
     * @param Data $helper
     */
    public function __construct(
        PaymentFactory $paymentFactory,
        ManagerInterface $messageManager,
        Data $helper
    )
    {
        $this->paymentFactory = $paymentFactory;
        $this->messageManager = $messageManager;
        $this->helper = $helper;
    }

    /**
     * Save
     *
     * @param Observer $observer
     * @return bool|void
     */
    public function execute(Observer $observer)
    {
        $customer = $observer->getCustomer();
        $params = $observer->getRequest()->getParams();
        $customerId = $customer->getId();
        try {
            if (isset($params['customer_payment_settings']) && $customerId) {
                $creditBalance = $params['customer_payment_settings']['credit_balance'];
                $creditLimit = $params['customer_payment_settings']['credit_limit'];
                if (isset($params['customer_payment_settings']['payment_method'])) {
                    $paymentMethodArray = $params['customer_payment_settings']['payment_method'];
                    $paymentMethod = implode(",", $paymentMethodArray);
                } else {
                    $paymentMethod = '';
                }

                $holdPayment = $params['customer_payment_settings']['hold_payment'];
                $currencyCode = $params['customer_payment_settings']['currency_code'];

                if ($creditBalance > $creditLimit) {
                    throw new LocalizedException(__('Invalid Credit Balance'));
                }
                /**
                 * If customer is there then update or create New Row
                 */
                $paymentModel = $this->helper->getCustomerPaymentSettingsById($customerId);
                if ($paymentModel != null) {
                    $paymentModel->setCreditLimit($creditLimit)
                        ->setCreditBalance($creditBalance)
                        ->setPaymentMethod($paymentMethod)
                        ->setHoldPayment($holdPayment)
                        ->setCurrencyCode($currencyCode)
                        ->save();
                } else {
                    $this->paymentFactory->create()
                        ->addData(
                            [
                                "customer_id" => $customerId,
                                "credit_balance" => $creditBalance,
                                "credit_limit" => $creditLimit,
                                "payment_method" => $paymentMethod,
                                "hold_payment" => $holdPayment,
                                "currency_code" => $currencyCode,
                            ]
                        )->save();

                }

            }

        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return false;
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return false;
        }
    }
}