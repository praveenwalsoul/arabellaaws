<?php

namespace Flordelcampo\Catalog\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;

/**
 * Class AddCustomerGroup
 *
 * @package Flordelcampo\Catalog\Setup\Patch\Data
 */
class AddInstructionAttribute implements DataPatchInterface
{

    /**
     * Moduele Data
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * EAV setup
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * Category
     *
     * @var CategorySetupFactory
     */
    protected $categorySetupFactory;

    /**
     * AddCustomerGroup constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup Module
     * @param EavSetupFactory $eavSetupFactory Eav
     * @param CategorySetupFactory $categorySetupFactory Category
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        CategorySetupFactory $categorySetupFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
    }

    /**
     * TO Apply
     *
     * @return DataPatchInterface|void
     *
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );
        $categorySetup = $this->categorySetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );

        // get default attribute set id
        $attributeSetId = $categorySetup->getDefaultAttributeSetId(
            Product::ENTITY
        );

        $entityTypeId = ProductAttributeInterface::ENTITY_TYPE_CODE;
        $attributeGroupId = $eavSetup->getDefaultAttributeGroupId(
            $entityTypeId,
            $attributeSetId
        );
        $groupName = $eavSetup->getAttributeGroup(
            $entityTypeId,
            $attributeSetId,
            $attributeGroupId,
            'attribute_group_name'
        );

        /**
         * Input Type Text Field Array data
         */
        $inputText_Ar[] = [
            'code' => 'instructions',
            'label' => 'Instructions'
        ];

        foreach ($inputText_Ar as $attr) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                trim($attr['code']),
                [
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => trim($attr['label']),
                    'input' => 'textarea',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'group' => $groupName,
                    'system' => false,
                ]
            );
        }
        /**
         * Input Text Field Ends
         */
    }

    /**
     * Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Alias
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }
}