<?php

namespace Flordelcampo\Catalog\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;

/**
 * Class AddCustomerGroup
 *
 * @package Flordelcampo\Catalog\Setup\Patch\Data
 */
class AddCustomProductAttributes implements DataPatchInterface
{

    /**
     * Moduele Data
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * EAV setup
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * Category
     *
     * @var CategorySetupFactory
     */
    protected $categorySetupFactory;

    /**
     * AddCustomerGroup constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup Module
     * @param EavSetupFactory $eavSetupFactory Eav
     * @param CategorySetupFactory $categorySetupFactory Category
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        CategorySetupFactory $categorySetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
    }

    /**
     * TO Apply
     *
     * @return DataPatchInterface|void
     *
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );
        $categorySetup = $this->categorySetupFactory->create(
            ['setup' => $this->moduleDataSetup]
        );

        // get default attribute set id
        $attributeSetId = $categorySetup->getDefaultAttributeSetId(
            Product::ENTITY
        );

        $entityTypeId = ProductAttributeInterface::ENTITY_TYPE_CODE;
        $attributeGroupId = $eavSetup->getDefaultAttributeGroupId(
            $entityTypeId,
            $attributeSetId
        );
        $groupName = $eavSetup->getAttributeGroup(
            $entityTypeId,
            $attributeSetId,
            $attributeGroupId,
            'attribute_group_name'
        );

        /**
         * Input Type Text Field Array data
         */
        $inputText_Ar[] = [
            'code' => 'vendor_sku',
            'label' => 'Vendor SKU'
        ];
        $inputText_Ar[] = [
            'code' => 'vendor_product_name',
            'label' => 'Vendor Product Name'
        ];
        $inputText_Ar[] = [
            'code' => 'fi_id',
            'label' => 'Unique Identifier ID'
        ];
        $inputText_Ar[] = [
            'code' => 'rewardpoints_earn',
            'label' => 'Number of points earned'
        ];
        $inputText_Ar[] = [
            'code' => 'rewardpoints_spend',
            'label' => 'Buy with number of points'
        ];
        $inputText_Ar[] = [
            'code' => 'qty_per_box',
            'label' => 'Qty Per Box'
        ];
        $inputText_Ar[] = [
            'code' => 'count_variants',
            'label' => 'Stems in Bunch'
        ];
        $inputText_Ar[] = [
            'code' => 'searchindex_weight',
            'label' => 'Search Weight'
        ];
        $inputText_Ar[] = [
            'code' => 'special_instructions',
            'label' => 'Special Instructions'
        ];
        $inputText_Ar[] = [
            'code' => 'head_size',
            'label' => 'Head Size'
        ];
        $inputText_Ar[] = [
            'code' => 'petal_count',
            'label' => 'Blooms Per Stem'
        ];
        $inputText_Ar[] = [
            'code' => 'sku_list',
            'label' => 'Sku list'
        ];
        $inputText_Ar[] = [
            'code' => 'box_height',
            'label' => 'Box Height'
        ];
        $inputText_Ar[] = [
            'code' => 'box_width',
            'label' => 'Box Width'
        ];
        $inputText_Ar[] = [
            'code' => 'box_weight',
            'label' => 'Box Weight'
        ];
        $inputText_Ar[] = [
            'code' => 'box_length',
            'label' => 'Box Length'
        ];
        $inputText_Ar[] = [
            'code' => 'dimension_weight',
            'label' => 'Dimensional Weight'
        ];

        foreach ($inputText_Ar as $attr) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                trim($attr['code']),
                [
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => trim($attr['label']),
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'group' => $groupName,
                    'system' => false,
                ]
            );
        }
        /**
         * Input Text Field Ends
         */

        /**
         * Boolean Type Attributes starts
         */
        $boolean_Ar[] = [
            'code' => 'is_bestseller',
            'label' => 'Is Bestseller'
        ];
        $boolean_Ar[] = [
            'code' => 'is_featured',
            'label' => 'Is Featured'
        ];
        $boolean_Ar[] = [
            'code' => 'em_featured',
            'label' => 'Featured Product'
        ];
        $boolean_Ar[] = [
            'code' => 'em_deal',
            'label' => 'Special Deal'
        ];
        $boolean_Ar[] = [
            'code' => 'em_hot',
            'label' => 'Hot Product'
        ];
        $boolean_Ar[] = [
            'code' => 'prime',
            'label' => 'Prime'
        ];
        foreach ($boolean_Ar as $attr) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                trim($attr['code']),
                [
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => trim($attr['label']),
                    'input' => 'boolean',
                    'class' => '',
                    'source' => Boolean::class,
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '0',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'group' => $groupName
                ]
            );
        }
        /**
         * Boolean attribute ends
         */

        /**
         * Dropdown attributes starts
         */
        $eavSetup->addAttribute(
                Product::ENTITY,
                'udropship_calculate_rates',
                [
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Dropship Rates Calculation Type',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'Flordelcampo\Catalog\Model\Config\Source\DropshipRates',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'group' => $groupName,
                    'system' => false,
                ]
            );
        $eavSetup->addAttribute(
                Product::ENTITY,
                'group',
                [
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Group',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'Flordelcampo\Catalog\Model\Config\Source\Group',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'group' => $groupName,
                    'system' => false,
                ]
            );
        $eavSetup->addAttribute(
                Product::ENTITY,
                'sustainably_certified',
                [
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Certified',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'Flordelcampo\Catalog\Model\Config\Source\Certified',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'group' => $groupName,
                    'system' => false,
                ]
            );

        /**
         * Dropdown attributes ends
         */


        /**
         * Multi Select attributes starts
         */
      
        $eavSetup->addAttribute(
            Product::ENTITY,
            'vendor',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Vendor',
                'input' => 'multiselect',
                'class' => '',
                'source' => 'Flordelcampo\Catalog\Model\Config\Source\Vendor',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'group' => $groupName,
                'system' => false,
            ]
        );

        
        $eavSetup->addAttribute(
            Product::ENTITY,
            'box_type',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Box Type',
                'input' => 'multiselect',
                'class' => '',
                'source' => 'Flordelcampo\Catalog\Model\Config\Source\BoxType',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'group' => $groupName,
                'system' => false,
            ]
        );

        
        $eavSetup->addAttribute(
            Product::ENTITY,
            'grade',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Grade',
                'input' => 'multiselect',
                'class' => '',
                'source' => 'Flordelcampo\Catalog\Model\Config\Source\Grade',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'group' => $groupName,
                'system' => false,
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'length',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Length',
                'input' => 'multiselect',
                'class' => '',
                'source' => 'Flordelcampo\Catalog\Model\Config\Source\Length',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'group' => $groupName,
                'system' => false,
            ]
        );


        $eavSetup->addAttribute(
            Product::ENTITY,
            'color',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Color',
                'input' => 'multiselect',
                'class' => '',
                'source' => 'Flordelcampo\Catalog\Model\Config\Source\Color',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'group' => $groupName,
                'system' => false,
            ]
        );


        $eavSetup->addAttribute(
            Product::ENTITY,
            'product_um',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Unit of Measure',
                'input' => 'multiselect',
                'class' => '',
                'source' => 'Flordelcampo\Catalog\Model\Config\Source\ProductUM',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'group' => $groupName,
                'system' => false,
            ]
        );

       
        $eavSetup->addAttribute(
            Product::ENTITY,
            'variety',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Variety',
                'input' => 'multiselect',
                'class' => '',
                'source' => 'Flordelcampo\Catalog\Model\Config\Source\Variety',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'group' => $groupName,
                'system' => false,
            ]
        );
        /**
         * Multi Select attributes ends
         */

        /**
         * Text Area attributes starts
         */
        $text_area_Ar[] = [
            'code' => 'variants',
            'label' => 'All Variants'
        ];
        foreach ($text_area_Ar as $attr) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                trim($attr['code']),
                [
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => trim($attr['label']),
                    'input' => 'textarea',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'group' => $groupName,
                    'system' => false,
                ]
            );
        }
        /**
         * Text Area attributes ends
         */


        /*$attributeGroupName = 'Flordel Attributes';

        // your custom attribute group/tab
        $categorySetup->addAttributeGroup(
            Product::ENTITY,
            $attributeSetId,
            $attributeGroupName,
            2
        );

        // add attribute to group
        $categorySetup->addAttributeToGroup(
            Product::ENTITY,
            $attributeSetId,
            $attributeGroupName,
            'is_bestseller',
            20
        );*/


    }


    /**
     * Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Alias
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }
}