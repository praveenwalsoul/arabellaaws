<?php
namespace Flordelcampo\Catalog\Model\Config\Source;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;


class Grade extends AbstractSource
{

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label'=>'Fancy','value'=>'fancy'],
            ['label'=>'Select','value'=>'select'],
            ['label'=>'Standards','value'=>'standard'],
            ['label'=>'Short','value'=>'short'],
            ['label'=>'Super Select','value'=>'super_select'],
            ['label'=>'Mini','value'=>'mini'],
            ['label'=>'Premium','value'=>'premium'],
            ['label'=>'Jumbo','value'=>'jumbo'],
            ['label'=>'Antique','value'=>'antique'],
            ['label'=>'Elite','value'=>'elite']

        ];
        return $this->_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}