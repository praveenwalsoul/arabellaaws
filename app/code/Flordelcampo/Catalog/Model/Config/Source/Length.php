<?php
namespace Flordelcampo\Catalog\Model\Config\Source;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;


class Length extends AbstractSource
{

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label'=>'60 CM','value'=>'60_cm'],
            ['label'=>'70 CM','value'=>'70_cm'],
            ['label'=>'55 CM','value'=>'55_cm'],
            ['label'=>'40 CM','value'=>'40_cm'],
            ['label'=>'50 CM','value'=>'50_cm'],
            ['label'=>'80 CM','value'=>'80_cm']


        ];
        return $this->_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}