<?php
namespace Flordelcampo\Catalog\Model\Config\Source;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;


class Color extends AbstractSource
{
    /**
     * @var array
     */
    protected $_option;

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_option = $this->toArray();
        return $this->_option;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }

    /**
     * Get options in "key-value" format
     * @return array
     */
    public function toArray()
    {
        return [
            ['label'=>'Assorted','value'=>'assorted'],
            ['label'=>'Holiday Pack','value'=>'holiday_pack'],
            ['label'=>'White','value'=>'white'],
            ['label'=>'Red','value'=>'red'],
            ['label'=>'Pink','value'=>'pink'],
            ['label'=>'Hot Pink','value'=>'hot_pink'],
            ['label'=>'Purple','value'=>'purple'],
            ['label'=>'Lavender','value'=>'lavender'],
            ['label'=>'Yellow','value'=>'yellow'],
            ['label'=>'Orange','value'=>'orange'],
            ['label'=>'Novelties','value'=>'novelties'],
            ['label'=>'Green','value'=>'green'],
            ['label'=>'Bi Color','value'=>'bi_color'],
            ['label'=>'Light Pink','value'=>'light_pink'],
            ['label'=>'Peach','value'=>'peach'],
            ['label'=>'Cream','value'=>'cream'],
            ['label'=>'Dark Purple','value'=>'dark_purple'],
            ['label'=>'Tinted','value'=>'tinted'],
            ['label'=>'Mojito','value'=>'mojito'],
            ['label'=>'Assorted wh-blue','value'=>'assorted_wh-blue'],
            ['label'=>'Blue','value'=>'blue'],
            ['label'=>'Esmeralda','value'=>'esmeralda'],
            ['label'=>'Diamante','value'=>'diamante'],
            ['label'=>'Shocking Blue','value'=>'shocking_blue'],
            ['label'=>'Cherry','value'=>'cherry']
        ];
    }
}