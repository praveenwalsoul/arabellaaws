<?php
namespace Flordelcampo\Catalog\Model\Config\Source;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;


class Variety extends AbstractSource
{

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label'=>'Cool Water','value'=>'cool_water'],
            ['label'=>'Celestial','value'=>'celestial'],
            ['label'=>'Purple Sky','value'=>'purple_sky'],
            ['label'=>'Ilse','value'=>'ilse'],
            ['label'=>'Freedom','value'=>'freedom'],
            ['label'=>'Mirabella','value'=>'mirabella']

        ];
        return $this->_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}