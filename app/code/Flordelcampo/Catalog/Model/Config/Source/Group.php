<?php
namespace Flordelcampo\Catalog\Model\Config\Source;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;


class Group extends AbstractSource
{

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['value' => '', 'label' => __('Please Select')],
            ['label'=>'Alstroemeria','value'=>'alstroemeria'],
            ['label'=>'Carnation','value'=>'carnation'],
            ['label'=>'Sunflower','value'=>'sunflower'],
            ['label'=>'Lily LA','value'=>'lily_la'],
            ['label'=>'Statice','value'=>'statice'],
            ['label'=>'Snapdragon','value'=>'snapdragon'],
            ['label'=>'Pompon Cushion','value'=>'pompon_cushion'],
            ['label'=>'Pompon Daisy','value'=>'pompon_daisy'],
            ['label'=>'Mum Fuji','value'=>'mum_fuji'],
            ['label'=>'Pompon CDN','value'=>'pompon_cdn'],
            ['label'=>'Pompon Novelties','value'=>'pompon_novelties'],
            ['label'=>'Pompon Button','value'=>'pompon_button'],
            ['label'=>'Rose','value'=>'rose'],
            ['label'=>'Spray Rose','value'=>'spray_rose'],
            ['label'=>'Mini Carnation','value'=>'mini_carnation'],
            ['label'=>'Gypsophila','value'=>'gypsophila'],
            ['label'=>'Ruscus','value'=>'ruscus'],
            ['label'=>'Cocculus','value'=>'cocculus'],
            ['label'=>'Brillantina','value'=>'brillantina'],
            ['label'=>'Stock','value'=>'stock'],
            ['label'=>'Bells of Ireland','value'=>'bells_of_ireland'],
            ['label'=>'Aster Monte casino','value'=>'aster_monte_casino'],
            ['label'=>'Aster Solidago','value'=>'aster_solidago'],
            ['label'=>'Hydrangea','value'=>'hydrangea']
            
        ];
       /* alstroemeria
        carnation
        sunflower
        lily_la
        statice
        snapdragon
        pompon_cushion
        pompon_daisy
        mum_fuji
        pompon_cdn
        pompon_novelties
        pompon_button
        rose
        spray_rose
        mini_carnation
        gypsophila
        ruscus
        cocculus
        brillantina
        stock
        bells_of_ireland
        aster_monte_casino
        aster_solidago
        hydrangea*/

        return $this->_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}