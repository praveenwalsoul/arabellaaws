<?php

namespace Flordelcampo\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Vendor extends AbstractSource
{
    protected $vendors;

    public function __construct(\Flordelcampo\VendorRegistration\Model\Vendor $vendorFactory)
    {
        $this->vendors = $vendorFactory;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $collections = $this->vendors->getCollection();
        $data = [];
        $i = 0;
        foreach ($collections as $value) {
            $data[$i]['label'] = $value->getVendorName() . ' ' . $value->getLastName();
            $data[$i]['value'] = $value->getVendorId();
            $i++;
        }
        $this->_options = $data;
        return $this->_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}