<?php

namespace Flordelcampo\Catalog\Helper;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Eav\Model\Config;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Helper\Category;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 *
 * Flordelcampo\Catalog\Helper
 */
class Data extends AbstractHelper
{
    const HARD_GOOD_ID = 'flordel_vendor_configuration/general/hard_good_id';

    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionsFactory;
    /**
     * @var Category
     */
    protected $categoryHelper;
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;
    /**
     * @var ProductRepository
     */
    protected $productRepository;
    /**
     * @var Config
     */
    protected $eavConfig;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepositoryInterface;
    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param CollectionFactory $categoryCollectionFactory
     * @param Category $categoryHelper
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepository $productRepository
     * @param Config $eavConfig
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param StockRegistryInterface $stockRegistry
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        CollectionFactory $categoryCollectionFactory,
        Category $categoryHelper,
        ProductCollectionFactory $productCollectionFactory,
        ProductRepository $productRepository,
        Config $eavConfig,
        ProductRepositoryInterface $productRepositoryInterface,
        StockRegistryInterface $stockRegistry,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->productRepository = $productRepository;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryCollectionsFactory = $categoryCollectionFactory;
        $this->categoryHelper = $categoryHelper;
        $this->eavConfig = $eavConfig;
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->stockRegistry = $stockRegistry;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @return ProductRepositoryInterface
     */
    public function getProductRepositoryInterface()
    {
        return $this->productRepositoryInterface;
    }

    /**
     * @return StockRegistryInterface
     */
    public function getStockRegistryInterface()
    {
        return $this->stockRegistry;
    }

    /**
     * Return collection factory object
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollectionFactory()
    {
        return $this->productCollectionFactory->create();
    }

    /**
     * Get Product Repository By SKU
     *
     * @param $sku
     * @return ProductInterface|Product|null
     * @throws NoSuchEntityException
     */
    public function getProductBySku($sku)
    {
        return $this->productRepository->get($sku);
    }

    /**
     * Return Collection with SKU filter
     *
     * @param $sku
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollectionBySku($sku)
    {
        $collection = $this->getProductCollectionFactory();
        $collection->addAttributeToSelect('*');
        /*
         * $collection->addAttributeToSelect(['entity_id','sku','name']);
         */
        if (is_array($sku)) {
            $collection->addFieldToFilter('sku', ['in' => $sku]);
        } else {
            $collection->addFieldToFilter('sku', $sku);
        }
        return $collection;
    }

    /**
     * Return Product Collection By Vendor Id or Vendor Array
     * @param $vendorId
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollectionByVendor($vendorId)
    {
        $collection = $this->getProductCollectionFactory();
        $collection->addAttributeToSelect(['entity_id', 'sku', 'name', 'vendor']);
        if (empty($vendorId) || $vendorId == null) {
            return $collection;
        }
        /* $collection->addAttributeToSelect('*');*/
        if (is_array($vendorId)) {
            $collection->addFieldToFilter('vendor', ['in' => $vendorId]);
        } else {
            $collection->addFieldToFilter('vendor', ['like' => "%$vendorId%"]);
        }
        return $collection;
    }

    /**
     * Return Hard Good Product Collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getHardGoodProductCollection()
    {
        $hardGoodId = $this->getHardGoodAttributeId();
        $collection = $this->getProductCollectionFactory();
        $collection->addAttributeToSelect(['entity_id', 'sku', 'name']);
        $collection->addFieldToFilter('attribute_set_id', $hardGoodId);
        return $collection;
    }

    /**
     * @return mixed
     */
    public function getHardGoodAttributeId()
    {
        return $this->scopeConfig->getValue(
            self::HARD_GOOD_ID,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Return Non hard good product collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getNonHardGoodProductCollection()
    {
        $hardGoodId = $this->getHardGoodAttributeId();
        $collection = $this->getProductCollectionFactory();
        $collection->addAttributeToSelect(['entity_id', 'sku', 'name']);
        $collection->addFieldToFilter('attribute_set_id', ['neq' => $hardGoodId]);
        return $collection;
    }

    /**
     * Return Vendor Ids associated with SKU
     *
     * @param $sku
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getVendorsBySku($sku)
    {
        $product = $this->getProductBySku($sku);
        return $product->getVendor();
    }

    /**
     * Get Category Collection
     * @param bool $isActive
     * @param bool $level
     * @param bool $sortBy
     * @param bool $pageSize
     * @return Collection
     * @throws LocalizedException
     */
    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->categoryCollectionsFactory->create();
        $collection->addAttributeToSelect('*');
        /*select only active categories in the store*/
        if ($isActive) {
            $collection->addIsActiveFilter();
        }

        /*select categories of certain level*/
        if ($level) {
            $collection->addLevelFilter($level);
        }

        /*sort categories by some value*/
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }

        /*set pagination*/
        if ($pageSize) {
            $collection->setPageSize($pageSize);
        }
        return $collection;
    }

    /**
     * Get Active Current Store Categories
     * @param bool $sorted
     * @param bool $asCollection
     * @param bool $toLoad
     * @return \Magento\Framework\Data\Tree\Node\Collection
     */
    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
    {
        return $this->categoryHelper->getStoreCategories($sorted = false, $asCollection = false, $toLoad = true);
    }

    /**
     * Get All Active and In-active Categories Data
     * @return array
     */
    public function getAllCategoryListArray()
    {
        $categories = $this->getStoreCategories();
        $categoryList = [];
        foreach ($categories as $category) {
            $categoryList[$category->getId()] = $category->getName();
        }
        return $categoryList;
    }

    /**
     * Return Product Attribute Options
     * @param $attribute_code
     * @return array
     */
    public function getAttributeOptionsByCode($attribute_code)
    {
        try {
            $attribute = $this->eavConfig->getAttribute('catalog_product', $attribute_code);
            $options = $attribute->getSource()->getAllOptions();
            $optionsArray = [];
            foreach ($options as $option) {
                $optionsArray[$option['value']] = $option['label'];
            }
            return $optionsArray;
        } catch (\Exception $e) {
            return [];
        }
    }
}
