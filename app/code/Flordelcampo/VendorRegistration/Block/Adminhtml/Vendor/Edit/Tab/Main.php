<?php

namespace Flordelcampo\VendorRegistration\Block\Adminhtml\Vendor\Edit\Tab;

use Flordelcampo\ProductsGrid\Model\Contact;
use Flordelcampo\VendorRegistration\Helper\Data;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Model\Config\Source\Yesno;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;

    /**
     * @var \Flordelcampo\ProductsGrid\Helper\Data $helper
     */
    protected $helper;
    /**
     * @var Yesno
     */
    protected $yesNo;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Data $helper
     * @param Yesno $yesNo
     * @param VendorHelper $vendorHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Data $helper,
        Yesno $yesNo,
        VendorHelper $vendorHelper,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->yesNo = $yesNo;
        $this->vendorHelper = $vendorHelper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('ws_vendor');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('vendor_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Vendor Information')]);
        if ($model->getId()) {
            $fieldset->addField('vendor_id', 'hidden', ['name' => 'vendor_id']);
        }
        $regionList = $this->vendorHelper->getFormattedRegionsDataByCountryId('USA');
        $countryList = $this->vendorHelper->getFormattedCountryListArray();
        $allVendorTypes = $this->vendorHelper->getSelectedVendorTypesArray();

        $fieldset->addField(
            'vendor_name',
            'text',
            [
                'name' => 'vendor_name',
                'label' => __('Vendor Full Name'),
                'title' => __('Vendor Full Name'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'last_name',
            'text',
            [
                'name' => 'last_name',
                'label' => __('Company Name'),
                'title' => __('Company Name'),
                'required' => false,
            ]
        );

        $fieldset->addField(
            'vendor_user_id',
            'text',
            [
                'name' => 'vendor_user_id',
                'label' => __('Vendor User Name'),
                'title' => __('Vendor User Name'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'email',
            'text',
            [
                'name' => 'email',
                'label' => __('Email'),
                'title' => __('Email'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'vendor_type',
            'multiselect',
            [
                'name' => 'vendor_type',
                'label' => __('Vendor Type'),
                'title' => __('Vendor Type'),
                'values' => $allVendorTypes,
                'value' => ($model->getVendorType() != null) ? $model->getVendorType() : '',
                'required' => true
            ]
        );

        $fieldset->addField(
            'address',
            'text',
            [
                'name' => 'address',
                'label' => __('Street Line 1'),
                'title' => __('Street Line 1'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'street',
            'text',
            [
                'name' => 'street',
                'label' => __('Street Line 2'),
                'title' => __('Street Line 2'),
                'required' => true,
            ]
        );
        /*$fieldset->addField(
            'street_2',
            'text',
            [
                'name' => 'street_2',
                'label' => __('Street Line 2'),
                'title' => __('Street Line 2'),
                'required' => true,
            ]
        );*/
        $fieldset->addField(
            'city',
            'text',
            [
                'name' => 'city',
                'label' => __('City'),
                'title' => __('City'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'country_id',
            'select',
            [
                'name' => 'country_id',
                'label' => __('Country Id'),
                'title' => __('Country Id'),
                'values' => $countryList,
                'required' => true,
            ]
        );
        $fieldset->addField(
            'region',
            'select',
            [
                'name' => 'region',
                'label' => __('Region or State'),
                'title' => __('Region or State'),
                'values' => $regionList,
                'required' => true,
            ]
        );
        $fieldset->addField(
            'zipcode',
            'text',
            [
                'name' => 'zipcode',
                'label' => __('Zipcode'),
                'title' => __('Zipcode'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'telephone',
            'text',
            [
                'name' => 'telephone',
                'label' => __('Telephone'),
                'title' => __('Telephone'),
                'required' => true,
            ]
        );

        /*$fieldset->addField(
            'state',
            'text',
            [
                'name' => 'state',
                'label' => __('State'),
                'title' => __('State'),
                //'required' => true,
            ]
        );*/
        /* $fieldset->addField(
             'region_id',
             'text',
             [
                 'name' => 'region_id',
                 'label' => __('Region Id'),
                 'title' => __('Region Id'),
                 //'required' => true,
             ]
         );*/
        $fieldset->addField(
            'carrier_code',
            'text',
            [
                'name' => 'carrier_code',
                'label' => __('Carrier Code'),
                'title' => __('Carrier Code'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'vendor_attn',
            'text',
            [
                'name' => 'vendor_attn',
                'label' => __('Vendor Attn'),
                'title' => __('Vendor Attn'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'remote_ip',
            'text',
            [
                'name' => 'remote_ip',
                'label' => __('Remote Id'),
                'title' => __('Remote Id'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'url_key',
            'text',
            [
                'name' => 'url_key',
                'label' => __('Url key'),
                'title' => __('Url key'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'gst_number',
            'text',
            [
                'name' => 'gst_number',
                'label' => __('Gst number'),
                'title' => __('Gst number'),
                'required' => false
            ]
        );

        $dateFormat = $this->_localeDate->getDateFormat(
            \IntlDateFormatter::MEDIUM
        );
        $timeFormat = $this->_localeDate->getTimeFormat(
            \IntlDateFormatter::MEDIUM
        );

        $fieldset->addField(
            'creation_time',
            'date',
            [
                'name' => 'creation_time',
                'label' => __('Created At'),
                'title' => __('Created At'),
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'disabled' => 'true'
            ]
        );
        $fieldset->addField(
            'update_time',
            'date',
            [
                'name' => 'update_time',
                'label' => __('Update At'),
                'title' => __('Update At'),
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'disabled' => 'true'
            ]
        );
        $fieldset->addField(
            'password',
            'password',
            [
                'name' => 'password',
                'label' => __('Password'),
                'title' => __('Password'),
                'class' => 'input-text validate-current-password required-entry',
                'required' => true,
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'values' => $this->yesNo->toOptionArray(),
                'name' => 'status',
                'value' => ($model != null) ? $model->getStatus() : ''
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Vendor Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Vendor Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
