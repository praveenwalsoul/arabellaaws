<?php

namespace Flordelcampo\VendorRegistration\Block\Adminhtml\Vendor\Edit\Tab;

use Flordelcampo\VendorRegistration\Model\VendorFactory;

class Products extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * Contact factory
     *
     * @var VendorFactory
     */
    protected $vendorFactory;

    /**
     * @var  \Magento\Framework\Registry
     */
    protected $registry;

    protected $_objectManager = null;

    /**
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $registry
     * @param VendorFactory $attachmentFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        VendorFactory $vendorFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = []
    )
    {
        $this->vendorFactory = $vendorFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->_objectManager = $objectManager;
        $this->registry = $registry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * _construct
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('productsGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('vendor_id')) {
            $this->setDefaultFilter(array('in_product' => 1));
        }
    }

    /**
     * add Column Filter To Collection
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_product') {
            $productIds = $this->_getSelectedProducts();

            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * prepare collection
     */
    protected function _prepareCollection()
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('sku');
        $collection->addAttributeToSelect('price');
        $collection->addAttributeToSelect('box_type');
        $collection->addAttributeToSelect('box_height');
        $collection->addAttributeToSelect('weight');
        $collection->addAttributeToSelect('box_weight');
        $collection->addAttributeToSelect('box_length');
        $collection->addAttributeToSelect('box_width');
        $collection->addAttributeToSelect('color');
        $collection->addAttributeToSelect('cost');
        $collection->addAttributeToSelect('dimensional_weight');
        $collection->addAttributeToSelect('group');
        $collection->addAttributeToSelect('head_size');
        $collection->addAttributeToSelect('product_um');
        $collection->addAttributeToSelect('qty_per_box');
        $collection->addAttributeToSelect('dimension_weight');
        $collection->addAttributeToSelect('quantity_and_stock_status');
        $collection->addAttributeToSelect('rewardpoints_earn');
        $collection->addAttributeToSelect('rewardpoints_spend');
        $collection->addAttributeToSelect('searchindex_weight');
        $collection->addAttributeToSelect('shipment_type');
        $collection->addAttributeToSelect('sku_list');
        $collection->addAttributeToSelect('fi_id');
        $collection->addAttributeToSelect('sku_type');
        $collection->addAttributeToSelect('status');
        $collection->addAttributeToSelect('sustainably_certified');
        $collection->addAttributeToSelect('ts_dimensions_height');
        $collection->addAttributeToSelect('ts_dimensions_width');
        $collection->addAttributeToSelect('ts_dimensions_length');
        $collection->addAttributeToSelect('udropship_calculate_rates');
        $collection->addAttributeToSelect('variants');
        $collection->addAttributeToSelect('variety');
        $collection->addAttributeToSelect('vendor_product_name');
        $collection->addAttributeToSelect('vendor_sku');
        $collection->addAttributeToSelect('weight');
        //$collection->addAttributeToSelect('weight_type');
        $collection->addAttributeToFilter('vendor', $this->getRequest()->getParam('vendor_id'));
        //echo $collection->getSelect();die;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        /* @var $model \Flordelcampo\ProductsGrid\Model\Slide */
        $model = $this->_objectManager->get('\Flordelcampo\VendorRegistration\Model\Vendor');

        /*$this->addColumn(
            'in_product',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_product',
                'align' => 'center',
                'index' => 'entity_id',
                'values' => $this->_getSelectedProducts(),
            ]
        );*/

        $this->addColumn(
            'entity_id',
            [
                'header' => __('Product ID'),
                'type' => 'number',
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'sku',
            [
                'header' => __('Sku'),
                'index' => 'sku',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'price',
            [
                'header' => __('Price'),
                'type' => 'currency',
                'index' => 'price',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'box_type',
            [
                'header' => __('Box Type'),
                'index' => 'box_type',
                'width' => '50px',
            ]
        );

        $this->addColumn(
            'box_height',
            [
                'header' => __('Box height'),
                'index' => 'box_height',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'box_weight',
            [
                'header' => __('Box weight'),
                'index' => 'box_weight',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'box_length',
            [
                'header' => __('Box Length'),
                'index' => 'box_length',
                'width' => '50px',

            ]
        );
        $this->addColumn(
            'box_width',
            [
                'header' => __('Box width'),
                'index' => 'box_width',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'color',
            [
                'header' => __('Color'),
                'index' => 'color',
                'width' => '50px',
            ]
        );

        $this->addColumn(
            'dimension_weight',
            [
                'header' => __('Dimensional weight'),
                'index' => 'dimension_weight',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'group',
            [
                'header' => __('Group'),
                'index' => 'group',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'head_size',
            [
                'header' => __('Head Size'),
                'index' => 'head_size',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'product_um',
            [
                'header' => __('Product um'),
                'index' => 'product_um',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'qty_per_box',
            [
                'header' => __('Qty Per Box'),
                'index' => 'qty_per_box',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'quantity_and_stock_status',
            [
                'header' => __('Quantity'),
                'index' => 'quantity_and_stock_status',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'rewardpoints_earn',
            [
                'header' => __('Rewardpoints Earn'),
                'index' => 'rewardpoints_earn',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'rewardpoints_spend',
            [
                'header' => __('Rewardpoints Spend'),
                'index' => 'rewardpoints_spend',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'searchindex_weight',
            [
                'header' => __('Searchindex Weight'),
                'index' => 'searchindex_weight',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'fi_id',
            [
                'header' => __('Unique Identifier ID'),
                'index' => 'fi_id',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'shipment_type',
            [
                'header' => __('Shipment Type'),
                'index' => 'shipment_type',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'sku_list',
            [
                'header' => __('Sku List'),
                'index' => 'sku_list',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'sustainably_certified',
            [
                'header' => __('Certified'),
                'index' => 'sustainably_certified',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'ts_dimensions_height',
            [
                'header' => __('Height'),
                'index' => 'ts_dimensions_height',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'ts_dimensions_width',
            [
                'header' => __('Width'),
                'index' => 'ts_dimensions_width',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'ts_dimensions_length',
            [
                'header' => __('Length'),
                'index' => 'ts_dimensions_length',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'udropship_calculate_rates',
            [
                'header' => __('Dropship Rates Calculation Type'),
                'index' => 'udropship_calculate_rates',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'variants',
            [
                'header' => __('All Variants'),
                'index' => 'variants',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'variety',
            [
                'header' => __('Variety'),
                'index' => 'variety',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'vendor_product_name',
            [
                'header' => __('Vendor Product Name'),
                'index' => 'vendor_product_name',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'vendor_sku',
            [
                'header' => __('Vendor SKU'),
                'index' => 'vendor_sku',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'weight',
            [
                'header' => __('Weight'),
                'index' => 'weight',
                'width' => '50px',
            ]
        );
        $this->addColumn('action', array(
            'header' => __('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getProductId',
            'actions' => array(
                array(
                    'caption' => __('Edit'),
                    'url' => "http://local.flordel/admin/vendor/vendor/edit/vendor_id/1/key/3b1de2576fb21e01f45be2924cded84f3bd07312f7a9cf27547c3d9125529cc6/",
                    'target' => '_blank',
                    'field' => 'id'
                )
            ),
            //'renderer'  => 'Flordelcampo\VendorRegistration\Block\Adminhtml\Abcreport\Edit\Tab\Renderer\Abcreportviewaction',
            'filter' => false,
            'sortable' => false,
            'index' => 'id',
            'is_system' => true,
        ));
        /*
        $this->addColumn(
            'weight_type',
            [
                'header' => __('Dynamic Weight'),
                'index' => 'weight_type',
                'width' => '50px',
            ]
        );
        */

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/productsgrid', ['_current' => true]);
    }

    /**
     * @param object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }

    protected function _getSelectedProducts()
    {
        $contact = $this->getContact();
        return $contact->getProducts($contact);
    }

    /**
     * Retrieve selected products
     *
     * @return array
     */
    public function getSelectedProducts()
    {
        $contact = $this->getContact();
        $selected = $contact->getProducts($contact);

        if (!is_array($selected)) {
            $selected = [];
        }
        return $selected;
    }

    protected function getContact()
    {
        $contactId = $this->getRequest()->getParam('vendor_id');
        $contact = $this->vendorFactory->create();
        if ($contactId) {
            $contact->load($contactId);
        }
        return $contact;
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return true;
    }
}
