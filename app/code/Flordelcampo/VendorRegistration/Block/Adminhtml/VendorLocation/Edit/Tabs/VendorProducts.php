<?php

namespace Flordelcampo\VendorRegistration\Block\Adminhtml\VendorLocation\Edit\Tabs;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use Magento\Framework\App\Request\Http as Request;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\VendorRegistration\Helper\Data as VendorRegistrationHelper;

/**
 * Registration edit form main tab
 */
class VendorProducts extends Generic implements TabInterface
{

    /**
     * @var Store
     */
    protected $_systemStore;

    protected $_template = 'products/vendor_products_grid.phtml';
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var VendorRegistrationHelper
     */
    protected $vendorRegistrationHelper;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Store $systemStore
     * @param Request $request
     * @param VendorHelper $vendorHelper
     * @param VendorRegistrationHelper $vendorRegistrationHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        Request $request,
        VendorHelper $vendorHelper,
        VendorRegistrationHelper $vendorRegistrationHelper,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        $this->request = $request;
        $this->vendorHelper = $vendorHelper;
        $this->vendorRegistrationHelper = $vendorRegistrationHelper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Return Vendor ID
     *
     * @return mixed|null
     */
    public function getVendorId()
    {
        return $this->request->getParam('vendor_id');
    }

    /**
     * Return HTML
     * @return string
     */
    public function getVendorOffersTableHtml()
    {
        return $this->vendorRegistrationHelper->getOffersProductsForVendor($this->getVendorId());
    }

    /**
     * @return string
     */
    public function getOfferUpdateUrl()
    {
        return $this->getUrl('vendor/offers/update');
    }

    /**
     * Prepare label for tab
     *
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Offer Products');
    }

    /**
     * Prepare title for tab
     *
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Offer Products');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Form Key
     * @return string
     */
    public function getFormKey()
    {
        return parent::getFormKey();
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

}
