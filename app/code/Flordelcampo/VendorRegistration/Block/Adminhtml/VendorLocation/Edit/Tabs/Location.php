<?php

namespace Flordelcampo\VendorRegistration\Block\Adminhtml\VendorLocation\Edit\Tabs;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use Magento\Framework\App\Request\Http as Request;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;

/**
 * Registration edit form main tab
 */
class Location extends Generic implements TabInterface
{

    /**
     * @var Store
     */
    protected $_systemStore;

    protected $_template = 'vendorLocation/vendor_loc.phtml';
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Store $systemStore
     * @param Request $request
     * @param VendorHelper $vendorHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        Request $request,
        VendorHelper $vendorHelper,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        $this->request = $request;
        $this->vendorHelper = $vendorHelper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('ws_vendor');
        $isElementDisabled = false;
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Vendor Location')]);
        if ($model->getId()) {
            $fieldset->addField('vendor_id', 'hidden', ['name' => 'vendor_id']);
        }
        /*if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }*/
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Return Vendor ID
     * @return mixed|null
     */
    public function getVendorId()
    {
        return $this->request->getParam('vendor_id');
    }

    /**
     * Return Collection
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorLocationCollectionByVendorId()
    {
        return $this->vendorHelper->getVendorLocationsCollectionByVendorId($this->getVendorId());
    }

    /**
     * @return string
     */
    public function getAddNewLocationFormUrl()
    {
        return $this->getUrl('vendor/location/newform');
    }

    /**
     * @return string
     */
    public function getSaveLocationFormUrl()
    {
        return $this->getUrl('vendor/location/save');
    }
    /**
     * @return string
     */
    public function getRetrieveLocationFormUrl()
    {
        return $this->getUrl('vendor/location/retrieve');
    }

    /**
     * @return string
     */
    public function getUpdateLocationFormUrl()
    {
        return $this->getUrl('vendor/location/update');
    }
    /**
     * @return string
     */
    public function getUpdateUserUrl()
    {
        return $this->getUrl('vendor/user/update');
    }
    /**
     * @return string
     */
    public function getSaveUserUrl()
    {
        return $this->getUrl('vendor/user/save');
    }

    /**
     * @return string
     */
    public function getUpdateLogisticUrl()
    {
        return $this->getUrl('vendor/logistics/update');
    }
    /**
     * @return string
     */
    public function getSaveLogisticsUrl()
    {
        return $this->getUrl('vendor/logistics/save');
    }

    public function getAllRegions()
    {
        return $this->vendorHelper->getAllRegionsByCountryId('USA');
    }

    public function getAllCountries()
    {
        return $this->vendorHelper->getAllCountryArray();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Vendor Location');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Vendor Location');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray()
    {
        return [
            '_self' => "Self",
            '_blank' => "New Page",
        ];
    }
}
