<?php

namespace Flordelcampo\VendorRegistration\Controller\Adminhtml\Offers;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class Update extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    private $_vendorId = null;

    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request,
        OrderHelper $orderHelper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        $this->orderHelper = $orderHelper;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $params = $this->getRequest()->getPostvalue();
            $offerId = $params['offer_id'];
            $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);

            $notInArray = ['offer_id', 'sku', 'form_key'];
            foreach ($params as $field => $value) {
                if (!in_array($field, $notInArray)) {
                    $offer->setData($field, $value);
                }
            }
            $offer->save();
            $data = [
                'status' => true,
                'message' => __('Offer Updated Successfully')
            ];
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }
}