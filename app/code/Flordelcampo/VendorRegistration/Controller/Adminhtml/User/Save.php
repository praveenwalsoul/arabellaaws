<?php

namespace Flordelcampo\VendorRegistration\Controller\Adminhtml\User;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $params = $this->getRequest()->getPostvalue();
            /**
             * Check if user name is already taken
             */
            if (isset($params['user_name'])) {
                $this->validateUserName($params['user_name']);
            }
            $vendorUserModel = $this->vendorHelper->getVendorUserModel();
            foreach ($params as $field => $value) {
                if ($field != 'form_key') {
                    $vendorUserModel->setData($field, $value);
                }
            }
            $vendorUserModel->save();
            $data = [
                'status' => true,
                'message' => __('User Saved Successfully')
            ];
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * Validate User Name
     * @param $userName
     * @throws LocalizedException
     */
    public function validateUserName($userName)
    {
        $userCollection = $this->vendorHelper->getVendorUserModel()->getCollection()
            ->addFieldToFilter('user_name', $userName);
        if ($userCollection->getSize() > 0) {
            throw new LocalizedException(__("This username is not available"));
        }
    }
}
