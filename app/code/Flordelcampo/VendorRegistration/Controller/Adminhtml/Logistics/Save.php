<?php

namespace Flordelcampo\VendorRegistration\Controller\Adminhtml\Logistics;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $params = $this->getRequest()->getPostvalue();
            $vendorLogisticModel = $this->vendorHelper->getVendorLogisticModel();
            $daysArray = $this->vendorHelper->getDaysArray();
            $InArray = [
                'destination_country',
                'shipping_method',
                'cost_channel',
                'store_code',
                'lead_time',
                'buffer_days',
                'buffer_days_adjust',
                'customer_truck_required',
                'zip_codes',
                'status',
                'vendor_id',
                'location_id',
                'priority'
            ];
            foreach ($params as $field => $value) {
                if (in_array($field, $InArray)) {
                    $vendorLogisticModel->setData($field, $value);
                }
            }

            /**
             * Get All values and Make array as "10:00-12:00,0" to update to DB
             * for days column
             */
            $ar = [];
            foreach ($daysArray as $day => $value) {
                $fromText = $day . '-from-time';
                $toText = $day . '-to-time';
                $checkBoxText = $day . '-check-box-status';
                $dayTimeCheckStatus = $params[$fromText] . '-' . $params[$toText] . ',' . $params[$checkBoxText];
                $vendorLogisticModel->setData($day, $dayTimeCheckStatus);
                $ar[$day] = $dayTimeCheckStatus;
            }
            $vendorLogisticModel->save();
            $data = [
                'status' => true,
                'message' => __('Logistic Saved Successfully')
            ];
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }
}
