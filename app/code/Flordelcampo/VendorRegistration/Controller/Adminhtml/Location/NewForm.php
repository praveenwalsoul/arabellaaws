<?php

namespace Flordelcampo\VendorRegistration\Controller\Adminhtml\Location;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class NewForm extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    private $_vendorId = null;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $vendorId = $this->getRequest()->getParam('vendor_id');
            $this->_vendorId = $vendorId;

            $newLogisticFormHtml = $this->getNewLogisticFormHtml();
            $data = [
                'status' => true,
                'new_logistic_form_html' => $newLogisticFormHtml
            ];
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    public function getNewLogisticFormHtml()
    {
        $vendorId = $this->_vendorId;
        $countryArray = $this->vendorHelper->getAllCountryArray();
        $regionsArray = $this->vendorHelper->getAllRegionsByCountryId('USA');
        /**
         * Create HTML for each Input Field
         */
        $locationNameHtml = "<input type='text' value='' required='required' name='loc_name' title='Location Full Name' />";
        $locationDisplayNameHtml = "<input type='text' required='required' value='' name='loc_cust_disp_name' title='Location Company Name' />";
        $locationAddress1Html = "<input type='text' value='' required='required' name='loc_address' title='Address 1' />";
        $locationAddress2Html = "<input type='text' value='' required='required' name='loc_street' title='Address 2' />";
        $cityHtml = "<input type='text' value='' required='required' name='loc_city' title='City' />";
        $zipHtml = "<input type='text' value='' required='required' name='loc_postcode' title='Postcode'/>";
        $telephoneHtml = "<input type='text' value='' name='loc_phone' title='Phone' />";
        $emailHtml = "<input type='email' value='' name='loc_email' title='Email' '/>";
        $faxHtml = "<input type='text' value='' name='loc_fax' title='Fax' />";

        $fedexShipAccountNumberHtml = "<input type='text' value='' name='fedex_ship_acc_number' title='Fedex Ship account number' />";
        $fedexBillAccountNumberHtml = "<input type='text' value='' name='fedex_bill_acc_number' title='Fedex bill account number' />";
        /**
         * Country List
         */
        $countryHtml = '';
        $countryHtml .= "<select type='select' name='loc_country'>";
        foreach ($countryArray as $countryId => $countryName) {
            if ($countryId == 'US') {
                $countryHtml .= "<option value='$countryId' selected='selected'>$countryName</option>";
            } else {
                $countryHtml .= "<option value='$countryId'>$countryName</option>";
            }
        }
        $countryHtml .= "</select>";

        /**
         * Region List
         */
        $regionHtml = '';
        $regionHtml .= "<select type='select' required='required' name='loc_state'>";
        foreach ($regionsArray as $regionId => $regionName) {
            $regionHtml .= "<option value='$regionId'>$regionName</option>";
        }
        $regionHtml .= "</select>";

        /**
         * Active Or not
         */
        $statusHtml = '';
        $yesNoArray = $this->getYesNoArray();
        $statusHtml .= "<select type='select' name='status'>";
        foreach ($yesNoArray as $id => $value) {
            if ($id == '1') {
                $statusHtml .= "<option value='$id' selected='selected'>$value</option>";
            } else {
                $statusHtml .= "<option value='$id'>$value</option>";
            }
        }

        $hiddenFields = "<input type='hidden' name='vendor_id' value='" . $vendorId . "' />";

        $statusHtml .= "</select>";
        $saveHtml = "<button type='button' class='save primary save-new-location-button'>Save Location</button>";
        $html = '';
        $html .= "<table class='admin__table-secondary vendor-logistic-new-form-table' style='border: 1px solid;'><tbody>";
        $html .= "<tr><td>* Location Full Name</td><td>" . $locationNameHtml . "</td></tr>";
        $html .= "<tr><td>* Location Company Name</td><td>" . $locationDisplayNameHtml . "</td></tr>";
        $html .= "<tr><td>* Address 1</td><td>" . $locationAddress1Html . "</td></tr>";
        $html .= "<tr><td>* Address 2</td><td>" . $locationAddress2Html . "</td></tr>";
        $html .= "<tr><td>* City</td><td>" . $cityHtml . "</td></tr>";
        $html .= "<tr><td>* Country</td><td>" . $countryHtml . "</td></tr>";
        $html .= "<tr><td>* State</td><td>" . $regionHtml . "</td></tr>";
        $html .= "<tr><td>* Postcode</td><td>" . $zipHtml . "</td></tr>";
        $html .= "<tr><td>Phone</td><td>" . $telephoneHtml . "</td></tr>";
        $html .= "<tr><td>Email</td><td>" . $emailHtml . "</td></tr>";
        $html .= "<tr><td>Fedex Ship Account Number</td><td>" . $fedexShipAccountNumberHtml . "</td></tr>";
        $html .= "<tr><td>Fedex Bill Account Number</td><td>" . $fedexBillAccountNumberHtml . "</td></tr>";
        $html .= "<tr><td>Fax</td><td>" . $faxHtml . "</td></tr>";
        $html .= "<tr><td>Is Active</td><td>" . $statusHtml . "</td></tr>";
        $html .= "<tr><td></td><td>" . $saveHtml . "</td></tr>";
        $html .= "</tbody></table>";
        $html .= $hiddenFields;
        return $html;
    }

    /**
     * Return Yes or No array
     * @return array
     */
    public function getYesNoArray()
    {
        return [
            '0' => 'No',
            '1' => 'Yes'
        ];
    }
}
