<?php

namespace Flordelcampo\VendorRegistration\Controller\Adminhtml\Location;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class Retrieve extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    private $_vendorId = null;

    private $_locationID = null;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request,
        OrderHelper $orderHelper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        $this->orderHelper = $orderHelper;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $vendorId = $this->getRequest()->getParam('vendor_id');
            $locationId = $this->getRequest()->getParam('location_id');
            $this->_vendorId = $vendorId;
            $this->_locationID = $locationId;

            $locationSettingHtml = $this->getLocationSettingHtml();
            $userSettingHtml = $this->getUserSettingHtml();
            $logisticSettingHtml = $this->getLogisticSettingHtml();
            $data = [
                'status' => true,
                'location_setting_html' => $locationSettingHtml,
                'user_setting_html' => $userSettingHtml,
                'logistic_setting_html' => $logisticSettingHtml
            ];
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * Return Location Setting HTML
     * @return string
     */
    public function getLocationSettingHtml()
    {
        $vendorId = $this->_vendorId;
        $locationId = $this->_locationID;
        $vendorLocationModel = $this->vendorHelper->getVendorLocationModelByLocationId($locationId);
        $locationId = $vendorLocationModel->getId();
        $locationName = $vendorLocationModel->getData('loc_name');
        $locationDisplayName = $vendorLocationModel->getData('loc_cust_disp_name');
        $address1 = $vendorLocationModel->getData('loc_address');
        $address2 = $vendorLocationModel->getData('loc_street');
        $city = $vendorLocationModel->getData('loc_city');
        $stateId = $vendorLocationModel->getData('loc_state');
        $postCode = $vendorLocationModel->getData('loc_postcode');
        $phone = $vendorLocationModel->getData('loc_phone');
        $fax = $vendorLocationModel->getData('loc_fax');
        $email = $vendorLocationModel->getData('loc_email');
        $countryCode = $vendorLocationModel->getData('loc_country');
        $isActive = $vendorLocationModel->getData('status');

        $fedexShipAccountNumber = $vendorLocationModel->getData('fedex_ship_acc_number');
        $fedexBillAccountNumber = $vendorLocationModel->getData('fedex_bill_acc_number');

        $countryArray = $this->vendorHelper->getAllCountryArray();
        $regionsArray = $this->vendorHelper->getAllRegionsByCountryId('USA');
        $yesNoArray = $this->getYesNoArray();
        /**
         * Create HTML for each Input Field
         */
        $locationIdHtml = "<input type='text' value='$locationId' disabled='disabled' title='Location Id' />";
        $locationNameHtml = "<input type='text' value='$locationName' required='required' name='loc_name' title='Location Full Name' />";
        $locationDisplayNameHtml = "<input type='text' required='required' value='$locationDisplayName' name='loc_cust_disp_name' title='Location Company Name' />";
        $locationAddress1Html = "<input type='text' value='$address1' required='required' name='loc_address' title='Address 1' />";
        $locationAddress2Html = "<input type='text' value='$address2' required='required' name='loc_street' title='Address 2' />";
        $cityHtml = "<input type='text' value='$city' required='required' name='loc_city' title='City' />";
        $zipHtml = "<input type='text' value='$postCode' required='required' name='loc_postcode' title='Postcode'/>";
        $telephoneHtml = "<input type='text' value='$phone' name='loc_phone' title='Phone' />";
        $emailHtml = "<input type='email' value='$email' name='loc_email' title='Email' '/>";
        $faxHtml = "<input type='text' value='$fax' name='loc_fax' title='Fax' />";

        $fedexShipAccountNumberHtml = "<input type='text' value='$fedexShipAccountNumber' name='fedex_ship_acc_number' title='Fedex Ship account number' />";
        $fedexBillAccountNumberHtml = "<input type='text' value='$fedexBillAccountNumber' name='fedex_bill_acc_number' title='Fedex bill account number' />";
        /**
         * Country List
         */
        $countryHtml = '';
        $countryHtml .= "<select type='select' name='loc_country'>";
        foreach ($countryArray as $countryId => $countryName) {
            if ($countryId == $countryCode) {
                $countryHtml .= "<option value='$countryId' selected='selected'>$countryName</option>";
            } else {
                $countryHtml .= "<option value='$countryId'>$countryName</option>";
            }
        }
        $countryHtml .= "</select>";

        /**
         * Region List
         */
        $regionHtml = '';
        $regionHtml .= "<select type='select' required='required' name='loc_state'>";
        foreach ($regionsArray as $regionId => $regionName) {
            if ($regionId == $stateId) {
                $regionHtml .= "<option value='$regionId'  selected='selected'>$regionName</option>";
            } else {
                $regionHtml .= "<option value='$regionId'>$regionName</option>";
            }

        }
        $regionHtml .= "</select>";

        /**
         * Active Or not
         */
        $statusHtml = '';
        $statusHtml .= "<select type='select' name='status'>";
        foreach ($yesNoArray as $id => $value) {
            if ($id == $isActive) {
                $statusHtml .= "<option value='$id' selected='selected'>$value</option>";
            } else {
                $statusHtml .= "<option value='$id'>$value</option>";
            }
        }
        $statusHtml .= "</select>";

        $hiddenFields = "<input type='hidden' name='vendor_id' value='" . $vendorId . "' />";
        $hiddenFields .= "<input type='hidden' name='location_id' value='" . $locationId . "' />";

        $updateHtml = "<button type='button' class='save primary update-location-button'>Update Location</button>";
        $html = '';
        $html .= "<table class='admin__table-secondary vendor-logistic-settings-table' style='border: 1px solid;'><tbody>";
        $html .= "<tr><td>Location ID</td><td>" . $locationIdHtml . "</td></tr>";
        $html .= "<tr><td>* Location Full Name</td><td>" . $locationNameHtml . "</td></tr>";
        $html .= "<tr><td>* Location Company Name</td><td>" . $locationDisplayNameHtml . "</td></tr>";
        $html .= "<tr><td>* Address 1</td><td>" . $locationAddress1Html . "</td></tr>";
        $html .= "<tr><td>* Address 2</td><td>" . $locationAddress2Html . "</td></tr>";
        $html .= "<tr><td>* City</td><td>" . $cityHtml . "</td></tr>";
        $html .= "<tr><td>* Country</td><td>" . $countryHtml . "</td></tr>";
        $html .= "<tr><td>* State</td><td>" . $regionHtml . "</td></tr>";
        $html .= "<tr><td>* Postcode</td><td>" . $zipHtml . "</td></tr>";
        $html .= "<tr><td>Phone</td><td>" . $telephoneHtml . "</td></tr>";
        $html .= "<tr><td>Email</td><td>" . $emailHtml . "</td></tr>";
        $html .= "<tr><td>Fedex Ship Account Number</td><td>" . $fedexShipAccountNumberHtml . "</td></tr>";
        $html .= "<tr><td>Fedex Bill Account Number</td><td>" . $fedexBillAccountNumberHtml . "</td></tr>";
        $html .= "<tr><td>Fax</td><td>" . $faxHtml . "</td></tr>";
        $html .= "<tr><td>Is Active</td><td>" . $statusHtml . "</td></tr>";
        $html .= "<tr><td></td><td>" . $updateHtml . "</td></tr>";
        $html .= "</tbody></table>";
        $html .= $hiddenFields;
        return $html;
    }

    /**
     * Return Logistics Stting Html
     * @return string
     */
    public function getLogisticSettingHtml()
    {
        $vendorId = $this->_vendorId;
        $locationId = $this->_locationID;

        $storeListArray = $this->orderHelper->getStoreLabelCodeArray();
        $countryArray = $this->vendorHelper->getAllCountryArray();
        $shippingMethodsArray = $this->vendorHelper->getAllShippingMethodsArray();
        $costChannelArray = $this->vendorHelper->getAllCostChannelArray();
        $yesNoArray = $this->getYesNoArray();
        $timeArray = $this->halfHourTimes();
        $daysArray = $this->getDaysArray();
        $vendorLogisticsCollection = $this->vendorHelper->getVendorLogisticModelByLocationId($locationId);

        $html = "";
        $html .= "<table class='admin__table-secondary vendor-logistic-table-class' id='vendor-logistic-setting-table'>";
        $html .= "<thead><tr><th>Destination Country</th>";
        $html .= "<th>Shipping Method</th>";
        $html .= "<th>Cost Channel</th>";
        $html .= "<th>Store</th>";
        $html .= "<th>Lead Time</th>";
        $html .= "<th>Buffer Days</th>";
        $html .= "<th>Buffer Days </br>Adjust</th>";
        $html .= "<th>Customer </br>Truck </br>Required</th>";
        $html .= "<th>Zip Codes</th>";
        $html .= "<th>Sunday</th>";
        $html .= "<th>Monday</th>";
        $html .= "<th>Tuesday</th>";
        $html .= "<th>Wednesday</th>";
        $html .= "<th>Thursday</th>";
        $html .= "<th>Friday</th>";
        $html .= "<th>Saturday</th>";
        $html .= "<th>Status</th>";
        $html .= "<th>Priority</th>";
        $html .= "<th>Action</th>";
        $html .= "</tr></thead><tbody>";
        foreach ($vendorLogisticsCollection as $oneLogistic) {

            $logisticEntityId = $oneLogistic->getData('logistic_entity_id');
            $vendorId = $oneLogistic->getData('vendor_id');
            $locationId = $oneLogistic->getData('location_id');
            $countryCode = $oneLogistic->getData('destination_country');
            $shippingCode = $oneLogistic->getData('shipping_method');
            $costChannelCode = $oneLogistic->getData('cost_channel');
            $leadTime = $oneLogistic->getData('lead_time');
            $bufferDays = $oneLogistic->getData('buffer_days');
            $bufferDaysAdjust = $oneLogistic->getData('buffer_days_adjust');
            $customerTruckRequired = $oneLogistic->getData('customer_truck_required');
            $storeCodeArray = explode(',', $oneLogistic->getData('store_code'));
            $zipCodes = $oneLogistic->getData('zip_codes');
            $isActive = $oneLogistic->getData('status');
            $priority = $oneLogistic->getData('priority');

            $class = "update-vendor-logistic-" . $logisticEntityId;
            $leadTimeHtml = "<input type='text' value='$leadTime' name='lead_time' class='$class' title='Lead Time'/>";
            $bufferDaysHtml = "<input type='text' value='$bufferDays' name='buffer_days' class='$class' title='Buffer Days'/>";
            $bufferDaysAdjustHtml = "<input type='text' value='$bufferDaysAdjust' name='buffer_days_adjust' class='$class' title='Buffer Days Adjustment'/>";
            $zipCodesHtml = "<input type='text' value='$zipCodes' name='zip_codes' class='$class' title='Zip Codes'/>";
            $priorityHtml = "<input type='text' value='$priority' name='priority' class='$class' title='Priority'/>";

            /**
             * Destination Country
             */
            $destinationCountryHtml = '';
            $destinationCountryHtml .= "<select type='select' required='required' class='$class' name='destination_country' title='Destination Country'>";
            foreach ($countryArray as $code => $value) {
                if ($code == $countryCode) {
                    $destinationCountryHtml .= "<option value='$code'  selected='selected'>$value</option>";
                } else {
                    $destinationCountryHtml .= "<option value='$code'>$value</option>";
                }
            }
            $destinationCountryHtml .= "</select>";

            /**
             * Shipping Method
             */
            $shippingMethodHtml = '';
            $shippingMethodHtml .= "<select type='select' required='required' class='$class' name='shipping_method' title='Shipping Method'>";
            foreach ($shippingMethodsArray as $code => $value) {
                if ($code == $shippingCode) {
                    $shippingMethodHtml .= "<option value='$code'  selected='selected'>$value</option>";
                } else {
                    $shippingMethodHtml .= "<option value='$code'>$value</option>";
                }
            }
            $shippingMethodHtml .= "</select>";

            /**
             * Cost Channel
             */
            $costChannelHtml = '';
            $costChannelHtml .= "<select type='select' required='required' class='$class' name='cost_channel' title='Cost Channel'>";
            foreach ($costChannelArray as $code => $value) {
                if ($code == $costChannelCode) {
                    $costChannelHtml .= "<option value='$code'  selected='selected'>$value</option>";
                } else {
                    $costChannelHtml .= "<option value='$code'>$value</option>";
                }
            }
            $costChannelHtml .= "</select>";

            /**
             * Store Code
             */
            $storeCodeHtml = '';
            $storeCodeHtml .= "<select type='select' required='required' class='$class' name='store_code' multiple title='Store'>";
            foreach ($storeListArray as $code => $value) {
                if (in_array($code, $storeCodeArray) == true) {
                    $storeCodeHtml .= "<option value='$code'  selected='selected'>$value</option>";
                } else {
                    $storeCodeHtml .= "<option value='$code'>$value</option>";
                }
            }
            $storeCodeHtml .= "</select>";

            /**
             * CTR
             */
            $ctrHtml = '';
            $ctrHtml .= "<select type='select' class='$class' name='customer_truck_required'>";
            foreach ($yesNoArray as $id => $value) {
                if ($id == $customerTruckRequired) {
                    $ctrHtml .= "<option value='$id' selected='selected'>$value</option>";
                } else {
                    $ctrHtml .= "<option value='$id'>$value</option>";
                }
            }
            $ctrHtml .= "</select>";

            /**
             * Active Or not
             */
            $statusHtml = '';
            $statusHtml .= "<select type='select' class='$class' name='status'>";
            foreach ($yesNoArray as $id => $value) {
                if ($id == $isActive) {
                    $statusHtml .= "<option value='$id' selected='selected'>$value</option>";
                } else {
                    $statusHtml .= "<option value='$id'>$value</option>";
                }
            }
            $statusHtml .= "</select>";

            /**
             * Days Data
             */
            $daysHtml = '';
            foreach ($daysArray as $day => $dayTitle) {
                $selectedDayArray = explode(",", $oneLogistic->getData($day));
                $fromToArray = explode('-', $selectedDayArray[0]);
                $selectedFromTime = $fromToArray[0];
                $selectedToTime = $fromToArray[1];
                $selectedCheckBoxStatus = (isset($selectedDayArray[1])) ? $selectedDayArray[1] : '0';

                $daysHtml .= '<td>';
                /**
                 * From time html
                 */
                $fromHtml = '';
                $fromHtml .= "<select type='select' class='$class' name= '$day-from-time'>";
                foreach ($timeArray as $id => $value) {
                    if ($value == $selectedFromTime) {
                        $fromHtml .= "<option value='$value' selected='selected'>$value</option>";
                    } else {
                        $fromHtml .= "<option value='$value'>$value</option>";
                    }
                }
                $fromHtml .= "</select>";

                /**
                 * To time html
                 */
                $toHtml = '';
                $toHtml .= "<select type='select' class='$class' name= '$day-to-time'>";
                foreach ($timeArray as $id => $value) {
                    if ($value == $selectedToTime) {
                        $toHtml .= "<option value='$value' selected='selected'>$value</option>";
                    } else {
                        $toHtml .= "<option value='$value'>$value</option>";
                    }
                }
                $toHtml .= "</select>";

                $checkBoxHtml = '';

                $uniqueCheckBoxClass = $class . '-' . $day . '-check-box-status';
                if ($selectedCheckBoxStatus == '1') {
                    $checkBoxHtml .= "<input type='checkbox' name='$day-check-box-status' class='$class $uniqueCheckBoxClass' checked='checked' />";
                } else {
                    $checkBoxHtml .= "<input type='checkbox' name='$day-check-box-status' class='$class $uniqueCheckBoxClass'/>";
                }
                $daysHtml .= $fromHtml;
                $daysHtml .= $toHtml;
                $daysHtml .= $checkBoxHtml;
                $daysHtml .= '</td>';
            }

            $hiddenFields = "<input type='hidden' class='$class' name='vendor_id' value='" . $vendorId . "' />";
            $hiddenFields .= "<input type='hidden' class='$class' name='location_id' value='" . $locationId . "' />";
            $hiddenFields .= "<input type='hidden' class='$class' name='logistic_entity_id' value='$logisticEntityId'/>";
            $updateHtml = "<button type='button' data-logistic-entity-id='$logisticEntityId' class='save primary update-vendor-logistic-button'>Update</button>";

            $html .= "<tr>";
            $html .= "<td>" . $destinationCountryHtml . "</td>";
            $html .= "<td>" . $shippingMethodHtml . "</td>";
            $html .= "<td>" . $costChannelHtml . "</td>";
            $html .= "<td>" . $storeCodeHtml . "</td>";
            $html .= "<td>" . $leadTimeHtml . "</td>";
            $html .= "<td>" . $bufferDaysHtml . "</td>";
            $html .= "<td>" . $bufferDaysAdjustHtml . "</td>";
            $html .= "<td>" . $ctrHtml . "</td>";
            $html .= "<td>" . $zipCodesHtml . "</td>";
            $html .= $daysHtml;
            $html .= "<td>" . $statusHtml . "</td>";
            $html .= "<td>" . $priorityHtml . "</td>";
            $html .= "<td>" . $updateHtml . "</td>";
            $html .= "</tr>";
            $html .= $hiddenFields;
        }
        $addNewLogisticHtml = $this->getNewLogisticHtml();
        $html .= $addNewLogisticHtml;
        $html .= "</tbody></table>";
        return $html;
    }

    /**
     * Return New logistic HTML
     * @return string
     */
    public function getNewLogisticHtml()
    {
        $vendorId = $this->_vendorId;
        $locationId = $this->_locationID;

        $storeListArray = $this->orderHelper->getStoreLabelCodeArray();
        $countryArray = $this->vendorHelper->getAllCountryArray();
        $shippingMethodsArray = $this->vendorHelper->getAllShippingMethodsArray();
        $costChannelArray = $this->vendorHelper->getAllCostChannelArray();
        $yesNoArray = $this->getYesNoArray();
        $timeArray = $this->halfHourTimes();
        $daysArray = $this->getDaysArray();

        $class = "save-new-vendor-logistic";
        $leadTimeHtml = "<input type='text' value='' name='lead_time' class='$class' title='Lead Time'/>";
        $bufferDaysHtml = "<input type='text' value='' name='buffer_days' class='$class' title='Buffer Days'/>";
        $bufferDaysAdjustHtml = "<input type='text' value='' name='buffer_days_adjust' class='$class' title='Buffer Days Adjustment'/>";
        $zipCodesHtml = "<input type='text' value='' name='zip_codes' class='$class' title='Zip Codes'/>";
        $priorityHtml = "<input type='text' value='' name='priority' class='$class' title='Priority'/>";

        /**
         * Destination Country
         */
        $destinationCountryHtml = '';
        $destinationCountryHtml .= "<select type='select' required='required' class='$class' name='destination_country' title='Destination Country'>";
        foreach ($countryArray as $code => $value) {
            if ($code == 'US') {
                $destinationCountryHtml .= "<option value='$code'  selected='selected'>$value</option>";
            } else {
                $destinationCountryHtml .= "<option value='$code'>$value</option>";
            }
        }
        $destinationCountryHtml .= "</select>";

        /**
         * Shipping Method
         */
        $shippingMethodHtml = '';
        $shippingMethodHtml .= "<select type='select' required='required' class='$class' name='shipping_method' title='Shipping Method'>";
        foreach ($shippingMethodsArray as $code => $value) {
            if ($code == '') {
                $shippingMethodHtml .= "<option value='$code'  selected='selected'>$value</option>";
            } else {
                $shippingMethodHtml .= "<option value='$code'>$value</option>";
            }
        }
        $shippingMethodHtml .= "</select>";

        /**
         * Cost Channel
         */
        $costChannelHtml = '';
        $costChannelHtml .= "<select type='select' required='required' class='$class' name='cost_channel' title='Cost Channel'>";
        foreach ($costChannelArray as $code => $value) {
            if ($code == '') {
                $costChannelHtml .= "<option value='$code'  selected='selected'>$value</option>";
            } else {
                $costChannelHtml .= "<option value='$code'>$value</option>";
            }
        }
        $costChannelHtml .= "</select>";

        /**
         * Store Code
         */
        $storeCodeHtml = '';
        $storeCodeHtml .= "<select type='select' required='required' class='$class' name='store_code' title='Store' multiple>";
        foreach ($storeListArray as $code => $value) {
            if ($code == '') {
                $storeCodeHtml .= "<option value='$code'  selected='selected'>$value</option>";
            } else {
                $storeCodeHtml .= "<option value='$code'>$value</option>";
            }
        }
        $storeCodeHtml .= "</select>";

        /**
         * CTR
         */
        $ctrHtml = '';
        $ctrHtml .= "<select type='select' class='$class' name='customer_truck_required'>";
        foreach ($yesNoArray as $id => $value) {
            if ($id == '0') {
                $ctrHtml .= "<option value='$id' selected='selected'>$value</option>";
            } else {
                $ctrHtml .= "<option value='$id'>$value</option>";
            }
        }
        $ctrHtml .= "</select>";

        /**
         * Active Or not
         */
        $statusHtml = '';
        $statusHtml .= "<select type='select' class='$class' name='status'>";
        foreach ($yesNoArray as $id => $value) {
            if ($id == '1') {
                $statusHtml .= "<option value='$id' selected='selected'>$value</option>";
            } else {
                $statusHtml .= "<option value='$id'>$value</option>";
            }
        }
        $statusHtml .= "</select>";
        /**
         * Days Data
         */
        $daysHtml = '';
        foreach ($daysArray as $day => $dayTitle) {
            $daysHtml .= '<td>';
            /**
             * From time html
             */
            $fromHtml = '';
            $fromHtml .= "<select type='select' class='$class' name= '$day-from-time'>";
            foreach ($timeArray as $id => $value) {
                if ($id == '0') {
                    $fromHtml .= "<option value='$value' selected='selected'>$value</option>";
                } else {
                    $fromHtml .= "<option value='$value'>$value</option>";
                }
            }
            $fromHtml .= "</select>";

            /**
             * To time html
             */
            $toHtml = '';
            $toHtml .= "<select type='select' class='$class' name= '$day-to-time'>";
            foreach ($timeArray as $id => $value) {
                if ($id == '0') {
                    $toHtml .= "<option value='$value' selected='selected'>$value</option>";
                } else {
                    $toHtml .= "<option value='$value'>$value</option>";
                }
            }
            $toHtml .= "</select>";

            $checkBoxHtml = '';

            $selectedCheckBoxStatus = '0';
            if ($selectedCheckBoxStatus == '1') {
                $checkBoxHtml .= "<input type='checkbox' name='$day-check-box-status' class='$class $day-check-box-status' checked='checked' />";
            } else {
                $checkBoxHtml .= "<input type='checkbox' name='$day-check-box-status' class='$class $day-check-box-status'/>";
            }
            $daysHtml .= $fromHtml;
            $daysHtml .= $toHtml;
            $daysHtml .= $checkBoxHtml;
            $daysHtml .= '</td>';
        }

        $hiddenFields = "<input type='hidden' class='$class' name='vendor_id' value='" . $vendorId . "' />";
        $hiddenFields .= "<input type='hidden' class='$class' name='location_id' value='" . $locationId . "' />";
        $addHtml = "<button type='button' class='save primary save-new-vendor-logistic-button'>Add</button>";

        $html = '';
        $html .= "<tr>";
        $html .= "<td>" . $destinationCountryHtml . "</td>";
        $html .= "<td>" . $shippingMethodHtml . "</td>";
        $html .= "<td>" . $costChannelHtml . "</td>";
        $html .= "<td>" . $storeCodeHtml . "</td>";
        $html .= "<td>" . $leadTimeHtml . "</td>";
        $html .= "<td>" . $bufferDaysHtml . "</td>";
        $html .= "<td>" . $bufferDaysAdjustHtml . "</td>";
        $html .= "<td>" . $ctrHtml . "</td>";
        $html .= "<td>" . $zipCodesHtml . "</td>";
        $html .= $daysHtml;
        $html .= "<td>" . $statusHtml . "</td>";
        $html .= "<td>" . $priorityHtml . "</td>";
        $html .= "<td>" . $addHtml . "</td>";
        $html .= "</tr>";
        $html .= $hiddenFields;
        $html .= "</tbody></table>";
        return $html;
    }

    /**
     * @return string
     */
    public function getUserSettingHtml()
    {
        $vendorId = $this->_vendorId;
        $locationId = $this->_locationID;

        $vendorUserTypeArray = $this->vendorHelper->getVendorUserTypesArray();
        $yesNoArray = $this->getYesNoArray();
        $vendorLocationModel = $this->vendorHelper->getVendorUserModelByLocationId($locationId);

        $html = "";
        $html .= "<table class='admin__table-secondary vendor-location-user-table-class' id='vendor-location-user-table'>";
        $html .= "<thead><tr><th>First Name</th>";
        $html .= "<th>Last Name</th>";
        $html .= "<th>User Name</th>";
        $html .= "<th>Password</th>";
        $html .= "<th>User Type</th>";
        $html .= "<th>Work Email</th>";
        $html .= "<th>Personal Email</th>";
        $html .= "<th>Phone</th>";
        $html .= "<th>Status</th>";
        $html .= "<th>Action</th>";
        $html .= "</tr></thead><tbody>";
        foreach ($vendorLocationModel as $oneUser) {
            $userEntityId = $oneUser->getData('user_entity_id');
            $vendorId = $oneUser->getData('vendor_id');
            $locationId = $oneUser->getData('location_id');
            $firstName = $oneUser->getData('first_name');
            $lastName = $oneUser->getData('last_name');
            $userName = $oneUser->getData('user_name');
            $password = $oneUser->getData('password');
            $userTypeCode = $oneUser->getData('user_type');
            $workEmail = $oneUser->getData('work_email');
            $personalEmail = $oneUser->getData('personal_email');
            $telephone = $oneUser->getData('telephone');
            $isActive = $oneUser->getData('status');

            $class = "update-vendor-user-" . $userEntityId;
            $firstNameHtml = "<input type='text' required='required' value='$firstName' name='first_name' class='$class' title='First Name'/>";
            $lastNameHtml = "<input type='text' value='$lastName' name='last_name' class='$class' title='Last Name'/>";
            $userNameHtml = "<input type='text' disabled value='$userName' required='required' class='$class' name='user_name'  title='User Name'/>";
            $passwordHtml = "<input type='password' disabled value='$password' required='required' class='$class' name='password'  title='Password'/>";
            $workEmailHtml = "<input type='text' value='$workEmail' name='work_email' class='$class' title='Work Email'/>";
            $personalEmailHtml = "<input type='text' value='$personalEmail' name='personal_email' class='$class' title='Personal Email'/>";
            $telephoneHtml = "<input type='text' value='$telephone' name='telephone'  class='$class' title='Telephone'/>";

            /**
             * User Type
             */
            $userTypeHtml = '';
            $userTypeHtml .= "<select type='select' class='$class' name='user_type'>";
            foreach ($vendorUserTypeArray as $code => $value) {
                if ($code == $userTypeCode) {
                    $userTypeHtml .= "<option value='$code'  selected='selected'>$value</option>";
                } else {
                    $userTypeHtml .= "<option value='$code'>$value</option>";
                }

            }
            $userTypeHtml .= "</select>";

            /**
             * Active Or not
             */
            $statusHtml = '';
            $statusHtml .= "<select type='select' class='$class' name='status'>";
            foreach ($yesNoArray as $id => $value) {
                if ($id == $isActive) {
                    $statusHtml .= "<option value='$id' selected='selected'>$value</option>";
                } else {
                    $statusHtml .= "<option value='$id'>$value</option>";
                }
            }
            $statusHtml .= "</select>";

            $hiddenFields = "<input type='hidden' class='$class' name='vendor_id' value='" . $vendorId . "' />";
            $hiddenFields .= "<input type='hidden' class='$class' name='location_id' value='" . $locationId . "' />";
            $hiddenFields .= "<input type='hidden' class='$class' name='user_entity_id' value='$userEntityId'/>";
            $updateHtml = "<button type='button' data-user-entity-id='$userEntityId' class='save primary update-vendor-user-button'>Update</button>";
            $html .= "<tr>";
            $html .= "<td>" . $firstNameHtml . "</td>";
            $html .= "<td>" . $lastNameHtml . "</td>";
            $html .= "<td>" . $userNameHtml . "</td>";
            $html .= "<td>" . $passwordHtml . "</td>";
            $html .= "<td>" . $userTypeHtml . "</td>";
            $html .= "<td>" . $workEmailHtml . "</td>";
            $html .= "<td>" . $personalEmailHtml . "</td>";
            $html .= "<td>" . $telephoneHtml . "</td>";
            $html .= "<td>" . $statusHtml . "</td>";
            $html .= "<td>" . $updateHtml . "</td>";
            $html .= "</tr>";
            $html .= $hiddenFields;
        }
        $addNewUserHtml = $this->getAddNewUserHtml();
        $html .= $addNewUserHtml;
        $html .= "</tbody></table>";
        return $html;
    }

    /**
     * Add New Html
     * @return string
     */
    public function getAddNewUserHtml()
    {
        $vendorId = $this->_vendorId;
        $locationId = $this->_locationID;
        $vendorUserTypeArray = $this->vendorHelper->getVendorUserTypesArray();
        $yesNoArray = $this->getYesNoArray();
        $class = "add-new-vendor-user";
        $firstNameHtml = "<input type='text' class='$class' required='required' value='' name='first_name' title='First Name'/>";
        $lastNameHtml = "<input type='text' class='$class' value='' name='last_name'  title='Last Name'/>";
        $userNameHtml = "<input type='text' class='$class' value='' required='required' name='user_name'  title='User Name'/>";
        $passwordHtml = "<input type='password' class='$class' value='' required='required' name='password'  title='Password'/>";
        $workEmailHtml = "<input type='text' class='$class' value='' name='work_email'  title='Work Email'/>";
        $personalEmailHtml = "<input type='text' class='$class' value='' name='personal_email'  title='Personal Email'/>";
        $telephoneHtml = "<input type='text' class='$class' value='' name='telephone'  title='Telephone'/>";

        /**
         * User Type
         */
        $userTypeHtml = '';
        $userTypeHtml .= "<select type='select' class='$class' name='user_type'>";
        foreach ($vendorUserTypeArray as $code => $value) {
            if ($code == '') {
                $userTypeHtml .= "<option value='$code'  selected='selected'>$value</option>";
            } else {
                $userTypeHtml .= "<option value='$code'>$value</option>";
            }
        }
        $userTypeHtml .= "</select>";

        /**
         * Active Or not
         */
        $statusHtml = '';
        $statusHtml .= "<select type='select' class='$class' name='status'>";
        foreach ($yesNoArray as $id => $value) {
            if ($id == '1') {
                $statusHtml .= "<option value='$id' selected='selected'>$value</option>";
            } else {
                $statusHtml .= "<option value='$id'>$value</option>";
            }
        }
        $statusHtml .= "</select>";

        $html = '';
        $hiddenFields = "<input type='hidden' class='$class' name='vendor_id' value='" . $vendorId . "' />";
        $hiddenFields .= "<input type='hidden' class='$class' name='location_id' value='" . $locationId . "' />";
        $saveHtml = "<button type='button' class='save primary save-vendor-user-button'>Add</button>";
        $html .= "<tr>";
        $html .= "<td>" . $firstNameHtml . "</td>";
        $html .= "<td>" . $lastNameHtml . "</td>";
        $html .= "<td>" . $userNameHtml . "</td>";
        $html .= "<td>" . $passwordHtml . "</td>";
        $html .= "<td>" . $userTypeHtml . "</td>";
        $html .= "<td>" . $workEmailHtml . "</td>";
        $html .= "<td>" . $personalEmailHtml . "</td>";
        $html .= "<td>" . $telephoneHtml . "</td>";
        $html .= "<td>" . $statusHtml . "</td>";
        $html .= "<td>" . $saveHtml . "</td>";
        $html .= "</tr>";
        $html .= $hiddenFields;
        return $html;
    }

    /**
     * Return Yes or No array
     * @return array
     */
    public function getYesNoArray()
    {
        return [
            '0' => 'No',
            '1' => 'Yes'
        ];
    }

    /**
     * Return Half Hour Time
     *
     * @return array
     */
    public function halfHourTimes()
    {
        $fromToTimeArray = [];
        $formatter = function ($time) {
            if ($time % 3600 == 0) {
                return date('H:i', $time);
            } else {
                return date('H:i', $time);
            }
        };
        $halfHourSteps = range(0, 47 * 1800, 1800);
        return array_map($formatter, $halfHourSteps);
    }

    /**
     * Return Days Array
     * @return array
     */
    public function getDaysArray()
    {
        return [
            'sunday' => 'Sunday',
            'monday' => 'Monday',
            'tuesday' => 'Tuesday',
            'wednesday' => 'Wednesday',
            'thursday' => 'Thursday',
            'friday' => 'Friday',
            'saturday' => 'Saturday'
        ];
    }
}
