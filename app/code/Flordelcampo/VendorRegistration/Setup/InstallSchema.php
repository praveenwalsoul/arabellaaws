<?php
namespace Flordelcampo\VendorRegistration\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('vendor_registrations')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('vendor_registrations'))
                ->addColumn(
                    'vendor_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
                )
                ->addColumn('store_id', Table::TYPE_TEXT, 5, ['nullable' => false])
                ->addColumn('website_id', Table::TYPE_TEXT, 5, ['nullable' => false])
                ->addColumn('vendor_name', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('last_name', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('telephone', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('email', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('password_enc', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('password_hash', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('carrier_code', Table::TYPE_TEXT, 64, ['nullable' => false])
                ->addColumn('vendor_attn', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('address', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('street', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('state', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('city', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('zipcode', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('region', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('region_id', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('remote_ip', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('url_key', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('gst_number', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('country_id', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('comments', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('notes', Table::TYPE_TEXT, 255, ['nullable' => false])
                
                ->addColumn('creation_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Creation Time')
                ->addColumn('update_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Update Time')
                ->addColumn('status',Table::TYPE_INTEGER,1,[],'Active')
                ->setComment('Sample table');

            $installer->getConnection()->createTable($table);
        }

        if (!$installer->tableExists('product_attachment_relation')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('product_attachment_relation'))
                ->addColumn('vendor_id', Table::TYPE_INTEGER, 10, ['nullable' => false, 'unsigned' => true])
                ->addColumn('product_id', Table::TYPE_INTEGER, 10, ['nullable' => false, 'unsigned' => true], 'Magento Product Id')
                ->addForeignKey(
                    $installer->getFkName(
                        'vendor_registrations',
                        'vendor_id',
                        'product_attachment_relation',
                        'vendor_id'
                    ),
                    'vendor_id',
                    $installer->getTable('vendor_registrations'),
                    'vendor_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'product_attachment_relation',
                        'vendor_id',
                        'catalog_product_entity',
                        'entity_id'
                    ),
                    'product_id',
                    $installer->getTable('catalog_product_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE
                )
                ->setComment('Flordelcampo Product Attachment relation table');

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
