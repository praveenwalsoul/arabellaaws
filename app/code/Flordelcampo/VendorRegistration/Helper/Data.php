<?php

namespace Flordelcampo\VendorRegistration\Helper;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var UrlInterface
     */
    protected $_backendUrl;

    /**
     * @var StoreManagerInterface $storeManager
     */
    protected $storeManager;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * @param Context $context
     * @param UrlInterface $backendUrl
     * @param StoreManagerInterface $storeManager
     * @param VendorHelper $vendorHelper
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Context $context,
        UrlInterface $backendUrl,
        StoreManagerInterface $storeManager,
        VendorHelper $vendorHelper,
        CatalogHelper $catalogHelper
    )
    {
        parent::__construct($context);
        $this->_backendUrl = $backendUrl;
        $this->storeManager = $storeManager;
        $this->vendorHelper = $vendorHelper;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * Return HTML
     *
     * @param $vendorId
     * @return string
     */
    public function getOffersProductsForVendor($vendorId)
    {
        $offerCollection = $this->vendorHelper->getVendorInventoryOfferCollectionByVendorId($vendorId);
        $yesOrNoArray = $this->vendorHelper->getYesNoArray();
        $tableHeaders = $this->getTableHeaders();
        $tableHeaderAttributes = array_values($tableHeaders);
        $editableAttributes = $this->getEditableAttributes();
        $notEditableAttributes = $this->getNotEditableAttributes();
        $dropDownAttributes = $this->dropdownAttributes();

        $dropdownAttributesOptions = [];
        foreach ($dropDownAttributes as $attribute) {
            if ($attribute == 'status') {
                $dropdownAttributesOptions[$attribute] = $yesOrNoArray;
            } else {
                $dropdownAttributesOptions[$attribute] = $this->catalogHelper->getAttributeOptionsByCode($attribute);
            }
        }
        /**
         * Table Headers HTML
         */
        $html = "<table class='admin__table-secondary vendor-offer-table'><tbody>";
        $html .= "<tr>";
        foreach ($tableHeaders as $colName => $code) {
            $html .= "<th>$colName</th>";
        }
        $html .= "</tr>";
        if ($offerCollection->getSize() > 0) {
            foreach ($offerCollection as $offer) {
                $offerId = $offer->getData('offer_id');
                $class = "update-vendor-offer-" . $offerId;
                $html .= "<tr>";
                foreach ($tableHeaderAttributes as $attribute) {
                    $currentAttributeValue = $offer->getData($attribute);
                    $currentHtml = '';
                    if (in_array($attribute, $notEditableAttributes)) {
                        $currentHtml = "<input type='text' value='$currentAttributeValue' name='$attribute' class='$class' title='$attribute' disabled/>";
                    }
                    if (in_array($attribute, $editableAttributes)) {
                        if (in_array($attribute, $dropDownAttributes)) {
                            $dropDownArray = $dropdownAttributesOptions[$attribute];
                            $currentHtml .= "<select type='select' required='required' class='$class' name='$attribute' title='$attribute'>";
                            foreach ($dropDownArray as $code => $value) {
                                if ($code == $currentAttributeValue) {
                                    $currentHtml .= "<option value='$code'  selected='selected'>$value</option>";
                                } else {
                                    $currentHtml .= "<option value='$code'>$value</option>";
                                }
                            }
                            $currentHtml .= "</select>";
                        } else {
                            $currentHtml = "<input type='text' value='$currentAttributeValue' name='$attribute' class='$class'/>";
                        }
                    }

                    if ($attribute == 'action') {
                        $currentHtml = "<button type='button' data-offer-id='$offerId' class='save primary update-vendor-offer-button'>Update</button>";
                    }
                    $html .= "<td class='value'>" . $currentHtml . "</td>";
                }
                $html .= "</tr>";
            }
        }
        $html .= "</tbody></table>";
        return $html;
    }

    /**
     * @return string[]
     */
    private function getTableHeaders()
    {
        return [
            'Offer-Id' => 'offer_id',
            'Name' => 'name',
            'Sku' => 'sku',
            'Cost' => 'price',
            'Vendor SKU' => 'vendor_product_sku',
            'Vendor Product Name' => 'vendor_product_name',
            'Inventory Qty' => 'inventory_qty',
            'Box Type' => 'box_type',
            'Box height' => 'box_height',
            'Box weight' => 'box_weight',
            'Box Length' => 'box_length',
            'Box width' => 'box_width',
            'Color' => 'color',
            'Dimensional weight' => 'dimensional_weight',
            'Group' => 'group',
            'Head Size' => 'head_size',
            'Product UM' => 'product_um',
            'Qty Per Box' => 'qty_per_box',
            'Certified' => 'sustainably_certified',
            'Brand' => 'brand',
            'Status' => 'status',
            'Action' => 'action'
        ];
    }

    /**
     * @return string[]
     */
    private function getNotEditableAttributes()
    {
        return [
            'offer_id',
            'sku',
            'color',
            'dimensional_weight',
            'group',
            'head_size'
        ];
    }

    /**
     * @return string[]
     */
    private function getEditableAttributes()
    {
        return [
            'name',
            'price',
            'vendor_product_sku',
            'vendor_product_name',
            'inventory_qty',
            'box_type',
            'box_height',
            'box_weight',
            'box_length',
            'box_width',
            'product_um',
            'qty_per_box',
            'sustainably_certified',
            'brand',
            'status'
        ];
    }

    /**
     * @return string[]
     */
    private function dropdownAttributes()
    {
        return [
            'box_type',
            'product_um',
            'status'
        ];
    }

    /**
     * get products tab Url in admin
     * @return string
     */
    public function getProductsGridUrl()
    {
        return $this->_backendUrl->getUrl('vendor/vendor/products', ['_current' => true]);
    }
}
