<?php

namespace Flordelcampo\VendorRegistration\Model\ResourceModel\VendorUser;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\VendorRegistration\Model\VendorUser;
use Flordelcampo\VendorRegistration\Model\ResourceModel\VendorUser as ResourceVendorUser;

/**
 * Class Collection
 *
 * Flordelcampo\VendorRegistration\Model\ResourceModel\VendorUser
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            VendorUser::class,
            ResourceVendorUser::class
        );
    }

}
