<?php

namespace Flordelcampo\VendorRegistration\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class VendorUser
 *
 * Flordelcampo\VendorRegistration\Model\ResourceModel
 */
class VendorUser extends AbstractDb
{
    /**
     * Post constructor.
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Stores Table
     */
    protected function _construct()
    {
        $this->_init('vendor_location_user_settings', 'user_entity_id');
    }

}