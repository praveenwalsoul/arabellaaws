<?php

namespace Flordelcampo\VendorRegistration\Model\ResourceModel\Logistics;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\VendorRegistration\Model\Logistics;
use Flordelcampo\VendorRegistration\Model\ResourceModel\Logistics as ResourceLogistics;

/**
 * Class Collection
 *
 * Flordelcampo\VendorRegistration\Model\ResourceModel\Logistics
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Logistics::class,
            ResourceLogistics::class
        );
    }

}
