<?php                                                                 
 namespace Flordelcampo\ProductsGrid\Model\Config\Source;                         
 class Options implements \Magento\Framework\Option\ArrayInterface            
 {
/**
 * Options for Type
 *
 * @return array
 */
public function toOptionArray()
{
    return [
        ['value' =>  1, 'label' => __('Active')],
        ['value' =>  0, 'label' => __('InActive')]
        
    ];
 }
}