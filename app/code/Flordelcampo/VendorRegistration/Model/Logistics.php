<?php

namespace Flordelcampo\VendorRegistration\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\VendorRegistration\Model\ResourceModel\Logistics as ResourceLogistics;

/**
 * Class Logistics
 *
 * Flordelcampo\VendorRegistration\Model
 */
class Logistics extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceLogistics::class);
    }

}