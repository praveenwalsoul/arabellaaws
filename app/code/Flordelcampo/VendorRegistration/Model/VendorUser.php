<?php

namespace Flordelcampo\VendorRegistration\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\VendorRegistration\Model\ResourceModel\VendorUser as ResourceVendorUser;

/**
 * Class VendorUser
 *
 * Flordelcampo\VendorRegistration\Model
 */
class VendorUser extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceVendorUser::class);
    }

}