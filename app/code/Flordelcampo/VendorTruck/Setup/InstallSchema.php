<?php

namespace Flordelcampo\VendorTruck\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0){

		$installer->run('CREATE TABLE `vendor_truck` (
          truck_id int not null auto_increment,
          `truck_name` varchar(255) DEFAULT NULL,
          `contact_person` varchar(255) DEFAULT NULL,
          `truck_phone_number` varchar(255) DEFAULT NULL,
          `sun` varchar(20) DEFAULT NULL,
          `mon` varchar(20) DEFAULT NULL,
          `tue` varchar(20) DEFAULT NULL,
          `wed` varchar(20) DEFAULT NULL,
          `thu` varchar(20) DEFAULT NULL,
          `fri` varchar(20) DEFAULT NULL,
          `sat` varchar(20) DEFAULT NULL,
          primary key(truck_id))');


		//demo
//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//$scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
//$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/updaterates.log');
//$logger = new \Zend\Log\Logger();
//$logger->addWriter($writer);
//$logger->info('updaterates');
//demo 

		}

        $installer->endSetup();

    }
}