<?php

namespace Flordelcampo\VendorTruck\Model\ResourceModel\Truck;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorTruck\Model\Truck', 'Flordelcampo\VendorTruck\Model\ResourceModel\Truck');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>