<?php
namespace Flordelcampo\VendorTruck\Model\ResourceModel;

class Truck extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendor_truck', 'truck_id');
    }
}
?>