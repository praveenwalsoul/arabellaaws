<?php
namespace Flordelcampo\VendorTruck\Model;

class Truck extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorTruck\Model\ResourceModel\Truck');
    }
}
?>