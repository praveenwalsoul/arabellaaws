<?php
namespace Flordelcampo\VendorTruck\Block\Adminhtml\Truck;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\VendorTruck\Model\truckFactory
     */
    protected $_truckFactory;

    /**
     * @var \Flordelcampo\VendorTruck\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\VendorTruck\Model\truckFactory $truckFactory
     * @param \Flordelcampo\VendorTruck\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\VendorTruck\Model\TruckFactory $TruckFactory,
        \Flordelcampo\VendorTruck\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_truckFactory = $TruckFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('truck_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_truckFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'truck_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'truck_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'truck_name',
					[
						'header' => __('Truck Name'),
						'index' => 'truck_name',
					]
				);
				
				$this->addColumn(
					'contact_person',
					[
						'header' => __('Contact Person'),
						'index' => 'contact_person',
					]
				);
				
				$this->addColumn(
					'truck_phone_number',
					[
						'header' => __('Truck Phone Number'),
						'index' => 'truck_phone_number',
					]
				);
				

						$this->addColumn(
							'sun',
							[
								'header' => __('Sunday'),
								'index' => 'sun',
								'type' => 'options',
								'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray3()
							]
						);

						

						$this->addColumn(
							'mon',
							[
								'header' => __('Monday'),
								'index' => 'mon',
								'type' => 'options',
								'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray4()
							]
						);

						

						$this->addColumn(
							'tue',
							[
								'header' => __('Tuesday'),
								'index' => 'tue',
								'type' => 'options',
								'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray5()
							]
						);

						

						$this->addColumn(
							'wed',
							[
								'header' => __('Wednesday'),
								'index' => 'wed',
								'type' => 'options',
								'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray6()
							]
						);

						

						$this->addColumn(
							'thu',
							[
								'header' => __('Thrusday'),
								'index' => 'thu',
								'type' => 'options',
								'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray7()
							]
						);

						

						$this->addColumn(
							'fri',
							[
								'header' => __('Friday'),
								'index' => 'fri',
								'type' => 'options',
								'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray8()
							]
						);

						

						$this->addColumn(
							'sat',
							[
								'header' => __('Saturday'),
								'index' => 'sat',
								'type' => 'options',
								'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray9()
							]
						);

						


		
        $this->addColumn(
             'edit',
             [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit'
                        ],
                        'field' => 'truck_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );
		

		
		   $this->addExportType($this->getUrl('vendortruck/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('vendortruck/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('truck_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_VendorTruck::truck/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('truck');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('vendortruck/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('vendortruck/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('vendortruck/*/index', ['_current' => true]);
    }

    /**
     * @param \Flordelcampo\VendorTruck\Model\truck|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'vendortruck/*/edit',
            ['truck_id' => $row->getId()]
        );
		
    }

	
		static public function getOptionArray3()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray3()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray3() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray5()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray5()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray5() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray6()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray6()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray6() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray7()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray7()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray7() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray8()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray8()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray8() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray9()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray9()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray9() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		

}