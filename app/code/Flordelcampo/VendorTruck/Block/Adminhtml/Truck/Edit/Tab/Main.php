<?php

namespace Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Edit\Tab;

/**
 * Truck edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Flordelcampo\VendorTruck\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Flordelcampo\VendorTruck\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Flordelcampo\VendorTruck\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('truck');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('truck_id', 'hidden', ['name' => 'truck_id']);
        }

		
        $fieldset->addField(
            'truck_name',
            'text',
            [
                'name' => 'truck_name',
                'label' => __('Truck Name'),
                'title' => __('Truck Name'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'contact_person',
            'text',
            [
                'name' => 'contact_person',
                'label' => __('Contact Person'),
                'title' => __('Contact Person'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'truck_phone_number',
            'text',
            [
                'name' => 'truck_phone_number',
                'label' => __('Truck Phone Number'),
                'title' => __('Truck Phone Number'),
				
                'disabled' => $isElementDisabled
            ]
        );
					

        $fieldset->addField(
            'sun',
            'select',
            [
                'label' => __('Sunday'),
                'title' => __('Sunday'),
                'name' => 'sun',
				
                'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray3(),
                'disabled' => $isElementDisabled
            ]
        );

						

        $fieldset->addField(
            'mon',
            'select',
            [
                'label' => __('Monday'),
                'title' => __('Monday'),
                'name' => 'mon',
				
                'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray4(),
                'disabled' => $isElementDisabled
            ]
        );

						

        $fieldset->addField(
            'tue',
            'select',
            [
                'label' => __('Tuesday'),
                'title' => __('Tuesday'),
                'name' => 'tue',
				
                'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray5(),
                'disabled' => $isElementDisabled
            ]
        );

						

        $fieldset->addField(
            'wed',
            'select',
            [
                'label' => __('Wednesday'),
                'title' => __('Wednesday'),
                'name' => 'wed',
				
                'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray6(),
                'disabled' => $isElementDisabled
            ]
        );

						

        $fieldset->addField(
            'thu',
            'select',
            [
                'label' => __('Thrusday'),
                'title' => __('Thrusday'),
                'name' => 'thu',
				
                'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray7(),
                'disabled' => $isElementDisabled
            ]
        );

						

        $fieldset->addField(
            'fri',
            'select',
            [
                'label' => __('Friday'),
                'title' => __('Friday'),
                'name' => 'fri',
				
                'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray8(),
                'disabled' => $isElementDisabled
            ]
        );

						

        $fieldset->addField(
            'sat',
            'select',
            [
                'label' => __('Saturday'),
                'title' => __('Saturday'),
                'name' => 'sat',
				
                'options' => \Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Grid::getOptionArray9(),
                'disabled' => $isElementDisabled
            ]
        );

						

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
