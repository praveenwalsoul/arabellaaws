<?php
namespace Flordelcampo\VendorTruck\Block\Adminhtml\Truck\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('truck_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Truck Information'));
    }
}