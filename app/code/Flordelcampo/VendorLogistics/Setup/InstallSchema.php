<?php

namespace Flordelcampo\VendorLogistics\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context){

       $installer = $setup;
        $installer->startSetup();

        // Get tutorial_simplenews table
  
        if (version_compare($context->getVersion(), '1.0.0') < 0){
            
            $installer->run('CREATE TABLE `vendor_loc_logistics` (
          loc_logistics_id int not null auto_increment,
          vendor_id int not null,
          `location_id` varchar(10) DEFAULT NULL,
          `store_id` varchar(10) DEFAULT NULL,
          `website_id` varchar(10) DEFAULT NULL,
          `cost_channel_id` varchar(10) DEFAULT NULL,
          `origin_country_id` varchar(10) DEFAULT NULL,
          `vendor_country_id` varchar(10) DEFAULT NULL,
          `freight_forwarder_id` varchar(10) DEFAULT NULL,
          `leadtime` varchar(10) DEFAULT NULL,
          `usefilabels` varchar(10) DEFAULT NULL,
          `booking_days_adj` varchar(10) DEFAULT NULL,
          `buffer_days` varchar(10) DEFAULT NULL,
          `customer_truck_req` varchar(10) DEFAULT NULL,
          `allow_add_chg` varchar(10) DEFAULT NULL,
          `hide_from_guest` varchar(10) DEFAULT NULL,
          `lead_time_to_box_handler` varchar(10) DEFAULT NULL,
          `shipping_account_number` varchar(255) DEFAULT NULL,
          `shipping_user` varchar(255) DEFAULT NULL,
          `shipping_password` varchar(255) DEFAULT NULL,
          `shipping_access_license` varchar(255) DEFAULT NULL,
          `sun` varchar(10) DEFAULT NULL,
          `mon` varchar(10) DEFAULT NULL,
          `tue` varchar(10) DEFAULT NULL,
          `wed` varchar(10) DEFAULT NULL,
          `thu` varchar(10) DEFAULT NULL,
          `fri` varchar(10) DEFAULT NULL,
          `sat` varchar(10) DEFAULT NULL,
          `sunday_timing` varchar(10) DEFAULT NULL,
          `monday_timing` varchar(10) DEFAULT NULL,
          `tuesday_timing` varchar(10) DEFAULT NULL,
          `wednesday_timing` varchar(10) DEFAULT NULL,
          `thursday_timing` varchar(10) DEFAULT NULL,
          `friday_timing` varchar(10) DEFAULT NULL,
          `saturday_timing` varchar(10) DEFAULT NULL,
          `is_active` varchar(11) DEFAULT NULL,
          primary key(loc_logistics_id))
        ');
        
    }
  
    $installer->endSetup();
  }

}