<?php

namespace Flordelcampo\VendorLogistics\Model\ResourceModel\Logistics;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorLogistics\Model\Logistics', 'Flordelcampo\VendorLogistics\Model\ResourceModel\Logistics');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>