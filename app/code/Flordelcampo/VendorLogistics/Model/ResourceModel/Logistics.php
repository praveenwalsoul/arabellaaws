<?php
namespace Flordelcampo\VendorLogistics\Model\ResourceModel;

class Logistics extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendor_loc_logistics', 'loc_logistics_id');
    }
}
?>