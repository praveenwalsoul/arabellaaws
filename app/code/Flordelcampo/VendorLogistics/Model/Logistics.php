<?php
namespace Flordelcampo\VendorLogistics\Model;

class Logistics extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorLogistics\Model\ResourceModel\Logistics');
    }
}
?>