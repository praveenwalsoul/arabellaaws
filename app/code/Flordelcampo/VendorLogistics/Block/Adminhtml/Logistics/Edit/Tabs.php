<?php
namespace Flordelcampo\VendorLogistics\Block\Adminhtml\Logistics\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('logistics_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Logistics Information'));
    }
}