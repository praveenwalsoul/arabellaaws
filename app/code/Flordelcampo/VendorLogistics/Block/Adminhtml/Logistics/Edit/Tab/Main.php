<?php

namespace Flordelcampo\VendorLogistics\Block\Adminhtml\Logistics\Edit\Tab;

/**
 * Logistics edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Flordelcampo\VendorLogistics\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Flordelcampo\VendorLogistics\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Flordelcampo\VendorLogistics\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('logistics');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('loc_logistics_id', 'hidden', ['name' => 'loc_logistics_id']);
        }

		
        $fieldset->addField(
            'vendor_id',
            'text',
            [
                'name' => 'vendor_id',
                'label' => __('Vendor id'),
                'title' => __('Vendor id'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'cost_channel_id',
            'text',
            [
                'name' => 'cost_channel_id',
                'label' => __('Cost Channel id'),
                'title' => __('Cost Channel id'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'location_id',
            'text',
            [
                'name' => 'location_id',
                'label' => __('Location id'),
                'title' => __('Location id'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'store_id',
            'text',
            [
                'name' => 'store_id',
                'label' => __('Store id'),
                'title' => __('Store id'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'origin_country_id',
            'text',
            [
                'name' => 'origin_country_id',
                'label' => __('Org Country Id'),
                'title' => __('Org Country Id'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'vendor_country_id',
            'text',
            [
                'name' => 'vendor_country_id',
                'label' => __('Country Vendor Id'),
                'title' => __('Country Vendor Id'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'freight_forwarder_id',
            'text',
            [
                'name' => 'freight_forwarder_id',
                'label' => __('Freight Forwarder id'),
                'title' => __('Freight Forwarder id'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'leadtime',
            'text',
            [
                'name' => 'leadtime',
                'label' => __('Lead Time'),
                'title' => __('Lead Time'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'usefilabels',
            'text',
            [
                'name' => 'usefilabels',
                'label' => __('usEfilabels'),
                'title' => __('usEfilabels'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'booking_days_adj',
            'text',
            [
                'name' => 'booking_days_adj',
                'label' => __('Booking Days Adj'),
                'title' => __('Booking Days Adj'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'buffer_days',
            'text',
            [
                'name' => 'buffer_days',
                'label' => __('Buffer days'),
                'title' => __('Buffer days'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'customer_truck_req',
            'text',
            [
                'name' => 'customer_truck_req',
                'label' => __('Customer Track Req'),
                'title' => __('Customer Track Req'),
				
                'disabled' => $isElementDisabled
            ]
        );
					

        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Active'),
                'title' => __('Active'),
                'name' => 'is_active',
				
                'options' => \Flordelcampo\VendorLogistics\Block\Adminhtml\Logistics\Grid::getOptionArray12(),
                'disabled' => $isElementDisabled
            ]
        );

						

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
