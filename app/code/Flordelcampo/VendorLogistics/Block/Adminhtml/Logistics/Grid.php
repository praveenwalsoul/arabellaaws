<?php
namespace Flordelcampo\VendorLogistics\Block\Adminhtml\Logistics;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\VendorLogistics\Model\logisticsFactory
     */
    protected $_logisticsFactory;

    /**
     * @var \Flordelcampo\VendorLogistics\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\VendorLogistics\Model\logisticsFactory $logisticsFactory
     * @param \Flordelcampo\VendorLogistics\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\VendorLogistics\Model\LogisticsFactory $LogisticsFactory,
        \Flordelcampo\VendorLogistics\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_logisticsFactory = $LogisticsFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('loc_logistics_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_logisticsFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'loc_logistics_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'loc_logistics_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'vendor_id',
					[
						'header' => __('Vendor id'),
						'index' => 'vendor_id',
					]
				);
				
				$this->addColumn(
					'cost_channel_id',
					[
						'header' => __('Cost Channel id'),
						'index' => 'cost_channel_id',
					]
				);
				
				$this->addColumn(
					'location_id',
					[
						'header' => __('Location id'),
						'index' => 'location_id',
					]
				);
				
				$this->addColumn(
					'store_id',
					[
						'header' => __('Store id'),
						'index' => 'store_id',
					]
				);
				
				$this->addColumn(
					'origin_country_id',
					[
						'header' => __('Org Country Id'),
						'index' => 'origin_country_id',
					]
				);
				
				$this->addColumn(
					'vendor_country_id',
					[
						'header' => __('Country Vendor Id'),
						'index' => 'vendor_country_id',
					]
				);
				
				$this->addColumn(
					'freight_forwarder_id',
					[
						'header' => __('Freight Forwarder id'),
						'index' => 'freight_forwarder_id',
					]
				);
				
				$this->addColumn(
					'leadtime',
					[
						'header' => __('Lead Time'),
						'index' => 'leadtime',
					]
				);
				
				$this->addColumn(
					'usefilabels',
					[
						'header' => __('usEfilabels'),
						'index' => 'usefilabels',
					]
				);
				
				$this->addColumn(
					'booking_days_adj',
					[
						'header' => __('Booking Days Adj'),
						'index' => 'booking_days_adj',
					]
				);
				
				$this->addColumn(
					'buffer_days',
					[
						'header' => __('Buffer days'),
						'index' => 'buffer_days',
					]
				);
				
				$this->addColumn(
					'customer_truck_req',
					[
						'header' => __('Customer Track Req'),
						'index' => 'customer_truck_req',
					]
				);
				

						$this->addColumn(
							'is_active',
							[
								'header' => __('Active'),
								'index' => 'is_active',
								'type' => 'options',
								'options' => \Flordelcampo\VendorLogistics\Block\Adminhtml\Logistics\Grid::getOptionArray12()
							]
						);

						


		
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'loc_logistics_id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
		

		
		   $this->addExportType($this->getUrl('vendorlogistics/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('vendorlogistics/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('loc_logistics_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_VendorLogistics::logistics/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('logistics');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('vendorlogistics/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('vendorlogistics/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('vendorlogistics/*/index', ['_current' => true]);
    }

    /**
     * @param \Flordelcampo\VendorLogistics\Model\logistics|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'vendorlogistics/*/edit',
            ['loc_logistics_id' => $row->getId()]
        );
		
    }

	
		static public function getOptionArray12()
		{
            $data_array=array(); 
			$data_array[0]='Yes';
			$data_array[1]='No';
            return($data_array);
		}
		static public function getValueArray12()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorLogistics\Block\Adminhtml\Logistics\Grid::getOptionArray12() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		

}