<?php

namespace Flordelcampo\Order\Plugin\Order;

use Exception;
use Flordelcampo\Order\Model\BoxFactory;
use Magento\Sales\Api\OrderManagementInterface;

/**
 * Class PlaceAfterPlugin
 *
 * @package Flordelcampo\Order\Plugin\Order
 */
class PlaceAfterPlugin
{
    /**
     * Order Object
     *
     * @var null
     */
    private $_order;
    /**
     * Box Facotory
     *
     * @var BoxFactory
     */
    protected $boxFactory;

    /**
     * PlaceAfterPlugin constructor.
     *
     * @param BoxFactory $boxFactory Box Factory
     */
    public function __construct(BoxFactory $boxFactory)
    {
        $this->boxFactory = $boxFactory;
    }

    /**
     * After Order Place Plugin
     *
     * @param OrderManagementInterface $orderManagementInterface Interface
     * @param OrderManagementInterface $order Order
     *
     * @return object
     * @throws Exception
     */
    public function afterPlace(
        OrderManagementInterface $orderManagementInterface,
        $order
    )
    {
        $this->_order = $order;
        $this->createBoxDataForOrder();
        return $order;
    }

    /**
     * Create Box Details For Order Object
     *
     * @param null $order Order
     *
     * @return void;
     * @throws Exception
     */
    public function createBoxDataForOrder($order = null)
    {
        if ($order) {
            $this->_order = $order;
            $orderId = $order->getId();
        } else {
            $orderId = $this->_order->getId();
        }

        foreach ($this->_order->getItems() as $item) {
            $qty = $item->getQtyOrdered();
            $itemId = $item->getId();
            $track_id = $item->getTrackId();
            for ($i = 1; $i <= $qty; $i++) {
                $boxModel = $this->boxFactory->create();
                $boxModel->addData(
                    [
                        "box_id" => $itemId . '_' . $i,
                        "order_id" => $orderId,
                        "item_id" => $itemId,
                        "track_id" => $track_id
                    ]
                );
                $boxModel->save();
            }
        }
        return;
    }
}