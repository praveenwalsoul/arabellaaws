<?php

namespace Flordelcampo\Order\Plugin\Quote;

use Flordelcampo\Order\Block\CreateOrder;

/**
 * Class Item
 * Flordelcampo\Order\Plugin\Quote
 */
class Item
{
    /**
     * @var CreateOrder
     */
    protected $createOrderBlock;

    /**
     * Item constructor.
     * @param CreateOrder $createOrderBlock
     */
    public function __construct(
        CreateOrder $createOrderBlock
    ) {
        $this->createOrderBlock = $createOrderBlock;
    }

    /**
     * After Plugin to add same sku multiple times to cart
     *
     * @param $product
     * @param $result
     * @return bool
     */
    public function afterRepresentProduct($product, $result)
    {
        $source = $this->createOrderBlock->getBackendSession()->getSource();
        if ($source == 'backend') {
            $this->createOrderBlock->getBackendSession()->unsSource();
            return false;
        }
        return $result;
    }
}