<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Createorder;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Store\Model\StoreManagerInterface;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;

/**
 * Class Address
 */
class Address extends Action
{
    /**
     * Json Factory
     *
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * Customer Factory
     *
     * @var CustomerFactory
     */
    protected $customerFactory;
    /**
     * Store
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var Customer
     */
    private $customerModel;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;

    /**
     * Address constructor.
     *
     * @param Context $context Context
     * @param JsonFactory $jsonFactory Json
     * @param CustomerFactory $customerFactory CustomerFactory
     * @param StoreManagerInterface $storeManager
     * @param Customer $customerModel
     * @param CustomerHelper $customerHelper
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CustomerFactory $customerFactory,
        StoreManagerInterface $storeManager,
        Customer $customerModel,
        CustomerHelper $customerHelper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->customerModel = $customerModel;
        $this->customerHelper = $customerHelper;
        parent::__construct($context);
    }

    /**
     * Return Customer Address
     *
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $customerId = $this->getRequest()->getParam('customerId');
        try {
            $customerAddress[''] = 'Please Select';
            $activeStoresArray = $this->customerHelper->getActiveCustomerStoresByCustomerId($customerId);
            if (count($activeStoresArray) > 0) {
                foreach ($activeStoresArray as $storeId => $storeName) {
                    $customerAddress[$storeId] = $storeName;
                }
            }
        } catch (Exception $e) {
            $customerAddress = ['id' => '', 'company' => "Please select"];
        }
        $customerAddressHtml = $this->addressHtml($customerAddress);
        $resultJson->setData(['response' => $customerAddressHtml]);
        return $resultJson;
    }

    /**
     * Return Address Html
     *
     * @param $customerAddress
     * @return string
     */
    public function addressHtml($customerAddress)
    {
        $html = '';
        if (count($customerAddress) > 0) {
            foreach ($customerAddress as $id => $value) {
                if ($id == '') {
                    $html .= "<option value='$id' selected='selected'>$value</option>";
                } else {
                    $html .= "<option value='$id'>$value</option>";
                }
            }
        }
        return $html;
    }

}