<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Createorder;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Order\Block\CreateOrder;
use Magento\Framework\File\Csv;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as vendorHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;


/**
 * Class Reader
 *
 * Flordelcampo\Order\Controller\Adminhtml\Createorder
 */
class Reader extends Action
{
    /**
     * @var null
     */
    private $_FILE;
    /**
     * @var null
     */
    private $_skuList;
    /**
     * @var
     */
    private $_csvHeaders;
    /**
     * @var null
     */
    private $_params;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CreateOrder
     */
    protected $createOrderBlock;
    /**
     * @var Csv
     */
    protected $_csv;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var vendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CreateOrder $createOrderBlock
     * @param Csv $csv
     * @param CustomerHelper $customerHelper
     * @param vendorHelper $vendorHelper
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CreateOrder $createOrderBlock,
        Csv $csv,
        CustomerHelper $customerHelper,
        vendorHelper $vendorHelper,
        CatalogHelper $catalogHelper
    )
    {
        $this->_csv = $csv;
        $this->jsonFactory = $jsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->createOrderBlock = $createOrderBlock;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->catalogHelper = $catalogHelper;
        parent::__construct($context);
    }

    /**
     * Read CSV and Append as Table
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $params = $this->getRequest()->getParams();
        $file = $this->getRequest()->getFiles('order_file');
        $this->_FILE = $file;
        $this->_params = $params;
        $data = null;
        if ($this->getRequest()->isPost()
            && !empty($this->getRequest()->getFiles('order_file'))
        ) {
            try {
                $validateFile = $this->validateFile();
                if ($validateFile) {
                    $processedDataHtml = $this->processCsvToHtml();
                    $itemsCount = $this->skuCountOfCsv();
                    $vendorFarmsHtml = $this->getAllFarmsWithVendorHtml();
                    $data = [
                        'status' => true,
                        'html' => $processedDataHtml,
                        'itemsCount' => $itemsCount,
                        'vendorFarmsHtml' => $vendorFarmsHtml,
                        'message' => 'success'
                    ];
                } else {
                    $data = [
                        'status' => false,
                        'message' => 'Invalid File Format'
                    ];
                }
            } catch (Exception $e) {
                $data = [
                    'status' => false,
                    'message' => $e->getMessage()
                ];
            }
        } else {
            $data = [
                'status' => false,
                'message' => 'Invalid File'
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * Return HTML of Template
     *
     * @throws Exception
     */
    public function processCsvToHtml()
    {
        $processedData = $this->processCsvToArray();
        $vendorList = $this->vendorHelper->getAllVendorListArrayForCreateOrder();
        $skuNamesArray = $this->getSkuNames();
        $html = '';
        if (count($processedData) > 0) {
            $csvHeaders = $this->_csvHeaders;
            /**
             * csv Headers HTML
             */
            $html = "<table class='admin__table-secondary customer-order-items-table'><tbody>";
            $html .= "<tr>";
            foreach ($csvHeaders as $colName) {
                $html .= "<th>$colName</th>";
                if (strtolower($colName) == 'sku') {
                    $html .= "<th>Name</th>";
                }
                if (strtolower($colName) == 'vendor') {
                    $html .= "<th>Vendor Farm</th>";
                }
            }
            $html .= "</tr>";

            $count = 1;
            foreach ($processedData as $param) {
                $sku = trim($param['sku']);
                $name = $skuNamesArray[$sku];
                $qtyPerBox = trim($param['qty_per_box']);
                $qty = trim($param['qty']);
                $soPrice = trim($param['so_price']);
                $poPrice = trim($param['po_price']);
                $vendorId = trim($param['vendor']);
                $deliveryDate = trim($param['delivery_date']);
                $shippingDate = trim($param['shipping_date']);
                $transDate = trim($param['trans_date']);
                $orderType = trim($param['order_type']);
                $orderType = trim($param['order_type']);

                $skuHtml = "<input type='text' class='order-item-row-data-$count item-row' name='sku' data-label='sku' value='$sku' disabled='disabled'/>";
                $nameHtml = "<input type='text' class='order-item-row-data-$count item-row' name='name' data-label='name' value='$name' />";
                $qtpHtml = "<input type='number' min='1' class='order-item-row-data-$count item-row' name='qty-per-box' data-label='qty_per_box' value='$qtyPerBox' />";
                $qtyHtml = "<input type='number' min='1' class='order-item-row-data-$count item-row' name='qty' data-label='qty' value='$qty' />";
                $soPriceHtml = "<input type='number' step='any' min='0' class='order-item-row-data-$count item-row' name='so-price' data-label='so_price' value='$soPrice' />";
                $poPriceHtml = "<input type='number' step='any' min='0' class='order-item-row-data-$count item-row' name='po-price' data-label='po_price' value='$poPrice' />";
                $deliveryDateHtml = "<input type='text' aria-required='true' class='order-item-row-data-$count item-row date-picker' name='delivery-date' data-label='delivery_date' value='$deliveryDate' />";
                $shippingDate = "<input type='text' aria-required='true' class='order-item-row-data-$count item-row date-picker' name='shipping-date' data-label='shipping_date'  value='$shippingDate' />";
                $transDate = "<input type='text' aria-required='true' class='order-item-row-data-$count item-row date-picker' name='trans-date' data-label='trans_date' value='$transDate' />";
                $orderType = "<input type='text' class='order-item-row-data-$count item-row' data-label='order_type' name='order-type' value='$orderType' />";

                /**
                 * If vendor Id is null get associated vendors for sku
                 */
                if ($vendorId == '' || $vendorId == '0') {
                    $vendorList = $this->getVendorsForSku($sku);
                }
                /**
                 * Vendor Html
                 */
                $vendorHtml = "<select type='select' data-label='vendor' name='customer-order-vendor' data-row-count='$count' class='order-item-row-data-$count vendor-dropdown item-row' >";
                foreach ($vendorList as $id => $value) {
                    $selected = ($id == null) ? "disabled='disabled'" : "";
                    if ($id == $vendorId) {
                        $vendorHtml .= "<option value='$id' $selected selected='selected'>$value</option>";
                    } else {
                        $vendorHtml .= "<option value='$id' $selected>$value</option>";
                    }
                }
                $vendorHtml .= "</select>";

                /**
                 * Farm Html
                 * If vendor Id empty
                 */
                $farmId = '';
                $farmHtml = '';
                $farmHtml .= "<select type='select' data-label='vendor_farm' name='customer-order-vendor-farm' id='vendor-farm-$count' class='order-item-row-data-$count item-row' >";
                if ($vendorId == '' || $vendorId == '0') {
                    $farmHtml .= "<option value='' selected='selected'>Select Farm</option>";
                } else {
                    $vendorFarmsList = $this->vendorHelper->getActiveFarmsByVendorId($vendorId);
                    foreach ($vendorFarmsList as $id => $value) {
                        $selected = ($id == '') ? "disabled='disabled'" : "";
                        if ($id == $farmId) {
                            $farmHtml .= "<option $selected value='$id' selected='selected'>$value</option>";
                        } else {
                            $farmHtml .= "<option value='$id' $selected>$value</option>";
                        }
                    }
                }
                $html .= "<tr>";
                $html .= "<td class='value'>" . $skuHtml . "</td>";
                $html .= "<td class='value'>" . $nameHtml . "</td>";
                $html .= "<td class='value'>" . $qtpHtml . "</td>";
                $html .= "<td class='value'>" . $qtyHtml . "</td>";
                $html .= "<td class='value'>" . $soPriceHtml . "</td>";
                $html .= "<td class='value'>" . $poPriceHtml . "</td>";
                $html .= "<td class='value'>" . $vendorHtml . "</td>";
                $html .= "<td class='value'>" . $farmHtml . "</td>";
                $html .= "<td class='value'>" . $deliveryDateHtml . "</td>";
                $html .= "<td class='value'>" . $shippingDate . "</td>";
                $html .= "<td class='value'>" . $transDate . "</td>";
                $html .= "<td class='value'>" . $orderType . "</td>";
                $html .= "</tr>";
                $count++;
            }

            $html .= "</tbody></table>";
        } else {
            throw new LocalizedException(__('No items in the CSV'));
        }
        return $html;
    }

    /**
     * Return Vendors Data for sku
     *
     * @param $sku
     * @return array|null
     */
    public function getVendorsForSku($sku)
    {
        $vendorList = $this->vendorHelper->getVendorDataBySku($sku);
        if ($vendorList == null) {
            return [];
        }
        return $vendorList;
    }

    /**
     * Return All Farms with Vendor Array
     */
    public function getAllFarmsWithVendorHtml()
    {
        $vendorFarmHtmlArray = [];
        $allVendorFarmsList = $this->vendorHelper->getAllVendorFarmsArray();
        if (count($allVendorFarmsList) > 0) {
            foreach ($allVendorFarmsList as $vendorId => $farmData) {
                $farmHtml = '';
                $farmHtml .= "<option value='' selected='selected'>Select Farm</option>";
                foreach ($farmData as $farmId => $farmName) {
                    $farmHtml .= "<option value='$farmId'>$farmName</option>";
                }
                $vendorFarmHtmlArray[$vendorId] = $farmHtml;
            }
        }
        return $vendorFarmHtmlArray;
    }

    /**
     * Return CSV Count
     *
     * @return int|void
     * @throws Exception
     */
    public function skuCountOfCsv()
    {
        $FILE = $this->_FILE;
        $rawData = $this->_csv->getData($FILE['tmp_name']);
        return count($rawData) - 1;
    }

    /**
     * Return Vendor Data in HTML
     *
     * @return string
     */
    public function vendorHtml()
    {
        $vendorList = $this->vendorHelper->getAllVendorListArray();
        $vendorHtml = '';
        $vendorHtml .= "<select type='select' name='customer-order-vendor' class='order-item-column' >";
        $count = 1;
        foreach ($vendorList as $id => $value) {
            if ($id == '0') {
                $vendorHtml .= "<option value='$id' selected='selected'>Select Vendor</option>";
                $count++;
            } else {
                $vendorHtml .= "<option value='$id'>$value</option>";
            }
        }
        $vendorHtml .= "</select>";
        return $vendorHtml;
    }

    /**
     * Validate File Template
     */
    public function validateFile()
    {
        $mimesArray = $this->createOrderBlock->getMimesTypes();
        $FILE = $this->_FILE;
        if (!isset($FILE['tmp_name'])) {
            $response = false;
        } elseif (!in_array($FILE['type'], $mimesArray)) {
            $response = false;
        } else {
            $response = true;
        }
        return $response;
    }

    /**
     * Return Processed Array
     *
     * @return array
     * @throws Exception
     */
    public function processCsvToArray()
    {
        $skuArray = [];
        $FILE = $this->_FILE;
        $rawData = $this->_csv->getData($FILE['tmp_name']);
        $headers = $this->getPreDefinedHeaders();
        $processedData = $csvHeaders = [];
        $count = 1;
        //$_csvHeaders
        foreach ($rawData as $row) {
            if ($count != 1) {
                $processedData[] = array_combine($headers, $row);
                $skuArray[] = $row[0];
            } else {

                $csvHeaders = $row;
                $count = 99;
            }
        }
        $this->_skuList = $skuArray;
        $this->_csvHeaders = $csvHeaders;
        return $processedData;
    }

    /**
     * Return All Sku with Names
     * @return array
     */
    public function getSkuNames()
    {
        $skuNamesArray = [];
        $skuList = $this->_skuList;
        $collection = $this->catalogHelper->getProductCollectionBySku($skuList);
        foreach ($collection as $oneItem) {
            $skuNamesArray[$oneItem->getSKu()] = $oneItem->getName();
        }
        return $skuNamesArray;
    }

    /**
     * Return Header Fields
     *
     * @return array
     */
    public function getPreDefinedHeaders()
    {
        return [
            'sku',
            'qty_per_box',
            'qty',
            'so_price',
            'po_price',
            'vendor',
            'delivery_date',
            'shipping_date',
            'trans_date',
            'order_type'
        ];
    }
}
