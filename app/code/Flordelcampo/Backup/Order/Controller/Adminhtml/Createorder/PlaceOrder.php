<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Createorder;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Order\Model\Order;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class PlaceOrder
 *
 * Flordelcampo\Order\Controller\Adminhtml\Createorder
 */
class PlaceOrder extends Action
{
    /**
     * @var null
     */
    private $_params;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Order
     */
    protected $orderModel;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Order $order
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Order $order,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->orderModel = $order;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Place Order
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $params = $this->getRequest()->getParams();
            $this->_params = $params;
            $validateMessage = $this->validateData();
            if ($validateMessage == 'success') {
                $orderFormat = $this->getOrderFormat();
                $response = $this->orderModel->createOrderForFlordelCsv($orderFormat);
                $data = [
                    'status' => true,
                    'message' => 'Order Created Success fully'
                ];
            } else {
                $data = [
                    'status' => false,
                    'message' => $validateMessage
                ];
            }

        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * Return Csv into order Format
     *
     * @return mixed|array
     */
    public function getOrderFormat()
    {
        $params = $this->_params;
        $customerId = $params['customer_id'];
        $storeId = $params['store_id'];
        $itemsDataArray[$customerId][$storeId] = $params['data'];
        return $itemsDataArray;
    }

    /**
     * Return Validate Array
     *
     * @return bool|array|string
     */
    public function validateData()
    {

        $error = [];
        $errorString = '';
        $params = $this->_params['data'];
        $count = 1;
        foreach ($params as $item) {
            if ($item['vendor'] == '0'
                || $item['vendor'] == '') {
                $error[] = 'Please Select Vendor at Row Number ' . $count;
            }
            if ($item['vendor_farm'] == '0'
                || $item['vendor_farm'] == '') {
                $error[] = 'Please Select Vendor Farm at Row Number ' . $count;
            }
            if (trim($item['delivery_date']) == '') {
                $error[] = 'Delivery Date Should Not be Empty at Row Number ' . $count;
            }
            if (trim($item['shipping_date']) == '') {
                $error[] = 'Shipping Date Should Not be Empty at Row Number ' . $count;
            }
            if (trim($item['trans_date']) == '') {
                $error[] = 'Trans Date Should Not be Empty at Row Number ' . $count;
            }
            $ship_date = strtotime($item['shipping_date']);
            $delivery_date = strtotime($item['delivery_date']);
            $trans_date = strtotime($item['trans_date']);
            if ($ship_date > $delivery_date) {
                $error[] = 'Shipping Date Should be less than Delivery Date at Row Number ' . $count;
            }
            if ($trans_date > $delivery_date) {
                $error[] = 'Trans Date Should be less than Delivery Date at Row Number ' . $count;
            }
            $count++;
        }
        if (count($error) == 0) {
            return 'success';
        }
        $errorString = implode("<br>", $error);
        return $errorString;
    }
}
