<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Walmart;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\File\Csv;
use Flordelcampo\Order\Block\CreateOrder;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Order\Model\Order;

/**
 * Class Create
 *
 * Flordelcampo\Order\Controller\Adminhtml\Walmart
 */
class Create extends Action
{
    /**
     * @var null
     */
    private $_error;
    /**
     * @var null
     */
    private $_FILE;
    /**
     * @var null
     */
    private $_params;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Csv
     */
    protected $_csv;
    /**
     * @var CreateOrder
     */
    protected $createOrderBlock;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var Order
     */
    protected $orderCreate;
    /**
     * @var Product
     */
    protected $productModel;

    /**
     * @param Context $context
     * @param Csv $csv
     * @param CreateOrder $createOrderBlock
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param Order $order
     * @param Product $productModel
     */
    public function __construct(
        Context $context,
        Csv $csv,
        CreateOrder $createOrderBlock,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        Order $order,
        Product $productModel
    )
    {
        $this->_csv = $csv;
        $this->createOrderBlock = $createOrderBlock;
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonFactory = $jsonFactory;
        $this->orderCreate = $order;
        $this->productModel = $productModel;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();

        $params = $this->getRequest()->getParams();
        $file = $this->getRequest()->getFiles('order_file');
        $this->_FILE = $file;
        $this->_params = $params;
        if ($this->getRequest()->isPost()
            && !empty($this->getRequest()->getFiles('order_file'))
        ) {
            try {
                $validateFile = $this->validateFile();
                if ($validateFile) {
                    $csvData = $this->processCsvToArray();
                    if (is_array($csvData)) {
                        $response = $this->orderCreate->createOrderForWalMart(
                            $csvData,
                            $this->_params
                        );
                        $data = [
                            'status' => true,
                            'message' => 'Order Created Successfully',
                        ];
                    } else {
                        $data = [
                            'status' => false,
                            'message' => $this->_error,
                        ];
                    }
                } else {
                    $data = [
                        'status' => false,
                        'message' => 'Invalid File Format'
                    ];
                }
            } catch (Exception $e) {
                $data = [
                    'status' => false,
                    'message' => $e->getMessage()
                ];
            }

        } else {
            $data = [
                'status' => false,
                'message' => 'Invalid File'
            ];
        }
        $resultJson->setData($data);

        return $resultJson;
    }

    /**
     * Validate File Template
     */
    public function validateFile()
    {
        $mimesArray = $this->createOrderBlock->getMimesTypes();
        $FILE = $this->_FILE;
        if (!isset($FILE['tmp_name'])) {
            $response = false;
        } elseif (!in_array($FILE['type'], $mimesArray)) {
            $response = false;
        } else {
            $response = true;
        }
        return $response;
    }

    /**
     * Return Processed Array
     *
     * @return array|string
     * @throws Exception
     */
    public function processCsvToArray()
    {
        $FILE = $this->_FILE;
        $rawData = $this->_csv->getData($FILE['tmp_name']);
        $processedData = $error = [];
        $count = 1;
        foreach ($rawData as $row) {
            $data = [];
            if ($count != 1) {

                /**
                 * Check for SKU existence
                 */
                $sku = $row[21];

                if (!$this->productModel->getIdBySku($sku)) {
                    $error[] = "Product with SKU " . $sku . " doesn't exist";
                    continue;
                }
                $partnerOrderId = $row[1];
                $partnerPo = $row[0];

                $data['order']['partner_order_id'] = $partnerOrderId;
                $data['order']['order_date'] = $row[2];
                $data['order']['ship_price'] = $row[23];
                $data['order']['tax'] = $row[24];

                $data['item']['partner_po'] = $partnerPo;
                $data['item']['shipping_date'] = $row[3];
                $data['item']['delivery_date'] = $row[4];

                $nameArray = explode(" ", $row[5]);
                $firstName = $middleName = $lastName = '';
                if (count($nameArray) == 3) {
                    $firstName = $nameArray[0];
                    $middleName = $nameArray[1];
                    $lastName = $nameArray[2];
                } elseif (count($nameArray) == 2) {
                    $firstName = $nameArray[0];
                    $lastName = $nameArray[1];
                } else {
                    $firstName = $nameArray[0];
                }

                $address = [
                    'customer_address_id' => '',
                    'prefix' => '',
                    'firstname' => $firstName,
                    'middlename' => $middleName,
                    'lastname' => $lastName,
                    'suffix' => '',
                    'company' => '',
                    'street' => [
                        '0' => $row[9],
                        '1' => $row[10]
                    ],
                    'city' => $row[11],
                    'country_id' => 'US',
                    'region' => $row[12],
                    'postcode' => $row[13],
                    'telephone' => $row[7]
                ];
                $data['address'] = $address;
                $data['item']['sku'] = $sku;
                $data['item']['qty'] = $row[20];
                $data['item']['item_price'] = $row[22];
                $data['item']['track_number'] = $row[28];

                $processedData[$partnerOrderId][] = $data;

            } else {
                $count = 99;
            }
        }
        if (count($error) > 0) {
            $errorString = implode("<br>", $error);
            $this->_error = $errorString;
            return 'fail';
        }
        return $processedData;
    }

}
