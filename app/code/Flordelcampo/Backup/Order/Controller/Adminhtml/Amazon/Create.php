<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Amazon;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\File\Csv;
use Flordelcampo\Order\Block\CreateOrder;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Order\Model\Order;

/**
 * Class Create
 *
 * Flordelcampo\Order\Controller\Adminhtml\Walmart
 */
class Create extends Action
{
    /**
     * @var null
     */
    private $_error;
    /**
     * @var null
     */
    private $_FILE;
    /**
     * @var null
     */
    private $_params;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Csv
     */
    protected $_csv;
    /**
     * @var CreateOrder
     */
    protected $createOrderBlock;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var Order
     */
    protected $orderCreate;
    /**
     * @var Product
     */
    protected $productModel;

    /**
     * @param Context $context
     * @param Csv $csv
     * @param CreateOrder $createOrderBlock
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param Order $order
     * @param Product $productRepository
     */
    public function __construct(
        Context $context,
        Csv $csv,
        CreateOrder $createOrderBlock,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        Order $order,
        Product $productModel
    )
    {
        $this->_csv = $csv;
        $this->createOrderBlock = $createOrderBlock;
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonFactory = $jsonFactory;
        $this->orderCreate = $order;
        $this->productModel = $productModel;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();

        $params = $this->getRequest()->getParams();
        $file = $this->getRequest()->getFiles('order_file');
        $this->_FILE = $file;
        $this->_params = $params;
        if ($this->getRequest()->isPost()
            && !empty($this->getRequest()->getFiles('order_file'))
        ) {
            try {
                $validateFile = $this->validateFile();
                if ($validateFile) {
                    $csvData = $this->processCsvToArray();
                    if (is_array($csvData)) {
                        $response = $this->orderCreate->createOrderForAmazon(
                            $csvData,
                            $this->_params
                        );
                        $data = [
                            'status' => true,
                            'message' => 'Order Created Successfully',
                        ];
                    } else {
                        $data = [
                            'status' => false,
                            'message' => $this->_error,
                        ];
                    }


                } else {
                    $data = [
                        'status' => false,
                        'message' => 'Invalid File Format'
                    ];
                }
            } catch (Exception $e) {
                $data = [
                    'status' => false,
                    'message' => $e->getMessage()
                ];
            }

        } else {
            $data = [
                'status' => false,
                'message' => 'Invalid File'
            ];
        }
        $resultJson->setData($data);

        return $resultJson;
    }

    /**
     * Validate File Template
     */
    public function validateFile()
    {
        $mimesArray = $this->createOrderBlock->getMimesTypes();
        $FILE = $this->_FILE;
        if (!isset($FILE['tmp_name'])) {
            $response = false;
        } elseif (!in_array($FILE['type'], $mimesArray)) {
            $response = false;
        } else {
            $response = true;
        }
        return $response;
    }

    /**
     * Return Processed Array
     *
     * @return array|string
     * @throws Exception
     */
    public function processCsvToArray()
    {
        $FILE = $this->_FILE;
        $rawData = $this->_csv->getData($FILE['tmp_name']);
        $processedData = $error = [];
        $count = 1;
        foreach ($rawData as $row) {
            $data = [];
            if ($count != 1) {

                /**
                 * Check for SKU existence
                 */
                $sku = $row[10];
                if (!$this->productModel->getIdBySku($sku)) {
                    $error[] = "Product with SKU " . $sku . " doesn't exist";
                    continue;
                }
                $partnerOrderId = $row[0];
                $partnerPo = $row[1];

                $data['order']['partner_order_id'] = $partnerOrderId;
                $data['order']['order_date'] = date("Y-m-d", strtotime($row[4]));

                $data['item']['partner_po'] = $partnerPo;
                $delivery_date = date("Y-m-d", strtotime($row[5]));
                $data['item']['shipping_date'] = date('Y-m-d', (strtotime('-2 day', strtotime($delivery_date))));
                $data['item']['delivery_date'] = $delivery_date;
                /**
                 * Billing Address
                 */
                $nameArray = explode(" ", $row[8]);
                $b_firstName = $b_middleName = $b_lastName = '';
                if (count($nameArray) == 3) {
                    $b_firstName = $nameArray[0];
                    $b_middleName = $nameArray[1];
                    $b_lastName = $nameArray[2];
                } elseif (count($nameArray) == 2) {
                    $b_firstName = $nameArray[0];
                    $b_lastName = $nameArray[1];
                } else {
                    $b_firstName = $nameArray[0];
                }

                $billingAddress = [
                    'customer_address_id' => '',
                    'prefix' => '',
                    'firstname' => $b_firstName,
                    'middlename' => $b_middleName,
                    'lastname' => $b_lastName,
                    'country_id' => 'US',
                    'postcode' => '',
                    'telephone' => '12345'//$row[9]
                ];

                /**
                 * Set Shipping Address
                 */
                $nameArray = explode(" ", $row[16]);
                /*echo '<pre>';
                print_r($nameArray);
                die;*/
                $s_firstName = $s_middleName = $s_lastName = '';
                if (count($nameArray) >= 3) {
                    $s_firstName = $nameArray[0];
                    $s_middleName = $nameArray[1];
                    $s_lastName = $nameArray[2];
                } elseif (count($nameArray) == 2) {
                    $s_firstName = $nameArray[0];
                    $s_lastName = $nameArray[1];
                } else {
                    $s_firstName = $nameArray[0];
                }

                $shippingAddress = [
                    'customer_address_id' => '',
                    'prefix' => '',
                    'firstname' => $s_firstName,
                    'middlename' => $s_middleName,
                    'lastname' => $s_lastName,
                    'street' => [
                        '0' => $row[17],
                        '1' => $row[18]
                    ],
                    'city' => $row[20],
                    'country_id' => 'US',
                    'region' => $row[21],
                    'postcode' => $row[22],
                    'telephone' => '12345' // $row[9]
                ];

                $data['billing_address'] = $shippingAddress;
                $data['shipping_address'] = $shippingAddress;
                $data['item']['sku'] = $sku;
                $data['item']['qty'] = $row[12];

                $processedData[$partnerOrderId][] = $data;

            } else {
                $count = 99;
            }
        }
        if (count($error) > 0) {
            $errorString = implode("<br>", $error);
            $this->_error = $errorString;
            return 'fail';
        }
        return $processedData;
    }
}
