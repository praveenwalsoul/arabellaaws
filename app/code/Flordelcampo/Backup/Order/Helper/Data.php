<?php

namespace Flordelcampo\Order\Helper;

use Magento\Customer\Model\Customer;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 * Flordelcampo\Order\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Wal Mart Store Code
     * @var string
     */
    private $_walMartStoreCode = 'walmart';
    /**
     * Amazon Store Code
     * @var string
     */
    private $_amazonStoreCode = 'amazon';
    /**
     * Customer Object
     *
     * @var Customer
     */
    protected $customer;
    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Data constructor.
     *
     * @param Context $context Context
     * @param Customer $customer Customer
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        Customer $customer,
        StoreRepositoryInterface $storeRepository,
        StoreManagerInterface $storeManager
    )
    {
        $this->customer = $customer;
        $this->storeRepository = $storeRepository;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Return Customer Address Data
     *
     * @param integer $customerId Customer Id
     *
     * @return DataObject[]
     */
    public function getCustomerAddress($customerId)
    {
        $customerModel = $this->customer->load($customerId);
        return $customerModel->getAddresses();
    }

    /**
     * Return All stores
     *
     * @return StoreInterface[]
     */
    public function getAllStoreList()
    {
        return $this->storeRepository->getList();
    }

    /**
     * Return Store Id and array
     *
     * @return array
     */
    public function getStoreListArray()
    {
        $storesArray = [];
        $storesList = $this->getAllStoreList();
        foreach ($storesList as $store) {
            if ($store->getIsActive()) {
                $storesArray[$store->getId()] = $store->getCode();
            }
        }
        return $storesArray;
    }

    /**
     * Get Store By Store Id
     * @param $Id
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getStoreById($Id)
    {
        return $this->storeRepository->getById($Id);
    }

    /**
     * Return Store By Store Code
     * @param $code
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getStoreByCode($code)
    {
        return $this->storeRepository->get($code);
    }

    /**
     * Return Amazon Store
     *
     * @return StoreInterface|string
     */
    public function getAmazonStore()
    {
        try {
            return $this->getStoreByCode($this->_amazonStoreCode);
        } catch (\Exception $e) {
            return '1';
        }
    }

    /**
     * Return Wal Mart Store
     *
     * @return StoreInterface|string
     */
    public function getWalMartStore()
    {
        try {
            return $this->getStoreByCode($this->_walMartStoreCode);
        } catch (\Exception $e) {
            return '1';
        }
    }
}
