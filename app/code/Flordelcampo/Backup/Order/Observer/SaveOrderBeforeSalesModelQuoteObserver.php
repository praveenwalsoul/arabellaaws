<?php

namespace Flordelcampo\Order\Observer;

use Magento\Framework\DataObject\Copy;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

/**
 * Class SaveOrderBeforeSalesModelQuoteObserver
 *
 * @package Flordelcampo\Order\Observer
 */
class SaveOrderBeforeSalesModelQuoteObserver implements ObserverInterface
{

    /**
     * Quote Items
     *
     * @var array
     */
    private $_quoteItems = [];
    /**
     * Quote Object
     *
     * @var null
     */
    private $_quote;

    /**
     * Order Object
     *
     * @var null
     */
    private $_order;


    /**
     * Attribute List
     *
     * @var array
     */
    private $_quoteAttributes = [
        'order_type',
        'customer_sales_rep',
        'partner_order_id'
    ];

    private $_quoteItemAttributes = [
        'qty_per_box',
        'farm_price',
        'total_price',
        'po_unit_price',
        'po_price',
        'vendor',
        'delivery_date',
        'pickup_date',
        'truck_pickup_date',
        'customer_sales_rep',
        'vendor_farm',
        'partner_po',
        'track_id'

    ];

    /**
     * Convert Quote to Order
     *
     * @param Observer $observer Observer
     *
     * @return SaveOrderBeforeSalesModelQuoteObserver
     */
    public function execute(Observer $observer)
    {
        /* @var Order $order */
        $order = $observer->getEvent()->getData('order');
        /* @var Quote $quote */
        $quote = $observer->getEvent()->getData('quote');

        $this->_order = $order;
        $this->_quote = $quote;
        /**
         *  Add Quote Attributes to Order
         */
        foreach ($this->_quoteAttributes as $attribute) {
            if (empty($this->_order->hasData($attribute))) {
                $order->setData(
                    $attribute, $this->_quote->getData($attribute)
                );
            }

        }


        /**
         * Add Quote Item to Order Item
         */

        foreach ($this->_quoteItemAttributes as $attribute) {
            foreach ($this->_order->getAllItems() as $orderItem) {
                if (empty($orderItem->hasData($attribute))) {
                    $quoteItem = $this->getQuoteItemById(
                        $orderItem->getQuoteItemId()
                    );
                    $orderItem->setData($attribute, $quoteItem->getData($attribute));
                }
            }
        }

        return $this;
    }

    /**
     * Return Quote Item Object By Order Item Quote Item ID
     *
     * @param integer $orderQuoteItemID Order Quote
     *
     * @return mixed|object
     */
    public function getQuoteItemById($orderQuoteItemID)
    {
        if (empty($this->_quoteItems)) {
            foreach ($this->_quote->getAllItems() as $item) {
                $this->_quoteItems[$item->getId()] = $item;
            }
        }
        if (array_key_exists($orderQuoteItemID, $this->_quoteItems)) {
            return $this->_quoteItems[$orderQuoteItemID];
        }

        return null;
    }
}

