<?php

namespace Flordelcampo\Order\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\Order\Model\ResourceModel\Box as ResourceBoxModel;

/**
 * Class Box
 *
 * @package Flordelcampo\Order\Model
 */
class Box extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceBoxModel::class);
    }

}