<?php

namespace Flordelcampo\Order\Model;

use Dotdigitalgroup\Email\Helper\File;
use Exception;
use Flordelcampo\Customer\Helper\Data;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\AddressFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\File\Csv;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteManagement;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Order\Block\CreateOrder;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Magento\Sales\Model\Order as OrderModel;

/**
 * Class Customer
 *
 * Flordelcampo\Order\Model
 */
class Order
{
    /**
     * CSV
     *
     * @var Csv
     */
    private $_csv;
    /**
     * Store Manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * Customer Factory
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * Product Repository
     *
     * @var ProductRepositoryInterface
     */
    private $_productRepositoryInterface;
    /**
     * Quote Factory
     *
     * @var QuoteFactory
     */
    protected $quote;
    /**
     * Cart Repository
     *
     * @var CartRepositoryInterface
     */
    protected $cartRepository;
    /**
     * Product Repository
     *
     * @var ProductRepository
     */
    private $_productRepository;
    /**
     * Address Factory
     *
     * @var AddressFactory
     */
    protected $addressFactory;
    /**
     * Address Repository
     *
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;
    /**
     * Quote Factory
     *
     * @var QuoteRepository
     */
    protected $quoteRepository;
    /**
     * Order Management
     *
     * @var QuoteManagement
     */
    protected $orderManagement;
    /**
     * Customer Helper
     *
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var CreateOrder
     */
    protected $createOrderBlock;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var OrderModel
     */
    protected $orderModel;
    /**
     * @var OrderModel
     */

    /**
     * Order constructor.
     *
     * @param Csv $csv CSV Processor
     * @param StoreManagerInterface $storeManager Store
     * @param CustomerRepositoryInterface $customerRepository
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param QuoteFactory $quote
     * @param CartRepositoryInterface $cartRepository
     * @param ProductRepository $productRepository
     * @param AddressFactory $addressFactory
     * @param AddressRepositoryInterface $addressRepository
     * @param QuoteRepository $quoteRepository
     * @param QuoteManagement $orderManagement
     * @param CustomerHelper $customerHelper
     * @param CreateOrder $createOrderBlock
     * @param OrderHelper $orderHelper
     * @param OrderModel $orderModel
     * @param OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Model\ResourceModel\Order $orderResourceModel
     */
    public function __construct(
        Csv $csv,
        StoreManagerInterface $storeManager,
        CustomerRepositoryInterface $customerRepository,
        ProductRepositoryInterface $productRepositoryInterface,
        QuoteFactory $quote,
        CartRepositoryInterface $cartRepository,
        ProductRepository $productRepository,
        AddressFactory $addressFactory,
        AddressRepositoryInterface $addressRepository,
        QuoteRepository $quoteRepository,
        QuoteManagement $orderManagement,
        CustomerHelper $customerHelper,
        CreateOrder $createOrderBlock,
        OrderHelper $orderHelper,
        OrderModel $orderModel
    ) {
        $this->_csv = $csv;
        $this->storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->quote = $quote;
        $this->cartRepository = $cartRepository;
        $this->_productRepository = $productRepository;
        $this->addressFactory = $addressFactory;
        $this->addressRepository = $addressRepository;
        $this->quoteRepository = $quoteRepository;
        $this->orderManagement = $orderManagement;
        $this->customerHelper = $customerHelper;
        $this->orderHelper = $orderHelper;
        $this->orderModel = $orderModel;
        $this->createOrderBlock = $createOrderBlock;
    }

    /**
     * Place order for Amazon
     *
     * @param $csvData
     * @param $params
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function createOrderForAmazon($csvData, $params)
    {
        $this->createOrderBlock->getBackendSession()->setSource('backend');
        $skuOriginalPriceUpdateArray = [];
        $customerId = $params['customer_id'];
        /**
         * Store id is store code id
         */
        $storeCode = $params['store_code'];
        $store = $this->orderHelper->getStoreByCode($storeCode);

        foreach ($csvData as $partnerOrderId => $orderItemData) {
            $quote = $this->getCustomerQuote($customerId);
            $customerSalesRep = $this->getCustomerSalesRep($customerId);

            foreach ($orderItemData as $item) {
                $sku = $item['item']['sku'];
                $product = $this->_productRepository->get($sku);
                $price = $product->getPrice();
                $qty = $item['item']['qty'];
                $skuCurrentPrice = $product->getPrice();
                if (!array_key_exists($sku, $skuOriginalPriceUpdateArray)) {
                    $skuOriginalPriceUpdateArray[$sku] = $skuCurrentPrice;
                }
                $product->setPrice($price)->save();
                $quote->addProduct($product, (int)$qty);
                $quote->setOrderType('');
                $quote->setPartnerOrderId($partnerOrderId);
                $quote->setStore($store);
                $quote->save();
                /**
                 * Save Quote
                 */
                $quoteItem = $this->getLatestQuoteItem($quote);
                $quoteItem->setQtyPerBox('1');
                $quoteItem->setFarmPrice($price);
                $quoteItem->setTotalPrice($price);
                $quoteItem->setPoUnitPrice($price);
                $quoteItem->setVendor('');
                $quoteItem->setVendorFarm('');
                $quoteItem->setDeliveryDate($item['item']['delivery_date']);
                $quoteItem->setPickupDate($item['item']['shipping_date']);
                $quoteItem->setTruckPickupDate($item['item']['delivery_date']);
                $quoteItem->setPartnerPo($item['item']['partner_po']);
                $quoteItem->setCustomerSalesRep($customerSalesRep);
                $quoteItem->save();
            }
            $quote = $this->getQuoteByQuoteId($quote->getId());
            $billingAddressArray = $orderItemData[0]['billing_address'];
            $shippingAddressArray = $orderItemData[0]['shipping_address'];
            $quote->getBillingAddress()->addData($billingAddressArray);
            $quote->getShippingAddress()->addData($shippingAddressArray);
            $quote->setCustomerSalesRep($customerSalesRep);
            /**
             * Set Shipping Method | flatrate_flatrate
             */
            $shippingAddress = $quote->getShippingAddress();
            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates()
                ->setShippingMethod('freeshipping_freeshipping');
            /**
             * Add Payment Method | checkmo
             */
            $quote->setPaymentMethod('cashondelivery');
            $quote->setInventoryProcessed(false);
            $quote->save();
            /**
             * Set Sales Order Payment
             */
            $quote->getPayment()->importData(['method' => 'checkmo']);
            $quote->collectTotals()->save();

            /**
             *  Order Creation
             */
            $order = $this->createQuoteOrder($quote);
            $order->setEmailSent(0);

        }
        $this->updateSkuOriginalPrice($skuOriginalPriceUpdateArray);
        return true;
    }

    /**
     * Place order for wal mart
     *
     * @param $csvData
     * @param $params
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function createOrderForWalMart($csvData, $params)
    {
        $this->createOrderBlock->getBackendSession()->setSource('backend');
        $skuOriginalPriceUpdateArray = [];
        $customerId = $params['customer_id'];
        /**
         * Store id is store code id
         */
        $storeCode = $params['store_code'];
        $store = $this->orderHelper->getStoreByCode($storeCode);
        foreach ($csvData as $partnerOrderId => $orderItemData) {
            $quote = $this->getCustomerQuote($customerId);
            $customerSalesRep = $this->getCustomerSalesRep($customerId);

            foreach ($orderItemData as $item) {
                $sku = $item['item']['sku'];
                $price = $item['item']['item_price'];
                $qty = $item['item']['qty'];
                $product = $this->_productRepository->get($sku);
                $skuCurrentPrice = $product->getPrice();
                if (!array_key_exists($sku, $skuOriginalPriceUpdateArray)) {
                    $skuOriginalPriceUpdateArray[$sku] = $skuCurrentPrice;
                }
                $product->setPrice($price)->save();
                $quote->addProduct($product, (int)$qty);
                $quote->setOrderType('');
                $quote->setPartnerOrderId($partnerOrderId);
                $quote->setStore($store);
                $quote->save();
                /**
                 * Save Quote
                 */
                $quoteItem = $this->getLatestQuoteItem($quote);
                $quoteItem->setQtyPerBox('1');
                $quoteItem->setFarmPrice($price);
                $quoteItem->setTotalPrice($price);
                $quoteItem->setPoUnitPrice($price);
                $quoteItem->setVendor('');
                $quoteItem->setVendorFarm('');
                $quoteItem->setDeliveryDate($item['item']['delivery_date']);
                $quoteItem->setPickupDate($item['item']['shipping_date']);
                $quoteItem->setTruckPickupDate($item['item']['delivery_date']);
                $quoteItem->setPartnerPo($item['item']['partner_po']);
                $quoteItem->setTrackId($item['item']['track_number']);
                $quoteItem->setCustomerSalesRep($customerSalesRep);
                $quoteItem->save();
            }
            $quote = $this->getQuoteByQuoteId($quote->getId());
            $addressArray = $orderItemData[0]['address'];
            $quote->getBillingAddress()->addData($addressArray);
            $quote->getShippingAddress()->addData($addressArray);
            $quote->setCustomerSalesRep($customerSalesRep);
            /**
             * Set Shipping Method | flatrate_flatrate
             */
            $shippingAddress = $quote->getShippingAddress();
            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates()
                ->setShippingMethod('freeshipping_freeshipping');
            /**
             * Add Payment Method | checkmo
             */
            $quote->setPaymentMethod('cashondelivery');
            $quote->setInventoryProcessed(false);
            $quote->save();
            /**
             * Set Sales Order Payment
             */
            $quote->getPayment()->importData(['method' => 'checkmo']);
            $quote->collectTotals()->save();

            /**
             *  Order Creation
             */
            $order = $this->createQuoteOrder($quote);
            $order->setEmailSent(0);

            /**
             * Set Tax and Shipping Cost
             */
            $shippingCost = $orderItemData[0]['order']['ship_price'];
            $tax = $orderItemData[0]['order']['tax'];

            $orderId = $order->getRealOrderId();
            $orderInfo = $this->orderModel->loadByIncrementId($orderId);
            $orderInfo->setShippingAmount($shippingCost);
            $orderInfo->setBaseShippingAmount($tax);
            $orderInfo->setTaxAmount($tax);
            $orderInfo->setGrandTotal($orderInfo->getSubtotal() + $tax + $shippingCost);
            $orderInfo->setBaseGrandTotal($orderInfo->getSubtotal() + $tax + $shippingCost);
            $orderInfo->save();

        }
        $this->updateSkuOriginalPrice($skuOriginalPriceUpdateArray);
        return true;
    }

    /**
     * Process File
     *
     * @param File $FILE File
     * @param array $params Params
     *
     * @return int
     * @throws LocalizedException
     * @throws Exception
     */
    public function process($FILE, $params)
    {
        $mimesArray = $this->getMimesTypes();
        if (!isset($FILE['tmp_name'])) {
            throw new LocalizedException(__('Invalid file upload attempt.'));
        } elseif (!in_array($FILE['type'], $mimesArray)) {
            throw new LocalizedException(
                __('Invalid file type. Only CSV files are allowed')
            );
        } else {
            return $this->createOrderForCsv($FILE, $params);
        }
    }

    /**
     * Place Order for Flordel Orders
     *
     * @param $orderFormat
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function createOrderForFlordelCsv($orderFormat)
    {
        return $this->place($orderFormat);
    }

    /**
     * Place Function
     *
     * @param $customerRawData
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function place($customerRawData)
    {
        $this->createOrderBlock->getBackendSession()->setSource('backend');
        $skuOriginalPriceUpdateArray = [];
        foreach ($customerRawData as $customerId => $storeData) {
            foreach ($storeData as $storeId => $itemData) {
                $quote = $this->getCustomerQuote($customerId);
                $customerSalesRep = $this->getCustomerSalesRep($customerId);
                foreach ($itemData as $item) {
                    $sku = $item['sku'];
                    /*  Custom Price Calculation Qtp * Price Per Stem  */
                    $price = round(
                        $item['so_price'] * $item['qty_per_box'],
                        4
                    );
                    $product = $this->_productRepository->get($sku);
                    $skuCurrentPrice = $product->getPrice();
                    if (!array_key_exists($sku, $skuOriginalPriceUpdateArray)) {
                        $skuOriginalPriceUpdateArray[$sku] = $skuCurrentPrice;
                    }
                    $product->setPrice($price)->save();
                    $quote->addProduct($product, intval($item['qty']));
                    $quote->setOrderType($item['order_type']);
                    $quote->save();

                    /**
                     * $quote->save();
                     * $quote->collectTotals()->save();
                     */

                    /**
                     * Get Quote Item Object to update Product Custom Options
                     */
                    /*$quoteItem = $this->getCurrentQuoteItemByProduct(
                        $quote,
                        $product
                    );*/
                    $quoteItem = $this->getLatestQuoteItem($quote);

                    $quoteItem->setQtyPerBox($item['qty_per_box']);
                    $quoteItem->setFarmPrice($item['so_price']);
                    $quoteItem->setTotalPrice($item['so_price']);
                    $quoteItem->setPoUnitPrice($item['po_price']);
                    $quoteItem->setVendor($item['vendor']);
                    $quoteItem->setVendorFarm($item['vendor_farm']);
                    $quoteItem->setDeliveryDate($item['delivery_date']);
                    $quoteItem->setPickupDate($item['shipping_date']);
                    $quoteItem->setTruckPickupDate($item['trans_date']);
                    $quoteItem->setCustomerSalesRep($customerSalesRep);

                    $quoteItem->save();

                }

                /**
                 * Get Quote ID to update Shipping Address
                 */
                $quote = $this->getQuoteByQuoteId($quote->getId());
                $quote = $this->setShippingAddressToQuote($quote, $storeId);

                /**
                 * Add Customer Sales Rep
                 */
                $quote->setCustomerSalesRep($customerSalesRep);

                /**
                 * Set Shipping Method | flatrate_flatrate
                 */
                $shippingAddress = $quote->getShippingAddress();
                $shippingAddress->setCollectShippingRates(true)
                    ->collectShippingRates()
                    ->setShippingMethod('freeshipping_freeshipping');
                /**
                 * Add Payment Method | checkmo
                 */
                $quote->setPaymentMethod('cashondelivery');
                $quote->setInventoryProcessed(false);
                $quote->save();
                /**
                 * Set Sales Order Payment
                 */
                $quote->getPayment()->importData(['method' => 'checkmo']);
                $quote->collectTotals()->save();

                /**
                 *  Order Creation
                 */
                $order = $this->createQuoteOrder($quote);
                $order->setEmailSent(0);

            }

        }
        $this->updateSkuOriginalPrice($skuOriginalPriceUpdateArray);
        return true;
    }

    /**
     * Update Product Old prices
     *
     * @param $skuOriginalPriceUpdateArray
     * @return bool
     * @throws NoSuchEntityException
     */
    public function updateSkuOriginalPrice($skuOriginalPriceUpdateArray)
    {
        /**
         * Set SKU Old price
         */
        if (count($skuOriginalPriceUpdateArray) > 0) {
            foreach ($skuOriginalPriceUpdateArray as $sku => $oldPrice) {
                $product = $this->_productRepository->get($sku);
                $product->setPrice($oldPrice)->save();
            }
        }
        return true;
    }

    /**
     * Return Order Data
     * This will create order with template store Id and Customer ID
     *
     * @param File $file File
     * @param array $params Params
     *
     * @return array|String
     * @throws Exception
     */
    public function createOrderForCsv($file, $params)
    {
        $customerRawData = $this->processCSV($file);
        $this->place($customerRawData);
        return true;
    }

    /**
     * Return Customer Quote
     *
     * @param integer $customerId Customer
     *
     * @return CartInterface|Quote
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerQuote($customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        $quote = $this->quote->create()->loadByCustomer($customerId);
        if (!$quote->getId()) {
            $quote = $this->quote->create()
                ->setStoreId($this->getStore()->getId())
                ->assignCustomer($customer);
        }
        return $quote;
    }

    /**
     * Return Sales Rep Associate with customer
     *
     * @param integer $customerId Customer ID
     *
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerSalesRep($customerId)
    {
        return $this->customerHelper->getCustomerSalesRep($customerId);
    }

    /**
     * Add Shipping Address To Quote
     *
     * @param object $quote Quote    Object
     * @param integer $shippingAddressID Shipping Address ID
     *
     * @return object
     *
     * @throws LocalizedException
     */
    public function setShippingAddressToQuote($quote, $shippingAddressID)
    {
        /**
         * Add Shipping Address from Shipping Address ID (store ID)
         */
        $addressObject = $this->addressRepository->getById($shippingAddressID);
        $quote->getShippingAddress()->setCustomerAddressId(
            $addressObject->getId()
        );
        $quote->getShippingAddress()->setPrefix($addressObject->getPrefix());
        $quote->getShippingAddress()->setFirstname(
            $addressObject->getFirstname()
        );
        $quote->getShippingAddress()->setMiddlename(
            $addressObject->getMiddlename()
        );
        $quote->getShippingAddress()->setLastname(
            $addressObject->getLastname()
        );
        $quote->getShippingAddress()->setSuffix(
            $addressObject->getSuffix()
        );
        $quote->getShippingAddress()->setCompany(
            $addressObject->getCompany()
        );
        $quote->getShippingAddress()->setStreet(
            $addressObject->getStreet()
        );
        $quote->getShippingAddress()->setCity(
            $addressObject->getCity()
        );
        $quote->getShippingAddress()->setCountryId(
            $addressObject->getCountryId()
        );
        $quote->getShippingAddress()->setRegion(
            $addressObject->getRegion()
        );
        $quote->getShippingAddress()->setRegionId(
            $addressObject->getRegionId()
        );
        $quote->getShippingAddress()->setPostcode(
            $addressObject->getPostcode()
        );
        $quote->getShippingAddress()->setTelephone(
            $addressObject->getTelephone()
        );
        $quote->getShippingAddress()->setFax(
            $addressObject->getFax()
        );

        return $quote;
    }

    /**
     * Return Quote Object By Quote ID
     *
     * @param integer $quoteId Quote
     *
     * @return CartInterface|Quote
     * @throws NoSuchEntityException
     */
    public function getQuoteByQuoteId($quoteId)
    {
        return $this->quoteRepository->get($quoteId);
    }

    /**
     * Return Latest Quote Item Object By id
     * @param $quote
     *
     * @return mixed
     */
    public function getLatestQuoteItem($quote)
    {
        $items = $quote->getAllItems();
        $maxId = 0;
        foreach ($items as $item) {
            if ($item->getId() > $maxId) {
                $maxId = $item->getId();
            }
        }
        $lastItemId = $maxId;
        return $quote->getItemById($lastItemId);
    }

    /**
     * Return Current Quote Item Object By Product
     *
     * @param object $quote Quote
     * @param object $product Product Object
     *
     * @return mixed|object
     */
    public function getCurrentQuoteItemByProduct($quote, $product)
    {

        return $quote->getItemByProduct($product);
    }

    /**
     * Create Order For Quote
     *
     * @param object $quote Quote
     *
     * @return mixed|object
     * @throws LocalizedException
     */
    public function createQuoteOrder($quote)
    {
        return $this->orderManagement->submit($quote);
    }

    /**
     * File Types
     *
     * @return array
     */
    public function getMimesTypes()
    {
        return [
            'application/vnd.ms-excel',
            'text/csv',
            'text/tsv'
        ];

    }

    /**
     * Returns CSV Array by customer wise
     *
     * @param File $file File
     *
     * @return array
     * @throws Exception
     */
    public function processCSV($file)
    {
        $rawData = $this->_csv->getData($file['tmp_name']);
        $headers = $this->getHeaders();
        $processedData = [];
        $count = 1;
        /*echo '<pre>'; print_r($rawData); die;*/
        foreach ($rawData as $row) {
            if ($count != 1) {
                $customerId = $row[0];
                $storeId = $row[1];
                $processedData[$customerId][$storeId][] = array_combine(
                    $headers, $row
                );
            } else {
                $count++;
            }

        }
        return $processedData;
    }

    /**
     * Return Header Fields
     *
     * @return array
     */
    public function getHeaders()
    {
        return [
            'customer_id',
            'store_id',
            'sku',
            'qty_per_box',
            'qty',
            'so_price',
            'po_price',
            'vendor',
            'delivery_date',
            'shipping_date',
            'trans_date',
            'order_type'
        ];
    }

    /**
     * Return Store Object
     *
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }

    /**
     * Return Website Id
     *
     * @return int
     * @throws NoSuchEntityException
     */
    public function getWebSiteId()
    {
        return $this->storeManager->getStore()->getWebsiteId();
    }
}