<?php

namespace Flordelcampo\Order\Block;

use Magento\Backend\Block\Template;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Backend\Block\Template\Context;
use Flordelcampo\Customer\Helper\Data;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Magento\Store\Api\Data\StoreInterface;

/**
 * Class Amazon
 *
 * Flordelcampo\Order\Block
 */
class Amazon extends Template
{
    /**
     * Wal Mart Customer
     * @var string
     */
    private $_amazonCustomerEmail = "amazon@arabellabouqets.com";
    /**
     * @var Data
     */
    protected $customerHelper;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * Walmart constructor.
     *
     * @param Context $context
     * @param Data $customerHelper
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        Context $context,
        Data $customerHelper,
        OrderHelper $orderHelper
    ) {
        $this->customerHelper = $customerHelper;
        $this->orderHelper = $orderHelper;
        parent::__construct($context);
    }

    /**
     * Return Check Data URL
     *
     * @return string
     */
    public function getAmazonCreateUrl()
    {
        return $this->getUrl('create/amazon/create');
    }

    /**
     * Return Amazon Customer
     * @return Customer
     */
    public function getAmazonCustomerId()
    {
        return $this->customerHelper->getCustomerByEmail($this->_amazonCustomerEmail)->getId();
    }

    /**
     * Return Amazon Store
     *
     * @return StoreInterface|string
     */
    public function getAmazonStoreCode()
    {
        return $this->orderHelper->getAmazonStore()->getCode();
    }

    /**
     * Return Amazon Store ID
     * @return int
     */
    public function getAmazonStoreId()
    {
        return $this->orderHelper->getAmazonStore()->getId();
    }
}