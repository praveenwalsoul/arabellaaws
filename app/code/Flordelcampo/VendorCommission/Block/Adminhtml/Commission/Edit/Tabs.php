<?php
namespace Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('commission_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Commission Information'));
    }
}