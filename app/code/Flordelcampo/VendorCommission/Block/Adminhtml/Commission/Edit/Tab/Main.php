<?php

namespace Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Edit\Tab;

/**
 * Commission edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Flordelcampo\VendorCommission\Model\Status
     */
    protected $_status;
    //protected $customerGroup;
    protected $vendorName;
    protected $categorylist;
    

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        //\Flordelcampo\VendorCommission\Model\Config\Source\CustomerGroup $customerGroup,
        \Flordelcampo\VendorCommission\Model\Config\Source\VendorName $vendorName,
        \Flordelcampo\VendorCommission\Model\Config\Source\Categorylist $categorylist,
        \Flordelcampo\VendorCommission\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        //$this->customerGroup = $customerGroup;
        $this->vendorName = $vendorName;
        $this->categorylist = $categorylist;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Flordelcampo\VendorCommission\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('commission');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('commission_id', 'hidden', ['name' => 'commission_id']);
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        // get customer group collection
        $customerGroupsCollection = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();

                /*echo "<pre>";
                print_r($customerGroupsCollection);
                echo "</pre>";*/

       // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();

        //$cusGroup = $this->_customerGroupColl->toOptionArray();
       // print_r($cusGroup);
        $vName = $this->vendorName->toOptionArray();
        $Category = $this->categorylist->toOptionArray();

		
        $fieldset->addField(
            'vendor',
            'multiselect',
            [
                'label' => __('Vendor'),
                'title' => __('Vendor'),
                'name' => 'vendor',
                'values' => $vName
				
                //'options' => \Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Grid::getValueArray0(),
               // 'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'product_category',
            'multiselect',
            [
                'label' => __('Product Category'),
                'title' => __('Product Category'),
                'name' => 'product_category',
                'values' => $Category
                
                //'options' => \Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Grid::getValueArray0(),
               // 'disabled' => $isElementDisabled
            ]
        );
						
        $fieldset->addField(
            'customer_group',
            'multiselect',
            [
                'label' => __('Customer Group'),
                'title' => __('Customer Group'),
                'name' => 'customer_group',
                'values' => $customerGroupsCollection
				
                //'options' => \Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Grid::getValueArray1(),
                //'disabled' => $isElementDisabled
            ]
        );
						
        $fieldset->addField(
            'rate',
            'text',
            [
                'name' => 'rate',
                'label' => __('Rate'),
                'title' => __('Rate'),
				
                'disabled' => $isElementDisabled
            ]
        );
					

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
    

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
