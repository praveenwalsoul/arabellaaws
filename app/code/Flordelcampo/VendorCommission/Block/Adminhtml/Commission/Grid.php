<?php
namespace Flordelcampo\VendorCommission\Block\Adminhtml\Commission;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\VendorCommission\Model\commissionFactory
     */
    protected $_commissionFactory;

    /**
     * @var \Flordelcampo\VendorCommission\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\VendorCommission\Model\commissionFactory $commissionFactory
     * @param \Flordelcampo\VendorCommission\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\VendorCommission\Model\CommissionFactory $CommissionFactory,
        \Flordelcampo\VendorCommission\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_commissionFactory = $CommissionFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('commission_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_commissionFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */

    protected function _prepareColumns()
    {
        $this->addColumn(
            'commission_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'commission_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn(
                    'customer_group',
                    [
                        'header' => __('Customer group'),
                        'index' => 'customer_group',
                        //'type'=> 'options',
                        'options'=>$this->getOptionArray0(),
                        'renderer' => \Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Renderer\CustomerGroup::class
                    ]
                );
        $this->addColumn(
                    'product_category',
                    [
                        'header' => __('Product Category'),
                        'index' => 'product_category',
                        'type'=> 'options',
                        'options'=>$this->getOptionArray0(),
                        'renderer' => \Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Renderer\CategoryProduct::class
                    ]
                );


        $this->addColumn(
                    'vendor',
                    [
                        'header' => __('vendor'),
                        'index' => 'vendor',
                    ]
                );


		
				$this->addColumn(
					'rate',
					[
						'header' => __('Rate'),
						'index' => 'rate',
					]
				);
				


		
        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit'
                        ],
                        'field' => 'commission_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );
		

		
		   $this->addExportType($this->getUrl('vendorcommission/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('vendorcommission/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('commission_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_VendorCommission::commission/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('commission');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('vendorcommission/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('vendorcommission/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('vendorcommission/*/index', ['_current' => true]);
    }

    /**
     * @param \Flordelcampo\VendorCommission\Model\commission|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'vendorcommission/*/edit',
            ['commission_id' => $row->getId()]
        );
		
    }

	
		static public function getOptionArray0()
		{
            $data_array=array(); 
			$data_array[0]='V1';
			$data_array[1]='V2';
            return($data_array);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		
		static public function getOptionArray1()
		{

            $data_array=array(); 
            $data_array[0]='customer group';
            $data_array[1]='customer group2';
            return($data_array);
            
		}
		static public function getValueArray1()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Grid::getOptionArray1() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		

}