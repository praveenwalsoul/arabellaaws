<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Renderer;


class CategoryProduct extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    protected $categoryCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param \Magento\Sales\Helper\Reorder $salesReorder
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) {
       
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $productAttribute = $row->getData('product_category');
        if (!empty($productAttribute)) {
            $attributeExploded = explode(',', $productAttribute); 
            $html = '';
            $numbers = [];
            if(count($attributeExploded) > 0){
                $collection = $this->categoryCollectionFactory->create()
                                    ->addAttributeToSelect(['name'])
                                    ->addFieldToFilter('entity_id', ['in' => $attributeExploded]);

                foreach($collection as $item){
                    $numbers[] = $item->getName();
                }
            }

            return implode(",\n",$numbers);

        }

        return;
    }

}
