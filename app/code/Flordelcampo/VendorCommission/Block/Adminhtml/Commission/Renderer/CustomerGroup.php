<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Flordelcampo\VendorCommission\Block\Adminhtml\Commission\Renderer;


class CustomerGroup extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    protected $customerGroupCollection;

    protected $customerGroupCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param \Magento\Sales\Helper\Reorder $salesReorder
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $customerGroupCollectionFactory,
        array $data = []
    ) {
        $this->customerGroupCollectionFactory = $customerGroupCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $customerGroup = $row->getData('customer_group');
        if (!empty($customerGroup)) {
            $groupsExploded = explode(',', $customerGroup); 
            $html = '';
            $numbers = [];
            if(count($groupsExploded) > 0){
                $collection = $this->customerGroupCollectionFactory->create()
                                    ->addFieldToFilter('customer_group_id', ['in' => $groupsExploded]);

                foreach($collection as $item){
                    $numbers[] = $item->getCustomerGroupCode();
                }
            }

            return implode(",\n",$numbers);

        }

        return;
    }

}
