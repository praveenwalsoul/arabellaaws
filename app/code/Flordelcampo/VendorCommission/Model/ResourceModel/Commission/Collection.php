<?php

namespace Flordelcampo\VendorCommission\Model\ResourceModel\Commission;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorCommission\Model\Commission', 'Flordelcampo\VendorCommission\Model\ResourceModel\Commission');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>