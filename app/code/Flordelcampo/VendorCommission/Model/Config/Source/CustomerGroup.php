<?php
namespace Flordelcampo\VendorCommission\Model\Config\Source;

class CustomerGroup implements \Magento\Framework\Option\ArrayInterface
{
    protected $customerGroup;

    public function __construct(\Magento\Customer\Model\ResourceModel\Group\Collection $customerGroup) {
        $this->customerGroup = $customerGroup;
    }
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
       //echo"die";die("ruk");
     $collections = $this->customerGroup->getCollection();
    //$customerGroups = $this->_customerGroup->toOptionArray();

    $data_arrays=array(); 

    foreach($collections as $cg){
      $data_arrays[$cg['value']]=$cg['label'];
    }
    return($data_arrays); 

    }
    
    public function toArray()
    {
        return [];
    } 

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
    /*
    public function toOptionArray($value)
    {
        
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    } */
}