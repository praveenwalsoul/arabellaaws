<?php
namespace Flordelcampo\VendorCommission\Model\Config\Source;

class VendorName implements \Magento\Framework\Option\ArrayInterface
{
    protected $vendors;

    public function __construct(\Flordelcampo\VendorRegistration\Model\Vendor $vendorFactory) {
        $this->vendors = $vendorFactory;
    }
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
       
    $collections = $this->vendors->getCollection();

    $data = array();
    $i=0;
    foreach ($collections as $value) {
        $data[$i]['value'] = $value->getVendorName();//.' '.$value->getLastName();
        $data[$i]['label'] = $value->getVendorName(); //$value->getVendorId()
        $i++;
    }        
     $this->_options = $data;
   
    return $this->_options;
    }
    
    public function toArray()
    {
        return [];
    } 

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
    /*
    public function toOptionArray($value)
    {
        
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    } */
}