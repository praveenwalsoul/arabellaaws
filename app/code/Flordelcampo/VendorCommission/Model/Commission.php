<?php
namespace Flordelcampo\VendorCommission\Model;

class Commission extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorCommission\Model\ResourceModel\Commission');
    }
}
?>