<?php

namespace Flordelcampo\VendorInventory\Block\Adminhtml\Index;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Index
 * Flordelcampo\VendorInventory\Block\Adminhtml\Index
 */
class Index extends Template
{
    const VENDOR_TYPE_DC = 'flordel_vendor_configuration/general/vendor_type_dc';
    const VENDOR_TYPE_FARM = 'flordel_vendor_configuration/general/vendor_type_farm';

    private $filter = null;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    /**
     * @var string
     */
    private $vendorTypeForDc = '1';

    /**
     * @var string
     */
    private $vendorTypeForFarm = '2';
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * Index constructor.
     * @param Context $context
     * @param VendorHelper $vendorHelper
     * @param CatalogHelper $catalogHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        VendorHelper $vendorHelper,
        CatalogHelper $catalogHelper,
        array $data = []
    )
    {
        $this->vendorHelper = $vendorHelper;
        $this->catalogHelper = $catalogHelper;
        parent::__construct($context, $data);
    }

    /**
     * Return ID for filter
     * @return mixed
     */
    public function getVendorDcId()
    {
        return $this->vendorHelper->getConfig(self::VENDOR_TYPE_DC);
        /*return $this->vendorTypeForDc;*/
    }

    /**
     * Return ID for filter
     * @return mixed
     */
    public function getVendorFarmId()
    {
        return $this->vendorHelper->getConfig(self::VENDOR_TYPE_FARM);
        /*return $this->vendorTypeForFarm;*/
    }

    /**
     * Return All Active Vendors List for Dc
     * @return array
     */
    public function getAllActiveDcVendors()
    {
        $dcId = $this->getVendorDcId();
        return $this->vendorHelper->getAllVendorListArray($dcId);
    }

    /**
     * Return All Active Vendors List for Farm
     * @return array
     */
    public function getAllActiveFarmVendors()
    {
        $farmId = $this->getVendorFarmId();
        return $this->vendorHelper->getAllVendorListArray($farmId);
    }

    /**
     * Return Active Farms Array for DC
     * @return array
     */
    public function getAllActiveVendorFarmsForDc()
    {
        $dcId = $this->getVendorDcId();
        return $this->vendorHelper->getAllActiveFarmsForVendorType($dcId);
    }

    /**
     * Return Active Farms Array for Farm
     * @return array
     */
    public function getAllActiveVendorFarmsForFarm()
    {
        $farmId = $this->getVendorFarmId();
        return $this->vendorHelper->getAllActiveFarmsForVendorType($farmId);
    }

    /**
     * Return Attribute set array
     * @return array
     */
    public function getAllAttributeSetsArray()
    {
        return $this->vendorHelper->getAllAttributeSetData();
    }

    /**
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorInventoryCollection()
    {
        return $this->vendorHelper->getVendorInventoryOfferCollection();
    }

    public function getAllVendorFarmsArray()
    {
        return $this->vendorHelper->getAllVendorFarmsArray();
    }

    /**
     * Return Yes or No array
     * @return array|string[]
     */
    public function getYesOrNoArray()
    {
        return [
            '0' => 'No',
            '1' => 'Yes',
            '2' => 'Both'
        ];
    }

    /**
     * Return offer table
     * @return string[]
     */
    public function getTableHeaders()
    {
        return [
            'Offer Id',
            'SKU',
            'Product Name',
            'Vendor Name',
            'Vendor Location',
            'Qty Uploaded',
            'Qty Confirmed',
            'Qty Shipped',
            'Discard',
            'Qty Available',
            'EOD inventory',
            'Node inventory',
            'Received Date',
            'Expiry Date',
            'Status',
            'Edit',
            'Comment/Log',
        ];
    }

    /**
     * @return string[]
     */
    public function getTableHeadersForFarm()
    {
        return [
            'Offer Id',
            'SKU',
            'Product Name',
            'Vendor Name',
            'Vendor Location',
            'Cost',
            'Qty Uploaded',
            'Qty Confirmed',
            'Qty Shipped',
            'Qty Per Box',
            'Discard',
            'Qty Available',
            'EOD inventory',
            'Node inventory',
            'Received Date',
            'Expiry Date',
            'Status',
            'Edit',
            'Comment/Log',
        ];
    }

    /**
     * Return Discard URL
     * @return string
     */
    public function getDiscardUrl()
    {
        return $this->getUrl('vendorinventory/inventory/discard');
    }

    /**
     * Return Vendor Farms HTML Array
     * @return array
     */
    public function getAllFarmsWithVendorHtml()
    {
        return $this->vendorHelper->getAllVendorFarmsArray();
    }

    /**
     * Return Save URL
     * @return string
     */
    public function getOfferSaveUrl()
    {
        return $this->getUrl('vendorinventory/inventory/save');
    }

    /**
     * Return Template URL
     * @return string
     */
    public function getDownloadTemplateUrl()
    {
        return $this->getUrl('vendorinventory/inventory/template');
    }

    /**
     * @return string
     */
    public function getMapAllSkuListUrl()
    {
        return $this->getUrl('vendorinventory/inventory/mapsku');
    }

    /**
     * Return Cron URL
     * @return string
     */
    public function getInventoryCronUrl()
    {
        return $this->getUrl('vendorinventory/inventory/cron');
    }

    /**
     * @return string[]
     */
    public function getDiscardReasons()
    {
        return [
            'Purchase from DC' => 'Purchase from DC',
            'Sold to DC' => 'Sold to DC',
            'Wilted Flowers' => 'Wilted Flowers',
            'Broken' => 'Broken',
            'Aging' => 'Aging',
            'Quality Issue' => 'Quality Issue',
            'Other' => 'Other'
        ];
    }

    /**
     * Return Upload URL
     * @return string
     */
    public function getUploadTemplateUrl()
    {
        return $this->getUrl('vendorinventory/inventory/uploadinventory');
    }

    /**
     * Return Logs Retrieve URL
     * @return string
     */
    public function getLogsRetrieveUrl()
    {
        return $this->getUrl('vendorinventory/inventory/logs');
    }

    /**
     * @return $this|Index
     */
    protected function _prepareLayout()
    {
        $this->initFilters();
        return $this;
    }

    public function initFilters()
    {
        $filter = $this->getRequest()->getParam('filter');
        if ($filter) {
            $filter = base64_decode($filter);
            parse_str($filter, $this->filter);
        }
        return $this->filter;
    }

    /**
     * @param $field
     * @return mixed|null
     */
    public function getFilterField($field)
    {
        if (isset($this->filter[$field]) && !empty($this->filter[$field])) {
            return $this->filter[$field];
        }
        return null;
    }

    /**
     * Hard Good Product Collection
     * @return Collection
     */
    public function getHardGoodProductCollection()
    {
        return $this->catalogHelper->getHardGoodProductCollection();
    }

    /**
     * @return AbstractDb|AbstractCollection|null
     */
    public function getHardGoodMappedCollection()
    {
        return $this->vendorHelper->getHardGoodMappingCollection();
    }

    /**
     * @return Collection
     */
    public function getAllProductCollection()
    {
        return $this->catalogHelper->getNonHardGoodProductCollection();
    }

    /**
     * @return string[]
     */
    public function getHardGoodTableHeaders()
    {
        return [
            'Id',
            'Product SKU',
            'Product Name',
            'Hard Good SKU',
            'Hard Good Product Name',
            'Qty',
            'Status',
            'Edit'
        ];
    }

}
