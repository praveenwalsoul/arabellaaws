<?php

namespace Flordelcampo\VendorInventory\Controller\Adminhtml\Inventory;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class Template extends Action
{
    const VENDOR_TYPE_DC = 'flordel_vendor_configuration/general/vendor_type_dc';
    const VENDOR_TYPE_FARM = 'flordel_vendor_configuration/general/vendor_type_farm';
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     */
    public function execute()
    {
        date_default_timezone_set("America/New_York");
        try {
            $vendorId = null;
            $params = $this->getRequest()->getPostvalue();
            if (isset($params['vendor_id'])) {
                $vendorId = $params['vendor_id'];
            }
            $productCollection = $this->vendorHelper->getVendorAssociateProductCollection($vendorId);
            $time = date('Y-m-d H:i:s');
            $vendorType = $params['vendor_type'];
            $data = [];
            $fileName = "template.csv";

            $dc = $this->vendorHelper->getConfig(self::VENDOR_TYPE_DC);
            $farm = $this->vendorHelper->getConfig(self::VENDOR_TYPE_FARM);
            switch ($vendorType) {
                case $dc:
                    $fileName = "order_receipt_template_dc_" . $time . ".csv";
                    /**
                     * Dc template
                     */
                    $data[] = ['SKU', 'Product Name', 'Qty', 'Vendor Name'];
                    /**
                     * product data
                     */
                    $vendorName = null;
                    foreach ($productCollection as $product) {
                        if ($vendorId != null) {
                            /**
                             * Check for correct vendor Id mapped to product
                             */
                            $vendorString = $product->getVendor();
                            $vendorArray = explode(',', $vendorString);
                            if (!in_array($vendorId, $vendorArray)) {
                                continue;
                            }
                            $vendorName = $this->vendorHelper->getVendorNameByVendorId($vendorId);
                        }
                        $data[] = [$product->getSku(), $product->getName(), 0, $vendorName];
                    }
                    break;
                case $farm:
                    $fileName = "order_receipt_template_farm_" . $time . ".csv";
                    /**
                     * Farm Template
                     */
                    $data[] = ['SKU', 'Product Name', 'Qty', 'Cost', 'Qty Per Box', 'Vendor Name'];
                    /**
                     * product data
                     */
                    $vendorName = null;
                    foreach ($productCollection as $product) {
                        if ($vendorId != null) {
                            /**
                             * Check for correct vendor Id mapped to product
                             */
                            $vendorString = $product->getVendor();
                            $vendorArray = explode(',', $vendorString);
                            if (!in_array($vendorId, $vendorArray)) {
                                continue;
                            }
                            $vendorName = $this->vendorHelper->getVendorNameByVendorId($vendorId);
                        }
                        $data[] = [$product->getSku(), $product->getName(), 0, 0, 0, $vendorName];
                    }
                    break;
            }
            header('Content-Type: application/csv; charset=UTF-8');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $fp = fopen('php://output', 'w');
            foreach ($data as $row) {
                fputcsv($fp, $row);
            }
            fclose($fp);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        exit();
    }
}
