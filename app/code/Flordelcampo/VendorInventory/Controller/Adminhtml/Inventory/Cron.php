<?php

namespace Flordelcampo\VendorInventory\Controller\Adminhtml\Inventory;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;
use Magento\Store\Model\StoreManagerInterface;
use Flordelcampo\VendorInventory\Model\InventoryCron;

class Cron extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var InventoryCron
     */
    protected $inventoryCron;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     * @param CatalogHelper $catalogHelper
     * @param StoreManagerInterface $storeManager
     * @param InventoryCron $inventoryCron
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request,
        CatalogHelper $catalogHelper,
        StoreManagerInterface $storeManager,
        InventoryCron $inventoryCron
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        $this->catalogHelper = $catalogHelper;
        $this->storeManager = $storeManager;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->inventoryCron = $inventoryCron;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     * @return void
     */
    public function execute()
    {
        $this->inventoryCron->executeInventoryCron();
    }

}
