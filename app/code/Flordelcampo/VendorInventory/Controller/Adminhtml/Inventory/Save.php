<?php

namespace Flordelcampo\VendorInventory\Controller\Adminhtml\Inventory;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $params = $this->getRequest()->getPostvalue();
            if (isset($params['offer_id'])) {
                $vendorOfferModel = $this->vendorHelper->getVendorInventoryOfferById($params['offer_id']);
            } else {
                throw new LocalizedException(__('Invalid Offer Id'));
            }
            /**
             * Log Update before save
             */
            $previousQtyConfirmed = $vendorOfferModel->getData('qty_confirmed');
            $yesOrNo = $this->yesOrNo();
            $comment = [];
            if ($previousQtyConfirmed != $params['qty_confirmed']) {
                $comment[] = 'Previous Qty Confirmed = ' . $vendorOfferModel->getData('inventory_qty');
                $comment[] = 'Updated Qty Confirmed = ' . $params['qty_confirmed'];
            }
            $previousEodInventory = $vendorOfferModel->getData('eod_inventory');
            if ($previousEodInventory != $params['eod_inventory']) {
                $comment[] = 'Previous EOD Inventory = ' . $vendorOfferModel->getData('inventory_qty');
                $comment[] = 'Updated EOD Inventory = ' . $params['eod_inventory'];
            }
            $previousStatus = $vendorOfferModel->getData('status');
            if ($previousStatus != $params['status']) {
                $comment[] = 'Previous Status = ' . $yesOrNo[$previousStatus];
                $comment[] = 'Updated Status = ' . $yesOrNo[$params['status']];
            }
            if (count($comment) > 0) {
                $log = $this->vendorHelper->getInventoryLogModel();
                $currentUser = $this->vendorHelper->getCurrentAdminUser();
                $log->setData('offer_id', $params['offer_id']);
                $log->setData('comment', implode('<br>', $comment));
                $log->setData('user_name', $currentUser->getUserName());
                $log->save();
            }

            /*$notInArray = ['offer_id', 'form_key'];
            foreach ($params as $field => $value) {
                if (!in_array($field, $notInArray)) {
                    $vendorOfferModel->setData($field, $value);
                }
            }*/
            if (isset($params['status'])) {
                $vendorOfferModel->setData('status', $params['status']);
            }
            if (isset($params['qty_confirmed']) && !empty($params['qty_confirmed'])) {
                $vendorOfferModel->setData('inventory_qty', $params['qty_confirmed']);
                $vendorOfferModel->setData('qty_confirmed', '0');
            }
            if (isset($params['eod_inventory']) && !empty($params['eod_inventory'])) {
                $vendorOfferModel->setData('inventory_qty', $params['eod_inventory']);
                $vendorOfferModel->setData('eod_inventory', '0');
            }
            $vendorOfferModel->save();
            $data = [
                'status' => true,
                'message' => __('Offer saved successfully')
            ];
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * @return string[]
     */
    public function yesOrNo()
    {
        return [
            '0' => 'Inactive',
            '1' => 'Active'
        ];
    }
}
