<?php

namespace Flordelcampo\VendorInventory\Controller\Adminhtml\Inventory;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class Discard extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $url = $this->_redirect->getRefererUrl();
        try {
            $params = $this->getRequest()->getParams();
            if (isset($params['offer_id'])) {
                $vendorOfferModel = $this->vendorHelper->getVendorInventoryOfferById($params['offer_id']);
            } else {
                throw new LocalizedException(__('Invalid Offer Id'));
            }
            $currentInvQty = $vendorOfferModel->getData('inventory_qty');
            $discardQty = $params['discard_qty'];
            $newQty = $currentInvQty - $discardQty;
            if ($newQty < 0) {
                $newQty = 0;
            }
            $vendorOfferModel->setData('inventory_qty', $newQty);
            $vendorOfferModel->save();

            /**
             * Log Update
             */
            $log = $this->vendorHelper->getInventoryLogModel();
            $currentUser = $this->vendorHelper->getCurrentAdminUser();
            $log->setData('offer_id', $params['offer_id']);
            $comment = 'Qty discarded = ' . $params['discard_qty'];
            $log->setData('comment', $comment);
            if (strtolower($params['reason']) == 'other') {
                $log->setData('reason', $params['other_reason']);
            } else {
                $log->setData('reason', $params['reason']);
            }
            $log->setData('vendor_type', $params['vendor_type']);
            $log->setData('user_name', $currentUser->getUserName());
            $log->save();

        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->messageManager->addSuccessMessage(
            __('Qty Discarded Successfully')
        );
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }
}
