<?php

namespace Flordelcampo\VendorInventory\Controller\Adminhtml\Inventory;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;
use Magento\Framework\File\Csv;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

class UploadInventory extends Action
{
    const VENDOR_TYPE_DC = 'flordel_vendor_configuration/general/vendor_type_dc';
    const VENDOR_TYPE_FARM = 'flordel_vendor_configuration/general/vendor_type_farm';
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Csv
     */
    protected $csv;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     * @param Csv $csv
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request,
        Csv $csv,
        CatalogHelper $catalogHelper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->csv = $csv;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     */
    public function execute()
    {
        $dc = $this->vendorHelper->getConfig(self::VENDOR_TYPE_DC);
        $farm = $this->vendorHelper->getConfig(self::VENDOR_TYPE_FARM);
        $resultRedirect = $this->resultRedirectFactory->create();
        $url = $this->_redirect->getRefererUrl();
        try {
            $params = $this->getRequest()->getParams();
            $offerProductAttributes = $this->offerProductAttributes();
            if ($params['vendor_type'] == $dc) {
                /**
                 * For DC type with qtp
                 */
                $defaultValuesAttributes = $this->defaultValuesAttributes();
            } else {
                $defaultValuesAttributes = $this->defaultValuesAttributesForFarm();
            }
            if (!empty($this->getRequest()->getFiles('offer_file'))) {
                $file = $this->getRequest()->getFiles('offer_file');
                $rawData = $this->csv->getData($file['tmp_name']);
                if (count($rawData) > 0) {
                    $first = 0;
                    foreach ($rawData as $row) {
                        if ($first == 0) {
                            $first = 9;
                            continue;
                        }
                        if (isset($row[0])) {
                            $sku = $row[0];
                            try {
                                $offer = $this->vendorHelper->getVendorInventoryModel();
                                $product = $this->catalogHelper->getProductBySku($sku);
                                $offer->setData('sku', $sku);
                                if (isset($row[1])) {
                                    $offer->setData('name', $row[1]);
                                }
                                if ($params['vendor_type'] == $dc) {
                                    if (isset($row[2])) {
                                        $qty = (int)$row[2];
                                        $offer->setData('qty_uploaded', $qty);
                                    }
                                }
                                if ($params['vendor_type'] == $farm) {
                                    /**
                                     * For Farm
                                     */
                                    if (isset($row[2])) {
                                        $qty = (int)$row[2];
                                        $offer->setData('qty_uploaded', $qty);
                                        $offer->setData('inventory_qty', $qty);
                                        $offer->setData('qty_confirmed', $qty);
                                    }
                                    if (isset($row[3])) {
                                        $price = $row[3];
                                        $offer->setData('price', $price);
                                    }
                                    if (isset($row[4])) {
                                        $qtp = (int)$row[4];
                                        $offer->setData('qty_per_box', $qtp);
                                    }
                                }

                                foreach ($offerProductAttributes as $productAttribute => $offerAttribute) {
                                    $offer->setData($offerAttribute, $product->getData($productAttribute));
                                }
                                foreach ($defaultValuesAttributes as $offerAttribute => $value) {
                                    $offer->setData($offerAttribute, $value);
                                }
                                foreach ($params as $attribute => $value) {
                                    $offer->setData($attribute, $value);
                                }
                                $offer->save();
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                    }
                }
            } else {
                throw new LocalizedException(__('Invalid file'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->messageManager->addSuccessMessage(
            __('Offer Updated Successfully')
        );
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }

    /**
     * Offer default values
     *
     * @return string[]
     */
    public function defaultValuesAttributes()
    {
        return [
            'inventory_qty' => '0',
            'qty_per_box' => '1',
            'qty_confirmed' => '0',
            'eod_inventory' => '0',
            'node_inventory' => '0',
            'status' => '1',
        ];
    }

    /**
     * Offer default values
     *
     * @return string[]
     */
    public function defaultValuesAttributesForFarm()
    {
        return [
            'eod_inventory' => '0',
            'node_inventory' => '0',
            'status' => '1',
        ];
    }

    /**
     * Product attributes => offer attributes
     * @return string[]
     */
    public function offerProductAttributes()
    {
        return [
            'vendor_sku' => 'vendor_product_sku',
            'vendor_product_name' => 'vendor_product_name',
            'box_type' => 'box_type',
            'box_height' => 'box_height',
            'box_weight' => 'box_weight',
            'box_length' => 'box_length',
            'box_width' => 'box_width',
            'color' => 'color',
            'dimensional_weight' => 'dimensional_weight',
            'group' => 'group',
            'head_size' => 'head_size',
            'product_um' => 'product_um',
            'sustainably_certified' => 'sustainably_certified',
            'brand' => 'brand',
            'attribute_set_id' => 'product_type_id'
        ];
    }
}
