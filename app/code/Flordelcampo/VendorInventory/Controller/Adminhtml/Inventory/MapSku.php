<?php

namespace Flordelcampo\VendorInventory\Controller\Adminhtml\Inventory;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;
use Magento\Framework\File\Csv;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

class MapSku extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Csv
     */
    protected $csv;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     * @param Csv $csv
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request,
        Csv $csv,
        CatalogHelper $catalogHelper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->csv = $csv;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * @return ResponseInterface|Json|Redirect|ResultInterface
     */
    public function execute()
    {
        $requestType = $this->getRequest()->getParam('request_type');
        switch ($requestType) {
            case 'get_sku_list':
                $resultJson = $this->jsonFactory->create();
                try {
                    $productCollection = $this->catalogHelper->getProductCollectionFactory();
                    $productCollection->addAttributeToSelect(['sku', 'name']);
                    $html = "<option value=''>Select</option>";
                    if ($productCollection->getSize() > 0) {
                        foreach ($productCollection as $product) {
                            $val = $product->getSku() . ' = ' . $product->getName();
                            $html .= "<option value='" . $product->getSku() . "'>" . $val . "</option>";
                        }
                    }
                    $data = [
                        'status' => true,
                        'sku_html' => $html
                    ];
                } catch (\Exception $e) {
                    $html = "<option value=''>Select SKU</option>";
                    $data = [
                        'status' => true,
                        'sku_html' => $html
                    ];
                }
                $resultJson->setData($data);
                return $resultJson;
            case 'map_sku':
                $resultRedirect = $this->resultRedirectFactory->create();
                $url = $this->_redirect->getRefererUrl();
                try {
                    $params = $this->getRequest()->getParams();
                    $offerProductAttributes = $this->offerProductAttributes();
                    $defaultValuesAttributes = $this->defaultValuesAttributes();

                    $sku = $params['sku'];
                    $offer = $this->vendorHelper->getVendorInventoryModel();
                    $product = $this->catalogHelper->getProductBySku($sku);

                    $offer->setData('sku', $sku);
                    $offer->setData('name', $product->getName());
                    $offer->setData('qty_uploaded', '0');
                    foreach ($offerProductAttributes as $productAttribute => $offerAttribute) {
                        $offer->setData($offerAttribute, $product->getData($productAttribute));
                    }
                    foreach ($defaultValuesAttributes as $offerAttribute => $value) {
                        $offer->setData($offerAttribute, $value);
                    }
                    foreach ($params as $attribute => $value) {
                        $offer->setData($attribute, $value);
                    }
                    $offer->save();
                    /**
                     * Map to Product
                     */
                    $currentVendorString = $product->getVendor();
                    if (empty($currentVendorString)) {
                        $product->setVendor($params['vendor_id'])->save();
                    } else {
                        $vendorArray = explode(",", $currentVendorString);
                        if (in_array($params['vendor_id'], $vendorArray)) {
                            $this->messageManager->addSuccessMessage(
                                __('Product already mapped with this vendor')
                            );
                        } else {
                            $vendorArray[] = $params['vendor_id'];
                            $vendorString = implode(",", $vendorArray);
                            $product->setData('vendor', $vendorString);
                        }
                    }
                    $product->save();

                    /**
                     * Create Log
                     */
                    $comment = 'Mapped vendor to sku = ' . $sku . ' and offer created';
                    $log = $this->vendorHelper->getInventoryLogModel();
                    $currentUser = $this->vendorHelper->getCurrentAdminUser();
                    $log->setData('offer_id', $offer->getId());
                    $log->setData('comment', $comment);
                    $log->setData('user_name', $currentUser->getUserName());
                    $log->save();

                } catch (\Exception $e) {
                    $this->messageManager->addError(__($e->getMessage()));
                }
                $this->messageManager->addSuccessMessage(
                    __('Product Mapped and Offer Created Successfully')
                );
                $resultRedirect->setUrl($url);
                return $resultRedirect;
        }
    }

    /**
     * Offer default values
     *
     * @return string[]
     */
    private function defaultValuesAttributes()
    {
        return [
            'inventory_qty' => '0',
            'price' => '0',
            'qty_per_box' => '1',
            'qty_confirmed' => '0',
            'eod_inventory' => '0',
            'node_inventory' => '0',
            'status' => '1',
        ];
    }

    /**
     * Product attributes => offer attributes
     * @return string[]
     */
    private function offerProductAttributes()
    {
        return [
            'vendor_sku' => 'vendor_product_sku',
            'vendor_product_name' => 'vendor_product_name',
            'box_type' => 'box_type',
            'box_height' => 'box_height',
            'box_weight' => 'box_weight',
            'box_length' => 'box_length',
            'box_width' => 'box_width',
            'color' => 'color',
            'dimensional_weight' => 'dimensional_weight',
            'group' => 'group',
            'head_size' => 'head_size',
            'product_um' => 'product_um',
            'sustainably_certified' => 'sustainably_certified',
            'brand' => 'brand',
            'attribute_set_id' => 'product_type_id'
        ];
    }
}
