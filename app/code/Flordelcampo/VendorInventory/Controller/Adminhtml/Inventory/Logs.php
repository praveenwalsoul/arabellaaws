<?php

namespace Flordelcampo\VendorInventory\Controller\Adminhtml\Inventory;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;

class Logs extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $offerId = $this->getRequest()->getParam('offer_id');
            $vendorOfferModel = null;
            if ($offerId != null) {
                $logCollection = $this->vendorHelper->getInventoryLogCollectionByOfferId($offerId);
            } else {
                throw new LocalizedException(__('Invalid Offer Id'));
            }
            $tableHeaders = $this->getTableHeaders();
            $logAttributes = $this->getLogAttributeToShow();
            if ($logCollection->getSize() > 0) {
                $html = '';
                $html .= "<table class='admin__table-secondary offer-log-table-class'><thead>";
                $html .= "<tr>";
                foreach ($tableHeaders as $colName) {
                    $html .= "<th>$colName</th>";
                }
                $html .= "</tr></thead><tbody>";
                $count = 1;
                foreach ($logCollection as $log) {

                    $html .= "<tr>";
                    $html .= "<td>" . $count . "</td>";
                    foreach ($logAttributes as $attribute) {
                        $html .= "<td>" . $log->getData($attribute) . "</td>";
                    }
                    $html .= "</tr>";
                    $count++;
                }
                $html .= "</tbody></table>";
                $data = [
                    'status' => true,
                    'table_html' => $html
                ];
            } else {
                throw new LocalizedException(__('No logs found'));
            }
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * @return string[]
     */
    public function getTableHeaders()
    {
        return [
            'Sl No',
            'Modified By',
            'Modified Date',
            'Reason',
            'Comment'
        ];
    }

    /**
     * @return string[]
     */
    public function getLogAttributeToShow()
    {
        return [
            'user_name',
            'updated_at',
            'reason',
            'comment'
        ];
    }
}
