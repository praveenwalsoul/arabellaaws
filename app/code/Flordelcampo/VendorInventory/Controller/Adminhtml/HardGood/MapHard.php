<?php

namespace Flordelcampo\VendorInventory\Controller\Adminhtml\HardGood;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;
use Magento\Framework\File\Csv;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

/**
 * Class MapHard
 * Flordelcampo\VendorInventory\Controller\Adminhtml\HardGood
 */
class MapHard extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Csv
     */
    protected $csv;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     * @param Csv $csv
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request,
        Csv $csv,
        CatalogHelper $catalogHelper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->csv = $csv;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * @return ResponseInterface|Json|Redirect|ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $url = $this->_redirect->getRefererUrl();
        try {
            $params = $this->getRequest()->getParams();
            $sku = $params['product_sku'];
            $hgSku = $params['hg_product_sku'];
            $hardGood = $this->vendorHelper->getHardGoodMappingModel();
            $product = $this->getProductBySku($sku);
            $hgProduct = $this->getProductBySku($hgSku);
            $hardGood->setData('product_sku', $sku);
            $hardGood->setData('product_name', $product->getName());
            $hardGood->setData('hg_product_sku', $hgSku);
            $hardGood->setData('hg_product_name', $hgProduct->getName());
            $hardGood->setData('qty', (int)$params['qty']);
            $hardGood->setData('status', '1');
            $hardGood->save();
            /**
             * Create Log
             */
            /*$comment = 'Mapped vendor to sku = ' . $sku . ' and offer created';
            $log = $this->vendorHelper->getInventoryLogModel();
            $currentUser = $this->vendorHelper->getCurrentAdminUser();
            $log->setData('offer_id', $offer->getId());
            $log->setData('comment', $comment);
            $log->setData('user_name', $currentUser->getUserName());
            $log->save();*/

        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->messageManager->addSuccessMessage(
            __('Product Mapped Successfully')
        );
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }

    /**
     * @param $sku
     * @return ProductInterface|Product|null
     * @throws NoSuchEntityException
     */
    public function getProductBySku($sku)
    {
        return $this->catalogHelper->getProductBySku($sku);
    }
}