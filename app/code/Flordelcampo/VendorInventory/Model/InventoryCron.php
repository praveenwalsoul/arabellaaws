<?php

namespace Flordelcampo\VendorInventory\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

class InventoryCron
{
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * InventoryCron constructor.
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        CatalogHelper $catalogHelper
    )
    {
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * Inventory Cron
     */
    public function executeInventoryCron()
    {
        date_default_timezone_set("America/New_York");
        try {
            $currentDateWithHour = date('Y-m-d H:i');
            $currentDate = date('Y-m-d');
            $currentDay = strtolower(date('l'));
            $offerCollection = $this->vendorHelper->getVendorInventoryOfferCollection();
            $offerCollection->addFieldToFilter('status', '1');
            $skuWithUpdatableInventory = [];
            if ($offerCollection->getSize() > 0) {
                foreach ($offerCollection as $offer) {
                    try {
                        $offerId = $offer->getData('offer_id');
                        $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
                        $locationId = $offer->getData('loc_id');
                        $sku = $offer->getData('sku');
                        $offerStartDate = $offer->getData('start_date');
                        $offerExpiryDate = $offer->getData('expiry_date');
                        $inventoryQty = $offer->getData('inventory_qty');
                        /**
                         * check for invalid date thing
                         */
                        if (strtotime($offerStartDate) > strtotime($offerExpiryDate)) {
                            continue;
                        }
                        /**
                         * check for offer expiration
                         */
                        if (strtotime($offerExpiryDate) < strtotime($currentDate)) {
                            $currentInventory = $offer->getData('inventory_qty');
                            $offer->setData('status', '0');
                            $offer->setData('node_inventory', $currentInventory);
                            $offer->save();
                            continue;
                        }
                        /**
                         * Check Current date is between offer start and expiry date
                         */
                        if (strtotime($currentDate) >= strtotime($offerStartDate)
                            && strtotime($currentDate) < strtotime($offerExpiryDate)) {
                            if (isset($skuWithUpdatableInventory[$sku])) {
                                $skuWithUpdatableInventory[$sku] += (int)$inventoryQty;
                            } else {
                                $skuWithUpdatableInventory[$sku] = (int)$inventoryQty;
                            }
                            continue;
                        }
                        $locationCollection = $this->vendorHelper->getVendorLogisticModelByLocationId($locationId);
                        $locationCollection->addFieldToFilter('status', '1');
                        if ($locationCollection->getSize() > 0) {
                            $allCutOffTimeArray = [];
                            foreach ($locationCollection as $location) {
                                $currentCutOffDataArray = explode(",", $location->getData($currentDay));
                                if (count($currentCutOffDataArray) > 0) {
                                    if (isset($currentCutOffDataArray[1])) {
                                        if ($currentCutOffDataArray[1] == '1') {
                                            /**
                                             * if day is selected
                                             */
                                            $cutOffArray = explode("-", $currentCutOffDataArray[0]);
                                            if (isset($cutOffArray[1])) {
                                                $allCutOffTimeArray[] = $cutOffArray[1];
                                            }
                                        }
                                    }
                                }
                            }
                            if (count($allCutOffTimeArray) > 0) {
                                $maxCutOffTime = $allCutOffTimeArray[0];
                                foreach ($allCutOffTimeArray as $exitTime) {
                                    if (strtotime($exitTime) > strtotime($maxCutOffTime)) {
                                        $maxCutOffTime = $exitTime;
                                    }
                                }

                                $offerExpiryDateWithCutOffTime = $offerExpiryDate . ' ' . $maxCutOffTime;
                                /**
                                 * Check today date == expiry Date
                                 * Then check current date and time with expiry date and cut off time
                                 * if cutoff happened ignore else include
                                 */
                                if (strtotime($offerExpiryDate) == strtotime($currentDate)) {
                                    if (strtotime($currentDateWithHour) < strtotime($offerExpiryDateWithCutOffTime)) {
                                        if (isset($skuWithUpdatableInventory[$sku])) {
                                            $skuWithUpdatableInventory[$sku] += (int)$inventoryQty;
                                        } else {
                                            $skuWithUpdatableInventory[$sku] = (int)$inventoryQty;
                                        }
                                    }
                                }

                                /**
                                 * Check offer start date -1 day == Today date
                                 * if yes offer start date-1 day + cut off time < current day and time
                                 * then take offer
                                 */
                                $previousOfferDay = date('Y-m-d', strtotime('-1 days', strtotime($offerStartDate)));
                                if (strtotime($previousOfferDay) == strtotime($currentDate)) {
                                    $previousOfferDayWithCutOffTime = $previousOfferDay . ' ' . $maxCutOffTime;
                                    if (strtotime($previousOfferDayWithCutOffTime) < strtotime($currentDateWithHour)) {
                                        if (isset($skuWithUpdatableInventory[$sku])) {
                                            $skuWithUpdatableInventory[$sku] += (int)$inventoryQty;
                                        } else {
                                            $skuWithUpdatableInventory[$sku] = (int)$inventoryQty;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        continue;
                    }
                }
                /**
                 * Update Global Sku qty
                 */
                $stockRegistry = $this->catalogHelper->getStockRegistryInterface();
                foreach ($skuWithUpdatableInventory as $sku => $qty) {
                    try {
                        $stockData = $stockRegistry->getStockItemBySku($sku);
                        $stockData->setQty($qty)->save();
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
            $message = "Cron run successfully";
            $this->writeLog($message);
        } catch (\Exception $e) {
            $this->writeLog($e->getMessage());
        }
    }

    /**
     * Write Log
     * @param $message
     */
    public function writeLog($message)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/inventory_cron.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
        /*$logger->debug('array', $array);*/
    }
}