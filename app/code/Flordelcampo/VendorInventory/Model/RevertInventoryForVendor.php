<?php

namespace Flordelcampo\VendorInventory\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;
use Flordelcampo\Order\Helper\Data as orderHelper;

/**
 * Class RevertInventoryForVendor
 * Flordelcampo\VendorInventory\Model
 */
class RevertInventoryForVendor
{
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;
    /**
     * @var orderHelper
     */
    protected $orderHelper;

    /**
     * InventoryCron constructor.
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param CatalogHelper $catalogHelper
     * @param orderHelper $orderHelper
     */
    public function __construct(
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        CatalogHelper $catalogHelper,
        orderHelper $orderHelper
    )
    {
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->catalogHelper = $catalogHelper;
        $this->orderHelper = $orderHelper;
    }

    /**
     * Revert Inventory For Item
     * @param $itemId
     * @return bool
     */
    public function revertInventoryForVendorByItemID($itemId)
    {
        try {
            $item = $this->orderHelper->getOrderItemsModelByItemId($itemId);
            $offerId = $item->getData('offer_avail_id');
            if ($offerId != '') {
                $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
                $currentQty = $item->getQtyOrdered() * $item->getData('qty_per_box');
                $currentInventory = $offer->getData('inventory_qty');
                $updatedInv = $currentInventory + $currentQty;
                $offer->setData('inventory_qty', $updatedInv)->save();
            }
            /**
             * Hard Good Revert
             */
            $hardGoodItemCollection = $this->orderHelper->getHardGoodItemCollection();
            $hardGoodItemCollection->addFieldToFilter('item_id', $itemId);
            if ($hardGoodItemCollection->getSize() > 0) {
                $stockRegistry = $this->catalogHelper->getStockRegistryInterface();
                foreach ($hardGoodItemCollection as $oneHg) {
                    $offerId = $oneHg->getData('hg_offer_id');
                    $orderedHgQty = $oneHg->getData('hg_ordered_qty');
                    $hardGoodSku = $oneHg->getData('hg_product_sku');
                    $invToIncrement = $item->getQtyOrdered() * $orderedHgQty;
                    $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
                    if ($offer->getId() != null) {
                        $currentInventory = $offer->getData('inventory_qty');
                        $updatedInv = $currentInventory + $invToIncrement;
                        $offer->setData('inventory_qty', $updatedInv)->save();
                        /**
                         * Lets Increment in actual product table also
                         */
                        $stockData = $stockRegistry->getStockItemBySku($hardGoodSku);
                        $curQty = $stockData->getQty();
                        $updatedQty = $curQty + $invToIncrement;
                        $stockData->setQty($updatedQty)->save();
                    }
                }
            }
        } catch (\Exception $e) {
            $this->writeLog($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Write Log
     * @param $message
     */
    public function writeLog($message)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/inventory_revert.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
        /*$logger->debug('array', $array);*/
    }
}