<?php

namespace Flordelcampo\VendorInventory\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\VendorInventory\Model\ResourceModel\Inventory as ResourceInventory;

/**
 * Class Inventory
 * Flordelcampo\VendorInventory\Model
 */
class Inventory extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceInventory::class);
    }

}