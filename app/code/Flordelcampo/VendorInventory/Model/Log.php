<?php

namespace Flordelcampo\VendorInventory\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\VendorInventory\Model\ResourceModel\Log as ResourceLog;

/**
 * Class Inventory
 * Flordelcampo\VendorInventory\Model
 */
class Log extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceLog::class);
    }

}