<?php

namespace Flordelcampo\VendorInventory\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\VendorInventory\Model\ResourceModel\HardGoodOrderItem as ResourceHardGoodOrderItem;

/**
 * Class HardGood
 * Flordelcampo\VendorInventory\Model
 */
class HardGoodOrderItem extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceHardGoodOrderItem::class);
    }

}