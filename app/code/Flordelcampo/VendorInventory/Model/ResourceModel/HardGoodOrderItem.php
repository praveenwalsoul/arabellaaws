<?php

namespace Flordelcampo\VendorInventory\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class HardGood
 * Flordelcampo\VendorInventory\Model\ResourceModel
 */
class HardGoodOrderItem extends AbstractDb
{
    /**
     * Post constructor.
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Stores Table
     */
    protected function _construct()
    {
        $this->_init('sales_order_hard_good_item', 'entity_id');
    }

}