<?php

namespace Flordelcampo\VendorInventory\Model\ResourceModel\HardGood;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\VendorInventory\Model\HardGood;
use Flordelcampo\VendorInventory\Model\ResourceModel\HardGood as ResourceHardGood;

/**
 * Class Collection
 * Flordelcampo\VendorInventory\Model\ResourceModel\HardGood
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            HardGood::class,
            ResourceHardGood::class
        );
    }

}
