<?php

namespace Flordelcampo\VendorInventory\Model\ResourceModel\HardGoodOrderItem;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\VendorInventory\Model\HardGoodOrderItem;
use Flordelcampo\VendorInventory\Model\ResourceModel\HardGoodOrderItem as ResourceHardGoodOrderItem;

/**
 * Class Collection
 * Flordelcampo\VendorInventory\Model\ResourceModel\HardGoodOrderItem
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            HardGoodOrderItem::class,
            ResourceHardGoodOrderItem::class
        );
    }

}
