<?php

namespace Flordelcampo\VendorInventory\Model\ResourceModel\VendorConfirm;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\VendorInventory\Model\VendorConfirm;
use Flordelcampo\VendorInventory\Model\ResourceModel\VendorConfirm as ResourceVendorConfirm;

/**
 * Class Collection
 * Flordelcampo\VendorInventory\Model\ResourceModel\Log
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            VendorConfirm::class,
            ResourceVendorConfirm::class
        );
    }

}
