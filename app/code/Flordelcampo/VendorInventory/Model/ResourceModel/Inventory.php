<?php

namespace Flordelcampo\VendorInventory\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Logistics
 * Flordelcampo\VendorInventory\Model\ResourceModel
 */
class Inventory extends AbstractDb
{
    /**
     * Post constructor.
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Stores Table
     */
    protected function _construct()
    {
        $this->_init('vendor_inventory_offer_table', 'offer_id');
    }

}