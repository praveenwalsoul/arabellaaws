<?php

namespace Flordelcampo\VendorInventory\Model\ResourceModel\Log;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\VendorInventory\Model\Log;
use Flordelcampo\VendorInventory\Model\ResourceModel\Log as ResourceLog;

/**
 * Class Collection
 * Flordelcampo\VendorInventory\Model\ResourceModel\Log
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Log::class,
            ResourceLog::class
        );
    }

}
