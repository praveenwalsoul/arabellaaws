<?php

namespace Flordelcampo\VendorInventory\Model\ResourceModel\Inventory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Flordelcampo\VendorInventory\Model\Inventory;
use Flordelcampo\VendorInventory\Model\ResourceModel\Inventory as ResourceInventory;

/**
 * Class Collection
 * Flordelcampo\VendorInventory\Model\ResourceModel\Inventory
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Inventory::class,
            ResourceInventory::class
        );
    }

}
