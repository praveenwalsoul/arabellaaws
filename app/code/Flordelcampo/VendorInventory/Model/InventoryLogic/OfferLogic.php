<?php

namespace Flordelcampo\VendorInventory\Model\InventoryLogic;

use Flordelcampo\CalendarApi\Controller\BaseController;
use Flordelcampo\VendorRegistration\Model\Logistics;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class InventoryLogic
 * Flordelcampo\VendorInventory\Model\InventoryLogic
 */
class OfferLogic extends BaseController
{
    const DEFAULT_VENDOR = 'flordel_vendor_configuration/general/default_vendor';
    const DEFAULT_VENDOR_FARM = 'flordel_vendor_configuration/general/default_vendor_farm';
    const DEFAULT_SHIP_METHOD = 'flordel_vendor_configuration/general/default_ship_method';
    const DEFAULT_PICKUP = 'flordel_vendor_configuration/general/default_pickup_date';
    const DEFAULT_DELIVERY = 'flordel_vendor_configuration/general/default_delivery_days';

    /**
     * @param $csvParams
     * @return array
     */
    public function getOfferDataForSku($csvParams)
    {
        date_default_timezone_set("America/New_York");
        try {
            $sku = trim($csvParams['sku']);
            $csvZipCode = (isset($csvParams['zip_code'])) ? trim($csvParams['zip_code']) : null;
            $csvPrefShipMethod = (isset($csvParams['pref_ship_method'])) ? trim($csvParams['pref_ship_method']) : null;
            $currentDate = date('Y-m-d');
            $currentDateWithHour = date('Y-m-d H:i');
            $currentDay = strtolower(date('l'));
            $vendors = $this->catalogHelper->getVendorsBySku($sku);
            $finalVendorResponse = null;
            if (!empty($vendors) && $vendors != '') {
                $vendorsArray = explode(",", $vendors);
                /**
                 * Find All Vendor Inventory Offers Collection
                 */
                $params['vendor_id'] = $vendorsArray;
                $params['sku'] = $sku;
                $params['exclude_zero_inventory'] = 'yes';
                $offerCollection = $this->vendorHelper->getVendorInventoryOfferCollection($params);
                $hardGoodAttributeId = $this->vendorHelper->getHardGoodAttributeId();
                $offerCollection->addFieldToFilter('product_type_id', ['neq' => $hardGoodAttributeId]);
                $offerFirstFilterArray = [];
                if ($offerCollection->getSize() > 0) {
                    foreach ($offerCollection as $offer) {
                        $offerId = $offer->getId();
                        $locationId = $offer->getData('loc_id');
                        $vendorId = $offer->getData('vendor_id');
                        $inventory = $offer->getData('inventory_qty');
                        $offerStartDate = $offer->getData('start_date');
                        $offerStartDateMinusDay = date('Y-m-d', strtotime('-1 days', strtotime($offerStartDate)));
                        $offerExpiryDate = $offer->getData('expiry_date');
                        /**
                         * check for invalid date thing
                         */
                        if (strtotime($offerStartDate) > strtotime($offerExpiryDate)) {
                            continue;
                        }
                        /**
                         * check for offer expiration
                         */
                        if (strtotime($offerExpiryDate) < strtotime($currentDate)) {
                            continue;
                        }
                        $logisticCollection = $this->vendorHelper->getVendorLogisticCollectionWithShipping($locationId);
                        $logisticCollection->addFieldToFilter('main_table.status', '1');
                        if ($logisticCollection->getSize() > 0) {
                            foreach ($logisticCollection as $logistic) {
                                $currentCutOffDataArray = explode(",", $logistic->getData($currentDay));
                                if (count($currentCutOffDataArray) > 0) {
                                    /**
                                     * If ware house is open
                                     */
                                    $shippingMethod = $logistic->getData('shipping_method_name');
                                    $priority = $logistic->getData('priority');
                                    $leadTime = (int)$logistic->getData('lead_time');
                                    $bufferDays = (int)$logistic->getData('buffer_days');
                                    $preferableShipMethod = trim($logistic->getData('pref_ship_method'));
                                    $locationZipCodes = explode(",", $logistic->getData('zip_codes'));
                                    $logisticPreferableShipMethod = explode(",", $preferableShipMethod);
                                    $isZipMatches = $isPreferableShipMatch = 0;

                                    if (in_array($csvZipCode, $locationZipCodes) !== false) {
                                        $isZipMatches = 1;
                                    }
                                    /**
                                     * If Zip code code matches or Zip code field is null
                                     * Then only take this logistic for calculations else ignore
                                     */
                                    if ($isZipMatches == 0 && $logistic->getData('zip_codes') != '') {
                                        continue;
                                    }
                                    if (in_array($csvPrefShipMethod, $logisticPreferableShipMethod) !== false) {
                                        $isPreferableShipMatch = 1;
                                    }
                                    $isWareHouseOpen = 0;
                                    if (isset($currentCutOffDataArray[1])) {
                                        if ($currentCutOffDataArray[1] == '1') {
                                            /**
                                             * If warehouse open today
                                             * if day is selected
                                             */
                                            $isWareHouseOpen = 1;
                                            $cutOffArray = explode("-", $currentCutOffDataArray[0]);
                                            if (isset($cutOffArray[1])) {
                                                /**
                                                 * if today date between start date-1 day cutoff time
                                                 * and expiry date and cutoff time
                                                 */
                                                $offerStartDateWithHour = $offerStartDateMinusDay . ' ' . $cutOffArray[1];
                                                $offerExpiryDateDateWithHour = $offerExpiryDate . ' ' . $cutOffArray[1];
                                                if (strtotime($currentDateWithHour) >= strtotime($offerStartDateWithHour)
                                                    && strtotime($currentDateWithHour) <= strtotime($offerExpiryDateDateWithHour)
                                                ) {

                                                    /**
                                                     * Check if warehouse close time is happen
                                                     */

                                                    $wareHouseCloseDateHourToday = $currentDate . ' ' . $cutOffArray[1];
                                                    if (strtotime($currentDateWithHour) < strtotime($wareHouseCloseDateHourToday)) {
                                                        $pickupDate = $currentDate;
                                                    } else {
                                                        $pickupDate = $this->getPickupDate($logistic);
                                                        if ($pickupDate == null) {
                                                            continue;
                                                        }
                                                    }
                                                    $deliveryDate = date('Y-m-d', strtotime("+3 days", strtotime($pickupDate)));;
                                                    if ($leadTime >= 0) {
                                                        $deliVariable = "+" . (int)$leadTime . ' days';
                                                        $deliveryDate = date('Y-m-d', strtotime($deliVariable, strtotime($pickupDate)));
                                                        if ($bufferDays >= 0) {
                                                            $deliVariable = "+" . (int)$bufferDays . ' days';
                                                            $deliveryDate = date('Y-m-d', strtotime($deliVariable, strtotime($deliveryDate)));
                                                        }
                                                    }
                                                    $offerFirstFilterArray[$vendorId][] = [
                                                        'vendor_id' => $vendorId,
                                                        'location_id' => $locationId,
                                                        'offer_id' => $offerId,
                                                        'inventory' => $inventory,
                                                        'pickup_date' => $pickupDate,
                                                        'delivery_date' => $deliveryDate,
                                                        'shipping_method' => $shippingMethod,
                                                        'priority' => $priority,
                                                        'is_warehouse_open' => $isWareHouseOpen,
                                                        'is_zip_matches' => $isZipMatches,
                                                        'is_pref_ship_match' => $isPreferableShipMatch
                                                    ];
                                                }
                                            }
                                        } else {
                                            /**
                                             * if ware house is closed
                                             */
                                            if (strtotime($currentDate) >= strtotime($offerStartDateMinusDay)
                                                && strtotime($currentDate) <= strtotime($offerExpiryDate)
                                            ) {
                                                $pickupDate = $this->getPickupDate($logistic);
                                                if ($pickupDate == null) {
                                                    continue;
                                                }
                                                $deliveryDate = date('Y-m-d', strtotime("+3 days", strtotime($pickupDate)));;
                                                if ($leadTime >= 0) {
                                                    $deliVariable = "+" . (int)$leadTime . ' days';
                                                    $deliveryDate = date('Y-m-d', strtotime($deliVariable, strtotime($pickupDate)));
                                                    if ($bufferDays >= 0) {
                                                        $deliVariable = "+" . (int)$bufferDays . ' days';
                                                        $deliveryDate = date('Y-m-d', strtotime($deliVariable, strtotime($deliveryDate)));
                                                    }
                                                }
                                                $offerFirstFilterArray[$vendorId][] = [
                                                    'vendor_id' => $vendorId,
                                                    'location_id' => $locationId,
                                                    'offer_id' => $offerId,
                                                    'inventory' => $inventory,
                                                    'pickup_date' => $pickupDate,
                                                    'delivery_date' => $deliveryDate,
                                                    'shipping_method' => $shippingMethod,
                                                    'priority' => $priority,
                                                    'is_warehouse_open' => $isWareHouseOpen,
                                                    'is_zip_matches' => $isZipMatches,
                                                    'is_pref_ship_match' => $isPreferableShipMatch
                                                ];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (count($offerFirstFilterArray) > 0) {
                    $finalLocationFilter = [];
                    foreach ($offerFirstFilterArray as $vendorId => $logisticsArray) {
                        if (count($logisticsArray) == 1) {
                            $finalLocationFilter[$vendorId] = $logisticsArray[0];
                            continue;
                        }

                        $tempZipMatch = $tempPrefShipMatch = [];
                        $count = 0;
                        $isThereValidZipMatch = 0;
                        foreach ($logisticsArray as $oneLogistic) {
                            if ($oneLogistic['is_zip_matches'] == 1) {
                                $isThereValidZipMatch = 1;
                            }
                            if ($count == 0) {
                                $tempZipMatch = $tempPrefShipMatch = $oneLogistic;
                                $count++;
                                continue;
                            }
                            /**
                             * Zip match logic
                             */
                            $prevZipMatch = $tempZipMatch['is_zip_matches'];
                            $currentZipMatch = $oneLogistic['is_zip_matches'];
                            $zipFlagDiff = $prevZipMatch - $currentZipMatch;
                            if ($zipFlagDiff < 0) {
                                /**
                                 * Means Previous dont have zip_match current one is having
                                 */
                                $tempZipMatch = $oneLogistic;
                            } elseif ($zipFlagDiff == 0) {
                                /**
                                 * Means both have same flag
                                 */
                                $prevPriority = $tempZipMatch['priority'];
                                $currentPriority = $oneLogistic['priority'];
                                if ($currentPriority > $prevPriority) {
                                    $tempZipMatch = $oneLogistic;
                                }
                                if ($currentPriority == $prevPriority) {
                                    $prevInventory = $tempZipMatch['inventory'];
                                    $currentInventory = $oneLogistic['inventory'];
                                    if ($currentInventory > $prevInventory) {
                                        $tempZipMatch = $oneLogistic;
                                    }
                                    if ($currentInventory == $prevInventory) {
                                        $prevVendorId = $tempZipMatch['vendor_id'];
                                        $currentVendorId = $oneLogistic['vendor_id'];
                                        if ($currentVendorId > $prevVendorId) {
                                            $tempZipMatch = $oneLogistic;
                                        }
                                    }
                                }
                            }

                            /**
                             * Preferable Ship Method Match
                             */
                            $prevPrefMatch = $tempPrefShipMatch['is_pref_ship_match'];
                            $currentPrefMatch = $oneLogistic['is_pref_ship_match'];
                            $prefShipFlagDiff = $prevPrefMatch - $currentPrefMatch;
                            if ($prefShipFlagDiff < 0) {
                                $tempPrefShipMatch = $oneLogistic;
                            } elseif ($prefShipFlagDiff == 0) {
                                $prevWareHouseFlag = $tempPrefShipMatch['is_warehouse_open'];
                                $currentWareHouseFlag = $oneLogistic['is_warehouse_open'];
                                if ($currentWareHouseFlag > $prevWareHouseFlag) {
                                    $tempPrefShipMatch = $oneLogistic;
                                } elseif ($prefShipFlagDiff == 0) {
                                    $prevWareHouseFlag = $tempPrefShipMatch['is_warehouse_open'];
                                    $currentWareHouseFlag = $oneLogistic['is_warehouse_open'];
                                    if ($currentWareHouseFlag > $prevWareHouseFlag) {
                                        $tempPrefShipMatch = $oneLogistic;
                                    }
                                    if ($currentWareHouseFlag == $prevWareHouseFlag) {
                                        $prevInventory = $tempPrefShipMatch['inventory'];
                                        $currentInventory = $oneLogistic['inventory'];
                                        if ($currentInventory > $prevInventory) {
                                            $tempPrefShipMatch = $oneLogistic;
                                        }
                                        if ($currentInventory == $prevInventory) {
                                            $prevVendorId = $tempPrefShipMatch['vendor_id'];
                                            $currentVendorId = $oneLogistic['vendor_id'];
                                            if ($currentVendorId > $prevVendorId) {
                                                $tempPrefShipMatch = $oneLogistic;
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        if ($isThereValidZipMatch == 1) {
                            $finalLocationFilter[$vendorId] = $tempZipMatch;
                        } else {
                            $finalLocationFilter[$vendorId] = $tempPrefShipMatch;
                        }
                    }
                    /**
                     * Follow same logic as above
                     */
                    if (count($finalLocationFilter) > 0) {
                        $tempZipMatch = $tempPrefShipMatch = [];
                        $count = 0;
                        $isThereValidZipMatch = 0;
                        foreach ($finalLocationFilter as $vendorId => $oneLogistic) {
                            if ($oneLogistic['is_zip_matches'] == 1) {
                                $isThereValidZipMatch = 1;
                            }
                            if ($count == 0) {
                                $tempZipMatch = $tempPrefShipMatch = $oneLogistic;
                                $count++;
                                continue;
                            }
                            /**
                             * Zip match logic
                             */
                            $prevZipMatch = $tempZipMatch['is_zip_matches'];
                            $currentZipMatch = $oneLogistic['is_zip_matches'];
                            $zipFlagDiff = $prevZipMatch - $currentZipMatch;
                            if ($zipFlagDiff < 0) {
                                /**
                                 * Means Previous dont have zip_match current one is having
                                 */
                                $tempZipMatch = $oneLogistic;
                            } elseif ($zipFlagDiff == 0) {
                                /**
                                 * Means both have same flag
                                 */
                                $prevPriority = $tempZipMatch['priority'];
                                $currentPriority = $oneLogistic['priority'];
                                if ($currentPriority > $prevPriority) {
                                    $tempZipMatch = $oneLogistic;
                                }
                                if ($currentPriority == $prevPriority) {
                                    $prevInventory = $tempZipMatch['inventory'];
                                    $currentInventory = $oneLogistic['inventory'];
                                    if ($currentInventory > $prevInventory) {
                                        $tempZipMatch = $oneLogistic;
                                    }
                                    if ($currentInventory == $prevInventory) {
                                        $prevVendorId = $tempZipMatch['vendor_id'];
                                        $currentVendorId = $oneLogistic['vendor_id'];
                                        if ($currentVendorId > $prevVendorId) {
                                            $tempZipMatch = $oneLogistic;
                                        }
                                    }
                                }
                            }

                            /**
                             * Preferable Ship Method Match
                             */
                            $prevPrefMatch = $tempPrefShipMatch['is_pref_ship_match'];
                            $currentPrefMatch = $oneLogistic['is_pref_ship_match'];
                            $prefShipFlagDiff = $prevPrefMatch - $currentPrefMatch;
                            if ($prefShipFlagDiff < 0) {
                                $tempPrefShipMatch = $oneLogistic;
                            } elseif ($prefShipFlagDiff == 0) {
                                $prevWareHouseFlag = $tempPrefShipMatch['is_warehouse_open'];
                                $currentWareHouseFlag = $oneLogistic['is_warehouse_open'];
                                if ($currentWareHouseFlag > $prevWareHouseFlag) {
                                    $tempPrefShipMatch = $oneLogistic;
                                } elseif ($prefShipFlagDiff == 0) {
                                    $prevWareHouseFlag = $tempPrefShipMatch['is_warehouse_open'];
                                    $currentWareHouseFlag = $oneLogistic['is_warehouse_open'];
                                    if ($currentWareHouseFlag > $prevWareHouseFlag) {
                                        $tempPrefShipMatch = $oneLogistic;
                                    }
                                    if ($currentWareHouseFlag == $prevWareHouseFlag) {
                                        $prevInventory = $tempPrefShipMatch['inventory'];
                                        $currentInventory = $oneLogistic['inventory'];
                                        if ($currentInventory > $prevInventory) {
                                            $tempPrefShipMatch = $oneLogistic;
                                        }
                                        if ($currentInventory == $prevInventory) {
                                            $prevVendorId = $tempPrefShipMatch['vendor_id'];
                                            $currentVendorId = $oneLogistic['vendor_id'];
                                            if ($currentVendorId > $prevVendorId) {
                                                $tempPrefShipMatch = $oneLogistic;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if ($isThereValidZipMatch == 1) {
                            $finalVendorResponse = $tempZipMatch;
                        } else {
                            $finalVendorResponse = $tempPrefShipMatch;
                        }
                    }
                }
            }
            if ($finalVendorResponse != null) {
                return $finalVendorResponse;
            } else {
                return $this->defaultVendorResponse();
            }

        } catch (\Exception $e) {
            return $this->defaultVendorResponse();
        }
    }

    /**
     * Return Hard Good Avail Id details for SKU
     * @param null $params
     * @return mixed
     */
    public function getHardGoodOfferData($params)
    {
        $hardGoodAvailIdDetails = $finalHardGoodDetails = [];
        try {
            $productSku = $params['product_sku'];
            $hardGoodSkusWithQty = $this->vendorHelper->getAllHardGoodMappedProductSkuArrayBySKu($productSku);
            if (count($hardGoodSkusWithQty) > 0) {
                $invParams = [];
                $skuArray = array_keys($hardGoodSkusWithQty);
                $invParams['vendor_id'] = $params['vendor_id'];
                $invParams['sku'] = $skuArray;
                $invParams['loc_id'] = $params['location_id'];
                /*$invParams['exclude_zero_inventory'] = 'yes';*/
                $hardGoodAttributeId = $this->vendorHelper->getHardGoodAttributeId();
                $invParams['product_type'] = $hardGoodAttributeId;
                $invParams['status'] = '';
                $invCollection = $this->vendorHelper->getVendorInventoryOfferCollection($invParams);
                if ($invCollection->getSize() > 0) {
                    foreach ($invCollection as $offer) {
                        $offerId = $offer->getId();
                        $hg_sku = $offer->getData('sku');
                        $invQty = $offer->getData('inventory_qty');
                        if (isset($hardGoodAvailIdDetails[$hg_sku])) {
                            $prevInvQty = $hardGoodAvailIdDetails[$hg_sku]['inventory_qty'];
                            if ($invQty > $prevInvQty) {
                                $hardGoodAvailIdDetails[$hg_sku] = [
                                    'product_sku' => $productSku,
                                    'hard_good_sku' => $hg_sku,
                                    'qty' => $hardGoodSkusWithQty[$hg_sku],
                                    'offer_id' => $offerId,
                                    'inventory_qty' => $invQty
                                ];
                            }
                        } else {
                            $hardGoodAvailIdDetails[$hg_sku] = [
                                'product_sku' => $productSku,
                                'hard_good_sku' => $hg_sku,
                                'qty' => $hardGoodSkusWithQty[$hg_sku],
                                'offer_id' => $offerId,
                                'inventory_qty' => $invQty
                            ];
                        }
                    }
                }
                /**
                 * If Hard good sku dont have offer then pass null
                 */
                foreach ($skuArray as $hg_sku) {
                    if (isset($hardGoodAvailIdDetails[$hg_sku])) {
                        $finalHardGoodDetails[$hg_sku] = [
                            'product_sku' => $productSku,
                            'hard_good_sku' => $hg_sku,
                            'qty' => $hardGoodSkusWithQty[$hg_sku],
                            'offer_id' => $hardGoodAvailIdDetails[$hg_sku]['offer_id'],
                            'inventory_qty' => $hardGoodAvailIdDetails[$hg_sku]['inventory_qty']
                        ];
                    } else {
                        $finalHardGoodDetails[$hg_sku] = [
                            'product_sku' => $productSku,
                            'hard_good_sku' => $hg_sku,
                            'qty' => $hardGoodSkusWithQty[$hg_sku],
                            'offer_id' => null,
                            'inventory_qty' => null
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            return $finalHardGoodDetails;
        }
        return $finalHardGoodDetails;
    }

    /**
     * Return Default Vendor Details
     * @return array
     */
    public function defaultVendorResponse()
    {
        $defVendor = $this->vendorHelper->getConfig(self::DEFAULT_VENDOR);
        $defVendorFarm = $this->vendorHelper->getConfig(self::DEFAULT_VENDOR_FARM);
        $defShipMethod = $this->vendorHelper->getConfig(self::DEFAULT_SHIP_METHOD);
        $defPickUpType = $this->vendorHelper->getConfig(self::DEFAULT_PICKUP);
        $defDeliveryDays = $this->vendorHelper->getConfig(self::DEFAULT_DELIVERY);

        $shipMethod = $this->vendorHelper->getShippingModelById($defShipMethod);
        if ($shipMethod->getId() != null) {
            $defShipMethod = $shipMethod->getData('shipping_method_name');
        }
        $params['vendor_id'] = $defVendor;
        $params['location_id'] = $defVendor;
        $params['location_id'] = $defVendorFarm;
        $params['shipping_method'] = $defShipMethod;
        if ($defDeliveryDays <= 0) {
            $defDeliveryDays = 2;
        }
        $deliVar = "+" . $defDeliveryDays . ' days';
        $pickupDate = $deliveryDate = null;
        switch ($defPickUpType) {
            case 'today':
                $pickupDate = date('Y-m-d');
                $deliveryDate = date('Y-m-d', strtotime($deliVar, strtotime($pickupDate)));
                break;
            case 'tomorrow':
                $pickupDate = date('Y-m-d', strtotime('+1 days', strtotime(date('Y-m-d'))));
                $deliveryDate = date('Y-m-d', strtotime($deliVar, strtotime($pickupDate)));
                break;
        }
        /*$logisticCollection = $this->vendorHelper->getVendorLogisticCollectionByParams($params);*/
        /**
         * TODO May be based on logistic
         * TODO we need to find pickup date and delivery date
         */
        return [
            'vendor_id' => $defVendor,
            'location_id' => $defVendorFarm,
            'offer_id' => null,
            'pickup_date' => $pickupDate,
            'delivery_date' => $deliveryDate,
            'shipping_method' => $defShipMethod
        ];
    }

    /**
     * Return Next Days
     * @return array
     */
    public function getNextDays()
    {
        $first = date('Y-m-d', strtotime("+1 days", strtotime(date('Y-m-d'))));
        $last = date('Y-m-d', strtotime("+6 days", strtotime($first)));
        $step = '+1 day';
        $output_format = "l";

        $dates = [];
        $current = strtotime($first);
        $last = strtotime($last);
        while ($current <= $last) {
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
        return $dates;
    }

    /**
     * @param Logistics $logistic
     * @return false|string|null
     */
    public function getPickupDate(Logistics $logistic)
    {
        $nextDaysArray = $this->getNextDays();
        /**
         * Choose next ware house open day
         * and its date as pickup
         */
        foreach ($nextDaysArray as $nextDay) {
            $day = strtolower($nextDay);
            $dateArray = explode(",", $logistic->getData($day));
            if ($dateArray[1] == '1') {
                $nextVariable = "next " . $day;
                return date('Y-m-d', strtotime($nextVariable));
            }
        }
        return null;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        // TODO: Implement execute() method.
    }
}