<?php

namespace Flordelcampo\VendorInventory\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\VendorInventory\Model\ResourceModel\HardGood as ResourceHardGood;

/**
 * Class HardGood
 * Flordelcampo\VendorInventory\Model
 */
class HardGood extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceHardGood::class);
    }

}