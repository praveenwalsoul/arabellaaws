<?php

namespace Flordelcampo\VendorInventory\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\VendorInventory\Model\ResourceModel\VendorConfirm as ResourceVendorConfirm;

/**
 * Class VendorConfirm
 * Flordelcampo\VendorInventory\Model
 */
class VendorConfirm extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceVendorConfirm::class);
    }

}