<?php

namespace Flordelcampo\VendorInventory\Cron;

use Flordelcampo\VendorInventory\Model\InventoryCron;

/**
 * Class InventoryUpdate
 * Flordelcampo\VendorInventory\Cron
 */
class InventoryUpdate
{
    /**
     * @var InventoryCron
     */
    protected $inventoryCron;

    /**
     * InventoryUpdate constructor.
     * @param InventoryCron $inventoryCron
     */
    public function __construct(
        InventoryCron $inventoryCron
    )
    {
        $this->inventoryCron = $inventoryCron;
    }

    /**
     * Cron
     */
    public function execute()
    {
        $message = "testing cron";
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/inventory_cron.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
        return $this;
        /*$logger->debug('array', $array);*/
        //$this->inventoryCron->executeInventoryCron();
    }
}