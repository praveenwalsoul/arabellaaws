<?php

namespace Flordelcampo\CalendarApi\Api;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Interface CalendarInterface
 * Flordelcampo\CalendarApi\Api
 */
interface CalendarInterface
{
    /**
     * Return Calendar Days
     *
     * @param string $productId
     * @return ApiResponseInterface
     * @throws LocalizedException
     */
    public function getCalendarForProduct($productId);
}