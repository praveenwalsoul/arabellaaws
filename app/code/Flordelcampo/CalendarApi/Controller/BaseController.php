<?php

namespace Flordelcampo\CalendarApi\Controller;

use Magento\Catalog\Model\Product;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\Quote;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Catalog\Model\ProductRepository;
use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\Api\Api\Data\ApiResponseInterfaceFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Api\Helper\Data as ApiHelper;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\CalendarApi\Helper\Data as CalendarHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;
use Flordelcampo\VendorInventory\Controller\Adminhtml\Inventory\UploadInventory as InventoryHelper;
use Flordelcampo\Dashboards\Controller\Adminhtml\Ups\Templates;

/**
 * Class BaseController
 * Flordelcampo\Api\Controller
 */
abstract class BaseController extends \Magento\Framework\App\Action\Action
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var ApiHelper
     */
    protected $apiHelper;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;
    /**
     * @var
     */
    protected $quoteResourceModel;
    /**
     * @var Quote
     */
    protected $quote;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;
    /**
     * @var ProductRepository
     */
    protected $productRepository;
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var ApiResponseInterface
     */
    protected $apiResponse;
    /**
     * @var ApiResponseInterfaceFactory
     */
    protected $responseFactory;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CalendarHelper
     */
    protected $calendarHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;
    /**
     * @var InventoryHelper
     */
    protected $inventoryHelper;
    /**
     * @var Templates
     */
    protected $upsTemplates;

    /**
     * BaseController constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param ApiHelper $apiHelper
     * @param CustomerHelper $customerHelper
     * @param Request $request
     * @param QuoteFactory $quoteFactory
     * @param Quote $quote
     * @param ResourceConnection $resourceConnection
     * @param CustomerFactory $customerFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param StoreRepositoryInterface $storeRepository
     * @param ProductRepository $productRepository
     * @param Product $product
     * @param AddressRepositoryInterface $addressRepository
     * @param ApiResponseInterfaceFactory $responseFactory
     * @param OrderHelper $orderHelper
     * @param VendorHelper $vendorHelper
     * @param CalendarHelper $calendarHelper
     * @param CatalogHelper $catalogHelper
     * @param InventoryHelper $inventoryHelper
     * @param Templates $upsTemplates
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        ApiHelper $apiHelper,
        CustomerHelper $customerHelper,
        Request $request,
        QuoteFactory $quoteFactory,
        Quote $quote,
        ResourceConnection $resourceConnection,
        CustomerFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        StoreRepositoryInterface $storeRepository,
        ProductRepository $productRepository,
        Product $product,
        AddressRepositoryInterface $addressRepository,
        ApiResponseInterfaceFactory $responseFactory,
        OrderHelper $orderHelper,
        VendorHelper $vendorHelper,
        CalendarHelper $calendarHelper,
        CatalogHelper $catalogHelper,
        InventoryHelper $inventoryHelper,
        Templates $upsTemplates
    ) {
        $this->storeManager = $storeManager;
        $this->apiHelper = $apiHelper;
        $this->customerHelper = $customerHelper;
        $this->request = $request;
        $this->quoteFactory = $quoteFactory;
        $this->quote = $quote;
        $this->resourceConnection = $resourceConnection;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->storeRepository = $storeRepository;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->addressRepository = $addressRepository;
        $this->responseFactory = $responseFactory;
        $this->orderHelper = $orderHelper;
        $this->vendorHelper = $vendorHelper;
        $this->calendarHelper = $calendarHelper;
        $this->catalogHelper = $catalogHelper;
        $this->inventoryHelper = $inventoryHelper;
        $this->upsTemplates = $upsTemplates;
        parent::__construct($context);
    }
}
