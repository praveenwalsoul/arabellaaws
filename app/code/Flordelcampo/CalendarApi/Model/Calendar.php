<?php

namespace Flordelcampo\CalendarApi\Model;

use Flordelcampo\Api\Api\Data\ApiResponseInterface;
use Flordelcampo\CalendarApi\Api\CalendarInterface;
use Flordelcampo\CalendarApi\Controller\BaseController;
use Flordelcampo\VendorRegistration\Model\Logistics;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

date_default_timezone_set('America/New_York');

/**
 * Class Calendar
 * Flordelcampo\CalendarApi\Model
 */
class Calendar extends BaseController implements CalendarInterface
{

    const CALENDAR_CONFIG_PATH = 'flordel_vendor_configuration/general/calendor_days';
    const SHIPPING_BLACKOUT_TYPE = '0';
    const DELIVERY_BLACKOUT_TYPE = '1';
    /**
     * @var null
     */
    private $_productId = null;
    /**
     * @var null
     */
    private $_product = null;
    /**
     * @var null
     */
    private $_vendor = null;
    /**
     * @var null
     */
    private $_zipCode = null;

    private $_postPoneDays = null;

    /**
     * Return Calendar Days for Product Id or SKU
     * @param string $productId
     * @return ApiResponseInterface|void
     */
    public function getCalendarForProduct($productId)
    {
        try {
            $this->_productId = $productId;
            $this->_zipCode = $this->request->getParam('zip_code');
            $this->validateProduct();
            $response = $this->getVendorDeliveryData();
        } catch (LocalizedException $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        $result['response'] = $response;
        return $result;
    }

    /**
     * Return Delivery Dates and details
     * @return array|ApiResponseInterface
     * @throws LocalizedException
     */
    public function getVendorDeliveryData()
    {
        $storeCode = $this->getStoreCode();
        $globalDeliveryDatesDataArray = $response = $offerVendorIds = [];
        /**
         * Get Vendors who have active offers and inventory qty > 0
         */
        $vendorArray = explode(",", $this->_vendor);
        $offerParams['vendor_id'] = $vendorArray;
        $offerParams['sku'] = $this->_product->getSku();
        $offerParams['exclude_zero_inventory'] = 'yes';
        $offerCollection = $this->vendorHelper->getVendorInventoryOfferCollection($offerParams);
        if ($offerCollection->getSize() > 0) {
            foreach ($offerCollection as $offer) {
                $offerVendorIds[] = $offer->getData('vendor_id');
            }
        } else {
            throw new LocalizedException(__("This product doesn't have any offers"));
        }
        $vendorCollection = $this->vendorHelper->getVendorCollectionByIds($offerVendorIds);
        foreach ($vendorCollection as $vendor) {
            $vendorName = $vendor->getData('vendor_name') . ' ' . $vendor->getData('last_name');
            $vendorId = $vendor->getId();
            $vendorLocation = $this->vendorHelper->getActiveFirstVendorLocationByVendorId($vendor->getId());
            foreach ($vendorLocation as $location) {
                $vendorFarmName = $location->getData('loc_name');
                $vendorFarmId = $location->getId();
                $logisticsCollection = $this->vendorHelper->getActiveLogisticsCollectionByLocationId($location->getId(), $storeCode, $this->_zipCode);
                foreach ($logisticsCollection as $oneLogistic) {
                    $shippingId = $oneLogistic->getData('shipping_method');
                    $shippingMethodName = $oneLogistic->getData('method_name');
                    $priority = $oneLogistic->getData('priority');
                    $shipAndDeliveryDaysArray = $this->getShippingAndDeliveryDays($oneLogistic, $vendor->getId(), $location->getId());
                    foreach ($shipAndDeliveryDaysArray as $shipDate => $deliveryDate) {
                        /**
                         * Check for priority of logistic if same delivery dates
                         * if same priority take least vendor id data as delivery
                         * 1 > 2 -- replace , 2 > 1 --- skip
                         */
                        if (isset($globalDeliveryDatesDataArray[$deliveryDate])) {
                            $currentPriority = (int)$priority;
                            $previousPriority = (int)$globalDeliveryDatesDataArray[$deliveryDate]['priority'];
                            if ($currentPriority > $previousPriority) {
                                continue;
                            } else if ($currentPriority < $previousPriority) {
                                $globalDeliveryDatesDataArray[$deliveryDate] = [
                                    'vendor' => $vendorId,
                                    'vendor_farm' => $vendorFarmId,
                                    'shipping_method' => $shippingId,
                                    'pickup_date' => $shipDate,
                                    'vendor_front_name' => $vendorName,
                                    'vendor_farm_name' => $vendorFarmName,
                                    'shipping_front_name' => $shippingMethodName,
                                    'priority' => $priority
                                ];
                                continue;
                            } else {
                                /**
                                 * if both are equal take least vendor Id
                                 */
                                $currentVendorId = (int)$vendorId;
                                $previousVendorId = (int)$globalDeliveryDatesDataArray[$deliveryDate]['vendor'];
                                if ($currentVendorId < $previousVendorId) {
                                    $globalDeliveryDatesDataArray[$deliveryDate] = [
                                        'vendor' => $vendorId,
                                        'vendor_farm' => $vendorFarmId,
                                        'shipping_method' => $shippingId,
                                        'pickup_date' => $shipDate,
                                        'vendor_front_name' => $vendorName,
                                        'vendor_farm_name' => $vendorFarmName,
                                        'shipping_front_name' => $shippingMethodName,
                                        'priority' => $priority
                                    ];
                                    continue;
                                }
                            }
                        }
                        $globalDeliveryDatesDataArray[$deliveryDate] = [
                            'vendor' => $vendorId,
                            'vendor_farm' => $vendorFarmId,
                            'shipping_method' => $shippingId,
                            'pickup_date' => $shipDate,
                            'vendor_front_name' => $vendorName,
                            'vendor_farm_name' => $vendorFarmName,
                            'shipping_front_name' => $shippingMethodName,
                            'priority' => $priority
                        ];
                    }
                }
            }
        }
        ksort($globalDeliveryDatesDataArray);
        $result = [
            'status' => true,
            'delivery_dates' => array_keys($globalDeliveryDatesDataArray),
            'delivery_details' => $globalDeliveryDatesDataArray
        ];
        $response['response'] = $result;
        return $response;
    }

    /**
     * Return Ship Dates and Corresponding Delivery Days
     * @param Logistics $oneLogistic
     * @param $vendorId
     * @param $vendorLocationId
     * @return array
     */
    public function getShippingAndDeliveryDays(Logistics $oneLogistic, $vendorId, $vendorLocationId)
    {
        $shippingId = $oneLogistic->getData('shipping_method');
        /**
         * Get Block dates array
         */
        $shippingBlackoutDaysArray = $this->getBlockOutDays($vendorId, $vendorLocationId, $shippingId, 'shipping');
        $deliveryBlackoutDaysArray = $this->getBlockOutDays($vendorId, $vendorLocationId, $shippingId, 'delivery');

        /**
         * Backend configuration for calendar days
         */
        $calendarConfigDays = $this->calendarHelper->getConfig(self::CALENDAR_CONFIG_PATH);

        /**
         * Get Warehouse data
         */
        $wareHouseOpenDaysWithCloseTimeArray = $this->getWareHouseOpenDays($oneLogistic);
        $wareHouseOpenDays = array_keys($wareHouseOpenDaysWithCloseTimeArray);

        $fromDay = date('Y-m-d');
        $dayVariable = "+" . $calendarConfigDays . ' days';
        $endDay = date('Y-m-d', strtotime($dayVariable, strtotime($fromDay)));
        $possibleShipAndDeliveryDates = [];
        while ($fromDay <= $endDay) {
            if (in_array(date('l', strtotime($fromDay)), $wareHouseOpenDays)) {
                /**
                 * check if today's timout is over or not
                 */
                if (strtotime(date('Y-m-d')) == strtotime($fromDay)) {
                    $openCloseTimeArray = $wareHouseOpenDaysWithCloseTimeArray[date('l')];
                    $timeArray = explode('-', $openCloseTimeArray);
                    if (isset($timeArray[1])) {
                        /**
                         * if warehouse close time is less than current time
                         * then allow it else skipping today
                         */
                        if (strtotime($timeArray[1]) < strtotime(date('H:i'))) {
                            $fromDay = date('Y-m-d', strtotime('+1 days', strtotime($fromDay)));
                            continue;
                        }
                    }
                }
                /**
                 * check if the date is in shipping block date array , if it is skip
                 */
                if (!in_array($fromDay, $shippingBlackoutDaysArray)) {
                    $deliveryDate = null;
                    $shipDate = $fromDay;
                    /**
                     * add lead time and buffer days to get delivery date
                     * and check if that delivery date is delivery block array if exist add postpone days
                     * if postpone days also blank then ignore shipping date
                     */
                    $leadTime = (int)$oneLogistic->getData('lead_time');

                    if ($leadTime >= 0) {
                        $deliVariable = "+" . (int)$leadTime . ' days';

                        $deliveryDate = date('Y-m-d', strtotime($deliVariable, strtotime($shipDate)));
                        $bufferDays = (int)$oneLogistic->getData('buffer_days');
                        if ($bufferDays >= 0) {
                            $deliVariable = "+" . (int)$bufferDays . ' days';
                            $deliveryDate = date('Y-m-d', strtotime($deliVariable, strtotime($deliveryDate)));
                        }
                        /**
                         * check if delivery date is in deliver block date array if exist
                         * then check for postpone days for the vendor,location,logistic
                         * if postpone days also not there then skip ship date
                         */
                        if (in_array($deliveryDate, $deliveryBlackoutDaysArray)) {
                            $postPoneArray = $this->_postPoneDays;
                            if (isset($postPoneArray[$vendorId][$vendorLocationId][$shippingId])) {
                                $postPoneDay = (int)$postPoneArray[$vendorId][$vendorLocationId][$shippingId];
                                if ($postPoneDay != 0) {
                                    $deliVariable = "+" . $postPoneDay . ' days';
                                    $deliveryDate = date('Y-m-d', strtotime($deliVariable, strtotime($deliveryDate)));
                                } else {
                                    $deliveryDate = null;
                                }
                            } else {
                                $deliveryDate = null;
                            }
                        }
                    }
                    if ($deliveryDate != null) {
                        $possibleShipAndDeliveryDates[$shipDate] = $deliveryDate;
                    }
                }
            }
            $fromDay = date('Y-m-d', strtotime('+1 days', strtotime($fromDay)));
        }
        return $possibleShipAndDeliveryDates;
    }

    /**
     * Return Array of Blackout Days 0 => shipping , 1 => delivery
     * $type = delivery , shipping
     * @param $vendorId
     * @param $locationId
     * @param $shipMethodId
     * @param $type
     * @return array
     */
    public function getBlockOutDays($vendorId, $locationId, $shipMethodId, $type)
    {
        $postPoneDaysArray = [];
        $blockType = ($type == 'shipping') ? self::SHIPPING_BLACKOUT_TYPE : self::DELIVERY_BLACKOUT_TYPE;
        $blockOutCollection = $this->vendorHelper->getVendorBlackoutCollection(
            $vendorId,
            $locationId,
            $shipMethodId,
            $blockType
        );
        $blockOutDatesWithDuplicate = [];
        foreach ($blockOutCollection as $oneBlackOut) {
            if ($type == 'delivery' && !empty($oneBlackOut->getData('postpone_days'))) {
                $postPoneDaysArray[$vendorId][$locationId][$shipMethodId] = $oneBlackOut->getData('postpone_days');
            }
            $startDate = date('Y-m-d', strtotime($oneBlackOut->getData('start_date')));
            $endDate = date('Y-m-d', strtotime($oneBlackOut->getData('end_date')));
            $blockOutDatesWithDuplicate = array_merge($blockOutDatesWithDuplicate, $this->getDateRange($startDate, $endDate));
        }
        if (count($blockOutDatesWithDuplicate) > 0) {
            $this->_postPoneDays = $postPoneDaysArray;
            $blockOutDates = array_unique($blockOutDatesWithDuplicate);
            asort($blockOutDates);
            return $blockOutDates;
        }
        return [];
    }

    /**
     * Return Date Ranges between two dates
     * @param $first
     * @param $last
     * @param string $step
     * @param string $output_format
     * @return array
     */
    public function getDateRange($first, $last, $step = '+1 day', $output_format = 'Y-m-d')
    {
        $dates = [];
        $current = strtotime($first);
        $last = strtotime($last);
        while ($current <= $last) {
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
        return $dates;
    }

    /**
     * Return WareHouse Open days array for the logistic
     * @param $oneLogistic
     * @return array
     */
    public function getWareHouseOpenDays(Logistics $oneLogistic)
    {
        $daysArray = $this->vendorHelper->getDaysArray();
        $wareHouseOpenDaysArray = [];
        foreach ($daysArray as $day => $value) {
            $dayData = explode(",", $oneLogistic->getData($day));
            if (isset($dayData[1]) && $dayData[1] == '1') {
                /**
                 * create , day => warehouse close time array
                 * to check close time of today's
                 */
                $wareHouseOpenDaysArray[$value] = $dayData[0];
            }
        }
        return $wareHouseOpenDaysArray;
    }

    /**
     * Return store Code
     * @return string
     */
    public function getStoreCode()
    {
        try {
            return $this->storeManager->getStore()->getCode();
        } catch (\Exception $e) {
            return 'default';
        }
    }

    /**
     * Validate and set Vendor for current Product
     * @return bool
     * @throws LocalizedException
     */
    public function validateProduct()
    {
        $product = $this->product->load($this->_productId);
        if ($product->getId() == null) {
            /**
             * check for SKU
             */
            $product = $this->product->loadByAttribute('sku', $this->_productId);
            if ($product == null) {
                throw new LocalizedException(__("This product doesn't Exist"));
            } else {
                $this->_productId = $product->getId();
            }
        }
        $this->_product = $product;
        $this->_vendor = $product->getVendor();
        return true;
    }

    public function execute()
    {
        /* throw new LocalizedException(__("Invalid Product Id"));*/
        // TODO: Implement execute() method.
    }


}