<?php

namespace Flordelcampo\Vendor\Model\Config\Source;

use Flordelcampo\Vendor\Helper\Data as VendorHelper;

class DefaultPickup
{
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    public function __construct(
        VendorHelper $vendorHelper
    )
    {
        $this->vendorHelper = $vendorHelper;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAttributeSetArray();
    }

    /**
     * @return array
     */
    public function getAttributeSetArray()
    {
        return [
            ['value' => 'today', 'label' => 'Today'],
            ['value' => 'tomorrow', 'label' => 'Tomorrow'],
        ];
    }
}