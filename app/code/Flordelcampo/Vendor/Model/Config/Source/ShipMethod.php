<?php

namespace Flordelcampo\Vendor\Model\Config\Source;

use Flordelcampo\Vendor\Helper\Data as VendorHelper;

class ShipMethod
{
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    public function __construct(
        VendorHelper $vendorHelper
    )
    {
        $this->vendorHelper = $vendorHelper;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAttributeSetArray();
    }

    /**
     * @return array
     */
    public function getAttributeSetArray()
    {
        $shippingNames = [];
        $options = $this->vendorHelper->getAllShippingMethodsArray();
        foreach ($options as $shipId => $shipName) {
            if ($shipId != '') {
                $shippingNames[] = ['value' => $shipId, 'label' => $shipName];
            }
        }
        return $shippingNames;
    }
}