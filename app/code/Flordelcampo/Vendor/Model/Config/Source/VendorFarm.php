<?php

namespace Flordelcampo\Vendor\Model\Config\Source;

use Flordelcampo\Vendor\Helper\Data as VendorHelper;

class VendorFarm
{
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    public function __construct(
        VendorHelper $vendorHelper
    )
    {
        $this->vendorHelper = $vendorHelper;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAttributeSetArray();
    }

    /**
     * @return array
     */
    public function getAttributeSetArray()
    {
        $vendorFarms = [];
        $options = $this->vendorHelper->getAllActiveFarmsArray();
        foreach ($options as $locId => $locName) {
            if ($locId != '') {
                $vendorFarms[] = ['value' => $locId, 'label' => $locName];
            }
        }
        return $vendorFarms;
    }
}