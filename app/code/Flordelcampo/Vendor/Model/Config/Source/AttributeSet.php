<?php

namespace Flordelcampo\Vendor\Model\Config\Source;

use Flordelcampo\Vendor\Helper\Data as VendorHelper;

class AttributeSet
{
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    public function __construct(
        VendorHelper $vendorHelper
    ){
        $this->vendorHelper = $vendorHelper;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAttributeSetArray();
    }

    /**
     * @return array
     */
    public function getAttributeSetArray()
    {
        $attributeSetArray = [];
        $options = $this->vendorHelper->getAttributeSetOptionsArray();
        foreach ($options as $option) {
            $attributeSetArray[] = ['value' => $option['value'], 'label' => $option['label']];
        }
        return $attributeSetArray;
    }
}