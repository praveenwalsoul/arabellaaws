<?php

namespace Flordelcampo\Vendor\Model\Config\Source;

use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Customer\Model\ResourceModel\Group\Collection;

class CustomerGroups
{
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Collection
     */
    protected $customerGroupColl;

    public function __construct(
        VendorHelper $vendorHelper,
        Collection $customerGroupColl
    )
    {
        $this->vendorHelper = $vendorHelper;
        $this->customerGroupColl = $customerGroupColl;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getCustomerGroupsArray();
    }

    /**
     * @return array
     */
    public function getCustomerGroupsArray()
    {
        return $this->customerGroupColl->toOptionArray();
    }
}