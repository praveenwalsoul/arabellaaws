<?php

namespace Flordelcampo\Vendor\Helper;

use Flordelcampo\Order\Model\Box;
use Flordelcampo\VendorInventory\Model\HardGood;
use Flordelcampo\VendorInventory\Model\Inventory;
use Flordelcampo\VendorInventory\Model\Log;
use Flordelcampo\VendorRegistration\Model\Logistics;
use Flordelcampo\VendorRegistration\Model\VendorUser;
use Magento\Backend\Model\Auth\Session;
use Magento\Catalog\Model\Product\AttributeSet\Options;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;
use Magento\Framework\App\Helper\Context;
use Flordelcampo\VendorRegistration\Model\Vendor as VendorFactory;
use Flordelcampo\DropshipLocations\Model\Location as VendorLocationFactory;
use Flordelcampo\VendorShipping\Model\Shipping as ShippingFactory;
use Flordelcampo\VendorCostchannel\Model\Costchannel as CostChannelFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Directory\Model\Country;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Flordelcampo\VendorRegistration\Model\VendorUserFactory;
use Flordelcampo\VendorRegistration\Model\LogisticsFactory;
use Flordelcampo\VendorBlackOutDays\Model\BlackoutFactory;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Directory\Model\Region;
use Flordelcampo\VendorInventory\Model\InventoryFactory;
use Flordelcampo\VendorInventory\Model\LogFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\User\Model\User;
use Magento\Sales\Model\Order\ItemFactory;
use Flordelcampo\Order\Model\BoxFactory;
use Flordelcampo\VendorInventory\Model\HardGoodFactory;
use Flordelcampo\VendorInventory\Model\HardGoodOrderItemFactory;
use Flordelcampo\VendorInventory\Model\VendorConfirmFactory;

/**
 * Class Data
 *
 * Flordelcampo\Vendor\Data
 */
class Data extends AbstractHelper
{
    const VENDOR_TYPE_CONFIGURATION = 'flordel_vendor_configuration/general/vendor_type';
    const HARD_GOOD_ID = 'flordel_vendor_configuration/general/hard_good_id';
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;
    /**
     * @var VendorFactory
     */
    protected $vendorFactory;
    /**
     * @var VendorLocationFactory
     */
    protected $vendorLocationFactory;
    /**
     * @var Country
     */
    protected $country;
    /**
     * @var CollectionFactory
     */
    protected $countryCollectionFactory;
    /**
     * @var VendorUserFactory
     */
    protected $vendorUserFactory;
    /**
     * @var LogisticsFactory
     */
    protected $logisticsFactory;
    /**
     * @var ShippingFactory
     */
    protected $shippingFactory;
    /**
     * @var CostChannelFactory
     */
    protected $costChannelFactory;
    /**
     * @var BlackoutFactory
     */
    protected $blackoutFactory;
    /**
     * @var AttributeSetRepositoryInterface
     */
    protected $attributeSetRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var Options
     */
    protected $attributeSetOptions;
    /**
     * @var Region
     */
    protected $region;
    /**
     * @var CountryFactory
     */
    protected $countryFactory;
    /**
     * @var InventoryFactory
     */
    protected $inventoryFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var LogFactory
     */
    protected $logFactory;
    /**
     * @var Session
     */
    protected $authSession;
    /**
     * @var ItemFactory
     */
    protected $itemFactory;
    /**
     * @var BoxFactory
     */
    protected $boxFactory;
    /**
     * @var HardGoodFactory
     */
    protected $hardGoodFactory;
    /**
     * @var HardGoodOrderItemFactory
     */
    protected $hardGoodOrderItemFactory;
    /**
     * @var VendorConfirmFactory
     */
    protected $vendorConfirmFactory;

    /**
     * Data constructor.
     * @param Context $context
     * @param CatalogHelper $catalogHelper
     * @param VendorFactory $vendorFactory
     * @param VendorLocationFactory $vendorLocationFactory
     * @param Country $country
     * @param CollectionFactory $countryCollectionFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param VendorUserFactory $vendorUserFactory
     * @param LogisticsFactory $logisticsFactory
     * @param ShippingFactory $shippingFactory
     * @param CostChannelFactory $costChannelFactory
     * @param BlackoutFactory $blackoutFactory
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Options $attributeSetOptions
     * @param Region $region
     * @param CountryFactory $countryFactory
     * @param InventoryFactory $inventoryFactory
     * @param LogFactory $logFactory
     * @param ResourceConnection $resourceConnection
     * @param Session $authSession
     * @param ItemFactory $itemFactory
     * @param BoxFactory $boxFactory
     * @param HardGoodFactory $hardGoodFactory
     * @param HardGoodOrderItemFactory $hardGoodOrderItemFactory
     * @param VendorConfirmFactory $vendorConfirmFactory
     */
    public function __construct(
        Context $context,
        CatalogHelper $catalogHelper,
        VendorFactory $vendorFactory,
        VendorLocationFactory $vendorLocationFactory,
        Country $country,
        CollectionFactory $countryCollectionFactory,
        ScopeConfigInterface $scopeConfig,
        VendorUserFactory $vendorUserFactory,
        LogisticsFactory $logisticsFactory,
        ShippingFactory $shippingFactory,
        CostChannelFactory $costChannelFactory,
        BlackoutFactory $blackoutFactory,
        AttributeSetRepositoryInterface $attributeSetRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Options $attributeSetOptions,
        Region $region,
        CountryFactory $countryFactory,
        InventoryFactory $inventoryFactory,
        LogFactory $logFactory,
        ResourceConnection $resourceConnection,
        Session $authSession,
        ItemFactory $itemFactory,
        BoxFactory $boxFactory,
        HardGoodFactory $hardGoodFactory,
        HardGoodOrderItemFactory $hardGoodOrderItemFactory,
        VendorConfirmFactory $vendorConfirmFactory
    )
    {
        $this->country = $country;
        $this->catalogHelper = $catalogHelper;
        $this->vendorFactory = $vendorFactory;
        $this->vendorLocationFactory = $vendorLocationFactory;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->vendorUserFactory = $vendorUserFactory;
        $this->logisticsFactory = $logisticsFactory;
        $this->shippingFactory = $shippingFactory;
        $this->costChannelFactory = $costChannelFactory;
        $this->blackoutFactory = $blackoutFactory;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->attributeSetOptions = $attributeSetOptions;
        $this->region = $region;
        $this->countryFactory = $countryFactory;
        $this->inventoryFactory = $inventoryFactory;
        $this->logFactory = $logFactory;
        $this->resourceConnection = $resourceConnection;
        $this->itemFactory = $itemFactory;
        parent::__construct($context);
        $this->boxFactory = $boxFactory;
        $this->authSession = $authSession;
        $this->hardGoodFactory = $hardGoodFactory;
        $this->hardGoodOrderItemFactory = $hardGoodOrderItemFactory;
        $this->vendorConfirmFactory = $vendorConfirmFactory;
    }

    /**
     * @return User|null
     */
    public function getCurrentAdminUser()
    {
        return $this->authSession->getUser();
    }

    /**
     * Get All Vendors List For Fedex Dashboard
     * @return array
     */

    public function getAllVendorListArray1()
    {
        $vendorsArray = [];
        $vendorsArray['0'] = 'All Vendor';
        $collections = $this->vendorFactory->getCollection();
        foreach ($collections as $value) {
            //$vendorsArray[$value->getVendorId()] = $value->getVendorName();
            $vendorsArray[$value->getId()] = $value->getData('vendor_name') . ' ' . $value->getData('last_name');
        }
        return $vendorsArray;
    }

    /**
     * Return All Vendors
     * @return array
     */
    public function getAllActiveAndInactiveVendorList()
    {
        $vendorsArray = [];
        $collections = $this->vendorFactory->getCollection();
        foreach ($collections as $value) {
            $vendorsArray[$value->getId()] = $value->getData('vendor_name') . ' ' . $value->getData('last_name');
        }
        asort($vendorsArray);
        return $vendorsArray;
    }

    /**
     * Return All Vendor Farms
     * @return array
     */
    public function getAllActiveAndInactiveVendorFarmsList()
    {
        $vendorLocationList = [];
        $vendorLocationCollection = $this->vendorLocationFactory->getCollection();
        if ($vendorLocationCollection->getSize() > 0) {
            foreach ($vendorLocationCollection as $oneLocation) {
                $vendorLocationList[$oneLocation->getId()] = $oneLocation->getData('loc_name');
            }
        }
        asort($vendorLocationList);
        return $vendorLocationList;
    }

    /**
     * Get All Vendors List For Create Order
     * @param null $vendorType
     * @return array
     */
    public function getAllVendorListArray($vendorType = null)
    {
        $vendorList[''] = 'All';
        $collection = $this->vendorFactory->getCollection()
            ->addFieldToFilter('status', '1');
        if ($vendorType) {
            $collection->addFieldToFilter('vendor_type', $vendorType);
        }
        if ($collection->getSize() > 0) {
            foreach ($collection as $vendor) {
                $vendorList[$vendor->getId()] = $vendor->getData('vendor_name') . ' ' . $vendor->getData('last_name');
            }
        }
        asort($vendorList);
        return $vendorList;
    }

    /**
     * Return Vendor Collection by Id or Array
     *
     * @param $vendorId
     * @return array
     */
    public function getVendorCollectionByVendorId($vendorId)
    {
        if (is_array($vendorId)) {
            return [
                '' => 'Select Vendor',
                '1' => 'Timana',
                '2' => 'Kabloom'
            ];
            // TODO return vendor collection with array filter
        } else {
            return [];
            // TODO return vendor collection by Id
        }
    }

    /**
     * Get All Vendors List For Create Order
     * @return array
     */
    public function getAllVendorListArrayForCreateOrder()
    {
        $vendorList[''] = __('Select Vendor');
        $collection = $this->vendorFactory->getCollection()
            ->addFieldToFilter('status', '1');
        if ($collection->getSize() > 0) {
            foreach ($collection as $vendor) {
                $vendorList[$vendor->getId()] = $vendor->getData('vendor_name') . ' ' . $vendor->getData('last_name');
            }
        }
        //asort($vendorList);
        return $vendorList;
    }

    /**
     * Return All Active Vendor Farms Array
     *
     * @return array
     */
    public function getAllVendorFarmsArray()
    {
        try {
            $vendorFarms = [];
            $vendorLocationCollection = $this->vendorLocationFactory->getCollection();
            $vendorLocationCollection->addFieldToFilter('main_table.status', '1');
            $vendorLocationCollection->getSelect()
                ->joinLeft(
                    ['vendor' => 'vendor_registrations'],
                    "main_table.vendor_id=vendor.vendor_id",
                    [
                        'vendor_name' => 'vendor.vendor_name',
                        'last_name' => 'vendor.last_name'
                    ]
                );
            if ($vendorLocationCollection->getSize() > 0) {
                foreach ($vendorLocationCollection as $location) {
                    $vendorFarms[$location->getData('vendor_id')][$location->getId()] = $location->getData('loc_name');
                }
            }
            return $vendorFarms;
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * Return Product Collection for vendor ID or array
     * @param $vendorId
     * @return Collection
     */
    public function getVendorAssociateProductCollection($vendorId)
    {
        return $this->catalogHelper->getProductCollectionByVendor($vendorId);
    }

    /**
     * Return All Active Farms
     *
     * @return array
     */
    public function getAllActiveFarmsArray()
    {
        $vendorLocationList[''] = 'All';
        $vendorLocationCollection = $this->vendorLocationFactory->getCollection();
        $vendorLocationCollection->addFieldToFilter('status', '1');
        if ($vendorLocationCollection->getSize() > 0) {
            foreach ($vendorLocationCollection as $oneLocation) {
                $vendorLocationList[$oneLocation->getId()] = $oneLocation->getData('loc_name');
            }
        }
        asort($vendorLocationList);
        return $vendorLocationList;
    }

    /**
     * Return All Active Farms for Vendor Type
     * @param null $vendorType
     * @return mixed
     */
    public function getAllActiveFarmsForVendorType($vendorType = null)
    {
        $vendorLocationList[''] = 'All';
        $vendorLocationCollection = $this->vendorLocationFactory->getCollection();
        $vendorLocationCollection->addFieldToFilter('status', '1');
        if ($vendorType) {
            $collection = $this->vendorFactory->getCollection()
                ->addFieldToFilter('status', '1');
            $collection->addFieldToFilter('vendor_type', $vendorType);
            $vendorIds = [];
            if ($collection->getSize() > 0) {
                foreach ($collection as $vendor) {
                    $vendorIds[] = $vendor->getId();
                }
            }
            $vendorLocationCollection->addFieldToFilter('vendor_id', ['in' => $vendorIds]);
        }
        if ($vendorLocationCollection->getSize() > 0) {
            foreach ($vendorLocationCollection as $oneLocation) {
                $vendorLocationList[$oneLocation->getId()] = $oneLocation->getData('loc_name');
            }
        }
        asort($vendorLocationList);
        return $vendorLocationList;
    }

    /**
     * Return Active Vendor Farm By Vendor Id
     * @param $vendorId
     * @return mixed
     */
    public function getActiveFarmsByVendorId($vendorId)
    {
        $vendorLocationList[''] = 'Please Select Farm';
        try {
            if ($vendorId != null) {
                $vendorLocationCollection = $this->vendorLocationFactory->getCollection();
                $vendorLocationCollection->addFieldToFilter('vendor_id', $vendorId);
                $vendorLocationCollection->addFieldToFilter('status', '1');
                if ($vendorLocationCollection->getSize() > 0) {
                    foreach ($vendorLocationCollection as $oneLocation) {
                        $vendorLocationList[$oneLocation->getId()] = $oneLocation->getData('loc_name');
                    }
                    asort($vendorLocationList);
                }
                return $vendorLocationList;
            }
        } catch (\Exception $e) {
            return $vendorLocationList;
        }
    }

    /**
     * Vendor Data By SKU
     * @param $sku
     * @return array|string[]|null
     * @throws NoSuchEntityException
     */
    public function getVendorDataBySku($sku)
    {
        $vendorIds = $this->catalogHelper->getVendorsBySku($sku);
        if (empty($vendorIds)) {
            return null;
        }
        $vendorData = [];
        /**
         * convert comma separated vendor ids to array
         */
        $vendorArray = explode(",", $vendorIds);
        $vendorData = $this->getVendorCollectionByVendorId($vendorArray);
        return $vendorData;
    }

    /**
     * Return Vendor By Vendor Id
     * @param $vendorId
     * @return mixed
     */
    public function getVendorModelById($vendorId)
    {
        return $this->vendorFactory->load($vendorId);
    }

    /**
     * Return Vendor Name by Id
     * @param $vendorId
     * @return string
     */
    public function getVendorNameByVendorId($vendorId)
    {
        $vendor = $this->getVendorModelById($vendorId);
        return $vendor->getData('vendor_name') . ' ' . $vendor->getData('last_name');
    }

    /**
     * Return Vendor Collection For Comma Separated Ids
     * @param $vendorIds
     * @return mixed
     */
    public function getVendorCollectionByIds($vendorIds)
    {
        return $this->vendorFactory->getCollection()
            ->addFieldToFilter('vendor_id', ['in' => $vendorIds]);
    }

    /**
     * Return Vendor model
     * @return mixed
     */
    public function getVendorModel()
    {
        return $this->vendorFactory;
    }

    /**
     * Vendor Collection By Email
     *
     * @param $email
     * @return AbstractCollection|null
     */
    public function getVendorCollectionByEmail($email)
    {
        $vendorCollection = $this->vendorFactory->getCollection();
        $vendorCollection->addFieldToFilter('email', $email);
        if (count($vendorCollection) > 0) {
            return $vendorCollection;
        } else {
            return null;
        }
    }

    /**
     * Return Location Model for CRUD
     * @return mixed
     */
    public function getVendorLocationModel()
    {
        return $this->vendorLocationFactory;
    }


    /**
     * Return vendor Location
     * @return mixed
     */
    public function getAllVendorLocation()
    {
        $locCollection = $this->vendorLocationFactory->getCollection();
        $locName[''] = "All Location";
        foreach ($locCollection as $oneLoc) {
            $code = $oneLoc->getData('loc_id');
            $name = $oneLoc->getData('loc_name');
            $locName[$code] = $name;
        }
        return $locName;
    }


    /**
     * Return Shipping Methods
     * @return mixed
     */
    public function getAllShippingMethodsArray()
    {
        $shippingCollection = $this->shippingFactory->getCollection();
        $shippingMethods[''] = "Please Select";
        foreach ($shippingCollection as $oneShipping) {
            $code = $oneShipping->getData('shipping_method_id');
            $name = $oneShipping->getData('shipping_method_name');
            $shippingMethods[$code] = $name;
        }
        asort($shippingMethods);
        return $shippingMethods;
    }

    /**
     * Return Shipping Model By Id
     * @param $id
     * @return ShippingFactory
     */
    public function getShippingModelById($id)
    {
        return $this->shippingFactory->load($id);
    }

    /**
     * Return All Cost Channel Array
     * @return mixed
     */
    public function getAllCostChannelArray()
    {
        $costChannelCollection = $this->costChannelFactory->getCollection();
        $costChannel[''] = "Please Select";
        foreach ($costChannelCollection as $oneChannel) {
            $code = $oneChannel->getData('cost_channel_id');
            $name = $oneChannel->getData('cost_channel_name');
            $costChannel[$code] = $name;
        }
        return $costChannel;
    }

    /**
     * @param $locationId
     * @return VendorLocationFactory
     */
    public function getVendorLocationModelByLocationId($locationId)
    {
        return $this->getVendorLocationModel()->load($locationId);
    }

    /**
     * Return Vendor Collection
     * @param $vendorId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorLocationsCollectionByVendorId($vendorId)
    {
        $vendorLocationCollection = $this->vendorLocationFactory->getCollection();
        $vendorLocationCollection->addFieldToFilter('vendor_id', $vendorId);
        return $vendorLocationCollection;
    }

    /**
     * Return Active and First Location for vendor
     * @param $vendorId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getActiveFirstVendorLocationByVendorId($vendorId)
    {
        $collection = $this->vendorLocationFactory->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('status', '1');
        $collection->getSelect()->limit(1);
        return $collection;
    }

    /**
     * Return Vendor User Model For CRUD Operation
     * @return VendorUser
     */
    public function getVendorUserModel()
    {
        return $this->vendorUserFactory->create();
    }

    /**
     * Return Log Model
     * @return Log
     */
    public function getInventoryLogModel()
    {
        return $this->logFactory->create();
    }

    /**
     * Return Box Model
     * @return Box
     */
    public function getBoxModel()
    {
        return $this->boxFactory->create();
    }

    /**
     * Return Box Model
     * @param $id
     * @return Box
     */
    public function getBoxModelById($id)
    {
        return $this->getBoxModel()->load($id);
    }

    /**
     * Return Box Collection
     * @return AbstractDb|AbstractCollection|null
     */
    public function getBoxCollection()
    {
        return $this->getBoxModel()->getCollection();
    }

    /**
     * Return Log model by Id
     * @param $id
     * @return Log
     */
    public function getInventoryLogModelById($id)
    {
        return $this->logFactory->create()->load($id);
    }

    /**
     * Return Log collection by Offer Id
     * @param $offerId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getInventoryLogCollectionByOfferId($offerId)
    {
        return $this->getInventoryLogModel()->getCollection()
            ->addFieldToFilter('offer_id', $offerId);
    }

    /**
     * Return Log Collection
     * @return AbstractDb|AbstractCollection|null
     */
    public function getInventoryLogCollection()
    {
        return $this->getInventoryLogModel()->getCollection();
    }

    /**
     * Return Model
     * @return Inventory
     */
    public function getVendorInventoryModel()
    {
        return $this->inventoryFactory->create();
    }

    /**
     * Return Inventory Collection
     * @param null $params
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorInventoryOfferCollection($params = null)
    {
        if ($params != null) {
            $collection = $this->inventoryFactory->create()->getCollection();
            if (isset($params['vendor_type'])) {
                $collection->addFieldToFilter('main_table.vendor_type', $params['vendor_type']);
            }
            if (isset($params['vendor_id'])) {
                if (is_array($params['vendor_id'])) {
                    $collection->addFieldToFilter('main_table.vendor_id', ['in' => $params['vendor_id']]);
                } else {
                    $collection->addFieldToFilter('main_table.vendor_id', $params['vendor_id']);
                }
            }
            if (isset($params['sku'])) {
                $sku = $params['sku'];
                if (is_array($sku)) {
                    $collection->addFieldToFilter('main_table.sku', ['in' => $sku]);
                } else {
                    $collection->addFieldToFilter('main_table.sku', $params['sku']);
                }
            }
            if (isset($params['store_id'])) {
                $storeId = $params['store_id'];
                if (is_array($storeId)) {
                    $collection->addFieldToFilter('main_table.store_id', ['in' => $storeId]);
                } else {
                    $collection->addFieldToFilter('main_table.store_id', $storeId);
                }
            }
            if (isset($params['exclude_zero_inventory'])) {
                $collection->addFieldToFilter('main_table.inventory_qty', ['gt' => '0']);
            }
            if (isset($params['loc_id'])) {
                $collection->addFieldToFilter('main_table.loc_id', $params['loc_id']);
            }
            if (isset($params['product_type'])) {
                $collection->addFieldToFilter('main_table.product_type_id', $params['product_type']);
            }
            if (isset($params['start_date'])) {
                $collection->addFieldToFilter('main_table.start_date', ['gteq' => $params['start_date']]);
                //$collection->addFieldToFilter('expiry_date', ['lteq' => $params['start_date']]);
            }
            if (!isset($params['status'])) {
                $collection->addFieldToFilter('main_table.status', '1');
            }
            if (isset($params['status']) && $params['status'] != '') {
                $collection->addFieldToFilter('main_table.status', $params['status']);
            }
            return $collection;
        }
        return $this->inventoryFactory->create()->getCollection();
    }

    /**
     * Return Order Count for SKU array
     * @param $sku
     * @return array
     */
    public function getOrderCountBySku($sku)
    {
        $notIn = ['cancelled', 'rejected'];
        $itemCollection = $this->itemFactory->create()->getCollection();
        if (is_array($sku)) {
            $itemCollection->addFieldToFilter('main_table.sku', ['in' => $sku]);
        } else {
            $itemCollection->addFieldToFilter('main_table.sku', $sku);
        }
        $notInQuery = "'" . implode("', '", $notIn) . "'";
        $itemCollection->getSelect()
            ->joinLeft(
                ['item_box' => 'order_item_box'],
                "main_table.item_id=item_box.item_id",
                [
                    'box_count' => 'COUNT(item_box.box_id)'
                ]
            )
            ->where("item_box.status NOT IN (" . $notInQuery . ")")
            ->group('main_table.sku');
        $skuWiseBoxCount = [];
        if ($itemCollection->getSize() > 0) {
            foreach ($itemCollection as $item) {
                $sku = $item->getData('sku');
                $boxCount = $item->getData('box_count');
                $skuWiseBoxCount[$sku] = $boxCount;
            }
        }
        return $skuWiseBoxCount;
    }

    /**
     * Return Order Count For Offer Id
     * @param $offerId
     * @param null $vendorId
     * @param null $locationId
     * @return array
     */
    public function getOrderCountByOfferId($offerId, $vendorId = null, $locationId = null)
    {
        $notIn = ['cancelled', 'rejected'];
        $notInQuery = "'" . implode("', '", $notIn) . "'";
        $itemCollection = $this->itemFactory->create()->getCollection();
        if (is_array($offerId)) {
            $itemCollection->addFieldToFilter('main_table.offer_avail_id', ['in' => $offerId]);
        } else {
            $itemCollection->addFieldToFilter('main_table.offer_avail_id', $offerId);
        }
        if ($vendorId) {
            $itemCollection->addFieldToFilter('main_table.vendor', $vendorId);
        }
        if ($locationId) {
            $itemCollection->addFieldToFilter('main_table.vendor_farm', $locationId);
        }
        $itemCollection->getSelect()
            ->joinLeft(
                ['item_box' => 'order_item_box'],
                "main_table.item_id=item_box.item_id",
                [
                    'box_count' => 'COUNT(item_box.box_id)'
                ]
            )
            ->where("item_box.status NOT IN (" . $notInQuery . ")")
            ->group('main_table.item_id');
        $offerWiseBoxCount = [];
        if ($itemCollection->getSize() > 0) {
            foreach ($itemCollection as $item) {
                $offerId = $item->getData('offer_avail_id');
                $boxCount = $item->getData('box_count');
                if (isset($offerWiseBoxCount[$offerId])) {
                    $offerWiseBoxCount[$offerId] += $boxCount;
                } else {
                    $offerWiseBoxCount[$offerId] = $boxCount;
                }
            }
        }
        return $offerWiseBoxCount;
    }

    /**
     * @param $offerId
     * @param $vendorId
     * @param $locationId
     * @return array
     */
    public function getHardGoodOrderCountByOfferId($offerId, $vendorId = null, $locationId = null)
    {
        $notIn = ['cancelled', 'rejected'];
        $notInQuery = "'" . implode("', '", $notIn) . "'";
        $hgItemCollection = $this->hardGoodOrderItemFactory->create()->getCollection();
        if (is_array($offerId)) {
            $hgItemCollection->addFieldToFilter('main_table.hg_offer_id', ['in' => $offerId]);
        } else {
            $hgItemCollection->addFieldToFilter('main_table.hg_offer_id', $offerId);
        }
        $hgItemCollection->getSelect()
            ->joinLeft(
                ['item_box' => 'order_item_box'],
                "main_table.item_id=item_box.item_id",
                [
                    'box_count' => 'SUM(main_table.hg_ordered_qty)'
                ]
            )->joinLeft(
                ['item' => 'sales_order_item'],
                "main_table.item_id=item.item_id"
            )
            ->where("item_box.status NOT IN (" . $notInQuery . ")")
            ->group('main_table.hg_offer_id');
        if (!empty($vendorId)) {
            $hgItemCollection->getSelect()->where('item.vendor = ?', $vendorId);
        }
        if (!empty($locationId)) {
            $hgItemCollection->getSelect()->where('item.vendor_farm = ?', $locationId);
        }
        $offerWiseBoxCount = [];
        if ($hgItemCollection->getSize() > 0) {
            foreach ($hgItemCollection as $item) {
                $offerId = $item->getData('hg_offer_id');
                $boxCount = $item->getData('box_count');
                $offerWiseBoxCount[$offerId] = $boxCount * $item->getData('hg_ordered_qty');
                /*if (isset($offerWiseBoxCount[$offerId])) {
                    $offerWiseBoxCount[$offerId] += $boxCount;
                } else {
                    $offerWiseBoxCount[$offerId] = $boxCount;
                }*/
            }
        }
        return $offerWiseBoxCount;
    }

    /**
     * Return Model By ID
     * @param $id
     * @return Inventory|AbstractDb|AbstractCollection|null
     */
    public function getVendorInventoryOfferById($id)
    {
        if (is_array($id)) {
            $collection = $this->inventoryFactory->create()->getCollection();
            $collection->addFieldToFilter('offer_id', ['in' => $id]);
            return $collection;
        }
        return $this->inventoryFactory->create()->load($id);
    }

    /**
     * Return Model
     * @return HardGood
     */
    public function getHardGoodMappingModel()
    {
        return $this->hardGoodFactory->create();
    }

    /**
     * @return mixed
     */
    public function getHardGoodAttributeId()
    {
        return $this->scopeConfig->getValue(
            self::HARD_GOOD_ID,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Return Model BY Id
     * @param $Id
     * @return HardGood
     */
    public function getHardGoodMappingModelById($Id)
    {
        return $this->hardGoodFactory->create()->load($Id);
    }

    /**
     * Return Hard Good Mapped Collection By Params
     * @param null $params
     * @return AbstractDb|AbstractCollection|null
     */
    public function getHardGoodMappingCollection($params = null)
    {
        $collection = $this->hardGoodFactory->create()->getCollection();
        if ($params != null) {
            if (isset($params['status'])) {
                $collection->addFieldToFilter('status', '1');
            }
            if (isset($params['product_id'])) {
                $collection->addFieldToFilter('product_sku', $params['product_sku']);
            }
            if (isset($params['hg_product_id'])) {
                $collection->addFieldToFilter('hg_product_sku', $params['hg_product_sku']);
            }
        }
        return $collection;
    }

    /**
     * Offer Ids for Hard Good Inventory
     * @param $params
     * @return array
     */
    public function getDecrementOffersForHardGood($params)
    {
        date_default_timezone_set("America/New_York");
        $today = date('Y-m-d');
        $offerCollection = $this->getVendorInventoryOfferCollection($params);
        $offerSku = [];
        if ($offerCollection->getSize() > 0) {
            foreach ($offerCollection as $offer) {
                $sku = $offer->getData('sku');
                $offerId = $offer->getId();
                $invData = $offer->getData('inventory_qty');
                $startDate = $offer->getData('start_date');
                $expiryDate = $offer->getData('expiry_date');
                /**
                 * check if offer is expired
                 */
                if (strtotime($today) >= strtotime($startDate)
                    && strtotime($today) <= strtotime($expiryDate)) {
                    if (isset($offerSku[$sku])) {
                        /**
                         * check for qty
                         */
                        if ($invData > $offerSku[$sku]['inventory']) {
                            $offerSku[$sku]['offer_id'] = $offerId;
                            $offerSku[$sku]['inventory'] = $invData;
                        }
                    } else {
                        $offerSku[$sku]['offer_id'] = $offerId;
                        $offerSku[$sku]['inventory'] = $invData;
                    }
                }

            }
        }
        return $offerSku;
    }

    /**
     * Return All Hard good sku mapped to the Actual Product
     * @param $sku
     * @return AbstractDb|AbstractCollection|null
     */
    public function getAllHardGoodProductsCollectionBySku($sku)
    {
        $collection = $this->hardGoodFactory->create()->getCollection();
        $collection->addFieldToFilter('product_sku', $sku);
        $collection->addFieldToFilter('status', '1');
        return $collection;
    }

    /**
     * @param $sku
     * @return array
     */
    public function getHardGoodMappedProductSkuArrayForSku($sku)
    {
        $collection = $this->getAllHardGoodProductsCollectionBySku($sku);
        $skuArray = [];
        if ($collection->getSize() > 0) {
            foreach ($collection as $one) {
                $skuArray[] = $one->getData('hg_product_sku');
            }
        }
        return $skuArray;
    }

    /**
     * Return Hard Good Product Skus
     * @param $sku
     * @return array
     */
    public function getAllHardGoodMappedProductSkuArrayBySKu($sku)
    {
        $collection = $this->getAllHardGoodProductsCollectionBySku($sku);
        $hgSkus = [];
        foreach ($collection as $one) {
            $hgSku = $one->getData('hg_product_sku');
            $hgSkus[$hgSku] = $one->getData('qty');
        }
        return $hgSkus;
    }

    /**
     * Return Collection By vendor ID
     * @param $vendorId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorInventoryOfferCollectionByVendorId($vendorId)
    {
        return $this->getVendorInventoryModel()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId);
    }

    /**
     * Return Vendor Confirm Box Data
     * @return mixed
     */
    public function getVendorConfirmBoxesModel()
    {
        return $this->vendorConfirmFactory->create();
    }

    /**
     * Return Model BY ID
     * @param $id
     * @return mixed
     */
    public function getVendorConfirmBoxModelById($id)
    {
        return $this->vendorConfirmFactory->create()->load($id);
    }

    /**
     * Return Vendor Confirm Box Table Collection
     * @param null $params
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorConfirmBoxCollection($params = null)
    {
        $collection = $this->vendorConfirmFactory->create()->getCollection();
        if ($params != null) {
            if (isset($params['vendor_id'])) {
                $collection->addFieldToFilter('vendor_id', $params['vendor_id']);
            }
            if (isset($params['loc_id'])) {
                $collection->addFieldToFilter('loc_id', $params['loc_id']);
            }
            if (isset($params['loc_id'])) {
                $collection->addFieldToFilter('loc_id', $params['loc_id']);
            }

        }
        return $collection;
    }

    /**
     * Return Vendor Logistic Model
     * @return Logistics
     */
    public function getVendorLogisticModel()
    {
        return $this->logisticsFactory->create();
    }

    /**
     * Return Model By ID
     * @param $id
     * @return Logistics
     */
    public function getVendorLogisticModelById($id)
    {
        return $this->logisticsFactory->create()->load($id);
    }

    /**
     * Return Logistic Collection By Vendor Id and Location Id
     * @param $vendorId
     * @param $locationId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorLogisticCollectionByVendorIdAndLocationId($vendorId, $locationId)
    {
        return $this->getVendorLogisticModel()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('location_id', $locationId);
    }

    /**
     * Return Vendor Logistic Model By Location Id
     * @param $locationId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorLogisticModelByLocationId($locationId)
    {
        $collection = $this->getVendorLogisticModel()->getCollection();
        $collection->addFieldToFilter('location_id', $locationId);
        return $collection;
    }

    /**
     * Return Vendor Logistic Collection by Params
     * @param null $params
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorLogisticCollectionByParams($params = null)
    {
        $collection = $this->getVendorLogisticModel()->getCollection();
        $collection->addFieldToFilter('status', '1');
        if ($params != null) {
            if (isset($params['vendor_id'])) {
                $collection->addFieldToFilter('vendor_id', $params['vendor_id']);
            }
            if (isset($params['location_id'])) {
                $collection->addFieldToFilter('location_id', $params['location_id']);
            }
            if (isset($params['shipping_method'])) {
                $collection->addFieldToFilter('shipping_method', $params['shipping_method']);
            }
        }
        return $collection;
    }

    /**
     * @param $locationId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorLogisticCollectionWithShipping($locationId)
    {
        $collection = $this->getVendorLogisticModel()->getCollection();
        $collection->addFieldToFilter('location_id', $locationId);
        $collection->getSelect()
            ->joinLeft(
                ['shipping' => 'vendor_shipping'],
                "main_table.shipping_method=shipping.shipping_method_id",
                [
                    'pref_ship_method' => 'shipping.pref_ship_methods',
                    'shipping_method_name' => 'shipping.shipping_method_name'
                ]
            )->joinLeft(
                ['location' => 'dropship_vendor_locations'],
                "main_table.location_id=location.loc_id",
                [
                    'loc_name' => 'location.loc_name'
                ]
            )->where('location.status =?', '1');
        return $collection;
    }

    /**
     * Return Active Logistics Collection By Location Id , Store code and Zip code
     * @param $locationId
     * @param $storeCode
     * @param $zipCode
     * @return AbstractDb|AbstractCollection|null
     */
    public function getActiveLogisticsCollectionByLocationId($locationId, $storeCode, $zipCode)
    {
        $collection = $this->getVendorLogisticModel()->getCollection()
            ->addFieldToFilter('location_id', $locationId)
            ->addFieldToFilter('status', '1')
            ->addFieldToFilter('store_code', ['like' => "%$storeCode%"])
            ->addFieldToFilter('zip_codes', [
                ['like' => "%$zipCode%"],
                ['null' => true]
            ]);
        $collection->getSelect()
            ->joinLeft(
                ['shipping' => 'vendor_shipping'],
                "main_table.shipping_method=shipping.shipping_method_id",
                [
                    'method_name' => 'shipping.shipping_method_name'
                ]
            );
        return $collection;
    }

    /**
     * Return Vendor Blackout Days Collection
     * @param $vendorId
     * @param $locationId
     * @param $shippingMethodId
     * @param $blockType
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorBlackoutCollection($vendorId, $locationId, $shippingMethodId, $blockType)
    {
        return $this->blackoutFactory->create()->getCollection()
            ->addFieldToFilter('vendor', $vendorId)
            ->addFieldToFilter('vendor_location_name', $locationId)
            ->addFieldToFilter('ship_method', $shippingMethodId)
            ->addFieldToFilter('block_type', $blockType);
    }

    /**
     * Return Model BY id
     * @param $id
     * @return VendorUser
     */
    public function getVendorUserModelByEntityId($id)
    {
        return $this->vendorUserFactory->create()->load($id);
    }

    /**
     * Return Vendor User Model By Location ID
     *
     * @param $locationId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorUserModelByLocationId($locationId)
    {
        $collection = $this->getVendorUserModel()->getCollection();
        $collection->addFieldToFilter('location_id', $locationId);
        return $collection;
    }

    /**
     * Return Country List
     * @return array
     */
    public function getAllCountryArray()
    {
        $collection = $this->countryCollectionFactory->create()->loadByStore();
        $options = $collection->setForegroundCountries($this->getTopDestinations())
            ->toOptionArray();
        $countryArray[''] = 'Please Select';
        foreach ($options as $country) {
            $countryArray[$country['value']] = $country['label'];
        }
        return $countryArray;
    }

    /**
     * Return Country Format
     * @return array
     */
    public function getFormattedCountryListArray()
    {
        $listArray = $this->getAllCountryArray();
        $countryFormat = [];
        foreach ($listArray as $countryCode => $value) {
            //if ($countryCode == 'US') {
            $countryFormat[] = ['value' => $countryCode, 'label' => $value];
            //}
        }
        return $countryFormat;
    }

    /**
     * Retrieve list of top destinations countries
     *
     * @return array
     */
    protected function getTopDestinations()
    {
        $destinations = (string)$this->scopeConfig->getValue(
            'general / country / destinations',
            ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }

    /**
     * Return Regions By Country ID
     * @param $countryCode
     * @return array
     */
    public function getAllRegionsByCountryId($countryCode)
    {
        $regionsData = [];
        //$countryCode = 'USA';
        $regionCollection = $this->country->loadByCode($countryCode)->getRegions();
        $regions = $regionCollection->loadData();
        $count = 1;
        foreach ($regions as $region) {
            $regionsData[$region->getRegionId()] = $region->getName();
        }
        return $regionsData;
    }

    /**
     * Return Region Format
     *
     * @param $countryCode
     * @return array
     */
    public function getFormattedRegionsDataByCountryId($countryCode)
    {
        $regionArray = $this->getAllRegionsByCountryId($countryCode);
        $regionFormat[] = ['value' => '', 'label' => 'Please Select'];
        foreach ($regionArray as $regionId => $value) {
            $regionFormat[] = ['value' => $regionId, 'label' => $value];
        }
        return $regionFormat;
    }

    /**
     * Return Yes or No array
     * @return array
     */
    public function getYesNoArray()
    {
        return [
            '0' => 'No',
            '1' => 'Yes'
        ];
    }

    /**
     * Return Vendor User Type Array
     * @return string[]
     */
    public function getVendorUserTypesArray()
    {
        return [
            '' => 'Please Select',
            'accounts_receivable' => 'Accounts Receivable',
            'logistics' => 'Logistics',
            'owner_partner' => 'Owner or Partner',
            'post_harvest' => 'Post Harvest',
            'sales_associate' => 'Sales Associate',
            'sales_manager' => 'Sales Manager'
        ];
    }

    /**
     * Return Days Array
     * @return array
     */
    public function getDaysArray()
    {
        return [
            'sunday' => 'Sunday',
            'monday' => 'Monday',
            'tuesday' => 'Tuesday',
            'wednesday' => 'Wednesday',
            'thursday' => 'Thursday',
            'friday' => 'Friday',
            'saturday' => 'Saturday'
        ];
    }

    /**
     * Vendor Type by Type Id
     *
     * @param $typeId
     * @return string
     */
    public function getVendorTypeNameById($typeId)
    {
        try {
            $attributeSet = $this->attributeSetRepository->get($typeId);
            return $attributeSet->getAttributeSetName();
        } catch (\Exception $e) {
            return '';
        }

        /*try {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from(
                    'vendor_types',
                    ['type_name']
                )->where('type_id = ?', $typeId);
            $typeName = $connection->fetchOne($select);
            if (!empty($typeName)) {
                return $typeName;
            }
        } catch (\Exception $e) {
            return '';
        }*/
    }

    /**
     * Return All Attribute Sets or Product Type
     * @return array
     */
    public function getAllAttributeSetData()
    {
        $attributeSetList['0'] = 'All';
        $options = $this->attributeSetOptions->toOptionArray();
        foreach ($options as $option) {
            /*if ($option['label'] != 'Default') {*/
            $attributeSetList[$option['value']] = $option['label'];
            /*}*/
        }
        return $attributeSetList;
    }

    /**
     * Return to bring in configuration
     * @return array
     */
    public function getAttributeSetOptionsArray()
    {
        return $this->attributeSetOptions->toOptionArray();
    }

    /**
     * Return Store Config Values
     * @param $config_path
     * @return mixed
     */
    public function getConfig($config_path)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Make array by configuration Ids
     * and prepare Vendor Type
     * @return array
     */
    public function getSelectedVendorTypesArray()
    {
        $vendorType = $this->scopeConfig->getValue(
            self::VENDOR_TYPE_CONFIGURATION,
            ScopeInterface::SCOPE_STORE
        );
        $vendorTypesArray = [];
        if ($vendorType != null && $vendorType != '') {
            $vendorTypeIds = explode(',', $vendorType);
            foreach ($vendorTypeIds as $typeId) {
                $vendorTypesArray[] = [
                    'value' => $typeId,
                    'label' => $this->getVendorTypeNameById($typeId)
                ];
            }
        }
        return $vendorTypesArray;
    }

    /**
     * Return All Vendor Type Array
     * @return array
     */
    public function getAllVendorTypes()
    {
        $typesArray = [];
        try {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from(
                    'vendor_types',
                    ['type_id', 'type_name']
                );
            $types = $connection->fetchAll($select);
            foreach ($types as $option) {
                $typesArray[] = [
                    'value' => $option['type_id'],
                    'label' => $option['type_name']
                ];
            }

        } catch (\Exception $e) {
            return [];
        }
        return $typesArray;
    }

    /**
     * Return All vendor Types for Vendor Id
     * @param $vendorId
     * @return array
     * @throws NoSuchEntityException
     */
    public function getVendorTypesByVendorId($vendorId)
    {
        $vendorTypes = [];
        $vendor = $this->getVendorModelById($vendorId);
        if ($vendor->getId() != null) {
            $vendorType = $vendor->getVendorType();
            if ($vendorType != null && $vendorType != '') {
                $vendorTypeIds = explode(',', $vendorType);
                foreach ($vendorTypeIds as $typeId) {
                    $vendorTypes[] = [
                        'vendor_type_id' => $typeId,
                        'vendor_type_label' => $this->getVendorTypeNameById($typeId)
                    ];
                }
            }
        }
        return $vendorTypes;
    }

    /**
     * Get Vendor Data by Vendor ID
     * @param $vendorId
     * @return array
     */
    public function getVendorLoginData($vendorId)
    {
        $vendorData = [];
        $vendor = $this->getVendorModelById($vendorId);
        if ($vendor->getId() != null) {
            $vendorData['vendor_id'] = $vendor->getId();
            $vendorData['vendor_name'] = $vendor->getData('vendor_name') . ' ' . $vendor->getData('last_name');
            $vendorData['telephone'] = $vendor->getData('telephone');
            $vendorData['email'] = $vendor->getData('email');
            $vendorData['address'] = [
                'street' => [
                    $vendor->getData('address'),
                    $vendor->getData('street')
                ],
                'city' => $vendor->getData('city'),
                'state' => $this->getRegionNameById($vendor->getData('region')),
                'country' => $this->getCountryNameByCountryCode($vendor->getData('country_id')),
                'zip_code' => $vendor->getData('zipcode')
            ];
            $vendorType = $vendor->getVendorType();
            $vendorTypes = [];
            if ($vendorType != null && $vendorType != '') {
                $vendorTypeIds = explode(',', $vendorType);
                foreach ($vendorTypeIds as $typeId) {
                    $vendorTypes[] = [
                        'vendor_type_id' => $typeId,
                        'vendor_type_label' => $this->getVendorTypeNameById($typeId)
                    ];
                }
            }
            $vendorData['vendor_type'] = $vendorTypes;
        }
        return $vendorData;
    }

    /**
     * Return Location API Data
     * @param $locationId
     * @return array
     */
    public function getVendorLocationApiData($locationId)
    {
        $locationData = [];
        $locationModel = $this->getVendorLocationModelByLocationId($locationId);
        if ($locationModel->getId() != null) {
            $locationData['location_id'] = $locationModel->getId();
            $locationData['loc_name'] = $locationModel->getData('loc_name');
            $locationData['loc_cust_disp_name'] = $locationModel->getData('loc_cust_disp_name');
            $locationData['loc_phone'] = $locationModel->getData('loc_phone');
            $locationData['loc_email'] = $locationModel->getData('loc_email');

            $regionId = $locationModel->getData('loc_state');
            $regionName = $this->getRegionNameById($regionId);
            $countryId = $locationModel->getData('loc_country');
            $countryName = $this->getCountryNameByCountryCode($countryId);
            $locationData['address'] = [
                'street' => [
                    $locationModel->getData('loc_address'),
                    $locationModel->getData('loc_street')
                ],
                'loc_city' => $locationModel->getData('loc_city'),
                'loc_state' => ['region_id' => $regionId, 'name' => $regionName],
                'loc_country' => ['country_id' => $countryId, 'name' => $countryName],
                'loc_postcode' => $locationModel->getData('loc_postcode'),
                'loc_fax' => $locationModel->getData('loc_fax')
            ];
            $locationData['status'] = $locationModel->getData('status');

        }
        return $locationData;
    }

    /**
     * Return User Data
     *
     * @param $entityId
     * @return array
     */
    public function getVendorUserApiDataById($entityId)
    {
        $userData = [];
        try {
            $user = $this->getVendorUserModelByEntityId($entityId);
            if ($user->getId() != null) {
                $userData['user_entity_id'] = $user->getData('user_entity_id');
                $userData['first_name'] = $user->getData('first_name');
                $userData['last_name'] = $user->getData('last_name');
                $userData['work_email'] = $user->getData('work_email');
                $userData['personal_email'] = $user->getData('personal_email');
                $userData['telephone'] = $user->getData('telephone');
                $userData['status'] = $user->getData('status');
            }
            return $userData;
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * Return Logistic Data by ID
     * @param $logisticId
     * @return array
     */
    public function getVendorLogisticApiData($logisticId)
    {
        $logisticModel = $this->getVendorLogisticModelById($logisticId);
        if ($logisticModel->getId() != null) {
            return $logisticModel->convertToArray();
        }
        return [];
    }

    /**
     * Authenticate Vendor Username and Password
     * @param $userName
     * @param $password
     * @return integer|null|array
     * @throws LocalizedException
     */
    public function checkAuthentication($userName, $password)
    {
        $loginAr = [];
        /**
         * First check in vendor Table
         */
        $vendorCollection = $this->vendorFactory->getCollection()
            ->addFieldToFilter('vendor_user_id', $userName)
            ->addFieldToFilter('password', $password);
        if ($vendorCollection->getSize() > 0) {
            $vendorId = null;
            foreach ($vendorCollection as $vendor) {
                $vendorId = $vendor->getId();
            }
            return $vendorId;
        }
        /**
         * Check in Vendor User Table
         */
        $userCollection = $this->getVendorUserModel()->getCollection()
            ->addFieldToFilter('user_name', $userName)
            ->addFieldToFilter('password', $password);
        if ($userCollection->getSize() > 0) {
            $vendorId = $locationId = $userId = null;
            foreach ($userCollection as $user) {
                $vendorId = $user->getVendorId();
                $locationId = $user->getLocationId();
                $userId = $user->getId();
            }
            $loginAr['vendor_id'] = $vendorId;
            $loginAr['location_id'] = $locationId;
            $loginAr['user_entity_id'] = $userId;
            return $loginAr;
        }
        throw new LocalizedException(__("Invalid username or password"));
    }

    /**
     * Return Region Name By Id
     * @param $id
     * @return string|null
     */
    public function getRegionNameById($id)
    {
        try {
            $region = $this->region->load($id);
            if ($region != null) {
                return $region->getName();
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Return Country Name By Code
     * @param $countryCode
     * @return string|null
     */
    public function getCountryNameByCountryCode($countryCode)
    {
        try {
            $country = $this->countryFactory->create()->loadByCode($countryCode);
            return $country->getName();
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Return Item Collection By Vendor Id and Location Id
     * @param null $vendorId
     * @param null $locationId
     * @param null $vendorType
     * @return AbstractDb|AbstractCollection|null
     */
    public function getVendorOrderItemCollection(
        $vendorId = null,
        $locationId = null,
        $vendorType = null
    )
    {
        $selectAttributes = [
            'item_id', 'order_id', 'product_id', 'sku', 'name', 'qty_ordered', 'price', 'pickup_date',
            'delivery_date', 'vendor', 'vendor_farm', 'vendor_type', 'shipping_method', 'time_slot_from',
            'time_slot_to', 'consultation_type', 'prescription_required', 'address_id',
            'prescription_id', 'order_item_status', 'enquiryperson',
            'enquiryemail', 'enquirphone', 'organizedby', 'venue', 'confdate'
        ];
        $itemCollection = $this->itemFactory->create()->getCollection();
        $itemCollection->addFieldToSelect($selectAttributes);
        if ($vendorId) {
            $itemCollection->addFieldToFilter('main_table.vendor', $vendorId);
        }
        if ($locationId) {
            $itemCollection->addFieldToFilter('main_table.vendor_farm',
                [
                    ['eq' => $locationId],
                    ['eq' => "0"]
                ]);
            /*$itemCollection->addFieldToFilter('main_table.vendor_farm', $locationId);*/
        }
        if ($vendorType) {
            $itemCollection->addFieldToFilter('main_table.vendor_type', $vendorType);
        }
        return $itemCollection;
    }

    /**
     * Return Order Item Box wise data
     *
     * @param null $vendorId
     * @param null $locationId
     * @param null $fromDate
     * @param null $toDate
     * @param null $boxStatus
     * @param null $shippingMethod
     * @param null $dateType
     * @return AbstractDb|AbstractCollection|null
     */
    public function getOrderItemBoxWiseCountCollection(
        $vendorId = null,
        $locationId = null,
        $fromDate = null,
        $toDate = null,
        $boxStatus = null,
        $shippingMethod = null,
        $dateType = null
    )
    {
        $selectAttributes = [
            'item_id', 'order_id', 'product_id', 'sku', 'name', 'price', 'pickup_date', 'vendor',
            'vendor_farm', 'delivery_date', 'truck_pickup_date', 'po_unit_price', 'price', 'created_at',
            'shipping_method', 'store_id'
        ];
        $notIn = ['cancelled', 'rejected'];
        $itemCollection = $this->itemFactory->create()->getCollection();
        $itemCollection->addFieldToSelect($selectAttributes);
        if ($vendorId) {
            $itemCollection->addFieldToFilter('main_table.vendor', $vendorId);
        }
        if ($shippingMethod) {
            if (is_array($shippingMethod)) {
                $itemCollection->addFieldToFilter('main_table.shipping_method', ['in' => $shippingMethod]);

            } else {
                $itemCollection->addFieldToFilter('main_table.shipping_method', ['like' => "%$shippingMethod%"]);
            }
        }
        if ($locationId) {
            $itemCollection->addFieldToFilter(
                'main_table.vendor_farm',
                [
                    ['eq' => $locationId],
                    ['eq' => "0"]
                ]
            );
        }
        if ($fromDate && $toDate) {
            if ($dateType != null && $dateType == 'order_date') {
                $itemCollection->addFieldToFilter('main_table.created_at', ['gteq' => $fromDate]);
                $itemCollection->addFieldToFilter('main_table.created_at', ['lteq' => $toDate]);
            } elseif ($dateType == 'ship_date') {
                $itemCollection->addFieldToFilter('main_table.pickup_date', ['gteq' => $fromDate]);
                $itemCollection->addFieldToFilter('main_table.pickup_date', ['lteq' => $toDate]);
            }

        }
        $notInQuery = "'" . implode("', '", $notIn) . "'";
        if ($boxStatus) {
            $itemCollection->getSelect()
                ->joinLeft(
                    ['item_box' => 'order_item_box'],
                    "main_table.item_id=item_box.item_id",
                    [
                        'box_count' => 'COUNT(item_box.box_id)'
                    ]
                )
                ->where("item_box.status = ?", $boxStatus)
                ->group('main_table.item_id');
        } else {
            $itemCollection->getSelect()
                ->joinLeft(
                    ['item_box' => 'order_item_box'],
                    "main_table.item_id=item_box.item_id",
                    [
                        'box_count' => 'COUNT(item_box.box_id)'
                    ]
                )
                ->where("item_box.status NOT IN (" . $notInQuery . ")")
                ->group('main_table.item_id');
        }
        $itemCollection->getSelect()
            ->joinLeft(
                ['vendor' => 'vendor_registrations'],
                "main_table.vendor=vendor.vendor_id",
                [
                    'vendor_name' => 'vendor.vendor_name',
                ]
            )->joinLeft(
                ['location' => 'dropship_vendor_locations'],
                "main_table.vendor_farm=location.loc_id",
                [
                    'location_name' => 'location.loc_name',
                ]
            );
        return $itemCollection;
    }

    /**
     * @param null $vendorId
     * @param null $locationId
     * @param null $fromDate
     * @param null $toDate
     * @param null $boxStatus
     * @return AbstractDb|AbstractCollection|null
     */
    public function getBoxWiseItemCollection(
        $vendorId = null,
        $locationId = null,
        $fromDate = null,
        $toDate = null,
        $boxStatus = null
    )
    {
        $notIn = ['cancelled', 'rejected'];
        $selectAttributes = ['box_id', 'order_id', 'item_id', 'status', 'track_id', 'gift_message'];
        $boxCollection = $this->getBoxCollection();
        if ($boxStatus) {
            if (is_array($boxStatus)) {
                $boxCollection->addFieldToFilter('main_table.status', ['in' => $boxStatus]);
            } else {
                $boxCollection->addFieldToFilter('main_table.status', $boxStatus);
            }
        }
        $boxCollection->addFieldToSelect($selectAttributes);
        $boxCollection->addFieldToFilter('main_table.status', ['nin' => $notIn]);
        $boxCollection->getSelect()
            ->joinLeft(
                ['order_item' => 'sales_order_item'],
                "main_table.item_id=order_item.item_id"
            );
        if ($vendorId) {
            $boxCollection->getSelect()
                ->where("order_item.vendor = ?", $vendorId);
        }
        if ($locationId) {
            $boxCollection->getSelect()
                ->where("order_item.vendor_farm = " . $locationId . " or order_item.vendor_farm ='0'");
        }
        if ($fromDate && $toDate) {
            $boxCollection->addFieldToFilter('order_item.pickup_date', ['gteq' => $fromDate]);
            $boxCollection->addFieldToFilter('order_item.pickup_date', ['lteq' => $toDate]);
        }
        return $boxCollection;
    }

    /**
     *
     * @param null $vendorId
     * @param null $locationId
     * @param null $boxStatus
     * @param null $boxId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getBoxWiseItemAndOrderCollection(
        $vendorId = null,
        $locationId = null,
        $boxStatus = null,
        $boxId = null
    )
    {
        $boxCollection = $this->getBoxCollection();
        $boxCollection->addFieldToSelect(['box_id', 'order_id', 'status', 'track_id', 'gift_message', 'ImageUrl']);
        if ($boxStatus) {
            $boxArray = explode(",", $boxStatus);
            $boxCollection->addFieldToFilter('main_table.status', ['in' => $boxArray]);
        }
        if ($boxId) {
            if (is_array($boxId)) {
                $boxIdArray = $boxId;
            } else {
                $boxIdArray = explode(",", $boxId);
            }
            $boxCollection->addFieldToFilter('main_table.box_id', ['in' => $boxIdArray]);
        }
        $boxCollection->getSelect()
            ->joinLeft(
                ['order_item' => 'sales_order_item'],
                "main_table.item_id=order_item.item_id"
            );
        if ($vendorId) {
            $boxCollection->getSelect()
                ->where("order_item.vendor = ?", $vendorId);
        }
        if ($locationId) {
            $boxCollection->getSelect()
                ->where("order_item.vendor_farm = " . $locationId . " or order_item.vendor_farm ='0'");
            /* ->where("order_item.vendor_farm = ?", $locationId);*/

        }
        return $boxCollection;
    }

    /**
     * Return Latest Track Status for Track ID
     * @param $trackId
     * @return array
     */
    public function getLatestTrackStatusByTrackId($trackId)
    {
        $latestTrackStatusArray = [];
        try {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from(
                    'track_history_new',
                    [
                        'th_track_id',
                        'th_date_created',
                        'th_time_created',
                        'th_activity'
                    ]
                );
            if (is_array($trackId)) {
                $trackIdArray = "'" . implode("', '", $trackId) . "'";
                $select->where("th_track_id IN (" . $trackIdArray . ")");
            } else {
                $trackId = trim($trackId);
                $select->where('th_track_id = ?', $trackId);
            }
            $trackHistoryData = $connection->fetchAll($select);
            if (count($trackHistoryData) > 0) {
                foreach ($trackHistoryData as $oneTrack) {
                    $trackId = $oneTrack['th_track_id'];
                    $status = $oneTrack['th_activity'];
                    $trackUpdatedTime = $oneTrack['th_date_created'];
                    if (isset($latestTrackStatusArray[$trackId])) {
                        $previousTime = $latestTrackStatusArray[$trackId]['time'];
                        if (strtotime($trackUpdatedTime) > strtotime($previousTime)) {
                            $latestTrackStatusArray[$trackId]['time'] = $trackUpdatedTime;
                            $latestTrackStatusArray[$trackId]['status'] = $status;
                        }
                    } else {
                        $latestTrackStatusArray[$trackId]['time'] = $trackUpdatedTime;
                        $latestTrackStatusArray[$trackId]['status'] = $status;
                    }
                }
            }
        } catch (\Exception $e) {
            return $latestTrackStatusArray;
        }
        return $latestTrackStatusArray;
    }

    public function getFirstTrackStatusDataByTrackId($trackId)
    {
        $latestTrackStatusArray = [];
        try {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from(
                    'track_history_new',
                    [
                        'th_track_id',
                        'th_date_created',
                        'th_time_created',
                        'th_activity'
                    ]
                );
            if (is_array($trackId)) {
                $trackIdArray = "'" . implode("', '", $trackId) . "'";
                $select->where("th_track_id IN (" . $trackIdArray . ")");
            } else {
                $trackId = trim($trackId);
                $select->where('th_track_id = ?', $trackId);
            }
            $trackHistoryData = $connection->fetchAll($select);

            if (count($trackHistoryData) > 0) {
                foreach ($trackHistoryData as $oneTrack) {
                    $trackId = $oneTrack['th_track_id'];
                    $status = $oneTrack['th_activity'];
                    $trackUpdatedTime = $oneTrack['th_date_created'];
                    if (isset($latestTrackStatusArray[$trackId])) {
                        $previousTime = $latestTrackStatusArray[$trackId]['time'];
                        if (strtotime($trackUpdatedTime) < strtotime($previousTime)) {
                            $latestTrackStatusArray[$trackId]['time'] = $trackUpdatedTime;
                            $latestTrackStatusArray[$trackId]['status'] = $status;
                        }
                    } else {
                        $latestTrackStatusArray[$trackId]['time'] = $trackUpdatedTime;
                        $latestTrackStatusArray[$trackId]['status'] = $status;
                    }
                }
            }
        } catch (\Exception $e) {
            return $latestTrackStatusArray;
        }
        return $latestTrackStatusArray;
    }
}
