<?php
namespace Flordelcampo\VendorConversionFactor\Model\Config\Source;

class ProductAtt implements \Magento\Framework\Option\ArrayInterface
{
    protected $eavConfig;

    public function __construct(\Magento\Eav\Model\Config $eavConfig) {
        $this->eavConfig = $eavConfig;
    }
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {


     $attribute = $this->eavConfig->getAttribute('catalog_product', 'box_type');
     $options = $attribute->getSource()->getAllOptions();
       
     $optionsExists = array();

     foreach($options as $option) {
            //$optionsExists[] = $option['label'];
            $optionsExists[$option['value']] = $option['label'];
            //$optionsExists[] = $option['value'];
        }

        return $optionsExists;
    }
    
    public function toArray()
    {
        return [];
    } 

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
    
}