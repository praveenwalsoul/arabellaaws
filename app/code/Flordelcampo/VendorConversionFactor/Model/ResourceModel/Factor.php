<?php
namespace Flordelcampo\VendorConversionFactor\Model\ResourceModel;

class Factor extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendor_conversion_factor', 'conversion_factor_id');
    }
}
?>