<?php

namespace Flordelcampo\VendorConversionFactor\Model\ResourceModel\Factor;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorConversionFactor\Model\Factor', 'Flordelcampo\VendorConversionFactor\Model\ResourceModel\Factor');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>