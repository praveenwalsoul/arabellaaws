<?php
namespace Flordelcampo\VendorConversionFactor\Model;

class Factor extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorConversionFactor\Model\ResourceModel\Factor');
    }
}
?>