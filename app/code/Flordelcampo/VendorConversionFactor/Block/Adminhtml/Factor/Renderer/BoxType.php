<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Flordelcampo\VendorConversionFactor\Block\Adminhtml\Factor\Renderer;


class BoxType extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    protected $customerGroupCollection;

    protected $eavConfig;

    protected $options = null;

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param \Magento\Sales\Helper\Reorder $salesReorder
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Eav\Model\Config $eavConfig,
        array $data = []
    ) {
        $this->eavConfig = $eavConfig;
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $boxType = $row->getData('box_type');

        if (!empty($boxType)) {

            return $this->findLabelByValue($boxType);

        }

        return null;
    }

    protected function getExistingOptions() {

        if($this->options === null){

            $attribute = $this->eavConfig->getAttribute('catalog_product', 'box_type');
            $this->options = $attribute->getSource()->getAllOptions();
        }
        
        return $this->options;
    }

    protected function findLabelByValue($attrValue){

        $options = $this->getExistingOptions();

        foreach($options as $key => $val){

            if($val['value'] == $attrValue){

                return $val['label'];
            }
        }

        return null;
    }

}
