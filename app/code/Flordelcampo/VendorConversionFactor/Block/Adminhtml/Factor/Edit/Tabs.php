<?php
namespace Flordelcampo\VendorConversionFactor\Block\Adminhtml\Factor\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('factor_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Factor Information'));
    }
}