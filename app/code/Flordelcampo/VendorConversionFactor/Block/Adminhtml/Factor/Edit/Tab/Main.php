<?php

namespace Flordelcampo\VendorConversionFactor\Block\Adminhtml\Factor\Edit\Tab;

/**
 * Factor edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Flordelcampo\VendorConversionFactor\Model\Status
     */
    protected $_status;
    protected $productAtt;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Flordelcampo\VendorConversionFactor\Model\Config\Source\ProductAtt $productAtt,
        \Flordelcampo\VendorConversionFactor\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        $this->productAtt = $productAtt;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Flordelcampo\VendorConversionFactor\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('factor');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('conversion_factor_id', 'hidden', ['name' => 'conversion_factor_id']);
        }

		$prodAtt = $this->productAtt->toOptionArray();

        $fieldset->addField(
            'box_type',
            'select',
            [
                'label' => __('Box Type'),
                'title' => __('Box Type'),
                'name' => 'box_type',
				'values' => $prodAtt
                //'options' => \Flordelcampo\VendorConversionFactor\Block\Adminhtml\Factor\Grid::getOptionArray0(),
                //'disabled' => $isElementDisabled
            ]
        );

						
        $fieldset->addField(
            'box_code',
            'text',
            [
                'name' => 'box_code',
                'label' => __('Box Code'),
                'title' => __('Box Code'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'conversion_factor',
            'text',
            [
                'name' => 'conversion_factor',
                'label' => __('Conversion Factor'),
                'title' => __('Conversion Factor'),
				
                'disabled' => $isElementDisabled
            ]
        );
					

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
