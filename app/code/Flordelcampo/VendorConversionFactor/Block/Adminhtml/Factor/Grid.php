<?php
namespace Flordelcampo\VendorConversionFactor\Block\Adminhtml\Factor;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\VendorConversionFactor\Model\factorFactory
     */
    protected $_factorFactory;

    /**
     * @var \Flordelcampo\VendorConversionFactor\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\VendorConversionFactor\Model\factorFactory $factorFactory
     * @param \Flordelcampo\VendorConversionFactor\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\VendorConversionFactor\Model\FactorFactory $FactorFactory,
        \Flordelcampo\VendorConversionFactor\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_factorFactory = $FactorFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('conversion_factor_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_factorFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'conversion_factor_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'conversion_factor_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
		

						$this->addColumn(
							'box_type',
							[
								'header' => __('Box Type'),
								'index' => 'box_type',
                                'type'=> 'options',
                                'renderer' => \Flordelcampo\VendorConversionFactor\Block\Adminhtml\Factor\Renderer\BoxType::class
								//'type' => 'options',
								//'options' => \Flordelcampo\VendorConversionFactor\Block\Adminhtml\Factor\Grid::getOptionArray0()
							]
						);

						
				$this->addColumn(
					'box_code',
					[
						'header' => __('Box Code'),
						'index' => 'box_code',
					]
				);
				
				$this->addColumn(
					'conversion_factor',
					[
						'header' => __('Conversion Factor'),
						'index' => 'conversion_factor',
					]
				);
				


		
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'conversion_factor_id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
		

		
		   $this->addExportType($this->getUrl('vendorconversionfactor/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('vendorconversionfactor/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('conversion_factor_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_VendorConversionFactor::factor/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('factor');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('vendorconversionfactor/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('vendorconversionfactor/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('vendorconversionfactor/*/index', ['_current' => true]);
    }

    /**
     * @param \Flordelcampo\VendorConversionFactor\Model\factor|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'vendorconversionfactor/*/edit',
            ['conversion_factor_id' => $row->getId()]
        );
		
    }

	
		static public function getOptionArray0()
		{
            $data_array=array(); 
			$data_array[0]='box1';
			$data_array[1]='box2';
            return($data_array);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(\Flordelcampo\VendorConversionFactor\Block\Adminhtml\Factor\Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		

}