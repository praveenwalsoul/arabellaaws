<?php

namespace Flordelcampo\VendorBlackOutDays\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0){

		$installer->run('CREATE TABLE `vendor_blackout` (
          blackout_id int not null auto_increment,
          `vendor` varchar(255) DEFAULT NULL,
          `vendor_location_name` varchar(255) DEFAULT NULL,
          `cost_channel` varchar(255) DEFAULT NULL,
          `ship_method` varchar(255) DEFAULT NULL,
          `reason` varchar(255) DEFAULT NULL,
          `block_type` varchar(255) DEFAULT NULL,
          `start_date` TIMESTAMP NOT NULL,
          `end_date` TIMESTAMP NOT NULL,
          primary key(blackout_id))');


		//demo
//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//$scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
//$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/updaterates.log');
//$logger = new \Zend\Log\Logger();
//$logger->addWriter($writer);
//$logger->info('updaterates');
//demo 

		}

        $installer->endSetup();

    }
}