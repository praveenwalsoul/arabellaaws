<?php

namespace Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Edit\Tab;

use Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Grid;
use Flordelcampo\VendorBlackOutDays\Model\BlogPosts;
use Flordelcampo\VendorBlackOutDays\Model\Config\Source\CostChannel;
use Flordelcampo\VendorBlackOutDays\Model\Config\Source\Location;
use Flordelcampo\VendorBlackOutDays\Model\Config\Source\ShipMethod;
use Flordelcampo\VendorBlackOutDays\Model\Config\Source\VendorName;
use Flordelcampo\VendorBlackOutDays\Model\Status;
use Flordelcampo\VendorRegistration\Model\Vendor;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

/**
 * Blackout edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var Store
     */
    protected $_systemStore;

    /**
     * @var Status
     */
    protected $_status;
    protected $vendorName;
    protected $vendorLocation;
    protected $costChannel;
    protected $shipMethod;
    /**
     * @var Vendor
     */
    protected $vendors;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Store $systemStore
     * @param Vendor $vendorFactory
     * @param VendorName $vendorName
     * @param CostChannel $costChannel
     * @param ShipMethod $shipMethod
     * @param Location $vendorLocation
     * @param Status $status
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        Vendor $vendorFactory,
        VendorName $vendorName,
        CostChannel $costChannel,
        ShipMethod $shipMethod,
        Location $vendorLocation,
        Status $status,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        $this->vendorName = $vendorName;
        $this->vendorLocation = $vendorLocation;
        $this->costChannel = $costChannel;
        $this->shipMethod = $shipMethod;
        $this->vendors = $vendorFactory;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('blackout');
        $isElementDisabled = false;
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('blackout_id', 'hidden', ['name' => 'blackout_id']);
        }
        $vend = $this->vendorName->toOptionArray();
        $vendLocation = $this->vendorLocation->toOptionArray();
        $vcostChannel = $this->costChannel->toOptionArray();
        $vshipMethod = $this->shipMethod->toOptionArray();

        $fieldset->addField(
            'vendor',
            'select',
            [
                'label' => __('Vendor'),
                'title' => __('Vendor'),
                'name' => 'vendor',
                'values' => $vend
            ]
        );

        $fieldset->addField(
            'vendor_location_name',
            'select',
            [
                'label' => __('Vendor Location'),
                'title' => __('Vendor Location'),
                'name' => 'vendor_location_name',
                'values' => $vendLocation
            ]
        );

        $fieldset->addField(
            'cost_channel',
            'select',
            [
                'label' => __('Cost Channel'),
                'title' => __('Cost Channel'),
                'name' => 'cost_channel',
                'values' => $vcostChannel
            ]
        );

        $fieldset->addField(
            'ship_method',
            'select',
            [
                'label' => __('Ship Method'),
                'title' => __('Ship Mettod'),
                'name' => 'ship_method',
                'values' => $vshipMethod
            ]
        );

        $fieldset->addField(
            'reason',
            'text',
            [
                'name' => 'reason',
                'label' => __('Reason'),
                'title' => __('Reason')
            ]
        );

        $fieldset->addField(
            'block_type',
            'select',
            [
                'label' => __('Block Type'),
                'title' => __('Block Type'),
                'name' => 'block_type',
                'options' => Grid::getOptionArray5(),
            ]
        );

        $dateFormat = $this->_localeDate->getDateFormat(
            \IntlDateFormatter::MEDIUM
        );
        $timeFormat = $this->_localeDate->getTimeFormat(
            \IntlDateFormatter::MEDIUM
        );

        $fieldset->addField(
            'start_date',
            'date',
            [
                'name' => 'start_date',
                'label' => __('Start Date'),
                'title' => __('Start Date'),
                'date_format' => $dateFormat
            ]
        );
        $fieldset->addField(
            'end_date',
            'date',
            [
                'name' => 'end_date',
                'label' => __('End Date'),
                'title' => __('End Date'),
                'date_format' => $dateFormat
            ]
        );
        $fieldset->addField(
            'postpone_days',
            'text',
            [
                'label' => __('Postpone Days'),
                'title' => __('Postpone Days'),
                'name' => 'postpone_days',
                'value' => $model->getPostponeDays(),
                'class' => 'validate-number'
            ]
        );
        /*if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }*/
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * @return string[]
     */
    public function getTargetOptionArray()
    {
        return [
            '_self' => "Self",
            '_blank' => "New Page",
        ];
    }
}
