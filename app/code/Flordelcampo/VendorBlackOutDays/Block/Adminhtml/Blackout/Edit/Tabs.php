<?php
namespace Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('blackout_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Blackout Information'));
    }
}