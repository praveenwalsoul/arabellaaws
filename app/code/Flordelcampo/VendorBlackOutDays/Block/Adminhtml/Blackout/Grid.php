<?php

namespace Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout;

use Flordelcampo\VendorBlackOutDays\Model\blackout;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\VendorBlackOutDays\Model\blackoutFactory
     */
    protected $_blackoutFactory;

    /**
     * @var \Flordelcampo\VendorBlackOutDays\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\VendorBlackOutDays\Model\blackoutFactory $blackoutFactory
     * @param \Flordelcampo\VendorBlackOutDays\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\VendorBlackOutDays\Model\BlackoutFactory $BlackoutFactory,
        \Flordelcampo\VendorBlackOutDays\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    )
    {
        $this->_blackoutFactory = $BlackoutFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('blackout_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_blackoutFactory->create()->getCollection();
        $this->setCollection($collection);
        $collection->getSelect()->joinLeft(
            ['vendor' => 'vendor_registrations'],
            "main_table.vendor=vendor.vendor_id",
            [
                'vendor_name' => 'vendor.vendor_name',
            ]
        )->joinLeft(
            ['location' => 'dropship_vendor_locations'],
            "main_table.vendor_location_name=location.loc_id",
            [
                'location_name' => 'location.loc_name'
            ]
        )->joinLeft(
            ['shipping' => 'vendor_shipping'],
            "main_table.ship_method=shipping.shipping_method_id",
            [
                'method_name' => 'shipping.shipping_method_name'
            ]
        )->joinLeft(
            ['cost_channel' => 'vendor_cost_channel'],
            "main_table.cost_channel=cost_channel.cost_channel_id",
            [
                'channel_name' => 'cost_channel.cost_channel_name'
            ]
        );
        /*echo $collection->getSelect();
        die;*/
        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'blackout_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'blackout_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'vendor',
            [
                'header' => __('Vendor'),
                'index' => 'vendor_name'
            ]
        );
        $this->addColumn(
            'vendor_location_name',
            [
                'header' => __('vendor Location'),
                'index' => 'location_name',
            ]
        );
        $this->addColumn(
            'ship_method',
            [
                'header' => __('Ship Method'),
                'index' => 'method_name',
            ]
        );
        $this->addColumn(
            'cost_channel',
            [
                'header' => __('Cost Channel'),
                'index' => 'channel_name',
            ]
        );
        /*$this->addColumn(
            'reason',
            [
                'header' => __('Reason'),
                'index' => 'reason',
            ]
        );*/
        $this->addColumn(
            'block_type',
            [
                'header' => __('Block Type'),
                'index' => 'block_type',
                'type' => 'options',
                'options' => \Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Grid::getOptionArray5()
            ]
        );
        $this->addColumn(
            'start_date',
            [
                'header' => __('Start Date'),
                'index' => 'start_date',
                'type' => 'datetime',
            ]
        );
        $this->addColumn(
            'end_date',
            [
                'header' => __('End Date'),
                'index' => 'end_date',
                'type' => 'datetime',
            ]
        );
        $this->addExportType($this->getUrl('vendorblackoutdays/*/exportCsv', ['_current' => true]), __('CSV'));
        $this->addExportType($this->getUrl('vendorblackoutdays/*/exportExcel', ['_current' => true]), __('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }
        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('blackout_id');
        $this->getMassactionBlock()->setFormFieldName('blackout');
        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('vendorblackoutdays/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );
        $statuses = $this->_status->getOptionArray();
        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('vendorblackoutdays/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );
        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('vendorblackoutdays/*/index', ['_current' => true]);
    }

    /**
     * @param blackout $row
     * @return string
     */
    public function getRowUrl($row)
    {

        return $this->getUrl(
            'vendorblackoutdays/*/edit',
            ['blackout_id' => $row->getId()]
        );

    }


    static public function getOptionArray0()
    {
        $data_array = array();
        $data_array[0] = 'Vendor1';
        $data_array[1] = 'Vendor2';
        return ($data_array);
    }

    static public function getValueArray0()
    {
        $data_array = array();
        foreach (\Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Grid::getOptionArray0() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return ($data_array);

    }

    static public function getOptionArray1()
    {
        $data_array = array();
        $data_array[0] = 'Location1';
        $data_array[1] = 'Location2';
        return ($data_array);
    }

    static public function getValueArray1()
    {
        $data_array = array();
        foreach (\Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Grid::getOptionArray1() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return ($data_array);

    }

    static public function getOptionArray2()
    {
        $data_array = array();
        $data_array[0] = 'Cost Channel1';
        $data_array[1] = 'Cost Channel2';
        return ($data_array);
    }

    static public function getValueArray2()
    {
        $data_array = array();
        foreach (\Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Grid::getOptionArray2() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return ($data_array);

    }

    static public function getOptionArray3()
    {
        $data_array = array();
        $data_array[0] = 'Ups';
        $data_array[1] = 'Fedx';
        return ($data_array);
    }

    static public function getValueArray3()
    {
        $data_array = array();
        foreach (\Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Grid::getOptionArray3() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return ($data_array);

    }

    static public function getOptionArray5()
    {
        $data_array = array();
        $data_array[0] = 'Block Farm Ship Date';
        $data_array[1] = 'Block Delivery Date';
        return ($data_array);
    }

    static public function getValueArray5()
    {
        $data_array = array();
        foreach (\Flordelcampo\VendorBlackOutDays\Block\Adminhtml\Blackout\Grid::getOptionArray5() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return ($data_array);

    }


}