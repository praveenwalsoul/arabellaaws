<?php
namespace Flordelcampo\VendorBlackOutDays\Model;

class Blackout extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\VendorBlackOutDays\Model\ResourceModel\Blackout');
    }
}
?>