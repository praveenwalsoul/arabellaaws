<?php

namespace Flordelcampo\VendorBlackOutDays\Model\Config\Source;

class Location implements \Magento\Framework\Option\ArrayInterface
{
    protected $location;

    public function __construct(\Flordelcampo\DropshipLocations\Model\Location $locationFactory)
    {
        $this->location = $locationFactory;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $collections = $this->location->getCollection();
        $data = [];
        $i = 0;
        foreach ($collections as $value) {
            $data[$i]['value'] = $value->getId();//.' '.$value->getLastName();
            $data[$i]['label'] = $value->getLocName(); //$value->getVendorId()
            $i++;
        }
        $this->_options = $data;
        return $this->_options;
    }

    public function toArray()
    {
        return [];
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
    /*
    public function toOptionArray($value)
    {
        
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    } */
}