<?php
namespace Flordelcampo\VendorBlackOutDays\Model\Config\Source;

class CostChannel implements \Magento\Framework\Option\ArrayInterface
{
    protected $costChannel;

    public function __construct(\Flordelcampo\VendorCostchannel\Model\Costchannel $costChannelFactory) {
        $this->costChannel = $costChannelFactory;
    }
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
       
    $collections = $this->costChannel->getCollection();

    $data = array();
    $i=0;
    foreach ($collections as $value) {
        $data[$i]['value'] = $value->getId();//.' '.$value->getLastName();
        $data[$i]['label'] = $value->getCostChannelName(); //$value->getVendorId()
        $i++;
    }        
     $this->_options = $data;
   
    return $this->_options;
    }
    
    public function toArray()
    {
        return [];
    } 

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
    /*
    public function toOptionArray($value)
    {
        
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    } */
}