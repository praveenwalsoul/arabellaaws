<?php
namespace Flordelcampo\VendorBlackOutDays\Model\ResourceModel;

class Blackout extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendor_blackout', 'blackout_id');
    }
}
?>