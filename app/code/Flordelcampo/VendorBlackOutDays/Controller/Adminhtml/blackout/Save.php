<?php

namespace Flordelcampo\VendorBlackOutDays\Controller\Adminhtml\blackout;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultInterface;


class Save extends \Magento\Backend\App\Action
{

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (isset($data['vendor']))
            $data['vendor'] = $data['vendor'];
        if (isset($data['vendor_location_name']))
            $data['vendor_location_name'] = $data['vendor_location_name'];
        if (isset($data['cost_channel']))
            $data['cost_channel'] = $data['cost_channel'];
        if (isset($data['ship_method']))
            $data['ship_method'] = $data['ship_method'];

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('Flordelcampo\VendorBlackOutDays\Model\Blackout');

            $id = $this->getRequest()->getParam('blackout_id');
            if ($id) {
                $model->load($id);
                $model->setCreatedAt(date('Y-m-d H:i:s'));
            }
            $model->setData($data);
            try {
                $model->save();
                $this->messageManager->addSuccess(__('The Blackout has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['blackout_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Blackout.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['blackout_id' => $this->getRequest()->getParam('blackout_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}