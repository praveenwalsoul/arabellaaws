<?php
namespace Flordelcampo\Prodreview\Controller\Adminhtml\Test;
 
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;


class ProductCsv extends \Magento\Backend\App\Action
{
protected $_fileFactory;
protected $directory;
protected $prdCollection;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $prdCollection
    ) {
        //$this->orderRepository = $orderRepository;
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->prdCollection = $prdCollection;
        parent::__construct($context);
    }
 
    public function execute()
    {
    	//echo"hhkhkh";die();
        $collection = $this->prdCollection->addAttributeToSelect('*')
                      ->addFieldToFilter('channel',
                array(
                    array('finset'=> array('5433')),
                    array('finset'=> array('5434')),
                    
                )
            )
                            ->joinField(
                               'qty', 'cataloginventory_stock_item', 'qty', 'product_id=entity_id', "{{table}}.stock_id=1", 'left'
                           );

        $name = date('d_m_Y_H_i_s');
        $filepath = 'export/custom' . $name . '.csv';
        $this->directory->create('export');
        /* Open file */
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();
        $columns = $this->getColumnHeader();
        foreach ($columns as $column) {
            $header[] = $column;
        }
        /* Write Header */
        $stream->writeCsv($header);
 
        foreach ($collection as $item) {
            $itemData = [];
            $itemData[] = $item->getSku();
            $itemData[] = $item->getName();
            $itemData[] = $item->getQty();
            $itemData[] = $item->getPrice();
            $stream->writeCsv($itemData);
        }
 
        $content = [];
        $content['type'] = 'filename'; // must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; //remove csv from var folder
 
        $csvfilename = 'Product.csv';
        return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
        
    }
 
    /* Header Columns */
    public function getColumnHeader() {
        $headers = ['Sku','Product name','Qty','Price'];
        return $headers;
    }
}