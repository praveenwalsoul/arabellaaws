<?php

namespace Flordelcampo\Prodreview\Controller\Adminhtml\Test;

use Magento\Framework\App\Filesystem\DirectoryList;

class Import extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
   // const ADMIN_RESOURCE = 'Flordelcampo_Prodreview::manage_pincodes';

    /**
     * Image uploader
     *
     * @var \Ktpl\BannerSlider\BannerImageUploader
     */
    private $csvUploader;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * CSV Processor
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;
    protected $productFactory;
    protected $resultRedirect;
    

    /**
     * @param Magento\Backend\App\Action\Context $context
     * @param Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Magento\Framework\Filesystem $filesystem
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Controller\ResultFactory $result,
        \Magento\Framework\File\Csv $csvProcessor
    ) {
        $this->_filesystem = $filesystem;
        $this->_storeManager = $storeManager;
        $this->csvProcessor = $csvProcessor;
        $this->productFactory = $productFactory;
        $this->resultRedirect = $result;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
        
        $file = $this->getRequest()->getFiles('file_upload');

        $resultRedirect = $this->resultRedirectFactory->create();

         if (!isset($file['tmp_name'])) 
        throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));

        $csvData = $this->csvProcessor->getData($file['tmp_name']);
       
         foreach ($csvData as $row => $data) {
            if ($row > 0){
              try {
          
                $product = $this->productFactory->create();
                $checkSku = $product->getIdBySku($data[0]);
                    if(!empty($checkSku)){
                $product->load($checkSku);
                $product->setPrice($data[3]);
                $product->setStockData(['qty' => $data[2]]);
                $product->setQuantityAndStockStatus(['qty' => $data[2], 'is_in_stock' => 1]);
                $product->save();
            }


            }
            catch (LocalizedException $e) {
                $messages[] = $this->getErrorWithTestId($product, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithTestId($product, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithTestId(
                    $product,
                    __('Something went wrong while Update.')
                );
                $error = true;
            } 
            
        }
    }
        //echo "string";
    //die();
        $resultRedirect->setPath('flordelcampo_prodreview/test/index');
        return $resultRedirect;
    }


}