<?php
namespace Flordelcampo\Prodreview\Controller\Adminhtml\Test;

use Flordelcampo\Prodreview\Api\TestRepositoryInterface;
use Flordelcampo\Prodreview\Api\Data\TestInterface;
use Flordelcampo\Prodreview\Model\ResourceModel\Test as TestResourceModel;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;

/**
 * Class InlineEdit
 */
class InlineEdit extends Action
{
    /**
     * Test repository
     * @var TestRepositoryInterface
     */
    protected $testRepository;
    /**
     * Data object processor
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * Data object helper
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * JSON Factory
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * Test resource model
     * @var TestResourceModel
     */
    protected $testResourceModel;


   protected $_product;

    /**
     * @var Magento\CatalogInventory\Api\StockStateInterface 
     */
    protected $_stockStateInterface;

    /**
     * @var Magento\CatalogInventory\Api\StockRegistryInterface 
     */
    protected $_stockRegistry;


    /**
     * constructor
     * @param Context $context
     * @param TestRepositoryInterface $testRepository
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param JsonFactory $jsonFactory
     * @param TestResourceModel $testResourceModel
     */
    public function __construct(
        Context $context,
        TestRepositoryInterface $testRepository,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        JsonFactory $jsonFactory,
        \Magento\Catalog\Model\Product $product,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry, 
        TestResourceModel $testResourceModel
    ) {
         $this->_product = $product;
        $this->_stockStateInterface = $stockStateInterface;
        $this->_stockRegistry = $stockRegistry;
        $this->testRepository = $testRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->jsonFactory = $jsonFactory;
        $this->testResourceModel = $testResourceModel;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
       // echo"sanoj"; die("hello");
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        //print_r($postItems);die("hello");
        //var_dump($postItems);die("hello");
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach ($postItems as $testId) {
           // echo $testId;die("fdfd");
            //print_r($testId);die("jkg");
            try {
          
            
            //$stockData = $this->getTotalStock($testId['entity_id']);//Load product stock.
            $product=$this->_product->load($testId['entity_id']); //load product which you want to update stock
            $product->setPrice($testId['price']);
            $product->setStockData(['qty' => $testId['qty']]);
            $product->setQuantityAndStockStatus(['qty' => $testId['qty'], 'is_in_stock' => 1]);
            $product->save();
            


        }
        catch (LocalizedException $e) {
                $messages[] = $this->getErrorWithTestId($product, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithTestId($product, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithTestId(
                    $product,
                    __('Something went wrong while saving the Test.')
                );
                $error = true;
            }
  }
            return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
            


    /**
     * Add Test id to error message
     *
     * @param \Flordelcampo\Prodreview\Api\Data\TestInterface $test
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithTestId(TestInterface $test, $errorText)
    {
        return '[Test ID: ' . $test->getId() . '] ' . $errorText;
    }
}
