<?php
namespace Flordelcampo\Prodreview\Controller\Adminhtml\Test;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Flordelcampo\Prodreview\Api\TestRepositoryInterface;

class Edit extends Action
{
    /**
     * @var TestRepositoryInterface
     */
    private $testRepository;
    /**
     * @var Registry
     */
    private $registry;

    /**
     * Edit constructor.
     * @param Context $context
     * @param TestRepositoryInterface $testRepository
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        TestRepositoryInterface $testRepository,
        Registry $registry
    ) {
        $this->testRepository = $testRepository;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * get current Test
     *
     * @return null|\Flordelcampo\Prodreview\Api\Data\TestInterface
     */
    private function initTest()
    {
        $testId = $this->getRequest()->getParam('test_id');
        try {
            $test = $this->testRepository->get($testId);
        } catch (NoSuchEntityException $e) {
            $test = null;
        }
        $this->registry->register('current_test', $test);
        return $test;
    }

    /**
     * Edit or create Test
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $test = $this->initTest();
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Flordelcampo_Prodreview::prodreview_test');
        $resultPage->getConfig()->getTitle()->prepend(__('Dashboar'));

        if ($test === null) {
            $resultPage->getConfig()->getTitle()->prepend(__('New Dashboar'));
        } else {
            $resultPage->getConfig()->getTitle()->prepend($test->getName());
        }
        return $resultPage;
    }
}
