<?php
namespace Flordelcampo\Prodreview\Controller\Adminhtml\Test;

use Flordelcampo\Prodreview\Api\TestRepositoryInterface;
use Flordelcampo\Prodreview\Api\Data\TestInterface;
use Flordelcampo\Prodreview\Api\Data\TestInterfaceFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;

/**
 * Class Save
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save extends Action
{
    /**
     * Test factory
     * @var TestInterfaceFactory
     */
    protected $testFactory;
    /**
     * Data Object Processor
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * Data Object Helper
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * Data Persistor
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    /**
     * Core registry
     * @var Registry
     */
    protected $registry;
    /**
     * Test repository
     * @var TestRepositoryInterface
     */
    protected $testRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param TestInterfaceFactory $testFactory
     * @param TestRepositoryInterface $testRepository
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param DataPersistorInterface $dataPersistor
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        TestInterfaceFactory $testFactory,
        TestRepositoryInterface $testRepository,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        DataPersistorInterface $dataPersistor,
        Registry $registry
    ) {
        $this->testFactory = $testFactory;
        $this->testRepository = $testRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPersistor = $dataPersistor;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        //echo"save";die("hjhjhj");
        /** @var TestInterface $test */
        $test = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['test_id']) ? $data['test_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $test = $this->testRepository->get((int)$id);
            } else {
                unset($data['test_id']);
                $test = $this->testFactory->create();
            }
            $this->dataObjectHelper->populateWithArray($test, $data, TestInterface::class);
            $this->testRepository->save($test);
            $this->messageManager->addSuccessMessage(__('You saved the Test'));
            $this->dataPersistor->clear('flordelcampo_prodreview_test');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('*/*/edit', ['test_id' => $test->getId()]);
            } else {
                $resultRedirect->setPath('*/*');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('flordelcampo_prodreview_test', $postData);
            $resultRedirect->setPath('*/*/edit', ['test_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the Test'));
            $this->dataPersistor->set('flordelcampo\prodreview_test', $postData);
            $resultRedirect->setPath('*/*/edit', ['test_id' => $id]);
        }
        return $resultRedirect;
    }
}
