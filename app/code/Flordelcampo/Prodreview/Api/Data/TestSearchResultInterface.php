<?php
namespace Flordelcampo\Prodreview\Api\Data;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface TestSearchResultInterface
{
    /**
     * get items
     *
     * @return \Flordelcampo\Prodreview\Api\Data\TestInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param \Flordelcampo\Prodreview\Api\Data\TestInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $count
     * @return $this
     */
    public function setTotalCount($count);
}
