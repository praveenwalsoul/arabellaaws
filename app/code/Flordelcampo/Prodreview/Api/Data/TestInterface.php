<?php
namespace Flordelcampo\Prodreview\Api\Data;

/**
 * @api
 */
interface TestInterface
{
    const TEST_ID = 'entity_id';
    const NAME = 'name';
    //const PRODUCT_ID = 'product_id';
    /**
     * @var string
     */
   // const IS_ACTIVE = 'is_active';
    /**
     * @var int
     */
    //const STATUS_ENABLED = 1;
    /**
     * @var int
     */
   // const STATUS_DISABLED = 2;
    /**
     * @param int $id
     * @return TestInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return TestInterface
     */
    public function setTestId($id);

    /**
     * @return int
     */
    public function getTestId();

    /**
     * @param string $name
     * @return TestInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();
    /**
     * @param int $isActive
     * @return TestInterface
     */
   // public function setProductId($productId);

    /**
     * @return int
     */
    //public function getProductId();
}
