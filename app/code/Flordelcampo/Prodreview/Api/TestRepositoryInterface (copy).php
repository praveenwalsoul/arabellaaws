<?php
namespace Flordelcampo\Prodreview\Api;

use Flordelcampo\Prodreview\Api\Data\TestInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface TestRepositoryInterface
{
    /**
     * @param TestInterface $test
     * @return TestInterface
     */
    public function save(TestInterface $test);

    /**
     * @param $id
     * @return TestInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Flordelcampo\Prodreview\Api\Data\TestSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param TestInterface $test
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(TestInterface $test);

    /**
     * @param int $testId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($testId);

    /**
     * clear caches instances
     * @return void
     */
    public function clear();
}
