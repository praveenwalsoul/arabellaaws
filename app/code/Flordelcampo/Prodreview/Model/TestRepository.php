<?php
namespace Flordelcampo\Prodreview\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Flordelcampo\Prodreview\Api\Data\TestInterface;
use Flordelcampo\Prodreview\Api\Data\TestInterfaceFactory;
use Flordelcampo\Prodreview\Api\Data\TestSearchResultInterfaceFactory;
use Flordelcampo\Prodreview\Api\TestRepositoryInterface;
use Flordelcampo\Prodreview\Model\ResourceModel\Test as TestResourceModel;
use Flordelcampo\Prodreview\Model\ResourceModel\Test\Collection;
use Flordelcampo\Prodreview\Model\ResourceModel\Test\CollectionFactory as TestCollectionFactory;

class TestRepository implements TestRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $instances = [];

    /**
     * Test resource model
     *
     * @var TestResourceModel
     */
    protected $resource;

    /**
     * Test collection factory
     *
     * @var TestCollectionFactory
     */
    protected $testCollectionFactory;

    /**
     * Test interface factory
     *
     * @var TestInterfaceFactory
     */
    protected $testInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     *
     * @var TestSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     * @param TestResourceModel $resource
     * @param TestCollectionFactory $testCollectionFactory
     * @param TestnterfaceFactory $testInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param TestSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        TestResourceModel $resource,
        TestCollectionFactory $testCollectionFactory,
        TestInterfaceFactory $testInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        TestSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource             = $resource;
        $this->testCollectionFactory = $testCollectionFactory;
        $this->testInterfaceFactory  = $testInterfaceFactory;
        $this->dataObjectHelper     = $dataObjectHelper;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save Test.
     *
     * @param \Flordelcampo\Prodreview\Api\Data\TestInterface $test
     * @return \Flordelcampo\Prodreview\Api\Data\TestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(TestInterface $test)
    {
        /** @var TestInterface|\Magento\Framework\Model\AbstractModel $test */
        try {
            $this->resource->save($test);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Test: %1',
                $exception->getMessage()
            ));
        }
        return $test;
    }

    /**
     * Retrieve Test
     *
     * @param int $testId
     * @return \Flordelcampo\Prodreview\Api\Data\TestInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($testId)
    {
        if (!isset($this->instances[$testId])) {
            /** @var TestInterface|\Magento\Framework\Model\AbstractModel $test */
            $test = $this->testInterfaceFactory->create();
            $this->resource->load($test, $testId);
            if (!$test->getId()) {
                throw new NoSuchEntityException(__('Requested Test doesn\'t exist'));
            }
            $this->instances[$testId] = $test;
        }
        return $this->instances[$testId];
    }

    /**
     * Retrieve Tests matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Flordelcampo\Prodreview\Api\Data\TestSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Flordelcampo\Prodreview\Api\Data\TestSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Flordelcampo\Prodreview\Model\ResourceModel\Test\Collection $collection */
        $collection = $this->testCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
                );
            }
        } else {
            $collection->addOrder('main_table.' . TestInterface::TEST_ID, SortOrder::SORT_ASC);
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var TestInterface[] $tests */
        $tests = [];
        /** @var \Flordelcampo\Prodreview\Model\Test $test */
        foreach ($collection as $test) {
            /** @var TestInterface $testDataObject */
            $testDataObject = $this->testInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $testDataObject,
                $test->getData(),
                TestInterface::class
            );
            $tests[] = $testDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($tests);
    }

    /**
     * Delete Test
     *
     * @param TestInterface $test
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(TestInterface $test)
    {
        /** @var TestInterface|\Magento\Framework\Model\AbstractModel $test */
        $id = $test->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($test);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to removeTest %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Test by ID.
     *
     * @param int $testId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($testId)
    {
        $test = $this->get($testId);
        return $this->delete($test);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

    /**
     * clear caches instances
     * @return void
     */
    public function clear()
    {
        $this->instances = [];
    }
}
