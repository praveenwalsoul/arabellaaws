<?php
namespace Flordelcampo\Prodreview\Model\Test\Executor;

use Flordelcampo\Prodreview\Api\TestRepositoryInterface;
use Flordelcampo\Prodreview\Api\ExecutorInterface;

class Delete implements ExecutorInterface
{
    /**
     * @var TestRepositoryInterface
     */
    private $testRepository;

    /**
     * Delete constructor.
     * @param TestRepositoryInterface $testRepository
     */
    public function __construct(
        TestRepositoryInterface $testRepository
    ) {
        $this->testRepository = $testRepository;
    }

    /**
     * @param int $id
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute($id)
    {
        $this->testRepository->deleteById($id);
    }
}
