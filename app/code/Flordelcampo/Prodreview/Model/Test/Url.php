<?php

namespace Flordelcampo\Prodreview\Model\Test;

use Magento\Framework\UrlInterface;
use Flordelcampo\Prodreview\Api\Data\TestInterface;

class Url
{
    /**
     * url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
    /**
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return string
     */
    public function getListUrl()
    {
        return $this->urlBuilder->getUrl('flordelcampo_prodreview/test/index');
    }

    /**
     * @param TestInterface $test
     * @return string
     */
    public function getTestUrl(TestInterface $test)
    {
        return $this->urlBuilder->getUrl('flordelcampo_prodreview/test/view', ['id' => $test->getId()]);
    }
}
