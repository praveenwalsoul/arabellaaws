<?php
namespace Flordelcampo\Prodreview\Model\Test;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Flordelcampo\Prodreview\Model\ResourceModel\Test\CollectionFactory as TestCollectionFactory;

class DataProvider extends AbstractDataProvider
{
    /**
     * Loaded data cache
     *
     * @var array
     */
    protected $loadedData;

    /**
     * Data persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param TestCollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        TestCollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Flordelcampo\Prodreview\Model\Test $test */
        foreach ($items as $test) {
            $this->loadedData[$test->getId()] = $test->getData();
        }
        $data = $this->dataPersistor->get('flordelcampo_prodreview_test');
        if (!empty($data)) {
            $test = $this->collection->getNewEmptyItem();
            $test->setData($data);
            $this->loadedData[$test->getId()] = $test->getData();
            $this->dataPersistor->clear('flordelcampo_prodreview_test');
        }
        return $this->loadedData;
    }
}
