<?php
namespace Flordelcampo\Prodreview\Model;

use Flordelcampo\Prodreview\Api\Data\TestInterface;
use Magento\Framework\Model\AbstractModel;
use Flordelcampo\Prodreview\Model\ResourceModel\Test as TestResourceModel;

/**
 * @method \Flordelcampo\Prodreview\Model\ResourceModel\Test _getResource()
 * @method \Flordelcampo\Prodreview\Model\ResourceModel\Test getResource()
 */
class Test extends AbstractModel implements TestInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'flordelcampo_prodreview_test';
    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'flordelcampo_prodreview_test';
    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'test';
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(TestResourceModel::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Page id
     *
     * @return array
     */
    public function getTestId()
    {
        return $this->getData(TestInterface::TEST_ID);
    }

    /**
     * set Test id
     *
     * @param  int $testId
     * @return TestInterface
     */
    public function setTestId($testId)
    {
        return $this->setData(TestInterface::TEST_ID, $testId);
    }

    /**
     * @param string $name
     * @return TestInterface
     */
    public function setName($name)
    {
        return $this->setData(TestInterface::NAME, $name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData(TestInterface::NAME);
    }

    /**
     * @param int $isActive
     * @return TestInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(TestInterface::IS_ACTIVE, $isActive);
    }

    /**
     * @return int
     */
    public function getIsActive()
    {
        return $this->getData(TestInterface::IS_ACTIVE);
    }
}
