<?php
namespace Flordelcampo\Prodreview\Model\ResourceModel;

class Test extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('flordelcampo_prodreview_test', 'test_id');
    }
}
