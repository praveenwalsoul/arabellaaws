<?php
namespace Flordelcampo\Prodreview\Model\ResourceModel\Test;

use Flordelcampo\Prodreview\Model\Test;
use Flordelcampo\Prodreview\Model\ResourceModel\AbstractCollection;
//use Flordelcampo\Prodreview\Model\ResourceModel\ProductCollection;

/**
 * @api
 */
class Collection extends AbstractCollection
//class Collection extends ProductCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Test::class,
            \Flordelcampo\Prodreview\Model\ResourceModel\Test::class
        );
    }
}
