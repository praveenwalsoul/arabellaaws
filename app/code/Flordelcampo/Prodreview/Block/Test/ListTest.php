<?php

namespace Flordelcampo\Prodreview\Block\Test;

use Magento\Framework\UrlFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Theme\Block\Html\Pager;
use Flordelcampo\Prodreview\Api\Data\TestInterface;
use Flordelcampo\Prodreview\Model\Test;
use Flordelcampo\Prodreview\Model\ResourceModel\Test\CollectionFactory as TestCollectionFactory;
use Flordelcampo\Prodreview\Model\Test\Url;

/**
 * @api
 */
class ListTest extends Template
{
    /**
     * @var TestCollectionFactory
     */
    private $testCollectionFactory;
    /**
     * @var \Flordelcampo\Prodreview\Model\ResourceModel\Test\Collection
     */
    private $tests;
    /**
     * @var Url
     */
    private $urlModel;
    /**
     * @param Context $context
     * @param TestCollectionFactory $testCollectionFactory
     * @param Url $urlModel
     * @param array $data
     */
    public function __construct(
        Context $context,
        TestCollectionFactory $testCollectionFactory,
        Url $urlModel,
        array $data = []
    ) {
        $this->testCollectionFactory = $testCollectionFactory;
        $this->urlModel = $urlModel;
        parent::__construct($context, $data);
    }

    /**
     * @return \Flordelcampo\Prodreview\Model\ResourceModel\Test\Collection
     */
    public function getTests()
    {
        if (is_null($this->tests)) {
            $this->tests = $this->testCollectionFactory->create()
                ->addFieldToFilter('is_active', TestInterface::STATUS_ENABLED)
                ->setOrder('name', 'ASC');
        }
        return $this->tests;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager $pager */
        $pager = $this->getLayout()->createBlock(Pager::class, 'flordelcampo.prodreview.test.list.pager');
        $pager->setCollection($this->getTests());
        $this->setChild('pager', $pager);
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @param TestInterface $test
     * @return string
    */
    public function getTestUrl(TestInterface $test)
    {
        return $this->urlModel->getTestUrl($test);
    }
}
