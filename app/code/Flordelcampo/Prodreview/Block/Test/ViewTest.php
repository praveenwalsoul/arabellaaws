<?php

namespace Flordelcampo\Prodreview\Block\Test;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * @api
 */
class ViewTest extends Template
{
    /**
     * @var Registry
     */
    private $coreRegistry;
    /**
     * @param Context $context
     * @param Registry $registry
     * @param $imageBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * get current Test
     *
     * @return \Flordelcampo\Prodreview\Api\Data\TestInterface
     */
    public function getCurrentTest()
    {
        return $this->coreRegistry->registry('current_test');
    }
}
