<?php
namespace Flordelcampo\Prodreview\Ui\Provider;

interface CollectionProviderInterface
{
    /**
     * @return \Flordelcampo\Prodreview\Model\ResourceModel\AbstractCollection
     */
    public function getCollection();
}
