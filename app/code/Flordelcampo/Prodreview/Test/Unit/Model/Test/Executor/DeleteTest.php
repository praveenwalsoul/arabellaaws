<?php
namespace Flordelcampo\Prodreview\Test\Unit\Model\Test\Executor;

use PHPUnit\Framework\TestCase;
use Flordelcampo\Prodreview\Api\TestRepositoryInterface;
use Flordelcampo\Prodreview\Api\Data\TestInterface;
use Flordelcampo\Prodreview\Model\Test\Executor\Delete;

class DeleteTest extends TestCase
{
    /**
     * @covers \Flordelcampo\Prodreview\Model\Test\Executor\Delete::execute()
     */
    public function testExecute()
    {
        /** @var TestRepositoryInterface | \PHPUnit_Framework_MockObject_MockObject $testRepository */
        $testRepository = $this->createMock(TestRepositoryInterface::class);
        $testRepository->expects($this->once())->method('deleteById');
        /** @var TestInterface | \PHPUnit_Framework_MockObject_MockObject $test */
        $test = $this->createMock(TestInterface::class);
        $delete = new Delete($testRepository);
        $delete->execute($test->getId());
    }
}
