<?php
namespace Flordelcampo\Prodreview\Test\Unit\Block\Adminhtml\Button\Test;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use PHPUnit\Framework\TestCase;
use Flordelcampo\Prodreview\Api\Data\TestInterface;
use Flordelcampo\Prodreview\Block\Adminhtml\Button\Test\Delete;

class DeleteTest extends TestCase
{
    /**
     * @var UrlInterface | \PHPUnit_Framework_MockObject_MockObject
     */
    private $url;
    /**
     * @var Registry | \PHPUnit_Framework_MockObject_MockObject
     */
    private $registry;
    /**
     * @var Delete
     */
    private $button;

    /**
     * set up tests
     */
    protected function setUp()
    {
        $this->url = $this->createMock(UrlInterface::class);
        $this->registry = $this->createMock(Registry::class);
        $this->button = new Delete($this->registry, $this->url);
    }

    /**
     * @covers \Flordelcampo\Prodreview\Block\Adminhtml\Button\Test\Delete::getButtonData()
     */
    public function testButtonDataNoTest()
    {
        $this->registry->method('registry')->willReturn(null);
        $this->url->expects($this->exactly(0))->method('getUrl');
        $this->assertEquals([], $this->button->getButtonData());
    }

    /**
     * @covers \Flordelcampo\Prodreview\Block\Adminhtml\Button\Test\Delete::getButtonData()
     */
    public function testButtonDataNoTestId()
    {
        $test = $this->createMock(TestInterface::class);
        $test->method('getId')->willReturn(null);
        $this->registry->method('registry')->willReturn($test);
        $this->url->expects($this->exactly(0))->method('getUrl');
        $this->assertEquals([], $this->button->getButtonData());
    }

    /**
     * @covers \Flordelcampo\Prodreview\Block\Adminhtml\Button\Test\Delete::getButtonData()
     */
    public function testButtonData()
    {
        $test = $this->createMock(TestInterface::class);
        $test->method('getId')->willReturn(2);
        $this->registry->method('registry')->willReturn($test);
        $this->url->expects($this->once())->method('getUrl');
        $data = $this->button->getButtonData();
        $this->assertArrayHasKey('on_click', $data);
        $this->assertArrayHasKey('label', $data);
        $this->assertArrayHasKey('class', $data);
    }
}
