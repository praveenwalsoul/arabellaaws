<?php

namespace Flordelcampo\Dashboards\Block;

use http\Exception;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Flordelcampo\Vendor\Helper\Data as vendorHelper;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Backend\Model\Auth\Session;
use Magento\User\Model\User;

/**
 * Class OrderTransfer
 * Flordelcampo\Dashboards\Block
 */
class OrderTransfer extends Template
{
    private $filter = null;
    /**
     * @var vendorHelper
     */
    protected $vendorHelper;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var Session
     */
    protected $authSession;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;

    /**
     * Ups constructor.
     * @param Context $context
     * @param vendorHelper $vendorHelper
     * @param orderHelper $orderHelper
     * @param Session $authSession
     * @param CustomerHelper $customerHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        vendorHelper $vendorHelper,
        orderHelper $orderHelper,
        Session $authSession,
        CustomerHelper $customerHelper,
        array $data = []
    )
    {
        $this->vendorHelper = $vendorHelper;
        $this->orderHelper = $orderHelper;
        $this->authSession = $authSession;
        $this->customerHelper = $customerHelper;
        parent::__construct($context, $data);
    }

    /**
     * Return Update Order Url
     * @return string
     */
    public function getUpdateVendorUrl()
    {
        return $this->getUrl('dashboards/ordertransfer/updatevendorforitem');
    }

    /**
     * @return AbstractDb|AbstractCollection|null
     */
    public function getOrderItemCollection()
    {
        return $this->orderHelper->getOrderItemCollection();
    }

    /**
     * @return array
     */
    public function getAllVendors()
    {
        return $this->vendorHelper->getAllActiveAndInactiveVendorList();
    }

    /**
     * @return array
     */
    public function getAllFarms()
    {
        return $this->vendorHelper->getAllActiveAndInactiveVendorFarmsList();
    }

    /**
     * @return mixed
     */
    public function getAllShippingMethods()
    {
        $methods = [];
        $shippingMethods = $this->vendorHelper->getAllShippingMethodsArray();
        foreach ($shippingMethods as $id => $value) {
            if ($id != '') {
                $methods[$id] = $value;
            }
        }
        return $methods;
    }

    /**
     * @return string[]
     */
    public function getTableHeaders()
    {
        return [
            '',
            'Item Id',
            'SKU',
            'Product Name',
            'Vendor',
            'Vendor Farm',
            'Shipping Method',
            'Action'
        ];
    }

    /**
     * Return Logged in user
     * @return User|null
     */
    public function getCurrentLoggedAdminUser()
    {
        return $this->authSession->getUser();
    }

    /**
     * @return $this|OrderTransfer
     */
    protected function _prepareLayout()
    {
        $this->initFilters();
        return $this;
    }

    /**
     * @return mixed
     */
    public function initFilters()
    {
        $filter = $this->getRequest()->getParam('order_item');
        if ($filter) {
            $filter = base64_decode($filter);
            parse_str($filter, $this->filter);
        }
        return $this->filter;
    }

    /**
     * @param $field
     * @return mixed|null
     */
    public function getFilterField($field)
    {
        if (isset($this->filter[$field]) && !empty($this->filter[$field])) {
            return $this->filter[$field];
        }
        return null;
    }

    /**
     * @return array
     */
    public function getStoresList()
    {
        $storeList = $this->orderHelper->getAllStoresListWithNameArray();
        asort($storeList);
        return $storeList;
    }

    /**
     * @return array
     */
    public function getCustomerList()
    {
        $customerData[''] = 'Please Select';
        try {
            $collection = $this->customerHelper->getCustomerCollection();
            foreach ($collection as $customer) {
                $customerData[$customer->getId()] = $customer->getName();
            }
            asort($customerData);
            $response[''] = 'Please Select';
            foreach ($customerData as $id => $value) {
                $response[$id] = $value;
            }
        } catch (\Exception $e) {
            return $customerData;
        }
        return $response;
    }

    /**
     * @return string[]
     */
    public function getNumberOfItemsList()
    {
        return [
            '1',
            '2',
            '4',
            '6'
        ];
    }
}