<?php

namespace Flordelcampo\Dashboards\Block;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Flordelcampo\Vendor\Helper\Data as vendorHelper;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Backend\Model\Auth\Session;
use Magento\User\Model\User;

/**
 * Class Ups
 *
 * Flordelcampo\Dashboards\Block
 */
class Ups extends Template
{

    /**
     * @var vendorHelper
     */
    protected $vendorHelper;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var Session
     */
    protected $authSession;

    /**
     * Ups constructor.
     * @param Context $context
     * @param vendorHelper $vendorHelper
     * @param orderHelper $orderHelper
     * @param Session $authSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        vendorHelper $vendorHelper,
        orderHelper $orderHelper,
        Session $authSession,
        array $data = []
    )
    {
        $this->vendorHelper = $vendorHelper;
        $this->orderHelper = $orderHelper;
        $this->authSession = $authSession;
        parent::__construct($context, $data);
    }

    /**
     * Ship from date
     *
     * @return false|string
     */
    public function shipFromDate()
    {
        $today = date('Y-m-d');
        return date('Y-m-d', (strtotime('-7 day', strtotime($today))));
    }

    /**
     * Ship To Date
     * @return false|string
     */
    public function shipToDate()
    {
        $today = date('Y-m-d');
        return date('Y-m-d', (strtotime('+7 day', strtotime($today))));
    }

    public function getDefaultFromDate()
    {
        $today = date('Y-m-d');
        return date('Y-m-d', (strtotime('-10 day', strtotime($today))));
    }

    public function getDefaultToDate()
    {
        return date('Y-m-d');
    }

    /**
     * Return Shipping Methods
     * @return mixed
     */
    public function allShippingMethodsArray()
    {
        return $this->vendorHelper->getAllShippingMethodsArray();
    }

    /**
     * Return Update shipping Method
     * @return string
     */
    public function updateShip()
    {
        return $this->getUrl('dashboards/extra/updateshipmehod');
    }

    /**
     * Return Update Vendor Location
     * @return string
     */
    public function updateVendorLocationUrl()
    {
        return $this->getUrl('dashboards/extra/vendorlocation');
    }

    /**
     * Get vendors location Farm
     *
     * @return array
     */
    public function getAllVendorsLocFarm()
    {
        return $this->vendorHelper->getAllVendorLocation();
    }

    /**
     * Get vendors list
     *
     * @return array
     */
    public function getAllVendors()
    {
        return $this->vendorHelper->getAllVendorListArray();
    }

    /**
     * Store List
     *
     * @return array
     */
    public function getAllStores()
    {
        $storeArray = $this->orderHelper->getStoreListArray();
        $storeArray[''] = 'all';
        asort($storeArray);
        return $storeArray;
    }

    /**
     * Return Controller URL
     *
     * @return string
     */
    public function getUpsInfoUrl()
    {
        return $this->getUrl('dashboards/ups/info');
    }

    /**
     * Return gift template Download URl
     * @return string
     */
    public function getTemplateDownloadUrl()
    {
        return $this->getUrl('dashboards/ups/templates');
    }

    /**
     * Return Upload URL
     *
     * @return string
     */
    public function getUploadUrl()
    {
        return $this->getUrl('dashboards/ups/upload');
    }

    /**
     * Return Amazon Cancelled Order Url
     *
     * @return string
     */
    public function getAmazonCancelledOrderUrl()
    {
        return $this->getUrl('flordel_amazon/action/cancelledorder');
    }

    /**
     * Return order Delete URL
     *
     * @return string
     */
    public function getDeleteOrderUrl()
    {
        return $this->getUrl('create/amazon/delete');
    }

    /**
     * Return order State change URL
     *
     * @return string
     */
    public function getStateOrderUrl()
    {
        return $this->getUrl('dashboards/extra/orderstate');
    }

    /**
     * Return Amazon Price Update URL
     *
     * @return string
     */
    public function getPriceUpdateUrl()
    {
        return $this->getUrl('dashboards/extra/updateamazonprice');
    }

    /**
     * Return Offers Inventory Decrement Cron Url
     *
     * @return string
     */
    public function getOfferInventoryUpdateCronUrl()
    {
        return $this->getUrl('vendorinventory/inventory/cron');
    }

    /**
     * Return box State change URL
     *
     * @return string
     */
    public function getBoxStateUrl()
    {
        return $this->getUrl('dashboards/extra/boxstate');
    }

    /**
     * Return Ups Order Collection
     * @return Collection
     */
    public function getUpsOrdersCollection()
    {
        return $this->orderHelper->getProcessingOrderCollection();
    }

    /**
     * Return Validate URl
     * @return string
     */
    public function getValidateUrl()
    {
        return $this->getUrl('dashboards/upsaddressvalidate/validate');
    }

    /**
     * Return Validate URl
     * @return string
     */
    public function getValidateAllCronUrl()
    {
        return $this->getUrl('dashboards/upsaddressvalidate/validatecron');
    }

    /**
     * Return Edit URL
     * @return string
     */
    public function getEditAddressUrl()
    {
        return $this->getUrl('dashboards/upsaddressvalidate/edit');
    }

    /**
     * Return Address Save URL
     * @return string
     */
    public function getSaveAddressUrl()
    {
        return $this->getUrl('dashboards/upsaddressvalidate/save');
    }

    /**
     * Return Delete Order Url
     * @return string
     */
    public function deleteOrderByIncrementIdUrl()
    {
        return $this->getUrl('dashboards/extra/deleteorder');
    }

    /**
     * Return current vendor user id if logged in user is vendor
     */
    public function getVendorUserId()
    {
        $vendorUserId = 0;
        try {
            $user = $this->getCurrentLoggedAdminUser();
            $currentUserEmail = $user->getEmail();
            $vendorCollection = $this->vendorHelper->getVendorCollectionByEmail($currentUserEmail);
            if ($vendorCollection != null) {
                foreach ($vendorCollection as $vendor) {
                    $vendorUserId = $vendor->getVendorId();
                }
            }
        } catch (\Exception $e) {
            $vendorUserId = 0;
        }
        return $vendorUserId;
    }

    /**
     * Return Update Box Label Url
     * @return string
     */
    public function updateBoxUrl()
    {
        return $this->getUrl('dashboards/extra/updateorderlabel');
    }

    /**
     * Return Logged in user
     * @return User|null
     */
    public function getCurrentLoggedAdminUser()
    {
        return $this->authSession->getUser();
    }
}