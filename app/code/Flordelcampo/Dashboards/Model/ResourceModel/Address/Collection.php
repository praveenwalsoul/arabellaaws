<?php

namespace Flordelcampo\Dashboards\Model\ResourceModel\Address;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * Flordelcampo\Dashboards\Model\ResourceModel\Address
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Flordelcampo\Dashboards\Model\Address',
            'Flordelcampo\Dashboards\Model\ResourceModel\Address'
        );
    }

}
