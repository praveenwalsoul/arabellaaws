<?php

namespace Flordelcampo\Dashboards\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Address
 * Flordelcampo\Dashboards\Model\ResourceModel
 */
class Address extends AbstractDb
{
    /**
     * Post constructor.
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Box Table
     */
    protected function _construct()
    {
        $this->_init('ups_address_validation', 'entity_id');
    }

}