<?php

namespace Flordelcampo\Dashboards\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\Dashboards\Model\ResourceModel\Address as ResourceBoxModel;

/**
 * Class Address
 * Flordelcampo\Dashboards\Model
 */
class Address extends AbstractModel
{
    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceBoxModel::class);
    }

}