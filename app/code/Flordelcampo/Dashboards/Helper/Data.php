<?php

namespace Flordelcampo\Dashboards\Helper;

use Magento\Customer\Model\Address;
use Magento\Customer\Model\Customer;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Flordelcampo\Dashboards\Model\AddressFactory;
use Flordelcampo\Vendor\Helper\Data as vendorHelper;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Data
 *
 * Flordelcampo\Dashboards\Helper
 */
class Data extends AbstractHelper
{
    const VENDOR_TYPE_DC = 'flordel_vendor_configuration/general/vendor_type_dc';
    const VENDOR_TYPE_FARM = 'flordel_vendor_configuration/general/vendor_type_farm';
    /**
     * @var AddressFactory
     */
    protected $AddressFactory;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var vendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * Data constructor.
     * @param Context $context
     * @param AddressFactory $upsAddressFactory
     * @param vendorHelper $vendorHelper
     * @param orderHelper $orderHelper
     * @param CustomerHelper $customerHelper
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Context $context,
        AddressFactory $upsAddressFactory,
        vendorHelper $vendorHelper,
        orderHelper $orderHelper,
        CustomerHelper $customerHelper,
        CatalogHelper $catalogHelper
    )
    {
        $this->vendorHelper = $vendorHelper;
        $this->orderHelper = $orderHelper;
        $this->customerHelper = $customerHelper;
        $this->AddressFactory = $upsAddressFactory;
        $this->catalogHelper = $catalogHelper;
        parent::__construct($context);
    }

    /**
     * Return Address Model
     * @return mixed
     */
    public function getUpsAddressModel()
    {
        return $this->AddressFactory->create();
    }

    /**
     * Return model by entity id
     * @param $upsEntityId
     * @return mixed
     */
    public function getUpsAddressModelByEntityId($upsEntityId)
    {
        return $this->AddressFactory->create()->load($upsEntityId);
    }

    /**
     * Return Address Data by order id
     * @param $orderId
     * @return |null
     */
    public function getUpsAddressModelByOrderId($orderId)
    {
        $addressModel = $this->AddressFactory->create();
        $addressCollection = $addressModel->getCollection();
        $addressCollection->addFieldToFilter('order_id', $orderId);
        if (count($addressCollection) > 0) {
            return $addressCollection;
        } else {
            return null;
        }
    }

    /**
     * @param $customerId
     * @return mixed
     */
    public function getCustomerAddressByCustomerId($customerId)
    {
        $customerAddress[''] = 'Please Select';
        try {
            $activeStoresArray = $this->customerHelper->getActiveCustomerStoresByCustomerId($customerId);
            if (count($activeStoresArray) > 0) {
                foreach ($activeStoresArray as $storeId => $storeName) {
                    $customerAddress[$storeId] = $storeName;
                }
            }
        } catch (\Exception $e) {
            return $customerAddress;
        }
        return $customerAddress;
    }

    /**
     * @param $customerId
     * @return Customer
     */
    public function getCurrentCustomerById($customerId)
    {
        return $this->customerHelper->getCustomerById($customerId);
    }

    /**
     * @param $customerId
     * @return Address|null
     */
    public function getCustomerBillingAddress($customerId)
    {
        $customer = $this->customerHelper->getCustomerById($customerId);
        $billingAddressId = $customer->getDefaultBilling();
        if ($billingAddressId != null) {
            return $this->customerHelper->getCustomerAddressById($billingAddressId);
        }
        return null;
    }

    /**
     * @param $customerId
     * @return Address|null
     */
    public function getCustomerShippingAddress($customerId)
    {
        $customer = $this->customerHelper->getCustomerById($customerId);
        $shippingAddressId = $customer->getDefaultShipping();
        if ($shippingAddressId != null) {
            return $this->customerHelper->getCustomerAddressById($shippingAddressId);
        }
        return null;
    }

    /**
     * @param $customerId
     * @return Customer|null
     */
    public function getSalesRepByCustomerId($customerId)
    {
        $customer = $this->customerHelper->getCustomerById($customerId);
        $salesRepId = $customer->getData('cust_sales_rep');
        if (is_numeric($salesRepId)) {
            return $this->customerHelper->getCustomerById($salesRepId);
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getAllShippingMethods()
    {
        return $this->vendorHelper->getAllShippingMethodsArray();
    }

    /**
     *
     * @throws NoSuchEntityException
     */
    public function getInventoryOfferCollection()
    {
        /*$dc = $this->vendorHelper->getConfig(self::VENDOR_TYPE_DC);*/
        $farm = $this->vendorHelper->getConfig(self::VENDOR_TYPE_FARM);
        $params['vendor_type'] = $farm;
        $params['status'] = 1;
        $offerCollection = $this->vendorHelper->getVendorInventoryOfferCollection($params);
        $offerCollection->getSelect()
            ->joinLeft(
                ['vendor' => 'vendor_registrations'],
                "main_table.vendor_id=vendor.vendor_id",
                ['vendor_name' => 'vendor.vendor_name']
            )->joinLeft(
                ['location' => 'dropship_vendor_locations'],
                "main_table.loc_id = location.loc_id",
                ['location_name' => 'location.loc_name']
            );
        $offerData = $skuVendor = $oneSkuData = $response = [];
        if ($offerCollection->getSize() > 0) {
            foreach ($offerCollection as $offer) {
                $sku = $offer->getData('sku');
                $vendorId = $offer->getData('vendor_id');
                $locationId = $offer->getData('loc_id');
                $product = $this->catalogHelper->getProductBySku($sku);
                $skuVendor[$sku][] = [
                    'vendor_id' => $vendorId . '-' . $locationId,
                    'vendor_name' => $offer->getData('vendor_name')
                ];
                $oneSkuData[$sku] = [
                    'sku' => $sku,
                    'product_name' => $offer->getData('name'),
                    'vendor_id' => $vendorId,
                    'location_id' => $locationId,
                    'vendor_name' => $offer->getData('vendor_name'),
                    'location_name' => $offer->getData('location_name'),
                    'po_price' => $product->getData('po_price'),
                    'box_type' => $product->getData('box_type'),
                    'product_um' => $product->getData('product_um'),
                    'pack' => $product->getData('group'),
                    'box_count' => 1,
                    'qty' => 1,
                    'price' => round($product->getPrice(), 2),
                    'ext_price' => round($product->getPrice(), 2)
                ];
                $offerData[$sku][$vendorId] = [
                    'sku' => $sku,
                    'product_name' => $offer->getData('name'),
                    'vendor_id' => $vendorId,
                    'location_id' => $locationId,
                    'vendor_name' => $offer->getData('vendor_name'),
                    'location_name' => $offer->getData('location_name'),
                    'po_price' => $product->getData('po_price'),
                    'box_type' => $product->getData('box_type'),
                    'product_um' => $product->getData('product_um'),
                    'pack' => $product->getData('group'),
                    'box_count' => 1,
                    'qty' => 1,
                    'price' => round($product->getPrice(), 2),
                    'ext_price' => round($product->getPrice(), 2)
                ];
            }
        }
        $response['all_sku_vendor_data'] = $offerData;
        $response['one_sku_vendor_data'] = $oneSkuData;
        $response['sku_with_multi_vendor_data'] = $skuVendor;
        return $response;
    }

    public function getTableHeaders()
    {
        return [
            'SKU',
            'Product Description',
            'Vendor',
            'PO Price',
            'Box Type',
            'Product UM',
            'Pack',
            '#Box',
            'Qty',
            'Price',
            'Ext Price',
        ];
    }
}
