<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\Ups;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Dashboards\Block\Ups;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

date_default_timezone_set('America/New_York');

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\Ups
 */
class Info extends Action
{

    /**
     * @var null
     */
    protected $_params;
    /**
     * @var
     */
    protected $_vendorUserId;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var Ups
     */
    protected $upsBlock;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param ResourceConnection $resourceConnection
     * @param Ups $upsBlock
     * @param VendorHelper $vendorHelper
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        ResourceConnection $resourceConnection,
        Ups $upsBlock,
        VendorHelper $vendorHelper,
        CatalogHelper $catalogHelper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->upsBlock = $upsBlock;
        $this->vendorHelper = $vendorHelper;
        $this->catalogHelper = $catalogHelper;
        parent::__construct($context);
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $vendorUserId = $this->upsBlock->getVendorUserId();
            $this->_vendorUserId = $vendorUserId;
            $params = $this->getRequest()->getParam('data');
            $this->_params = $params;
            if (count($params) > 0) {
                $responseData = $this->getBoxDataHtml();
                $data = [
                    'status' => true,
                    'html' => $responseData['html'],
                    'sku_html' => $responseData['sku_html'],
                    'box_ids' => $responseData['all_box_ids']
                ];
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Invalid inputs'
                ];
            }

        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * Return Box Data
     * @return array
     */
    public function getBoxDataHtml()
    {
        $queryResult = $this->getQueryResult();
        $vendorWiseData = $this->vendorWiseQueryResult($queryResult);
        $headers = $this->getHeaders();
        $html = "<table class='admin__table-secondary customer-order-items-table'><tbody>";
        $html .= "<tr>";
        $vendorUserId = $this->_vendorUserId;
        $hideColumnsArray = $this->hideColumns();
        foreach ($headers as $colName) {
            if ($vendorUserId > 0 && in_array($colName, $hideColumnsArray)) {
                continue;
            }
            $html .= "<th>$colName</th>";
        }
        $html .= "</tr>";
        $grandTotalProcessing = $vendorWiseData['grand_total_processed_count'];
        $grandTotalPrinted = $vendorWiseData['grand_total_printed_count'];
        $grandTotalVendorPrint = $vendorWiseData['grand_total_vendor_print_count'];
        $grandTotalShipped = $vendorWiseData['grand_total_shipped_count'];
        $grandTotalNotShipped = $vendorWiseData['grand_total_not_shipped_count'];
        $grandTotalBoxes = $vendorWiseData['grand_total_boxes_count'];
        $vendorData = $vendorWiseData['vendor_data'];

        /**
         * Vendor wise SKU HTML
         */
        $skuHtml = $this->getVendorSkuWiseHtml($vendorWiseData);
        /**
         * Vendor Confirm Box Data
         */
        $vendorConfirmBoxes = $this->getVendorConfirmBoxesData();

        /**
         * Block Params
         */
        $isVendor = "false";
        $disabled = '';
        if ($vendorUserId > 0) {
            $isVendor = "true";
            $disabled = 'disabled';
        }
        $formKey = $this->upsBlock->getFormKey();
        $TemplateUrl = $this->upsBlock->getTemplateDownloadUrl();
        $totalConfirmBoxes = 0;
        foreach ($vendorData as $key => $oneVendor) {
            $vendorId = $oneVendor['vendor_id'];
            /**
             * Confirm Boxes
             */
            $vendorConfirmBoxCount = 0;
            if (isset($vendorConfirmBoxes[$vendorId])) {
                $vendorConfirmBoxCount = $vendorConfirmBoxes[$vendorId];
            }
            $totalConfirmBoxes += $vendorConfirmBoxCount;
            $boxes = (isset($oneVendor['boxes'])) ? $oneVendor['boxes'] : '';
            $vendorName = $oneVendor['vendor_name'];
            $processedBox = (isset($oneVendor['processing'])) ? $oneVendor['processing'] : '0';
            $printedBox = (isset($oneVendor['printed'])) ? $oneVendor['printed'] : '0';
            $vendorPrintedBox = (isset($oneVendor['vendorPrint'])) ? $oneVendor['vendorPrint'] : '0';
            $shippedBox = (isset($oneVendor['shipped'])) ? $oneVendor['shipped'] : '0';
            $notShippedBox = (isset($oneVendor['not_shipped'])) ? $oneVendor['not_shipped'] : '0';
            $totalBox = (isset($oneVendor['total'])) ? $oneVendor['total'] : '0';

            $processedBoxHtml = $shippedBoxHtml = $totalBoxHtml = $printedBoxHtml = $vendorPrintedBoxHtml = '0';
            $notShippedBoxHtml = '0';
            if ($processedBox != '0') {
                $processedBoxes = $oneVendor['process_boxes'];
                $processedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $processedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $processedBox . "'></form>";
            }

            if ($printedBox != '0') {
                $printedBoxes = $oneVendor['printed_boxes'];
                $printedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $printedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $printedBox . "'></form>";
            }
            if ($vendorPrintedBox != '0') {
                $vendorPrintedBoxes = $oneVendor['vendor_printed_boxes'];
                $vendorPrintedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $vendorPrintedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $vendorPrintedBox . "'></form>";
            }

            if ($shippedBox != '0') {
                $shippedBoxes = $oneVendor['shipped_boxes'];
                $shippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $shippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $shippedBox . "'></form>";
            }
            if ($notShippedBox != '0') {
                $notShippedBoxes = $oneVendor['not_shipped_boxes'];
                $notShippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $notShippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $notShippedBox . "'></form>";
            }
            if ($totalBox != '0') {
                $totalBoxes = $oneVendor['total_boxes'];
                $totalBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $totalBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $totalBox . "'></form>";
            }

            /**
             * Download Gift Message Form
             */
            $giftForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='gift' >
                <input type='submit' value='Download'></form>";

            /**
             * UPs File Form
             */
            $upsFileForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='ups_file' >
                <input type='submit' value='Download'></form>";

            /**
             * Track Id
             */
            $trackFileForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='track_file' >
                <input type='submit' value='Download'></form>";

            /**
             * All Boxes with Box Id and status
             */
            $allBoxFileForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $boxes . "' >
                <input type='hidden' name='type' value='all_boxes' >
                <input type='submit' value='All Boxes'></form>";

            $html .= "<tr>";
            $html .= "<td class='value'>" . $vendorName . "</td>";
            $html .= "<td class='value'>" . $processedBoxHtml . "</td>";
            $html .= "<td class='value'>" . $printedBoxHtml . "</td>";
            $html .= "<td class='value'>" . $vendorPrintedBoxHtml . "</td>";
            $html .= "<td class='value'>" . $shippedBoxHtml . "</td>";
            $html .= "<td class='value'>" . $totalBoxHtml . "</td>";
            $html .= "<td class='value'>" . $notShippedBoxHtml . "</td>";
            $html .= "<td class='value'>" . $vendorConfirmBoxCount . "</td>";

            if ($isVendor == "false") {
                $html .= "<td class='value'>" . $giftForm . "</td>";
                $html .= "<td class='value'>" . $upsFileForm . "</td>";
                $html .= "<td class='value'>" . $trackFileForm . "</td>";
                $html .= "<td class='value'>" . $allBoxFileForm . "</td>";
            }
            $html .= "</tr>";
        }
        $globalProcessedBoxHtml = $globalShippedBoxHtml = $globalTotalBoxHtml = $globalPrintedBoxHtml = '0';
        $globalVendorPrintBoxHtml = $globalNotShippedBoxHtml = '0';
        $globalTotalBoxes = '';
        if ($grandTotalProcessing != '0') {
            $globalProcessedBoxes = $vendorWiseData['grand_total_processed_boxes'];
            $globalProcessedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalProcessedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalProcessing . "'></form>";
        }

        if ($grandTotalPrinted != '0') {
            $globalPrintedBoxes = $vendorWiseData['grand_total_printed_count'];
            $globalPrintedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalPrintedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalPrinted . "'></form>";
        }

        if ($grandTotalShipped != '0') {
            $globalShippedBoxes = $vendorWiseData['grand_total_shipped_boxes'];
            $globalShippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalShippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalShipped . "'></form>";
        }
        if ($grandTotalNotShipped != '0') {
            $globalNotShippedBoxes = $vendorWiseData['grand_total_not_shipped_boxes'];
            $globalNotShippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalNotShippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalNotShipped . "'></form>";
        }
        if ($grandTotalVendorPrint != '0') {
            $globalVendorPrintBoxes = $vendorWiseData['grand_total_vendor_print_count_boxes'];
            $globalVendorPrintBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalVendorPrintBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalVendorPrint . "'></form>";
        }
        if ($grandTotalBoxes != '0') {
            $globalTotalBoxes = $vendorWiseData['grand_total_boxes_boxes'];
            $globalTotalBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $grandTotalBoxes . "'></form>";
        }
        /**
         * Download Gift Message Form
         */
        $giftForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxes . "' >
                <input type='hidden' name='type' value='gift' >
                <input type='submit' value='Download'></form>";

        /**
         * UPs File Form
         */
        $upsFileForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxes . "' >
                <input type='hidden' name='type' value='ups_file' >
                <input type='submit' value='Download'></form>";

        /**
         * Track Id
         */
        $trackFileForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxes . "' >
                <input type='hidden' name='type' value='track_file' >
                <input type='submit' value='Download'></form>";

        /**
         * All Boxes with Box Id and status
         */
        $allBoxFileForm = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $globalTotalBoxes . "' >
                <input type='hidden' name='type' value='all_boxes' >
                <input type='submit' value='All Boxes'></form>";

        $html .= "</tbody>";
        $html .= "<tfoot><tr>";
        $html .= "<td class='value'>Grand Total</td>";
        $html .= "<td class='value'>" . $globalProcessedBoxHtml . "</td>";
        $html .= "<td class='value'>" . $globalPrintedBoxHtml . "</td>";
        $html .= "<td class='value'>" . $globalVendorPrintBoxHtml . "</td>";
        $html .= "<td class='value'>" . $globalShippedBoxHtml . "</td>";
        $html .= "<td class='value'>" . $globalTotalBoxHtml . "</td>";
        $html .= "<td class='value'>" . $globalNotShippedBoxHtml . "</td>";
        $html .= "<td class='value'>" . $totalConfirmBoxes . "</td>";
        if ($isVendor == "false") {
            $html .= "<td class='value'>" . $giftForm . "</td>";
            $html .= "<td class='value'>" . $upsFileForm . "</td>";
            $html .= "<td class='value'>" . $trackFileForm . "</td>";
            $html .= "<td class='value'>" . $allBoxFileForm . "</td>";
        }
        $html .= "<td colspan='4'></td>";
        $html .= "</tr></tfoot>";
        $html .= "</table>";

        /**
         * SKU wise html
         */
        $skuHtml .= "</tbody>";
        $skuHtml .= "<tfoot><tr>";
        $skuHtml .= "<td class='value' colspan='4'>Grand Total</td>";
        $skuHtml .= "<td class='value'>" . $globalProcessedBoxHtml . "</td>";
        $skuHtml .= "<td class='value'>" . $globalPrintedBoxHtml . "</td>";
        $skuHtml .= "<td class='value'>" . $globalVendorPrintBoxHtml . "</td>";
        $skuHtml .= "<td class='value'>" . $globalShippedBoxHtml . "</td>";
        $skuHtml .= "<td class='value'>" . $globalTotalBoxHtml . "</td>";
        $skuHtml .= "<td class='value'>" . $globalNotShippedBoxHtml . "</td>";
        $skuHtml .= "</tr></tfoot>";
        $skuHtml .= "</table>";

        $response = [];
        $response['html'] = $html;
        $response['sku_html'] = $skuHtml;
        $response['all_box_ids'] = $vendorWiseData['grand_total_boxes_boxes'];
        return $response;
    }

    /**
     * Return HTML for vendor
     *
     * @param $vendorWiseData
     * @return string
     */
    public function getVendorSkuWiseHtml($vendorWiseData)
    {
        $formKey = $this->upsBlock->getFormKey();
        $TemplateUrl = $this->upsBlock->getTemplateDownloadUrl();
        $vendorUserId = $this->_vendorUserId;
        $disabled = '';
        if ($vendorUserId > 0) {
            $disabled = 'disabled';
        }
        $vendorSkuData = $vendorWiseData['vendor_sku_wise_data'];
        $vendorNameArray = $vendorWiseData['vendor_name_array'];
        $skuNameArray = $vendorWiseData['sku_name_array'];
        $asinNameArray = $vendorWiseData['asin_array'];

        $headers = $this->getSKuWiseHeaders();

        $skuHtml = "<table class='admin__table-secondary vendor-sku-item-table'><tbody>";
        $skuHtml .= "<tr>";
        foreach ($headers as $colName) {
            $skuHtml .= "<th>$colName</th>";
        }
        $skuHtml .= "</tr>";
        foreach ($vendorSkuData as $vendorId => $skuData) {
            $vendorName = $vendorNameArray[$vendorId];;
            foreach ($skuData as $sku => $vendorSKuData) {
                $productName = $skuNameArray[$sku];
                $asin = $asinNameArray[$sku];
                $processedBox = (isset($vendorSKuData['processing'])) ? $vendorSKuData['processing'] : '0';
                $printedBox = (isset($vendorSKuData['printed'])) ? $vendorSKuData['printed'] : '0';
                $shippedBox = (isset($vendorSKuData['shipped'])) ? $vendorSKuData['shipped'] : '0';
                $notShippedBox = (isset($vendorSKuData['not_shipped'])) ? $vendorSKuData['not_shipped'] : '0';
                $vendorPrintedBox = (isset($vendorSKuData['vendorPrint'])) ? $vendorSKuData['vendorPrint'] : '0';
                $totalBox = (isset($vendorSKuData['total'])) ? $vendorSKuData['total'] : '0';

                $processedBoxHtml = $shippedBoxHtml = $totalBoxHtml = $printedBoxHtml = '0';
                $vendorPrintedBoxHtml = $notShippedBoxHtml = '0';
                if ($processedBox != '0') {
                    $processedBoxes = $vendorSKuData['process_boxes'];
                    $processedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $processedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $processedBox . "'></form>";
                }

                if ($printedBox != '0') {
                    $printedBoxes = $vendorSKuData['printed_boxes'];
                    $printedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $printedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $printedBox . "'></form>";
                }

                if ($shippedBox != '0') {
                    $shippedBoxes = $vendorSKuData['shipped_boxes'];
                    $shippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $shippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $shippedBox . "'></form>";
                }
                if ($notShippedBox != '0') {
                    $notShippedBoxes = $vendorSKuData['not_shipped_boxes'];
                    $notShippedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $notShippedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $notShippedBox . "'></form>";
                }

                if ($vendorPrintedBox != '0') {
                    $vendorPrintedBoxes = $vendorSKuData['vendor_printed_boxes'];
                    $vendorPrintedBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $vendorPrintedBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $vendorPrintedBox . "'></form>";
                }

                if ($totalBox != '0') {
                    $totalBoxes = $vendorSKuData['total_boxes'];
                    $totalBoxHtml = "<form action='" . $TemplateUrl . "' target='_blank' method='post'>
                <input name='form_key' type='hidden' value='" . $formKey . "'>
                <input type='hidden' name='boxes' value='" . $totalBoxes . "' >
                <input type='hidden' name='type' value='box_details' >
                <input type='submit' $disabled value='" . $totalBox . "'></form>";
                }

                $skuHtml .= "<tr>";
                $skuHtml .= "<td class='value'>" . $vendorName . "</td>";
                $skuHtml .= "<td class='value'>" . $sku . "</td>";
                $skuHtml .= "<td class='value'>" . $productName . "</td>";
                $skuHtml .= "<td class='value'>" . $asin . "</td>";
                $skuHtml .= "<td class='value'>" . $processedBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $printedBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $vendorPrintedBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $shippedBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $totalBoxHtml . "</td>";
                $skuHtml .= "<td class='value'>" . $notShippedBoxHtml . "</td>";

                $skuHtml .= "</tr>";
            }

        }

        return $skuHtml;
    }

    /**
     * Vendor wise Data
     * @param $queryResult
     * @return array
     */
    public function vendorWiseQueryResult($queryResult)
    {
        $vendorWise = $vendorNameArray = $processedData = [];
        $globalTotal = $globalTotalShipped = $globalTotalProcessed = $globalTotalPrinted = 0;
        $globalTotalVendorPrinted = $globalTotalNotShipped = 0;
        $globalTotalBoxes = $globalTotalShippedBoxes = $globalTotalProcessedBoxes = $globalTotalPrintedBoxes = [];
        $globalTotalVendorPrintedBoxes = $globalTotalNotShippedBoxes = [];
        $vendorSkuWise = $skuNameArray = $vendorNameArray = [];
        /**
         * Get Latest track status for box
         */

        $allTrackIdArray = $skuArray = $asinArray = [];
        foreach ($queryResult as $oneBox) {
            $allTrackIdArray[] = $oneBox['track_id'];
            $skuArray[] = $oneBox['sku'];
        }
        /**
         * get product Asin
         */
        $skuArray = array_unique($skuArray);
        $collection = $this->catalogHelper->getProductCollectionBySku($skuArray);
        foreach ($collection as $product) {
            $asinArray[$product->getSku()] = $product->getAsin();
        }
        $allTrackIdArray = array_unique($allTrackIdArray);
        $trackStatusArray = $this->vendorHelper->getLatestTrackStatusByTrackId($allTrackIdArray);
        foreach ($queryResult as $oneBox) {
            $sku = $oneBox['sku'];
            $trackId = $oneBox['track_id'];
            if (isset($trackStatusArray[$trackId]['status'])) {
                $latestTrackStatus = $trackStatusArray[$trackId]['status'];
            } else {
                $latestTrackStatus = '';
            }
            $skuNameArray[$sku] = $oneBox['name'];
            $vendorWise[$oneBox['vendor']]['vendor_name'] = $oneBox['vendor_name'];
            $vendorWise[$oneBox['vendor']]['vendor_id'] = $oneBox['vendor'];
            $vendorNameArray[$oneBox['vendor']] = $oneBox['vendor_name'];
            if ($oneBox['status'] != 'invoiced') {
                $globalTotalBoxes[] = $oneBox['box_id'];
                $globalTotal++;

                if (isset($vendorWise[$oneBox['vendor']]['boxes'])) {
                    $vendorWise[$oneBox['vendor']]['boxes'] = $vendorWise[$oneBox['vendor']]['boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['boxes'] = $oneBox['box_id'];
                }
            }
            /**
             * Track status - not shipped boxes calculation
             */
            if ($latestTrackStatus == 'Shipping information sent to FedEx' ||
                $latestTrackStatus == 'Order Processed: Ready for UPS' ||
                $latestTrackStatus == 'Shipper created a label, UPS has not received the package yet.'
            ) {
                if ($oneBox['status'] == 'invoiced' || $oneBox['status'] == 'shipped') {
                    $globalTotalNotShipped++;
                    $globalTotalNotShippedBoxes[] = $oneBox['box_id'];
                    if (isset($vendorWise[$oneBox['vendor']]['not_shipped'])) {
                        $vendorWise[$oneBox['vendor']]['not_shipped'] += 1;
                        $vendorWise[$oneBox['vendor']]['not_shipped_boxes'] = $vendorWise[$oneBox['vendor']]['not_shipped_boxes']
                            . "," . $oneBox['box_id'];
                    } else {
                        $vendorWise[$oneBox['vendor']]['not_shipped'] = 1;
                        $vendorWise[$oneBox['vendor']]['not_shipped_boxes'] = $oneBox['box_id'];
                    }

                    if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['not_shipped'])) {
                        $vendorSkuWise[$oneBox['vendor']][$sku]['not_shipped'] += 1;
                        $vendorSkuWise[$oneBox['vendor']][$sku]['not_shipped_boxes'] =
                            $vendorSkuWise[$oneBox['vendor']][$sku]['not_shipped_boxes'] . "," . $oneBox['box_id'];
                    } else {
                        $vendorSkuWise[$oneBox['vendor']][$sku]['not_shipped'] = 1;
                        $vendorSkuWise[$oneBox['vendor']][$sku]['not_shipped_boxes'] = $oneBox['box_id'];
                    }
                }
            }
            if ($oneBox['status'] == 'pending' || $oneBox['status'] == 'processing') {
                $globalTotalProcessed++;
                $globalTotalProcessedBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['processing'])) {
                    $vendorWise[$oneBox['vendor']]['processing'] += 1;
                    $vendorWise[$oneBox['vendor']]['process_boxes'] = $vendorWise[$oneBox['vendor']]['process_boxes']
                        . "," . $oneBox['box_id'];

                } else {
                    $vendorWise[$oneBox['vendor']]['processing'] = 1;
                    $vendorWise[$oneBox['vendor']]['process_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['processing'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['processing'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['process_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['process_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['processing'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['process_boxes'] = $oneBox['box_id'];
                }
            }

            if ($oneBox['status'] == 'printed') {
                $globalTotalPrinted++;
                $globalTotalPrintedBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['printed'])) {
                    $vendorWise[$oneBox['vendor']]['printed'] += 1;
                    $vendorWise[$oneBox['vendor']]['printed_boxes'] = $vendorWise[$oneBox['vendor']]['printed_boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['printed'] = 1;
                    $vendorWise[$oneBox['vendor']]['printed_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['printed'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['printed'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['printed_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['printed_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['printed'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['printed_boxes'] = $oneBox['box_id'];
                }
            }
            if ($oneBox['status'] == 'vendorPrint') {
                $globalTotalVendorPrinted++;
                $globalTotalVendorPrintedBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['vendorPrint'])) {
                    $vendorWise[$oneBox['vendor']]['vendorPrint'] += 1;
                    $vendorWise[$oneBox['vendor']]['vendor_printed_boxes'] = $vendorWise[$oneBox['vendor']]['vendor_printed_boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['vendorPrint'] = 1;
                    $vendorWise[$oneBox['vendor']]['vendor_printed_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrint'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrint'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['vendor_printed_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['vendor_printed_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['vendorPrint'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['vendor_printed_boxes'] = $oneBox['box_id'];
                }
            }

            if ($oneBox['status'] == 'shipped') {
                $globalTotalShipped++;
                $globalTotalShippedBoxes[] = $oneBox['box_id'];
                if (isset($vendorWise[$oneBox['vendor']]['shipped'])) {
                    $vendorWise[$oneBox['vendor']]['shipped'] += 1;
                    $vendorWise[$oneBox['vendor']]['shipped_boxes'] = $vendorWise[$oneBox['vendor']]['shipped_boxes']
                        . "," . $oneBox['box_id'];

                } else {
                    $vendorWise[$oneBox['vendor']]['shipped'] = 1;
                    $vendorWise[$oneBox['vendor']]['shipped_boxes'] = $oneBox['box_id'];
                }
                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['shipped'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['shipped'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['shipped_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['shipped_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['shipped'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['shipped_boxes'] = $oneBox['box_id'];
                }
            }
            if ($oneBox['status'] != 'invoiced') {
                if (isset($vendorWise[$oneBox['vendor']]['total'])) {
                    $vendorWise[$oneBox['vendor']]['total'] += 1;
                    $vendorWise[$oneBox['vendor']]['total_boxes'] = $vendorWise[$oneBox['vendor']]['total_boxes']
                        . "," . $oneBox['box_id'];
                } else {
                    $vendorWise[$oneBox['vendor']]['total'] = 1;
                    $vendorWise[$oneBox['vendor']]['total_boxes'] = $oneBox['box_id'];
                }

                if (isset($vendorSkuWise[$oneBox['vendor']][$sku]['total'])) {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['total'] += 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['total_boxes'] =
                        $vendorSkuWise[$oneBox['vendor']][$sku]['total_boxes'] . "," . $oneBox['box_id'];
                } else {
                    $vendorSkuWise[$oneBox['vendor']][$sku]['total'] = 1;
                    $vendorSkuWise[$oneBox['vendor']][$sku]['total_boxes'] = $oneBox['box_id'];
                }
            }

        }
        $processedData['grand_total_processed_count'] = $globalTotalProcessed;
        $processedData['grand_total_printed_count'] = $globalTotalPrinted;
        $processedData['grand_total_shipped_count'] = $globalTotalShipped;
        $processedData['grand_total_not_shipped_count'] = $globalTotalNotShipped;
        $processedData['grand_total_vendor_print_count'] = $globalTotalVendorPrinted;
        $processedData['grand_total_boxes_count'] = $globalTotal;

        $processedData['grand_total_processed_boxes'] = implode(",", $globalTotalProcessedBoxes);
        $processedData['grand_total_printed_boxes'] = implode(",", $globalTotalPrintedBoxes);
        $processedData['grand_total_shipped_boxes'] = implode(",", $globalTotalShippedBoxes);
        $processedData['grand_total_not_shipped_boxes'] = implode(",", $globalTotalNotShippedBoxes);
        $processedData['grand_total_vendor_print_count_boxes'] = implode(",", $globalTotalVendorPrintedBoxes);
        $processedData['grand_total_boxes_boxes'] = implode(",", $globalTotalBoxes);
        usort($vendorWise, function ($item1, $item2) {
            return $item1['vendor_name'] <=> $item2['vendor_name'];
        });
        $processedData['vendor_data'] = $vendorWise;
        $processedData['sku_name_array'] = $skuNameArray;
        $processedData['asin_array'] = $asinArray;
        $processedData['vendor_sku_wise_data'] = $vendorSkuWise;
        $processedData['vendor_name_array'] = $vendorNameArray;
        return $processedData;
    }

    /**
     * Query Result
     * @return array
     */
    public function getQueryResult()
    {
        $inputParams = $this->_params;
        $shipFromDate = $inputParams['ship_date_from'];
        $shipToDate = $inputParams['ship_date_to'];
        $vendorId = $inputParams['vendor_id'];
        $storeId = $inputParams['store_id'];
        $vendorQuery = '';
        if ($vendorId != '0' && $vendorId != '') {
            $vendorQuery .= "and item.vendor='$vendorId'";
        }
        if ($storeId != '') {
            $vendorQuery .= "and item.store_id='$storeId'";
        }

        $query = "SELECT box.box_id,box.item_id,item.name,box.status,item.vendor,item.sku,item.product_id,vend.vendor_name,box.track_id,vend.vendor_id 
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  LEFT JOIN vendor_registrations as vend on item.vendor = vend.vendor_id
                  WHERE (item.pickup_date BETWEEN '$shipFromDate' and '$shipToDate' ) and box.status in ('pending','shipped','printed','vendorPrint','invoiced') and lower(item.shipping_method) like '%ups%' $vendorQuery
                  ORDER by item.name ASC";
        return $this->resourceConnection->getConnection()->fetchAll($query);
    }

    /**
     * Get Table name using direct query
     *
     * @param $tableName
     * @return string
     */
    public function getTableName($tableName)
    {
        /* Create Connection */
        $connection = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName($tableName);
        return $tableName;
    }

    /**
     * Column Headers
     * @return string[]
     */
    public function getHeaders()
    {
        return [
            'Vendor Name',
            'New Orders',
            'Ready to Print',
            'Order printed',
            'Origin Scan confirmed',
            'Grand Total',
            'No Origin Scan',
            'Vendor Confirmation',
            'Gift Message Template',
            'UPS File',
            'Track Id Template',
            'All'
        ];
    }

    /**
     * Return SKU wise result
     *
     * @return string[]
     */
    public function getSKuWiseHeaders()
    {
        return [
            'Vendor Name',
            'SKU',
            'Product Name',
            'ASIN',
            'New Orders',
            'Ready to Print',
            'Order printed',
            'Origin Scan confirmed',
            'Grand Total',
            'No Origin Scan'
        ];
    }

    /**
     * Hide these items for vendors
     * @return string[]
     */
    public function hideColumns()
    {
        return [
            'Gift Message Template',
            'UPS File',
            'Track Id Template',
            'All'
        ];
    }

    /**
     * Return Vendor Confirm Boxes Data
     * @return array
     */
    public function getVendorConfirmBoxesData()
    {
        $vendorWiseConfirmBoxes = [];
        $today = date('Y-m-d');
        $collection = $this->vendorHelper->getVendorConfirmBoxCollection();
        if ($collection->getSize() > 0) {
            foreach ($collection as $one) {
                $dateArray = explode(" ", $one->getData('created_at'));
                if ($dateArray[0] == $today) {
                    $vendorId = $one->getData('vendor_id');
                    $confirmBoxes = $one->getData('confirmed_boxes');
                    if (isset($vendorWiseConfirmBoxes[$vendorId])) {
                        $vendorWiseConfirmBoxes[$vendorId] += $confirmBoxes;
                    } else {
                        $vendorWiseConfirmBoxes[$vendorId] = $confirmBoxes;
                    }
                }
            }
        }
        return $vendorWiseConfirmBoxes;
    }
}
