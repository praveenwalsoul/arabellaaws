<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\Ups;

use Exception;
use Flordelcampo\Order\Model\Box;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\File\Csv;
use Flordelcampo\Order\Model\BoxFactory;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;

/**
 * Class Upload
 * Flordelcampo\Dashboards\Controller\Adminhtml\Ups
 */
class Upload extends Action
{
    private $_params = null;
    /**
     * @var null
     */
    private $_boxes;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var Csv
     */
    protected $csv;
    /**
     * @var BoxFactory
     */
    protected $boxFactory;
    /**
     * @var OrderHelper
     */
    private $orderHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param Csv $csv
     * @param BoxFactory $boxFactory
     * @param OrderHelper $orderHelper
     * @param VendorHelper $vendorHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        Csv $csv,
        BoxFactory $boxFactory,
        OrderHelper $orderHelper,
        VendorHelper $vendorHelper
    )
    {
        $this->csv = $csv;
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->boxFactory = $boxFactory;
        $this->orderHelper = $orderHelper;
        $this->vendorHelper = $vendorHelper;
        parent::__construct($context);
    }

    /**
     * @return Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $url = $this->_redirect->getRefererUrl();
        $this->_params = $this->getRequest()->getParams();
        try {
            $responseMsg = 'Updated Successfully';
            if (!empty($this->getRequest()->getParam('box_ids'))) {
                $boxIds = $this->getRequest()->getParam('box_ids');
                $responseMsg = $this->changeBoxStatusToInvoiced($boxIds);
            }
            if (!empty($this->getRequest()->getFiles('gift_file'))) {
                $file = $this->getRequest()->getFiles('gift_file');
                $responseMsg = $this->updateGiftMessage($file);
            }
            if (!empty($this->getRequest()->getFiles('track_file'))) {
                $file = $this->getRequest()->getFiles('track_file');
                $responseMsg = $this->updateTrackId($file);
            }
            if (!empty($this->getRequest()->getFiles('print_status'))) {
                $file = $this->getRequest()->getFiles('print_status');
                $responseMsg = $this->changeBoxStatusToPrinted($file);
            }

            if (!empty($this->getRequest()->getFiles('hard_good'))) {
                $file = $this->getRequest()->getFiles('hard_good');
                $responseMsg = $this->uploadHardGoodRows($file);
            }
            if (!empty($this->getRequest()->getParams('sync_order_status'))) {
                $responseMsg = $this->syncOrderStatus();
            }
            if (!empty($this->getRequest()->getFiles('update_amazon_price'))) {
                $file = $this->getRequest()->getFiles('update_amazon_price');
                $responseMsg = $this->updateAmazonOrderPrice($file);
            }
            $this->messageManager->addSuccessMessage(
                $responseMsg
            );
        } catch (Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }

    /**
     * Update Gift Messages
     *
     * @param $FILE
     * @return Phrase
     * @throws Exception
     */
    public function updateGiftMessage($FILE)
    {
        try {
            $rawData = $this->csv->getData($FILE['tmp_name']);
            $count = 1;
            foreach ($rawData as $oneRow) {
                if ($count == 1) {
                    $count++;
                    continue;
                } else {
                    $boxId = $oneRow[0];
                    $boxModel = $this->orderHelper->getBoxModelByBoxId($boxId);
                    if ($boxModel != null) {
                        $newGiftMessage = $oneRow[3];
                        $boxModel->setGiftMessage($newGiftMessage)
                            ->save();
                    }
                }
            }
            return __("Gift Message Updated Successfully");
        } catch (Exception $e) {
            return __($e->getMessage());
        }
    }

    /**
     * Update Track Ids
     *
     * @param $FILE
     * @return Phrase
     * @throws Exception
     */
    public function updateTrackId($FILE)
    {
        try {
            $rawData = $this->csv->getData($FILE['tmp_name']);
            $count = 1;
            foreach ($rawData as $oneRow) {
                try {
                    if ($count == 1) {
                        $count++;
                        continue;
                    } else {
                        $boxId = $oneRow[0];
                        $boxModel = $this->orderHelper->getBoxModelByBoxId($boxId);
                        if ($boxModel != null) {
                            $newTrackId = $oneRow[3];
                            $boxModel->setData('track_id', $newTrackId);
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::SHIPPED);
                            $boxModel->save();
                        }
                        $this->orderHelper->syncStatusByBoxId($boxId);
                    }
                } catch (Exception $e) {
                    continue;
                }
            }
            return __("Track Id Updated Successfully");
        } catch (Exception $e) {
            return __($e->getMessage());
        }
    }

    /**
     * Insert Rows to Hard Good
     * @param $FILE
     * @return Phrase
     */
    public function uploadHardGoodRows($FILE)
    {
        try {
            $rawData = $this->csv->getData($FILE['tmp_name']);
            /**
             *  'entity_id',
             * 'created_at',
             * 'updated_at',
             */
            $tableHeads = [
                'item_id',
                'order_id',
                'sku',
                'hg_product_sku',
                'hg_ordered_qty',
                'hg_offer_id'
            ];
            $count = 1;
            foreach ($rawData as $oneRow) {
                if ($count == 1) {
                    $count = 99;
                    continue;
                }
                try {
                    $hardGoodItem = $this->orderHelper->getHardGoodItemModel();
                    foreach ($oneRow as $key => $value) {
                        /*if ($key == 0) {
                            continue;
                        }*/
                        $hardGoodItem->setData($tableHeads[$key], $value);
                    }
                    $hardGoodItem->save();
                } catch (Exception $e) {
                    continue;
                }
            }
            /**
             * Delete rows which order ids are deleted
             */
            /*$collection = $this->orderHelper->getHardGoodItemCollection();
            if ($collection->getSize() > 0) {
                foreach ($collection as $one) {
                    try {
                        $orderId = $one->getData('order_id');
                        $entityId = $one->getId();
                        $order = $this->orderHelper->getOrderCollectionById($orderId);
                        if ($order->getSize() == 0) {
                            $hardGoodItem = $this->orderHelper->getHardGoodItemModelById($entityId);
                            $hardGoodItem->delete()->save();
                        }

                    } catch (Exception $e) {
                        continue;
                    }
                }
            }*/
            return __("Inserted and Deleted Invalid Rows");
        } catch (Exception $e) {
            return __($e->getMessage());
        }
    }

    /**
     * Update Box status to printed
     *
     * @param $FILE
     * @return Phrase
     */
    public function changeBoxStatusToPrinted($FILE)
    {
        try {
            $rawData = $this->csv->getData($FILE['tmp_name']);
            $count = 1;
            foreach ($rawData as $oneRow) {
                try {
                    if ($count == 1) {
                        $count++;
                        continue;
                    } else {
                        $boxId = $oneRow[0];
                        $boxModel = $this->orderHelper->getBoxModelByBoxId($boxId);
                        if ($boxModel != null) {
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::PRINTED);
                            $boxModel->save();
                        }
                        $this->orderHelper->syncStatusByBoxId($boxId);
                    }
                } catch (Exception $e) {
                    continue;
                }
            }
            return __("Box status changed to Printed Successfully");

        } catch (Exception $e) {
            return __($e->getMessage());
        }
    }

    /**
     * Change status to invoiced
     * @param $box_Ids
     * @return Phrase
     */
    public function changeBoxStatusToInvoiced($box_Ids)
    {
        try {
            if ($box_Ids != '' && $box_Ids != null) {
                $boxArray = explode(",", $box_Ids);
                foreach ($boxArray as $boxId) {
                    try {
                        $box = $this->orderHelper->getBoxModelByBoxId($boxId);
                        if ($box->getData('status') == \Flordelcampo\Order\Model\Box::SHIPPED) {
                            $box->setData('status', \Flordelcampo\Order\Model\Box::INVOICED);
                            $box->save();
                        }
                        $this->orderHelper->syncStatusByBoxId($boxId);
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
            return __("Box status changed to Invoiced Successfully");
        } catch (Exception $e) {
            return __($e->getMessage());
        }
    }

    /**
     * Sync Order Status
     */
    public function syncOrderStatus()
    {
        $params = $this->_params;
        if (!empty($params['from_date']) && !empty($params['to_date'])) {
            $fromDate = $params['from_date'] . ' 00:00:00';
            $toDate = $params['to_date'] . ' 00:00:00';
            $orderCollection = $this->orderHelper->getOrderCollection();
            $orderCollection->addFieldToSelect(['increment_id', 'status', 'state']);
            $orderCollection->addFieldToFilter('created_at', ['gteq' => $fromDate]);
            $orderCollection->addFieldToFilter('created_at', ['lteq' => $toDate]);
            if ($orderCollection->getSize() > 0) {
                foreach ($orderCollection as $order) {
                    try {
                        $this->orderHelper->syncOrderItemBoxStatusByOrderId($order->getData('entity_id'));
                    } catch (Exception $e) {
                        continue;
                    }
                }
            }
        }
        if (!empty($params['order_ids'])) {
            $orderCollection = $this->orderHelper->getOrderCollection();
            $orderCollection->addFieldToSelect(['increment_id', 'status', 'state']);
            $orderArray = explode(",", $params['order_ids']);
            $orderCollection->addFieldToFilter('increment_id', ['in' => $orderArray]);
            foreach ($orderArray as $orderIncrementId) {
                if ($orderCollection->getSize() > 0) {
                    foreach ($orderCollection as $order) {
                        try {
                            $this->orderHelper->syncOrderItemBoxStatusByOrderId($order->getData('entity_id'));
                        } catch (Exception $e) {
                            continue;
                        }
                    }
                }
            }
        }
        return __("Order - Item Synced Successfully");
    }

    /**
     * @param $FILE
     * @return Phrase
     */
    public function updateAmazonOrderPrice($FILE)
    {
        try {
            $rawData = $this->csv->getData($FILE['tmp_name']);
            $count = 1;
            foreach ($rawData as $oneRow) {
                try {
                    if ($count !== 1) {
                        $params['partner_po'] = $oneRow[0];
                        $price = $oneRow[1];
                        $orderItemCollection = $this->orderHelper->getOrderItemCollection($params);
                        if ($orderItemCollection->getSize() > 0) {
                            foreach ($orderItemCollection as $item) {
                                $rowTotal = $item->getQtyOrdered() * $price;
                                $item->setData('price', $price);
                                $item->setData('row_total', $rowTotal);
                                $item->save();
                            }
                        }
                    }
                    $count++;
                } catch (Exception $e) {
                    continue;
                }
            }
            return __("Amazon orders price has been Successfully");

        } catch (Exception $e) {
            return __($e->getMessage());
        }
    }
}
