<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\Ups;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordelcampo\Catalog\Helper\Data as CategoryHelper;
use Magento\Sales\Model\ResourceModel\Order\Collection;

/**
 * Class Templates
 * Flordelcampo\Dashboards\Controller\Adminhtml\Ups
 */
class Templates extends Action
{
    /**
     * @var null
     */
    private $_boxes;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var CategoryHelper
     */
    protected $categoryHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param OrderHelper $orderHelper
     * @param CategoryHelper $categoryHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        OrderHelper $orderHelper,
        CategoryHelper $categoryHelper
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->orderHelper = $orderHelper;
        $this->categoryHelper = $categoryHelper;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $boxes = $this->getRequest()->getParam('boxes');
        $this->_boxes = $boxes;
        $templateType = $this->getRequest()->getParam('type');
        switch ($templateType) {
            case 'box_details':
                $this->showBoxDetails();
                break;
            case 'gift':
                $this->downloadGiftTemplate();
                break;
            case 'ups_file':
                $this->downloadUpsFileTemplate();
                break;
            case 'track_file':
                $this->downloadTrackFileTemplate();
                break;
            case 'all_boxes':
                $this->downloadAllBoxesWithBoxStatusTemplate();
                break;
            default:
        }
        exit();
        //return $resultPage;
    }

    /**
     * Show Box Details
     */
    public function showBoxDetails()
    {
        $boxSql = $this->getSqlBoxesString();
        $query = "SELECT box.box_id,box.item_id,box.status,item.sku,item.name,vend.vendor_name,box.track_id,
        item.pickup_date,item.delivery_date,box.gift_message,item.shipping_method,ord.partner_order_id,ord.increment_id
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item on box.item_id = item.item_id
                  LEFT JOIN sales_order as ord on box.order_id = ord.entity_id
                  LEFT JOIN vendor_registrations as vend on item.vendor = vend.vendor_id
                  WHERE box.box_id in ($boxSql) 
                  ORDER by item.name ASC";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);

        /**
         * SKU wise
         */
        $skuWiseArray = [];
        $totalCount = 0;
        foreach ($queryResult as $key => $result) {
            if (isset($skuWiseArray[$result['sku']])) {
                $skuWiseArray[$result['sku']]['count'] += 1;
            } else {
                $skuWiseArray[$result['sku']] = [
                    'sku' => $result['sku'],
                    'name' => $result['name'],
                    'count' => 1];
            }
            $totalCount++;

        }
        /**
         * Box wise HTML
         */
        echo "<h2>SKU Summary</h2>";
        $html = "<table border='2'><tbody>";
        $html .= "<tr>";
        $html .= "<th>SKU</th>";
        $html .= "<th>Product Name</th>";
        $html .= "<th>Box Count</th>";
        $html .= "</tr>";
        foreach ($skuWiseArray as $sku => $result) {
            $html .= "<tr>";
            foreach ($result as $key => $value) {
                $html .= "<td class='value'>" . $value . "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody>";
        $html .= "<tfoot>";
        $html .= "<tr>";
        $html .= "<td colspan='2'>Total</td>";
        $html .= "<td>" . $totalCount . "</td>";
        $html .= "</tr>";
        $html .= "</tfoot>";

        $html .= "</table>";
        $html .= "</br>";
        echo $html;
        $headers = $this->getBoxHeaders();
        echo "<h2>Box Details</h2>";
        $html = "<table border='2'><tbody>";
        $html .= "<tr>";
        foreach ($headers as $colName) {
            $html .= "<th>$colName</th>";
        }
        $html .= "</tr>";
        foreach ($queryResult as $key => $result) {
            $html .= "<tr>";
            foreach ($result as $key => $value) {
                $html .= "<td class='value'>" . $value . "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody></table>";
        echo $html;
        exit();
    }

    /**
     * Download Gift template
     */
    public function downloadGiftTemplate()
    {
        $data = [];
        $fileName = 'gift_message_upload_template.csv';
        $data[] = ['box_id', 'sku', 'name', 'gift_message', 'shipping_name', 'order number', 'partner_order_id'];

        $boxSql = $this->getSqlBoxesString();

        /**
         * Get distinct order ids
         */
        $query = "SELECT DISTINCT box.order_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
        }
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $shippingAddress = $order->getShippingAddress();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
        }

        /**
         * Box data
         */
        $query = "SELECT box.box_id,item.sku,item.name,box.gift_message,box.order_id,ord.increment_id,ord.partner_order_id
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item on box.item_id = item.item_id
                  LEFT JOIN sales_order as ord on box.order_id = ord.entity_id
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $key => $result) {
            $queryData = [];
            foreach ($result as $index => $value) {
                if ($index == 'order_id') {
                    $value = $boxOrderData[$value]['ship_name'];
                }
                $queryData[] = $value;
            }
            $data[] = $queryData;
        }

        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $fp = fopen('php://output', 'w');
        foreach ($data as $row) {
            fputcsv($fp, $row);
        }

        fclose($fp);
        exit();
    }

    /**
     * Download UPS file Template
     * @throws NoSuchEntityException
     */
    public function downloadUpsFileTemplate()
    {
        $csvFileData = [];
        $upsHeaders = $this->getUpsFileHeaders();
        $csvFileData[] = $upsHeaders;
        $boxSql = $this->getSqlBoxesString();

        $filterQuery = "and box.status in ('pending','processing')";
        $query = "SELECT box.order_id,box.item_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql) $filterQuery";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = $itemArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
            $itemArray[] = $row['item_id'];
        }
        $orderArray = array_unique($orderArray);
        $itemArray = array_unique($itemArray);

        /**
         * Logic for most sold products item ids in order wise
         */
        $itemIdString = "'" . implode("', '", $itemArray) . "'";
        $query = "SELECT item.sku,sum(item.qty_ordered) as qty_sold
                  FROM `sales_order_item` as item
                  WHERE item.item_id in ($itemIdString)
                  GROUP by item.sku
                  ORDER by qty_sold DESC";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $skuArray = [];
        foreach ($queryResult as $qryResult) {
            $skuArray[] = $qryResult['sku'];
        }
        $skuString = "'" . implode("', '", $skuArray) . "'";

        /**
         * Get order details
         */
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $partnerId = $order->getPartnerOrderId();
            $shippingAddress = $order->getShippingAddress();
            $billingAddress = $order->getBillingAddress();
            $billingName = $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $shippingCity = $shippingAddress->getCity();
            $shippingStreet = $shippingAddress->getStreet();
            $shippingPostCode = $shippingAddress->getPostcode();
            $shippingTelephone = $shippingAddress->getTelephone();
            $shippingState = $shippingAddress->getRegionCode();

            $shippingAddressLine1 = (isset($shippingStreet[0])) ? $shippingStreet[0] : '';
            $shippingAddressLine2 = (isset($shippingStreet[1])) ? $shippingStreet[1] : '';
            $shippingAddressLine3 = (isset($shippingStreet[2])) ? $shippingStreet[2] : '';
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
            $boxOrderData[$orderId]['ship_line_1'] = $shippingAddressLine1;
            $boxOrderData[$orderId]['ship_line_2'] = $shippingAddressLine2;
            $boxOrderData[$orderId]['ship_line_3'] = $shippingAddressLine3;
            $boxOrderData[$orderId]['city'] = $shippingCity;
            $boxOrderData[$orderId]['state'] = $shippingState;

            $postCodeArray = explode("-", $shippingPostCode);
            if (count($postCodeArray) > 1) {
                $postCode = $shippingPostCode;
            } else {
                $postCode = $postCodeArray[0];
                if (strlen($postCode) < 5) {
                    $padLength = 5 - strlen($postCode);
                    $postCode = str_pad($postCode, 5, "0", STR_PAD_LEFT);
                    $postCode = "=" . $postCode;
                }
            }
            $boxOrderData[$orderId]['post_code'] = (string)$postCode;
            $boxOrderData[$orderId]['country'] = 'US';
            $boxOrderData[$orderId]['phone'] = $shippingTelephone;
            $boxOrderData[$orderId]['partner_order_id'] = $partnerId;
            $boxOrderData[$orderId]['billing_name'] = $billingName;
        }

        /**
         * Actual logic
         * Box details
         */
        $query = "SELECT box.box_id,box.order_id,box.item_id,box.status,box.gift_message,item.vendor,item.sku,item.product_id,item.name 
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  WHERE box.box_id in ($boxSql) $filterQuery
                  ORDER by FIELD(item.sku,$skuString) ";

        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $oneBox) {
            $upsBoxWiseData = [];
            $sku = $oneBox['sku'];
            $orderId = $oneBox['order_id'];
            $product = $this->categoryHelper->getProductBySku($sku);
            $productId = $product->getId();
            $productName = $product->getName();
            $boxLength = $product->getBoxLength();
            $boxWidth = $product->getBoxWidth();
            $boxHeight = $product->getBoxHeight();
            $boxWeight = $product->getBoxWeight();
            $poPrice = $product->getPoPrice();
            $group = $product->getGroup();
            $attr = $product->getResource()->getAttribute('group');
            $groupName = '';
            if ($attr->usesSource()) {
                $groupName = $attr->getSource()->getOptionText($group);
            }
            $giftMessage = (trim($oneBox['gift_message']) == '') ?
                $boxOrderData[$orderId]['billing_name'] : $oneBox['gift_message'];

            $giftMessage = str_replace("?", "", $giftMessage);
            $upsBoxWiseData = [
                $boxOrderData[$orderId]['ship_name'],
                $boxOrderData[$orderId]['ship_name'],
                $boxOrderData[$orderId]['ship_line_1'],
                $boxOrderData[$orderId]['ship_line_2'],
                $boxOrderData[$orderId]['ship_line_3'],
                $boxOrderData[$orderId]['city'],
                $boxOrderData[$orderId]['state'],
                $boxOrderData[$orderId]['post_code'],
                $boxOrderData[$orderId]['country'],
                $boxOrderData[$orderId]['phone'],
                $giftMessage,
                $boxOrderData[$orderId]['partner_order_id'],
                $productName,
                '1',
                $productName,
                'Fresh Flowers',
                $boxLength,
                $boxWidth,
                $boxHeight,
                $boxWeight,
                $poPrice,
                'ES',
                'CP',
                $groupName,
                'USD',
                'CO',
                'Box',
                'Isha Flowers',
                'Zach Hemple',
                '4862 Cadiz Circle',
                'Palm Beach Gardens',
                'FL',
                'US',
                '33418',
                '8562819187',
                '85086E'
            ];
            $csvFileData[] = $upsBoxWiseData;
        }

        /**
         * Download file
         */

        $fileName = "ups_file_template.csv";
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        $fp = fopen('php://output', 'w');
        foreach ($csvFileData as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
        exit();

    }

    /**
     * Download Track Id upload Template
     */
    public function downloadTrackFileTemplate()
    {
        $data = [];
        $fileName = 'track_id_upload_template.csv';
        $data[] = ['box_id', 'sku', 'name', 'track_id', 'shipping_name', 'order number', 'partner_order_id'];

        $boxSql = $this->getSqlBoxesString();

        /**
         * Get distinct order ids
         */
        $query = "SELECT DISTINCT box.order_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
        }
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $shippingAddress = $order->getShippingAddress();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
        }

        /**
         * Box data
         */
        $query = "SELECT box.box_id,item.sku,item.name,box.track_id,box.order_id,ord.increment_id,ord.partner_order_id
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item on box.item_id = item.item_id
                  LEFT JOIN sales_order as ord on box.order_id = ord.entity_id
                  WHERE box.box_id in ($boxSql)";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $key => $result) {
            $queryData = [];
            foreach ($result as $index => $value) {
                if ($index == 'order_id') {
                    $value = $boxOrderData[$value]['ship_name'];
                }
                $queryData[] = $value;
            }
            $data[] = $queryData;
        }

        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $fp = fopen('php://output', 'w');
        foreach ($data as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
        exit();
    }

    /**
     * Return Headers
     *
     * @return array
     */
    public function getBoxHeaders()
    {
        return [
            'Box Id',
            'Item Id',
            'Box Status',
            'SKU',
            'Product Name',
            'Vendor Name',
            'Track Id',
            'Pickup Date',
            'Delivery Date',
            'Gift Message',
            'Shipping Method',
            'Partner Order Id',
            'Increment Number'
        ];
    }

    /**
     * Track Id Template Headers
     * @return string[]
     */
    public function getTrackIdTemplateHeaders()
    {
        return [
            'box_id',
            'sku',
            'name',
            'track_id',
            'shipping_name',
            'order number',
            'partner_order_id'
        ];
    }

    /**
     * Return Ups File Array
     *
     * @return array
     */
    public function getUpsFileHeaders()
    {
        return [
            'Companyshipto',
            'contactshipto',
            'address1shipto',
            'address2shipto',
            'address3shipto',
            'cityshipto',
            'stateshipto',
            'zipshipto',
            'Countryshipto',
            'phoneshipto',
            'Message',
            'Reference1',
            'Reference2',
            'Quantity',
            'Item',
            'ProdDesc',
            'Length',
            'width',
            'height',
            'WeightKg',
            'DclValue',
            'Service',
            'PkgType',
            'GenDesc',
            'Currency',
            'Origin',
            'UOM',
            'TPComp',
            'TPAttn',
            'TPAdd1',
            'TPCity',
            'TPState',
            'TPCtry',
            'TPZip',
            'TPPhone',
            'TPAcct'
        ];
    }

    /**
     * Get Sql Query
     * @return string
     */
    public function getSqlBoxesString()
    {
        $boxArray = explode(",", $this->_boxes);
        return "'" . implode("', '", $boxArray) . "'";
    }

    /**
     * Return Order object by ids
     * @param $orderArray
     * @return Collection
     */
    public function getOrderCollectionByIds($orderArray)
    {
        return $this->orderHelper->getOrderCollectionByIds($orderArray);
    }

    /**
     * Download UPS file Template with Box Id and Box status
     * @throws NoSuchEntityException
     */
    public function downloadAllBoxesWithBoxStatusTemplate()
    {
        $csvFileData = [];
        $upsHeaders = $this->getUpsFileHeaders();
        $upsHeaders[] = 'Box Id';
        $upsHeaders[] = 'Box Status';
        $csvFileData[] = $upsHeaders;
        $boxSql = $this->getSqlBoxesString();

        $filterQuery = "and box.status in ('pending','processing','printed')";
        $query = "SELECT box.order_id,box.item_id
                  FROM `order_item_box` as box
                  WHERE box.box_id in ($boxSql) $filterQuery";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $orderArray = $itemArray = [];
        foreach ($queryResult as $row) {
            $orderArray[] = $row['order_id'];
            $itemArray[] = $row['item_id'];
        }
        $orderArray = array_unique($orderArray);
        $itemArray = array_unique($itemArray);

        /**
         * Logic for most sold products item ids in order wise
         */
        $itemIdString = "'" . implode("', '", $itemArray) . "'";
        $query = "SELECT item.sku,sum(item.qty_ordered) as qty_sold
                  FROM `sales_order_item` as item
                  WHERE item.item_id in ($itemIdString)
                  GROUP by item.sku
                  ORDER by qty_sold DESC";
        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        $skuArray = [];
        foreach ($queryResult as $qryResult) {
            $skuArray[] = $qryResult['sku'];
        }
        $skuString = "'" . implode("', '", $skuArray) . "'";

        /**
         * Get order details
         */
        $orderCollection = $this->getOrderCollectionByIds($orderArray);
        $boxOrderData = [];
        foreach ($orderCollection as $order) {
            $orderId = $order->getId();
            $partnerId = $order->getPartnerOrderId();
            $shippingAddress = $order->getShippingAddress();
            $billingAddress = $order->getBillingAddress();
            $billingName = $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName();
            $shippingName = $shippingAddress->getFirstName() . ' ' . $shippingAddress->getLastName();
            $shippingCity = $shippingAddress->getCity();
            $shippingStreet = $shippingAddress->getStreet();
            $shippingPostCode = $shippingAddress->getPostcode();
            $shippingTelephone = $shippingAddress->getTelephone();
            $shippingState = $shippingAddress->getRegionCode();

            $shippingAddressLine1 = (isset($shippingStreet[0])) ? $shippingStreet[0] : '';
            $shippingAddressLine2 = (isset($shippingStreet[1])) ? $shippingStreet[1] : '';
            $shippingAddressLine3 = (isset($shippingStreet[2])) ? $shippingStreet[2] : '';
            $boxOrderData[$orderId]['ship_name'] = $shippingName;
            $boxOrderData[$orderId]['ship_line_1'] = $shippingAddressLine1;
            $boxOrderData[$orderId]['ship_line_2'] = $shippingAddressLine2;
            $boxOrderData[$orderId]['ship_line_3'] = $shippingAddressLine3;
            $boxOrderData[$orderId]['city'] = $shippingCity;
            $boxOrderData[$orderId]['state'] = $shippingState;

            $postCodeArray = explode("-", $shippingPostCode);
            if (count($postCodeArray) > 1) {
                $postCode = $shippingPostCode;
            } else {
                $postCode = $postCodeArray[0];
                if (strlen($postCode) < 5) {
                    $padLength = 5 - strlen($postCode);
                    $postCode = str_pad($postCode, 5, "0", STR_PAD_LEFT);
                    $postCode = "=" . $postCode;
                }
            }
            $boxOrderData[$orderId]['post_code'] = (string)$postCode;
            $boxOrderData[$orderId]['country'] = 'US';
            $boxOrderData[$orderId]['phone'] = $shippingTelephone;
            $boxOrderData[$orderId]['partner_order_id'] = $partnerId;
            $boxOrderData[$orderId]['billing_name'] = $billingName;
        }

        /**
         * Actual logic
         * Box details
         */
        $query = "SELECT box.box_id,box.order_id,box.item_id,box.status,box.gift_message,item.vendor,item.sku,item.product_id,item.name 
                  FROM `order_item_box` as box
                  LEFT JOIN sales_order_item as item 
                   on box.item_id = item.item_id
                  WHERE box.box_id in ($boxSql) $filterQuery
                  ORDER by FIELD(item.sku,$skuString) ";

        $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
        foreach ($queryResult as $oneBox) {
            $upsBoxWiseData = [];
            $sku = $oneBox['sku'];
            $orderId = $oneBox['order_id'];
            $product = $this->categoryHelper->getProductBySku($sku);
            $productId = $product->getId();
            $productName = $product->getName();
            $boxLength = $product->getBoxLength();
            $boxWidth = $product->getBoxWidth();
            $boxHeight = $product->getBoxHeight();
            $boxWeight = $product->getBoxWeight();
            $poPrice = $product->getPoPrice();
            $group = $product->getGroup();
            $attr = $product->getResource()->getAttribute('group');
            $groupName = '';
            if ($attr->usesSource()) {
                $groupName = $attr->getSource()->getOptionText($group);
            }
            $giftMessage = (trim($oneBox['gift_message']) == '') ?
                $boxOrderData[$orderId]['billing_name'] : $oneBox['gift_message'];

            $giftMessage = str_replace("?", "", $giftMessage);
            $upsBoxWiseData = [
                $boxOrderData[$orderId]['ship_name'],
                $boxOrderData[$orderId]['ship_name'],
                $boxOrderData[$orderId]['ship_line_1'],
                $boxOrderData[$orderId]['ship_line_2'],
                $boxOrderData[$orderId]['ship_line_3'],
                $boxOrderData[$orderId]['city'],
                $boxOrderData[$orderId]['state'],
                $boxOrderData[$orderId]['post_code'],
                $boxOrderData[$orderId]['country'],
                $boxOrderData[$orderId]['phone'],
                $giftMessage,
                $boxOrderData[$orderId]['partner_order_id'],
                $productName,
                '1',
                $productName,
                'Fresh Flowers',
                $boxLength,
                $boxWidth,
                $boxHeight,
                $boxWeight,
                $poPrice,
                'ES',
                'CP',
                $groupName,
                'USD',
                'CO',
                'Box',
                'Isha Flowers',
                'Zach Hemple',
                '4862 Cadiz Circle',
                'Palm Beach Gardens',
                'FL',
                'US',
                '33418',
                '8562819187',
                '85086E',
                $oneBox['box_id'],
                $oneBox['status']
            ];
            $csvFileData[] = $upsBoxWiseData;
        }

        /**
         * Download file
         */
        $fileName = "all_boxes_template.csv";
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        $fp = fopen('php://output', 'w');
        foreach ($csvFileData as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
        exit();
    }
}

