<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\UpsAddressValidate;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Flordelcampo\Dashboards\Helper\Data as DashboardHelper;
use Exception;
use Magento\Sales\Model\ResourceModel\Order;

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\UpsAddressValidate
 */
class Edit extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var DashboardHelper
     */
    protected $dashboardHelper;
    /**
     * @var Order
     */
    protected $orderResourceModel;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param orderHelper $orderHelper
     * @param DashboardHelper $dashboardHelper
     * @param Order $orderResourceModel
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        orderHelper $orderHelper,
        DashboardHelper $dashboardHelper,
        Order $orderResourceModel
    )
    {
        parent::__construct($context);
        $this->orderResourceModel = $orderResourceModel;
        $this->jsonFactory = $jsonFactory;
        $this->orderHelper = $orderHelper;
        $this->dashboardHelper = $dashboardHelper;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     *
     * Validate
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $orderId = $this->getRequest()->getParam('order_id');
            $orderCollection = $this->orderHelper->getOrderCollectionById($orderId);
            $addressData = [];
            foreach ($orderCollection as $order) {
                $shippingAddress = $order->getShippingAddress();
                $addressData['first_name'] = $shippingAddress->getFirstName();
                $addressData['last_name'] = $shippingAddress->getLastName();
                $addressData['company'] = $shippingAddress->getCompany();
                $addressData['city'] = $shippingAddress->getCity();
                $streetArray = $shippingAddress->getStreet();
                $count = 1;
                foreach ($streetArray as $index => $street) {
                    $key = "street_" . $count;
                    $addressData['street'][$key] = $street;
                    $count++;
                }
                $addressData['post_code'] = $shippingAddress->getPostcode();
                $addressData['state'] = $shippingAddress->getRegion();
                $addressData['phone'] = $shippingAddress->getTelephone();
                $addressData['order_id'] = $orderId;
            }
            $data = [
                'status' => true,
                'address' => $addressData
            ];
        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

}
