<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\UpsAddressValidate;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Flordelcampo\Dashboards\Helper\Data as DashboardHelper;
use Exception;
use Magento\Sales\Model\Order\AddressRepository;
use Magento\Sales\Model\ResourceModel\Order;

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\UpsAddressValidate
 */
class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var DashboardHelper
     */
    protected $dashboardHelper;
    /**
     * @var Order
     */
    protected $orderResourceModel;
    /**
     * @var AddressRepository
     */
    protected $repositoryAddress;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param orderHelper $orderHelper
     * @param DashboardHelper $dashboardHelper
     * @param Order $orderResourceModel
     * @param AddressRepository $repositoryAddress
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        orderHelper $orderHelper,
        DashboardHelper $dashboardHelper,
        Order $orderResourceModel,
        AddressRepository $repositoryAddress
    )
    {
        parent::__construct($context);
        $this->orderResourceModel = $orderResourceModel;
        $this->jsonFactory = $jsonFactory;
        $this->orderHelper = $orderHelper;
        $this->dashboardHelper = $dashboardHelper;
        $this->resultPageFactory = $resultPageFactory;
        $this->repositoryAddress = $repositoryAddress;
    }

    /**
     *
     * Validate
     *
     * @return void
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $addressData = $this->getRequest()->getParam('address_data');
            $orderId = $addressData['order_id'];
            $orderCollection = $this->orderHelper->getOrderCollectionById($orderId);
            $shippingAddressId = 0;
            foreach ($orderCollection as $order) {
                $shippingAddress = $order->getShippingAddress();
                $shippingAddressId = $shippingAddress->getId();
            }
            $shipAddress = $this->repositoryAddress->get($shippingAddressId);
            if ($shipAddress->getId()) {
                $shipAddress->setFirstname($addressData['first_name']);
                $shipAddress->setLastname($addressData['last_name']);
                $shipAddress->setCompany($addressData['company']);
                $streetArray[] = $addressData['street_1'];
                $streetArray[] = $addressData['street_2'];
                $shipAddress->setCountryId('US');
                $shipAddress->setPostcode($addressData['post_code']);
                $shipAddress->setStreet($streetArray);
                $shipAddress->setCity($addressData['city']);
                $shipAddress->setRegion($addressData['state']);
                $shipAddress->setTelephone($addressData['phone']);
                $this->repositoryAddress->save($shipAddress);
            }
            $data = [
                'status' => true,
                'message' => 'Address Added successfully'
            ];
        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    public function addressHtml($addressData)
    {
        /**
         * Array
         * (
         * [first_name] => Kathleen
         * [last_name] => Frohock
         * [company] =>
         * [city] => Essex
         * [street] => Array
         * (
         * [0] => 6 Chebacco Terrace   Apt 22
         * )
         *
         * [post_code] => 9532
         * [state] => Massachusetts
         * [phone] => 210-728-4548
         * )
         */
        echo '<pre>';
    }
}
