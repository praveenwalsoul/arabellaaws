<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\UpsAddressValidate;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Flordelcampo\Dashboards\Helper\Data as DashboardHelper;
use Exception;
use Magento\Sales\Model\ResourceModel\Order;

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\UpsAddressValidate
 */
class ValidateCron extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var DashboardHelper
     */
    protected $dashboardHelper;
    /**
     * @var Order
     */
    protected $orderResourceModel;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param orderHelper $orderHelper
     * @param DashboardHelper $dashboardHelper
     * @param Order $orderResourceModel
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        orderHelper $orderHelper,
        DashboardHelper $dashboardHelper,
        Order $orderResourceModel
    )
    {
        parent::__construct($context);
        $this->orderResourceModel = $orderResourceModel;
        $this->jsonFactory = $jsonFactory;
        $this->orderHelper = $orderHelper;
        $this->dashboardHelper = $dashboardHelper;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     *
     * Validate
     *
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $orderCollection = $this->orderHelper->getProcessingOrderCollection();
            $addressData = [];
            $validatedOrderIds = [];
            foreach ($orderCollection as $order) {
                $orderId = $order->getId();
                $validatedOrderIds[] = $order->getIncrementId();
                $shippingAddress = $order->getShippingAddress();
                $addressData['city'] = $shippingAddress->getCity();
                $addressData['street'] = $shippingAddress->getStreet();
                $postCodeArray = explode("-", $shippingAddress->getPostcode());
                $addressData['full_post_code'] = $shippingAddress->getPostcode();
                if (count($postCodeArray) > 1) {
                    $addressData['post_code'] = $postCodeArray[0];
                    $addressData['ext_code'] = $postCodeArray[1];
                } else {
                    $addressData['post_code'] = $postCodeArray[0];
                    $addressData['ext_code'] = '';
                }
                $addressData['region_id'] = $shippingAddress->getRegionCode();
                $addressData['country_id'] = $shippingAddress->getCountryId();

                $response = $this->validate($addressData);
                $upsColData = [
                    'order_id' => $orderId,
                    'status' => $response['status'],
                    'suggested_address' => $response['address']
                ];
                $upsAddressModel = $this->dashboardHelper->getUpsAddressModel();

                foreach ($upsColData as $field => $value) {
                    $upsAddressModel->setData($field, $value);
                }
                $upsAddressModel->save();
                $order = $this->orderHelper->getOrderModelById($orderId);
                $order->setUpsAddressStatus('true');
                $this->orderResourceModel->save($order);
            }
            /*$validatedIds = implode("</br>", $validatedOrderIds);*/
            $data = [
                'status' => true,
                'message' => 'Validated',
                'order_ids' => $validatedOrderIds
            ];
        } catch (Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        echo '<pre>';
        print_r($data);
        die();
    }

    public function validate($addressData)
    {
        $userId = "newKaBloom";
        $license = "8CCFD3D445A6F805";
        $password = "Rosegarden123";

        $endPointUrl = 'https://onlinetools.ups.com/ups.app/xml/XAV';
        $region = $addressData['city'] . ' ' . $addressData['region_id'] . ' ' . $addressData['full_post_code'];
        $data = "<?xml version=\"1.0\" ?>
        <AccessRequest xml:lang='en-US'>
        <AccessLicenseNumber>" . $license . "</AccessLicenseNumber>
        <UserId>" . $userId . "</UserId>
        <Password>" . $password . "</Password>
        </AccessRequest>
        <?xml version=\"1.0\"?>
        <AddressValidationRequest xml:lang=\"en-US\">
            <Request>
            <TransactionReference>
            <CustomerContext>Street Level Address verification</CustomerContext>      
            </TransactionReference>
            <RequestAction>XAV</RequestAction>
            <RequestOption>3</RequestOption>
            </Request>
            <AddressKeyFormat>
            <AddressLine>" . $addressData['street'][0] . "</AddressLine> 
            <Region>" . $region . "</Region> 
            <PoliticalDivision2>" . $addressData['city'] . "</PoliticalDivision2>
            <PoliticalDivision1>" . $addressData['region_id'] . "</PoliticalDivision1>
            <PostcodePrimaryLow>" . $addressData['post_code'] . "</PostcodePrimaryLow>
            <PostcodeExtendedLow>" . $addressData['ext_code'] . "</PostcodeExtendedLow>
            <CountryCode>US</CountryCode>
            </AddressKeyFormat>
        </AddressValidationRequest>";
        $ch = curl_init($endPointUrl);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);//die;
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);
        $xml = simplexml_load_string($body);
        $json = json_encode($xml);
        $address = json_decode($json, TRUE);
        $validResponse = $address['Response'];
        $responseArray = [];
        if ($validResponse['ResponseStatusCode'] == 1) {
            if (isset($address['AddressKeyFormat'])) {
                $jsonAddress = json_encode($address['AddressKeyFormat']);
                $responseArray = ['status' => 'true', 'address' => $jsonAddress];
            } else {
                $responseArray = ['status' => 'true', 'address' => null];
            }
        } else {
            $responseArray = ['status' => 'false', 'address' => null];
        }
        return $responseArray;
    }
}
