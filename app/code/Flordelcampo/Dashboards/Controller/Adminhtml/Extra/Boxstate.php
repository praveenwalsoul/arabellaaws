<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\Extra;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\Extra
 */
class Boxstate extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    protected $_fileFactory;
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\App\RequestInterface $request,
        ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->_fileFactory = $fileFactory;
        $this->request = $request;
    }

    /**
     * Ups home page
     *
     * @return Page|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        try {

            $boxids = $this->request->getPostValue();
            $changeStatus = $boxids['change_status'];

            if (!empty($boxids)) {
                $boxIncrementIdArray = explode(",", $boxids['box_ids']);

                foreach($boxIncrementIdArray as $box){

                $qry = "UPDATE `order_item_box` SET `status`= '$changeStatus' WHERE `box_id`= '$box'";
                $this->resourceConnection->getConnection()->query($qry);

                }


                $data = [
                    'status' => true,
                    'message' => 'Box Status Change  Successfully'
                ];
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Please enter valid order Ids'
                ];
            }

        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        echo '<pre>';
        print_r($data);
        exit();
    }
}
