<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\Extra;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\Extra
 */
class Orderstate extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    
    protected $order;
    protected $resourceConnection;

    protected $directory;

    protected $_fileFactory;
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        ResourceConnection $resourceConnection,
        \Magento\Framework\App\RequestInterface $request,
        Order $order
        
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->order = $order;
        $this->resourceConnection = $resourceConnection;
        $this->request = $request;

        
    }

    /**
     * Ups home page
     *
     * @return Page|ResponseInterface|ResultInterface
     */
    public function execute()
    {
       
        try {

            $orderId = $this->request->getPostValue();
            $changeStatus = $orderId['change_status'];
            if (!empty($orderId)) {
                $orderIncrementIdArray = explode(",", $orderId['increment_ids']);
                foreach($orderIncrementIdArray as $Id) {
                    $orders = $this->order->loadByIncrementId($Id);
                    $orders->setData($changeStatus)->setStatus($changeStatus);
                    $orders->save();
                    }
                    
                $data = [
                    'status' => true,
                    'message' => 'Orders State Change Successfully'
                ];
            } 
        else {
                $data = [
                    'status' => false,
                    'message' => 'Please enter valid order Ids'
                ];
            }

        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        echo '<pre>';
        print_r($data);
        exit();
    }
}
