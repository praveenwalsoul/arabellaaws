<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\Extra;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Flordelcampo\VendorInventory\Model\RevertInventoryForVendor;
use Magento\Sales\Api\OrderRepositoryInterface;
use Anktech\DeleteOrder\Helper\Data as AnkTechHelper;

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\Extra
 */
class DeleteOrder extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var RevertInventoryForVendor
     */
    protected $revertInventoryForVendor;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param orderHelper $orderHelper
     * @param RevertInventoryForVendor $revertInventoryForVendor
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        orderHelper $orderHelper,
        RevertInventoryForVendor $revertInventoryForVendor,
        OrderRepositoryInterface $orderRepository
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->orderHelper = $orderHelper;
        $this->orderRepository = $orderRepository;
        $this->revertInventoryForVendor = $revertInventoryForVendor;
    }

    /**
     * Ups home page
     *
     * @return Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $url = $this->_redirect->getRefererUrl();
        $responseMsg = 'Orders has been deleted Successfully';
        try {
            $orderIdString = $this->_request->getParam('order_ids');
            if (!empty($orderIdString)) {
                $orderIncrementIdArray = explode(",", $orderIdString);
                foreach ($orderIncrementIdArray as $incrementId) {
                    try {
                        $order = $this->orderHelper->getOrderRepositoryByIncrementId($incrementId);
                        foreach ($order->getAllItems() as $item) {
                            $this->revertInventoryForVendor
                                ->revertInventoryForVendorByItemID($item->getId());
                        }
                        $this->orderRepository->delete($order);
                        $helper = $this->_objectManager->create(AnkTechHelper::class);
                        $helper->deleteOrderEntity($order->getId());
                    } catch (\Exception $e) {
                        continue;
                    }
                }
                $this->messageManager->addSuccessMessage(
                    $responseMsg
                );
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }

        $resultRedirect->setUrl($url);
        return $resultRedirect;
        /*return $resultRedirect->setPath('dashboards/extra/index');*/
    }
}
