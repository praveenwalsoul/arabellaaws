<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\Extra;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\Extra
 */
class UpdateShipMehod extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    protected $_fileFactory;
    protected $request;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\App\RequestInterface $request,
        ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->_fileFactory = $fileFactory;
        $this->request = $request;
    }

    /**
     * Ups home page
     *
     * @return Page|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        try {
            
            $post = $this->request->getPostValue();
            $ship_method = $post['change_ship_method'];
            $item_box = $post['box_ids'];
            

            if (!empty($post)) {
                $boxIncrementIdArray = explode(",", $post['box_ids']);

                $query = "SELECT box.box_id,box.item_id,box.order_id,item.shipping_method,item.vendor,item.shipping_method
                  FROM `sales_order_item` as item
                  LEFT JOIN order_item_box as box 
                   on item.item_id = box.item_id
                 WHERE  find_in_set(box.box_id, '".$item_box."') > 0";
                $result = $this->resourceConnection->getConnection()->fetchAll($query);       
                foreach($result as $value){
                    //print_r($value); die('test');
                    $shippinMethod = $post['change_ship_method'];
                    $qry = "UPDATE sales_order_item SET shipping_method = '$shippinMethod' WHERE item_id = '".$value['item_id']."' ";
                $this->resourceConnection->getConnection()->query($qry);
                }
                $data = [
                    'status' => true,
                    'message' => 'Update Box ImageUrl and    Successfully'
                ];
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Please enter valid box Ids'
                ];
            }

        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        echo '<pre>';
        print_r($data);
        exit();
    }
}
