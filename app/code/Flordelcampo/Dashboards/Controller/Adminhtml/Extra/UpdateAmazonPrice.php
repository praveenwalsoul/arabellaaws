<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\Extra;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Flordelcampo\CronJobs\Model\UpdateAmazonPrice as UpdateCron;

/**
 * Class Index
 *
 * Flordelcampo\Dashboards\Controller\Adminhtml\Extra
 */
class UpdateAmazonPrice extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var UpdateCron
     */
    protected $updateCron;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param UpdateCron $updateCron
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        UpdateCron $updateCron
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->updateCron = $updateCron;
    }

    /**
     * Ups home page
     *
     * @return Page|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $url = $this->_redirect->getRefererUrl();
        try {
            $params = $this->_request->getParams();
            $fromDate = $toDate = null;
            if (isset($params['from_date']) && isset($params['to_date'])) {
                $fromDate = $params['from_date'];
                $toDate = $params['to_date'];
            }
            $this->updateCron->updateOrderItemPrice($fromDate, $toDate);
            
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->messageManager->addSuccessMessage(
            __('Orders Price Updated Successfully')
        );
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }
}
