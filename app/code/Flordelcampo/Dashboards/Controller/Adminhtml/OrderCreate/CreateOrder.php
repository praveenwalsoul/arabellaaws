<?php

namespace Flordelcampo\Dashboards\Controller\Adminhtml\OrderCreate;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http as Request;
use Magento\Framework\File\Csv;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;
use Flordelcampo\Order\Helper\Data as orderHelper;
use Flordelcampo\Order\Model\Order as Create;

/**
 * Class UpdateVendorForItem
 * Flordelcampo\Dashboards\Controller\Adminhtml\OrderCreate
 */
class CreateOrder extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Csv
     */
    protected $csv;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;
    /**
     * @var orderHelper
     */
    protected $orderHelper;
    /**
     * @var Create
     */
    protected $orderCreate;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param CustomerHelper $customerHelper
     * @param VendorHelper $vendorHelper
     * @param Request $request
     * @param Csv $csv
     * @param CatalogHelper $catalogHelper
     * @param orderHelper $orderHelper
     * @param Create $orderCreate
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        CustomerHelper $customerHelper,
        VendorHelper $vendorHelper,
        Request $request,
        Csv $csv,
        CatalogHelper $catalogHelper,
        orderHelper $orderHelper,
        Create $orderCreate
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerHelper = $customerHelper;
        $this->vendorHelper = $vendorHelper;
        $this->request = $request;
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->csv = $csv;
        $this->catalogHelper = $catalogHelper;
        $this->orderHelper = $orderHelper;
        $this->orderCreate = $orderCreate;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Return New Form HTML
     *
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        try {
            $params = $this->getRequest()->getParams();
            $inputParams['customer_id'] = $params['customer_id'];
            $inputParams['store_id'] = $params['store_id'];
            $inputParams['shipping_address_id'] = $params['shipping_address_id'];
            $inputParams['shipping_name'] = $params['shipping_name'];
            $inputParams['pickup_date'] = $params['pickup_date'];
            $inputParams['delivery_date'] = $params['delivery_date'];
            $inputParams['items'] = $params['items'];
            $this->orderCreate->placeOrderFromDashboard($inputParams);
            $data = [
                'status' => true,
                'message' => __('Order placed successfully')
            ];
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        $resultJson->setData($data);
        return $resultJson;
    }

    /**
     * @param $params
     * @return mixed|null
     */
    public function getNewAvailIdForVendor($params)
    {
        try {
            date_default_timezone_set("America/New_York");
            $currentDate = date('Y-m-d');
            $offerParams['vendor_id'] = $params['new_vendor'];
            $offerParams['loc_id'] = $params['new_vendor_farm'];
            $offerParams['sku'] = $params['sku'];
            $offerParams['exclude_zero_inventory'] = 'yes';
            $hardGoodAttributeId = $this->vendorHelper->getHardGoodAttributeId();
            $offerCollection = $this->vendorHelper->getVendorInventoryOfferCollection($offerParams);
            $offerCollection->addFieldToFilter('product_type_id', ['neq' => $hardGoodAttributeId]);
            $offerResponse = [];
            if ($offerCollection->getSize() > 0) {
                foreach ($offerCollection as $offer) {
                    $offerId = $offer->getId();
                    $vendorId = $offer->getData('vendor_id');
                    $inventory = $offer->getData('inventory_qty');
                    $offerStartDate = $offer->getData('start_date');
                    $offerExpiryDate = $offer->getData('expiry_date');
                    if (strtotime($currentDate) >= strtotime($offerStartDate)
                        && strtotime($currentDate) <= strtotime($offerExpiryDate)
                    ) {
                        if (isset($offerResponse[$vendorId])) {
                            $prevInventory = $offerResponse[$vendorId]['inventory'];
                            if ($inventory > $prevInventory) {
                                $offerResponse[$vendorId] = [
                                    'inventory' => $inventory,
                                    'offer_id' => $offerId
                                ];
                            }
                        } else {
                            $offerResponse[$vendorId] = [
                                'inventory' => $inventory,
                                'offer_id' => $offerId
                            ];
                        }
                    }
                }
            }
            $newOfferId = null;
            if (count($offerResponse) > 0) {
                foreach ($offerResponse as $vendorId => $offerData) {
                    $newOfferId = $offerData['offer_id'];
                }
            }
            return $newOfferId;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Return New Hard Good Offer ID
     * @param $params
     * @return mixed|null
     */
    public function getNewHardGoodOfferId($params)
    {
        date_default_timezone_set("America/New_York");
        $currentDate = date('Y-m-d');
        try {
            $hardGoodSku = $params['sku'];
            $hardGoodAvailIdDetails = [];
            $invCollection = $this->vendorHelper->getVendorInventoryOfferCollection($params);
            if ($invCollection->getSize() > 0) {
                foreach ($invCollection as $offer) {
                    $offerId = $offer->getId();
                    $hg_sku = $offer->getData('sku');
                    $invQty = $offer->getData('inventory_qty');
                    $offerStartDate = $offer->getData('start_date');
                    $offerExpiryDate = $offer->getData('expiry_date');
                    if (strtotime($currentDate) >= strtotime($offerStartDate)
                        && strtotime($currentDate) <= strtotime($offerExpiryDate)
                    ) {
                        if (isset($hardGoodAvailIdDetails[$hg_sku])) {
                            $prevInvQty = $hardGoodAvailIdDetails[$hg_sku]['inventory_qty'];
                            if ($invQty > $prevInvQty) {
                                $hardGoodAvailIdDetails[$hg_sku] = [
                                    'hard_good_sku' => $hg_sku,
                                    'offer_id' => $offerId,
                                    'inventory_qty' => $invQty
                                ];
                            }
                        } else {
                            $hardGoodAvailIdDetails[$hg_sku] = [
                                'hard_good_sku' => $hg_sku,
                                'offer_id' => $offerId,
                                'inventory_qty' => $invQty
                            ];
                        }
                    }
                }
            }
            $hardGoodNewOfferId = null;
            if (count($hardGoodAvailIdDetails) > 0) {
                $hardGoodNewOfferId = $hardGoodAvailIdDetails[$hardGoodSku]['offer_id'];
            }
            return $hardGoodNewOfferId;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param $itemId
     * @return false
     */
    public function decrementInventoryForNewOfferByItemId($itemId)
    {
        try {
            $orderItem = $this->orderHelper->getOrderItemsModelByItemId($itemId);
            /**
             * Decrement Qty in vendor_inventory_offer_table inventory_qty for sku
             */
            $offerId = $orderItem->getData('offer_avail_id');
            if ($offerId != null && $offerId != '') {
                $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
                if ($offer->getId() != null) {
                    $currentQty = $orderItem->getQtyOrdered() * $orderItem->getData('qty_per_box');
                    $currentInventory = $offer->getData('inventory_qty');
                    $updatedInv = $currentInventory - $currentQty;
                    $offer->setData('inventory_qty', $updatedInv)->save();
                }
            }
            /**
             * Hard Good Inventory Decrement Logic
             */
            $itemId = $orderItem->getId();
            $hardGoodItemCollection = $this->orderHelper->getHardGoodItemCollection();
            $hardGoodItemCollection->addFieldToFilter('item_id', $itemId);
            if ($hardGoodItemCollection->getSize() > 0) {
                $stockRegistry = $this->catalogHelper->getStockRegistryInterface();
                foreach ($hardGoodItemCollection as $oneHg) {
                    $offerId = $oneHg->getData('hg_offer_id');
                    $orderedHgQty = $oneHg->getData('hg_ordered_qty');
                    $hardGoodSku = $oneHg->getData('hg_product_sku');
                    $invToDecrement = $orderItem->getQtyOrdered() * $orderedHgQty;
                    $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
                    if ($offer->getId() != null) {
                        $currentInventory = $offer->getData('inventory_qty');
                        $updatedInv = $currentInventory - $invToDecrement;
                        $offer->setData('inventory_qty', $updatedInv)->save();
                        /**
                         * Lets decrement in actual product table also
                         */
                        $stockData = $stockRegistry->getStockItemBySku($hardGoodSku);
                        $curQty = $stockData->getQty();
                        $updatedQty = $curQty - $invToDecrement;
                        $stockData->setQty($updatedQty)->save();
                    }
                }
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}
