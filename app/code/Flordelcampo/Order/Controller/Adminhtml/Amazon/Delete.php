<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Amazon;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;


/**
 * Class Delete
 * Flordelcampo\Order\Controller\Adminhtml\Amazon
 */
class Delete extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection
    )
    {
        parent::__construct($context);
        $this->resourceConnection = $resourceConnection;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('');
    }

    /**
     * Index action
     *
     * @return Page
     */
    public function execute()
    {
        try {
            $duplicateQuery = "SELECT COUNT(partner_order_id) as num_of_duplicate ,partner_order_id , GROUP_CONCAT(entity_id) as ids 
                        FROM `sales_order` 
                        GROUP by partner_order_id
                        HAVING COUNT(partner_order_id) > 1";
            $queryResult = $this->resourceConnection->getConnection()->fetchAll($duplicateQuery);
            if (count($queryResult) > 0) {
                $duplicateIdArray = [];
                foreach ($queryResult as $result) {
                    $idString = $result['ids'];
                    $idArray = explode(",", $idString);
                    array_shift($idArray);
                    $duplicateIdArray[] = array_values($idArray);
                }
                $orderEntityIdArray = [];
                foreach ($duplicateIdArray as $key => $idData) {
                    foreach ($idData as $id) {
                        $orderEntityIdArray[] = $id;
                    }
                }
                $entityIdString = "'" . implode("', '", $orderEntityIdArray) . "'";
                $deleteQry = "DELETE FROM sales_order 
                WHERE entity_id in ($entityIdString)";
                $this->resourceConnection->getConnection()->query($deleteQry);
            } else {
                echo "No Duplicate Orders to delete";
                die;
            }
        } catch (\Exception $e) {
            echo "ERROR ! : " . $e->getMessage();
            exit();
        }
        echo "Duplicate Orders Deleted Successfully";
        die;
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        return $resultPage;
    }
}
