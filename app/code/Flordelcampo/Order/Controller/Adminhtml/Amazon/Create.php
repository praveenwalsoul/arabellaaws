<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Amazon;

use Exception;
use Flordel\Amazon\Model\ListOrders;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Catalog\Model\Product;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\File\Csv;
use Flordelcampo\Order\Block\CreateOrder;
use Magento\Framework\Controller\Result\JsonFactory;
use Flordelcampo\Order\Model\Order;
use Flordelcampo\Order\Block\Amazon as AmazonBlock;
use Flordelcampo\Order\Block\Walmart as WalmartBlock;
use Magento\Framework\App\ResourceConnection;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Flordel\Amazon\Helper\Data as AmazonHelper;

/**
 * Class Create
 *
 * Flordelcampo\Order\Controller\Adminhtml\Walmart
 */
class Create extends Action
{
    /**
     * @var null
     */
    private $_error;
    /**
     * @var null
     */
    private $_FILE;
    /**
     * @var null
     */
    private $_params;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Csv
     */
    protected $_csv;
    /**
     * @var CreateOrder
     */
    protected $createOrderBlock;
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var Order
     */
    protected $orderCreate;
    /**
     * @var Product
     */
    protected $productModel;
    /**
     * @var RegionFactory
     */
    protected $regionFactory;
    /**
     * @var AmazonBlock
     */
    protected $amazonBlock;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var ListOrders
     */
    protected $listOrders;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var AmazonHelper
     */
    protected $amazonHelper;
    /**
     * @var WalmartBlock
     */
    protected $walmartBlock;

    /**
     * @param Context $context
     * @param Csv $csv
     * @param CreateOrder $createOrderBlock
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $jsonFactory
     * @param Order $order
     * @param Product $productModel
     * @param RegionFactory $regionFactory
     * @param AmazonBlock $amazonBlock
     * @param ResourceConnection $resourceConnection
     * @param ListOrders $listOrders
     * @param OrderHelper $orderHelper
     * @param AmazonHelper $amazonHelper
     * @param WalmartBlock $walmartBlock
     */
    public function __construct(
        Context $context,
        Csv $csv,
        CreateOrder $createOrderBlock,
        PageFactory $resultPageFactory,
        JsonFactory $jsonFactory,
        Order $order,
        Product $productModel,
        RegionFactory $regionFactory,
        AmazonBlock $amazonBlock,
        ResourceConnection $resourceConnection,
        ListOrders $listOrders,
        OrderHelper $orderHelper,
        AmazonHelper $amazonHelper,
        WalmartBlock $walmartBlock
    )
    {
        $this->_csv = $csv;
        $this->createOrderBlock = $createOrderBlock;
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonFactory = $jsonFactory;
        $this->orderCreate = $order;
        $this->productModel = $productModel;
        $this->regionFactory = $regionFactory;
        $this->amazonBlock = $amazonBlock;
        $this->walmartBlock = $walmartBlock;
        $this->resourceConnection = $resourceConnection;
        $this->listOrders = $listOrders;
        $this->orderHelper = $orderHelper;
        $this->amazonHelper = $amazonHelper;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();

        $params = $this->getRequest()->getParams();
        $file = $this->getRequest()->getFiles('order_file');
        $this->_FILE = $file;
        $this->_params = $params;
        if ($this->getRequest()->isPost()
            && !empty($this->getRequest()->getFiles('order_file'))
        ) {
            try {
                $validateFile = $this->validateFile();
                if ($validateFile) {
                    $csvData = $this->processCsvToArrayByFileName($file['tmp_name']);
                    if (is_array($csvData)) {
                        $response = $this->orderCreate->createOrderForAmazon(
                            $csvData,
                            $this->_params
                        );
                        $data = [
                            'status' => true,
                            'message' => 'Order Created Successfully',
                        ];
                    } else {
                        $data = [
                            'status' => false,
                            'message' => $this->_error,
                        ];
                    }

                } else {
                    $data = [
                        'status' => false,
                        'message' => 'Invalid File Format'
                    ];
                }
            } catch (Exception $e) {
                $data = [
                    'status' => false,
                    'message' => $e->getMessage()
                ];
            }

        } else {
            $data = [
                'status' => false,
                'message' => 'Invalid File'
            ];
        }
        $resultJson->setData($data);

        return $resultJson;
    }

    /**
     * Validate File Template
     */
    public function validateFile()
    {
        $mimesArray = $this->createOrderBlock->getMimesTypes();
        $FILE = $this->_FILE;
        if (!isset($FILE['tmp_name'])) {
            $response = false;
        } elseif (!in_array($FILE['type'], $mimesArray)) {
            $response = false;
        } else {
            $response = true;
        }
        return $response;
    }

    /**
     * Return Processed Array
     * @param $fileName
     * @return array|string
     * @throws Exception
     */
    public function processCsvToArrayByFileName($fileName)
    {
        $this->_error = null;
        $rawData = $this->_csv->getData($fileName);
        $processedData = $error = [];
        $count = 1;
        $rowIncrement = 0;
        foreach ($rawData as $row) {
            $rowIncrement++;
            $data = [];
            if ($count != 1) {
                /**
                 * Check for SKU existence
                 */
                $sku = $row[10];

                if (!$this->productModel->getIdBySku($sku)) {
                    $error[] = "Product with SKU " . $sku . " doesn't exist at row number " . $rowIncrement;
                    continue;
                }
                $partnerOrderId = $row[0];
                $partnerPo = $row[1];

                $data['order']['partner_order_id'] = $partnerOrderId;
                $data['order']['order_date'] = date("Y-m-d", strtotime($row[4]));

                $data['item']['partner_po'] = $partnerPo;
                $delivery_date = date("Y-m-d", strtotime($row[5]));
                $data['item']['shipping_date'] = date('Y-m-d', (strtotime('-2 day', strtotime($delivery_date))));
                $data['item']['delivery_date'] = $delivery_date;
                $data['item']['gift_message'] = $row[25];
                $data['item']['pref_ship_method'] = $row[15];
                $data['item']['promise_delivery_date'] = date("Y-m-d", strtotime($row[5]));
                $phoneArray = explode(" ", $row[9]);
                if (count($phoneArray) > 1) {
                    $phoneNumber = $phoneArray[1];
                } else {
                    $phoneNumber = $phoneArray[0];
                }
                /**
                 * Billing Address
                 */
                $nameArray = explode(" ", $row[8]);
                $b_firstName = $b_middleName = $b_lastName = '';
                if (count($nameArray) == 3) {
                    $b_firstName = $nameArray[0];
                    $b_middleName = $nameArray[1];
                    $b_lastName = $nameArray[2];
                } elseif (count($nameArray) == 2) {
                    $b_firstName = $nameArray[0];
                    $b_lastName = $nameArray[1];
                } else {
                    $b_firstName = $nameArray[0];
                }

                if ($b_lastName == '') {
                    $b_lastName = ".";
                }
                $region = strtoupper($row[21]);
                if (strlen($region) > 2) {
                    $regionArray = $this->getAllRegionsArray();
                    if (isset($regionArray[$region])) {
                        $region = $regionArray[$region];
                    } else {
                        $region = '';
                    }
                }
                $zip = $row[22];
                $zipArray = explode("-", $row[22]);
                if (count($zipArray) > 0) {
                    $zip = $zipArray[0];
                }
                $billingAddress = [
                    'customer_address_id' => '',
                    'prefix' => '',
                    'firstname' => $b_firstName,
                    'middlename' => $b_middleName,
                    'lastname' => $b_lastName,
                    'street' => [
                        '0' => $row[17],
                        '1' => $row[18]
                    ],
                    'city' => $row[20],
                    'country_id' => 'US',
                    'region' => $region,
                    'postcode' => $zip,
                    'telephone' => $phoneNumber
                ];

                /**
                 * Set Shipping Address
                 */

                $shippingNameArray = explode(" ", $row[16]);
                $pieces = array_chunk($shippingNameArray, 2);
                $nameArray = [];
                if (isset($pieces[0])) {
                    $nameArray[] = implode(" ", $pieces[0]);
                }
                if (isset($pieces[1])) {
                    $nameArray[] = implode("", $pieces[1]);
                }

                $s_firstName = $s_middleName = $s_lastName = '';
                if (count($nameArray) >= 3) {
                    $s_firstName = $nameArray[0];
                    $s_middleName = $nameArray[1];
                    $s_lastName = $nameArray[2];
                } elseif (count($nameArray) == 2) {
                    $s_firstName = $nameArray[0];
                    $s_lastName = $nameArray[1];
                } else {
                    $s_firstName = $nameArray[0];
                }
                /*$region = $this->regionFactory->create();
                $region = $region->loadByCode($row[21], 'US');*/

                if ($s_lastName == '') {
                    $s_lastName = ".";
                }
                $shippingAddress = [
                    'customer_address_id' => '',
                    'prefix' => '',
                    'firstname' => $s_firstName,
                    'middlename' => $s_middleName,
                    'lastname' => $s_lastName,
                    'street' => [
                        '0' => $row[17],
                        '1' => $row[18]
                    ],
                    'city' => $row[20],
                    'country_id' => 'US',
                    'region' => $region,
                    'postcode' => $zip,
                    'telephone' => $phoneNumber
                ];
                $data['billing_address'] = $billingAddress;
                $data['shipping_address'] = $shippingAddress;
                $data['item']['sku'] = $sku;
                $data['item']['qty'] = $row[12];

                $processedData[$partnerOrderId][] = $data;

            } else {
                $count = 99;
            }
        }
        if (count($error) > 0) {
            $errorString = implode("<br>", $error);
            $this->_error = $errorString;
            return 'fail';
        }
        return $processedData;
    }

    /**
     * Return processed data for Amazon API csv
     * @param $filePath
     * @return array|string
     * @throws Exception
     */
    public function processCsvForApiOrderToArrayByFileData($filePath)
    {
        $this->_error = null;
        $rawData = $this->amazonHelper->getConvertCsvTOArray($filePath);
        $file = fopen($filePath, "r");
        $firstRow = fgets($file);
        $header = preg_split("/[\t]/", $firstRow);
        $rawData = [];
        $header = array_map('trim', $header);
        $rawData[] = $header;
        while (!feof($file)) {
            $row = fgets($file);
            if (trim($row) != '') {
                $row = preg_split("/[\t]/", $row);
                $row = $this->utf8ize($row);
                //$all_rows[] = array_combine($header, $row);
                $rawData[] = $row;
            }
        };
        /**
         * Get Price data for item id
         */
        /**
         * $firstLastOrderIdArray = $this->getFirstAndLastOrderId($rawData);
         * $firstOrderId = $firstLastOrderIdArray['firstOrderId'];
         * $lastOrderId = $firstLastOrderIdArray['lastOrderId'];
         * $priceDataArray = $this->listOrders->getOrdersItemsPrice($firstOrderId, $lastOrderId);
         * $orderItemPriceArray = [];
         * foreach ($priceDataArray as $orderId => $itemData) {
         * foreach ($itemData as $item) {
         * $orderItemPriceArray[$item['order-item-id']] = $item['price'];
         * }
         * }
         */
        $processedData = $error = [];
        $count = 1;
        $rowIncrement = 0;
        foreach ($rawData as $row) {
            $rowIncrement++;
            $data = [];
            if ($count != 1) {
                /**
                 * Check for SKU existence
                 */
                $sku = $row[10];
                if (!$this->productModel->getIdBySku($sku)) {
                    $error[] = "Product with SKU " . $sku . " doesn't exist at row number " . $rowIncrement;
                    continue;
                }
                $partnerOrderId = $row[0];
                $partnerPo = $row[1];

                $data['order']['partner_order_id'] = $partnerOrderId;
                $data['order']['order_date'] = date("Y-m-d", strtotime($row[4]));

                $data['item']['partner_po'] = $partnerPo;
                $delivery_date = date("Y-m-d", strtotime($row[5]));
                $data['item']['shipping_date'] = date('Y-m-d', (strtotime('-2 day', strtotime($delivery_date))));
                $data['item']['delivery_date'] = $delivery_date;
                $data['item']['gift_message'] = $row[25];
                $data['item']['pref_ship_method'] = $row[15];
                $data['item']['promise_delivery_date'] = $delivery_date;
                if (isset($orderItemPriceArray[$partnerPo])) {
                    $data['item']['price'] = $orderItemPriceArray[$partnerPo];
                }
                if (trim($row[9]) == '') {
                    $row[9] = "+1 314-282-9402 ext. 38477";
                }
                $phoneArray = explode(" ", $row[9]);
                if (count($phoneArray) > 1) {
                    $phoneNumber = $phoneArray[1];
                } else {
                    $phoneNumber = $phoneArray[0];
                }
                /**
                 * Billing Address
                 */
                $nameArray = explode(" ", $row[8]);
                $b_firstName = $b_middleName = $b_lastName = '';
                if (count($nameArray) == 3) {
                    $b_firstName = $nameArray[0];
                    $b_middleName = $nameArray[1];
                    $b_lastName = $nameArray[2];
                } elseif (count($nameArray) == 2) {
                    $b_firstName = $nameArray[0];
                    $b_lastName = $nameArray[1];
                } else {
                    $b_firstName = $nameArray[0];
                }

                if ($b_lastName == '') {
                    $b_lastName = ".";
                }

                $region = strtoupper($row[21]);
                if (strlen($region) > 2) {
                    $regionArray = $this->getAllRegionsArray();
                    if (isset($regionArray[$region])) {
                        $region = $regionArray[$region];
                    } else {
                        $region = '';
                    }
                }

                $zip = $row[22];
                $zipArray = explode("-", $row[22]);
                if (count($zipArray) > 0) {
                    $zip = $zipArray[0];
                }
                $billingAddress = [
                    'customer_address_id' => '',
                    'prefix' => '',
                    'firstname' => $b_firstName,
                    'middlename' => $b_middleName,
                    'lastname' => $b_lastName,
                    'street' => [
                        '0' => $row[17],
                        '1' => $row[18]
                    ],
                    'city' => $row[20],
                    'country_id' => 'US',
                    'region' => $region,
                    'postcode' => $zip,
                    'telephone' => $phoneNumber
                ];

                /**
                 * Set Shipping Address
                 */
                $shippingNameArray = explode(" ", $row[16]);
                $pieces = array_chunk($shippingNameArray, 2);
                $nameArray = [];
                if (isset($pieces[0])) {
                    $nameArray[] = implode(" ", $pieces[0]);
                }
                if (isset($pieces[1])) {
                    $nameArray[] = implode("", $pieces[1]);
                }
                $s_firstName = $s_middleName = $s_lastName = '';
                if (count($nameArray) >= 3) {
                    $s_firstName = $nameArray[0];
                    $s_middleName = $nameArray[1];
                    $s_lastName = $nameArray[2];
                } elseif (count($nameArray) == 2) {
                    $s_firstName = $nameArray[0];
                    $s_lastName = $nameArray[1];
                } else {
                    $s_firstName = $nameArray[0];
                }
                /*$region = $this->regionFactory->create();
                $region = $region->loadByCode($row[21], 'US');*/

                if ($s_lastName == '') {
                    $s_lastName = ".";
                }
                $shippingAddress = [
                    'customer_address_id' => '',
                    'prefix' => '',
                    'firstname' => $s_firstName,
                    'middlename' => $s_middleName,
                    'lastname' => $s_lastName,
                    'street' => [
                        '0' => $row[17],
                        '1' => $row[18]
                    ],
                    'city' => $row[20],
                    'country_id' => 'US',
                    'region' => $region,
                    'postcode' => $zip,
                    'telephone' => $phoneNumber
                ];
                $data['billing_address'] = $billingAddress;
                $data['shipping_address'] = $shippingAddress;
                $data['item']['sku'] = $sku;
                $data['item']['qty'] = $row[12];

                $processedData[$partnerOrderId][] = $data;

            } else {
                $count = 99;
            }
        }
        if (count($error) > 0) {
            $errorString = implode("<br>", $error);
            $this->_error = $errorString;
        }
        return $processedData;
    }

    /**
     * Process Walmart orders to array
     * @param $orders
     * @return array
     */
    public function processWalmartApiOrderToArray($orders)
    {
        $processedData = $error = [];
        $this->_error = null;
        foreach ($orders as $oneOrder) {
            $sku = $oneOrder['sku'];
            $data = [];
            if (!$this->productModel->getIdBySku($sku)) {
                $error[] = "Product with SKU " . $sku . " doesn't exist at row number";
                continue;
            }
            $partnerOrderId = $oneOrder['purchaseOrderId'];
            $partnerPo = $oneOrder['customerOrderId'];

            $data['order']['partner_order_id'] = $partnerOrderId;

            $data['item']['partner_po'] = $partnerPo;
            $delivery_date = date("Y-m-d", strtotime($oneOrder['estimatedDeliveryDate']));
            $data['item']['shipping_date'] = date("Y-m-d", strtotime($oneOrder['estimatedShipDate']));
            $data['item']['delivery_date'] = $delivery_date;
            $data['item']['track_number'] = '';
            $data['item']['item_price'] = $str = ltrim($oneOrder['itemPrice'], '$');

            $phoneNumber = $oneOrder['customerPhone'];
            $nameArray = explode(" ", $oneOrder['customerName']);
            $firstName = $lastName = ".";
            if (count($nameArray) > 1) {
                $firstName = $nameArray[0];
                $lastName = $nameArray[1];
            }
            $billingAddress = $shippingAddress = [
                'customer_address_id' => '',
                'prefix' => '',
                'firstname' => $firstName,
                'middlename' => '',
                'lastname' => $lastName,
                'street' => [
                    '0' => $oneOrder['address1'],
                    '1' => $oneOrder['address2']
                ],
                'city' => $oneOrder['city'],
                'country_id' => 'US',
                'region' => $oneOrder['state'],
                'postcode' => $oneOrder['zip'],
                'telephone' => $oneOrder['customerPhone']
            ];

            $data['address'] = $billingAddress;
            $data['item']['sku'] = $oneOrder['sku'];
            $data['item']['qty'] = $oneOrder['qty'];

            $tax = ($oneOrder['tax'] == '$') ? '$0' : $oneOrder['tax'];
            $shippingPrice = ($oneOrder['shipping_price'] == '$') ? '$0' : $oneOrder['shipping_price'];
            $data['order']['tax'] = ltrim($tax, '$');
            $data['order']['ship_price'] = ltrim($shippingPrice, '$');
            $processedData[$partnerOrderId][] = $data;
        }
        if (count($error) > 0) {
            $this->_error = implode("<br>", $error);
        }
        return $processedData;
    }

    /**
     * Return Processed Array
     * @param $filePath
     * @return bool|array
     * @throws Exception
     */
    public function createOrderForAmazonApiOrders($filePath)
    {
        $data = $params = [];
        try {
            $processedArray = $this->processCsvForApiOrderToArrayByFileData($filePath);
            if (count($processedArray) > 0) {
                $params['customer_id'] = $this->amazonBlock->getAmazonCustomerId();
                $params['store_code'] = $this->amazonBlock->getAmazonStoreCode();
                $response = $this->orderCreate->createOrderForAmazon(
                    $processedArray,
                    $params
                );
                $data = [
                    'status' => true,
                    'message' => 'Order Created Successfully',
                ];
            } else {
                $data = [
                    'status' => true,
                    'message' => 'No Orders Found !!',
                ];
            }
        } catch (Exception $e) {
            $errorMsg = '';
            if ($this->_error != null) {
                $errorMsg = $this->_error;
            }
            $data = [
                'status' => false,
                'message' => $e->getMessage(),
                'error_msg' => $errorMsg
            ];
        }

        return $data;
    }

    public function createOrderForWalmartApiOrders($orders)
    {
        $data = $params = [];
        try {
            if (count($orders) > 0) {
                $processedArray = $this->processWalmartApiOrderToArray($orders);
                $params['customer_id'] = $this->walmartBlock->getWalMartCustomerId();
                $params['store_code'] = $this->walmartBlock->getWalMartStoreCode();
                $response = $this->orderCreate->createOrderForWalMart(
                    $processedArray,
                    $params
                );
                $data = [
                    'status' => true,
                    'message' => 'Order Created Successfully',
                ];
            } else {
                $data = [
                    'status' => true,
                    'message' => 'No Orders Found !!',
                ];
            }
        } catch (Exception $e) {
            $errorMsg = '';
            if ($this->_error != null) {
                $errorMsg = $this->_error;
            }
            $data = [
                'status' => false,
                'message' => $e->getMessage(),
                'error_msg' => $errorMsg
            ];
        }
        return $data;
    }

    /**
     * Update Status for cancelled orders
     * @param $ordersArray
     * @return array
     */
    public function updateStatusForAmazonCancelOrders($ordersArray)
    {
        $data = [];
        try {
            $orderItemsIdArray = [];
            foreach ($ordersArray as $orderId => $itemArray) {
                foreach ($itemArray as $itemId) {
                    $orderItemsIdArray[] = $itemId;
                    //$orderItemsIdArray[] = ltrim($itemId, '0');
                }
            }
            if (count($orderItemsIdArray) > 0) {
                $amazonItemIdString = "'" . implode("', '", $orderItemsIdArray) . "'";
                $query = "SELECT DISTINCT item . item_id
                  FROM sales_order_item as item
                  WHERE item . partner_po in($amazonItemIdString)";

                $queryResult = $this->resourceConnection->getConnection()->fetchAll($query);
                $orderItemIdArray = [];
                foreach ($queryResult as $row) {
                    $orderItemIdArray[] = $row['item_id'];
                }

                $itemIdString = "'" . implode("', '", $orderItemIdArray) . "'";
                $query = "UPDATE order_item_box 
                          SET status = 'cancelled' 
                          WHERE item_id in($itemIdString)";
                $this->resourceConnection->getConnection()->query($query);
                $data = [
                    'status' => 'success',
                    'message' => 'updated successfully'
                ];
            } else {
                $data = [
                    'status' => 'fail',
                    'message' => 'No record found!!'
                ];
            }
        } catch (Exception $e) {
            $data = [
                'status' => 'fail',
                'message' => $e->getMessage()
            ];
        }
        return $data;
    }

    /**
     * UTF 8
     *
     * @param $mixed
     * @return array|false|string|string[]|null
     */
    public function utf8ize($mixed)
    {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = $this->utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
        }
        return $mixed;
    }

    /**
     * Get first and Last order Id
     * @param array $rawData
     * @return array
     */
    public function getFirstAndLastOrderId($rawData)
    {
        array_shift($rawData);
        $firstOrderId = $rawData[0][0];
        if (count($rawData) > 1) {
            $row = end($rawData);
            $lastOrderId = $row[0];
        } else {
            $lastOrderId = $firstOrderId;
        }
        return ['firstOrderId' => $firstOrderId, 'lastOrderId' => $lastOrderId];
    }

    /**
     * Return Regions
     * @return array
     */
    public function getAllRegionsArray()
    {
        return $this->orderHelper->getAllUsRegionsByNameWise();
    }
}
