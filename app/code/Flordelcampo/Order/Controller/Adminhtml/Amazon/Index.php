<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Amazon;

use Flordel\Amazon\Helper\Data;
use Flordel\Amazon\Model\ListOrders;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Flordelcampo\Order\Block\Amazon as AmazonBlock;
use Flordelcampo\Order\Model\Order;

/**
 * Class Index
 * Flordelcampo\Order\Controller\Adminhtml\Amazon
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var ListOrders
     */
    protected $listOrder;
    /**
     * @var Order
     */
    protected $orderCreate;
    /**
     * @var AmazonBlock
     */
    protected $amazonBlock;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Data $helper
     * @param ListOrders $listOrder
     * @param Order $order
     * @param AmazonBlock $amazonBlock
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Data $helper,
        ListOrders $listOrder,
        Order $order,
        AmazonBlock $amazonBlock
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->listOrder = $listOrder;
        $this->resultPageFactory = $resultPageFactory;
        $this->orderCreate = $order;
        $this->amazonBlock = $amazonBlock;
    }

    /**
     * Index action
     *
     * @return Page
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Create Order For Amazon')));
        return $resultPage;
    }
}

 