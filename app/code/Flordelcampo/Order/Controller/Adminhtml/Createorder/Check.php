<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Createorder;

use Flordelcampo\Order\Block\CreateOrder;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;


/**
 * Class CheckData
 *
 * @package Flordelcampo\Order\Controller\Adminhtml\Createorder
 */
class Check extends Action
{

    /**
     * Result Factory
     *
     * @var ResultFactory
     */
    protected $resultPageFactory;
    /**
     * @var CreateOrder
     */
    protected $blockOrder;

    /**
     * Check constructor.
     *
     * @param Context $context Context
     * @param CreateOrder $blockOrder
     * @param ResultFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        CreateOrder $blockOrder,
        ResultFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->blockOrder = $blockOrder;
        parent::__construct($context);
    }

    /**
     * Check Data
     *
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        try {

            $resultRedirect = $this->resultPageFactory->create(
                ResultFactory::TYPE_REDIRECT
            );

            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;


        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

}