<?php

namespace Flordelcampo\Order\Controller\Adminhtml\Createorder;

use Flordelcampo\Order\Model\Order;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Check
 *
 * @package Flordelcampo\Order\Controller\Adminhtml\Createorder
 */
class Create extends Action
{
    /**
     * Json
     *
     * @var JsonFactory
     */
    protected $resultPageFactory;
    /**
     * Create order
     *
     * @var Order
     */
    protected $orderCreate;

    /**
     * Check constructor.
     *
     * @param Context $context Context
     * @param ResultFactory $resultPageFactory Result Factory
     * @param Order $order
     */
    public function __construct(
        Context $context,
        ResultFactory $resultPageFactory,
        Order $order
    )
    {
        $this->orderCreate = $order;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Return
     *
     * @return bool|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        if ($this->getRequest()->isPost()
            && !empty($this->getRequest()->getFiles('order_file'))
        ) {
            try {
                $res = $this->orderCreate->process(
                    $this->getRequest()->getFiles('order_file'),
                    $this->getRequest()->getParams()
                );
                $this->messageManager->addSuccessMessage(
                    __('The Orders Has Been Created Successfully')
                );
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        } else {
            $this->messageManager->addErrorMessage(
                __('Invalid file upload attempt')
            );
        }
        $resultRedirect = $this->resultPageFactory->create(
            ResultFactory::TYPE_REDIRECT
        );
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}