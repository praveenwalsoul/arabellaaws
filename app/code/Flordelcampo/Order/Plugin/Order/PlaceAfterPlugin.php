<?php

namespace Flordelcampo\Order\Plugin\Order;

use Exception;
use Flordelcampo\Order\Model\BoxFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Flordelcampo\Order\Model\Order;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Framework\App\ResourceConnection;
use Flordelcampo\VendorInventory\Model\InventoryLogic\OfferLogic;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Magento\Sales\Model\Convert\Order as ShipmentConvert;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

/**
 * Class PlaceAfterPlugin
 *
 * Flordelcampo\Order\Plugin\Order
 */
class PlaceAfterPlugin
{
    const QUOTE = 'quote';
    /**
     * Order Object
     *
     * @var null
     */
    private $_order;
    /**
     * Box Facotory
     *
     * @var BoxFactory
     */
    protected $boxFactory;
    /**
     * @var Order
     */
    protected $order;
    /**
     * @var OrderModel
     */
    protected $orderModel;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var OfferLogic
     */
    protected $offerLogic;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var ShipmentConvert
     */
    protected $shipmentConvert;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * PlaceAfterPlugin constructor.
     *
     * @param BoxFactory $boxFactory Box Factory
     * @param Order $order
     * @param OrderModel $orderModel
     * @param ResourceConnection $resourceConnection
     * @param OfferLogic $offerLogic
     * @param OrderHelper $orderHelper
     * @param ShipmentConvert $shipmentConvert
     * @param VendorHelper $vendorHelper
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        BoxFactory $boxFactory,
        Order $order,
        OrderModel $orderModel,
        ResourceConnection $resourceConnection,
        OfferLogic $offerLogic,
        OrderHelper $orderHelper,
        ShipmentConvert $shipmentConvert,
        VendorHelper $vendorHelper,
        CatalogHelper $catalogHelper
    ) {
        $this->boxFactory = $boxFactory;
        $this->order = $order;
        $this->orderModel = $orderModel;
        $this->resourceConnection = $resourceConnection;
        $this->offerLogic = $offerLogic;
        $this->orderHelper = $orderHelper;
        $this->shipmentConvert = $shipmentConvert;
        $this->vendorHelper = $vendorHelper;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * After Order Place Plugin
     *
     * @param OrderManagementInterface $orderManagementInterface Interface
     * @param OrderInterface $order Order
     *
     * @return object
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function afterPlace(
        OrderManagementInterface $orderManagementInterface,
        OrderInterface $order
    ) {
        $this->_order = $order;
        $this->createBoxDataForOrder();
        $orderModel = $this->orderModel->loadByIncrementId($order->getRealOrderId());
        $quote = $this->order->getQuoteByQuoteId($orderModel->getQuoteId());
        if ($order->getPartnerOrderId() == null) {
            $order->setPartnerOrderId($order->getRealOrderId());
            $order->save();
        }
        /**
         * Insert Hard Good Offer Details
         */
        foreach ($order->getAllVisibleItems() as $item) {
            try {
                $productSku = $item->getSku();
                $vendorId = $item->getData('vendor');
                $locationId = $item->getData('vendor_farm');
                $params['product_sku'] = $productSku;
                $params['vendor_id'] = $vendorId;
                $params['location_id'] = $locationId;
                $hardGoodOfferDetails = $this->offerLogic->getHardGoodOfferData($params);
                if (count($hardGoodOfferDetails) > 0) {
                    foreach ($hardGoodOfferDetails as $oneHg) {
                        $hardGoodItem = $this->orderHelper->getHardGoodItemModel();
                        $hardGoodItem->setData('item_id', $item->getId());
                        $hardGoodItem->setData('order_id', $item->getOrderId());
                        $hardGoodItem->setData('sku', $oneHg['product_sku']);
                        $hardGoodItem->setData('hg_product_sku', $oneHg['hard_good_sku']);
                        $hardGoodItem->setData('hg_ordered_qty', $oneHg['qty']);
                        $hardGoodItem->setData('hg_offer_id', $oneHg['offer_id']);
                        $hardGoodItem->save();
                    }
                }
            } catch (Exception $e) {
                continue;
            }
        }
        /**
         * Delete Quote
         */
        $quote->setIsActive(false)->save();
        /**
         * Create Shipment
         */
        $this->createShipmentForOderByOrderId($order->getRealOrderId());
        /**
         * Create Invoice
         */
        $this->createInvoiceForOrderByOrderId($order->getRealOrderId());

        return $order;
    }

    /**
     * @param $orderId
     * @return bool
     */
    public function createShipmentForOderByOrderId($orderId)
    {
        $order = $this->orderModel->loadByIncrementId($orderId);
        try {
            if (!$order->canShip()) {
                return true;
            }
            $shipment = $this->shipmentConvert->toShipment($order);
            foreach ($order->getAllItems() as $orderItem) {
                try {
                    if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                        continue;
                    }
                    $qtyShipped = $orderItem->getQtyToShip();
                    $shipmentItem = $this->shipmentConvert->itemToShipmentItem($orderItem)
                        ->setQty($qtyShipped);
                    $shipment->addItem($shipmentItem);
                    /**
                     * Decrement Qty in vendor_inventory_offer_table inventory_qty for sku
                     */
                    $offerId = $orderItem->getData('offer_avail_id');
                    if ($offerId != null && $offerId != '') {
                        $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
                        if ($offer->getId() != null) {
                            $currentQty = $orderItem->getQtyOrdered() * $orderItem->getData('qty_per_box');
                            $currentInventory = $offer->getData('inventory_qty');
                            $updatedInv = $currentInventory - $currentQty;
                            $offer->setData('inventory_qty', $updatedInv)->save();
                        }
                    }
                    /**
                     * Hard Good Inventory Decrement Logic
                     */
                    $itemId = $orderItem->getId();
                    $hardGoodItemCollection = $this->orderHelper->getHardGoodItemCollection();
                    $hardGoodItemCollection->addFieldToFilter('item_id', $itemId);
                    if ($hardGoodItemCollection->getSize() > 0) {
                        $stockRegistry = $this->catalogHelper->getStockRegistryInterface();
                        foreach ($hardGoodItemCollection as $oneHg) {
                            $offerId = $oneHg->getData('hg_offer_id');
                            $orderedHgQty = $oneHg->getData('hg_ordered_qty');
                            $hardGoodSku = $oneHg->getData('hg_product_sku');
                            $invToDecrement = $orderItem->getQtyOrdered() * $orderedHgQty;
                            $offer = $this->vendorHelper->getVendorInventoryOfferById($offerId);
                            if ($offer->getId() != null) {
                                $currentInventory = $offer->getData('inventory_qty');
                                $updatedInv = $currentInventory - $invToDecrement;
                                $offer->setData('inventory_qty', $updatedInv)->save();
                                /**
                                 * Lets decrement in actual product table also
                                 */
                                $stockData = $stockRegistry->getStockItemBySku($hardGoodSku);
                                $curQty = $stockData->getQty();
                                $updatedQty = $curQty - $invToDecrement;
                                $stockData->setQty($updatedQty)->save();
                            }
                        }
                    }
                } catch (Exception $e) {
                    continue;
                }
            }
            $shipment->register();
            $shipment->getOrder()->setIsInProcess(true);
            $shipment->save();
            $shipment->getOrder()->save();
            $shipment->save();

        } catch (\Exception $e) {
            return true;
        }
        return true;
    }

    /**
     * Create Invoice Once Order is Placed
     * @param $orderId
     * @return bool
     */
    public function createInvoiceForOrderByOrderId($orderId)
    {
        try {
            $order = $this->orderModel->loadByIncrementId($orderId);
            $invoiceService = $this->orderHelper->getInvoiceService();
            $transactionService = $this->orderHelper->getTransactionService();
            if ($order->canInvoice()) {
                $invoice = $invoiceService->prepareInvoice($order);
                $invoice->register();
                $invoice->save();
                $transactionSave = $transactionService->addObject(
                    $invoice
                )->addObject(
                    $invoice->getOrder()
                );
                $transactionSave->save();
                /*$this->invoiceSender->send($invoice);
                $order->addStatusHistoryComment(
                    __('Notified customer about invoice #%1.', $invoice->getId())
                )
                    ->setIsCustomerNotified(true)
                    ->save();*/
            }
        } catch (Exception $e) {
            return true;
        }
        return true;
    }

    /**
     * Create Box Details For Order Object
     *
     * @param null $order Order
     *
     * @return void;
     * @throws Exception
     */
    public function createBoxDataForOrder($order = null)
    {
        if ($order) {
            $this->_order = $order;
            $orderId = $order->getId();
        } else {
            $orderId = $this->_order->getId();
        }

        foreach ($this->_order->getItems() as $item) {
            $qty = $item->getQtyOrdered();
            $itemId = $item->getId();
            $track_id = $item->getTrackId();
            $gift_message = $item->getGiftMessage();
            for ($i = 1; $i <= $qty; $i++) {
                $boxModel = $this->boxFactory->create();
                $boxModel->addData(
                    [
                        "box_id" => $itemId . '_' . $i,
                        "order_id" => $orderId,
                        "item_id" => $itemId,
                        "track_id" => $track_id,
                        "gift_message" => $gift_message
                    ]
                );
                $boxModel->save();
            }
        }
        return;
    }
}