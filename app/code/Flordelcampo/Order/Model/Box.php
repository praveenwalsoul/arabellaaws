<?php

namespace Flordelcampo\Order\Model;

use Magento\Framework\Model\AbstractModel;
use Flordelcampo\Order\Model\ResourceModel\Box as ResourceBoxModel;

/**
 * Class Box
 *
 * Flordelcampo\Order\Model
 */
class Box extends AbstractModel
{
    const PRINTED = 'printed';
    const SHIPPED = 'shipped';
    const PENDING = 'pending';
    const CANCELLED = 'cancelled';
    const VENDOR_PRINT = 'vendorPrint';
    const REJECTED = 'rejected';

    const PROCESSING = 'processing';
    const INVOICED = 'invoiced';
    const REFUNDED = 'refunded';
    const DELIVERED = 'delivered';
    const UNDER_REVIEW = 'underReview';

    /**
     * Model
     */
    protected function _construct()
    {
        $this->_init(ResourceBoxModel::class);
    }

    /**
     * @return string[]
     */
    public function getAllBoxStatusArray()
    {
        return [
            'pending' => 'Pending',
            'processing' => 'Processing',
            'printed' => 'Packed',
            'vendorPrint' => 'Ready for Dispatch',
            'cancelled' => 'Cancelled',
            'rejected' => 'Rejected',
            'shipped' => 'Dispatched',
            'invoiced' => 'Invoiced',
            'refunded' => 'Refunded',
            'delivered' => 'Delivered',
            'underReview' => 'Under Review',
        ];
    }

    /**
     * Return Box Status Label By Status code
     * @param $code
     * @return string|null
     */
    public function getBoxStatusLabelByCode($code)
    {
        $statusList = $this->getAllBoxStatusArray();
        if (isset($statusList[$code])) {
            return $statusList[$code];
        } else {
            return null;
        }
    }

}