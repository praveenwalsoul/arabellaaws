<?php

namespace Flordelcampo\Order\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Post
 *
 * @package Flordelcampo\Order\Model\ResourceModel
 */
class Box extends AbstractDb
{

    /**
     * Post constructor.
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Box Table
     */
    protected function _construct()
    {
        $this->_init('order_item_box', 'entity_id');
    }

}