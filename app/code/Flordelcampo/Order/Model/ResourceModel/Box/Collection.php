<?php

namespace Flordelcampo\Order\Model\ResourceModel\Box;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @package Flordelcampo\Order\Model\ResourceModel\Box
 */
class Collection extends AbstractCollection
{
    protected $_idFieldName = 'box_id';
    protected $_eventPrefix = 'order_item_box_collection';
    protected $_eventObject = 'box_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Flordelcampo\Order\Model\Box',
            'Flordelcampo\Order\Model\ResourceModel\Box'
        );
    }

}
