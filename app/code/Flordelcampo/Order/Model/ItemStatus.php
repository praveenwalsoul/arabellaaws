<?php

namespace Flordelcampo\Order\Model;

/**
 * Class ItemStatus
 * Flordelcampo\Order\Model
 */
class ItemStatus
{
    const PRINTED = 'printed';
    const SHIPPED = 'shipped';
    const PENDING = 'pending';
    const CANCELLED = 'cancelled';
    const VENDOR_PRINT = 'vendorPrint';
    const REJECTED = 'rejected';

    const PROCESSING = 'processing';
    const INVOICED = 'invoiced';
    const REFUNDED = 'refunded';
    const DELIVERED = 'delivered';
    const UNDER_REVIEW = 'underReview';
    const UNDER_PROCESSING = 'underProcessing';

    /**
     * @return string[]
     */
    public function getAllItemStatusArray()
    {
        return [
            'pending' => 'Pending',
            'processing' => 'Processing',
            'printed' => 'Packed',
            'vendorPrint' => 'Ready for Dispatch',
            'cancelled' => 'Cancelled',
            'rejected' => 'Rejected',
            'shipped' => 'Dispatched',
            'invoiced' => 'Invoiced',
            'refunded' => 'Refunded',
            'delivered' => 'Delivered',
            'underReview' => 'Under Review',
            'underProcessing' => 'Under Processing',
        ];
    }

    /**
     * Return Box Status Label By Status code
     * @param $code
     * @return string|null
     */
    public function getItemStatusLabelByCode($code)
    {
        $statusList = $this->getAllItemStatusArray();
        if (isset($statusList[$code])) {
            return $statusList[$code];
        } else {
            return null;
        }
    }

}