<?php

namespace Flordelcampo\Order\Model;

use Dotdigitalgroup\Email\Helper\File;
use Exception;
use Flordelcampo\Customer\Helper\Data;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\AddressFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\File\Csv;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteManagement;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Flordelcampo\Customer\Helper\Data as CustomerHelper;
use Flordelcampo\Order\Block\CreateOrder;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\Convert\Order as ShipmentConvert;
use Flordelcampo\VendorInventory\Model\InventoryLogic\OfferLogic;
use Flordelcampo\Vendor\Helper\Data as VendorHelper;
use Flordelcampo\Catalog\Helper\Data as CatalogHelper;

/**
 * Class Customer
 *
 * Flordelcampo\Order\Model
 */
class Order
{
    /**
     * CSV
     *
     * @var Csv
     */
    private $_csv;
    /**
     * Store Manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * Customer Factory
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * Product Repository
     *
     * @var ProductRepositoryInterface
     */
    private $_productRepositoryInterface;
    /**
     * Quote Factory
     *
     * @var QuoteFactory
     */
    protected $quote;
    /**
     * Cart Repository
     *
     * @var CartRepositoryInterface
     */
    protected $cartRepository;
    /**
     * Product Repository
     *
     * @var ProductRepository
     */
    private $_productRepository;
    /**
     * Address Factory
     *
     * @var AddressFactory
     */
    protected $addressFactory;
    /**
     * Address Repository
     *
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;
    /**
     * Quote Factory
     *
     * @var QuoteRepository
     */
    protected $quoteRepository;
    /**
     * Order Management
     *
     * @var QuoteManagement
     */
    protected $orderManagement;
    /**
     * Customer Helper
     *
     * @var CustomerHelper
     */
    protected $customerHelper;
    /**
     * @var CreateOrder
     */
    protected $createOrderBlock;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;
    /**
     * @var OrderModel
     */
    protected $orderModel;
    /**
     * @var ShipmentConvert
     */
    protected $shipmentConvert;
    /**
     * @var OfferLogic
     */
    protected $offerLogic;
    /**
     * @var VendorHelper
     */
    protected $vendorLogic;
    /**
     * @var VendorHelper
     */
    protected $vendorHelper;
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;
    /**
     * @var OrderModel
     */

    /**
     * Order constructor.
     *
     * @param Csv $csv CSV Processor
     * @param StoreManagerInterface $storeManager Store
     * @param CustomerRepositoryInterface $customerRepository
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param QuoteFactory $quote
     * @param CartRepositoryInterface $cartRepository
     * @param ProductRepository $productRepository
     * @param AddressFactory $addressFactory
     * @param AddressRepositoryInterface $addressRepository
     * @param QuoteRepository $quoteRepository
     * @param QuoteManagement $orderManagement
     * @param CustomerHelper $customerHelper
     * @param CreateOrder $createOrderBlock
     * @param OrderHelper $orderHelper
     * @param OrderModel $orderModel
     * @param ShipmentConvert $shipmentConvert
     * @param OfferLogic $offerLogic
     * @param VendorHelper $vendorHelper
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Csv $csv,
        StoreManagerInterface $storeManager,
        CustomerRepositoryInterface $customerRepository,
        ProductRepositoryInterface $productRepositoryInterface,
        QuoteFactory $quote,
        CartRepositoryInterface $cartRepository,
        ProductRepository $productRepository,
        AddressFactory $addressFactory,
        AddressRepositoryInterface $addressRepository,
        QuoteRepository $quoteRepository,
        QuoteManagement $orderManagement,
        CustomerHelper $customerHelper,
        CreateOrder $createOrderBlock,
        OrderHelper $orderHelper,
        OrderModel $orderModel,
        ShipmentConvert $shipmentConvert,
        OfferLogic $offerLogic,
        VendorHelper $vendorHelper,
        CatalogHelper $catalogHelper
    )
    {
        $this->_csv = $csv;
        $this->storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->quote = $quote;
        $this->cartRepository = $cartRepository;
        $this->_productRepository = $productRepository;
        $this->addressFactory = $addressFactory;
        $this->addressRepository = $addressRepository;
        $this->quoteRepository = $quoteRepository;
        $this->orderManagement = $orderManagement;
        $this->customerHelper = $customerHelper;
        $this->orderHelper = $orderHelper;
        $this->orderModel = $orderModel;
        $this->createOrderBlock = $createOrderBlock;
        $this->shipmentConvert = $shipmentConvert;
        $this->offerLogic = $offerLogic;
        $this->vendorHelper = $vendorHelper;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * Place order for Amazon
     *
     * @param $csvData
     * @param $params
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function createOrderForAmazon($csvData, $params)
    {
        $this->createOrderBlock->getBackendSession()->setSource('backend');
        $skuOriginalPriceUpdateArray = [];
        $customerId = $params['customer_id'];
        /**
         * Store id is store code id
         */
        $inventoryLogicParams = [];
        $storeCode = $params['store_code'];
        $store = $this->orderHelper->getStoreByCode($storeCode);

        foreach ($csvData as $partnerOrderId => $orderItemData) {
            try {
                $orderCollection = $this->orderHelper->getOrderCollectionByPartnerOrderId($partnerOrderId);
                /**
                 * check if order with partner id already created or not
                 */
                if ($orderCollection->count() > 0) {
                    continue;
                }
                $quote = $this->getCustomerQuoteForAmazonWalMart($customerId, $store);
                $customerSalesRep = $this->getCustomerSalesRep($customerId);
                foreach ($orderItemData as $item) {
                    $inventoryLogicParams['pref_ship_method'] = $item['item']['pref_ship_method'];
                    $inventoryLogicParams['sku'] = $item['item']['sku'];
                    $inventoryLogicParams['zip_code'] = $item['shipping_address']['postcode'];
                    $sku = $item['item']['sku'];
                    $offerDetails = $this->offerLogic->getOfferDataForSku($inventoryLogicParams);
                    $vendorId = $vendorFarm = $deliveryDate = $shipMethod = $pickUpDate = $offerId = null;
                    if (isset($offerDetails['vendor_id'])) {
                        $vendorId = $offerDetails['vendor_id'];
                    }
                    if (isset($offerDetails['location_id'])) {
                        $vendorFarm = $offerDetails['location_id'];
                    }
                    if (isset($offerDetails['offer_id'])) {
                        $offerId = $offerDetails['offer_id'];
                    }
                    if (isset($offerDetails['pickup_date'])) {
                        $pickUpDate = $offerDetails['pickup_date'];
                    }
                    if (isset($offerDetails['delivery_date'])) {
                        $deliveryDate = $offerDetails['delivery_date'];
                    }
                    if (isset($offerDetails['shipping_method'])) {
                        $shipMethod = $offerDetails['shipping_method'];
                    }
                    $product = $this->_productRepository->get($sku);
                    if (isset($item['item']['price'])) {
                        $price = $item['item']['price'];
                    } else {
                        $price = $product->getPrice();
                    }
                    $qty = $item['item']['qty'];
                    $skuCurrentPrice = $product->getPrice();
                    if (!array_key_exists($sku, $skuOriginalPriceUpdateArray)) {
                        $skuOriginalPriceUpdateArray[$sku] = $skuCurrentPrice;
                    }
                    $product->setPrice($price)->save();
                    $quote->addProduct($product, (int)$qty);
                    $quote->setOrderType('');
                    $quote->setPartnerOrderId($partnerOrderId);
                    /*$quote->setStore($store);*/
                    $quote->save();
                    /**
                     * Save Quote
                     */
                    $quoteItem = $this->getLatestQuoteItem($quote);
                    $quoteItem->setQtyPerBox('1');
                    $quoteItem->setFarmPrice($price);
                    $quoteItem->setTotalPrice($price);
                    $quoteItem->setPoUnitPrice($price);
                    $quoteItem->setVendor($vendorId);
                    $quoteItem->setVendorFarm($vendorFarm);
                    $quoteItem->setShippingMethod($shipMethod);
                    $quoteItem->setDeliveryDate($deliveryDate);
                    $quoteItem->setPickupDate($pickUpDate);
                    $quoteItem->setTruckPickupDate($deliveryDate);
                    $quoteItem->setOfferAvailId($offerId);
                    $quoteItem->setPartnerPo($item['item']['partner_po']);
                    $quoteItem->setGiftMessage($item['item']['gift_message']);
                    $quoteItem->setCustomerSalesRep($customerSalesRep);
                    $quoteItem->setPromiseDeliveryDate($item['item']['promise_delivery_date']);
                    $quoteItem->setPromiseShipMethod($item['item']['pref_ship_method']);
                    $quoteItem->save();
                }
                $quote = $this->getQuoteByQuoteId($quote->getId());
                $billingAddressArray = $orderItemData[0]['billing_address'];
                $shippingAddressArray = $orderItemData[0]['shipping_address'];
                $quote->getBillingAddress()->addData($billingAddressArray);
                $quote->getShippingAddress()->addData($shippingAddressArray);
                $quote->setCustomerSalesRep($customerSalesRep);
                /**
                 * Set Shipping Method | flatrate_flatrate
                 */
                $shippingAddress = $quote->getShippingAddress();
                $shippingAddress->setCollectShippingRates(true)
                    ->collectShippingRates()
                    ->setShippingMethod('freeshipping_freeshipping');
                /**
                 * Add Payment Method | checkmo
                 */
                $quote->setPaymentMethod('cashondelivery');
                $quote->setInventoryProcessed(false);
                $quote->save();
                /**
                 * Set Sales Order Payment
                 */
                $quote->getPayment()->importData(['method' => 'cashondelivery']);
                $quote->collectTotals()->save();

                /**
                 *  Order Creation
                 */

                $order = $this->createQuoteOrder($quote);
                $order->setEmailSent(0);
                $order->getRealOrderId();
            } catch (Exception $e) {
                continue;
            }
        }
        $this->updateSkuOriginalPrice($skuOriginalPriceUpdateArray);
        return true;
    }

    /**
     * Place order for wal mart
     *
     * @param $csvData
     * @param $params
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function createOrderForWalMart($csvData, $params)
    {
        /*echo '<pre>';
        print_r($csvData);
        die;*/
        $this->createOrderBlock->getBackendSession()->setSource('backend');
        $skuOriginalPriceUpdateArray = [];
        $customerId = $params['customer_id'];
        /**
         * Store id is store code id
         */
        $storeCode = $params['store_code'];
        $store = $this->orderHelper->getStoreByCode($storeCode);
        foreach ($csvData as $partnerOrderId => $orderItemData) {
            try {
                $orderCollection = $this->orderHelper->getOrderCollectionByPartnerOrderId($partnerOrderId);
                /**
                 * check if order with partner id already created or not
                 */
                if ($orderCollection->count() > 0) {
                    continue;
                }
                $quote = $this->getCustomerQuoteForAmazonWalMart($customerId, $store);
                $customerSalesRep = $this->getCustomerSalesRep($customerId);

                foreach ($orderItemData as $item) {
                    $sku = $item['item']['sku'];
                    $inventoryLogicParams['pref_ship_method'] = $item['item']['pref_ship_method'];
                    $inventoryLogicParams['sku'] = $item['item']['sku'];
                    $inventoryLogicParams['zip_code'] = $item['address']['postcode'];
                    $sku = $item['item']['sku'];
                    $offerDetails = $this->offerLogic->getOfferDataForSku($inventoryLogicParams);
                    $vendorId = $vendorFarm = $deliveryDate = $shipMethod = $pickUpDate = $offerId = null;
                    if (isset($offerDetails['vendor_id'])) {
                        $vendorId = $offerDetails['vendor_id'];
                    }
                    if (isset($offerDetails['location_id'])) {
                        $vendorFarm = $offerDetails['location_id'];
                    }
                    if (isset($offerDetails['offer_id'])) {
                        $offerId = $offerDetails['offer_id'];
                    }
                    if (isset($offerDetails['pickup_date'])) {
                        $pickUpDate = $offerDetails['pickup_date'];
                    }
                    if (isset($offerDetails['delivery_date'])) {
                        $deliveryDate = $offerDetails['delivery_date'];
                    }
                    if (isset($offerDetails['shipping_method'])) {
                        $shipMethod = $offerDetails['shipping_method'];
                    }

                    $price = $item['item']['item_price'];
                    $qty = $item['item']['qty'];
                    $product = $this->_productRepository->get($sku);
                    $skuCurrentPrice = $product->getPrice();
                    if (!array_key_exists($sku, $skuOriginalPriceUpdateArray)) {
                        $skuOriginalPriceUpdateArray[$sku] = $skuCurrentPrice;
                    }
                    $product->setPrice($price)->save();
                    $quote->addProduct($product, (int)$qty);
                    $quote->setOrderType('');
                    $quote->setPartnerOrderId($partnerOrderId);
                    $quote->setStore($store);
                    $quote->save();
                    /**
                     * Save Quote
                     */
                    $quoteItem = $this->getLatestQuoteItem($quote);
                    $quoteItem->setQtyPerBox('1');
                    $quoteItem->setFarmPrice($price);
                    $quoteItem->setTotalPrice($price);
                    $quoteItem->setPoUnitPrice($price);
                    $quoteItem->setVendor($vendorId);
                    $quoteItem->setVendorFarm($vendorFarm);
                    $quoteItem->setShippingMethod($shipMethod);
                    $quoteItem->setDeliveryDate($deliveryDate);
                    $quoteItem->setPickupDate($pickUpDate);
                    $quoteItem->setTruckPickupDate($deliveryDate);
                    $quoteItem->setOfferAvailId($offerId);
                    $quoteItem->setPartnerPo($item['item']['partner_po']);
                    $quoteItem->setTrackId($item['item']['track_number']);
                    $quoteItem->setPromiseDeliveryDate($item['item']['promise_delivery_date']);
                    $quoteItem->setPromiseShipMethod($item['item']['pref_ship_method']);
                    $quoteItem->setCustomerSalesRep($customerSalesRep);
                    $quoteItem->save();
                }
                $quote = $this->getQuoteByQuoteId($quote->getId());
                $addressArray = $orderItemData[0]['address'];
                $quote->getBillingAddress()->addData($addressArray);
                $quote->getShippingAddress()->addData($addressArray);
                $quote->setCustomerSalesRep($customerSalesRep);
                /**
                 * Set Shipping Method | flatrate_flatrate
                 */
                $shippingAddress = $quote->getShippingAddress();
                $shippingAddress->setCollectShippingRates(true)
                    ->collectShippingRates()
                    ->setShippingMethod('freeshipping_freeshipping');
                /**
                 * Add Payment Method | checkmo
                 */
                $quote->setPaymentMethod('cashondelivery');
                $quote->setInventoryProcessed(false);
                $quote->save();
                /**
                 * Set Sales Order Payment
                 */
                $quote->getPayment()->importData(['method' => 'cashondelivery']);
                $quote->collectTotals()->save();

                /**
                 *  Order Creation
                 */
                $order = $this->createQuoteOrder($quote);
                $order->setEmailSent(0);
                $order->getRealOrderId();
                /*$order->setState("processing")->setStatus("processing");
                $order->save();*/

                /**
                 * Set Tax and Shipping Cost
                 */
                $shippingCost = $orderItemData[0]['order']['ship_price'];
                $tax = $orderItemData[0]['order']['tax'];

                $orderId = $order->getRealOrderId();
                $orderInfo = $this->orderModel->loadByIncrementId($orderId);
                $orderInfo->setShippingAmount($shippingCost);
                $orderInfo->setBaseShippingAmount($shippingCost);
                $orderInfo->setTaxAmount($tax);
                $orderInfo->setGrandTotal($orderInfo->getSubtotal() + $tax + $shippingCost);
                $orderInfo->setBaseGrandTotal($orderInfo->getSubtotal() + $tax + $shippingCost);
                $orderInfo->save();
            } catch (Exception $e) {
                continue;
            }

        }
        $this->updateSkuOriginalPrice($skuOriginalPriceUpdateArray);
        return true;
    }

    /**
     * Process File
     *
     * @param File $FILE File
     * @param array $params Params
     *
     * @return int
     * @throws LocalizedException
     * @throws Exception
     */
    public function process($FILE, $params)
    {
        $mimesArray = $this->getMimesTypes();
        if (!isset($FILE['tmp_name'])) {
            throw new LocalizedException(__('Invalid file upload attempt.'));
        } elseif (!in_array($FILE['type'], $mimesArray)) {
            throw new LocalizedException(
                __('Invalid file type. Only CSV files are allowed')
            );
        } else {
            return $this->createOrderForCsv($FILE, $params);
        }
    }

    /**
     * Place Order for Flordel Orders
     *
     * @param $orderFormat
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function createOrderForFlordelCsv($orderFormat)
    {
        return $this->place($orderFormat);
    }

    /**
     * Place Function
     *
     * @param $customerRawData
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function place($customerRawData)
    {
        $this->createOrderBlock->getBackendSession()->setSource('backend');
        $skuOriginalPriceUpdateArray = [];
        $currentStoreId = $this->getStore()->getId();
        foreach ($customerRawData as $customerId => $storeData) {
            foreach ($storeData as $storeId => $itemData) {
                $quote = $this->getCustomerQuoteForAmazonWalMart($customerId, $currentStoreId);
                $customerSalesRep = $this->getCustomerSalesRep($customerId);
                foreach ($itemData as $item) {
                    $sku = $item['sku'];
                    /*  Custom Price Calculation Qtp * Price Per Stem  */
                    $price = round(
                        $item['so_price'] * $item['qty_per_box'],
                        4
                    );
                    $product = $this->_productRepository->get($sku);
                    $skuCurrentPrice = $product->getPrice();
                    if (!array_key_exists($sku, $skuOriginalPriceUpdateArray)) {
                        $skuOriginalPriceUpdateArray[$sku] = $skuCurrentPrice;
                    }
                    $product->setPrice($price)->save();
                    $quote->addProduct($product, intval($item['qty']));
                    $quote->setOrderType($item['order_type']);
                    $quote->save();

                    /**
                     * $quote->save();
                     * $quote->collectTotals()->save();
                     */

                    /**
                     * Get Quote Item Object to update Product Custom Options
                     */
                    /*$quoteItem = $this->getCurrentQuoteItemByProduct(
                        $quote,
                        $product
                    );*/
                    $quoteItem = $this->getLatestQuoteItem($quote);

                    $quoteItem->setQtyPerBox($item['qty_per_box']);
                    $quoteItem->setFarmPrice($item['so_price']);
                    $quoteItem->setTotalPrice($item['so_price']);
                    $quoteItem->setPoUnitPrice($item['po_price']);
                    $quoteItem->setVendor($item['vendor']);
                    $quoteItem->setVendorFarm($item['vendor_farm']);
                    $quoteItem->setDeliveryDate($item['delivery_date']);
                    $quoteItem->setPickupDate($item['shipping_date']);
                    $quoteItem->setTruckPickupDate($item['trans_date']);
                    $quoteItem->setCustomerSalesRep($customerSalesRep);

                    $quoteItem->save();

                }

                /**
                 * Get Quote ID to update Shipping Address
                 */
                $quote = $this->getQuoteByQuoteId($quote->getId());

                $quote = $this->setShippingAddressToQuote($quote, $storeId);

                /**
                 * Add Customer Sales Rep
                 */
                $quote->setCustomerSalesRep($customerSalesRep);

                /**
                 * Set Shipping Method | flatrate_flatrate
                 */
                $shippingAddress = $quote->getShippingAddress();
                $shippingAddress->setCollectShippingRates(true)
                    ->collectShippingRates()
                    ->setShippingMethod('freeshipping_freeshipping');
                /**
                 * Add Payment Method | checkmo
                 */
                $quote->setPaymentMethod('cashondelivery');
                $quote->setInventoryProcessed(false);
                $quote->save();
                /**
                 * Set Sales Order Payment
                 */
                $quote->getPayment()->importData(['method' => 'cashondelivery']);
                $quote->collectTotals()->save();

                /**
                 *  Order Creation
                 */
                $order = $this->createQuoteOrder($quote);
                $order->setEmailSent(0);
                /**
                 * Update Partner order Id with Magento Order Id for Arabella orders
                 */
                $order->setData('partner_order_id', $order->getRealOrderId());
                $order->save();

            }

        }
        $this->updateSkuOriginalPrice($skuOriginalPriceUpdateArray);
        return true;
    }

    /**
     * Place Order For Create Order Dashboard
     * @param $customerData
     * @return bool
     * @throws LocalizedException
     * @throws Exception
     */
    public function placeOrderFromDashboard($customerData)
    {
        try {
            $this->createOrderBlock->getBackendSession()->setSource('backend');
            $customerId = $customerData['customer_id'];
            $customer = $this->customerHelper->getCustomerById($customerId);
            $storeId = $customerData['store_id'];
            $store = $this->orderHelper->getStoreById($storeId);
            $shippingAddressId = $customerData['shipping_address_id'];
            $billingAddressId = $customer->getDefaultBilling();
            if ($billingAddressId == null) {
                throw new LocalizedException(__('This customer dont have any billing address'));
            }
            $shippingMethod = $customerData['shipping_name'];
            $pickupDate = $customerData['pickup_date'];
            $deliveryDate = $customerData['delivery_date'];
            $quote = $this->getCustomerQuoteForAmazonWalMart($customerId, $store);
            $customerSalesRep = $this->getCustomerSalesRep($customerId);

            $items = $customerData['items'];
            foreach ($items as $oneItem) {
                $sku = $oneItem['sku'];
                $vendorArray = explode('-', $oneItem['vendor_id']);
                $vendorId = $vendorArray[0];
                $locationId = $vendorArray[1];
                $qty = $oneItem['qty'];
                $qtyPerBox = $oneItem['box_count'];
                $po_price = $oneItem['po_price'];
                $price = $oneItem['price'];

                $product = $this->_productRepository->get($sku);
                $quote->addProduct($product, intval($qty));
                $quote->setOrderType('Regular');
                $quote->save();
                $quoteItem = $this->getLatestQuoteItem($quote);
                $quoteItem->setCustomPrice($price);
                $quoteItem->setOriginalCustomPrice($price);
                $quoteItem->getProduct()->setIsSuperMode(true);
                $quoteItem->setQtyPerBox($qtyPerBox);
                $quoteItem->setFarmPrice($price);
                $quoteItem->setTotalPrice($price);
                $quoteItem->setPoUnitPrice($po_price);
                $quoteItem->setVendor($vendorId);
                $quoteItem->setVendorFarm($locationId);
                $quoteItem->setDeliveryDate($deliveryDate);
                $quoteItem->setPickupDate($pickupDate);
                $quoteItem->setTruckPickupDate($pickupDate);
                $quoteItem->setCustomerSalesRep($customerSalesRep);
                $quoteItem->setShippingMethod($shippingMethod);
                $quoteItem->save();
            }
            /**
             * Get Quote ID to update Shipping Address
             */
            $quote = $this->getQuoteByQuoteId($quote->getId());
            $quote = $this->setShippingAddressToQuote($quote, $shippingAddressId);
            $quote = $this->setBillingAddressToQuote($quote, $billingAddressId);
            $quote->setCustomerSalesRep($customerSalesRep);
            $shippingAddress = $quote->getShippingAddress();
            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates()
                ->setShippingMethod('freeshipping_freeshipping');
            $quote->setPaymentMethod('cashondelivery');
            $quote->setInventoryProcessed(false);
            $quote->save();
            $quote->getPayment()->importData(['method' => 'cashondelivery']);
            $quote->collectTotals()->save();
            $order = $this->createQuoteOrder($quote);
            $order->setEmailSent(0);
            $order->setData('partner_order_id', $order->getRealOrderId());
            $order->save();
            return true;
        } catch (Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
    }

    /**
     * Update Product Old prices
     *
     * @param $skuOriginalPriceUpdateArray
     * @return bool
     * @throws NoSuchEntityException
     */
    public function updateSkuOriginalPrice($skuOriginalPriceUpdateArray)
    {
        /**
         * Set SKU Old price
         */
        if (count($skuOriginalPriceUpdateArray) > 0) {
            foreach ($skuOriginalPriceUpdateArray as $sku => $oldPrice) {
                $product = $this->_productRepository->get($sku);
                $product->setPrice($oldPrice)->save();
            }
        }
        return true;
    }

    /**
     * Return Order Data
     * This will create order with template store Id and Customer ID
     *
     * @param File $file File
     * @param array $params Params
     *
     * @return array|String
     * @throws Exception
     */
    public function createOrderForCsv($file, $params)
    {
        $customerRawData = $this->processCSV($file);
        $this->place($customerRawData);
        return true;
    }

    /**
     * Return Customer Quote
     *
     * @param integer $customerId Customer
     *
     * @return CartInterface|Quote
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerQuote($customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        $quote = $this->quote->create()->loadByCustomer($customerId);
        if (!$quote->getId()) {
            $quote = $this->quote->create()
                ->setStoreId($this->getStore()->getId())
                ->assignCustomer($customer);
        }
        return $quote;
    }

    /**
     * Quote for Amazon and walmart
     *
     * @param $customerId
     * @param $store
     * @return CartInterface|Quote
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerQuoteForAmazonWalMart($customerId, $store)
    {
        $customer = $this->customerRepository->getById($customerId);
        try {
            $quote = $this->cartRepository->getActiveForCustomer($customer->getId());
        } catch (NoSuchEntityException $e) {
            $customer = $this->customerRepository->getById($customer->getId());
            $quote = $this->quote->create();
            $quote->setStoreId($store->getId());
            $quote->setCustomer($customer);
            $quote->setCustomerIsGuest(0);
        }
        try {
            $this->cartRepository->save($quote);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__("The quote can't be created."));
        }
        /*$customer = $this->customerRepository->getById($customerId);
        $quote = $this->quote->create()->loadByCustomer($customerId);
        if (!$quote->getId()) {
            $quote = $this->quote->create();
            $quote->setStore($store);
            $quote->setCurrency();
            $quote->assignCustomer($customer);
        }*/
        return $quote;
    }

    /**
     * Return Sales Rep Associate with customer
     *
     * @param integer $customerId Customer ID
     *
     * @return mixed
     */
    public function getCustomerSalesRep($customerId)
    {
        return $this->customerHelper->getCustomerSalesRep($customerId);
    }

    /**
     * @param $quote
     * @param $shippingAddressID
     * @return mixed
     * @throws LocalizedException
     */
    public function setShippingAddressToQuote($quote, $shippingAddressID)
    {
        /**
         * Add Shipping Address from Shipping Address ID (store ID)
         */
        $addressObject = $this->addressRepository->getById($shippingAddressID);
        $quote->getShippingAddress()->setCustomerAddressId(
            $addressObject->getId()
        );
        $quote->getShippingAddress()->setPrefix($addressObject->getPrefix());
        $quote->getShippingAddress()->setFirstname(
            $addressObject->getFirstname()
        );
        $quote->getShippingAddress()->setMiddlename(
            $addressObject->getMiddlename()
        );
        $quote->getShippingAddress()->setLastname(
            $addressObject->getLastname()
        );
        $quote->getShippingAddress()->setSuffix(
            $addressObject->getSuffix()
        );
        $quote->getShippingAddress()->setCompany(
            $addressObject->getCompany()
        );
        $quote->getShippingAddress()->setStreet(
            $addressObject->getStreet()
        );
        $quote->getShippingAddress()->setCity(
            $addressObject->getCity()
        );
        $quote->getShippingAddress()->setCountryId(
            $addressObject->getCountryId()
        );
        $quote->getShippingAddress()->setRegion(
            $addressObject->getRegion()
        );
        $quote->getShippingAddress()->setRegionId(
            $addressObject->getRegionId()
        );
        $quote->getShippingAddress()->setPostcode(
            $addressObject->getPostcode()
        );
        $quote->getShippingAddress()->setTelephone(
            $addressObject->getTelephone()
        );
        $quote->getShippingAddress()->setFax(
            $addressObject->getFax()
        );

        return $quote;
    }

    /**
     * @param Quote $quote
     * @param $billingAddressID
     * @return Quote
     * @throws LocalizedException
     */
    public function setBillingAddressToQuote(Quote $quote, $billingAddressID)
    {
        $addressObject = $this->addressRepository->getById($billingAddressID);
        $quote->getBillingAddress()->setPrefix($addressObject->getPrefix());
        $quote->getBillingAddress()->setFirstname($addressObject->getFirstname());
        $quote->getBillingAddress()->setMiddlename($addressObject->getMiddlename());
        $quote->getBillingAddress()->setLastname($addressObject->getLastname());
        $quote->getBillingAddress()->setSuffix($addressObject->getSuffix());
        $quote->getBillingAddress()->setCompany($addressObject->getCompany());
        $quote->getBillingAddress()->setStreet($addressObject->getStreet());
        $quote->getBillingAddress()->setCity($addressObject->getCity());
        $quote->getBillingAddress()->setCountryId($addressObject->getCountryId());
        $quote->getBillingAddress()->setRegion($addressObject->getRegion());
        $quote->getBillingAddress()->setRegionId($addressObject->getRegionId());
        $quote->getBillingAddress()->setPostcode($addressObject->getPostcode());
        $quote->getBillingAddress()->setTelephone($addressObject->getTelephone());
        $quote->getBillingAddress()->setFax($addressObject->getFax());
        return $quote;
    }

    /**
     * Return Quote Object By Quote ID
     *
     * @param integer $quoteId Quote
     *
     * @return CartInterface|Quote
     * @throws NoSuchEntityException
     */
    public function getQuoteByQuoteId($quoteId)
    {
        return $this->quoteRepository->get($quoteId);
    }

    /**
     * Return Latest Quote Item Object By id
     * @param $quote
     *
     * @return mixed
     */
    public function getLatestQuoteItem($quote)
    {
        $items = $quote->getAllItems();
        $maxId = 0;
        foreach ($items as $item) {
            if ($item->getId() > $maxId) {
                $maxId = $item->getId();
            }
        }
        $lastItemId = $maxId;
        return $quote->getItemById($lastItemId);
    }

    /**
     * Return Current Quote Item Object By Product
     *
     * @param object $quote Quote
     * @param object $product Product Object
     *
     * @return mixed|object
     */
    public function getCurrentQuoteItemByProduct($quote, $product)
    {

        return $quote->getItemByProduct($product);
    }

    /**
     * Create Order For Quote
     *
     * @param object $quote Quote
     *
     * @return mixed|object
     * @throws LocalizedException
     */
    public function createQuoteOrder($quote)
    {
        return $this->orderManagement->submit($quote);
    }

    /**
     * File Types
     *
     * @return array
     */
    public function getMimesTypes()
    {
        return [
            'application/vnd.ms-excel',
            'text/csv',
            'text/tsv'
        ];
    }

    /**
     * Returns CSV Array by customer wise
     *
     * @param File $file File
     *
     * @return array
     * @throws Exception
     */
    public function processCSV($file)
    {
        $rawData = $this->_csv->getData($file['tmp_name']);
        $headers = $this->getHeaders();
        $processedData = [];
        $count = 1;
        foreach ($rawData as $row) {
            if ($count != 1) {
                $customerId = $row[0];
                $storeId = $row[1];
                $processedData[$customerId][$storeId][] = array_combine(
                    $headers, $row
                );
            } else {
                $count++;
            }

        }
        return $processedData;
    }

    /**
     * Return Header Fields
     *
     * @return array
     */
    public function getHeaders()
    {
        return [
            'customer_id',
            'store_id',
            'sku',
            'qty_per_box',
            'qty',
            'so_price',
            'po_price',
            'vendor',
            'delivery_date',
            'shipping_date',
            'trans_date',
            'order_type'
        ];
    }

    /**
     * Return Store Object
     *
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }

    /**
     * Return Website Id
     *
     * @return int
     * @throws NoSuchEntityException
     */
    public function getWebSiteId()
    {
        return $this->storeManager->getStore()->getWebsiteId();
    }

}