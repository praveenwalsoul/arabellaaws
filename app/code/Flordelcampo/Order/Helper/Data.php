<?php

namespace Flordelcampo\Order\Helper;

use Arabella\Prescription\Model\PrescriptionFactory;
use Exception;
use Flordelcampo\Order\Model\Box;
use Magento\Customer\Model\Customer;
use Magento\Directory\Model\Country;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject;
use Magento\Framework\DB\Transaction;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\ResourceModel\Order;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Flordelcampo\Order\Model\BoxFactory;
use Magento\Sales\Model\Order\ItemFactory;
use Magento\Store\Api\StoreWebsiteRelationInterface;
use Flordelcampo\VendorInventory\Model\HardGoodOrderItemFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Model\Order as OrderModel;

/**
 * Class Data
 * Flordelcampo\Order\Helper
 */
class Data extends AbstractHelper
{
    const DEFAULT_ORDER_ITEM_STATUS = \Flordelcampo\Order\Model\ItemStatus::UNDER_PROCESSING;
    /**
     * Wal Mart Store Code
     * @var string
     */
    private $_walMartStoreCode = 'walmart';
    /**
     * Amazon Store Code
     * @var string
     */
    private $_amazonStoreCode = 'amazon';
    /**
     * Customer Object
     *
     * @var Customer
     */
    protected $customer;
    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var CollectionFactory
     */
    protected $orderCollectionFactory;
    /**
     * @var RegionFactory
     */
    protected $regionColFactory;
    /**
     * @var Order
     */
    protected $orderResourceModel;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;
    /**
     * @var BoxFactory
     */
    protected $boxFactory;
    /**
     * @var ItemFactory
     */
    protected $itemFactory;
    /**
     * @var StoreWebsiteRelationInterface
     */
    protected $storeWebsiteRelation;
    /**
     * @var HardGoodOrderItemFactory
     */
    protected $hardGoodOrderItemFactory;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var InvoiceService
     */
    protected $invoiceService;
    /**
     * @var Transaction
     */
    protected $transaction;
    /**
     * @var OrderModel
     */
    protected $orderModel;
    /**
     * @var PrescriptionFactory
     */
    protected $prescriptionFactory;

    /**
     * Data constructor.
     *
     * @param Context $context Context
     * @param Customer $customer Customer
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $orderCollectionFactory
     * @param RegionFactory $regionColFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param Order $orderResourceModel
     * @param BoxFactory $boxFactory
     * @param ItemFactory $itemFactory
     * @param StoreWebsiteRelationInterface $storeWebsiteRelation
     * @param HardGoodOrderItemFactory $hardGoodOrderItemFactory
     * @param ResourceConnection $resourceConnection
     * @param InvoiceService $invoiceService
     * @param Transaction $transaction
     * @param OrderModel $orderModel
     * @param PrescriptionFactory $prescriptionFactory
     */
    public function __construct(
        Context $context,
        Customer $customer,
        StoreRepositoryInterface $storeRepository,
        StoreManagerInterface $storeManager,
        CollectionFactory $orderCollectionFactory,
        RegionFactory $regionColFactory,
        OrderRepositoryInterface $orderRepository,
        Order $orderResourceModel,
        BoxFactory $boxFactory,
        ItemFactory $itemFactory,
        StoreWebsiteRelationInterface $storeWebsiteRelation,
        HardGoodOrderItemFactory $hardGoodOrderItemFactory,
        ResourceConnection $resourceConnection,
        InvoiceService $invoiceService,
        Transaction $transaction,
        OrderModel $orderModel,
        PrescriptionFactory $prescriptionFactory
    )
    {
        $this->orderResourceModel = $orderResourceModel;
        $this->orderRepository = $orderRepository;
        $this->customer = $customer;
        $this->storeRepository = $storeRepository;
        $this->storeManager = $storeManager;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->regionColFactory = $regionColFactory;
        $this->boxFactory = $boxFactory;
        $this->itemFactory = $itemFactory;
        $this->storeWebsiteRelation = $storeWebsiteRelation;
        $this->hardGoodOrderItemFactory = $hardGoodOrderItemFactory;
        $this->resourceConnection = $resourceConnection;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
        $this->orderModel = $orderModel;
        $this->prescriptionFactory = $prescriptionFactory;
        parent::__construct($context);
    }

    /**
     * Return Invoice Service
     * @return InvoiceService
     */
    public function getInvoiceService()
    {
        return $this->invoiceService;
    }

    /**
     * Return Transaction Service
     * @return Transaction
     */
    public function getTransactionService()
    {
        return $this->transaction;
    }

    /**
     * Return Customer Address Data
     *
     * @param integer $customerId Customer Id
     *
     * @return DataObject[]
     */
    public function getCustomerAddress($customerId)
    {
        $customerModel = $this->customer->load($customerId);
        return $customerModel->getAddresses();
    }

    /**
     * Return All stores
     *
     * @return StoreInterface[]
     */
    public function getAllStoreList()
    {
        return $this->storeRepository->getList();
    }

    /**
     * Return Store Id and array
     *
     * @return array
     */
    public function getStoreListArray()
    {
        $storesArray = [];
        $storesList = $this->getAllStoreList();
        foreach ($storesList as $store) {
            if ($store->getIsActive()) {
                $storesArray[$store->getId()] = $store->getCode();
            }
        }
        return $storesArray;
    }

    /**
     * Return Store Name list
     * @return array
     */
    public function getAllStoresListWithNameArray()
    {
        $storesArray = [];
        $storesList = $this->getAllStoreList();
        foreach ($storesList as $store) {
            if ($store->getIsActive()) {
                $storesArray[$store->getId()] = $store->getName();
            }
        }
        return $storesArray;
    }

    /**
     * Return ALl website List
     * @return array
     */
    public function getAllWebsiteListWithNameArray()
    {
        $websiteList = [];
        foreach ($this->storeManager->getWebsites() as $website) {
            $websiteList[$website->getId()] = $website->getName();
        }
        return $websiteList;
    }

    /**
     * @param $websiteId
     * @return array
     * @throws LocalizedException
     */
    public function getAllStoreIdsForWebsiteId($websiteId)
    {
        $websiteStoreIds = [];
        try {
            if ($websiteId == null) {
                /**
                 * Send all store Ids
                 */
                $allWebsiteList = $this->getAllWebsiteListWithNameArray();
                foreach ($allWebsiteList as $websiteId => $name) {
                    $websiteStoreIds[$websiteId] = $this->storeWebsiteRelation->getStoreByWebsiteId($websiteId);
                }
            }
            $storeId = $this->storeWebsiteRelation->getStoreByWebsiteId($websiteId);
            $websiteStoreIds[$websiteId] = $storeId;
        } catch (Exception $exception) {
            throw new LocalizedException(__('Invalid website Id'));
        }
        return $websiteStoreIds;
    }

    /**
     * Return Store Label Code Array
     * @return array
     */
    public function getStoreLabelCodeArray()
    {
        /*$storesArray[''] = 'Please Select';*/
        $storesArray = [];
        $storesList = $this->getAllStoreList();
        foreach ($storesList as $store) {
            if ($store->getIsActive()) {
                $storesArray[$store->getCode()] = $store->getName();
            }
        }
        return $storesArray;
    }

    /**
     * Get Store By Store Id
     * @param $Id
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getStoreById($Id)
    {
        return $this->storeRepository->getById($Id);
    }

    /**
     * Return Store By Store Code
     * @param $code
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getStoreByCode($code)
    {
        return $this->storeRepository->get($code);
    }

    /**
     * Return Amazon Store
     *
     * @return StoreInterface|string
     */
    public function getAmazonStore()
    {
        try {
            return $this->getStoreByCode($this->_amazonStoreCode);
        } catch (Exception $e) {
            return '1';
        }
    }

    /**
     * Return Wal Mart Store
     *
     * @return StoreInterface|string
     */
    public function getWalMartStore()
    {
        try {
            return $this->getStoreByCode($this->_walMartStoreCode);
        } catch (Exception $e) {
            return '1';
        }
    }

    /**
     * Return Order Model for CRUD operation
     * @param $orderId
     * @return OrderInterface
     */
    public function getOrderModelById($orderId)
    {
        return $this->orderRepository->get($orderId);
    }

    /**
     * Return Order By Increment Id
     * @param $incrementId
     * @return OrderModel
     */
    public function getOrderByIncrementId($incrementId)
    {
        return $this->orderModel->loadByIncrementId($incrementId);
    }

    /**
     * Order Repository
     * @param $orderId
     * @return OrderInterface
     */
    public function getOrderRepositoryById($orderId)
    {
        return $this->orderRepository->get($orderId);
    }

    /**
     * Return Order Repository By Id
     * @param $incrementId
     * @return OrderInterface
     */
    public function getOrderRepositoryByIncrementId($incrementId)
    {
        $orderModel = $this->orderModel->loadByIncrementId($incrementId);
        return $this->getOrderRepositoryById($orderModel->getId());
    }

    /**
     * Return box model by box Id
     * @param $boxId
     * @return Box|null
     */
    public function getBoxModelByBoxId($boxId)
    {
        $boxModel = $this->boxFactory->create()->load($boxId, 'box_id');
        if (isset($boxModel) && $boxModel->getBoxId()) {
            return $boxModel;
        } else {
            return null;
        }
    }

    /**
     * Return Box Collection
     * @param $boxId
     * @return AbstractDb|AbstractCollection|null
     */
    public function getBoxCollectionByIds($boxId)
    {
        $collection = $this->boxFactory->create()->getCollection();
        $collection->addFieldToFilter('main_table.box_id', ['in' => $boxId]);
        return $collection;
    }

    /**
     * Update Status for Box , Item , Order
     * @param $type
     * @param $id
     * @param $statusToUpdate
     * @return bool
     * @throws LocalizedException
     * @throws Exception
     */
    public function updateStatusByIdAndType($type, $id, $statusToUpdate)
    {
        switch ($type) {
            case 'box':
                $boxModel = $this->boxFactory->create()->load($id, 'box_id');
                if ($boxModel != null) {
                    switch ($statusToUpdate) {
                        case 'cancelled':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::CANCELLED);
                            $boxModel->save();
                            break;
                        case 'printed':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::PRINTED);
                            $boxModel->save();
                            break;
                        case 'shipped':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::SHIPPED);
                            $boxModel->save();
                            break;
                        case 'pending':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::PENDING);
                            $boxModel->save();
                            break;
                        case 'vendorPrint':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::VENDOR_PRINT);
                            $boxModel->save();
                            break;
                        case 'rejected':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::REJECTED);
                            $boxModel->save();
                            break;
                        case 'invoiced':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::INVOICED);
                            $boxModel->save();
                            break;
                        case 'delivered':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::DELIVERED);
                            $boxModel->save();
                            break;
                        case 'underReview':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::UNDER_REVIEW);
                            $boxModel->save();
                            break;
                        case 'refunded':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::REFUNDED);
                            $boxModel->save();
                            break;
                        case 'processing':
                            $boxModel->setData('status', \Flordelcampo\Order\Model\Box::PROCESSING);
                            $boxModel->save();
                            break;
                        default:
                            throw new LocalizedException(__("Invalid Box Status"));
                    }
                    $this->syncStatusByBoxId($id);
                }
                break;
            case 'item':
                //TODO update item status
                break;
            case 'order':
                //TODO update order status
                break;
            default:
                return true;
        }
        return true;
    }

    /**
     * @param $boxId
     * @return bool
     */
    public function syncStatusByBoxId($boxId)
    {
        $boxModel = $this->getBoxModelByBoxId($boxId);
        $orderId = $boxModel->getData('order_id');
        $this->syncOrderItemBoxStatusByOrderId($orderId);
        return true;
    }

    /**
     * Sync All Status
     * @param $orderId
     * @return bool
     */
    public function syncOrderItemBoxStatusByOrderId($orderId)
    {
        try {
            $collection = $this->getOrderCollectionById($orderId);
            foreach ($collection as $order) {
                $globalUniqueStatusToUpdate = self::DEFAULT_ORDER_ITEM_STATUS;
                $itemStatusArray = [];
                foreach ($order->getItems() as $item) {
                    $boxCollection = $this->getBoxCollectionByItemId($item->getItemId());
                    $boxStatusArray = [];
                    if ($boxCollection != null) {
                        foreach ($boxCollection as $box) {
                            $boxStatusArray[] = $box->getData('status');
                        }
                        $uniqueBoxStatusArray = array_unique($boxStatusArray);
                        if (count($uniqueBoxStatusArray) == 1) {
                            $globalUniqueStatusToUpdate = $uniqueBoxStatusArray[0];
                        }
                        $item->setData('order_item_status', $globalUniqueStatusToUpdate);
                        $item->save();
                    }
                    $itemStatusArray[] = $item->getData('order_item_status');
                }
                $uniqueItemStatusArray = array_unique($itemStatusArray);
                if (count($uniqueItemStatusArray) == 1) {
                    $globalUniqueStatusToUpdate = $uniqueItemStatusArray[0];
                }
                if ($globalUniqueStatusToUpdate == 'cancelled') {
                    $globalUniqueStatusToUpdate = 'canceled';
                }
                $order->setState($globalUniqueStatusToUpdate)->setStatus($globalUniqueStatusToUpdate);
                $order->save();
            }
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * Return Box Collection By Item Id
     * @param $itemId
     * @param false $status
     * @return AbstractDb|AbstractCollection|null
     */
    public function getBoxCollectionByItemId($itemId, $status = false)
    {
        $boxModel = $this->boxFactory->create();
        $boxModelCollection = $boxModel->getCollection();
        if (is_array($itemId)) {
            $boxModelCollection->addFieldToFilter('main_table.item_id', ['in' => $itemId]);
        } else {
            $boxModelCollection->addFieldToFilter('main_table.item_id', $itemId);
        }
        if (!empty($status)) {
            $boxModelCollection->addFieldToFilter('main_table.status', $status);
        }
        if (count($boxModelCollection) > 0) {
            return $boxModelCollection;
        } else {
            return null;
        }
    }

    /**
     * Return box data by order id and status
     * @param $orderId
     * @param bool $status
     * @return AbstractDb|AbstractCollection|null |null
     */
    public function getBoxCollectionByOrderId($orderId, $status = false)
    {
        $boxModel = $this->boxFactory->create();
        $boxModelCollection = $boxModel->getCollection();
        $boxModelCollection->addFieldToFilter('order_id', $orderId);
        if (!empty($status)) {
            $boxModelCollection->addFieldToFilter('status', $status);
        }
        if (count($boxModelCollection) > 0) {
            return $boxModelCollection;
        } else {
            return null;
        }
    }

    /**
     * All order collection
     *
     * @return Collection
     */
    public function getOrderCollection()
    {
        return $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');
    }

    /**
     * Return Partner order Id collection
     *
     * @param $partnerOrderId
     * @return Collection
     */
    public function getOrderCollectionByPartnerOrderId($partnerOrderId)
    {
        return $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('partner_order_id', $partnerOrderId);
    }

    /**
     * Order Collection
     * @param $orderIds
     * @return Collection
     */
    public function getOrderCollectionByIds($orderIds)
    {
        return $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', ['in' => $orderIds]);
    }

    /**
     * Return order collection by id
     * @param $orderId
     * @return Collection
     */
    public function getOrderCollectionById($orderId)
    {
        return $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', $orderId);
    }

    /**
     * Return All Us States or Regions
     * @return array
     */
    public function getAllUsRegionsByNameWise()
    {
        $regions = $this->regionColFactory->create()
            ->getCollection()
            ->addFieldToFilter('country_id', 'US');
        $regionsArray = [];
        foreach ($regions as $state) {
            $stateName = strtoupper($state->getName());
            $regionsArray[$stateName] = $state->getCode();
        }
        return $regionsArray;
    }

    /**
     * Return All Us States or Regions
     * @return array
     */
    public function getAllUsRegionsByCodeWise()
    {
        $regions = $this->regionColFactory->create()
            ->getCollection()
            ->addFieldToFilter('country_id', 'US');
        $regionsArray = [];
        foreach ($regions as $state) {
            $regionsArray[$state->getCode()] = $state->getName();
        }
        return $regionsArray;
    }

    /**
     * Return Processing order collection
     *
     * @return Collection
     */
    public function getProcessingOrderCollection()
    {
        $status = ['processing'];
        return $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('status', ['in' => $status])
            ->setOrder('ups_address_status', 'ASC');
    }

    /**
     * Return Order collection by params
     *
     * @param $params
     * @return Collection
     */
    public function getOrderCollectionByParams($params)
    {
        return $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter($params['code'], $params['value']);
    }

    /**
     * @param null $params
     * @return AbstractDb|AbstractCollection|null
     */
    public function getOrderItemCollection($params = null)
    {
        $collection = $this->itemFactory->create()->getCollection();
        if ($params != null) {
            if (isset($params['partner_po'])) {
                $collection->addFieldToFilter('partner_po', $params['partner_po']);
            }
        }
        return $collection;
    }

    /**
     * @param $itemId
     * @return Item
     */
    public function getOrderItemsModelByItemId($itemId)
    {
        return $this->itemFactory->create()->load($itemId);
    }

    /**
     * Return Order Address Street Array By order Id
     * @param $orderId
     * @return array
     */
    public function getOrderStreetAddressArrayByOrderId($orderId)
    {
        $order = $this->getOrderModelById($orderId);
        $shippingAddress = $order->getShippingAddress();
        return $shippingAddress->getStreet();
    }

    /**
     * @return mixed
     */
    public function getHardGoodItemModel()
    {
        return $this->hardGoodOrderItemFactory->create();
    }

    /**
     * Return Collection
     * @return AbstractDb|AbstractCollection|null
     */
    public function getHardGoodItemCollection()
    {
        return $this->hardGoodOrderItemFactory->create()->getCollection();
    }

    /**
     * @param $Id
     * @return mixed
     */
    public function getHardGoodItemModelById($Id)
    {
        return $this->hardGoodOrderItemFactory->create()->load($Id);
    }

    /**
     * @param null $params
     * @return AbstractDb|AbstractCollection|null
     */
    /*public function getHardGoodItemCollection($params = null)
    {
        $collection = $this->hardGoodOrderItemFactory->create()->getCollection();
        if ($params != null) {
            if (isset($params['item_id'])) {
                $collection->addFieldToFilter('item_id', $params['item_id']);
            }
        }
        return $collection;
    }*/

    /**
     * @param $id
     * @return Country
     */
    public function getPrescriptionModelById($id)
    {
        return $this->prescriptionFactory->create()->load($id);
    }

    /**
     * Return Prescription Url by Id
     * @param $prescriptionId
     * @return array
     */
    public function getPrescriptionUrlById($prescriptionId)
    {
        $prescriptionUrlArray = [];
        try {
            if ($prescriptionId == null) {
                return $prescriptionUrlArray;
            }
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from(
                    'prescriptions',
                    ['id', 'filename']
                );
            if (is_array($prescriptionId)) {
                $prescriptionArray = "'" . implode("', '", $prescriptionId) . "'";
                $select->where("prescriptions.id  IN (" . $prescriptionArray . ")");
            } else {
                $select->where('prescriptions.id = ?', $prescriptionId);
            }
            $prescriptionArray = $connection->fetchAll($select);
            if (count($prescriptionArray) > 0) {
                $fileMediaUrl = $this->getPrescriptionMediaUrl();
                foreach ($prescriptionArray as $one) {
                    $prescriptionUrlArray[$one['id']] = $fileMediaUrl . $one['filename'];
                }
            }
            return $prescriptionUrlArray;
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Return Media URL
     * @return mixed
     */
    public function getMediaUrl()
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $_objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $currentStore = $storeManager->getStore();
        return $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * Return URL
     * @return string
     */
    public function getPrescriptionMediaUrl()
    {
        return $this->getMediaUrl() . 'prescriptions/';
    }

    /**
     * Return Var Folder Path
     * @return mixed
     */
    public function getVarFolderPath()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        return $directory->getPath('var');
    }

    /**
     * @return string
     */
    public function getInvoicePdfPath()
    {
        return $this->getMediaUrl() . 'pdf/invoice/';
    }
}
