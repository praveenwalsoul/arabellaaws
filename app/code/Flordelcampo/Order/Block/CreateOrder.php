<?php

namespace Flordelcampo\Order\Block;

use http\Exception;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Flordelcampo\Customer\Helper\Data;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Backend\Model\Session;


/**
 * Class CreateOrder
 *
 * @package Flordelcampo\Orde\Block
 */
class CreateOrder extends Template
{
    /**
     * Helper
     *
     * @var Data
     */
    protected $customerHelper;
    /**
     * Store Manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var null
     */
    public $csvResponse = null;
    /**
     * @var Session
     */
    protected $adminSession;

    /**
     * CreateOrder constructor
     *
     * @param Context $context Context
     * @param Data $customerHelper Helper
     * @param StoreManagerInterface $storeManager
     * @param Session $adminSession
     */
    public function __construct(
        Context $context,
        Data $customerHelper,
        StoreManagerInterface $storeManager,
        Session $adminSession
    ) {
        $this->customerHelper = $customerHelper;
        $this->storeManager = $storeManager;
        $this->adminSession = $adminSession;
        parent::__construct($context);
    }

    /**
     * Returns Cusomters
     *
     * @return array
     */
    public function getCustomerData()
    {
        $customerData = [];
        try {
            $collection = $this->customerHelper->getCustomerCollection();
            /*$customerData = $collection->toArray();*/
            /*echo '<pre>';print_r($customerData);die;*/
            foreach ($collection as $customer) {
                $customerData[] = [
                    'id' => $customer->getId(),
                    'name' => $customer->getName()
                ];
            }
            array_multisort(
                array_column($customerData, 'name'),
                SORT_ASC, $customerData
            );
        } catch (Exception $e) {
            $customerData = [];
        }
        return $customerData;
    }

    /**
     * Return Address URL
     *
     * @return string
     */
    public function getCustomerCompanyDataUrl()
    {
        return $this->getUrl('create/createorder/address');
    }

    /**
     * Return Check Data URL
     *
     * @return string
     */
    public function getCreateOrderUrl()
    {
        return $this->getUrl('create/createorder/create');
    }

    /**
     * Return Place Order URL for Table
     * @return string
     */
    public function getPlaceOrderForTableItemsUrl()
    {
        return $this->getUrl('create/createorder/placeorder');
    }

    /**
     * Return Check Controller URL
     *
     * @return string
     */
    public function getCheckDataUrl()
    {
        return $this->getUrl('create/createorder/check');
    }

    /**
     * Return CSV Read URL
     *
     * @return string
     */
    public function getFileReaderUrl()
    {
        return $this->getUrl('create/createorder/reader');
    }

    /**
     * Return Downloadable Template URL
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getOrderTemplateURL()
    {
        return $this->getMediaUrl() . 'Order/order_template.csv';
    }

    /**
     * Return SKU Template URL
     * @return string
     * @throws NoSuchEntityException
     */
    public function getSkuTemplateUrl()
    {
        return $this->getMediaUrl() . 'Order/sku_template.csv';
    }

    /**
     * Return Media URL
     *
     * @return mixed
     *
     * @throws NoSuchEntityException
     */
    public function getMediaUrl()
    {
        return $this->storeManager->getStore()
            ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * File Types
     *
     * @return array
     */
    public function getMimesTypes()
    {
        return [
            'application/vnd.ms-excel',
            'text/csv',
            'text/tsv'
        ];
    }

    /**
     * Return Admin Session
     *
     * @return Session
     */
    public function getBackendSession()
    {
        return $this->adminSession;
    }

}