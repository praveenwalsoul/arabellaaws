<?php

namespace Flordelcampo\Order\Block;

use Magento\Backend\Block\Template;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Backend\Block\Template\Context;
use Flordelcampo\Customer\Helper\Data;
use Flordelcampo\Order\Helper\Data as OrderHelper;
use Magento\Store\Api\Data\StoreInterface;

/**
 * Class Wal mart
 * Flordelcampo\Order\Block
 */
class Walmart extends Template
{
    /**
     * Wal Mart Customer
     * @var string
     */
    private $_walMartCustomerEmail = "walmart@arabellabouquets.com";
    /**
     * @var Data
     */
    protected $customerHelper;
    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * Walmart constructor.
     *
     * @param Context $context
     * @param Data $customerHelper
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        Context $context,
        Data $customerHelper,
        OrderHelper $orderHelper
    ) {
        $this->customerHelper = $customerHelper;
        $this->orderHelper = $orderHelper;
        parent::__construct($context);
    }

    /**
     * Return Check Data URL
     *
     * @return string
     */
    public function getWalMartCreateUrl()
    {
        return $this->getUrl('create/walmart/create');
    }

    /**
     * Return Wal Mart Customer
     * @return Customer
     */
    public function getWalMartCustomerId()
    {
        return $this->customerHelper->getCustomerByEmail($this->_walMartCustomerEmail)->getId();
    }

    /**
     * Return Wal Mart Store
     *
     * @return StoreInterface|string
     */
    public function getWalMartStoreCode()
    {
        return $this->orderHelper->getWalMartStore()->getCode();
    }

    /**
     * Return Wal mart Store ID
     * @return int
     */
    public function getWalMartStoreId()
    {
        return $this->orderHelper->getWalMartStore()->getId();
    }
}