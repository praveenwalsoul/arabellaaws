<?php

namespace Flordelcampo\Order\Setup\Patch\Data;

use Exception;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Status;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusResource;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;

/**
 * Class AddOrderStatus
 * Flordelcampo\Order\Setup\Patch\Data
 */
class AddOrderStatus implements DataPatchInterface
{

    const ORDER_STATUS_CUSTOM_READY_CODE = 'shipped';

    const ORDER_STATUS_CUSTOM_READY_LABEL = 'Shipped';

    const ORDER_STATE_CUSTOM_READY_CODE = 'shipped';
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var StatusFactory
     */
    protected $statusFactory;
    /**
     * @var StatusResourceFactory
     */
    protected $statusResourceFactory;

    /**
     * CreateCustomer constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param StoreManagerInterface $storeManager
     * @param StatusFactory $statusFactory
     * @param StatusResourceFactory $statusResourceFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        StoreManagerInterface $storeManager,
        StatusFactory $statusFactory,
        StatusResourceFactory $statusResourceFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->storeManager = $storeManager;
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
    }

    /**
     * Create Customer
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        try {
            /**
             *  Created statuses data
             */
            $statusesData = [
                [
                    'status' => self::ORDER_STATUS_CUSTOM_READY_CODE,
                    'label' => self::ORDER_STATUS_CUSTOM_READY_LABEL,
                    'state' => self::ORDER_STATE_CUSTOM_READY_CODE
                ]
            ];

            foreach ($statusesData as $oneStatus) {

                if ($oneStatus['status'] != "") {
                    $statusResource = $this->statusResourceFactory->create();
                    $status = $this->statusFactory->create();
                    $status->setData([
                        'status' => $oneStatus['status'],
                        'label' => $oneStatus['label']
                    ]);
                    $statusResource->save($status);
                    $status->assignState($oneStatus['state'], true, true);
                }
            }

        } catch (Exception $e) {
            return '';
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }
}