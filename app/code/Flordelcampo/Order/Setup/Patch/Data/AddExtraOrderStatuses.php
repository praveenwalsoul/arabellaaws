<?php

namespace Flordelcampo\Order\Setup\Patch\Data;

use Exception;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Status;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusResource;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;

/**
 * Class AddOrderStatus
 * Flordelcampo\Order\Setup\Patch\Data
 */
class AddExtraOrderStatuses implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var StatusFactory
     */
    protected $statusFactory;
    /**
     * @var StatusResourceFactory
     */
    protected $statusResourceFactory;

    /**
     * CreateCustomer constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param StoreManagerInterface $storeManager
     * @param StatusFactory $statusFactory
     * @param StatusResourceFactory $statusResourceFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        StoreManagerInterface $storeManager,
        StatusFactory $statusFactory,
        StatusResourceFactory $statusResourceFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->storeManager = $storeManager;
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
    }

    /**
     * Create Customer
     */
    public function apply()
    {
        $statusesData[] = ['code' => 'underReview', 'label' => 'Under Review'];
        $statusesData[] = ['code' => 'vendorPrint', 'label' => 'Ready for Dispatch'];
        $statusesData[] = ['code' => 'printed', 'label' => 'Packed'];
        $statusesData[] = ['code' => 'rejected', 'label' => 'Rejected'];
        $statusesData[] = ['code' => 'invoiced', 'label' => 'Invoiced'];
        $statusesData[] = ['code' => 'refunded', 'label' => 'Refunded'];
        $statusesData[] = ['code' => 'delivered', 'label' => 'Delivered'];
        $statusesData[] = ['code' => 'underProcessing', 'label' => 'Under Processing'];

        $this->moduleDataSetup->getConnection()->startSetup();
        try {
            /**
             *  Created statuses data
             */
            foreach ($statusesData as $oneStatus) {
                $statusResource = $this->statusResourceFactory->create();
                $status = $this->statusFactory->create();
                $status->setData([
                    'status' => $oneStatus['code'],
                    'label' => $oneStatus['label']
                ]);
                $statusResource->save($status);
                $status->assignState($oneStatus['code'], true, true);
            }

        } catch (Exception $e) {
            return '';
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }
}