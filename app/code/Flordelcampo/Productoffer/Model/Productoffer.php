<?php
namespace Flordelcampo\Productoffer\Model;

class Productoffer extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\Productoffer\Model\ResourceModel\Productoffer');
    }
}
?>