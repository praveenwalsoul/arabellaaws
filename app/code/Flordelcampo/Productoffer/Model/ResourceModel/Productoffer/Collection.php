<?php

namespace Flordelcampo\Productoffer\Model\ResourceModel\Productoffer;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Flordelcampo\Productoffer\Model\Productoffer', 'Flordelcampo\Productoffer\Model\ResourceModel\Productoffer');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>