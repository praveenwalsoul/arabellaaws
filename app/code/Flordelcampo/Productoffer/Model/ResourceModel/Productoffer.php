<?php
namespace Flordelcampo\Productoffer\Model\ResourceModel;

class Productoffer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('udropship_product_offer', 'product_offer_id');
    }
}
?>