<?php

namespace Flordelcampo\Productoffer\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context){

       $installer = $setup;
        $installer->startSetup();

        // Get tutorial_simplenews table
  
        if (version_compare($context->getVersion(), '1.1.0') < 0){

            //$installer->run('create table user_registration(id int not null auto_increment, name varchar(255), city varchar(255), country varchar(200), status varchar(100) primary key(id))');
        $installer->run('CREATE TABLE `udropship_product_offer` (
          product_offer_id int not null auto_increment,
          `status` varchar(255) DEFAULT NULL,
          `effective_date` TIMESTAMP NOT NULL,
          `expiry_date` TIMESTAMP NOT NULL,
          `updated_date` TIMESTAMP NOT NULL,
          `location_id` varchar(255) DEFAULT NULL,
          `vendor_id` varchar(255) DEFAULT NULL,
          `file_path` varchar(255) DEFAULT NULL,
          `user_name` varchar(255) DEFAULT NULL,
          primary key(product_offer_id))
        ');
    }
  
    $installer->endSetup();
  }

}