<?php
namespace Flordelcampo\Productoffer\Block\Adminhtml\Productoffer\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('productoffer_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Productoffer Information'));
    }
}