<?php
namespace Flordelcampo\Productoffer\Block\Adminhtml\Productoffer;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Flordelcampo\Productoffer\Model\productofferFactory
     */
    protected $_productofferFactory;

    /**
     * @var \Flordelcampo\Productoffer\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Flordelcampo\Productoffer\Model\productofferFactory $productofferFactory
     * @param \Flordelcampo\Productoffer\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Flordelcampo\Productoffer\Model\ProductofferFactory $ProductofferFactory,
        \Flordelcampo\Productoffer\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_productofferFactory = $ProductofferFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('product_offer_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_productofferFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'product_offer_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'product_offer_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		

						$this->addColumn(
							'status',
							[
								'header' => __('Status'),
								'index' => 'status',
								'type' => 'options',
								'options' => \Flordelcampo\Productoffer\Block\Adminhtml\Productoffer\Grid::getOptionArray0()
							]
						);

						
				$this->addColumn(
					'effective_date',
					[
						'header' => __('Effective Date'),
						'index' => 'effective_date',
						'type'      => 'datetime',
					]
				);

					
				$this->addColumn(
					'expiry_date',
					[
						'header' => __('Expiry Date'),
						'index' => 'expiry_date',
					]
				);
				
				$this->addColumn(
					'updated_date',
					[
						'header' => __('Updated Date'),
						'index' => 'updated_date',
					]
				);
				
				$this->addColumn(
					'user_name',
					[
						'header' => __('User Name'),
						'index' => 'user_name',
					]
				);
				


		
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'product_offer_id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
		

		
		   $this->addExportType($this->getUrl('productoffer/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('productoffer/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('product_offer_id');
        //$this->getMassactionBlock()->setTemplate('Flordelcampo_Productoffer::productoffer/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('productoffer');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('productoffer/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('productoffer/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('productoffer/*/index', ['_current' => true]);
    }

    /**
     * @param \Flordelcampo\Productoffer\Model\productoffer|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        //return $this->getUrl(
          //  'productoffer/*/edit',
           // ['product_offer_id' => $row->getId()]
       // );
		
    }

	
		static public function getOptionArray0()
		{
            $data_array=array(); 
			$data_array[0]='Active';
			$data_array[1]='InActive';
            return($data_array);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(\Flordelcampo\Productoffer\Block\Adminhtml\Productoffer\Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);
			}
            return($data_array);

		}
		

}