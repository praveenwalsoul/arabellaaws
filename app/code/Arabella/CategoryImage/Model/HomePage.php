<?php
namespace Arabella\CategoryImage\Model;

use Arabella\CategoryImage\Api\HomePageInterface;
use Exception;
use Magento\Catalog\Api\CategoryManagementInterface;
use Magento\Catalog\Api\Data\CategoryTreeInterface;
use Magento\Catalog\Model\Category;
use Magento\Directory\Model\CurrencyFactory;

class HomePage implements HomePageInterface
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */

    protected $storeManagerInterface;

    /**
     * @var CategoryManagementInterface
     */
    private $categoryManagement;

    /**
     * @var CurrencyFactory
     */
    private $currencyCode;
    
    /**
     * @var \Magento\Catalog\Model\CategoryRepository
     */
    protected $categoryRepository;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    
    protected $storeId;
    
    /**
     * @var \Arabella\CategoryImage\Helper\Category
     */
    protected $_categoryHelper;

    protected $imageHelper;
    
    protected $productRepository;
    
    protected $homeCategory;
    
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        CategoryManagementInterface $categoryManagement,
        CurrencyFactory $currencyFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Arabella\CategoryImage\Helper\Category $categoryHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->storeManagerInterface = $storeManagerInterface;
        $this->categoryManagement    = $categoryManagement;
        $this->currencyCode = $currencyFactory->create();
        $this->categoryRepository = $categoryRepository;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_categoryHelper = $categoryHelper;
        $this->imageHelper = $imageHelper;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        
        
    }
    /**
     * Returns home page data
     *
     * @api
     * @param string $id Store Id.
     * @return array
     */
    public function getHomePage($id)
    {
        try {
            $this->storeId = $id;
            $categoryId = $this->getRootCategoryId($id);
            
            $response   = [
                'status'   => 200,
                'response' => [
                    'store_id'     => $id,
                    'categoryList' => $this->getCategoryTree($categoryId)
                ],
            ];
            
            $catObj = $this->categoryRepository->get($this->homeCategory->getId());
            $response['response']['seo_details'] = $this->_categoryHelper->getHomeMetaData($catObj);
        } catch (Exception $e) {
            $response = [
                'status'   => $e->getCode(),
                'response' => [
                    'message' => $e->getMessage(),
                ],
            ];
        }
        
        return ['response' => $response];
        
    }

    public function getContent($storeId)
    {
        $categoryId = $this->getRootCategoryId($storeId);
    }

    public function getRootCategoryId($storeId)
    {
        return $this->storeManagerInterface->getStore($storeId)->getRootCategoryId();
    }

    /**
     * Fetch Category Tree
     *
     * @return CategoryTreeInterface
     */
    public function getCategoryTree($rootCategoryId)
    {
        $categoryList = [];
        $homePageData = [];
        try {
            $categoryTreeList = $this->categoryManagement->getTree($rootCategoryId);
            foreach ($categoryTreeList->getChildrenData() as $category) {
                $catObj = $this->categoryRepository->get($category->getId());
                if ($catObj->getIsActive()) {
                    if ($catObj->getIncludeInMenu()) {
                        $categoryList['header']['top_menu'][$catObj->getId()] = [
                            'id'   => $catObj->getId(),
                            'name' => $catObj->getName(),
                            'description' => strip_tags($catObj->getDescription()),
                            'short_description' => strip_tags($catObj->getShortDescription()),
                            'h1' => $catObj->getData('h1'),
                            'h2' => $catObj->getData('h2'),
                            'h3' => $catObj->getData('h3'),
                            'h4' => $catObj->getData('h4'),
                            'h5' => $catObj->getData('h5'),
                            'h6' => $catObj->getData('h6'),
                            'image' => $this->_categoryHelper->getImageUrl($catObj->getImage()),
                            'thumbnail' => $this->_categoryHelper->getImageUrl($catObj->getThumbnail()),
                            'url' => $catObj->getUrl(),
                            'parent_id' => $catObj->getParentId(),
                            'seo_details' => $this->getSeoDetails($catObj),
                            'child_data' => []
                            
                        ];
                        
                        
                        foreach ($category->getChildrenData() as $subcategory) {
                            $subcatObj = $this->categoryRepository->get($subcategory->getId());
                            if ($subcatObj->getIsActive() && $subcatObj->getIncludeInMenu()) {
                                $childCategory = [
                                    'id'   => $subcatObj->getId(),
                                    'name' => $subcatObj->getName(),
                                    'description' => strip_tags($subcatObj->getDescription()),
                                    'short_description' => strip_tags($subcatObj->getShortDescription()),
                                    'h1' => $subcatObj->getData('h1'),
                                    'h2' => $subcatObj->getData('h2'),
                                    'h3' => $subcatObj->getData('h3'),
                                    'h4' => $subcatObj->getData('h4'),
                                    'h5' => $subcatObj->getData('h5'),
                                    'h6' => $subcatObj->getData('h6'),
                                    'image' => $this->_categoryHelper->getImageUrl($subcatObj->getImage()),
                                    'thumbnail' => $this->_categoryHelper->getImageUrl($subcatObj->getThumbnail()),
                                    'parent_id' => $subcatObj->getParentId(),
                                    'url' => $subcatObj->getUrl(),
                                    'seo_details' => $this->getSeoDetails($subcatObj)
                                ];
                                
                                $categoryList['header']['top_menu'][$subcatObj->getParentId()]['child_data'][] = array_merge($childCategory, $this->getCategoryImages($subcatObj));
                            }
                        }
                        
                        $categoryList['header']['top_menu'] = array_values($categoryList['header']['top_menu']);
                    } else if(!$catObj->getIncludeInMenu()) {
                        $this->homeCategory = $category;
                        foreach ($category->getChildrenData() as $blockCat) {
                            $homesubcatObj = $this->categoryRepository->get($blockCat->getId());
                            if (count($blockCat->getChildrenData()) == 0) {
                                $productsList = $this->getProductCollectionByCategories([$blockCat->getId()], $this->storeId);
                                if(count($productsList) > 0) {
                                    $categoryList['home_page_block'][]['home_product_block'][$blockCat->getId()] = [
                                        'category_name' => $homesubcatObj->getName(),
                                        'products' => $this->getProductCollectionByCategories([$blockCat->getId()], $this->storeId)
                                        ];
                                } else {
                                    $home_slider = [
                                        'id'   => $homesubcatObj->getId(),
                                        'name' => $homesubcatObj->getName(),
                                        'description' => $homesubcatObj->getDescription(),
                                        'short_description' => $homesubcatObj->getShortDescription()
                                        ];
                                        
                                    $categoryList['home_page_block'][]['home_slider_block'] = array_merge($home_slider, $this->getCategoryImages($homesubcatObj));
                                }
                            } else {
                                $homeSubCategories = [];
                                foreach ($blockCat->getChildrenData() as $subblockCat) {
                                    $homesubsubcatObj = $this->categoryRepository->get($subblockCat->getId());
                                    $homeSubCat = [
                                        'id'   => $homesubsubcatObj->getId(),
                                        'name' => $homesubsubcatObj->getName(),
                                        'description'  => strip_tags($homesubsubcatObj->getDescription()),
                                        'short_description' => strip_tags($homesubsubcatObj->getShortDescription()),
                                        'seo_details' => $this->getSeoDetails($homesubsubcatObj)
                                        ];
                                        
                                    $homeSubCategories[] = array_merge($homeSubCat, $this->getCategoryImages($homesubsubcatObj));
                                }
                                
                                $categoryList['home_page_block'][]['home_category_block'][$blockCat->getId()] = [
                                    'category_name' => $homesubcatObj->getName(),
                                    'sub_category' => $homeSubCategories
                                    ];
                                
                            }
                        }
                        
                        
                    }

                    
                }
            }

            return $categoryList;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }
    
    /**
     *
     * @param string $price
     * @return string
     */
    public function formatPrice($price)
    {
        $pricingHelper = \Magento\Framework\App\ObjectManager::getInstance()->get(
            '\Magento\Framework\Pricing\Helper\Data'
        );
        return $pricingHelper->currency($price, true, false);
    }
    
    public function getConnection()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		return $connection;
    }
    
    
    /**
     * Return data of rating summary
     *
     * @param int $productId
     * @return array
     */
    protected function getReviewSummaryData($productId, $storeId)
    {
        $connection = $this->getConnection();

        $sumColumn = new \Zend_Db_Expr("rating_vote.{$connection->quoteIdentifier('value')}");
        $countColumn = new \Zend_Db_Expr("COUNT(*)");

        $select = $connection->select()->from(
            ['rating_vote' => 'rating_option_vote'],
            ['entity_pk_value' => 'rating_vote.entity_pk_value', 'rating' => $sumColumn, 'count' => $countColumn]
        )->join(
            ['review' => 'review'],
            'rating_vote.review_id=review.review_id',
            []
        )->joinLeft(
            ['review_store' => 'review_store'],
            'rating_vote.review_id=review_store.review_id',
            ['review_store.store_id']
        );
        $select->join(
            ['review_status' => 'review_status'],
            'review.status_id = review_status.status_id',
            []
        )->where(
            'review_status.status_code = :status_code'
        )
        ->group(
            'rating_vote.value'
        );
        $bind = [':status_code' => 'Approved'];

        $entityPkValue = $productId;
        if ($entityPkValue) {
            $select->where('rating_vote.entity_pk_value = :pk_value');
            $bind[':pk_value'] = $entityPkValue;
            $select->where('review_store.store_id = :store_id');
            $bind[':store_id'] = $storeId;
        }

        return $connection->fetchAll($select, $bind);
    }
    
    /**
     * Return data of rating summary
     * 
     * @param $productId
     * @return array
     */
    protected function getGlobalRating($productId, $storeId)
    {
        $connection = $this->getConnection();

        $sumColumn = new \Zend_Db_Expr("SUM(rating_vote.{$connection->quoteIdentifier('percent')})");
        $countColumn = new \Zend_Db_Expr("COUNT(*)");

        $select = $connection->select()->from(
            ['rating_vote' => 'rating_option_vote'],
            ['entity_pk_value' => 'rating_vote.entity_pk_value', 'sum' => $sumColumn, 'count' => $countColumn]
        )->join(
            ['review' => 'review'],
            'rating_vote.review_id=review.review_id',
            []
        )->joinLeft(
            ['review_store' => 'review_store'],
            'rating_vote.review_id=review_store.review_id',
            ['review_store.store_id']
        );
        $select->join(
            ['review_status' => 'review_status'],
            'review.status_id = review_status.status_id',
            []
        )->where(
            'review_status.status_code = :status_code'
        )->group(
            'rating_vote.entity_pk_value'
        )->group(
            'review_store.store_id'
        );
        $bind = [':status_code' => 'Approved'];

        $entityPkValue = $productId;
        if ($entityPkValue) {
            $select->where('rating_vote.entity_pk_value = :pk_value');
            $bind[':pk_value'] = $entityPkValue;
            $select->where('review_store.store_id = :store_id');
            $bind[':store_id'] = $storeId;
        }

        return $connection->fetchAll($select, $bind);
    }
    
    /**
     * Get Product Rating Details
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\RatingDetailsInterface[]|null
     */
    public function getRatingDetails($productId, $storeId)
    {
        $reviewSummery = $this->getReviewSummaryData($productId, $storeId);
        $totalReview = count($reviewSummery);
        $reviews = [
            1 => ['rating' => 0, 'percent' => 0],
            2 => ['rating' => 0, 'percent' => 0],
            3 => ['rating' => 0, 'percent' => 0],
            4 => ['rating' => 0, 'percent' => 0],
            5 => ['rating' => 0, 'percent' => 0]
        ];
        $globalReviews = 0;
        foreach($reviewSummery as $review) {
            $reviews[$review['rating']] = ['rating' => $review['rating'], 'percent' => $review['count']];
            $globalReviews += $review['count'];
        }
        
        if($globalReviews > 0) {
            for($i = 1; $i<=5; $i++) {
                $reviews[$i]['percent'] = round(($reviews[$i]['percent']/$globalReviews)*100, 2);
            }
        }
        $data = $this->getGlobalRating($productId, $storeId);
        $globalRating = (isset($data[0]['count']) && $data[0]['count'] > 0) ? round($data[0]['sum']/$data[0]['count'], 2):0;
        return [['rating' => $reviews, 'global_percent' => $globalRating, 'global_review' => $globalReviews]] ;
    }
    
    
    public function getProductCollectionByCategories($ids, $storeId)
    {
        $collection = $this->productFactory->create()
                                    ->setStoreId($storeId)
                                    ->getCollection();
        $collection->addAttributeToSelect('*');
        $collection->addCategoriesFilter(['in' => $ids]);
        $products = [];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        foreach($collection as $product) {
            $productStockObj = $objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($product->getId());
            $products[] = [
                'id' => $product->getId(),
                'sku' => $product->getSku(),
                'name' => $product->getName(),
                'short_description' => $product->getShortDescription(),
                'price' => $this->formatPrice($product->getPrice()),
                'qty_in_stock' => $productStockObj->getData('is_in_stock'),
                'image' => $this->getItemImage($product->getId()),
                'rating_details' => $this->getRatingDetails($product->getId(), $storeId)
                ];
        }
        return $products;
    }
    
    
    /**s
     * @return string
     */
    public function getSymbol()
    {
        $currentCurrency = $this->storeManagerInterface->getStore($this->storeId)->getCurrentCurrencyCode();
        $currency = $this->currencyCode->load($currentCurrency);
        return $currency->getCurrencySymbol();
    }
    
    
    public function getCategoryImages($category)
    {
        $images = [];
        
        $images['image_url'] = $this->_categoryHelper->getImageUrl($category->getImage());
        $images['thumbnail'] = $this->_categoryHelper->getImageUrl($category->getThumbnail());
        $images['image_mobile'] = $this->_categoryHelper->getImageUrl($category->getImageMobile());
        
        // Gallery Images
        
        
        $images['gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('gallery_image_1'));
        $images['gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('gallery_image_2'));
        $images['gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('gallery_image_3'));
        $images['gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('gallery_image_4'));
        $images['gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('gallery_image_5'));
        
        // Mobile Gallery Images
        
        $images['mobile_gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('mobile_gallery_1'));
        $images['mobile_gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('mobile_gallery_2'));
        $images['mobile_gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('mobile_gallery_3'));
        $images['mobile_gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('mobile_gallery_4'));
        $images['mobile_gallery_images'][] = $this->_categoryHelper->getImageUrl($category->getData('mobile_gallery_5'));
        
        return $images;
    }
    
    /**
     * @param int $id
     * @return string
     */
    public function getItemImage($productId)
    {
        try {
            $images = [];
            $_product = $this->productRepository->getById($productId);
            $productimages = $_product->getMediaGalleryImages();
            foreach($productimages as $productimage) {
                $images[] = $productimage['url'];
            }
        } catch (NoSuchEntityException $e) {
            return 'product not found';
        }
        return $images;
    }
    
    /**
     * Get Search Engin Optomization Details
     * @param string $cat
     * @return array
     */
    public function getSeoDetails($cat)
    {
        $result = [
            'url_key' => $cat->getUrlKey(),
            'meta_title' => $cat->getMetaTitle(),
            'meta_keyword' => $cat->getMetaKeywords(),
            'meta_description' => $cat->getMetaDescription()
        ];
        return $result;
    }
}
