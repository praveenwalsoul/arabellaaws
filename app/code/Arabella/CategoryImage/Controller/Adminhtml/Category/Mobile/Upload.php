<?php
namespace Arabella\CategoryImage\Controller\Adminhtml\Category\Mobile;

use Arabella\CategoryImage\Controller\Adminhtml\AbstractUpload;

/**
 * Class Upload
 */
class Upload extends AbstractUpload
{
    const CATEGORY_ATTRIBUTE_IMAGE = 'image_mobile';

    /**
     * {@inheritdoc}
     */
    protected function getCategoryAttributeImage($categoryAttribute = null)
    {
        return self::CATEGORY_ATTRIBUTE_IMAGE;
    }
}