<?php
namespace Arabella\CategoryImage\Controller\Adminhtml\Category;

use Arabella\CategoryImage\Controller\Adminhtml\AbstractUpload;

/**
 * Class Upload
 */
class Gallery extends AbstractUpload
{
    const GALLERY_IMAGE_1  = 'gallery_image_1';
    const GALLERY_IMAGE_2  = 'gallery_image_2';
    const GALLERY_IMAGE_3  = 'gallery_image_3';
    const GALLERY_IMAGE_4  = 'gallery_image_4';
    const GALLERY_IMAGE_5  = 'gallery_image_5';
    const MOBILE_GALLERY_1 = 'mobile_gallery_1';
    const MOBILE_GALLERY_2 = 'mobile_gallery_2';
    const MOBILE_GALLERY_3 = 'mobile_gallery_3';
    const MOBILE_GALLERY_4 = 'mobile_gallery_4';
    const MOBILE_GALLERY_5 = 'mobile_gallery_5';

    /**
     * {@inheritdoc}
     */
    protected function getCategoryAttributeImage($categoryAttribute = null)
    {
        return $categoryAttribute;
    }
}
