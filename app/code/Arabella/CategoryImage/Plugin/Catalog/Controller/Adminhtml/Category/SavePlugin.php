<?php
namespace Arabella\CategoryImage\Plugin\Catalog\Controller\Adminhtml\Category;

use Magento\Catalog\Controller\Adminhtml\Category\Save as SaveController;
use Arabella\CategoryImage\Controller\Adminhtml\Category\Mobile\Upload as MobileUpload;
use Arabella\CategoryImage\Controller\Adminhtml\Category\Thumbnail\Upload as ThumbnailUpload;
use Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery as Gallery;

class SavePlugin
{
    /**
     * Add additional images
     *
     * @param SaveController $subject
     * @param array $data
     * @return array
     */
    public function beforeImagePreprocessing(SaveController $subject, $data)
    {
        foreach ($this->getAdditionalImages() as $imageType) {
            if (empty($data[$imageType])) {
                unset($data[$imageType]);
                $data[$imageType]['delete'] = true;
            }
        }

        return [$data];
    }

    /**
     * Get additional Images
     *
     * @return array
     */
    protected function getAdditionalImages() {
        return [
            MobileUpload::CATEGORY_ATTRIBUTE_IMAGE,
            ThumbnailUpload::CATEGORY_ATTRIBUTE_IMAGE,
            Gallery::GALLERY_IMAGE_1,
            Gallery::GALLERY_IMAGE_2,
            Gallery::GALLERY_IMAGE_3,
            Gallery::GALLERY_IMAGE_4,
            Gallery::GALLERY_IMAGE_5,
            Gallery::MOBILE_GALLERY_1,
            Gallery::MOBILE_GALLERY_2,
            Gallery::MOBILE_GALLERY_3,
            Gallery::MOBILE_GALLERY_4,
            Gallery::MOBILE_GALLERY_5
        ];
    }
}