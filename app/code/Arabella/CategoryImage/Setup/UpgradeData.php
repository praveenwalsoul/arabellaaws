<?php
namespace Arabella\CategoryImage\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Constructor
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2', '<=')) {
            $this->addCategoryAttributeImageMobile($setup);
            $this->addCategoryAttributeShortDescription($setup);
            $this->addHeadingTags($setup);
        }

        $setup->endSetup();
    }

    /**
     * Add category attribute "Category Image Mobile"
     *
     * @param ModuleDataSetupInterface $setup
     * @return $this
     */
    private function addCategoryAttributeImageMobile(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Mobile\Upload::CATEGORY_ATTRIBUTE_IMAGE,
            [
                'type' => 'varchar',
                'label' => 'Mobile Thumbnail',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::GALLERY_IMAGE_1,
            [
                'type' => 'varchar',
                'label' => 'Gallery Image 1',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::GALLERY_IMAGE_2,
            [
                'type' => 'varchar',
                'label' => 'Gallery Image 2',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::GALLERY_IMAGE_3,
            [
                'type' => 'varchar',
                'label' => 'Gallery Image 3',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::GALLERY_IMAGE_4,
            [
                'type' => 'varchar',
                'label' => 'Gallery Image 4',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::GALLERY_IMAGE_5,
            [
                'type' => 'varchar',
                'label' => 'Gallery Image 5',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::MOBILE_GALLERY_1,
            [
                'type' => 'varchar',
                'label' => 'Mobile Gallery 1',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::MOBILE_GALLERY_2,
            [
                'type' => 'varchar',
                'label' => 'Mobile Gallery 2',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::MOBILE_GALLERY_3,
            [
                'type' => 'varchar',
                'label' => 'Mobile Gallery 3',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::MOBILE_GALLERY_4,
            [
                'type' => 'varchar',
                'label' => 'Mobile Gallery 4',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            \Arabella\CategoryImage\Controller\Adminhtml\Category\Gallery::MOBILE_GALLERY_5,
            [
                'type' => 'varchar',
                'label' => 'Mobile Gallery 5',
                'input' => 'image',
                'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        return $this;
    }
    
    /**
     * Add category attribute "Short Description"
     *
     * @param ModuleDataSetupInterface $setup
     * @return $this
     */
    private function addCategoryAttributeShortDescription(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'short_description',
            [
                'type' => 'text',
                'label' => 'Short Description',
                'input' => 'textarea',
                'sort_order' => 420,
                'source' => '',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => null,
                'group'        => '',
                'backend'      => ''
            ]
        );

        return $this;
    }
    
    /**
     * Add category heading atrriute"
     *
     * @param ModuleDataSetupInterface $setup
     * @return $this
     */
    private function addHeadingTags(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'h1',
			[
				'type'         => 'varchar',
				'label'        => 'H1',
				'input'        => 'text',
				'sort_order'   => 100,
				'source'       => '',
				'global'       => 1,
				'visible'      => true,
				'required'     => false,
				'user_defined' => false,
				'default'      => null,
				'group'        => 'Search Engine Optimization',
				'backend'      => ''
			]
		);
		
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'h2',
			[
				'type'         => 'varchar',
				'label'        => 'H2',
				'input'        => 'text',
				'sort_order'   => 100,
				'source'       => '',
				'global'       => 1,
				'visible'      => true,
				'required'     => false,
				'user_defined' => false,
				'default'      => null,
				'group'        => 'Search Engine Optimization',
				'backend'      => ''
			]
		);
		
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'h3',
			[
				'type'         => 'varchar',
				'label'        => 'H3',
				'input'        => 'text',
				'sort_order'   => 100,
				'source'       => '',
				'global'       => 1,
				'visible'      => true,
				'required'     => false,
				'user_defined' => false,
				'default'      => null,
				'group'        => 'Search Engine Optimization',
				'backend'      => ''
			]
		);
		
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'h4',
			[
				'type'         => 'varchar',
				'label'        => 'H4',
				'input'        => 'text',
				'sort_order'   => 100,
				'source'       => '',
				'global'       => 1,
				'visible'      => true,
				'required'     => false,
				'user_defined' => false,
				'default'      => null,
				'group'        => 'Search Engine Optimization',
				'backend'      => ''
			]
		);
		
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'h5',
			[
				'type'         => 'varchar',
				'label'        => 'H5',
				'input'        => 'text',
				'sort_order'   => 100,
				'source'       => '',
				'global'       => 1,
				'visible'      => true,
				'required'     => false,
				'user_defined' => false,
				'default'      => null,
				'group'        => 'Search Engine Optimization',
				'backend'      => ''
			]
		);
		
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'h6',
			[
				'type'         => 'varchar',
				'label'        => 'H6',
				'input'        => 'text',
				'sort_order'   => 100,
				'source'       => '',
				'global'       => 1,
				'visible'      => true,
				'required'     => false,
				'user_defined' => false,
				'default'      => null,
				'group'        => 'Search Engine Optimization',
				'backend'      => ''
			]
		);
		
		return $this;
    }
}