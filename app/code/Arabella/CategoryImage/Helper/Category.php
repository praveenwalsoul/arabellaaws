<?php

namespace Arabella\CategoryImage\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Category extends AbstractHelper
{


    /**
     * @return array
     */
    public function getAdditionalImageTypes()
    {
        return array('thumbnail');
    }

    /**
     * Retrieve image URL
     * @param $image
     * @return string
     */
    public function getImageUrl($image)
    {
        $url = false;
        //$image = $this->getImage();
        if ($image) {
            if (is_string($image)) {
                $url = $this->_urlBuilder->getBaseUrl(
                        ['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]
                    ) . 'catalog/category/' . $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }
    
    public function getHomeMetaData($catObj = null)
    {
        $title = $this->scopeConfig->getValue('design/head/default_title', ScopeInterface::SCOPE_STORE);
        $keyword = $this->scopeConfig->getValue('design/head/default_keywords', ScopeInterface::SCOPE_STORE);
        $description = $this->scopeConfig->getValue('design/head/default_description', ScopeInterface::SCOPE_STORE);
        
        return [
            'title' => $catObj->getMetaTitle(),
            'meta_keyword' => $catObj->getMetaKeywords(),
            'meta_description' => $catObj->getMetaDescription(),
            'h1' => $catObj->getData('h1'),
            'h2' => $catObj->getData('h2'),
            'h3' => $catObj->getData('h3'),
            'h4' => $catObj->getData('h4'),
            'h5' => $catObj->getData('h5'),
            'h6' => $catObj->getData('h6'),
            ];
    }

}