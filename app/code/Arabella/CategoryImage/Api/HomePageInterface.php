<?php
namespace Arabella\CategoryImage\Api;
 
interface HomePageInterface
{
    /**
     * Returns greeting message to user
     *
     * @api
     * @param string $id Store Id.
     * @return string.
     */
    public function getHomePage($id);
}