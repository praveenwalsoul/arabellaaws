<?php
namespace Arabella\Prescription\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface PrescriptionSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Arabella\Prescription\Api\Data\PrescriptionInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Arabella\Prescription\Api\Data\PrescriptionInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
