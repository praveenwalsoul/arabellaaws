<?php

namespace Arabella\Prescription\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface ConfigOptionsInterface
 */
interface PrescriptionInterface extends ExtensibleDataInterface
{
    public const ID = 'id';
    public const CUSTOMER_ID = 'customer_id';
    public const QUOTE_ID = 'quote_id';
    public const FILENAME = 'filename';
    public const ORDER_PLACED = 'order_placed';
    public const TYPE = 'type';
    public const UPLOADED_BY = 'uploaded_by';
    public const USERNAME = 'username';
    public const UPLOADED_DATE = 'created_at';

    /**
     * Get Customer Precription id
     *
     * @return int
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getUploadedBy();

    /**
     * @return mixed
     */
    public function getUploadedDate();

    /**
     * @param $uploadedDate
     * @return mixed
     */
    public function setUploadedDate($uploadedDate);

    /**
     * @return mixed
     */
    public function getUsername();

    /**
     * @param $username
     * @return mixed
     */
    public function setUsername($username);

    /**
     * @param $uploadedBy
     * @return $this
     */
    public function setUploadedBy($uploadedBy);

    /**
     * Set Customer Precription id
     *
     * @param int $entityId
     * @return $this
     */
    public function setId($entityId);

    /**
     * Get Customer id
     *
     * @return int
     */
    public function getCustomerId();

    /**
     * Set Customer id
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * Get Quote id
     *
     * @return int
     */
    public function getQuoteId();

    /**
     * Set Quote id
     *
     * @param int $quoteId
     * @return $this
     */
    public function setQuoteId($quoteId);

    /**
     * Get Filename
     *
     * @return string
     */
    public function getFilename();

    /**
     * Set Filename
     *
     * @param string $filename
     * @return $this
     */
    public function setFilename($filename);

    /**
     * Get Report Type
     *
     * @return string
     */
    public function getType();

    /**
     * Set Report Type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type);


    /**
     * Get OrderPlaced id
     *
     * @return int
     */
    public function getOrderPlaced();

    /**
     * Set OrderPlaced id
     *
     * @param int $orderPlaced
     * @return $this
     */
    public function setOrderPlaced($orderPlaced);
}
