<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Arabella\Prescription\Api;

interface PrescriptionRepositoryInterface
{
    /**
     * @return \Arabella\Prescription\Api\Data\PrescriptionInterface
     */
    public function uploadPrescription(\Arabella\Prescription\Api\Data\PrescriptionInterface $prescription);

    /**
     * @param int $quoteId
     * @return \Arabella\Prescription\Api\Data\PrescriptionInterface[]
     */
    public function getPrescriptionByCart($quoteId);

    /**
     * @param int $customerId
     * @return \Arabella\Prescription\Api\Data\PrescriptionInterface[]
     */
    public function myPrescriptions($customerId);
    
    /**
     * @param int $id
     * @return Arabella\Prescription\Api\Data\PrescriptionInterface
     */
    public function getPrescriptionById($id);
}
