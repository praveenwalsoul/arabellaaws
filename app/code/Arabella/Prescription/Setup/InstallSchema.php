<?php

namespace Arabella\Prescription\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0) {

            $installer->run('create table prescriptions(id int not null auto_increment, customer_id varchar(10),quote_id varchar(10),filename varchar(255), type varchar(255),order_placed tinyint(1),created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, primary key(id))');

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $scopeConfig   = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
            $writer        = new \Zend\Log\Writer\Stream(BP . '/var/log/updaterates.log');
            $logger        = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('updaterates');

        }

        $installer->endSetup();

    }
}
