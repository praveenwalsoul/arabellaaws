<?php

namespace Arabella\Prescription\Model;

use Arabella\Prescription\Api\PrescriptionRepositoryInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class PrescriptionRepository implements PrescriptionRepositoryInterface
{
    protected $_fileUploaderFactory;
    protected $_filesystem;
    protected $fileFactory;
    protected $prescriptionFactory;
    protected $collectionFactory;
    /**
     * @var \Arabella\Prescription\Api\Data\PrescriptionSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    public function __construct(
        \Arabella\Prescription\Api\Data\PrescriptionSearchResultsInterfaceFactory $searchResultsFactory,
        \Arabella\Prescription\Model\ResourceModel\Prescription\CollectionFactory $collectionFactory,
        \Arabella\Prescription\Model\PrescriptionFactory $prescriptionFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Filesystem $_filesystem
    )
    {
        $this->prescriptionFactory = $prescriptionFactory;
        $this->collectionFactory = $collectionFactory;
        $this->fileFactory = $fileFactory;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $_filesystem;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @return \Exception||\Arabella\Prescription\Api\Data\PrescriptionInterface
     */
    public function uploadPrescription(\Arabella\Prescription\Api\Data\PrescriptionInterface $prescription)
    {
        try {
            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'filename']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','pdf']);

            $uploader->setAllowRenameFiles(false);

            $uploader->setFilesDispersion(false);
            $path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)
                ->getAbsolutePath('prescriptions/');

            $uploader->save($path, time() . '.' . $uploader->getFileExtension());
            $fileName = $uploader->getUploadedFileName();
            $prescriptionObj = $this->prescriptionFactory->create();
            $prescriptionObj->setCustomerId($prescription->getCustomerId());
            $prescriptionObj->setQuoteId($prescription->getQuoteId());
            $prescriptionObj->setFilename($fileName);
            $prescriptionObj->setType($prescription->getType());
            $prescriptionObj->setOrderPlaced($prescription->getOrderPlaced());
            $prescriptionObj->setUsername($prescription->getUsername());
            $prescriptionObj->setUploadedBy($prescription->getUploadedBy());
            $prescriptionObj->save();
            return $prescriptionObj;
        } catch (\Exception $e) {
            return ['status' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    /**
     * @param int $quoteId
     * @return \Arabella\Prescription\Api\Data\PrescriptionInterface[]
     */
    public function getPrescriptionByCart($quoteId)
    {
        /** @var \Arabella\Prescription\Model\ResourceModel\Prescription\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('quote_id', ['eq' => $quoteId]);
        $collection->load();
        $prescription = [];
        foreach ($collection->getItems() as $item) {
            $prescription[] = $item;
        }

        return $prescription;
    }

    /**
     * @param int $customerId
     * @return \Arabella\Prescription\Api\Data\PrescriptionInterface[]
     */
    public function myPrescriptions($customerId)
    {
        /** @var \Arabella\Prescription\Model\ResourceModel\Prescription\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('customer_id', ['eq' => $customerId]);
        $collection->load();
        $prescription = [];
        foreach ($collection->getItems() as $item) {
            $prescription[] = $item;
        }

        return $prescription;
    }

    /**
     * @param int $id
     * @return Arabella\Prescription\Api\Data\PrescriptionInterface
     */
    public function getPrescriptionById($id)
    {
        /** @var \Arabella\Prescription\Model\ResourceModel\Prescription\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('id', ['eq' => $id]);
        $collection->load();
        $prescription = [];
        foreach ($collection->getItems() as $item) {
            $prescription = $item;
        }

        return $prescription;
    }
}
