<?php

namespace Arabella\Prescription\Model\ResourceModel\Prescription;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Arabella\Prescription\Model\Prescription', 'Arabella\Prescription\Model\ResourceModel\Prescription');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>