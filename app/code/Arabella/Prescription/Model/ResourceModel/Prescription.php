<?php
namespace Arabella\Prescription\Model\ResourceModel;

class Prescription extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('prescriptions', 'id');
    }
}
?>