<?php

namespace Arabella\Prescription\Model;

use Arabella\Prescription\Api\Data\PrescriptionInterface;

class Prescription extends \Magento\Framework\Model\AbstractModel implements PrescriptionInterface
{
    public const DOCUMENT_PATH = 'prescriptions/';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Arabella\Prescription\Model\ResourceModel\Prescription');
    }

    /**
     * Set Customer Precription id
     *
     * @param int $entityId
     * @return $this
     */
    public function setId($entityId)
    {
        $this->setData(self::ID, $entityId);
        return $this;
    }

    /**
     * Get Customer Precription id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }


    /**
     * Set Customer id
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        $this->setData(self::CUSTOMER_ID, $customerId);
        return $this;
    }

    /**
     * Get Customer id
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set Quote id
     *
     * @param int $quoteId
     * @return $this
     */
    public function setQuoteId($quoteId)
    {
        $this->setData(self::QUOTE_ID, $quoteId);
        return $this;
    }

    /**
     * Get Quote id
     *
     * @return int
     */
    public function getQuoteId()
    {
        return $this->getData(self::QUOTE_ID);
    }

    /**
     * Set Filename id
     *
     * @param string $filename
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->setData(self::FILENAME, $filename);
        return $this;
    }

    /**
     * Get Filename id
     *
     * @return string
     */
    public function getFilename()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mediaPath = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaPath . self::DOCUMENT_PATH . $this->getData(self::FILENAME);
    }

    /**
     * Get Report Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * Set Report Type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->setData(self::TYPE, $type);
        return $this;
    }

    /**
     * Set OrderPlaced id
     *
     * @param int $orderPlaced
     * @return $this
     */
    public function setOrderPlaced($orderPlaced)
    {
        $this->setData(self::ORDER_PLACED, $orderPlaced);
        return $this;
    }

    /**
     * Get OrderPlaced id
     *
     * @return int
     */
    public function getOrderPlaced()
    {
        return $this->getData(self::ORDER_PLACED);
    }

    /**
     * @return array|mixed|string|null
     */
    public function getUploadedBy()
    {
        return $this->getData(self::UPLOADED_BY);
    }

    /**
     * @return array|mixed|null
     */
    public function getUsername()
    {
        return $this->getData(self::USERNAME);
    }

    /**
     * @param $username
     * @return $this|mixed
     */
    public function setUsername($username)
    {
        $this->setData(self::USERNAME, $username);
        return $this;
    }

    /**
     * @param $uploadedBy
     * @return $this|Prescription
     */
    public function setUploadedBy($uploadedBy)
    {
        $this->setData(self::UPLOADED_BY, $uploadedBy);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getUploadedDate()
    {
        return $this->getData(self::UPLOADED_DATE);
    }

    /**
     * @param $uploadedDate
     * @return $this|mixed
     */
    public function setUploadedDate($uploadedDate)
    {
        $this->setData(self::UPLOADED_DATE, $uploadedDate);
        return $this;
    }
}
