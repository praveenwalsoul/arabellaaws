<?php
/**
 * Copyright Arabella Sp. z o.o.
 * See LICENSE_DIVANTE.txt for license details.
 */
declare(strict_types=1);

namespace Arabella\Mageapi\Model\Review\Validator;

use Arabella\Mageapi\Api\Data\ReviewInterface;
use Arabella\Mageapi\Validation\ValidationResult;
use Arabella\Mageapi\Validation\ValidationResultFactory;
use Arabella\Mageapi\Model\ReviewValidatorInterface;

/**
 * Class TitleValidator - - validates review nickname
 */
class NicknameValidator implements ReviewValidatorInterface
{
    /**
     * @var ValidationResultFactory
     */
    private $validationResultFactory;

    /**
     * @param ValidationResultFactory $validationResultFactory
     */
    public function __construct(ValidationResultFactory $validationResultFactory)
    {
        $this->validationResultFactory = $validationResultFactory;
    }

    /**
     * Check if nickname is not empty
     *
     * @param ReviewInterface $review
     *
     * @return ValidationResult
     */
    public function validate(ReviewInterface $review): ValidationResult
    {
        $value = (string)$review->getNickname();
        $errors = [];

        if (trim($value) === '') {
            $errors[] = __('"%field" can not be empty.', ['field' => ReviewInterface::NICKNAME]);
        }

        return $this->validationResultFactory->create(['errors' => $errors]);
    }
}
