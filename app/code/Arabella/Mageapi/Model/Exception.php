<?php

/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */
namespace Arabella\Mageapi\Model;

class Exception
{
    const DUPPLICATE = '001';
}
