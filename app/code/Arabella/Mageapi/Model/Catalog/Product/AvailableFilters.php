<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */
namespace Arabella\Mageapi\Model\Catalog\Product;

/**
 * Class AvailableFilters
 * @package Arabella\Mageapi\Model\Catalog\Product
 */
class AvailableFilters extends \Magento\Framework\Model\AbstractExtensibleModel implements \Arabella\Mageapi\Api\Data\Catalog\Product\AvailableFiltersInterface
{
    /**
     * Get Available Filters
     *
     * @return array|null
     */
    public function getAvailablefilter()
    {
        return $this->getData(self::AVAILABLEFILTER);
    }

    /**
     * Set Available Filters
     * @param $availablefilter
     * @return $this
     */
    public function setAvailablefilter($availablefilter)
    {
        return $this->setData(self::AVAILABLEFILTER, $availablefilter);
    }
}
