<?php
/**
 *  Copyright © 2020 Magestore. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */
namespace Arabella\Mageapi\Model\Catalog\Product;

/**
 * Class Customfields
 * @package Arabella\Mageapi\Model\Catalog\Product
 */
class CustomFields extends \Magento\Framework\Model\AbstractExtensibleModel implements \Arabella\Mageapi\Api\Data\Catalog\Product\CustomFieldsInterface
{
    /**
     * Get Custom field lable
     *
     * @return string|bool
     */
    public function getLable()
    {
        return $this->getData(self::ATTRIBUTE_LABLE);
    }

    /**
     * Set Custom field lable
     *
     * @return $this
     */
    public function setLable($attrLabel)
    {
        return $this->setData(self::ATTRIBUTE_LABLE, $attrLabel);
    }

    /**
     * Get Custom field value
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->getData(self::ATTRIBUTE_VALUE);
    }

    /**
     * Set Custom field value
     *
     * @return $this
     */
    public function setValue($attrValue)
    {
        return $this->setData(self::ATTRIBUTE_VALUE, $attrValue);
    }
}
