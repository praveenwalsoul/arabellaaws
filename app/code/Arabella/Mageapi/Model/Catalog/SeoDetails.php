<?php
/**
 *  Copyright © 2020 Magestore. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */
namespace Arabella\Mageapi\Model\Catalog;

/**
 * Class SeoDetails
 * @package Arabella\Mageapi\Model\Catalog
 */
class SeoDetails extends \Magento\Framework\Model\AbstractExtensibleModel implements \Arabella\Mageapi\Api\Data\Catalog\SeoDetailsInterface
{
    /**
     * Get URL key
     *
     * @return string|null
     */
    public function getUrlKey()
    {
        return $this->getData(self::URL_KEY);
    }

    /**
     * Set URL key
     * @param $urlKey
     * @return $this
     */
    public function setUrlKey($urlKey)
    {
        $this->setSata(self::URL_KEY, $urlKey);
        
        return $this;
    }

    /**
     * Get Meta title
     *
     * @return string|null
     */
    public function getMetaTitle()
    {
        return $this->getData(self::META_TITLE);
    }

    /**
     * Set Meta title
     * @param $metaTitle
     * @return $this
     */
    public function setMetaTitle($metaTitle)
    {
        $this->setSata(self::META_TITLE, $metaTitle);
        
        return $this;
    }
    
    /**
     * Get Meta Keyword
     *
     * @return string|null
     */
    public function getMetaKeyword()
    {
        return $this->getData(self::META_KEYWORD);
    }

    /**
     * Set Meta Keyword
     * @param $metaKeyword
     * @return $this
     */
    public function setMetaKeyword($metaKeyword)
    {
        $this->setSata(self::META_KEYWORD, $metaKeyword);
        
        return $this;
    }
    
    /**
     * Get Meta Keyword
     *
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->getData(self::META_DESCRIPTION);
    }

    /**
     * Set Meta Keyword
     * @param $metaDescription
     * @return $this
     */
    public function setMetaDescription($metaDescription)
    {
        $this->setSata(self::META_DESCRIPTION, $metaDescription);
        
        return $this;
    }
}
