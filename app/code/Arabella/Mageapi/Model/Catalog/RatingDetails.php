<?php
/**
 *  Copyright © 2020 Magestore. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */
namespace Arabella\Mageapi\Model\Catalog;

/**
 * Class SeoDetails
 * @package Arabella\Mageapi\Model\Catalog
 */
class RatingDetails extends \Magento\Framework\Model\AbstractExtensibleModel implements \Arabella\Mageapi\Api\Data\Catalog\RatingDetailsInterface
{
    /**
     * Get Rating
     *
     * @return array|null
     */
    public function getRatings()
    {
        return $this->getData(self::RATING);
    }

    /**
     * Set URL key
     * @param $rating
     * @return $this
     */
    public function setRatings($rating)
    {
        $this->setSata(self::RATING, $rating);
        
        return $this;
    }

    /**
     * Get global review
     *
     * @return int|null
     */
    public function getGlobalReview()
    {
        return $this->getData(self::GLOBAL_REVIEW);
    }

    /**
     * Set Meta title
     * @param $globalReview
     * @return $this
     */
    public function setGlobalReview($globalReview)
    {
        $this->setSata(self::GLOBAL_REVIEW, $globalReview);
        
        return $this;
    }
}
