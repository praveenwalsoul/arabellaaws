<?php
namespace Arabella\Mageapi\Api\Catalog;

interface SwatchRepositoryInterface
{
    /**
     *
     * @param
     * @return \Arabella\Mageapi\Api\Data\Catalog\SwatchResultInterface
     */
    public function getList();
}
