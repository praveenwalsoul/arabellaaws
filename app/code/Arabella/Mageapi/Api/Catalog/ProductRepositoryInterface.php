<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */

namespace Arabella\Mageapi\Api\Catalog;

/**
 * @api
 */
interface ProductRepositoryInterface extends \Magento\Catalog\Api\ProductRepositoryInterface
{
    const TYPE_ID = 'type_id';
    const NAME = 'name';
    const PRICE = 'price';
    const SPECIAL_PRICE = 'special_price';
    const SPECIAL_FROM_DATE = 'special_from_date';
    const SPECIAL_TO_DATE = 'special_to_date';
    const SKU = 'sku';
    const SHORT_DESCRIPTION = 'short_description';
    const DESCRIPTION = 'description';
    const IMAGE = 'image';
    const FINAL_PRICE = 'final_price';

    /**
     * Get info about product by product SKU
     *
     * @param string $sku
     * @param bool $editMode
     * @param int|null $storeId
     * @param bool $forceReload
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($sku, $editMode = false, $storeId = null, $forceReload = false);

    /**
     * Get product list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
    
    /**
     * Get product list by Zip Code
     *
     * @param string $zipcode
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductSearchResultsInterface
     */
    public function getListByzipCode(string $zipcode, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Get product list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductOriginalSearchResultsInterface
     */
    public function getProductsWithoutOptions(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Get product options
     *
     * @param string $id
     * @param bool $editMode
     * @param int|null $storeId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOptions($id, $editMode = false, $storeId = null);

    /**
     * Get info about product by product SKU
     *
     * @param string $id
     * @param bool $editMode
     * @param int|null $storeId
     * @param bool $forceReload
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductById($id, $editMode = false, $storeId = null, $forceReload = false);
    
    /**
     * Retrieve filterlist
     *
     * @param int $categoryId
     * @return \Arabella\Mageapi\Api\Data\Catalog\Product\AvailableFiltersInterface
     */
    public function getSearchFilters($categoryId);
    
    /**
     * @param string $sku
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductInterface[]
     */
    public function getRelatedProducts($sku);
    
    /**
     * @param string $sku
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductInterface[]
     */
    public function getUpsellProducts($sku);
    
    /**
     * Get product Search Suggetion list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return array
     */
    public function getSearchSuggetion(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
    
    /**
     * Get product Search Suggetion list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @param string $zipcode
     * @return array
     */
    public function getSearchSuggetionByZipCode(string $zipcode, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
