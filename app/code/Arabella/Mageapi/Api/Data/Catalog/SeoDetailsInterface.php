<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */

namespace Arabella\Mageapi\Api\Data\Catalog;

/**
 * @api
 */
interface SeoDetailsInterface
{
    const URL_KEY = 'url_key';
    const META_TITLE = 'meta_title';
    const META_KEYWORD = 'meta_keyword';
    const META_DESCRIPTION = 'meta_description';

    /**
     * Get URL key
     *
     * @return string|null
     */
    public function getUrlKey();

    /**
     * Set URL key
     * @param $urlKey
     * @return $this
     */
    public function setUrlKey($urlKey);

    /**
     * Get Meta title
     *
     * @return string|null
     */
    public function getMetaTitle();

    /**
     * Set Meta title
     * @param $metaTitle
     * @return $this
     */
    public function setMetaTitle($metaTitle);
    
    /**
     * Get Meta Keyword
     *
     * @return string|null
     */
    public function getMetaKeyword();

    /**
     * Set Meta Keyword
     * @param $metaKeyword
     * @return $this
     */
    public function setMetaKeyword($metaKeyword);
    
    /**
     * Get Meta Keyword
     *
     * @return string|null
     */
    public function getMetaDescription();

    /**
     * Set Meta Keyword
     * @param $metaDescription
     * @return $this
     */
    public function setMetaDescription($metaDescription);
}
