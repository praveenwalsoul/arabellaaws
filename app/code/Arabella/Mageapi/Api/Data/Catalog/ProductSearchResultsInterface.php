<?php

/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */
namespace Arabella\Mageapi\Api\Data\Catalog;

/**
 * @api
 */
interface ProductSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Arabella\Mageapi\Api\Data\Catalog\ProductInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
