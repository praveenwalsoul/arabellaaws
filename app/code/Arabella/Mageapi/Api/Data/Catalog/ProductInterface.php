<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */

namespace Arabella\Mageapi\Api\Data\Catalog;

/**
 * @api
 */
interface ProductInterface extends ProductOriginalInterface
{
    /**
     * Get list of product options
     *
     * @return \Magento\Catalog\Api\Data\ProductCustomOptionInterface[]|null
     */
    public function getCustomOptions();

    /**
     * Get list of product config options
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\Product\ConfigOptionsInterface[]|null
     */
    public function getConfigOptions();

    /**
     * Get list of product bundle options
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\Product\BundleOptionsInterface[]|null
     */
    public function getBundleOptions();

    /**
     * Get list of product grouped options
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\Product\GroupedOptionsInterface[]|null
     */
    public function getGroupedOptions();

    /**
     * Composes configuration for js
     *
     * @return string
     */
    public function getJsonConfig();

    /**
     * Get JSON encoded configuration array which can be used for JS dynamic
     * price calculation depending on product options
     *
     * @return string
     */
    public function getPriceConfig();

    /**
     * Get Custom fields
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\Product\CustomFieldsInterface[]
     */
    public function getCustomFields();

    /**
     * Get stocks data by product
     *
     * @return \Arabella\Mageapi\Api\Data\Inventory\StockItemInterface[]
     */
    //public function getStocks();

    /**
     * Get data of children product
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductInterface[]|null
     */
    public function getChildrenProducts();

    /**
     * Get qty in online mode
     *
     * @return float|null
     */
    public function getQtyOnline();
    
    /**
     * Get Search Engin Optomization Details
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\SeoDetailsInterface[]
     */
    public function getSeoDetails();
    
    /**
     * Get Product Rating Details
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\RatingDetailsInterface[]|null
     */
    public function getRatingDetails();
}
