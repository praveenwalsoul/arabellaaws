<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */

namespace Arabella\Mageapi\Api\Data\Catalog;

/**
 * @api
 */
interface RatingDetailsInterface
{
    const RATING = 'rating';
    const GLOBAL_REVIEW = 'global_review';

    /**
     * Get Rating
     *
     * @return array|null
     */
    public function getRatings();

    /**
     * Set URL key
     * @param $rating
     * @return $this
     */
    public function setRatings($rating);

    /**
     * Get global review
     *
     * @return int|null
     */
    public function getGlobalReview();

    /**
     * Set Meta title
     * @param $globalReview
     * @return $this
     */
    public function setGlobalReview($globalReview);
}
