<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */

namespace Arabella\Mageapi\Api\Data\Catalog\Product;

/**
 * @api
 */
interface CustomFieldsInterface
{
    const ATTRIBUTE_LABLE = 'lable';
    const ATTRIBUTE_VALUE = 'value';

    /**
     * Get Custom field lable
     *
     * @return string|null
     */
    public function getLable();

    /**
     * Set Custom field lable
     * @param $attrLable
     * @return $this
     */
    public function setLable($attrLable);

    /**
     * Get Custom field value
     *
     * @return string|null
     */
    public function getValue();

    /**
     * Set Custom field value
     * @param $attrValue
     * @return $this
     */
    public function setValue($attrValue);
}
