<?php

/**
 *  Copyright © 2016 Arabella. All rights reserved.
 *
 */
namespace Arabella\Mageapi\Api\Data\Catalog\Product;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface ConfigOptionsInterface
 */
interface ConfigOptionsInterface extends ExtensibleDataInterface
{
    /**#@+
     * Config Options object data keys
     */

    const KEY_CONFIG_OPTIONS = 'config_options';

    /**
     * Gets product items of config options
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\Product\ConfigOptionsInterface
     */
    public function getConfigOptions();

    /**
     * Sets product items of config options
     *
     * @param \Arabella\Mageapi\Api\Data\Catalog\Product\ConfigOptionsInterface $configOptions
     * @return $this
     */
    public function setConfigOptions(\Arabella\Mageapi\Api\Data\Catalog\Product\ConfigOptionsInterface $configOptions);
}
