<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */

namespace Arabella\Mageapi\Api\Data\Catalog\Product;

/**
 * @api
 */
interface AvailableFiltersInterface
{
    const AVAILABLEFILTER = 'availablefilter';

    /**
     * Get Available Filters
     *
     * @return array|null
     */
    public function getAvailablefilter();

    /**
     * Set Available Filters
     * @param $availablefilter
     * @return $this
     */
    public function setAvailablefilter($availablefilter);
}
