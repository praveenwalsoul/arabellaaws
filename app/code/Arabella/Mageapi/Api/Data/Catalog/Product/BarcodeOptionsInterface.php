<?php

/**
 *  Copyright © 2016 Arabella. All rights reserved.
 *
 */
namespace Arabella\Mageapi\Api\Data\Catalog\Product;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface ConfigOptionsInterface
 */
interface BarcodeOptionsInterface extends ExtensibleDataInterface
{
    /**#@+
     * Barcode Options object data keys
     */

    const KEY_CONFIG_OPTIONS = 'barcode_options';

    /**
     * Gets barcode options
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\Product\BarcodeOptionsInterface
     */
    public function getBarcodeOptions();

    /**
     * Sets barcode options
     *
     * @param \Arabella\Mageapi\Api\Data\Catalog\Product\BarcodeOptionsInterface $barcodeOptions
     * @return $this
     */
    public function setBarcodeOptions(\Arabella\Mageapi\Api\Data\Catalog\Product\BarcodeOptionsInterface $barcodeOptions);
}
