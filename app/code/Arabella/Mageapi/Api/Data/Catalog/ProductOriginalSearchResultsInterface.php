<?php

/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */
namespace Arabella\Mageapi\Api\Data\Catalog;

/**
 * @api
 */
interface ProductOriginalSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\ProductOriginalInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Arabella\Mageapi\Api\Data\Catalog\ProductOriginalInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
