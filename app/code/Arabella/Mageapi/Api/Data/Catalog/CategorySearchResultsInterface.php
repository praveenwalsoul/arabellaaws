<?php

/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */
namespace Arabella\Mageapi\Api\Data\Catalog;

/**
 * @api
 */
interface CategorySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Arabella\Mageapi\Api\Data\Catalog\CategoryInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Arabella\Mageapi\Api\Data\Catalog\CategoryInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

    /**
     * Get first categories
     *
     * @return string[]
     */
    public function getFirstCategories();

    /**
     * Set total count.
     *
     * @param string[] $categories
     * @return $this
     */
    public function setFirstCategories($categories);
}
