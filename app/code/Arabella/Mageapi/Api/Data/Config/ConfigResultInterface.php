<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */

namespace Arabella\Mageapi\Api\Data\Config;

interface ConfigResultInterface
{
    /**
     * Set config list.
     *
     * @api
     * @param anyType
     * @return $this
     */
    public function setItems(array $items);

    /**
     * Get config list.
     *
     * @api
     * @return \Arabella\Mageapi\Api\Data\Config\ConfigInterface[]
     */
    public function getItems();

    /**
     * Set total count
     *
     * @param int $count
     * @return $this
     */
    public function setTotalCount($count);

    /**
     * Get total count
     *
     * @return int
     */
    public function getTotalCount();
}
