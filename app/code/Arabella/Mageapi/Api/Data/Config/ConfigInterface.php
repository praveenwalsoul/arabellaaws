<?php
/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */
namespace Arabella\Mageapi\Api\Data\Config;

interface ConfigInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const PATH = 'path';
    const VALUE = 'value';
    /**#@-*/

    /**
     * Get path
     *
     * @api
     * @return string
     */
    public function getPath();

    /**
     * Set path
     *
     * @api
     * @param string $path
     * @return $this
     */
    public function setPath($path);

    /**
     * Get value
     *
     * @api
     * @return string|null
     */
    public function getValue();

    /**
     * Set value
     *
     * @api
     * @param string $value
     * @return $this
     */
    public function setValue($value);
}
