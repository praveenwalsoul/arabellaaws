<?php

/**
 *  Copyright © 2020 Arabella. All rights reserved.
 *
 */

namespace Arabella\Mageapi\Helper;

/**
 * class \Arabella\Mageapi\Helper\Data
 *
 * Web POS Data helper
 * Methods:
 *  formatCurrency
 *  formatDate
 *  formatPrice
 *  getCurrentDatetime
 *  getModel
 *  getObjectManager
 *  getStore
 *  getStoreConfig
 *  htmlEscape
 *
 * @category    Magestore
 * @package     Magestore_Webpos
 * @module      Webpos
 * @author      Magestore Developer
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MAGENTO_EE = 'Enterprise';
    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    /**
     *
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;

    protected $storeRepository;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param  \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository
    ) {
        $this->productMetadata  = $productMetadata;
        $this->_storeManager    = $storeManager;
        $this->_objectManager   = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_localeDate      = $localeDate;
        $this->_dateTime        = $dateTime;
        $this->storeRepository  = $storeRepository;
        parent::__construct($context);
    }

    /**
     *
     * @return Magento store
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }

    /**
     *
     * @param string $data
     * @return string
     */
    public function formatCurrency($data)
    {
        $currencyHelper = $this->_objectManager->get('Magento\Framework\Pricing\Helper\Data');
        return $currencyHelper->currency($data, true, false);
    }

    /**
     *
     * @param string $data
     * @return string
     */
    public function formatPrice($data)
    {
        $checkoutHelper = $this->_objectManager->get('Magento\Checkout\Helper\Data');
        return $checkoutHelper->formatPrice($data);
    }

    /**
     *
     * @param string $data
     * @return string
     */
    public function formatDate($data, $format = '')
    {
        $format = ($format == '') ? 'M d,Y H:i:s a' : $format;
        return $this->_localeDate->date(new \DateTime($data))->format($format);
    }

    /**
     *
     * @param string $path
     * @return string
     */
    public function getStoreConfig($path)
    {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     *
     * @return Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getOrderCollection()
    {
        return $this->_objectManager->create('Magento\Sales\Model\ResourceModel\Order\Collection');
    }

    /**
     *
     * @param string $till_id
     */
    public function setTillData($till_id)
    {
        try {
            if (!$till_id instanceof Arabella\Mageapi\Model\Till) {
                $till = $this->_objectManager->create('Arabella\Mageapi\Model\Till')->load($till_id);
            } else {
                $till = $till_id;
            }
            $currentTill = $this->_webposSession->getTill();
            if ($till->getId()) {
                $this->_webposSession->setTill($till);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     *
     * @return string
     */
    public function getCurrentDatetime()
    {
        return $this->_dateTime->gmtDate();
    }

    /**
     * string class name
     * @return Model
     */
    public function getModel($class)
    {
        return $this->_objectManager->get($class);
    }

    /**
     *
     * @param string $str
     * @return string
     */
    public function htmlEscape($str)
    {
        return htmlspecialchars($str);
    }

    /**
     *
     * @return array
     */
    public function getOfflineConfig()
    {
        $configData  = array();
        $configItems = array('offline/enable',
            'offline/network_check_interval',
            'offline/check_stock_interval',
            'offline/data_load_interval',
            'offline/product_per_request',
            'offline/customer_per_request',
            'product_search/search_offline',
        );
        foreach ($configItems as $configItem) {
            $config             = explode('/', $configItem);
            $value              = $config[1];
            $configData[$value] = $this->getStoreConfig('webpos/' . $configItem);
        }
        if (empty($configData['data_load_interval'])) {
            $configData['data_load_interval'] = 0;
        }

        if (empty($configData['product_per_request'])) {
            $configData['product_per_request'] = 50;
        }

        if (empty($configData['customer_per_request'])) {
            $configData['customer_per_request'] = 100;
        }

        return $configData;
    }

    /**
     *
     * @return \Magento\Framework\App\ObjectManager
     */
    public function getObjectManager()
    {
        return $this->_objectManager;
    }

    /**
     * @param $message
     * @param string $type
     */
    public function addLog($message, $type = '')
    {
        switch ($type) {
            case 'info':
                $this->_logger->info($message);
                break;
            case 'debug':
                $this->_logger->debug($message);
                break;
            case 'info':
                $this->_logger->info($message);
                break;
            case 'notice':
                $this->_logger->notice($message);
                break;
            case 'warning':
                $this->_logger->warning($message);
                break;
            case 'error':
                $this->_logger->error($message);
                break;
            case 'emergency':
                $this->_logger->emergency($message);
                break;
            case 'critical':
                $this->_logger->critical($message);
                break;
            case 'alert':
                $this->_logger->alert($message);
                break;
            default:
                $this->_logger->error($message);
                break;
        }
    }

    /**
     *
     * @return Magento store
     */
    public function getStoreManager()
    {
        return $this->_storeManager;
    }

    public function getStoreView()
    {
        $stores = [];
        foreach ($this->storeRepository->getList() as $store) {
            if ($store->getCode() != 'admin') {
                $stores[] = array(
                    'id'    => $store->getId(),
                    'value' => $store->getCode(),
                    'text'  => $store->getName(),
                );
            }
        }
        return $stores;
    }

    /**
     * Set secure url checkout is secure for current store.
     *
     * @param string $route
     * @param array $params
     * @return string
     */
    protected function _getUrl($route, $params = [])
    {
        $params['_type'] = \Magento\Framework\UrlInterface::URL_TYPE_LINK;
        if (isset($params['is_secure'])) {
            $params['_secure'] = (bool) $params['is_secure'];
        } elseif ($this->_storeManager->getStore()->isCurrentlySecure()) {
            $params['_secure'] = true;
        }
        return parent::_getUrl($route, $params);
    }

    /**
     * Retrieve url
     *
     * @param array $params
     * @return string
     */
    public function getUrl($route, $params)
    {
        return $this->_getUrl($route, $params);
    }

    /**
     * Dispatch event
     * @param string $eventName
     * @param array $params
     * @return $this
     */
    public function dispatchEvent($eventName, $params = [])
    {
        $this->_eventManager->dispatch($eventName, $params);
        return $this;
    }
}
