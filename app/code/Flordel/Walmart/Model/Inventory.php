<?php
namespace Flordel\Walmart\Model;

use \Flordel\Walmart\Model\HTTPRequester;

class Inventory
{

    public function getItemInventory($sku)
    {
    	//1976663, NOT207585-B006S5VIS4
        $client = new HTTPRequester(true);
        $items  = $client->getItemInventory($sku);

        print_r(json_decode($items['ResponseBody'], true));
    }
    
    public function updateInventory($items)
    {
    	//1976663, NOT207585-B006S5VIS4
        $client = new HTTPRequester(true);
        $items  = $client->updateItemInventory($items);

        print_r(json_decode($items['ResponseBody'], true));
    }
}
