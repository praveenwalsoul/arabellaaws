<?php
namespace Flordel\Walmart\Model;

use Flordel\Walmart\Model\ClientException;
use Flordel\Walmart\Model\Service\OAuthTokenDTO;
use DateTime;
use DateTimeZone;

class HTTPRequester
{
    private $_config        = [];
    private const PRODUCTION_URL = "https://marketplace.walmartapis.com";
    private const SANDBOX_URL = "https://sandbox.walmartapis.com";
    private const SANDBOX_CLIENT_ID = "d49062e7-b76c-4b26-8bc8-82ffeefd89cb";
    private const SANDBOX_SECRET_KEY = "AMRShdNDwXw4rgGH4b0UvN4n10N76PLLLVgjSCXuak1pL1dXRiNMGkpZXig-nmom9ErbszLERn0rlsRYxRGH3u0";

    private const CLIENT_ID = "551e4301-7adf-4653-a37f-5a638c02d4ce";
    private const SECRET_KEY = "ITOViDwqSGqi4SGx1WVDaHu4DMiRXVjT7QpHbLwu_28jy1glDATksbnoCPghcujtXZvd2CnDQAQPsQzqyG4YIA";
    private $isLive         = false;

    public function __construct($production = false)
    {
        $this->isLive = $production;
    }

    public function getUrl()
    {
        $url = self::SANDBOX_URL;
        if ($this->isLive) {
            $url = self::PRODUCTION_URL;
        }
        return $url;
    }

    public function generateOauthAccessToken()
    {
        $strResponse          = null;
        $params['grant_type'] = 'client_credentials';
        $query                = http_build_query($params);

        if ($this->isLive) {
            $encodeData           = base64_encode(self::CLIENT_ID . ":" . self::SECRET_KEY);
        } else {
            $encodeData           = base64_encode(self::SANDBOX_CLIENT_ID . ":" . self::SANDBOX_SECRET_KEY);
        }

        $tokenUrl             = $this->getUrl()."/v3/token";
        $allHeadersStr[]      = "Authorization: Basic " . $encodeData;
        $allHeadersStr[]      = "Accept: application/json";
        $allHeadersStr[]      = "WM_SVC.NAME: Walmart Marketplace";
        $allHeadersStr[]      = "WM_QOS.CORRELATION_ID: " . abs(crc32(uniqid('walmart_', true)));
        $ch                   = curl_init();
        curl_setopt($ch, CURLOPT_URL, $tokenUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $allHeadersStr);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        if ($response === false) {
            $exProps["Message"]   = curl_error($ch);
            $exProps["ErrorType"] = "HTTP";
            curl_close($ch);
            throw new Exception($exProps["Message"]);
        }
        curl_close($ch);
        $data = $this->_extractHeadersAndBody($response);
        $responseBody = json_decode($data['ResponseBody'], true);
        return $responseBody['access_token'];
    }


    public function getItems(int $offset, int $limit)
    {
        $accessToken = $this->generateOauthAccessToken();
        $url = $this->getUrl()."/v3/items";
        $params = [];
        if ($offset >= 0) {
            $params["offset"] = $offset;
        }
        if ($limit >= 1) {
            $params["limit"] = $limit;
        }

        if ($this->isLive) {
            $encodeData           = base64_encode(self::CLIENT_ID . ":" . self::SECRET_KEY);
        } else {
            $encodeData           = base64_encode(self::SANDBOX_CLIENT_ID . ":" . self::SANDBOX_SECRET_KEY);
        }

        $allHeadersStr                          = [];
        $allHeadersStr['Authorization']         = "Basic " . $encodeData;
        $allHeadersStr['Accept']                = "application/json";
        $allHeadersStr['WM_SVC.NAME']           = "Walmart Marketplace";
        $allHeadersStr['WM_SEC.ACCESS_TOKEN']   = $accessToken;
        $allHeadersStr['WM_QOS.CORRELATION_ID'] = abs(crc32(uniqid('walmart_', true)));
        $allHeadersStr['Content-Length']        = '0';

        return $this->invoke($url, $allHeadersStr, $params, 'GET');
    }


    /**
     * Get item inventory
     *
     * @param sku
     * @return array
     */
    public function getItemInventory(String $sku)
    {
        $accessToken = $this->generateOauthAccessToken();
        $url = $this->getUrl()."/v3/inventory";

        $params['sku'] = $sku;

        if ($this->isLive) {
            $encodeData           = base64_encode(self::CLIENT_ID . ":" . self::SECRET_KEY);
        } else {
            $encodeData           = base64_encode(self::SANDBOX_CLIENT_ID . ":" . self::SANDBOX_SECRET_KEY);
        }

        $allHeadersStr                          = [];
        $allHeadersStr['Authorization']         = "Basic " . $encodeData;
        $allHeadersStr['Accept']                = "application/json";
        $allHeadersStr['WM_SVC.NAME']           = "Walmart Marketplace";
        $allHeadersStr['WM_SEC.ACCESS_TOKEN']   = $accessToken;
        $allHeadersStr['WM_QOS.CORRELATION_ID'] = abs(crc32(uniqid('walmart_', true)));
        $allHeadersStr['Content-Length']        = '0';

        return $this->invoke($url, $allHeadersStr, $params, 'GET');
    }

    /**
     * Update product title, description, price and features
     *
     * @param items
     * @return array
     */
    public function updateItems($items)
    {
        $jsonData = $this->arrayToJson($items);
        $mediaPath = $this->getMediaPath();
        $filename  = 'productfeed.json';
        $fullFilePath = $mediaPath . '/Amazon/' . $filename;
        $file      = @fopen($fullFilePath, 'w');
        fwrite($file, $jsonData);
        fclose($file);

        if (filesize($fullFilePath) <= 10 * 1024 * 1024) {
            $accessToken = $this->generateOauthAccessToken();
            //echo $accessToken;die;
            $url = $this->getUrl()."/v3/feeds?feedType=inventory";
            $params['file'] = $this->getCurlValue($fullFilePath, $contentType, $filename);

            if ($this->isLive) {
                $encodeData           = base64_encode(self::CLIENT_ID . ":" . self::SECRET_KEY);
            } else {
                $encodeData           = base64_encode(self::SANDBOX_CLIENT_ID . ":" . self::SANDBOX_SECRET_KEY);
            }

            $allHeadersStr                          = [];
            $allHeadersStr['Authorization']         = "Basic " . $encodeData;
            $allHeadersStr['WM_SEC.ACCESS_TOKEN']   = $accessToken;
            $allHeadersStr['WM_QOS.CORRELATION_ID'] = abs(crc32(uniqid('walmart_', true)));
            $allHeadersStr['WM_SVC.NAME']           = "Walmart Marketplace";
            $allHeadersStr['Content-Type']          = "multipart/form-data";
            $allHeadersStr['Accept']                = "application/json";

            return $this->invoke($url, $allHeadersStr, $params, 'POST');
        } else {
            $this->writeLog('Feed Size is more than 10MB', 'create');
        }
    }

    /**
     * Update SKU inventory for V2
     *
     * @param items
     * @return array
     */
    public function updateItemInventory($items)
    {
        $jsonData = $this->arrayToJson($items);
        $mediaPath = $this->getMediaPath();
        $filename  = 'inventoryfeed.json';
        $fullFilePath = $mediaPath . '/Amazon/' . $filename;
        $file      = @fopen($fullFilePath, 'w');
        fwrite($file, $jsonData);
        fclose($file);

        if (filesize($fullFilePath) <= 25 * 1024 * 1024) {
            $accessToken = $this->generateOauthAccessToken();
            $url = $this->getUrl()."/v3/feeds?feedType=inventory";
            $contentType = "application/json";
            $params['file'] = $this->getCurlValue($fullFilePath, $contentType, $filename);

            if ($this->isLive) {
                $encodeData           = base64_encode(self::CLIENT_ID . ":" . self::SECRET_KEY);
            } else {
                $encodeData           = base64_encode(self::SANDBOX_CLIENT_ID . ":" . self::SANDBOX_SECRET_KEY);
            }

            $allHeadersStr                          = [];
            $allHeadersStr['Authorization']         = "Basic " . $encodeData;
            $allHeadersStr['WM_SEC.ACCESS_TOKEN']   = $accessToken;
            $allHeadersStr['WM_QOS.CORRELATION_ID'] = abs(crc32(uniqid('walmart_', true)));
            $allHeadersStr['WM_SVC.NAME']           = "Walmart Marketplace";
            $allHeadersStr['Content-Type']          = "multipart/form-data";
            $allHeadersStr['Accept']                = "application/json";

            return $this->invoke($url, $allHeadersStr, $params, 'POST');
        } else {
            $this->writeLog('Feed Size is more than 25MB', 'create');
        }
    }


    public function getOrders()
    {
        $accessToken = $this->generateOauthAccessToken();
        //echo $accessToken;die;
        $url = $this->getUrl()."/v3/orders/released";
        $params['createdStartDate'] = $this->getBackDate();
        $params['createdEndDate'] = $this->getCurrentDate();
        $params['limit'] = 200;

        if ($this->isLive) {
            $encodeData           = base64_encode(self::CLIENT_ID . ":" . self::SECRET_KEY);
        } else {
            $encodeData           = base64_encode(self::SANDBOX_CLIENT_ID . ":" . self::SANDBOX_SECRET_KEY);
        }

        $allHeadersStr                          = [];
        $allHeadersStr['Authorization']         = "Basic " . $encodeData;
        $allHeadersStr['WM_SEC.ACCESS_TOKEN']   = $accessToken;
        $allHeadersStr['WM_QOS.CORRELATION_ID'] = abs(crc32(uniqid('walmart_', true)));
        $allHeadersStr['WM_SVC.NAME']           = "Walmart Marketplace";
        $allHeadersStr['Content-Type']          = "application/json";
        $allHeadersStr['Accept']                = "application/json";

        $data = $this->invoke($url, $allHeadersStr, $params, 'GET');
        if ($data['Status'] == 200) {
            $data = json_decode($data['ResponseBody'], true);
            $results = $data['list']['elements']['order'];
            $results = (count($results) > 0)?$results:[];
            $orders = $this->orderFormat($results);
            return $orders;
        } else {
             $this->writeLog('Some error occured while retrieving orders', 'create');
        }
    }

    private function orderFormat($results)
    {
        $orders = [];
        foreach ($results as $key => $order) {
            $shippingInfo = $order['shippingInfo'];
            $postalAddress = $shippingInfo['postalAddress'];
            foreach ($order['orderLines']['orderLine'] as $item) {
                $itemCharges = $item['charges']['charge'];
                $itemcurrency = $itemCharges[0]['chargeAmount']['currency'];
                $itemCurrency = ($itemcurrency == 'USD')?'$':$itemcurrency;

                $orders[] = [
                    'purchaseOrderId'=>$order['purchaseOrderId'],
                    'customerOrderId' => $order['customerOrderId'],
                    'orderDate' => $this->convertMilisecondToData($order['orderDate']),
                    'estimatedShipDate' => $this->convertMilisecondToData($shippingInfo['estimatedShipDate']),
                    'estimatedDeliveryDate' => $this->convertMilisecondToData($shippingInfo['estimatedDeliveryDate']),
                    'customerName' => $postalAddress['name'],
                    'customerShippingAddress' => $postalAddress['city'].', '.$postalAddress['state'].', '.$postalAddress['postalCode'].', '.$postalAddress['country'].', Phone:'.$shippingInfo['phone'],
                    'address2' => $postalAddress['address2'],
                    'address2' => $postalAddress['address2'],
                    'customerPhone' => $shippingInfo['phone'],
                    'email' => $order['customerEmailId'],
                    'city' => $postalAddress['city'],
                    'state' => $postalAddress['state'],
                    'zip' => $postalAddress['postalCode'],
                    'status' => 'unshipped',
                    'itemName' => $item['item']['productName'],
                    'sku' => $item['item']['sku'],
                    'itemPrice' => $itemCurrency.$itemCharges[0]['chargeAmount']['amount'],
                    'shipping_price' => $itemCurrency.$itemCharges[1]['chargeAmount']['amount'],
                    'tax' => $itemCurrency.$itemCharges[0]['tax']['taxAmount']['amount'],
                    'qty' => $item['orderLineQuantity']['amount'],
                    'carrier' => $shippingInfo['methodCode']
                ];
            }
        }

        return $orders;
    }

    private function arrayToJson($items)
    {
        if (! is_array($items)) {
            throw new \Exception('Items is not an array', 14663491774);
        }

        $data = ['InventoryHeader' => ['version' => 1.4], 'Inventory' =>[]];

        foreach ($items as $sku => $qty) {
            $data['Inventory'][] = [
                'sku' => $sku,
                 'quantity' => [
                    'unit'=> 'EACH',
                    'amount' => $qty
                    ]
            ];
        }

        return json_encode($data);
    }

    public function getMediaPath()
    {
        $om            = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem    = $om->get('Magento\Framework\Filesystem');
        $directoryList = $om->get('Magento\Framework\App\Filesystem\DirectoryList');
        $mediapath     = $filesystem->getDirectoryRead($directoryList::MEDIA)->getAbsolutePath();
        return $mediapath;
    }

    /**
     * Collapse multiple whitespace characters into a single ' ' character.
     * @param $s
     * @return string
     */
    private function collapseWhitespace($s)
    {
        return preg_replace('/ {2,}|\s/', ' ', $s);
    }

    /**
     * Collapse multiple whitespace characters into a single ' ' and backslash escape '\',
     * and '/' characters from a string.
     * @param $s
     * @return string
     */
    private function quoteApplicationName($s)
    {
        $quotedString = $this->collapseWhitespace($s);
        $quotedString = preg_replace('/\\\\/', '\\\\\\\\', $quotedString);
        $quotedString = preg_replace('/\//', '\\/', $quotedString);

        return $quotedString;
    }

    /**
     * Collapse multiple whitespace characters into a single ' ' and backslash escape '\',
     * and '(' characters from a string.
     *
     * @param $s
     * @return string
     */
    private function quoteApplicationVersion($s)
    {
        $quotedString = $this->collapseWhitespace($s);
        $quotedString = preg_replace('/\\\\/', '\\\\\\\\', $quotedString);
        $quotedString = preg_replace('/\\(/', '\\(', $quotedString);

        return $quotedString;
    }

    /**
     * Collapse multiple whitespace characters into a single ' ' and backslash escape '\',
     * and '=' characters from a string.
     *
     * @param $s
     * @return unknown_type
     */
    private function quoteAttributeName($s)
    {
        $quotedString = $this->collapseWhitespace($s);
        $quotedString = preg_replace('/\\\\/', '\\\\\\\\', $quotedString);
        $quotedString = preg_replace('/\\=/', '\\=', $quotedString);

        return $quotedString;
    }

    /**
     * Collapse multiple whitespace characters into a single ' ' and backslash escape ';', '\',
     * and ')' characters from a string.
     *
     * @param $s
     * @return unknown_type
     */
    private function quoteAttributeValue($s)
    {
        $quotedString = $this->collapseWhitespace($s);
        $quotedString = preg_replace('/\\\\/', '\\\\\\\\', $quotedString);
        $quotedString = preg_replace('/\\;/', '\\;', $quotedString);
        $quotedString = preg_replace('/\\)/', '\\)', $quotedString);

        return $quotedString;
    }

    // Private API ------------------------------------------------------------//

    /**
     * Invoke request and return response
     */
    private function invoke($url, $headeres, $parameters, $requestType = 'POST')
    {
        try {
            if (empty($url)) {
                throw new ClientException(
                    array(
                        'ErrorCode' => 'InvalidServiceURL',
                        'Message'   => "Missing serviceUrl configuration value. You may obtain a list of valid MWS URLs by consulting the MWS Developer's Guide, or reviewing the sample code published along side this library.",
                    )
                );
            }
            $retries    = 0;
            for (;;) {
                $response = $this->_httpPost($url, $headeres, $parameters, $requestType);
                $status   = $response['Status'];
                if ($status == 200) {
                    $responseData = array(
                        'Status'=>$status,
                        'ResponseBody' => $response['ResponseBody'],
                        'params' => $parameters
                        );
                    return $responseData;
                }
                if ($status == 500 && $this->_pauseOnRetry(++$retries)) {
                    continue;
                }
                throw $this->_reportAnyErrors(
                    $response['ResponseBody'],
                    $status
                );
            }
        } catch (ClientException $se) {
            throw $se;
        } catch (Exception $t) {
            throw new ClientException(array('Exception' => $t, 'Message' => $t->getMessage()));
        }
    }
    

    /**
     * Look for additional error strings in the response and return formatted exception
     */
    private function _reportAnyErrors($responseBody, $status, Exception $e = null)
    {
        $exProps                           = array();
        $exProps["StatusCode"]             = $status;

        libxml_use_internal_errors(true); // Silence XML parsing errors
        $xmlBody = simplexml_load_string($responseBody);

        if ($xmlBody !== false) {
            // Check XML loaded without errors
            $exProps["XML"]       = $responseBody;
            $exProps["ErrorCode"] = $xmlBody->Error->Code;
            $exProps["Message"]   = $xmlBody->Error->Message;
            $exProps["ErrorType"] = !empty($xmlBody->Error->Type) ? $xmlBody->Error->Type : "Unknown";
            $exProps["RequestId"] = !empty($xmlBody->RequestID) ? $xmlBody->RequestID : $xmlBody->RequestId; // 'd' in RequestId is sometimes capitalized
        } else {
            // We got bad XML in response, just throw a generic exception
            $exProps["Message"] = "Internal Error";
        }
        return new ClientException($exProps);
    }

    /**
     * Perform HTTP post with exponential retries on error 500 and 503
     *
     */
    private function _httpPost($url, $allHeaders, array $parameters, $requestType = 'POST')
    {
        $allHeadersStr              = array();
        foreach ($allHeaders as $name => $val) {
            $str = $name . ": ";
            if (isset($val)) {
                $str = $str . $val;
            }
            $allHeadersStr[] = $str;
        }

        $ch = curl_init();
        if ($requestType == 'POST') {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        } else {
            $query  = $this->_getParametersAsString($parameters);
            curl_setopt($ch, CURLOPT_URL, $url.'?'.$query);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $allHeadersStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = "";
        $response = curl_exec($ch);
        if ($response === false) {
            $exProps["Message"]   = curl_error($ch);
            $exProps["ErrorType"] = "HTTP";
            curl_close($ch);
            throw new ClientException($exProps);
        }

        curl_close($ch);
        return $this->_extractHeadersAndBody($response);
    }


    private function _extractHeadersAndBody($response)
    {
        //First split by 2 'CRLF'
        $responseComponents = preg_split("/(?:\r?\n){2}/", $response, 2);
        $body               = null;
        for ($count = 0;
            $count < count($responseComponents) && $body == null;
            $count++) {
            $headers        = $responseComponents[$count];
            $responseStatus = $this->_extractHttpStatusCode($headers);

            if ($responseStatus != null &&
                $this->_httpHeadersHaveContent($headers)) {
                //The body will be the next item in the responseComponents array
                $body = $responseComponents[++$count];
            }
        }

        //If the body is null here then we were unable to parse the response and will throw an exception
        if ($body == null) {
            $exProps["Message"]   = "Failed to parse valid HTTP response (" . $response . ")";
            $exProps["ErrorType"] = "HTTP";
            throw new ClientException($exProps);
        }

        return array(
            'Status'       => $responseStatus,
            'ResponseBody' => $body,
        );
    }

    /**
     * parse the status line of a header string for the proper format and
     * return the status code
     *
     * Example: HTTP/1.1 200 OK
     * ...
     * returns String statusCode or null if the status line can't be parsed
     */
    private function _extractHttpStatusCode($headers)
    {
        $statusCode = null;
        if (1 === preg_match("/(\\S+) +(\\d+) +([^\n\r]+)(?:\r?\n|\r)/", $headers, $matches)) {
            //The matches array [entireMatchString, protocol, statusCode, the rest]
            $statusCode = $matches[2];
        }
        return $statusCode;
    }

    /**
     * Tries to determine some valid headers indicating this response
     * has content.  In this case
     * return true if there is a valid "Content-Length" or "Transfer-Encoding" header
     */
    private function _httpHeadersHaveContent($headers)
    {
        return (1 === preg_match("/[cC]ontent-[lL]ength: +(?:\\d+)(?:\\r?\\n|\\r|$)/", $headers) ||
            1 === preg_match("/Transfer-Encoding: +(?!identity[\r\n;= ])(?:[^\r\n]+)(?:\r?\n|\r|$)/i", $headers));
    }

    /**
     * Set curl options relating to SSL. Protected to allow overriding.
     * @param $ch curl handle
     */
    protected function setSSLCurlOptions($ch)
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    }

    /**
     * Exponential sleep on failed request
     *
     * @param retries current retry
     */
    private function _pauseOnRetry($retries)
    {
        if ($retries <= $this->_config['MaxErrorRetry']) {
            $delay = (int) (pow(4, $retries) * 100000);
            usleep($delay);
            return true;
        }
        return false;
    }

    /**
     * Add authentication related and version parameters
     */
    private function _addRequiredParameters(array $parameters)
    {
        $parameters['AWSAccessKeyId']   = $this->_awsAccessKeyId;
        $parameters['Timestamp']        = $this->_getFormattedTimestamp();
        $parameters['Version']          = self::SERVICE_VERSION;
        $parameters['SignatureVersion'] = $this->_config['SignatureVersion'];
        if ($parameters['SignatureVersion'] > 1) {
            $parameters['SignatureMethod'] = $this->_config['SignatureMethod'];
        }
        $parameters['Signature'] = $this->_signParameters($parameters, $this->_awsSecretAccessKey);

        return $parameters;
    }

    /**
     * Convert paremeters to Url encoded query string
     */
    private function _getParametersAsString(array $parameters)
    {
        $queryParameters = array();
        foreach ($parameters as $key => $value) {
            $queryParameters[] = $key . '=' . $this->_urlencode($value);
        }
        return implode('&', $queryParameters);
    }

    private function _urlencode($value)
    {
        return str_replace('%7E', '~', rawurlencode($value));
    }


    /**
     * Write Log
     *
     * @param $data
     * @param $type
     */
    public function writeLog($data, $type)
    {
        switch ($type) {
            case 'create':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/walmart_order.log');
                break;
            case 'cancel':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/walmart_cancel_order.log');
                break;
            case 'inventory':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/walmart_inventory.log');
                break;    
            default:
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/walmart_order.log');
        }
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($data, true));
    }

    /**
     * Formats date as ISO 8601 timestamp
     */
    public function getFormattedTimestamp($date = null)
    {
        $UTC   = new DateTimeZone("UTC");
        $newTZ = new DateTimeZone("America/Chicago");
        $time  = time();
        if (isset($date)) {
            $time = strtotime($date);
        }
        $dateTime = new DateTime(gmdate("Y-m-dTH:i:s.Z", $time), $UTC);
        $dateTime->setTimezone($newTZ);
        return $dateTime->format(DATE_ISO8601);
    }

    public function getBackDate()
    {
        $date = date("Y-m-d 00:00", strtotime("-5 day"));
        return $this->getFormattedTimestamp($date);
    }

    public function getCurrentDate()
    {
        $date = date("Y-m-d 00:00");
        return $this->getFormattedTimestamp($date);
    }

    public function convertMilisecondToData($milisecond)
    {
        return date('Y-m-d H:i:s', ($milisecond/1000));
    }
    
    /**
     * Get curl file
     */
    public function getCurlValue($filename, $contentType, $postname)
    {
        // PHP 5.5 introduced a CurlFile object that deprecates the old @filename syntax
        if (function_exists('curl_file_create')) {
            return curl_file_create($filename, $contentType, $postname);
        }

        // Use the old style if using an older version of PHP
        $value = "@{$filename};filename=" . $postname;
        if ($contentType) {
            $value .= ';type=' . $contentType;
        }

        return $value;
    }
}
