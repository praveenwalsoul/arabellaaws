<?php
namespace Flordel\Walmart\Model\Service;

class OAuthTokenDTO extends \Flordel\Walmart\Model\ServiceModel
{
    public function __construct($data = null)
    {
        $this->_fields = array(
            'accesstoken' => array('FieldValue' => null, 'FieldType' => 'string'),
            'tokentype'   => array('FieldValue' => null, 'FieldType' => 'string'),
            'expiresin'   => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }

    /**
     * Get the value of the Name property.
     *
     * @return String Accesstoken.
     */
    public function getAccesstoken()
    {
        return $this->_fields['accesstoken']['FieldValue'];
    }

    /**
     * Set the value of the Name property.
     *
     * @param string accesstoken
     * @return this instance
     */
    public function setAccesstoken($value)
    {
        $this->_fields['accesstoken']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if accesstoken is set.
     *
     * @return true if accesstoken is set.
     */
    public function isAccesstoken()
    {
        return !is_null($this->_fields['accesstoken']['FieldValue']);
    }

    /**
     * Set the value of Name, return this.
     *
     * @param name
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withAccesstoken($value)
    {
        $this->setAccesstoken($value);
        return $this;
    }

    /**
     * Get the value of the tokentype property.
     *
     * @return String tokentype.
     */
    public function getTokentype()
    {
        return $this->_fields['tokentype']['FieldValue'];
    }

    /**
     * Set the value of the tokentype property.
     *
     * @param string tokentype
     * @return this instance
     */
    public function setTokentype($value)
    {
        $this->_fields['tokentype']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if tokentype is set.
     *
     * @return true if tokentype is set.
     */
    public function isSetTokentype()
    {
        return !is_null($this->_fields['tokentype']['FieldValue']);
    }

    /**
     * Set the value of tokentype, return this.
     *
     * @param tokentype
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withTokentype($value)
    {
        $this->setTokentype($value);
        return $this;
    }

    /**
     * Get the value of the expiresin property.
     *
     * @return String expiresin.
     */
    public function getExpiresin()
    {
        return $this->_fields['expiresin']['FieldValue'];
    }

    /**
     * Set the value of the expiresin property.
     *
     * @param string expiresin
     * @return this instance
     */
    public function setExpiresin($value)
    {
        $this->_fields['expiresin']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if expiresin is set.
     *
     * @return true if expiresin is set.
     */
    public function isSetExpiresin()
    {
        return !is_null($this->_fields['expiresin']['FieldValue']);
    }

    /**
     * Set the value of expiresin, return this.
     *
     * @param expiresin
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withExpiresin($value)
    {
        $this->setExpiresin($value);
        return $this;
    }
}
