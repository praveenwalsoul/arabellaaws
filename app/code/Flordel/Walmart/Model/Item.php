<?php
namespace Flordel\Walmart\Model;

use \Flordel\Walmart\Model\HTTPRequester;

class Item
{
    public function list()
    {
        $client = new HTTPRequester(true);
        $items  = $client->getItems(0, 10);
        return json_decode($items['ResponseBody'], true);
    }

    public function getItemInventory($sku)
    {
        //1976663, NOT207585-B006S5VIS4
        $client = new HTTPRequester(true);
        $items  = $client->getItemInventory($sku);
        return json_decode($items['ResponseBody'], true);
    }

    public function updateItemInventory($items)
    {
        $client = new HTTPRequester(true);
        $items  = $client->updateItemInventory($items);
        return json_decode($items['ResponseBody'], true);
    }

    public function getOrders()
    {
        $client = new HTTPRequester(true);
        $items  = $client->getOrders();
        return $items;
    }
}
