<?php
namespace Flordel\Amazon\Plugin\Block\Adminhtml;

use Magento\Sales\Block\Adminhtml\Order as salesOrder;

class Order
{
    public function beforeSetLayout(salesOrder $subject)
    {
        $subject->addButton(
            'order_custom_button',
            [
                'label'   => __('Update Amazon Order'),
                'class'   => __('primary'),
                'id'      => 'amz-order-sync-button',
                'onclick' => 'setLocation(\'' . $subject->getUrl('flordel_amazon/action/updateamzorder') . '\')',
            ]
        );
    }
}
