<?php
namespace Flordel\Amazon\Plugin\Block\Adminhtml;

use Magento\Catalog\Block\Adminhtml\Product as catalogProduct;

class Product
{
    public function beforeSetLayout(catalogProduct $subject)
    {
        $subject->addButton(
            'order_custom_button',
            [
                'label' => __('Update Inventory'),
                'class' => __('primary'),
                'id' => 'product-inventory-sync-button',
                'onclick' => 'setLocation(\'' . $subject->getUrl('flordel_amazon/action/updateinventory') . '\')'
            ]
        );
    }
}
