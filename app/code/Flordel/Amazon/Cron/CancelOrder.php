<?php
namespace Flordel\Amazon\Cron;

class CancelOrder
{

    protected $helper;
    
    protected $listOrders;
    
    /**
     * @var \Flordelcampo\Order\Controller\Adminhtml\Amazon\Create
     */
    protected $create;
    
    public function __construct(
        \Flordel\Amazon\Helper\Data $helper,
        \Flordel\Amazon\Model\ListOrders $listOrders,
        \Flordelcampo\Order\Controller\Adminhtml\Amazon\Create $create
    ) {
        $this->helper    = $helper;
        $this->listOrders = $listOrders;
        $this->create = $create;
    }
    
	public function execute()
	{
	    $startDate = $this->helper->getBackDate();
        $endDate = $this->helper->getCurrentDate();
	    $data = $this->listOrders->getCancelledOrders($startDate, $endDate);
        $updateResponse = $this->create->updateStatusForAmazonCancelOrders($data);
        $this->writeLog($updateResponse, 'cancel');

		return $this;
	}
}