<?php
namespace Flordel\Amazon\Cron;

class TrackingUpdate
{
    protected $helper;
    protected $amazonTrackingUpdate;

    public function __construct(
        \Flordel\Amazon\Model\AmazonTrackingUpdate $amazonTrackingUpdate
    ) {
        $this->amazonTrackingUpdate = $amazonTrackingUpdate;
    }

    public function execute()
    {
        $data = $this->amazonTrackingUpdate->updateTrackingNumber();
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cron.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__METHOD__);
        $logger->info('TrackingUpdate '.get_class($this->amazonTrackingUpdate).' => '.print_r($data, true));

        return $this;
    }
}
