<?php
namespace Flordel\Amazon\Cron;

class OrderUpdate
{
    protected $helper;
    protected $listOrdersReport;
    /**
     * @var \Flordelcampo\Order\Controller\Adminhtml\Amazon\Create
     */
    protected $create;

    public function __construct(
        \Flordel\Amazon\Helper\Data $helper,
        \Flordel\Amazon\Model\ListOrdersReport $listOrdersReport,
        \Flordelcampo\Order\Controller\Adminhtml\Amazon\Create $create
    ) {
        $this->helper    = $helper;
        $this->listOrdersReport = $listOrdersReport;
        $this->create = $create;
    }

    public function execute()
    {
        $apiCsvFilePath = $this->listOrdersReport->getOrderReports();
        $createResponse = $this->create->createOrderForAmazonApiOrders($apiCsvFilePath);

        $this->helper->writeLog($createResponse, 'create');

        return $this;
    }
}
