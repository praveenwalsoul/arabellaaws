<?php
namespace Flordel\Amazon\Cron;

class Inventoryupdate
{
    /**
     * @var \Flordel\Amazon\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaInterface
     */
    protected $searchCriteria;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroup
     */
    protected $filterGroup;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $productStatus;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $productVisibility;

    protected $walmartItem;

    /**
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param \Flordel\Amazon\Helper\Data $helper
     * @param \Flordel\Walmart\Model\Item $item
     */
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Api\SearchCriteriaInterface $criteria,
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Flordel\Amazon\Helper\Data $helper,
        \Flordel\Walmart\Model\Item $item
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteria    = $criteria;
        $this->filterGroup       = $filterGroup;
        $this->filterBuilder     = $filterBuilder;
        $this->productStatus     = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->helper            = $helper;
        $this->walmartItem = $item;
    }

    /**
     * Execute controller
     * @return Magento\Framework\Controller\ResultFactor
     */
    public function execute()
    {

        $products = $this->getProductData();
        $productsUpdated = 0;
        $inventoryData = [];
        $walmartInventory = [];
        foreach ($products as $product) {
            $channels = $this->getSelectedChannels($product);
            $productDataObject = $this->productRepository->getById($product->getId());
            $sku = $product->getSku();
            $stockItem = $productDataObject->getExtensionAttributes()->getStockItem();
            $quantity = $stockItem->getQty();
            
            $thresholdQty = $productDataObject->getThreshold();
            if ($quantity <= $thresholdQty) {
                $quantity = $quantity;
            } else {
                $quantity = rand(300, 600);
            }
            
            if (in_array('Amazon', $channels)) {
                $inventoryData[$sku] = $quantity;
            } 
            if (in_array('Walmart', $channels)) {
                $walmartInventory[$sku] = $quantity;
            }
        }
        
        if (count($inventoryData) > 0) {
            $this->helper->updateAmazonWalmartQuantity($inventoryData);
            $productsUpdated += count($inventoryData);
        }
        
        if (count($walmartInventory) > 0) {
            $this->walmartItem->updateItemInventory($walmartInventory);
            $productsUpdated += count($walmartInventory);
        }

        if ($productsUpdated) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cron.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info(__('A total of %1 record(s) were updated.', $productsUpdated));
        }

        return $this;
    }

    /**
     * @return \Magento\Cms\Model\Block|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getProductData()
    {

        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('status')
                ->setConditionType('in')
                ->setValue($this->productStatus->getVisibleStatusIds())
                ->create(),
            $this->filterBuilder
                ->setField('visibility')
                ->setConditionType('in')
                ->setValue($this->productVisibility->getVisibleInSiteIds())
                ->create(),
        ]);

        $this->searchCriteria->setFilterGroups([$this->filterGroup]);
        $products     = $this->productRepository->getList($this->searchCriteria);
        $productItems = $products->getItems();

        return $productItems;
    }

    public function getSelectedChannels($product)
    {
        $channels = explode(',', $product->getChannel());
        $data     = [];
        $attr     = $product->getResource()->getAttribute('channel');
        foreach ($channels as $value) {
            if ($attr->usesSource()) {
                $option_value = $attr->getSource()->getOptionText($value);
                $data[]       = $option_value;
            }
        }

        return $data;
    }
}
