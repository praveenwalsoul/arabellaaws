<?php
namespace Flordel\Amazon\Model\ServiceOrders;

/**
 * \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest
 *
 * Properties:
 * <ul>
 *
 * <li>Marketplace: string</li>
 * <li>Merchant: string</li>
 * <li>MaxCount: int</li>
 * <li>ReportTypeList: \Flordel\Amazon\Model\ServiceOrders\TypeList</li>
 * <li>Acknowledged: bool</li>
 * <li>AvailableFromDate: string</li>
 * <li>AvailableToDate: string</li>
 * <li>ReportRequestIdList: \Flordel\Amazon\Model\ServiceOrders\IdList</li>
 *
 * </ul>
 */
class GetReportListRequest extends \Flordel\Amazon\Model\ServiceOrders
{

    /**
     * Construct new \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest
     *
     * @param mixed $data DOMElement or Associative Array to construct from.
     *
     * Valid properties:
     * <ul>
     *
     * <li>Marketplace: string</li>
     * <li>Merchant: string</li>
     * <li>MaxCount: int</li>
     * <li>ReportTypeList: \Flordel\Amazon\Model\ServiceOrders\TypeList</li>
     * <li>Acknowledged: bool</li>
     * <li>AvailableFromDate: string</li>
     * <li>AvailableToDate: string</li>
     * <li>ReportRequestIdList: \Flordel\Amazon\Model\ServiceOrders\IdList</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array(
            'Marketplace'         => array('FieldValue' => null, 'FieldType' => 'string'),
            'Merchant'            => array('FieldValue' => null, 'FieldType' => 'string'),
            'MaxCount'            => array('FieldValue' => null, 'FieldType' => 'string'),
            'ReportTypeList'      => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_TypeList'),
            'Acknowledged'        => array('FieldValue' => null, 'FieldType' => 'bool'),
            'AvailableFromDate'   => array('FieldValue' => null, 'FieldType' => 'DateTime'),
            'AvailableToDate'     => array('FieldValue' => null, 'FieldType' => 'DateTime'),
            'ReportRequestIdList' => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_IdList'),
        );

        parent::__construct($data);
    }

    /**
     * Gets the value of the Marketplace property.
     *
     * @return string Marketplace
     */
    public function getMarketplace()
    {
        return $this->_fields['Marketplace']['FieldValue'];
    }

    /**
     * Sets the value of the Marketplace property.
     *
     * @param string Marketplace
     * @return this instance
     */
    public function setMarketplace($value)
    {
        $this->_fields['Marketplace']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Marketplace and returns this instance
     *
     * @param string $value Marketplace
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest instance
     */
    public function withMarketplace($value)
    {
        $this->setMarketplace($value);
        return $this;
    }

    /**
     * Checks if Marketplace is set
     *
     * @return bool true if Marketplace  is set
     */
    public function isSetMarketplace()
    {
        return !is_null($this->_fields['Marketplace']['FieldValue']);
    }

    /**
     * Gets the value of the Merchant property.
     *
     * @return string Merchant
     */
    public function getMerchant()
    {
        return $this->_fields['Merchant']['FieldValue'];
    }

    /**
     * Sets the value of the Merchant property.
     *
     * @param string Merchant
     * @return this instance
     */
    public function setMerchant($value)
    {
        $this->_fields['Merchant']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Merchant and returns this instance
     *
     * @param string $value Merchant
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest instance
     */
    public function withMerchant($value)
    {
        $this->setMerchant($value);
        return $this;
    }

    /**
     * Checks if Merchant is set
     *
     * @return bool true if Merchant  is set
     */
    public function isSetMerchant()
    {
        return !is_null($this->_fields['Merchant']['FieldValue']);
    }

    /**
     * Gets the value of the MaxCount property.
     *
     * @return int MaxCount
     */
    public function getMaxCount()
    {
        return $this->_fields['MaxCount']['FieldValue'];
    }

    /**
     * Sets the value of the MaxCount property.
     *
     * @param int MaxCount
     * @return this instance
     */
    public function setMaxCount($value)
    {
        $this->_fields['MaxCount']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the MaxCount and returns this instance
     *
     * @param int $value MaxCount
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest instance
     */
    public function withMaxCount($value)
    {
        $this->setMaxCount($value);
        return $this;
    }

    /**
     * Checks if MaxCount is set
     *
     * @return bool true if MaxCount  is set
     */
    public function isSetMaxCount()
    {
        return !is_null($this->_fields['MaxCount']['FieldValue']);
    }

    /**
     * Gets the value of the ReportTypeList.
     *
     * @return TypeList ReportTypeList
     */
    public function getReportTypeList()
    {
        return $this->_fields['ReportTypeList']['FieldValue'];
    }

    /**
     * Sets the value of the ReportTypeList.
     *
     * @param TypeList ReportTypeList
     * @return void
     */
    public function setReportTypeList($value)
    {
        $this->_fields['ReportTypeList']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ReportTypeList  and returns this instance
     *
     * @param TypeList $value ReportTypeList
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest instance
     */
    public function withReportTypeList($value)
    {
        $this->setReportTypeList($value);
        return $this;
    }

    /**
     * Checks if ReportTypeList  is set
     *
     * @return bool true if ReportTypeList property is set
     */
    public function isSetReportTypeList()
    {
        return !is_null($this->_fields['ReportTypeList']['FieldValue']);
    }

    /**
     * Gets the value of the Acknowledged property.
     *
     * @return bool Acknowledged
     */
    public function getAcknowledged()
    {
        return $this->_fields['Acknowledged']['FieldValue'];
    }

    /**
     * Sets the value of the Acknowledged property.
     *
     * @param bool Acknowledged
     * @return this instance
     */
    public function setAcknowledged($value)
    {
        $this->_fields['Acknowledged']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Acknowledged and returns this instance
     *
     * @param bool $value Acknowledged
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest instance
     */
    public function withAcknowledged($value)
    {
        $this->setAcknowledged($value);
        return $this;
    }

    /**
     * Checks if Acknowledged is set
     *
     * @return bool true if Acknowledged  is set
     */
    public function isSetAcknowledged()
    {
        return !is_null($this->_fields['Acknowledged']['FieldValue']);
    }

    /**
     * Gets the value of the AvailableFromDate property.
     *
     * @return string AvailableFromDate
     */
    public function getAvailableFromDate()
    {
        return $this->_fields['AvailableFromDate']['FieldValue'];
    }

    /**
     * Sets the value of the AvailableFromDate property.
     *
     * @param string AvailableFromDate
     * @return this instance
     */
    public function setAvailableFromDate($value)
    {
        $this->_fields['AvailableFromDate']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the AvailableFromDate and returns this instance
     *
     * @param string $value AvailableFromDate
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest instance
     */
    public function withAvailableFromDate($value)
    {
        $this->setAvailableFromDate($value);
        return $this;
    }

    /**
     * Checks if AvailableFromDate is set
     *
     * @return bool true if AvailableFromDate  is set
     */
    public function isSetAvailableFromDate()
    {
        return !is_null($this->_fields['AvailableFromDate']['FieldValue']);
    }

    /**
     * Gets the value of the AvailableToDate property.
     *
     * @return string AvailableToDate
     */
    public function getAvailableToDate()
    {
        return $this->_fields['AvailableToDate']['FieldValue'];
    }

    /**
     * Sets the value of the AvailableToDate property.
     *
     * @param string AvailableToDate
     * @return this instance
     */
    public function setAvailableToDate($value)
    {
        $this->_fields['AvailableToDate']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the AvailableToDate and returns this instance
     *
     * @param string $value AvailableToDate
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest instance
     */
    public function withAvailableToDate($value)
    {
        $this->setAvailableToDate($value);
        return $this;
    }

    /**
     * Checks if AvailableToDate is set
     *
     * @return bool true if AvailableToDate  is set
     */
    public function isSetAvailableToDate()
    {
        return !is_null($this->_fields['AvailableToDate']['FieldValue']);
    }

    /**
     * Gets the value of the ReportRequestIdList.
     *
     * @return IdList ReportRequestIdList
     */
    public function getReportRequestIdList()
    {
        return $this->_fields['ReportRequestIdList']['FieldValue'];
    }

    /**
     * Sets the value of the ReportRequestIdList.
     *
     * @param IdList ReportRequestIdList
     * @return void
     */
    public function setReportRequestIdList($value)
    {
        $this->_fields['ReportRequestIdList']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ReportRequestIdList  and returns this instance
     *
     * @param IdList $value ReportRequestIdList
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest instance
     */
    public function withReportRequestIdList($value)
    {
        $this->setReportRequestIdList($value);
        return $this;
    }

    /**
     * Checks if ReportRequestIdList  is set
     *
     * @return bool true if ReportRequestIdList property is set
     */
    public function isSetReportRequestIdList()
    {
        return !is_null($this->_fields['ReportRequestIdList']['FieldValue']);
    }
}
