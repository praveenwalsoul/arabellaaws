<?php
namespace Flordel\Amazon\Model\ServiceOrders;

/**
 * \Flordel\Amazon\Model\ServiceOrders\RequestReportResult
 *
 * Properties:
 * <ul>
 *
 * <li>ReportRequestInfo: \Flordel\Amazon\Model\ServiceOrders\ReportRequestInfo</li>
 *
 * </ul>
 */
class RequestReportResult extends \Flordel\Amazon\Model\ServiceOrders
{

    /**
     * Construct new \Flordel\Amazon\Model\ServiceOrders\RequestReportResult
     *
     * @param mixed $data DOMElement or Associative Array to construct from.
     *
     * Valid properties:
     * <ul>
     *
     * <li>ReportRequestInfo: \Flordel\Amazon\Model\ServiceOrders\ReportRequestInfo</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array(
            'ReportRequestInfo' => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_ReportRequestInfo'),
        );
        parent::__construct($data);
    }

    /**
     * Gets the value of the ReportRequestInfo.
     *
     * @return ReportRequestInfo ReportRequestInfo
     */
    public function getReportRequestInfo()
    {
        return $this->_fields['ReportRequestInfo']['FieldValue'];
    }

    /**
     * Sets the value of the ReportRequestInfo.
     *
     * @param ReportRequestInfo ReportRequestInfo
     * @return void
     */
    public function setReportRequestInfo($value)
    {
        $this->_fields['ReportRequestInfo']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ReportRequestInfo  and returns this instance
     *
     * @param ReportRequestInfo $value ReportRequestInfo
     * @return \Flordel\Amazon\Model\ServiceOrders\RequestReportResult instance
     */
    public function withReportRequestInfo($value)
    {
        $this->setReportRequestInfo($value);
        return $this;
    }

    /**
     * Checks if ReportRequestInfo  is set
     *
     * @return bool true if ReportRequestInfo property is set
     */
    public function isSetReportRequestInfo()
    {
        return !is_null($this->_fields['ReportRequestInfo']['FieldValue']);
    }
}
