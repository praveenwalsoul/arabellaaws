<?php
namespace Flordel\Amazon\Model\ServiceOrders;

/**
 * \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse
 *
 * Properties:
 * <ul>
 *
 * <li>GetReportScheduleListResult: \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResult</li>
 * <li>ResponseMetadata: \Flordel\Amazon\Model\ServiceOrders\ResponseMetadata</li>
 *
 * </ul>
 */
class GetReportScheduleListResponse extends \Flordel\Amazon\Model\ServiceOrders
{

    /**
     * Construct new \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse
     *
     * @param mixed $data DOMElement or Associative Array to construct from.
     *
     * Valid properties:
     * <ul>
     *
     * <li>GetReportScheduleListResult: \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResult</li>
     * <li>ResponseMetadata: \Flordel\Amazon\Model\ServiceOrders\ResponseMetadata</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array(
            'GetReportScheduleListResult' => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_GetReportScheduleListResult'),
            'ResponseMetadata'            => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_ResponseMetadata'),
        );
        parent::__construct($data);
    }

    /**
     * Construct \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse from XML string
     *
     * @param string $xml XML string to construct from
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse
     */
    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('a', 'http://mws.amazonaws.com/doc/2009-01-01/');
        $response = $xpath->query('//a:GetReportScheduleListResponse');
        if ($response->length == 1) {
            return new \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse(($response->item(0)));
        } else {
            throw new Exception("Unable to construct \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse from provided XML.
                                  Make sure that GetReportScheduleListResponse is a root element");
        }
    }

    /**
     * Gets the value of the GetReportScheduleListResult.
     *
     * @return GetReportScheduleListResult GetReportScheduleListResult
     */
    public function getGetReportScheduleListResult()
    {
        return $this->_fields['GetReportScheduleListResult']['FieldValue'];
    }

    /**
     * Sets the value of the GetReportScheduleListResult.
     *
     * @param GetReportScheduleListResult GetReportScheduleListResult
     * @return void
     */
    public function setGetReportScheduleListResult($value)
    {
        $this->_fields['GetReportScheduleListResult']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the GetReportScheduleListResult  and returns this instance
     *
     * @param GetReportScheduleListResult $value GetReportScheduleListResult
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse instance
     */
    public function withGetReportScheduleListResult($value)
    {
        $this->setGetReportScheduleListResult($value);
        return $this;
    }

    /**
     * Checks if GetReportScheduleListResult  is set
     *
     * @return bool true if GetReportScheduleListResult property is set
     */
    public function isSetGetReportScheduleListResult()
    {
        return !is_null($this->_fields['GetReportScheduleListResult']['FieldValue']);
    }

    /**
     * Gets the value of the ResponseMetadata.
     *
     * @return ResponseMetadata ResponseMetadata
     */
    public function getResponseMetadata()
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }

    /**
     * Sets the value of the ResponseMetadata.
     *
     * @param ResponseMetadata ResponseMetadata
     * @return void
     */
    public function setResponseMetadata($value)
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ResponseMetadata  and returns this instance
     *
     * @param ResponseMetadata $value ResponseMetadata
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse instance
     */
    public function withResponseMetadata($value)
    {
        $this->setResponseMetadata($value);
        return $this;
    }

    /**
     * Checks if ResponseMetadata  is set
     *
     * @return bool true if ResponseMetadata property is set
     */
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);
    }

    /**
     * XML Representation for this object
     *
     * @return string XML for this object
     */
    public function toXML()
    {
        $xml = "";
        $xml .= "<GetReportScheduleListResponse xmlns=\"http://mws.amazonaws.com/doc/2009-01-01/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</GetReportScheduleListResponse>";
        return $xml;
    }

    private $_responseHeaderMetadata = null;

    public function getResponseHeaderMetadata()
    {
        return $this->_responseHeaderMetadata;
    }

    public function setResponseHeaderMetadata($responseHeaderMetadata)
    {
        return $this->_responseHeaderMetadata = $responseHeaderMetadata;
    }
}
