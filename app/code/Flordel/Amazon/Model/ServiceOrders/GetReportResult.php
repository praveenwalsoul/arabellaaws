<?php
namespace Flordel\Amazon\Model\ServiceOrders;

/**
 * \Flordel\Amazon\Model\ServiceOrders\GetReportResult
 *
 * Properties:
 * <ul>
 *
 *
 * </ul>
 */
class GetReportResult extends \Flordel\Amazon\Model\ServiceOrders
{

    /**
     * Construct new \Flordel\Amazon\Model\ServiceOrders\GetReportResult
     *
     * @param mixed $data DOMElement or Associative Array to construct from.
     *
     * Valid properties:
     * <ul>
     *
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array(
            'ContentMd5' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }

    public function getContentMd5()
    {
        return $this->_fields['ContentMd5']['FieldValue'];
    }

    public function setContentMd5($value)
    {
        $this->_fields['ContentMd5']['FieldValue'] = $value;
    }

    public function isSetContentMd5()
    {
        return !is_null($this->_fields['ContentMd5']['FieldValue']);
    }

    public function withContentMd5($value)
    {
        $this->setContentMd5($value);
        return $this;
    }
}
