<?php
namespace Flordel\Amazon\Model\ServiceOrders;

/**
 * \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse
 *
 * Properties:
 * <ul>
 *
 * <li>ManageReportScheduleResult: \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResult</li>
 * <li>ResponseMetadata: \Flordel\Amazon\Model\ServiceOrders\ResponseMetadata</li>
 *
 * </ul>
 */
class ManageReportScheduleResponse extends \Flordel\Amazon\Model\ServiceOrders
{

    /**
     * Construct new \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse
     *
     * @param mixed $data DOMElement or Associative Array to construct from.
     *
     * Valid properties:
     * <ul>
     *
     * <li>ManageReportScheduleResult: \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResult</li>
     * <li>ResponseMetadata: \Flordel\Amazon\Model\ServiceOrders\ResponseMetadata</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array(
            'ManageReportScheduleResult' => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_ManageReportScheduleResult'),
            'ResponseMetadata'           => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_ResponseMetadata'),
        );
        parent::__construct($data);
    }

    /**
     * Construct \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse from XML string
     *
     * @param string $xml XML string to construct from
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse
     */
    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('a', 'http://mws.amazonaws.com/doc/2009-01-01/');
        $response = $xpath->query('//a:ManageReportScheduleResponse');
        if ($response->length == 1) {
            return new \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse(($response->item(0)));
        } else {
            throw new Exception("Unable to construct \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse from provided XML.
                                  Make sure that ManageReportScheduleResponse is a root element");
        }
    }

    /**
     * Gets the value of the ManageReportScheduleResult.
     *
     * @return ManageReportScheduleResult ManageReportScheduleResult
     */
    public function getManageReportScheduleResult()
    {
        return $this->_fields['ManageReportScheduleResult']['FieldValue'];
    }

    /**
     * Sets the value of the ManageReportScheduleResult.
     *
     * @param ManageReportScheduleResult ManageReportScheduleResult
     * @return void
     */
    public function setManageReportScheduleResult($value)
    {
        $this->_fields['ManageReportScheduleResult']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ManageReportScheduleResult  and returns this instance
     *
     * @param ManageReportScheduleResult $value ManageReportScheduleResult
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse instance
     */
    public function withManageReportScheduleResult($value)
    {
        $this->setManageReportScheduleResult($value);
        return $this;
    }

    /**
     * Checks if ManageReportScheduleResult  is set
     *
     * @return bool true if ManageReportScheduleResult property is set
     */
    public function isSetManageReportScheduleResult()
    {
        return !is_null($this->_fields['ManageReportScheduleResult']['FieldValue']);
    }

    /**
     * Gets the value of the ResponseMetadata.
     *
     * @return ResponseMetadata ResponseMetadata
     */
    public function getResponseMetadata()
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }

    /**
     * Sets the value of the ResponseMetadata.
     *
     * @param ResponseMetadata ResponseMetadata
     * @return void
     */
    public function setResponseMetadata($value)
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ResponseMetadata  and returns this instance
     *
     * @param ResponseMetadata $value ResponseMetadata
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse instance
     */
    public function withResponseMetadata($value)
    {
        $this->setResponseMetadata($value);
        return $this;
    }

    /**
     * Checks if ResponseMetadata  is set
     *
     * @return bool true if ResponseMetadata property is set
     */
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);
    }

    /**
     * XML Representation for this object
     *
     * @return string XML for this object
     */
    public function toXML()
    {
        $xml = "";
        $xml .= "<ManageReportScheduleResponse xmlns=\"http://mws.amazonaws.com/doc/2009-01-01/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</ManageReportScheduleResponse>";
        return $xml;
    }

    private $_responseHeaderMetadata = null;

    public function getResponseHeaderMetadata()
    {
        return $this->_responseHeaderMetadata;
    }

    public function setResponseHeaderMetadata($responseHeaderMetadata)
    {
        return $this->_responseHeaderMetadata = $responseHeaderMetadata;
    }
}
