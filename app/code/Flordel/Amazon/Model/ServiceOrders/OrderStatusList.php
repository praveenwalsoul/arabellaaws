<?php

namespace Flordel\Amazon\Model\ServiceOrders;

/**
 * OrderStatusList
 *
 * Properties:
 * <ul>
 *
 * <li>Status: OrderStatusEnum</li>
 *
 * </ul>
 */
class OrderStatusList extends \Flordel\Amazon\Model\ServiceOrders
{

    /**
     * Construct new MarketplaceWebServiceOrders_Model_OrderStatusList
     *
     * @param mixed $data DOMElement or Associative Array to construct from.
     *
     * Valid properties:
     * <ul>
     *
     * <li>Status: OrderStatusEnum</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array(
            'Status' => array('FieldValue' => array(), 'FieldType' => array('OrderStatusEnum')),
        );
        parent::__construct($data);
    }

    /**
     * Gets the value of the Status .
     *
     * @return array of OrderStatusEnum Status
     */
    public function getStatus()
    {
        return $this->_fields['Status']['FieldValue'];
    }

    /**
     * Sets the value of the Status.
     *
     * @param OrderStatusEnum or an array of OrderStatusEnum Status
     * @return this instance
     */
    public function setStatus($status)
    {
        if (!$this->_isNumericArray($status)) {
            $status = array($status);
        }
        $this->_fields['Status']['FieldValue'] = $status;
        return $this;
    }

    /**
     * Sets single or multiple values of Status list via variable number of arguments.
     * For example, to set the list with two elements, simply pass two values as arguments to this function
     * <code>withStatus($status1, $status2)</code>
     *
     * @param OrderStatusEnum  $orderStatusEnumArgs one or more Status
     * @return MarketplaceWebServiceOrders_Model_OrderStatusList  instance
     */
    public function withStatus($orderStatusEnumArgs)
    {
        foreach (func_get_args() as $status) {
            $this->_fields['Status']['FieldValue'][] = $status;
        }
        return $this;
    }

    /**
     * Checks if Status list is non-empty
     *
     * @return bool true if Status list is non-empty
     */
    public function isSetStatus()
    {
        return count($this->_fields['Status']['FieldValue']) > 0;
    }
}
