<?php
namespace Flordel\Amazon\Model\ServiceOrders;

/**
 * \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest
 *
 * Properties:
 * <ul>
 *
 * <li>Marketplace: string</li>
 * <li>Merchant: string</li>
 * <li>ReportType: string</li>
 * <li>Schedule: string</li>
 * <li>ScheduledDate: string</li>
 *
 * </ul>
 */
class ManageReportScheduleRequest extends \Flordel\Amazon\Model\ServiceOrders
{

    /**
     * Construct new \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest
     *
     * @param mixed $data DOMElement or Associative Array to construct from.
     *
     * Valid properties:
     * <ul>
     *
     * <li>Marketplace: string</li>
     * <li>Merchant: string</li>
     * <li>ReportType: string</li>
     * <li>Schedule: string</li>
     * <li>ScheduledDate: string</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array(
            'Marketplace'   => array('FieldValue' => null, 'FieldType' => 'string'),
            'Merchant'      => array('FieldValue' => null, 'FieldType' => 'string'),
            'ReportType'    => array('FieldValue' => null, 'FieldType' => 'string'),
            'Schedule'      => array('FieldValue' => null, 'FieldType' => 'string'),
            'ScheduledDate' => array('FieldValue' => null, 'FieldType' => 'DateTime'),
        );
        parent::__construct($data);
    }

    /**
     * Gets the value of the Marketplace property.
     *
     * @return string Marketplace
     */
    public function getMarketplace()
    {
        return $this->_fields['Marketplace']['FieldValue'];
    }

    /**
     * Sets the value of the Marketplace property.
     *
     * @param string Marketplace
     * @return this instance
     */
    public function setMarketplace($value)
    {
        $this->_fields['Marketplace']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Marketplace and returns this instance
     *
     * @param string $value Marketplace
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest instance
     */
    public function withMarketplace($value)
    {
        $this->setMarketplace($value);
        return $this;
    }

    /**
     * Checks if Marketplace is set
     *
     * @return bool true if Marketplace  is set
     */
    public function isSetMarketplace()
    {
        return !is_null($this->_fields['Marketplace']['FieldValue']);
    }

    /**
     * Gets the value of the Merchant property.
     *
     * @return string Merchant
     */
    public function getMerchant()
    {
        return $this->_fields['Merchant']['FieldValue'];
    }

    /**
     * Sets the value of the Merchant property.
     *
     * @param string Merchant
     * @return this instance
     */
    public function setMerchant($value)
    {
        $this->_fields['Merchant']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Merchant and returns this instance
     *
     * @param string $value Merchant
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest instance
     */
    public function withMerchant($value)
    {
        $this->setMerchant($value);
        return $this;
    }

    /**
     * Checks if Merchant is set
     *
     * @return bool true if Merchant  is set
     */
    public function isSetMerchant()
    {
        return !is_null($this->_fields['Merchant']['FieldValue']);
    }

    /**
     * Gets the value of the ReportType property.
     *
     * @return string ReportType
     */
    public function getReportType()
    {
        return $this->_fields['ReportType']['FieldValue'];
    }

    /**
     * Sets the value of the ReportType property.
     *
     * @param string ReportType
     * @return this instance
     */
    public function setReportType($value)
    {
        $this->_fields['ReportType']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the ReportType and returns this instance
     *
     * @param string $value ReportType
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest instance
     */
    public function withReportType($value)
    {
        $this->setReportType($value);
        return $this;
    }

    /**
     * Checks if ReportType is set
     *
     * @return bool true if ReportType  is set
     */
    public function isSetReportType()
    {
        return !is_null($this->_fields['ReportType']['FieldValue']);
    }

    /**
     * Gets the value of the Schedule property.
     *
     * @return string Schedule
     */
    public function getSchedule()
    {
        return $this->_fields['Schedule']['FieldValue'];
    }

    /**
     * Sets the value of the Schedule property.
     *
     * @param string Schedule
     * @return this instance
     */
    public function setSchedule($value)
    {
        $this->_fields['Schedule']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Schedule and returns this instance
     *
     * @param string $value Schedule
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest instance
     */
    public function withSchedule($value)
    {
        $this->setSchedule($value);
        return $this;
    }

    /**
     * Checks if Schedule is set
     *
     * @return bool true if Schedule  is set
     */
    public function isSetSchedule()
    {
        return !is_null($this->_fields['Schedule']['FieldValue']);
    }

    /**
     * Gets the value of the ScheduledDate property.
     *
     * @return string ScheduledDate
     */
    public function getScheduledDate()
    {
        return $this->_fields['ScheduledDate']['FieldValue'];
    }

    /**
     * Sets the value of the ScheduledDate property.
     *
     * @param string ScheduledDate
     * @return this instance
     */
    public function setScheduledDate($value)
    {
        $this->_fields['ScheduledDate']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the ScheduledDate and returns this instance
     *
     * @param string $value ScheduledDate
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest instance
     */
    public function withScheduledDate($value)
    {
        $this->setScheduledDate($value);
        return $this;
    }

    /**
     * Checks if ScheduledDate is set
     *
     * @return bool true if ScheduledDate  is set
     */
    public function isSetScheduledDate()
    {
        return !is_null($this->_fields['ScheduledDate']['FieldValue']);
    }
}
