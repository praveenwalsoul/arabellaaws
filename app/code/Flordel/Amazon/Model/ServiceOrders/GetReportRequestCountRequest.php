<?php
namespace Flordel\Amazon\Model\ServiceOrders;

/**
 * \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest
 *
 * Properties:
 * <ul>
 *
 * <li>Marketplace: string</li>
 * <li>Merchant: string</li>
 * <li>ReportTypeList: \Flordel\Amazon\Model\ServiceOrders\TypeList</li>
 * <li>ReportProcessingStatusList: \Flordel\Amazon\Model\ServiceOrders\StatusList</li>
 * <li>RequestedFromDate: string</li>
 * <li>RequestedToDate: string</li>
 *
 * </ul>
 */
class GetReportRequestCountRequest extends \Flordel\Amazon\Model\ServiceOrders
{

    /**
     * Construct new \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest
     *
     * @param mixed $data DOMElement or Associative Array to construct from.
     *
     * Valid properties:
     * <ul>
     *
     * <li>Marketplace: string</li>
     * <li>Merchant: string</li>
     * <li>ReportTypeList: \Flordel\Amazon\Model\ServiceOrders\TypeList</li>
     * <li>ReportProcessingStatusList: \Flordel\Amazon\Model\ServiceOrders\StatusList</li>
     * <li>RequestedFromDate: string</li>
     * <li>RequestedToDate: string</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array(
            'Marketplace'                => array('FieldValue' => null, 'FieldType' => 'string'),
            'Merchant'                   => array('FieldValue' => null, 'FieldType' => 'string'),
            'ReportTypeList'             => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_TypeList'),
            'ReportProcessingStatusList' => array('FieldValue' => null, 'FieldType' => 'Flordel_Amazon_Model_ServiceOrders_StatusList'),
            'RequestedFromDate'          => array('FieldValue' => null, 'FieldType' => 'DateTime'),
            'RequestedToDate'            => array('FieldValue' => null, 'FieldType' => 'DateTime'),
        );
        parent::__construct($data);
    }

    /**
     * Gets the value of the Marketplace property.
     *
     * @return string Marketplace
     */
    public function getMarketplace()
    {
        return $this->_fields['Marketplace']['FieldValue'];
    }

    /**
     * Sets the value of the Marketplace property.
     *
     * @param string Marketplace
     * @return this instance
     */
    public function setMarketplace($value)
    {
        $this->_fields['Marketplace']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Marketplace and returns this instance
     *
     * @param string $value Marketplace
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest instance
     */
    public function withMarketplace($value)
    {
        $this->setMarketplace($value);
        return $this;
    }

    /**
     * Checks if Marketplace is set
     *
     * @return bool true if Marketplace  is set
     */
    public function isSetMarketplace()
    {
        return !is_null($this->_fields['Marketplace']['FieldValue']);
    }

    /**
     * Gets the value of the Merchant property.
     *
     * @return string Merchant
     */
    public function getMerchant()
    {
        return $this->_fields['Merchant']['FieldValue'];
    }

    /**
     * Sets the value of the Merchant property.
     *
     * @param string Merchant
     * @return this instance
     */
    public function setMerchant($value)
    {
        $this->_fields['Merchant']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the Merchant and returns this instance
     *
     * @param string $value Merchant
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest instance
     */
    public function withMerchant($value)
    {
        $this->setMerchant($value);
        return $this;
    }

    /**
     * Checks if Merchant is set
     *
     * @return bool true if Merchant  is set
     */
    public function isSetMerchant()
    {
        return !is_null($this->_fields['Merchant']['FieldValue']);
    }

    /**
     * Gets the value of the ReportTypeList.
     *
     * @return TypeList ReportTypeList
     */
    public function getReportTypeList()
    {
        return $this->_fields['ReportTypeList']['FieldValue'];
    }

    /**
     * Sets the value of the ReportTypeList.
     *
     * @param TypeList ReportTypeList
     * @return void
     */
    public function setReportTypeList($value)
    {
        $this->_fields['ReportTypeList']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ReportTypeList  and returns this instance
     *
     * @param TypeList $value ReportTypeList
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest instance
     */
    public function withReportTypeList($value)
    {
        $this->setReportTypeList($value);
        return $this;
    }

    /**
     * Checks if ReportTypeList  is set
     *
     * @return bool true if ReportTypeList property is set
     */
    public function isSetReportTypeList()
    {
        return !is_null($this->_fields['ReportTypeList']['FieldValue']);
    }

    /**
     * Gets the value of the ReportProcessingStatusList.
     *
     * @return StatusList ReportProcessingStatusList
     */
    public function getReportProcessingStatusList()
    {
        return $this->_fields['ReportProcessingStatusList']['FieldValue'];
    }

    /**
     * Sets the value of the ReportProcessingStatusList.
     *
     * @param StatusList ReportProcessingStatusList
     * @return void
     */
    public function setReportProcessingStatusList($value)
    {
        $this->_fields['ReportProcessingStatusList']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ReportProcessingStatusList  and returns this instance
     *
     * @param StatusList $value ReportProcessingStatusList
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest instance
     */
    public function withReportProcessingStatusList($value)
    {
        $this->setReportProcessingStatusList($value);
        return $this;
    }

    /**
     * Checks if ReportProcessingStatusList  is set
     *
     * @return bool true if ReportProcessingStatusList property is set
     */
    public function isSetReportProcessingStatusList()
    {
        return !is_null($this->_fields['ReportProcessingStatusList']['FieldValue']);
    }

    /**
     * Gets the value of the RequestedFromDate property.
     *
     * @return string RequestedFromDate
     */
    public function getRequestedFromDate()
    {
        return $this->_fields['RequestedFromDate']['FieldValue'];
    }

    /**
     * Sets the value of the RequestedFromDate property.
     *
     * @param string RequestedFromDate
     * @return this instance
     */
    public function setRequestedFromDate($value)
    {
        $this->_fields['RequestedFromDate']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the RequestedFromDate and returns this instance
     *
     * @param string $value RequestedFromDate
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest instance
     */
    public function withRequestedFromDate($value)
    {
        $this->setRequestedFromDate($value);
        return $this;
    }

    /**
     * Checks if RequestedFromDate is set
     *
     * @return bool true if RequestedFromDate  is set
     */
    public function isSetRequestedFromDate()
    {
        return !is_null($this->_fields['RequestedFromDate']['FieldValue']);
    }

    /**
     * Gets the value of the RequestedToDate property.
     *
     * @return string RequestedToDate
     */
    public function getRequestedToDate()
    {
        return $this->_fields['RequestedToDate']['FieldValue'];
    }

    /**
     * Sets the value of the RequestedToDate property.
     *
     * @param string RequestedToDate
     * @return this instance
     */
    public function setRequestedToDate($value)
    {
        $this->_fields['RequestedToDate']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the RequestedToDate and returns this instance
     *
     * @param string $value RequestedToDate
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest instance
     */
    public function withRequestedToDate($value)
    {
        $this->setRequestedToDate($value);
        return $this;
    }

    /**
     * Checks if RequestedToDate is set
     *
     * @return bool true if RequestedToDate  is set
     */
    public function isSetRequestedToDate()
    {
        return !is_null($this->_fields['RequestedToDate']['FieldValue']);
    }
}
