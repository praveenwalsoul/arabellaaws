<?php

namespace Flordel\Amazon\Model;

use Exception;
use Flordel\Amazon\Model\Client;
use Flordel\Amazon\Model\ServiceOrders\GetOrderRequest;
use Flordel\Amazon\Model\ServiceOrders\ListOrderItemsRequest;
use Flordel\Amazon\Model\ServiceOrders\ListOrdersByNextTokenRequest;
use Flordel\Amazon\Model\ServiceOrders\ListOrdersByNextTokenResponse;
use Flordel\Amazon\Model\ServiceOrders\ListOrdersRequest;

class ListOrders
{
    protected $serviceUrl = "https://mws.amazonservices.com/Orders/2013-09-01";
    protected $awsAccessKeyId;
    protected $awsSecretAccessKey;
    protected $applicationName;
    protected $applicationVersion;
    protected $merchantId;
    protected $marketplaceId;
    protected $helper;
    protected $onlyItemPrice = false;
    protected $onlyItemPriceByDate = false;

    public function __construct(\Flordel\Amazon\Helper\Data $helper)
    {
        $this->helper = $helper;
        $this->initConfig();
    }

    public function initConfig()
    {
        $this->awsAccessKeyId     = $this->helper->getConfigData('webapi/amazon_sop/amz_clientid');
        $this->awsSecretAccessKey = $this->helper->getConfigData('webapi/amazon_sop/amz_secretkey');
        $this->applicationName    = 'Amazon MWS';
        $this->applicationVersion = '1';
        $this->merchantId         = $this->helper->getConfigData('webapi/amazon_sop/amz_sellerid');
        $this->marketplaceId      = $this->helper->getConfigData('webapi/amazon_sop/amz_marketplaceid');
    }

    public function getListOrders($startDate, $endDate, $orderStatuses = null)
    {
        try {
            $config = array(
                "ServiceURL"    => $this->serviceUrl,
                "ProxyHost"     => null,
                "ProxyPort"     => -1,
                "MaxErrorRetry" => 3,
            );

            $service = new Client(
                $this->awsAccessKeyId,
                $this->awsSecretAccessKey,
                $this->applicationName,
                $this->applicationVersion,
                $config
            );

            $request = new ListOrdersRequest();
            $request->setSellerId($this->merchantId);

            $request->setCreatedAfter($startDate);
            $request->setCreatedBefore($endDate);
            //$request->setMaxResultsPerPage(5); // Page limit
            $request->setMarketplaceId($this->marketplaceId);
            if ($orderStatuses != 'All') {
                $orderStatuses = isset($orderStatuses) ? $orderStatuses : array("Unshipped", "PartiallyShipped"); // for new order
                $request->setOrderStatus($orderStatuses);
            }
            return $this->invokeListOrders($service, $request);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function invokeListOrders(\Flordel\Amazon\Model\ServiceOrdersInterface $service, $request)
    {
        $hasNextOrders = false;
        $nextToken     = null;
        $processedData = [];
        do {
            if ($hasNextOrders) {
                $request = new ListOrdersByNextTokenRequest();
                $request->setSellerId($this->merchantId);
                $request->setNextToken($nextToken);
                $response = $service->ListOrdersByNextToken($request);
            } else {
                $response = $service->listOrders($request);
            }

            if ($this->isSetListOrdersResult($response)) {
                $listOrdersResult = $this->getListOrdersResult($response);

                if ($listOrdersResult->isSetOrders()) {
                    if ($listOrdersResult->isSetNextToken()) {
                        $hasNextOrders = true;
                        $nextToken     = $listOrdersResult->getNextToken();
                    } else {
                        $hasNextOrders = false;
                        $nextToken     = null;
                    }

                    $orders = $listOrdersResult->getOrders();
                    foreach ($orders as $order) {
                        $partnerOrderId = $order->getAmazonOrderId();
                        $request        = new ListOrderItemsRequest();
                        $request->setSellerId($this->merchantId);
                        $request->setAmazonOrderId($order->getAmazonOrderId());
                        $responseArray = $this->invokeListOrderItems($service, $request, $order);
                        if ($order->getOrderStatus() == 'Canceled') {
                            $processedData[$partnerOrderId] = $responseArray;
                        } elseif ($this->onlyItemPrice && $order->getOrderStatus() != 'Canceled') {
                            $processedData[$partnerOrderId] = $responseArray;
                        } elseif ($this->onlyItemPriceByDate && $order->getOrderStatus() != 'Canceled') {
                            $processedData[] = $responseArray;
                        } elseif ((!$this->onlyItemPrice || !$this->onlyItemPriceByDate) && $order->getOrderStatus() != 'Canceled') {
                            $processedData[$partnerOrderId] = $this->processedOrdersData($order, $responseArray);
                        }
                    }
                }
            }
        } while ($hasNextOrders);
        
        if($this->onlyItemPriceByDate && $order->getOrderStatus() != 'Canceled') {
            $processedData = $this->helper->getOrderItemPrice($processedData);
        }
        return $processedData;
    }

    public function invokeListOrderItems(\Flordel\Amazon\Model\ServiceOrdersInterface $service, $request, $order)
    {
        global $basket;
        $data     = $responseArray     = [];
        $response = $service->listOrderItems($request);
        if ($response->isSetListOrderItemsResult()) {
            $listOrderItemsResult = $response->getListOrderItemsResult();
            if ($listOrderItemsResult->isSetOrderItems()) {
                $orderItems    = $listOrderItemsResult->getOrderItems();
                $responseArray = [];
                foreach ($orderItems as $orderItem) {
                    if ($order->getOrderStatus() == 'Canceled') {
                        $responseArray[] = $orderItem->getOrderItemId();
                    } elseif ($this->onlyItemPrice && $order->getOrderStatus() != 'Canceled') {
                        $itemPrice     = $orderItem->getItemPrice();
                        $quantity      = $orderItem->getQuantityOrdered();
                        $product_price = $itemPrice->getAmount() / $quantity;

                        $responseArray[] = [
                            'order-item-id' => $orderItem->getOrderItemId(),
                            'price'         => $product_price,
                        ];
                    } elseif ($this->onlyItemPriceByDate && $order->getOrderStatus() != 'Canceled') {
                        $itemPrice     = $orderItem->getItemPrice();
                        $quantity      = $orderItem->getQuantityOrdered();
                        $product_price = $itemPrice->getAmount() / $quantity;
                        $responseArray[$orderItem->getOrderItemId()] = $product_price;
                    } elseif ((!$this->onlyItemPrice || !$this->onlyItemPriceByDate) && $order->getOrderStatus() != 'Canceled') {
                        $product_name = $orderItem->getTitle();
                        /*if ($orderItem->isSetSellerSKU()) {
                        $product_stockcode = $orderItem->getSellerSKU();
                        }*/
                        $itemPrice     = $orderItem->getItemPrice();
                        $quantity      = $orderItem->getQuantityOrdered();
                        $product_price = $itemPrice->getAmount() / $quantity;
                        $ship_date     = date("Y-m-d", strtotime($order->getLatestShipDate()));
                        $deli_date     = date("Y-m-d", strtotime($order->getLatestDeliveryDate()));

                        $data = [
                            'order-id'      => $order->getAmazonOrderId(),
                            'tax'           => $orderItem->getItemTax()->getCurrencyCode() . $orderItem->getItemTax()->getAmount(),
                            'ship_price'    => $orderItem->getShippingPrice(),
                            'order-item-id' => $orderItem->getOrderItemId(),
                            'shipping_date' => $ship_date,
                            'delivery_date' => $deli_date,
                            'sku'           => $orderItem->getSellerSKU(),
                            'qty'           => $quantity,
                            'price'         => $product_price,

                        ];

                        $responseArray['item'][] = $data;
                    }
                }
            }
        }

        /*$this->writeLog($responseArray);*/
        return $responseArray;
    }

    public function getOrder($orderId)
    {
        try {
            $config = array(
                "ServiceURL"    => $this->serviceUrl,
                "ProxyHost"     => null,
                "ProxyPort"     => -1,
                "MaxErrorRetry" => 3,
            );

            $service = new Client(
                $this->awsAccessKeyId,
                $this->awsSecretAccessKey,
                $this->applicationName,
                $this->applicationVersion,
                $config
            );

            $request = new GetOrderRequest();
            $request->setSellerId($this->merchantId);
            $request->setAmazonOrderId($orderId);
            $result = $service->GetOrder($request);
            $order  = $result->getGetOrderResult()->getOrders();
            $order  = $order[0];
            //Get Order Item
            $request = new ListOrderItemsRequest();
            $request->setSellerId($this->merchantId);
            $request->setAmazonOrderId($orderId);
            $responseArray = $this->invokeListOrderItems($service, $request, $order);
            return $responseArray;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function writeLog($data)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/nandish_test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($data, true));
    }

    public function getCancelledOrders($startDate, $endDate)
    {
        try {
            $config = array(
                "ServiceURL"    => $this->serviceUrl,
                "ProxyHost"     => null,
                "ProxyPort"     => -1,
                "MaxErrorRetry" => 3,
            );

            $service = new Client(
                $this->awsAccessKeyId,
                $this->awsSecretAccessKey,
                $this->applicationName,
                $this->applicationVersion,
                $config
            );

            $request = new ListOrdersRequest();
            $request->setSellerId($this->merchantId);

            $request->setCreatedAfter($startDate);
            $request->setCreatedBefore($endDate);
            $request->setMaxResultsPerPage(30); // Page limit
            $request->setMarketplaceId($this->marketplaceId);

            $orderStatuses = ['Canceled']; // for new order
            $request->setOrderStatus($orderStatuses);
            return $this->invokeListOrders($service, $request);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function processedOrdersData(\Flordel\Amazon\Model\ServiceOrders\Order $order, array $responseArray)
    {
        $partnerOrderId                             = $order->getAmazonOrderId();
        $orderTotal                                 = $order->getOrderTotal();
        $total                                      = $orderTotal->getAmount();
        $responseArray['order']['partner_order_id'] = $partnerOrderId;
        $ord_date                                   = date("Y-m-d", strtotime($order->getPurchaseDate()));
        $responseArray['order']['order_date']       = $ord_date;
        $shippingAddress                            = $order->getShippingAddress();
        $shippingName                               = $shippingAddress->getName();
        $addr1                                      = $shippingAddress->getAddressLine1();
        $addr2                                      = $shippingAddress->getAddressLine2();
        $addr3                                      = $shippingAddress->getAddressLine3();
        $city                                       = $shippingAddress->getCity();
        $region                                     = $shippingAddress->getStateOrRegion();
        $postcode                                   = $shippingAddress->getPostalCode();
        $country                                    = $shippingAddress->getCountryCode();
        $tel                                        = $shippingAddress->getPhone();
        //$this->writeLog($order->getBuyerName());
        $bNameArray = explode(" ", $order->getBuyerName());
        $bFirstName = $bMiddleName = $bLastName = '';
        if (count($bNameArray) >= 3) {
            $bFirstName  = $bNameArray[0];
            $bMiddleName = $bNameArray[1];
            $bLastName   = $bNameArray[2];
        } elseif (count($bNameArray) == 2) {
            $bFirstName = $bNameArray[0];
            $bLastName  = $bNameArray[1];
        } else {
            $bFirstName = $bNameArray[0];
        }
        $billingAddress = [
            'customer_address_id' => '',
            'prefix'              => '',
            'firstname'           => $bFirstName,
            'middlename'          => $bMiddleName,
            'lastname'            => $bLastName,
            'street'              => [
                '0' => $addr1,
                '1' => $addr2,
            ],
            'city'                => $city,
            'country_id'          => 'US',
            'region'              => $region,
            'postcode'            => $postcode,
            'telephone'           => $tel,
        ];
        /**
         * Shipping data
         */
        $nameArray  = explode(" ", $shippingName);
        $sFirstName = $sMiddleName = $sLastName = '';
        if (count($nameArray) >= 3) {
            $sFirstName  = $nameArray[0];
            $sMiddleName = $nameArray[1];
            $sLastName   = $nameArray[2];
        } elseif (count($nameArray) == 2) {
            $sFirstName = $nameArray[0];
            $sLastName  = $nameArray[1];
        } else {
            $sFirstName = $nameArray[0];
        }
        $shippingAddress = [
            'customer_address_id' => '',
            'prefix'              => '',
            'firstname'           => $sFirstName,
            'middlename'          => $sMiddleName,
            'lastname'            => $sLastName,
            'street'              => [
                '0' => $addr1,
                '1' => $addr2,
            ],
            'city'                => $city,
            'country_id'          => 'US',
            'region'              => $region,
            'postcode'            => $postcode,
            'telephone'           => $tel,
        ];
        $responseArray['billing_address']  = $billingAddress;
        $responseArray['shipping_address'] = $shippingAddress;

        return $responseArray;
    }

    private function isSetListOrdersResult(\Flordel\Amazon\Model\ServiceOrders $response)
    {
        if ($response instanceof ListOrdersByNextTokenResponse) {
            return $response->isSetListOrdersByNextTokenResult();
        } else {
            return $response->isSetListOrdersResult();
        }
    }

    private function getListOrdersResult(\Flordel\Amazon\Model\ServiceOrders $response)
    {
        if ($response instanceof ListOrdersByNextTokenResponse) {
            return $response->getListOrdersByNextTokenResult();
        } else {
            return $response->getListOrdersResult();
        }
    }

    public function getOrdersItemsPrice($firstOrderId, $lastOrderId)
    {
        try {
            $this->onlyItemPrice = true;
            $orderStatuses       = 'All';
            $startDate = $this->getOrderPurchaseDate($firstOrderId);
            $endDate = $this->getOrderPurchaseDate($lastOrderId);
            //$startDate = '2020-04-08T15:56:40.927Z';
            //$endDate = '2020-04-08T16:23:09.762Z';
            $responseArray = $this->getListOrders($startDate, $endDate, $orderStatuses);
            return $responseArray;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function getOrdersItemsPriceByDate($startDate = null, $endDate = null)
    {
        try {
            $this->onlyItemPriceByDate = true;
            $orderStatuses       = 'All';
            $startDate = ($startDate)?$this->helper->getFormattedTimestamp($startDate):$this->helper->getBackDate();
            $endDate = ($endDate)?$this->helper->getFormattedTimestamp($endDate):$this->helper->getCurrentDate();
            $responseArray = $this->getListOrders($startDate, $endDate, $orderStatuses);
            return $responseArray;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function getOrderPurchaseDate($orderId)
    {
        try {
            $config = array(
                "ServiceURL"    => $this->serviceUrl,
                "ProxyHost"     => null,
                "ProxyPort"     => -1,
                "MaxErrorRetry" => 3,
            );

            $service = new Client(
                $this->awsAccessKeyId,
                $this->awsSecretAccessKey,
                $this->applicationName,
                $this->applicationVersion,
                $config
            );

            $request = new GetOrderRequest();
            $request->setSellerId($this->merchantId);
            $request->setAmazonOrderId($orderId);
            $result = $service->GetOrder($request);
            $order  = $result->getGetOrderResult()->getOrders();
            $order  = $order[0];
            return $order->getPurchaseDate();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
