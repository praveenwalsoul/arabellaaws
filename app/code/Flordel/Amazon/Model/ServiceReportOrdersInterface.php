<?php
namespace Flordel\Amazon\Model;

interface ServiceReportOrdersInterface
{

    /**
     * Get Report
     * The GetReport operation returns the contents of a report. Reports can potentially be
     * very large (>100MB) which is why we only return one report at a time, and in a
     * streaming fashion.
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReport.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportResponse \Flordel\Amazon\Model\ServiceOrders\GetReportResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReport($request);

    /**
     * Get Report Schedule Count
     * returns the number of report schedules
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportScheduleCount.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleCountRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleCountRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleCountRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleCountResponse \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleCountResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportScheduleCount($request);

    /**
     * Get Report Request List By Next Token
     * retrieve the next batch of list items and if there are more items to retrieve
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportRequestListByNextToken.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListByNextTokenRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListByNextTokenRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListByNextTokenRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListByNextTokenResponse \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListByNextTokenResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportRequestListByNextToken($request);

    /**
     * Update Report Acknowledgements
     * The UpdateReportAcknowledgements operation updates the acknowledged status of one or more reports.
     *
     * @see http://docs.amazonwebservices.com/${docPath}UpdateReportAcknowledgements.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\UpdateReportAcknowledgementsRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\UpdateReportAcknowledgementsRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\UpdateReportAcknowledgementsRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\UpdateReportAcknowledgementsResponse \Flordel\Amazon\Model\ServiceOrders\UpdateReportAcknowledgementsResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function updateReportAcknowledgements($request);

    /**
     * Submit Feed
     * Uploads a file for processing together with the necessary
     * metadata to process the file, such as which type of feed it is.
     * PurgeAndReplace if true means that your existing e.g. inventory is
     * wiped out and replace with the contents of this feed - use with
     * caution (the default is false).
     *
     * @see http://docs.amazonwebservices.com/${docPath}SubmitFeed.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\SubmitFeedRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\SubmitFeedRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\SubmitFeedRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\SubmitFeedResponse \Flordel\Amazon\Model\ServiceOrders\SubmitFeedResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function submitFeed($request);

    /**
     * Get Report Count
     * returns a count of reports matching your criteria;
     * by default, the number of reports generated in the last 90 days,
     * regardless of acknowledgement status
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportCount.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportCountRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportCountRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportCountRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportCountResponse \Flordel\Amazon\Model\ServiceOrders\GetReportCountResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportCount($request);

    /**
     * Get Feed Submission List By Next Token
     * retrieve the next batch of list items and if there are more items to retrieve
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetFeedSubmissionListByNextToken.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListByNextTokenRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListByNextTokenRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListByNextTokenRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListByNextTokenResponse \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListByNextTokenResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getFeedSubmissionListByNextToken($request);

    /**
     * Cancel Feed Submissions
     * cancels feed submissions - by default all of the submissions of the
     * last 30 days that have not started processing
     *
     * @see http://docs.amazonwebservices.com/${docPath}CancelFeedSubmissions.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\CancelFeedSubmissionsRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\CancelFeedSubmissionsRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\CancelFeedSubmissionsRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\CancelFeedSubmissionsResponse \Flordel\Amazon\Model\ServiceOrders\CancelFeedSubmissionsResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function cancelFeedSubmissions($request);

    /**
     * Request Report
     * requests the generation of a report
     *
     * @see http://docs.amazonwebservices.com/${docPath}RequestReport.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\RequestReportRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\RequestReportRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\RequestReportRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\RequestReportResponse \Flordel\Amazon\Model\ServiceOrders\RequestReportResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function requestReport($request);

    /**
     * Get Feed Submission Count
     * returns the number of feeds matching all of the specified criteria
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetFeedSubmissionCount.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionCountRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionCountRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionCountRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionCountResponse \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionCountResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getFeedSubmissionCount($request);

    /**
     * Cancel Report Requests
     * cancels report requests that have not yet started processing,
     * by default all those within the last 90 days
     *
     * @see http://docs.amazonwebservices.com/${docPath}CancelReportRequests.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\CancelReportRequestsRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\CancelReportRequestsRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\CancelReportRequestsRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\CancelReportRequestsResponse \Flordel\Amazon\Model\ServiceOrders\CancelReportRequestsResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function cancelReportRequests($request);

    /**
     * Get Report List
     * returns a list of reports; by default the most recent ten reports,
     * regardless of their acknowledgement status
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportList.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListResponse \Flordel\Amazon\Model\ServiceOrders\GetReportListResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportList($request);

    /**
     * Get Feed Submission Result
     * retrieves the feed processing report
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetFeedSubmissionResult.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionResultRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionResultRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionResultRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionResultResponse \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionResultResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getFeedSubmissionResult($request);

    /**
     * Get Feed Submission List
     * returns a list of feed submission identifiers and their associated metadata
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetFeedSubmissionList.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListResponse \Flordel\Amazon\Model\ServiceOrders\GetFeedSubmissionListResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getFeedSubmissionList($request);

    /**
     * Get Report Request List
     * returns a list of report requests ids and their associated metadata
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportRequestList.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListResponse \Flordel\Amazon\Model\ServiceOrders\GetReportRequestListResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportRequestList($request);

    /**
     * Get Report Schedule List By Next Token
     * retrieve the next batch of list items and if there are more items to retrieve
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportScheduleListByNextToken.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListByNextTokenRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListByNextTokenRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListByNextTokenRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListByNextTokenResponse \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListByNextTokenResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportScheduleListByNextToken($request);

    /**
     * Get Report List By Next Token
     * retrieve the next batch of list items and if there are more items to retrieve
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportListByNextToken.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportListByNextTokenRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportListByNextTokenRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportListByNextTokenRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportListByNextTokenResponse \Flordel\Amazon\Model\ServiceOrders\GetReportListByNextTokenResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportListByNextToken($request);

    /**
     * Manage Report Schedule
     * Creates, updates, or deletes a report schedule
     * for a given report type, such as order reports in particular.
     *
     * @see http://docs.amazonwebservices.com/${docPath}ManageReportSchedule.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse \Flordel\Amazon\Model\ServiceOrders\ManageReportScheduleResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function manageReportSchedule($request);

    /**
     * Get Report Request Count
     * returns a count of report requests; by default all the report
     * requests in the last 90 days
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportRequestCount.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountResponse \Flordel\Amazon\Model\ServiceOrders\GetReportRequestCountResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportRequestCount($request);

    /**
     * Get Report Schedule List
     * returns the list of report schedules
     *
     * @see http://docs.amazonwebservices.com/${docPath}GetReportScheduleList.html
     * @param mixed $request array of parameters for \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListRequest request
     * or \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListRequest object itself
     * @see \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListRequest
     * @return \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse \Flordel\Amazon\Model\ServiceOrders\GetReportScheduleListResponse
     *
     * @throws MarketplaceWebService_Exception
     */
    public function getReportScheduleList($request);
}
