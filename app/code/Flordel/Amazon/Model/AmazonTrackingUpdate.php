<?php
namespace Flordel\Amazon\Model;

use Flordel\Amazon\Model\ServiceOrders\AmazonFeed;
use \DomDocument;

class AmazonTrackingUpdate
{
    public function sendOrderTrackFeed($feed)
    {
        $config = [
        'logpath' => $this->getMediaPath() . 'tracking/logs.txt',
        'serviceUrl' => 'https://mws.amazonservices.com/',
        'merchantId' => 'A337Y0QJFO0F0Z',
        'AWSAccessKeyId' => 'AKIAII2TTACCE6NTMNNA',
        'secretKey' => '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',
        ];
        $amz = new AmazonFeed($config); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_ORDER_FULFILLMENT_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed);
        $amz->submitFeed();
        return $amz->getResponse();
    }
    
    public function getMediaPath()
    {
        $om            = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem    = $om->get('Magento\Framework\Filesystem');
        $directoryList = $om->get('Magento\Framework\App\Filesystem\DirectoryList');
        $mediapath     = $filesystem->getDirectoryRead($directoryList::MEDIA)->getAbsolutePath();
        return $mediapath;
    }

    public function buildOrderTrackFeed($order, $trackNumber, $itemId)
    {
        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi      = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi      = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion    = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier    = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode('A1XT1BNDOGPVZ');
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType    = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('OrderFulfillment');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        $id = 0;
        $id++;
        $PickupDate = date("Y-m-d\TH:i:s");
        $message    = $xml->createElement('Message', '');

        $messageId    = $xml->createElement('MessageID');
        $txtMessageId = $xml->createTextNode($id);
        $messageId->appendChild($txtMessageId);
        $message->appendChild($messageId);

        $OrderFulfillment = $xml->createElement('OrderFulfillment', '');

        $MerchantOrderID = $xml->createElement('AmazonOrderID');
        $txtMessageId    = $xml->createTextNode($order->getPartnerOrderId());
        $MerchantOrderID->appendChild($txtMessageId);
        $OrderFulfillment->appendChild($MerchantOrderID);

        $MerchantFulfillmentID = $xml->createElement('MerchantFulfillmentID');
        $txtMessageId          = $xml->createTextNode($order->getIncrementId());
        $MerchantFulfillmentID->appendChild($txtMessageId);
        $OrderFulfillment->appendChild($MerchantFulfillmentID);

        $FulfillmentDate = $xml->createElement('FulfillmentDate');
        $txtMessageId    = $xml->createTextNode($PickupDate);
        $FulfillmentDate->appendChild($txtMessageId);
        $OrderFulfillment->appendChild($FulfillmentDate);

        $ordered_items = $order->getAllItems();
        $j             = 0;

        foreach ($ordered_items as $orderitem) {
            if($orderitem->getId() == $itemId) {
                // add fullfilment data

                $fullfilment = $xml->createElement('FulfillmentData', '');
    
                // add item code
                $fulfillmentLatency = $xml->createElement('CarrierCode', '');
                $fulfillmentLatency->appendChild($xml->createTextNode("UPS"));
                $fullfilment->appendChild($fulfillmentLatency);
    
                // add cancel reason
                $fulfillmentLatency = $xml->createElement('ShippingMethod', '');
                $fulfillmentLatency->appendChild($xml->createTextNode('Standard'));
                $fullfilment->appendChild($fulfillmentLatency);
    
                // add cancel reason
                $fulfillmentLatency = $xml->createElement('ShipperTrackingNumber', '');
                $fulfillmentLatency->appendChild($xml->createTextNode("$trackNumber"));
                $fullfilment->appendChild($fulfillmentLatency);
    
                $OrderFulfillment->appendChild($fullfilment);
    
                $item = $xml->createElement('Item', '');
    
                // add item code
                $fulfillmentLatency = $xml->createElement('AmazonOrderItemCode', '');
                $fulfillmentLatency->appendChild($xml->createTextNode($orderitem->getPartnerPo()));
                $item->appendChild($fulfillmentLatency);
    
                // add item quantity
                $fulfillmentLatency = $xml->createElement('Quantity', '');
                $fulfillmentLatency->appendChild($xml->createTextNode(1));
                $item->appendChild($fulfillmentLatency);
    
                $OrderFulfillment->appendChild($item);
            }
        }
        
        $message->appendChild($OrderFulfillment);
        $amazonEnvelope->appendChild($message);

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    public function updateTrackingNumber()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $box = $objectManager->create('Flordelcampo\Order\Model\Box');
    	$boxItems = $box->getCollection()
    	->addFieldToSelect('box_id')
    	->addFieldToSelect('track_id')
    	->addFieldToSelect('order_id')
    	->addFieldToSelect('item_id')
    	->addFieldToFilter('status', ['eq' => 'shipped']);
    	foreach($boxItems as $boxItem) {
    	    $orderCollecton = $objectManager->create('Magento\Sales\Model\ResourceModel\Order\Collection');
        	$orderCollecton->addFieldToFilter('entity_id', ['eq' => $boxItem->getOrderId()]);
        	$order = $orderCollecton->getFirstItem();
        	$trackingId = $boxItem->getTrackId();
        	$itemId = $boxItem->getItemId();
        	//Generate Feed
        	$orderFeed = $this->buildOrderTrackFeed($order, $trackingId, $itemId);
        	//Send tracking details
        	$result = $this->sendOrderTrackFeed($orderFeed);
        	
        	if($result['FeedProcessingStatus'] == '_SUBMITTED_') {
        	    $id = $boxItem->getBoxId();
        	    $boxModel = $box->load($id, 'box_id');
        	    $boxModel->setData('status', \Flordelcampo\Order\Model\Box::INVOICED);
        	    $boxModel->save();
        	}
    	}
    }
}
