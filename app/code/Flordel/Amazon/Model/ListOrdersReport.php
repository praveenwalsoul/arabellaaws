<?php
namespace Flordel\Amazon\Model;

use Flordel\Amazon\Model\ReportClient;
use Flordel\Amazon\Model\ServiceOrders\GetReportRequest;
use Flordel\Amazon\Model\ServiceOrders\TypeList;

class ListOrdersReport
{

    protected $serviceUrl = "https://mws.amazonservices.com";
    protected $awsAccessKeyId;
    protected $awsSecretAccessKey;
    protected $applicationName;
    protected $applicationVersion;
    protected $merchantId;
    protected $marketplaceId;
    protected $helper;

    public function __construct(\Flordel\Amazon\Helper\Data $helper)
    {
        $this->helper = $helper;
        $this->initConfig();
    }

    public function initConfig()
    {
        $this->awsAccessKeyId     = $this->helper->getConfigData('webapi/amazon_sop/amz_clientid');
        $this->awsSecretAccessKey = $this->helper->getConfigData('webapi/amazon_sop/amz_secretkey');
        $this->applicationName    = 'Amazon MWS';
        $this->applicationVersion = '1';
        $this->merchantId         = $this->helper->getConfigData('webapi/amazon_sop/amz_sellerid');
        $this->marketplaceId      = $this->helper->getConfigData('webapi/amazon_sop/amz_marketplaceid');
    }

    /* Return Order Report csv file path  */
    public function getOrderReports()
    {
        // @TODO: set request. Action can be passed as \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest
        // object or array of parameters

        $typeList = new TypeList();
        $typeList->setType(array('_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_'));

        $parameters = array(
            'Merchant'       => $this->merchantId,
            'ReportTypeList' => $typeList,
            'Acknowledged'   => false,
        );

        $config = array(
            'ServiceURL'    => $this->serviceUrl,
            'ProxyHost'     => null,
            'ProxyPort'     => -1,
            'MaxErrorRetry' => 3,
        );

        $service = new ReportClient(
            $this->awsAccessKeyId,
            $this->awsSecretAccessKey,
            $config,
            $this->applicationName,
            $this->applicationVersion
        );

        //$request = new \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest($parameters);

        $request = new \Flordel\Amazon\Model\ServiceOrders\GetReportListRequest();
        $request->setMerchant($this->merchantId);

        $request->setReportTypeList($typeList);
        $request->setMaxCount(1);
        $request->setAcknowledged(false);

        return $this->invokeGetReportList($service, $request);
    }

/**
 * Get Report List Action Sample
 * returns a list of reports; by default the most recent ten reports,
 * regardless of their acknowledgement status
 *
 * @param \Flordel\Amazon\Model\ServiceReportOrdersInterface $service instance of \Flordel\Amazon\Model\ServiceReportOrdersInterface
 * @param mixed $request \Flordel\Amazon\Model\ServiceOrders\GetReportList or array of parameters
 */
    public function invokeGetReportList(\Flordel\Amazon\Model\ServiceReportOrdersInterface $service, $request)
    {
        try {
            $reportFile = '';
            $response   = $service->getReportList($request);
            if ($response->isSetGetReportListResult()) {
                $getReportListResult = $response->getGetReportListResult();
                $reportInfoList      = $getReportListResult->getReportInfoList();
                foreach ($reportInfoList as $reportInfo) {
                    $reportId         = $reportInfo->getReportId();
                    $getReportRequest = new GetReportRequest();
                    $getReportRequest->setMerchant($this->merchantId);
                    $getReportRequest->setReportId($reportId);
                    $fileStream = $this->getReportFileName($reportId);
                    $getReportRequest->setReport($fileStream); //file object
                    $this->invokeGetReport($service, $getReportRequest);
                    $reportFile = $this->getMediaPath() . 'Amazon/' . $reportId . '.txt';
                }
            }
            return $reportFile;
        } catch (\Flordel\Amazon\Model\ServiceOrdersexception $ex) {
            echo ("Caught Exception: " . $ex->getMessage() . "\n");
            echo ("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo ("Error Code: " . $ex->getErrorCode() . "\n");
            echo ("Error Type: " . $ex->getErrorType() . "\n");
            echo ("Request ID: " . $ex->getRequestId() . "\n");
            echo ("XML: " . $ex->getXML() . "\n");
            echo ("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }

    public function invokeGetReport(\Flordel\Amazon\Model\ServiceReportOrdersInterface $service, $request)
    {
        $reportResponse = $service->getReport($request);
    }

    /* Return file stream  */
    public function getReportFileName($reportId)
    {
        $mediapath = $this->getMediaPath();
        $filename = $reportId . '.txt';
        $file = @fopen($mediapath . 'Amazon/' . $filename, 'w');
        return $file;
    }

    public function getMediaPath()
    {
        $om            = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem    = $om->get('Magento\Framework\Filesystem');
        $directoryList = $om->get('Magento\Framework\App\Filesystem\DirectoryList');
        $mediapath     = $filesystem->getDirectoryRead($directoryList::MEDIA)->getAbsolutePath();
        return $mediapath;
    }
}
