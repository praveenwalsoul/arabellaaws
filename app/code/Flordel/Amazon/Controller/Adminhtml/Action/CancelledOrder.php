<?php

namespace Flordel\Amazon\Controller\Adminhtml\Action;

use Flordel\Amazon\Helper\Data;
use Flordel\Amazon\Model\ListOrders;
use Flordelcampo\Order\Controller\Adminhtml\Amazon\Create;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * Cancelled Order
 *
 * Class CancelledOrder
 */
class CancelledOrder extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var ListOrders
     */
    protected $listOrders;
    /**
     * @var Create
     */
    protected $create;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ListOrders $listOrders
     * @param Create $create
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ListOrders $listOrders,
        Create $create,
        Data $helper
    )
    {
        parent::__construct($context);
        $this->helper = $helper;
        $this->listOrders = $listOrders;
        $this->resultPageFactory = $resultPageFactory;
        $this->create = $create;
    }

    /**
     * Index action
     *
     * @return Page
     */
    public function execute()
    {
        try {
            $startDate = $this->helper->getBackDate();
            $endDate = $this->helper->getCurrentDate();
            /**
             * update status for cancel orders
             */
            $data = $this->listOrders->getCancelledOrders($startDate, $endDate);
            $updateResponse = $this->create->updateStatusForAmazonCancelOrders($data);
            $this->writeLog($data, 'cancel');
        } catch (\Exception $e) {
            $data = [
                'status' => 'fail',
                'message' => $e->getMessage()
            ];
        }
        die('Amazon cancelled orders status updated successfully... check log for any issues');
        return $resultPage;
    }

    /**
     * Write Log
     *
     * @param $data
     * @param $type
     */
    public function writeLog($data, $type)
    {
        switch ($type) {
            case 'create':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_order.log');
                break;
            case 'cancel':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_cancel_order.log');
                break;
            default:
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_order.log');
        }
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($data, true));
    }
}
