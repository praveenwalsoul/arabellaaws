<?php

namespace Flordel\Amazon\Controller\Adminhtml\Action;

class Synchronizeinventory extends \Magento\Catalog\Controller\Adminhtml\Product
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Ui\Component\MassAction\Filter $filter
     */
    protected $filter;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Flordel\Amazon\Helper\Data
     */
    protected $helper;

    protected $walmartItem;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Flordel\Walmart\Model\Item $item
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Flordel\Amazon\Helper\Data $helper,
        \Flordel\Walmart\Model\Item $item
    ) {
        parent::__construct($context, $productBuilder);
        $this->productRepository = $productRepositoryInterface;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->helper = $helper;
        $this->walmartItem = $item;
    }

    /**
     * Execute controller
     * @return Magento\Framework\Controller\ResultFactor
     */
    public function execute()
    {

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $visibilityStatus = $this->getRequest()->getParam('synchronizeinventory');

        $productsUpdated = 0;
        $inventoryData = [];
        $walmartInventory = [];
        foreach ($collection->getAllIds() as $productId) {
            $productDataObject = $this->productRepository->getById($productId);
            $channels = $this->getSelectedChannels($productDataObject);
            if ($productDataObject->getSku() != '') {
                $sku = $productDataObject->getSku();
                $stockItem = $productDataObject->getExtensionAttributes()->getStockItem();
                $quantity = $stockItem->getQty();
                $thresholdQty = $productDataObject->getThreshold();
                if ($quantity <= $thresholdQty) {
                    $quantity = $quantity;
                } else {
                    $quantity = rand(300, 600);
                }
                
                if (in_array('Amazon', $channels)) {
                    $inventoryData[$sku] = $quantity;
                } else if (in_array('Walmart', $channels)) {
                    $walmartInventory[$sku] = $quantity;
                }
                
            }

        }
        
        if (count($inventoryData) > 0) {
            $this->helper->updateAmazonWalmartQuantity($inventoryData);
            $productsUpdated += count($inventoryData);
        }
        
        if (count($walmartInventory) > 0) {
            $this->walmartItem->updateItemInventory($walmartInventory);
            $productsUpdated += count($walmartInventory);
        }
        

        if ($productsUpdated) {
            $this->messageManager->addSuccess(__('A total of %1 record(s) were updated.', $productsUpdated));
        }

        $resultRedirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('catalog/product/index');
        return $resultRedirect;
    }
    
    public function getSelectedChannels($product) {
        $channels = explode(',',$product->getChannel());
        $data = [];
        $attr = $product->getResource()->getAttribute('channel');
        foreach ($channels as $value) {
            if ($attr->usesSource()) {
                $option_value = $attr->getSource()->getOptionText($value);
                $data[] = $option_value;
            }
        }

        return $data;        
    }
}