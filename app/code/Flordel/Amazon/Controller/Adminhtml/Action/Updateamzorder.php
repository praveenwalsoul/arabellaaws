<?php

namespace Flordel\Amazon\Controller\Adminhtml\Action;

class Updateamzorder extends \Magento\Backend\App\Action
{

    /**
     * @var \Flordel\Amazon\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Flordel\Amazon\Helper\Data $helper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Flordel\Amazon\Helper\Data $helper
    ) {
        parent::__construct($context);
    }

    /**
     * Execute controller
     * @return Magento\Framework\Controller\ResultFactor
     */
    public function execute()
    {

        $resultRedirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('catalog/product/index');
        return $resultRedirect;
    }

}
