<?php

namespace Flordel\Amazon\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $listOrders;
    protected $listOrdersReport;
    protected $helper;
    /**
     * @var \Flordelcampo\Order\Controller\Adminhtml\Amazon\Create
     */
    protected $create;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Flordel\Amazon\Model\ListOrders $listOrders
     * @param \Flordel\Amazon\Model\ListOrdersReport $listOrdersReport
     * @param \Flordel\Amazon\Helper\Data $helper
     * @param \Flordelcampo\Order\Controller\Adminhtml\Amazon\Create $create
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Flordel\Amazon\Model\ListOrders $listOrders,
        \Flordel\Amazon\Model\ListOrdersReport $listOrdersReport,
        \Flordel\Amazon\Helper\Data $helper,
        \Flordelcampo\Order\Controller\Adminhtml\Amazon\Create $create
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->listOrdersReport = $listOrdersReport;
        $this->listOrders = $listOrders;
        $this->helper = $helper;
        $this->create = $create;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            $startDate = $this->helper->getBackDate();
            $endDate = $this->helper->getCurrentDate();

            /**
             * To get all order data in csv sheet and get sheet file path
             */
            $apiCsvFilePath = $this->listOrdersReport->getOrderReports();
            $createResponse = $this->create->createOrderForAmazonApiOrders($apiCsvFilePath);
            $this->writeLog($createResponse, 'create');

            /**
             * update status for cancel orders
             */
            /* $data = $this->listOrders->getCancelledOrders($startDate, $endDate);
             $updateResponse = $this->create->updateStatusForAmazonCancelOrders($data);
             $this->writeLog($updateResponse, 'cancel');*/

        } catch (\Exception $e) {
            $data = [
                'status' => 'fail',
                'message' => $e->getMessage()
            ];
        }
        die('Amazon orders creation and status updated successfully... check log for any issues');
        return $this->_pageFactory->create();
    }

    /**
     * Write Log
     *
     * @param $data
     * @param $type
     */
    public function writeLog($data, $type)
    {
        switch ($type) {
            case 'create':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_order.log');
                break;
            case 'cancel':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_cancel_order.log');
                break;
            default:
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_order.log');
        }
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($data, true));
    }
}
