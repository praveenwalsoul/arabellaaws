<?php

namespace Flordel\Amazon\Controller\Index;

class Test extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $listOrders;
    protected $listOrdersReport;
    protected $helper;
    /**
     * @var \Flordel\Walmart\Model\Item
     */
    protected $inventory;
    
    protected $amazonTrackingUpdate;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Flordel\Amazon\Model\ListOrders $listOrders
     * @param \Flordel\Amazon\Model\ListOrdersReport $listOrdersReport
     * @param \Flordel\Amazon\Helper\Data $helper
     * @param \Flordelcampo\Order\Controller\Adminhtml\Amazon\Create $create
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Flordel\Amazon\Model\ListOrders $listOrders,
        \Flordel\Amazon\Model\ListOrdersReport $listOrdersReport,
        \Flordel\Amazon\Helper\Data $helper,
        \Flordel\Walmart\Model\Inventory $inventory,
        \Flordel\Amazon\Model\AmazonTrackingUpdate $amazonTrackingUpdate
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->listOrdersReport = $listOrdersReport;
        $this->listOrders = $listOrders;
        $this->helper = $helper;
        $this->inventory = $inventory;
        $this->amazonTrackingUpdate = $amazonTrackingUpdate;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            //$this->amazonTrackingUpdate->updateTrackingNumber();
            //die;
            /*$sku = 'ABR001';
            $items = [$sku => 150];
            echo $this->inventory->updateInventory($items);*/
            $data = $this->listOrders->getOrdersItemsPriceByDate();
            echo "<pre>";
            print_r($data);die;
        } catch (\Exception $e) {
            $data = [
                'status' => 'fail',
                'message' => $e->getMessage()
            ];
            
            print_r($data);
        }
        die('Amazon orders creation and status updated successfully... check log for any issues');
        return $this->_pageFactory->create();
    }

    /**
     * Write Log
     *
     * @param $data
     * @param $type
     */
    public function writeLog($data, $type)
    {
        switch ($type) {
            case 'create':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_order.log');
                break;
            case 'cancel':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_cancel_order.log');
                break;
            default:
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_order.log');
        }
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($data, true));
    }
}
