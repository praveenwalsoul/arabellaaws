<?php

namespace Flordel\Amazon\Helper;

use DateTime;
use DateTimeZone;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    protected $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Formats date as ISO 8601 timestamp
     */
    public function getFormattedTimestamp($date = null)
    {
        $UTC   = new DateTimeZone("UTC");
        $newTZ = new DateTimeZone("America/Chicago");
        $time  = time();
        if (isset($date)) {
            $time = strtotime($date);
        }
        $dateTime = new DateTime(gmdate("Y-m-dTH:i:s.Z", $time), $UTC);
        $dateTime->setTimezone($newTZ);
        return $dateTime->format(DATE_ISO8601);
    }

    /**
     *
     * @param $inventoryData
     */

    public function updateAmazonWalmartQuantity($inventoryData)
    {
        $amazonSellerId       = $this->getConfigData('webapi/amazon_sop/amz_sellerid');
        $amazonAWSAccessKeyId = $this->getConfigData('webapi/amazon_sop/amz_clientid');
        $amazonSecretKey      = $this->getConfigData('webapi/amazon_sop/amz_secretkey');
        $amazonMarketPlaceId  = $this->getConfigData('webapi/amazon_sop/amz_marketplaceid');
        $hostUrlData          = parse_url($this->getConfigData('webapi/amazon_sop/amz_endur'));

        $param                           = array();
        $param['AWSAccessKeyId']         = $amazonAWSAccessKeyId;
        $param['Action']                 = "SubmitFeed";
        $param['Merchant']               = $amazonSellerId;
        $param['FeedType']               = "_POST_INVENTORY_AVAILABILITY_DATA_";
        $param['SignatureMethod']        = "HmacSHA256";
        $param['SignatureVersion']       = "2";
        $param['Timestamp']              = $this->getFormattedTimestamp();
        $param['Version']                = "2009-01-01";
        $param['MarketplaceIdList.Id.1'] = $amazonMarketPlaceId;
        $param['PurgeAndReplace']        = "false";

        $url = array();
        foreach ($param as $key => $val) {
            $key   = rawurlencode($key);
            $val   = rawurlencode($val);
            $url[] = "{$key}={$val}";
        }

        $message = '';
        foreach ($inventoryData as $sku => $quantity) {
            $messageId = abs(crc32(uniqid('flordel_', true)));
            $message .= "<Message>
                        <MessageID>$messageId</MessageID>
                        <OperationType>Update</OperationType>
                        <Inventory>
                            <SKU>$sku</SKU>
                            <Quantity>$quantity</Quantity>
                        </Inventory>
                    </Message>";
        }

        $amazon_feed = '<?xml version="1.0" encoding="utf-8"?>
        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
            <Header>
                <DocumentVersion>1.01</DocumentVersion>
                <MerchantIdentifier>' . $amazonSellerId . '</MerchantIdentifier>
            </Header>
            <MessageType>Inventory</MessageType>
            ' . $message . '
        </AmazonEnvelope>';

        sort($url);

        $arr = implode("&", $url);

        $sign = "POST\n";
        $sign .= $hostUrlData['host'] . "\n";
        $sign .= "/Feeds/{$param['Version']}\n";
        $sign .= $arr;

        $signature    = hash_hmac("sha256", $sign, $amazonSecretKey, true);
        $httpHeader   = array();
        $httpHeader[] = "Transfer-Encoding: chunked";
        $httpHeader[] = "Content-Type: application/xml";
        $httpHeader[] = "Content-MD5: " . base64_encode(md5($amazon_feed, true));
        $httpHeader[] = "Expect:";
        $httpHeader[] = "Accept:";

        $signature = urlencode(base64_encode($signature));

        $link = "https://" . $hostUrlData['host'] . "/Feeds/{$param['Version']}?";
        $link .= $arr . "&Signature=" . $signature;

        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $amazon_feed);
        $response = curl_exec($ch);
        $info     = curl_getinfo($ch);
        $errors   = curl_error($ch);
        curl_close($ch);

        $xml    = simplexml_load_string($response);
        $result = $xml->children()->SubmitFeedResult->
            children()->FeedSubmissionInfo->
            children()->FeedProcessingStatus;

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_walmart_inventory_update.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($response);
    }

    public function getConfigData($configName)
    {
        return $this->scopeConfig->getValue(
            $configName,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getBackDate()
    {
        $date = date("Y-m-d 00:00", strtotime("-5 day"));
        return $this->getFormattedTimestamp($date);
    }

    public function getCurrentDate()
    {
        $date = date("Y-m-d 00:00");
        return $this->getFormattedTimestamp($date);
    }

    /*
     * @return array
     */
    public function getConvertCsvTOArray(String $reportFileNamewithFullPath)
    {
        $reportFile = $reportFileNamewithFullPath;
        $file       = fopen($reportFile, "r");
        $firstRow   = fgets($file);
        $header     = preg_split("/[\t]/", $firstRow);
        $all_rows   = array();
        $header     = array_map('trim', $header);

        while (!feof($file)) {
            $row = fgets($file);
            if (trim($row) != '') {
                $row        = preg_split("/[\t]/", $row);
                $row        = $this->utf8ize($row);
                $all_rows[] = array_combine($header, $row);
            }
        }

        fclose($file);

        // Input Array
        $orderReports = $all_rows;
        
        // sort array with given user-defined function
        usort($orderReports, function ($result1, $result2) {
            if (strtotime($result1['purchase-date']) < strtotime($result2['purchase-date'])) {
                return 1;
            } else if (strtotime($result1['purchase-date']) > strtotime($result2['purchase-date'])) {
                return -1;
            } else {
                return 0;
            }
        });
        return $orderReports;
    }

    public function utf8ize($mixed)
    {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = $this->utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
        }
        return $mixed;
    }

    public function getFirstAndLastOrderId(array $collection)
    {
        $firstOrderId = $collection[0]['order-id'];

        if (count($collection) > 1) {
            $row         = end($collection);
            $lastOrderId = $row['order-id'];
        } else {
            $lastOrderId = $firstOrderId;
        }

        return ['firstOrderId' => $firstOrderId, 'lastOrderId' => $lastOrderId];
    }
    
    /**
     * Get Order Item Price
     *
     * @param $data
     * @return array
     */
    public function getOrderItemPrice($data)
    {
        $prices = [];
        foreach($data as $value) {
            $itemId = key($value);
            $price = $value[$itemId];
            $prices[$itemId] = $price;
        }
        
        return $prices;
        
    }
    
    /**
     * Write Log
     *
     * @param $data
     * @param $type
     */
    public function writeLog($data, $type)
    {
        switch ($type) {
            case 'create':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_order.log');
                break;
            case 'cancel':
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_cancel_order.log');
                break;
            default:
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/amazon_order.log');
        }
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($data, true));
    }
}
